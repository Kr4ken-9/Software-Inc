﻿using System;
using UnityEngine;

public class PlotObject : MonoBehaviour
{
	public Color GetColor(float alpha)
	{
		return (!this.Plot.PlayerOwned) ? this.Plot.PlotColor.ToColor().Alpha(alpha) : new Color(1f, 1f, 1f, 0.5f);
	}

	public void UpdatePlayerOwned()
	{
		this.Renderer.material.color = this.GetColor(0.5f);
		this.PlotMesh.gameObject.SetActive((GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode) && this.Plot.PlayerOwned);
	}

	public MeshFilter EdgeMesh;

	public MeshFilter PlotMesh;

	public MeshRenderer Renderer;

	public MeshRenderer EdgeRenderer;

	public PlotArea Plot;
}
