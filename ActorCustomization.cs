﻿using System;
using System.Collections.Generic;
using System.Linq;
using SSAA;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CinematicEffects;
using UnityStandardAssets.ImageEffects;

public class ActorCustomization : MonoBehaviour, IStylable
{
	public int StartYear
	{
		get
		{
			return ActorCustomization.StartYears[this.Year.Selected] - SDateTime.BaseYear;
		}
	}

	public List<ActorBodyItem> BodyItems
	{
		get
		{
			return this._bodyItems;
		}
		set
		{
			this._bodyItems = value;
		}
	}

	public Transform GetTransform()
	{
		return base.transform;
	}

	public void ChangeCategory(int cat)
	{
		this.CurrentCategory = cat;
		switch (cat)
		{
		case 0:
			this.HeadSliderPanel.SetActive(true);
			this.BodyPartPanel.SetActive(false);
			this.ColorPanel.SetActive(false);
			this.HeadSliderScroll.value = 1f;
			break;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			this.HeadSliderPanel.SetActive(false);
			this.BodyPartPanel.SetActive(true);
			this.ColorPanel.SetActive(false);
			this.UpdateBodyParts();
			this.BodyPartScroll.value = 1f;
			break;
		case 6:
			this.HeadSliderPanel.SetActive(false);
			this.BodyPartPanel.SetActive(false);
			this.ColorPanel.SetActive(true);
			this.UpdateColors();
			this.ColorScroll.value = 1f;
			break;
		}
	}

	private void GenerateBodyButtons()
	{
		foreach (ActorBodyItem actorBodyItem in from x in ActorStyler.Instance.Items
		select x.GetComponent<ActorBodyItem>())
		{
			if (!actorBodyItem.Hidden)
			{
				string key = actorBodyItem.Type + actorBodyItem.Name;
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ThumbnailButtonPrefab);
				gameObject.transform.GetChild(0).GetComponent<Image>().sprite = actorBodyItem.Thumbnail;
				ActorBodyItem item1 = actorBodyItem;
				gameObject.GetComponent<Button>().onClick.AddListener(delegate
				{
					ActorBodyItem actorBodyItem2 = this.BodyItems.FirstOrDefault((ActorBodyItem x) => x.Type == item1.Type && x.Name.Equals(item1.Name));
					if (actorBodyItem2 != null)
					{
						if (item1.CanDeselect)
						{
							this.BodyItems.Remove(actorBodyItem2);
							UnityEngine.Object.Destroy(actorBodyItem2.gameObject);
							this.UpdateColors();
						}
					}
					else
					{
						ActorBodyItem actorBodyItem3 = ActorStyler.Instance.SetItem(this, key);
						if (actorBodyItem3.Colormap.Any((ActorBodyItem.ColorMapping x) => x.ColorName.Equals("Skin")))
						{
							ActorBodyItem actorBodyItem4 = this.BodyItems.First((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head);
							actorBodyItem3.SetColor("Skin", actorBodyItem4.GetColor("Skin"));
						}
						this.UpdateSliders(actorBodyItem3);
						this.UpdateColors();
					}
				});
				gameObject.GetComponent<GUIToolTipper>().ToolTipValue = actorBodyItem.Name;
				gameObject.transform.SetParent(this.BodyPartContent, false);
				gameObject.SetActive(false);
				this.BodyButtons[actorBodyItem] = gameObject;
			}
		}
	}

	private void UpdateSliders(ActorBodyItem item)
	{
		List<KeyValuePair<string, Slider>> list;
		if (this.HeadSliders.TryGetValue(item.Type, out list))
		{
			SkinnedMeshRenderer component = item.rend.GetComponent<SkinnedMeshRenderer>();
			for (int i = 0; i < list.Count; i++)
			{
				KeyValuePair<string, Slider> slider = list[i];
				ActorBodyItem.BlendKeys blendKeys = item.Blends.First((ActorBodyItem.BlendKeys x) => x.BlendName.Equals(slider.Key));
				if (blendKeys.doubleKey)
				{
					float blendShapeWeight = component.GetBlendShapeWeight(blendKeys.Index);
					float blendShapeWeight2 = component.GetBlendShapeWeight(blendKeys.Index2);
					slider.Value.value = blendShapeWeight2 - blendShapeWeight;
				}
				else
				{
					slider.Value.value = component.GetBlendShapeWeight(blendKeys.Index);
				}
			}
		}
	}

	private void GenerateSliderCat(ActorBodyItem.BodyType type)
	{
		GameObject gameObject = ActorStyler.Instance.Items.First((GameObject x) => x.GetComponent<ActorBodyItem>().Type == type);
		GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.MajorLabelPrefab);
		gameObject2.GetComponent<Text>().text = type.ToString();
		gameObject2.transform.SetParent(this.HeadSliderContent, false);
		List<KeyValuePair<string, Slider>> list = new List<KeyValuePair<string, Slider>>();
		this.HeadSliders[type] = list;
		ActorBodyItem.BlendKeys[] blends = gameObject.GetComponent<ActorBodyItem>().Blends;
		for (int i = 0; i < blends.Length; i++)
		{
			ActorBodyItem.BlendKeys blendKeys = blends[i];
			ActorBodyItem.BlendKeys lBlend = blendKeys;
			gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.MinorLabelPrefab);
			gameObject2.GetComponent<Text>().text = blendKeys.BlendName;
			gameObject2.transform.SetParent(this.HeadSliderContent, false);
			gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.SliderPrefab);
			Slider component = gameObject2.GetComponent<Slider>();
			component.maxValue = 100f;
			if (blendKeys.doubleKey)
			{
				component.minValue = -100f;
			}
			component.onValueChanged.AddListener(delegate(float x)
			{
				ActorBodyItem actorBodyItem = this.BodyItems.First((ActorBodyItem z) => z.Type == type);
				SkinnedMeshRenderer component2 = actorBodyItem.rend.GetComponent<SkinnedMeshRenderer>();
				if (lBlend.doubleKey)
				{
					if (x < 0f)
					{
						component2.SetBlendShapeWeight(lBlend.Index, -x);
						component2.SetBlendShapeWeight(lBlend.Index2, 0f);
					}
					else
					{
						component2.SetBlendShapeWeight(lBlend.Index2, x);
						component2.SetBlendShapeWeight(lBlend.Index, 0f);
					}
				}
				else
				{
					component2.SetBlendShapeWeight(lBlend.Index, x);
				}
			});
			list.Add(new KeyValuePair<string, Slider>(blendKeys.BlendName, component));
			gameObject2.transform.SetParent(this.HeadSliderContent, false);
		}
	}

	private void GenerateSliders()
	{
		this.GenerateSliderCat(ActorBodyItem.BodyType.Head);
		this.GenerateSliderCat(ActorBodyItem.BodyType.Eyebrows);
	}

	private void CreateSkinColorButton()
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
		this._skinButton = gameObject.GetComponent<ColorBarButton>();
		this._skinButton.colors = new Color[]
		{
			Color.white
		};
		gameObject.GetComponentInChildren<Text>().text = "Skin";
		gameObject.GetComponent<Button>().onClick.AddListener(delegate
		{
			WindowManager.SpawnColorDialog(delegate(Color c)
			{
				foreach (ActorBodyItem actorBodyItem in this.BodyItems)
				{
					actorBodyItem.SetColor("Skin", c);
				}
				this._skinButton.colors[0] = c;
				this._skinButton.Refresh();
			}, this.BodyItems.First((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head).GetColor("Skin"), this.SkinColors);
		});
		gameObject.transform.SetParent(this.ColorContent, false);
	}

	private void UpdateColors()
	{
		foreach (GameObject obj in this.ColorButtons)
		{
			UnityEngine.Object.Destroy(obj);
		}
		this._skinButton.colors[0] = this.BodyItems.First((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head).GetColor("Skin");
		this._skinButton.Refresh();
		this.ColorButtons.Clear();
		foreach (ActorBodyItem actorBodyItem in this.BodyItems)
		{
			if (actorBodyItem.Colormap != null && actorBodyItem.Colormap.Length != 0)
			{
				if (actorBodyItem.Type != ActorBodyItem.BodyType.Head || !(actorBodyItem.rend.material.GetTexture("_OverlayTex") == null))
				{
					List<ActorBodyItem.ColorMapping> colorMap = (from x in actorBodyItem.Colormap
					where !x.ColorName.Equals("Skin")
					select x).ToList<ActorBodyItem.ColorMapping>();
					if (colorMap.Count != 0)
					{
						GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
						ColorBarButton cb = gameObject.GetComponent<ColorBarButton>();
						ActorBodyItem item1 = actorBodyItem;
						cb.colors = (from x in colorMap
						select item1.rend.material.GetColor(x.MaterialSlot)).ToArray<Color>();
						gameObject.GetComponentInChildren<Text>().text = actorBodyItem.GetColorName();
						gameObject.GetComponent<Button>().onClick.AddListener(delegate
						{
							ActorCustomization.ChangeColor(item1, colorMap, cb);
						});
						gameObject.transform.SetParent(this.ColorContent, false);
						this.ColorButtons.Add(gameObject);
					}
				}
			}
		}
	}

	private static void ChangeColor(ActorBodyItem item, List<ActorBodyItem.ColorMapping> colorMap, ColorBarButton cb)
	{
		string[] tabs = (from x in colorMap
		select x.ColorName).ToArray<string>();
		string name = item.Name;
		Action<Color>[] actions = colorMap.Select((ActorBodyItem.ColorMapping x, int i) => delegate(Color y)
		{
			try
			{
				item.rend.material.SetColor(x.MaterialSlot, y);
			}
			catch (Exception ex)
			{
				Debug.LogException(new Exception("Error changing color for " + name + ":\n" + ex.ToString()));
			}
			cb.colors[i] = y;
			cb.Refresh();
		}).ToArray<Action<Color>>();
		Color[] startColors = (from x in colorMap
		select item.rend.material.GetColor(x.MaterialSlot)).ToArray<Color>();
		HashSet<Color> hashSet = new HashSet<Color>();
		foreach (ActorBodyItem.ColorMapping colorMapping in colorMap)
		{
			if (!string.IsNullOrEmpty(colorMapping.LogicalCategory))
			{
				foreach (ActorBodyItem actorBodyItem in ActorCustomization.Instance.BodyItems)
				{
					foreach (ActorBodyItem.ColorMapping colorMapping2 in actorBodyItem.Colormap)
					{
						if (colorMapping.LogicalCategory.Equals(colorMapping2.LogicalCategory))
						{
							hashSet.Add(actorBodyItem.GetColor(colorMapping2.ColorName));
						}
					}
				}
			}
		}
		WindowManager.SpawnColorDialog(tabs, actions, startColors, hashSet.ToArray<Color>(), null);
	}

	private void UpdateBodyParts()
	{
		foreach (KeyValuePair<ActorBodyItem, GameObject> keyValuePair in this.BodyButtons)
		{
			keyValuePair.Value.SetActive(this.GenderMatch(keyValuePair.Key.Gender) && keyValuePair.Key.guiCategory == (ActorBodyItem.GUICategory)this.CurrentCategory);
		}
	}

	private bool GenderMatch(ActorBodyItem.GenderType gender)
	{
		if (gender != ActorBodyItem.GenderType.Male)
		{
			return gender != ActorBodyItem.GenderType.Female || this.Female;
		}
		return !this.Female;
	}

	private void Start()
	{
		ActorStyler.Instance.InitShadow(this);
		this.SSAO.enabled = Options.AmbientOcclusion;
		this.bloom.enabled = Options.Bloom;
		this.SSAA.enabled = (Options.SSAA > 10);
		internal_SSAA.ChangeScale((float)Options.SSAA / 10f);
		this.FXAA.enabled = Options.FXAA;
		this.SMAA.enabled = Options.SMAA;
		foreach (Light light in this.lights)
		{
			light.shadows = ((!Options.MoreShadow) ? LightShadows.None : LightShadows.Soft);
		}
		this.GenderText.text = "Male".Loc();
		string text = this.CompanyName.text.Loc();
		this.CompanyName.text = text;
		this.defaultCompanyName = text;
		this.Difficulty.UpdateContent<string>(GameData.DifficultySettings);
		this.Difficulty.Selected = 1;
		this.GenerateSliders();
		this.GenerateBodyButtons();
		this.CreateSkinColorButton();
		ActorStyler.Instance.ApplySavedStyle(ActorStyler.Instance.GenerateStyle(this.Female, "Default"), this);
		this.UpdateSliders(this.BodyItems.First((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head));
		this.UpdateSliders(this.BodyItems.First((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Eyebrows));
		this.UpdateColors();
		ActorCustomization.Instance = this;
		int num = this.Skill.Length;
		for (int l = 0; l < num; l++)
		{
			this.Skill[l].value = ActorCustomization.MaxPoints[this.Difficulty.Selected] / (float)num;
		}
		this.Anim.SetInteger("AnimControl", 0);
		this.ModList.Items = GameData.ModPackages.Cast<object>().ToList<object>();
		this.StartMoney.maxValue = (float)(ActorCustomization.StartLoans.Length - 1);
		this.UpdateMoneyDescription();
		this.Year.UpdateContent<int>(ActorCustomization.StartYears);
		this.UpdatePersonalities();
		for (int k = 0; k < this.PersonalityChosen.Length; k++)
		{
			int j = k;
			this.PersonalityChosen[k].OnSelectedChanged.AddListener(delegate
			{
				this.UpdateIncompatibilities(j);
				this.UpdateTraits();
			});
		}
		TutorialSystem.Instance.StartTutorial("Customization", false);
		this.UpdateSpec();
		this.UpdateDaysPerMonth();
	}

	public void ToggleZoom()
	{
		this.CurrentCameraPosition = (this.CurrentCameraPosition + 1) % this.CameraPositions.Length;
	}

	public void UpdatePersonalities()
	{
		string[] array = new string[]
		{
			this.PersonalityChosen[0].SelectedItemString,
			this.PersonalityChosen[1].SelectedItemString
		};
		List<string> personalityTraits = GameData.AllPersonalities().PersonalityTraits;
		this.PersonalityChosen[0].UpdateContent<string>(personalityTraits);
		this.PersonalityChosen[1].UpdateContent<string>(personalityTraits);
		for (int i = 0; i < 2; i++)
		{
			if (this.PersonalityChosen[i].Items.Contains(array[i]))
			{
				this.PersonalityChosen[i].SelectedItem = array[i];
			}
			else
			{
				this.PersonalityChosen[i].Selected = 0;
			}
			this.UpdateIncompatibilities(i);
		}
		this.UpdateTraits();
	}

	private void UpdateTraits()
	{
		PersonalityGraph personalityGraph = GameData.AllPersonalities();
		float[][] array = new float[this.PersonalityChosen.Length][];
		for (int i = 0; i < this.PersonalityChosen.Length; i++)
		{
			array[i] = personalityGraph.GetEffects(this.PersonalityChosen[i].SelectedItemString);
		}
		this.TraitBars[0].Value = (from x in array
		select x[0]).Average().MapRange(-1f, 1f, 0f, 1f, false);
		this.TraitBars[1].Value = (from x in array
		select x[1]).Average().MapRange(-1f, 1f, 0f, 1f, false);
		this.TraitBars[2].Value = (from x in array
		select x[2]).Average().MapRange(-1f, 1f, 0f, 1f, false);
	}

	public void UpdateSpec()
	{
		string[][] unlockedSpecializations = GameData.GetUnlockedSpecializations(this.StartYear);
		this.DesignSpec.UpdateContent<string>(unlockedSpecializations[0]);
		this.CodeSpec.UpdateContent<string>(unlockedSpecializations[1]);
		this.ArtSpec.UpdateContent<string>(unlockedSpecializations[2]);
		if (this.DesignSpec.Selected < 0)
		{
			this.DesignSpec.Selected = 0;
		}
		if (this.CodeSpec.Selected < 0)
		{
			this.CodeSpec.Selected = 0;
		}
		if (this.ArtSpec.Selected < 0)
		{
			this.ArtSpec.Selected = 0;
		}
	}

	public void ChangeGender(bool female)
	{
		this.Female = female;
		this.GenderText.text = ((!this.Female) ? "Male".Loc() : "Female".Loc());
	}

	private void UpdateIncompatibilities(int i)
	{
		if (!this.DisablePerson)
		{
			this.DisablePerson = true;
			object selectedItem = this.PersonalityChosen[1 - i].SelectedItem;
			PersonalityGraph personalityGraph = GameData.AllPersonalities();
			HashSet<string> incompatible = personalityGraph.GetIncompatibilities(this.PersonalityChosen[i].SelectedItemString);
			List<string> list = personalityGraph.PersonalityTraits.ToList<string>();
			list.RemoveAll((string x) => incompatible.Contains(x));
			this.PersonalityChosen[1 - i].UpdateContent<string>(list);
			if (this.PersonalityChosen[1 - i].Items.Contains(selectedItem))
			{
				this.PersonalityChosen[1 - i].SelectedItem = selectedItem;
			}
			else
			{
				object p2 = this.PersonalityChosen[i].SelectedItem;
				this.PersonalityChosen[1 - i].SelectedItem = this.PersonalityChosen[1 - i].Items.First((object x) => !x.Equals(p2));
			}
			this.DisablePerson = false;
		}
	}

	private void Update()
	{
		this.MainCamera.position = Vector3.Lerp(this.MainCamera.position, this.CameraPositions[this.CurrentCameraPosition].position, Time.deltaTime * 5f);
		this.MainCamera.rotation = Quaternion.Lerp(this.MainCamera.rotation, this.CameraPositions[this.CurrentCameraPosition].rotation, Time.deltaTime * 5f);
		if (Input.GetMouseButtonDown(0))
		{
			ActorCustomization.CastResult.Clear();
			EventSystem.current.RaycastAll(new PointerEventData(EventSystem.current).PopulateDefault(), ActorCustomization.CastResult);
			if (ActorCustomization.CastResult.None((RaycastResult x) => x.gameObject != WindowManager.Instance.BlockPanel))
			{
				this.drag = Input.mousePosition.x;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit[] l = Physics.RaycastAll(ray, 100f);
				if (l.Any((RaycastHit x) => x.rigidbody != null && x.rigidbody.gameObject == base.gameObject))
				{
					this.dragNow = true;
				}
			}
		}
		if (Input.GetMouseButtonUp(0))
		{
			this.dragNow = false;
		}
		if (this.dragNow)
		{
			base.transform.rotation = Quaternion.Euler(0f, base.transform.rotation.eulerAngles.y + this.drag - Input.mousePosition.x, 0f);
			this.drag = Input.mousePosition.x;
		}
		if (Input.GetKeyUp(KeyCode.Escape) && !WindowManager.HasModal)
		{
			this.CancelClick();
		}
	}

	public Actor GenerateActor()
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.FinalActor);
		Actor component = gameObject.GetComponent<Actor>();
		component.Female = this.Female;
		component.employee = new Employee(new SDateTime(ActorCustomization.StartYears[this.Year.Selected]), this.Female, this.FounderName.text, this.Skill.SelectInPlace((Slider x) => x.value), this.PersonalityChosen.SelectInPlace((GUICombobox x) => x.SelectedItem.ToString()), new string[]
		{
			this.CodeSpec.SelectedItemString,
			this.DesignSpec.SelectedItemString,
			this.ArtSpec.SelectedItemString
		}, GameData.AllPersonalities());
		component.enabled = false;
		component._savedStyle = this.BodyItems.SelectInPlace((ActorBodyItem x) => x.Save());
		return component;
	}

	public void UpdateEyes()
	{
		ActorBodyItem actorBodyItem = this._bodyItems.FirstOrDefault((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head);
		if (actorBodyItem != null)
		{
			base.GetComponent<EyeScript>().Face = actorBodyItem.rend.material;
		}
	}

	public void UpdateHairColor(Color col)
	{
	}

	public void UpdateSkinColor(Color col)
	{
	}

	public void PostUpdate()
	{
	}

	public void SetLOD2Color(string part, Color col)
	{
	}

	public void UpdateSkillStat(int changed)
	{
		this.UpdateStat(changed, true);
	}

	public void UpdateSEducationStat(int changed)
	{
		this.UpdateStat(changed, false);
	}

	public void ScaleSkillStats()
	{
		this.DisableStat = true;
		float num = this.Skill.Sum((Slider x) => x.value);
		float num2 = ActorCustomization.MaxPoints[this.Difficulty.Selected];
		for (int i = 0; i < this.Skill.Length; i++)
		{
			this.Skill[i].value = this.Skill[i].value * (num2 / num);
		}
		num = this.Skill.Sum((Slider x) => x.value);
		if (num < num2)
		{
			float num3 = num2 - num;
			for (int j = 0; j < this.Skill.Length; j++)
			{
				float num4 = Mathf.Min(1f - this.Skill[j].value, num3);
				if (num4 > 0f)
				{
					this.Skill[j].value += num3;
					num3 -= num4;
					if (num3 <= 0f)
					{
						break;
					}
				}
			}
		}
		this.DisableStat = false;
	}

	private void UpdateStat(int changed, bool skills)
	{
		if (!this.DisableStat)
		{
			this.DisableStat = true;
			Slider[] skill = this.Skill;
			float num = skill.Sum((Slider x) => x.value);
			float num2 = num - skill[changed].value;
			float num3 = ActorCustomization.MaxPoints[this.Difficulty.Selected];
			if (num != num3 && num2 > 0f)
			{
				float num4 = num3 - skill[changed].value;
				for (int i = 0; i < skill.Length; i++)
				{
					if (i != changed)
					{
						skill[i].value = Mathf.Min(1f, skill[i].value / num2 * num4);
					}
				}
			}
			float num5 = skill.Sum((Slider x) => x.value);
			if (num5 < num3)
			{
				float num6 = num3 - num5;
				int num7 = skill.Length - 1;
				for (int j = 0; j < skill.Length; j++)
				{
					if (j != changed)
					{
						float value = skill[j].value;
						float num8 = num6 / (float)num7;
						skill[j].value = Mathf.Min(1f, skill[j].value + num8);
						num6 -= skill[j].value - value;
						num7--;
					}
				}
			}
			this.DisableStat = false;
		}
	}

	public void ToggleGender()
	{
		this.ChangeGender(!this.Female);
		ActorStyler.Instance.ApplySavedStyle(ActorStyler.Instance.GenerateStyle(this.Female, "Default"), this);
		this.UpdateBodyParts();
		this.UpdateSliders(this.BodyItems.First((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head));
		this.UpdateSliders(this.BodyItems.First((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Eyebrows));
		this.UpdateColors();
	}

	public void UpdateMoneyDescription()
	{
		if (this.StartMoney.value > 0f)
		{
			int num = ActorCustomization.StartLoans[Mathf.FloorToInt(this.StartMoney.value)];
			int num2 = ActorCustomization.StartLoanMonths;
			int x = ActorCustomization.DefaultStartMoney[this.Difficulty.Selected] + num;
			float num3 = LoanWindow.CalculateInterest(num2, Mathf.FloorToInt((float)num / LoanWindow.factor));
			float num4 = num3 * (float)num + (float)num / (float)num2;
			if (num == 0)
			{
				num2 = 0;
			}
			this.MoneyDesc.SetData(new string[]
			{
				"Starting funds".Loc(),
				"Loan".Loc(),
				"Monthly".Loc(),
				"Deadline".Loc(),
				"Cost".Loc()
			}, new string[]
			{
				x.CurrencyInt(true),
				num.CurrencyInt(true),
				num4.Currency(true),
				Utilities.DateDiff(new SDateTime(0, 0, 0), new SDateTime(0, num2, 0)),
				(num4 * (float)num2).Currency(true)
			});
		}
		else
		{
			this.MoneyDesc.SetData(new string[]
			{
				"Starting funds".Loc()
			}, new string[]
			{
				ActorCustomization.DefaultStartMoney[this.Difficulty.Selected].CurrencyInt(true)
			});
		}
	}

	public void StartGameClick()
	{
		if (this.defaultCompanyName != null && this.defaultCompanyName.Equals(this.CompanyName.text))
		{
			DialogWindow dialog = WindowManager.SpawnDialog();
			dialog.Show("CompanyNameWarning".Loc(), false, DialogWindow.DialogType.Warning, new KeyValuePair<string, Action>[]
			{
				new KeyValuePair<string, Action>("Yes", delegate
				{
					this.StartGame();
					dialog.Window.Close();
				}),
				new KeyValuePair<string, Action>("No", delegate
				{
					dialog.Window.Close();
				})
			});
			return;
		}
		if (string.IsNullOrEmpty(this.CompanyName.text.Trim()))
		{
			DialogWindow dialog = WindowManager.SpawnDialog();
			dialog.Show("NoCompanyName".Loc(), false, DialogWindow.DialogType.Error, new KeyValuePair<string, Action>[]
			{
				new KeyValuePair<string, Action>("OK", delegate
				{
					dialog.Window.Close();
				})
			});
			return;
		}
		this.StartGame();
	}

	private void StartGame()
	{
		SaveGameManager.Instance.Show(false, false, true, true, delegate(SaveGame save)
		{
			int num = ActorCustomization.StartLoans[Mathf.FloorToInt(this.StartMoney.value)];
			int num2 = ActorCustomization.DefaultStartMoney[this.Difficulty.Selected] + num;
			float num3 = 0f;
			if (save != null)
			{
				if (save.GetBuildMeta()[0] == 1f)
				{
					num3 = save.GetBuildMeta()[2];
				}
			}
			else
			{
				num3 = PlotArea.StartPlotPrice;
			}
			if (num3 > (float)num2)
			{
				WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), true, DialogWindow.DialogType.Error);
				return;
			}
			this.LoadingPanel.SetActive(true);
			Camera.main.Render();
			GameData.SelectedDifficulty = this.Difficulty.Selected;
			GameData.CompanyName = this.CompanyName.text;
			GameData.StartingMoney = ActorCustomization.DefaultStartMoney[this.Difficulty.Selected] - Mathf.FloorToInt(num3);
			GameData.ActiveYear = this.StartYear;
			GameData.LoanAmount = num;
			GameData.EditMode = false;
			Actor actor = this.GenerateActor();
			UnityEngine.Object.DontDestroyOnLoad(actor.gameObject);
			GameData.founder = actor;
			if (save != null)
			{
				GameData.LoadYear = ActorCustomization.StartYears[this.Year.Selected];
				SaveGameManager.LoadGame(save, null, false, false, true, false);
			}
			else
			{
				ErrorLogging.FirstOfScene = true;
				ErrorLogging.SceneChanging = true;
				SceneManager.LoadScene("MainScene");
			}
		});
		SaveGameManager.Instance.BuildingToggle.isOn = true;
	}

	public static int GetDefaultStartMoney()
	{
		if (ActorCustomization.Instance != null)
		{
			return ActorCustomization.DefaultStartMoney[ActorCustomization.Instance.Difficulty.Selected];
		}
		if (GameSettings.Instance != null)
		{
			return ActorCustomization.DefaultStartMoney[GameSettings.Instance.Difficulty];
		}
		return ActorCustomization.DefaultStartMoney[GameData.SelectedDifficulty];
	}

	public void UpdateDaysPerMonth()
	{
		GameData.DaysPerMonth = (int)this.DaysPerMonth.value;
		this.DaysPerMonthLabel.text = "DayPostfix".LocPlural(GameData.DaysPerMonth);
	}

	public void CancelClick()
	{
		ErrorLogging.FirstOfScene = true;
		ErrorLogging.SceneChanging = true;
		SceneManager.LoadScene("MainMenu");
	}

	public void PickStyle()
	{
		List<KeyValuePair<string, KeyValuePair<bool, ActorBodyItem.BodyItemObject[]>>> styles = GameData.SavedStyles.ToList<KeyValuePair<string, KeyValuePair<bool, ActorBodyItem.BodyItemObject[]>>>();
		this.SelectWindow.Show("Style", from x in styles
		select x.Key, delegate(int i)
		{
			this.FounderName.text = styles[i].Key;
			ActorStyler.Instance.ApplySavedStyle(styles[i].Value.Value, this);
			this.ChangeGender(styles[i].Value.Key);
			this.UpdateBodyParts();
			this.UpdateSliders(this.BodyItems.First((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head));
			this.UpdateSliders(this.BodyItems.First((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Eyebrows));
			this.UpdateColors();
		}, false, true, true, false, delegate(int i)
		{
			GameData.DeleteStyle(styles[i].Key);
		});
	}

	public void SaveStyle()
	{
		GameData.CreateStyle(Utilities.CleanFileName(this.FounderName.text), this.Female, this.BodyItems.SelectInPlace((ActorBodyItem x) => x.Save()));
	}

	static ActorCustomization()
	{
		// Note: this type is marked as 'beforefieldinit'.
		int[] array = new int[3];
		array[0] = 25000;
		array[1] = 10000;
		ActorCustomization.DefaultStartMoney = array;
		ActorCustomization.StartLoans = new int[]
		{
			0,
			10000,
			40000,
			90000
		};
		ActorCustomization.StartLoanMonths = 60;
		ActorCustomization.StartYears = new int[]
		{
			1980,
			1990,
			2000,
			2010
		};
		ActorCustomization.MaxPoints = new float[]
		{
			2.5f,
			1.5f,
			1f
		};
		ActorCustomization.CastResult = new List<RaycastResult>();
	}

	public Image[] Colors;

	public Text GenderText;

	public Text DaysPerMonthLabel;

	public VarValueSheet MoneyDesc;

	public InputField CompanyName;

	public InputField FounderName;

	public static int[] DefaultStartMoney;

	public static int[] StartLoans;

	public static int StartLoanMonths;

	public static int[] StartYears;

	public GUICombobox Difficulty;

	public GUICombobox Year;

	public GUICombobox ArtSpec;

	public GUICombobox DesignSpec;

	public GUICombobox CodeSpec;

	public GUICombobox[] PersonalityChosen;

	public Slider[] Skill;

	public Slider StartMoney;

	public Slider DaysPerMonth;

	public Transform[] CameraPositions;

	public DotBar[] TraitBars;

	public int CurrentCameraPosition;

	public Transform MainCamera;

	public GameObject ThumbnailButtonPrefab;

	public GameObject MajorLabelPrefab;

	public GameObject MinorLabelPrefab;

	public GameObject SliderPrefab;

	public GameObject ButtonPrefab;

	public GameObject BodyPartPanel;

	public GameObject HeadSliderPanel;

	public GameObject ColorPanel;

	public Transform BodyPartContent;

	public Transform HeadSliderContent;

	public Transform ColorContent;

	public Scrollbar BodyPartScroll;

	public Scrollbar HeadSliderScroll;

	public Scrollbar ColorScroll;

	public MultiSelectWindow SelectWindow;

	public GUIListView ModList;

	public SSAOPro SSAO;

	public BloomOptimized bloom;

	public Antialiasing FXAA;

	public AntiAliasing SMAA;

	public SuperSampling_SSAA SSAA;

	public Light[] lights;

	public bool Female;

	public int CurrentCategory;

	public Color[] SkinColors;

	public Color[] HairColors;

	public GameObject FinalActor;

	public GameObject LoadingPanel;

	public Texture2D[] MaleEyes;

	public Texture2D[] FemaleEyes;

	public Texture2D ColorMap;

	public Texture2D LightMap;

	private float drag;

	private bool dragNow;

	private bool DisableStat;

	private bool DisablePerson;

	public static float[] MaxPoints;

	public static ActorCustomization Instance;

	public Animator Anim;

	public int PersonalityCounter;

	[NonSerialized]
	private List<ActorBodyItem> _bodyItems = new List<ActorBodyItem>();

	[NonSerialized]
	private Dictionary<ActorBodyItem, GameObject> BodyButtons = new Dictionary<ActorBodyItem, GameObject>();

	[NonSerialized]
	private Dictionary<ActorBodyItem.BodyType, List<KeyValuePair<string, Slider>>> HeadSliders = new Dictionary<ActorBodyItem.BodyType, List<KeyValuePair<string, Slider>>>();

	[NonSerialized]
	private List<GameObject> ColorButtons = new List<GameObject>();

	[NonSerialized]
	private ColorBarButton _skinButton;

	public GUIToolTipper DaysPerMonthsKnob;

	private string defaultCompanyName;

	private static readonly List<RaycastResult> CastResult;

	public string UseStyle = "Default";
}
