﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HireWindow : MonoBehaviour
{
	public Team GetCompareTeam()
	{
		if (this.SelectedTeam != null && this.CompareTeam.isOn)
		{
			return GameSettings.Instance.sActorManager.Teams.GetOrNull(this.SelectedTeam);
		}
		return null;
	}

	private void Awake()
	{
		this.InterviewPanels["Person"] = this.PersonInterviewPanel;
		this.InterviewPanels["Spec"] = this.SpecInterviewPanel;
		this.InterviewPanels["Skill"] = this.SkillInterviewPanel;
		this.InterviewButtons["Person"] = this.PersonInterviewButton;
		this.InterviewButtons["Spec"] = this.SpecInterviewButton;
		this.InterviewButtons["Skill"] = this.SkillInterviewButton;
		this.RoleCombo.UpdateContent<object>(new List<object>
		{
			"Lead",
			"Programmer",
			"Designer",
			"Artist",
			"Marketer"
		});
		this.RoleCombo.OnSelectedChanged.AddListener(delegate
		{
			this.SpecCombo.UpdateContent<string>(GameSettings.Instance.GetUnlockedSpecializations((Employee.EmployeeRole)this.RoleCombo.Selected));
			bool active = Employee.IsSpecialized((Employee.EmployeeRole)this.RoleCombo.Selected);
			this.SpecCombo.gameObject.SetActive(active);
			this.SpecLabel.SetActive(active);
		});
		this.WageBracket.UpdateContent<string>(Enum.GetNames(typeof(Employee.WageBracket)));
		this.EmployeeList.OnSelectChange = delegate(bool d)
		{
			Employee employee = this.EmployeeList.GetSelected<Employee>().FirstOrDefault<Employee>();
			if (employee != null)
			{
				foreach (KeyValuePair<string, HashSet<Employee>> keyValuePair in this.AlreadyInterviewed)
				{
					if (keyValuePair.Value.Contains(employee))
					{
						this.InterviewPanels[keyValuePair.Key].SetActive(true);
						this.InterviewButtons[keyValuePair.Key].gameObject.SetActive(false);
					}
					else
					{
						this.InterviewPanels[keyValuePair.Key].SetActive(false);
						this.InterviewButtons[keyValuePair.Key].gameObject.SetActive(true);
					}
				}
				this.UpdateInterviewPanel(employee);
				this.chart.SetContent(new Employee[]
				{
					employee
				});
				if (this.SelectedTeam != null)
				{
					Team team = GameSettings.Instance.sActorManager.Teams[this.SelectedTeam];
					this.CompatText.text = "Compatibility".Loc() + ": " + this.GetCompatibility(team);
					this.CompatText.color = this.GetCompatColor(team);
					this.chart.CompareTeam = this.GetCompareTeam();
					this.chart.MinSkillTeam = team;
				}
				else
				{
					this.CompatText.text = "Compatibility".Loc() + ": " + "TeamCompat0".Loc();
					this.CompatText.color = this.GetCompatColor(null);
					this.chart.CompareTeam = null;
					this.chart.MinSkillTeam = null;
				}
			}
			else
			{
				foreach (Button button in this.InterviewButtons.Values)
				{
					button.gameObject.SetActive(false);
				}
				foreach (GameObject gameObject in this.InterviewPanels.Values)
				{
					gameObject.SetActive(false);
				}
			}
		};
		this.Window.OnClose = delegate
		{
			GameSettings.ForcePause = false;
		};
	}

	public void UpdateInterviewToggles()
	{
		this.UpdateCost();
	}

	public void UpdateWageBracket()
	{
		float num = 0.25f;
		float employeeWorth = Employee.GetEmployeeWorth(this.RoleCombo.Selected, (float)this.WageBracket.Selected * num, (float)Employee.AgeBrackets[this.WageBracket.Selected][0], 0f);
		string text = employeeWorth.Currency(true);
		if (this.WageBracket.Selected < 2)
		{
			float employeeWorth2 = Employee.GetEmployeeWorth(this.RoleCombo.Selected, (float)this.WageBracket.Selected * num + num, (float)Employee.AgeBrackets[this.WageBracket.Selected][1], 0f);
			text = text + " - " + employeeWorth2.Currency(true);
		}
		else
		{
			text += "+";
		}
		this.WageLabel.text = text;
	}

	public void UpdateCompatibilities(IEnumerable<Employee> employees)
	{
		this.Compatibility.Clear();
		if (this.SelectedTeam != null)
		{
			Team orNull = GameSettings.Instance.sActorManager.Teams.GetOrNull(this.SelectedTeam);
			if (orNull != null && orNull.Count > 0)
			{
				foreach (Employee employee in employees)
				{
					this.Compatibility[employee] = orNull.GetCompatibility(employee);
				}
			}
		}
		this.EmployeeList.UpdateActiveList(true);
	}

	public static float GetFinalCost(int lookLevel, int wageBracket, bool skill, bool person, bool spec)
	{
		float num = Mathf.Floor(Mathf.Pow((float)lookLevel, 0.8f) * 500f);
		return num * ((!skill) ? 1f : 1.2f) * ((!spec) ? 1f : 1.1f) * ((!person) ? 1f : 1.3f) * Mathf.Pow(1.3f, (float)wageBracket);
	}

	private float GetFinalCost(int lookLevel)
	{
		return HireWindow.GetFinalCost(lookLevel, this.WageBracket.Selected, this.InterviewSkill.isOn, this.InterviewPerson.isOn, this.InterviewSpec.isOn);
	}

	private float GetFinalCost()
	{
		return HireWindow.GetFinalCost(Mathf.RoundToInt(this.Cost.value), this.WageBracket.Selected, this.InterviewSkill.isOn, this.InterviewPerson.isOn, this.InterviewSpec.isOn);
	}

	private Color GetCompatColor(Team team)
	{
		if (team != null)
		{
			Actor[] employees = team.GetEmployees();
			if (employees.Length > 0)
			{
				Employee employee = this.EmployeeList.GetSelected<Employee>().FirstOrDefault<Employee>();
				if (employee != null)
				{
					float compatibility = team.GetCompatibility(employee);
					if (compatibility < 0.5f)
					{
						return this.CompatColors[0];
					}
					if (compatibility < 0.9f)
					{
						return Color.Lerp(this.CompatColors[0], this.CompatColors[1], (compatibility - 0.5f) / 0.4f);
					}
					if (compatibility < 1f)
					{
						return Color.Lerp(this.CompatColors[1], this.CompatColors[2], (compatibility - 0.9f) / 0.1f);
					}
					return this.CompatColors[2];
				}
			}
		}
		return new Color32(50, 50, 50, byte.MaxValue);
	}

	private string GetCompatibility(Team team)
	{
		Actor[] employees = team.GetEmployees();
		if (employees.Length > 0)
		{
			Employee employee = this.EmployeeList.GetSelected<Employee>().FirstOrDefault<Employee>();
			return (employee != null) ? Team.GetCompatDesc(team.GetCompatibility(employee)) : "TeamCompat0".Loc();
		}
		return "TeamCompat0".Loc();
	}

	public void ShowHireWindow()
	{
		if (GameSettings.Instance.MyCompany.CanMakeTransaction(-this.GetFinalCost()))
		{
			this.StartLooking();
		}
	}

	private void StartLooking()
	{
		UISoundFX.PlaySFX("Kaching", -1f, 0f);
		GameSettings.Instance.MyCompany.MakeTransaction(-this.GetFinalCost(), Company.TransactionCategory.Hire, null);
		this.EmployeeList["HireCompatibility"].gameObject.SetActive(this.InterviewPerson.isOn);
		this.Show(Mathf.RoundToInt(this.Cost.value), true);
		this.Cost.value = 1f;
		this.ChangeTeam(GameSettings.Instance.sActorManager.Teams.Keys.FirstOrDefault<string>());
		this.LookWindow.Close();
	}

	public void ShowLookWindow()
	{
		if (this.LookWindow.Shown)
		{
			this.LookWindow.Close();
			return;
		}
		this.UpdateCost();
		this.RoleCombo.UpdateSelection(0);
		this.LookWindow.Show();
	}

	public void UpdateCost()
	{
		this.CostText.text = this.GetFinalCost().Currency(true);
	}

	public void HireEmployee(Employee emp, bool clearSelected = true)
	{
		if (this.AboutToHire)
		{
			return;
		}
		this.AboutToHire = true;
		if (GameSettings.Instance.sActorManager.Actors.Count == 1 && emp.Salary > GameSettings.Instance.MyCompany.Money / 24f)
		{
			WindowManager.Instance.ShowMessageBox(string.Format("LowMoneyHireWarning".Loc(), emp.FullName), false, DialogWindow.DialogType.Warning, new KeyValuePair<string, Action>[]
			{
				new KeyValuePair<string, Action>("Yes", delegate
				{
					this.HireSub(emp, clearSelected);
				}),
				new KeyValuePair<string, Action>("No", delegate
				{
					this.AboutToHire = false;
				})
			});
		}
		else
		{
			this.HireSub(emp, clearSelected);
		}
	}

	private void HireSub(Employee emp, bool clearSelected)
	{
		Actor actor = GameSettings.Instance.SpawnActor(emp.Female);
		actor.employee = emp;
		actor.Team = this.SelectedTeam;
		if (this.EmployeeList.GetSelected<Employee>().FirstOrDefault<Employee>() == emp)
		{
			int b = this.EmployeeList.Selected[0];
			this.EmployeeList.ClearSelected();
			this.EmployeeList.Items.Remove(emp);
			this.UpdateCompatibilities(this.EmployeeList.Items.OfType<Employee>());
			if (!clearSelected && this.EmployeeList.Items.Count > 0)
			{
				this.EmployeeList.UpdateElements();
				this.EmployeeList.Select(Mathf.Min(this.EmployeeList.Items.Count - 1, b));
			}
		}
		else
		{
			this.EmployeeList.Items.Remove(emp);
			this.UpdateCompatibilities(this.EmployeeList.Items.OfType<Employee>());
		}
		this.AboutToHire = false;
	}

	public Employee[] GenerateEmployees(int num, Employee.WageBracket bracket, Employee.EmployeeRole role, int lvl = 10)
	{
		Employee[] array = new Employee[num];
		for (int i = 0; i < num; i++)
		{
			array[i] = new Employee(SDateTime.Now(), role, UnityEngine.Random.value > 0.5f, bracket, GameSettings.Instance.Personalities, false, (!Employee.IsSpecialized(role)) ? null : this.SpecCombo.SelectedItemString, null, null, 1f, (float)lvl * 0.01f);
		}
		return array;
	}

	public Employee[] GenerateEmployees(Employee.WageBracket bracket, Employee.EmployeeRole role, int lvl)
	{
		int num = (int)Mathf.Max(1f, Utilities.RandomGaussClamped(0.5f, 0.1f) * 10f * (float)lvl);
		return this.GenerateEmployees(num, bracket, role, lvl);
	}

	public void LookAgain()
	{
		if (!this.lookingAgain)
		{
			this.lookingAgain = true;
			float cost = this.GetFinalCost(this.LastLookLevel);
			WindowManager.Instance.ShowMessageBox("LookAgainPrompt".Loc(new object[]
			{
				cost.Currency(true)
			}), true, DialogWindow.DialogType.Question, delegate
			{
				if (GameSettings.Instance.MyCompany.CanMakeTransaction(-cost))
				{
					GameSettings.Instance.MyCompany.MakeTransaction(-cost, Company.TransactionCategory.Hire, null);
					this.Show(this.LastLookLevel, !this.Window.Shown);
				}
				else
				{
					WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), false, DialogWindow.DialogType.Error);
				}
				this.lookingAgain = false;
			}, "HireLookAgain", delegate
			{
				this.lookingAgain = false;
			});
		}
	}

	private void Show(int lvl, bool withWindow = true)
	{
		this.LastLookLevel = lvl;
		foreach (KeyValuePair<string, HashSet<Employee>> keyValuePair in this.AlreadyInterviewed)
		{
			keyValuePair.Value.Clear();
		}
		this.EmployeeList.Items.Clear();
		this.EmployeeList.Selected.Clear();
		this.Compatibility.Clear();
		Employee[] array = this.GenerateEmployees((Employee.WageBracket)this.WageBracket.Selected, (Employee.EmployeeRole)this.RoleCombo.Selected, lvl);
		this.UpdateCompatibilities(array);
		this.EmployeeList.Items = array.ToList<object>();
		this.EmployeeList.scrollbar.value = 0f;
		if (this.InterviewSkill.isOn)
		{
			this.AlreadyInterviewed["Skill"].AddRange(array);
		}
		if (this.InterviewSpec.isOn)
		{
			this.AlreadyInterviewed["Spec"].AddRange(array);
		}
		if (this.InterviewPerson.isOn)
		{
			this.AlreadyInterviewed["Person"].AddRange(array);
		}
		if (withWindow)
		{
			this.Window.Show();
			GameSettings.ForcePause = true;
			GameSettings.FreezeGame = true;
		}
		foreach (Button button in this.InterviewButtons.Values)
		{
			button.GetComponent<GUIButton>().ToolTipValue = 100f.Currency(true);
			button.gameObject.SetActive(false);
		}
		foreach (GameObject gameObject in this.InterviewPanels.Values)
		{
			gameObject.SetActive(false);
		}
	}

	private void UpdateInterviewPanel(Employee emp)
	{
		Team compareTeam = this.GetCompareTeam();
		this.chart.CompareTeam = compareTeam;
		this.chart.MinSkillTeam = ((this.SelectedTeam != null) ? GameSettings.Instance.sActorManager.Teams.GetOrNull(this.SelectedTeam) : null);
		if (compareTeam != null && compareTeam.GetEmployeesDirect().Count > 0)
		{
			int i;
			for (i = 0; i < Employee.RoleCount; i++)
			{
				this.Skill[i].AltValue = new float?(compareTeam.GetEmployeesDirect().Average((Actor x) => x.employee.GetSkillI(i)));
			}
		}
		else
		{
			for (int k = 0; k < Employee.RoleCount; k++)
			{
				this.Skill[k].AltValue = null;
			}
		}
		for (int j = 0; j < Employee.RoleCount; j++)
		{
			this.Skill[j].Value = emp.GetSkillI(j);
		}
		this.Personlity.text = emp.PersonalityTraits[0].LocTry() + " - " + emp.PersonalityTraits[1].LocTry();
		this.Trait[0].Value = emp.Autodidactic.MapRange(-1f, 1f, 0f, 1f, false);
		this.Trait[1].Value = emp.Leadership.MapRange(-1f, 1f, 0f, 1f, false);
		this.Trait[2].Value = emp.Diligence.MapRange(-1f, 1f, 0f, 1f, false);
	}

	private void Update()
	{
		if (this.EmployeeList.ActualItems.Count > 0)
		{
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				if (this.EmployeeList.Selected.Count > 0)
				{
					if (this.EmployeeList.Selected[0] > 0)
					{
						int i = this.EmployeeList.Selected[0] - 1;
						this.EmployeeList.ClearSelected();
						this.EmployeeList.Select(i);
					}
				}
				else
				{
					this.EmployeeList.Select(0);
				}
				this.EmployeeList.KeepIdxInView(this.EmployeeList.Selected[0]);
			}
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				if (this.EmployeeList.Selected.Count > 0)
				{
					if (this.EmployeeList.Selected[0] < this.EmployeeList.ActualItems.Count - 1)
					{
						int i2 = this.EmployeeList.Selected[0] + 1;
						this.EmployeeList.ClearSelected();
						this.EmployeeList.Select(i2);
					}
				}
				else
				{
					this.EmployeeList.Select(0);
				}
				this.EmployeeList.KeepIdxInView(this.EmployeeList.Selected[0]);
			}
			if (Input.GetKeyUp(KeyCode.Return))
			{
				Employee[] selected = this.EmployeeList.GetSelected<Employee>();
				if (selected.Length > 0)
				{
					this.HireEmployee(selected[0], false);
				}
			}
		}
	}

	public void InterviewEmployee(string type)
	{
		Employee employee = this.EmployeeList.GetSelected<Employee>().FirstOrDefault<Employee>();
		if (employee != null && GameSettings.Instance.MyCompany.CanMakeTransaction(-100f))
		{
			UISoundFX.PlaySFX("Kaching", -1f, 0f);
			GameSettings.Instance.MyCompany.MakeTransaction(-100f, Company.TransactionCategory.Hire, null);
			this.AlreadyInterviewed[type].Add(employee);
			this.InterviewPanels[type].SetActive(true);
			this.InterviewButtons[type].gameObject.SetActive(false);
		}
	}

	private void UpdateTeamLabel()
	{
		this.TeamLabel.text = (this.SelectedTeam ?? "None".Loc());
	}

	public void ChangeTeam(string nTeam)
	{
		this.SelectedTeam = nTeam;
		this.UpdateTeamLabel();
		this.UpdateCompatibilities(this.EmployeeList.Items.OfType<Employee>());
		if (this.SelectedTeam != null && this.EmployeeList.Selected.Count > 0)
		{
			Team team = GameSettings.Instance.sActorManager.Teams[this.SelectedTeam];
			this.CompatText.text = "Compatibility".Loc() + ": " + this.GetCompatibility(team);
			this.CompatText.color = this.GetCompatColor(team);
		}
		else
		{
			this.CompatText.text = "Compatibility".Loc() + ": " + "TeamCompat0".Loc();
			this.CompatText.color = this.GetCompatColor(null);
		}
		this.UpdateInterviewPanel();
	}

	public void UpdateInterviewPanel()
	{
		Employee employee = this.EmployeeList.GetSelected<Employee>().FirstOrDefault<Employee>();
		if (employee != null)
		{
			this.UpdateInterviewPanel(employee);
		}
	}

	public void PickTeam()
	{
		HUD.Instance.TeamSelectWindow.Show(true, this.SelectedTeam, delegate(string[] ts)
		{
			this.ChangeTeam((ts.Length <= 0) ? null : ts[0]);
		}, null);
	}

	public GUIWindow LookWindow;

	public GUIWindow Window;

	public GameObject SkillInterviewPanel;

	public GameObject PersonInterviewPanel;

	public GameObject SpecInterviewPanel;

	public GameObject SpecLabel;

	public GUIProgressBar[] Skill;

	public DotBar[] Trait;

	public GUIListView EmployeeList;

	public Text Personlity;

	public Text CostText;

	public Text CompatText;

	public Text TeamLabel;

	public Text WageLabel;

	public Button SkillInterviewButton;

	public Button PersonInterviewButton;

	public Button SpecInterviewButton;

	public Slider Cost;

	public GUICombobox RoleCombo;

	public GUICombobox SpecCombo;

	public GUICombobox WageBracket;

	public Color[] CompatColors;

	public Toggle InterviewSkill;

	public Toggle InterviewSpec;

	public Toggle InterviewPerson;

	public Toggle CompareTeam;

	[NonSerialized]
	public Dictionary<string, HashSet<Employee>> AlreadyInterviewed = new Dictionary<string, HashSet<Employee>>
	{
		{
			"Person",
			new HashSet<Employee>()
		},
		{
			"Spec",
			new HashSet<Employee>()
		},
		{
			"Skill",
			new HashSet<Employee>()
		}
	};

	[NonSerialized]
	public Dictionary<string, GameObject> InterviewPanels = new Dictionary<string, GameObject>();

	[NonSerialized]
	public Dictionary<string, Button> InterviewButtons = new Dictionary<string, Button>();

	[NonSerialized]
	public Dictionary<Employee, float> Compatibility = new Dictionary<Employee, float>();

	public SpecializationChart chart;

	public string SelectedTeam;

	private bool AboutToHire;

	[NonSerialized]
	private bool lookingAgain;

	[NonSerialized]
	private int LastLookLevel;
}
