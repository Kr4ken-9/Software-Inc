﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CleaningAI : AI<Actor>
{
	public override void InitTree()
	{
		string name = "Spawn";
		if (CleaningAI.<>f__mg$cache0 == null)
		{
			CleaningAI.<>f__mg$cache0 = new Func<Actor, int>(CleaningAI.Spawn);
		}
		BehaviorNode<Actor> behaviorNode = new BehaviorNode<Actor>(name, CleaningAI.<>f__mg$cache0, true, -1);
		string name2 = "GoHome";
		if (CleaningAI.<>f__mg$cache1 == null)
		{
			CleaningAI.<>f__mg$cache1 = new Func<Actor, int>(Actor.GoHomeState);
		}
		BehaviorNode<Actor> behaviorNode2 = new BehaviorNode<Actor>(name2, CleaningAI.<>f__mg$cache1, true, -1);
		string name3 = "Despawn";
		if (CleaningAI.<>f__mg$cache2 == null)
		{
			CleaningAI.<>f__mg$cache2 = new Func<Actor, int>(CleaningAI.Despawn);
		}
		BehaviorNode<Actor> behaviorNode3 = new BehaviorNode<Actor>(name3, CleaningAI.<>f__mg$cache2, true, -1);
		string name4 = "IsOff";
		if (CleaningAI.<>f__mg$cache3 == null)
		{
			CleaningAI.<>f__mg$cache3 = new Func<Actor, int>(CleaningAI.IsOff);
		}
		BehaviorNode<Actor> behaviorNode4 = new BehaviorNode<Actor>(name4, CleaningAI.<>f__mg$cache3, false, -1);
		string name5 = "FindCleanRoom";
		if (CleaningAI.<>f__mg$cache4 == null)
		{
			CleaningAI.<>f__mg$cache4 = new Func<Actor, int>(CleaningAI.FindCleanRoom);
		}
		BehaviorNode<Actor> behaviorNode5 = new BehaviorNode<Actor>(name5, CleaningAI.<>f__mg$cache4, true, -1);
		string name6 = "GotoCleanSpot";
		if (CleaningAI.<>f__mg$cache5 == null)
		{
			CleaningAI.<>f__mg$cache5 = new Func<Actor, int>(CleaningAI.GotoCleanSpot);
		}
		BehaviorNode<Actor> behaviorNode6 = new BehaviorNode<Actor>(name6, CleaningAI.<>f__mg$cache5, true, -1);
		string name7 = "Clean";
		if (CleaningAI.<>f__mg$cache6 == null)
		{
			CleaningAI.<>f__mg$cache6 = new Func<Actor, int>(CleaningAI.Clean);
		}
		BehaviorNode<Actor> behaviorNode7 = new BehaviorNode<Actor>(name7, CleaningAI.<>f__mg$cache6, true, -1);
		string name8 = "Loiter";
		if (CleaningAI.<>f__mg$cache7 == null)
		{
			CleaningAI.<>f__mg$cache7 = new Func<Actor, int>(CleaningAI.Loiter);
		}
		BehaviorNode<Actor> behaviorNode8 = new BehaviorNode<Actor>(name8, CleaningAI.<>f__mg$cache7, true, 1);
		string name9 = "FindNewSpot";
		if (CleaningAI.<>f__mg$cache8 == null)
		{
			CleaningAI.<>f__mg$cache8 = new Func<Actor, int>(CleaningAI.FindNewSpot);
		}
		BehaviorNode<Actor> behaviorNode9 = new BehaviorNode<Actor>(name9, CleaningAI.<>f__mg$cache8, false, -1);
		string name10 = "GoHomeBusStop";
		if (CleaningAI.<>f__mg$cache9 == null)
		{
			CleaningAI.<>f__mg$cache9 = new Func<Actor, int>(Actor.GoHomeBusStop);
		}
		BehaviorNode<Actor> behaviorNode10 = new BehaviorNode<Actor>(name10, CleaningAI.<>f__mg$cache9, true, -1);
		string name11 = "ShouldUseBus";
		if (CleaningAI.<>f__mg$cacheA == null)
		{
			CleaningAI.<>f__mg$cacheA = new Func<Actor, int>(Actor.ShouldUseBus);
		}
		BehaviorNode<Actor> behaviorNode11 = new BehaviorNode<Actor>(name11, CleaningAI.<>f__mg$cacheA, true, -1);
		this.BehaviorNodes.Add("Spawn", behaviorNode);
		this.BehaviorNodes.Add("GoHome", behaviorNode2);
		this.BehaviorNodes.Add("Despawn", behaviorNode3);
		this.BehaviorNodes.Add("IsOff", behaviorNode4);
		this.BehaviorNodes.Add("FindCleanRoom", behaviorNode5);
		this.BehaviorNodes.Add("GotoCleanSpot", behaviorNode6);
		this.BehaviorNodes.Add("Clean", behaviorNode7);
		this.BehaviorNodes.Add("Loiter", behaviorNode8);
		this.BehaviorNodes.Add("FindNewSpot", behaviorNode9);
		this.BehaviorNodes.Add("GoHomeBusStop", behaviorNode10);
		this.BehaviorNodes.Add("ShouldUseBus", behaviorNode11);
		behaviorNode.Success = behaviorNode5;
		behaviorNode5.Success = behaviorNode4;
		behaviorNode5.Failure = behaviorNode8;
		behaviorNode9.Success = behaviorNode6;
		behaviorNode9.Failure = behaviorNode5;
		behaviorNode6.Success = behaviorNode7;
		behaviorNode6.Failure = behaviorNode5;
		behaviorNode7.Success = behaviorNode4;
		behaviorNode4.Success = behaviorNode11;
		behaviorNode4.Failure = behaviorNode9;
		behaviorNode2.Success = behaviorNode3;
		behaviorNode2.Failure = behaviorNode10;
		behaviorNode3.Success = AI<Actor>.DummyNode;
		behaviorNode8.Success = behaviorNode4;
		behaviorNode11.Success = behaviorNode10;
		behaviorNode11.Failure = behaviorNode2;
		behaviorNode10.Success = behaviorNode3;
		behaviorNode10.Failure = behaviorNode8;
	}

	private static int Spawn(Actor self)
	{
		if (self.CurrentPath == null)
		{
			return 2;
		}
		return (!self.WalkPath()) ? 1 : 2;
	}

	private static int Despawn(Actor self)
	{
		GameSettings.Instance.StaffSalaryDue += self.GetRealSalary() / (float)GameSettings.DaysPerMonth;
		foreach (InteractionPoint interactionPoint in self.InQueue.Values)
		{
			interactionPoint.RemoveFromQueue(self);
		}
		self.InQueue.Clear();
		if (self.OnCall)
		{
			UnityEngine.Object.Destroy(self.gameObject);
		}
		else
		{
			SDateTime sdateTime = SDateTime.Now();
			GameSettings.Instance.sActorManager.AddToAwaiting(self, new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), self.StaffOn - 1, sdateTime.Day + ((self.StaffOn <= TimeOfDay.Instance.Hour) ? 1 : 0), sdateTime.Month, sdateTime.Year), false, true);
			self.OnDespawn();
		}
		return 2;
	}

	private static int IsOff(Actor self)
	{
		if (self.GoHomeNow)
		{
			self.FreeLoiterTable();
			if (self.UsingPoint != null)
			{
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
			if (self.CleaningRoom != null)
			{
				self.CleaningRoom.Reservers--;
				self.CleaningRoom = null;
			}
			return 2;
		}
		return 0;
	}

	private static int FindCleanRoom(Actor self)
	{
		if (self.CleaningRoom != null)
		{
			self.CleaningRoom.Reservers--;
			self.CleaningRoom = null;
		}
		IEnumerable<Room> enumerable;
		if (self.HasAssignedRooms)
		{
			enumerable = from x in self.GetAssignedRooms()
			orderby x.DirtScore * (float)(1 + x.Reservers)
			select x;
		}
		else if (self.currentRoom == GameSettings.Instance.sRoomManager.Outside)
		{
			enumerable = from x in GameSettings.Instance.sRoomManager.Rooms
			orderby x.DirtScore * (float)(1 + x.Reservers)
			select x;
		}
		else
		{
			enumerable = from x in GameSettings.Instance.sRoomManager.GetConnectedRooms(self.currentRoom)
			orderby x.Value / 3, x.Key.DirtScore * (float)(1 + x.Key.Reservers)
			select x.Key;
		}
		IEnumerable<Room> enumerable2 = enumerable;
		bool flag = false;
		foreach (Room room in enumerable2)
		{
			if (!(room == GameSettings.Instance.sRoomManager.Outside) && !(room == self.currentRoom) && room.CanClean && (!GameSettings.Instance.RentMode || (room.PlayerOwned && room.Rentable)))
			{
				if (room.NavmeshRebuildStarted || room.ToiletInUse())
				{
					flag = true;
				}
				else if (!Mathf.Approximately(room.DirtScore, 1f))
				{
					self.FreeLoiterTable();
					self.UsingPoint = null;
					self.CleaningRoom = room;
					room.Reservers++;
					return 2;
				}
			}
		}
		if (!flag && self.OnCall)
		{
			self.AIScript.currentNode = self.AIScript.BehaviorNodes["ShouldUseBus"];
			return 1;
		}
		return 0;
	}

	private static int FindNewSpot(Actor self)
	{
		if (self.CleaningRoom == null)
		{
			self.CleaningPoints.Clear();
			return 0;
		}
		if (self.CleaningRoom.NavmeshRebuildStarted)
		{
			self.CleaningRoom = null;
			self.CleaningPoints.Clear();
			return 0;
		}
		bool flag = false;
		if (self.CleaningPoints.Count == 0)
		{
			flag = (self.CleaningRoom.Dirts.Count > 0);
			self.anim.SetInteger("AnimControl", 0);
			self.CleaningPoints.PushRange((from x in self.CleaningRoom.Dirts
			orderby Mathf.FloorToInt(x.Pos.x), Mathf.FloorToInt(x.Pos.y) * ((Mathf.FloorToInt(x.Pos.x) % 2 != 1) ? 1 : -1)
			select new Vector3(x.Pos.x, (float)(self.CleaningRoom.Floor * 2), x.Pos.y)).ToList<Vector3>().RandomOffset<Vector3>());
		}
		while (self.CleaningPoints.Count > 0)
		{
			Vector3 e = self.CleaningPoints.Pop();
			float dirt = self.CleaningRoom.GetDirt(e.x, e.z);
			if (dirt > 0f && self.PathToPoint(e, false))
			{
				return 2;
			}
		}
		self.CleaningRoom.RefreshDirtNavmesh();
		self.CleaningPoints.Clear();
		if (self.CleaningRoom.Dirts.Count > 0 && flag)
		{
			self.CleaningRoom.CanClean = false;
		}
		return 0;
	}

	private static int GotoCleanSpot(Actor self)
	{
		return (!self.WalkPath()) ? 1 : 2;
	}

	private static int Clean(Actor self)
	{
		self.anim.SetInteger("AnimControl", 10);
		if (self.CleaningRoom != null)
		{
			self.CleaningRoom.AddDirt(self.transform.position.x, self.transform.position.z, -Time.deltaTime * GameSettings.GameSpeed * 5f, null);
			float dirt = self.CleaningRoom.GetDirt(self.transform.position.x, self.transform.position.z);
			return (dirt != 0f) ? 1 : 2;
		}
		return 2;
	}

	private static int Loiter(Actor self)
	{
		return self.HandleLoiter(false);
	}

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache5;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache6;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache7;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache8;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache9;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheA;
}
