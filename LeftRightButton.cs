﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class LeftRightButton : Button
{
	public Button.ButtonClickedEvent onLeftClick
	{
		get
		{
			return this.m_OnLeftClick;
		}
		set
		{
			this.m_OnLeftClick = value;
		}
	}

	public Button.ButtonClickedEvent onRightClick
	{
		get
		{
			return this.m_OnRightClick;
		}
		set
		{
			this.m_OnRightClick = value;
		}
	}

	public override void OnSubmit(BaseEventData eventData)
	{
		if (this.IsActive() && this.IsInteractable())
		{
			this.m_OnLeftClick.Invoke();
		}
		if (!this.IsActive() || !this.IsInteractable())
		{
			return;
		}
		this.DoStateTransition(UnityEngine.UI.Selectable.SelectionState.Pressed, false);
		base.StartCoroutine(this.OnFinishSubmit());
	}

	private IEnumerator OnFinishSubmit()
	{
		float fadeTime = base.colors.fadeDuration;
		float elapsedTime = 0f;
		while (elapsedTime < fadeTime)
		{
			elapsedTime += Time.unscaledDeltaTime;
			yield return null;
		}
		this.DoStateTransition(base.currentSelectionState, false);
		yield break;
	}

	public override void OnPointerClick(PointerEventData eventData)
	{
		if (!this.IsActive() || !this.IsInteractable())
		{
			return;
		}
		if (eventData.button == PointerEventData.InputButton.Left)
		{
			this.m_OnLeftClick.Invoke();
		}
		if (eventData.button == PointerEventData.InputButton.Right)
		{
			this.m_OnRightClick.Invoke();
		}
	}

	[FormerlySerializedAs("onLeftClick")]
	[SerializeField]
	private Button.ButtonClickedEvent m_OnLeftClick = new Button.ButtonClickedEvent();

	[FormerlySerializedAs("onRightClick")]
	[SerializeField]
	private Button.ButtonClickedEvent m_OnRightClick = new Button.ButtonClickedEvent();
}
