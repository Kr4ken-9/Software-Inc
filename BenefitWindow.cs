﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BenefitWindow : MonoBehaviour
{
	public bool IsCompany
	{
		get
		{
			return this._targets == null;
		}
	}

	public void Show(Actor[] targets)
	{
		this._targets = targets;
		this.Benefits.SetTargets(EmployeeBenefitPanel.Style.Override, this._targets.SelectInPlace((Actor x) => x.employee.CustomBenefits));
		this.Window.Show();
	}

	public void Toggle()
	{
		if (!this.Window.Shown || this._targets != null)
		{
			this._targets = null;
			this.Benefits.SetTargets(EmployeeBenefitPanel.Style.Reset, new Dictionary<string, float>[]
			{
				GameSettings.Instance.CompanyBenefits
			});
			this.Window.Show();
			TutorialSystem.Instance.StartTutorial("Benefits", false);
		}
		else
		{
			this.Window.Close();
		}
	}

	public void OnChange()
	{
		if (this._targets == null)
		{
			foreach (Actor actor in GameSettings.Instance.sActorManager.Actors)
			{
				actor.ApplyNewBenefits();
			}
		}
		else
		{
			foreach (Actor actor2 in this._targets)
			{
				actor2.ApplyNewBenefits();
			}
		}
	}

	public GUIWindow Window;

	public EmployeeBenefitPanel Benefits;

	[NonSerialized]
	private Actor[] _targets;
}
