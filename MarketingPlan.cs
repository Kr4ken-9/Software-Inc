﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MarketingPlan : WorkItem
{
	public MarketingPlan(string name) : base(name, null, -1)
	{
	}

	public MarketingPlan()
	{
	}

	public MarketingPlan(float budget, uint targetProduct, string productName) : base(productName, null, -1)
	{
		this.Type = MarketingPlan.TaskType.PostMarket;
		this.MaxBudget = budget;
		this.Cost = MarketingPlan.PostMarketingPrice;
		this.TargetProduct = targetProduct;
		this.PreMarketing = false;
		this.CorrectName();
	}

	public MarketingPlan(SoftwareWorkItem targetItem, MarketingPlan.TaskType type, float cost, float potential, int siblingIndex) : base(targetItem.Name, null, siblingIndex)
	{
		this.Type = type;
		this.Cost = cost;
		this.MPotential = potential;
		this.TargetItem = targetItem;
		this.PreMarketing = true;
		this.CorrectName();
	}

	public uint TargetItemID
	{
		get
		{
			return this.targetItemID;
		}
	}

	public SoftwareWorkItem TargetItem
	{
		get
		{
			foreach (WorkItem workItem in GameSettings.Instance.MyCompany.WorkItems)
			{
				if (workItem.ID == this.targetItemID)
				{
					return (SoftwareWorkItem)workItem;
				}
			}
			return null;
		}
		set
		{
			this.targetItemID = value.ID;
		}
	}

	public SoftwareProduct TargetProd
	{
		get
		{
			return (this.TargetProduct != 0u) ? GameSettings.Instance.simulation.GetProduct(this.TargetProduct, false) : null;
		}
	}

	public void CorrectName()
	{
		if (this.PreMarketing)
		{
			base.Name = "MarketingProduct".Loc(new object[]
			{
				this.TargetItem.Name
			});
		}
		else
		{
			base.Name = "MarketingProduct".Loc(new object[]
			{
				this.TargetProd.Name
			});
		}
	}

	public override Color BackColor
	{
		get
		{
			return new Color(0.75f, 0f, 0f);
		}
	}

	public override string GetIcon()
	{
		return "Chart";
	}

	public override WorkItem.HasWorkReturn HasWork(Actor actor)
	{
		if (!actor.employee.IsRole(Employee.RoleBit.Marketer))
		{
			return WorkItem.HasWorkReturn.NotApplicable;
		}
		if (this.Type == MarketingPlan.TaskType.PressRelease && this.Progress >= 1f)
		{
			if (this.AutoDev)
			{
				this.StopMarketing();
			}
			return WorkItem.HasWorkReturn.Finished;
		}
		if (this.Type == MarketingPlan.TaskType.PostMarket && this.MaxBudget > 0f && this.Spent >= this.MaxBudget)
		{
			return WorkItem.HasWorkReturn.Finished;
		}
		if (this.Type == MarketingPlan.TaskType.Hype)
		{
			SoftwareWorkItem targetItem = this.TargetItem;
			if (targetItem.FollowerChange >= 0f || targetItem.Followers == 0f)
			{
				return WorkItem.HasWorkReturn.Finished;
			}
		}
		return WorkItem.HasWorkReturn.True;
	}

	public override void DoWork(Actor actor, float effectiveness, float delta)
	{
		float skill = actor.employee.GetSkill(Employee.EmployeeRole.Marketer);
		effectiveness *= skill.MapRange(0f, 1f, 0.01f, 1f, false) * actor.GetPCAddonBonus((!actor.employee.IsRole(Employee.RoleBit.Lead)) ? Employee.EmployeeRole.Marketer : Employee.EmployeeRole.Lead);
		base.RecordSkill(Employee.EmployeeRole.Marketer, skill, delta);
		this.DoSubWork(effectiveness, delta, actor.employee.IsRole(Employee.RoleBit.Lead), true);
	}

	private void DoSubWork(float effectiveness, float delta, bool leader, bool useGameTime)
	{
		float num = (!leader) ? 1f : 0.25f;
		MarketingPlan.TaskType type = this.Type;
		if (type != MarketingPlan.TaskType.PostMarket)
		{
			if (type != MarketingPlan.TaskType.Hype)
			{
				if (type == MarketingPlan.TaskType.PressRelease)
				{
					if (this.Progress < 1f)
					{
						float num2 = Utilities.PerDay(effectiveness * num, delta, useGameTime);
						float num3 = num2 * this.Cost;
						this.Spent += num3;
						this.Progress = Mathf.Clamp01(this.Progress + num2);
					}
					else if (this.AutoDev)
					{
						this.Finish();
					}
				}
			}
			else
			{
				SoftwareWorkItem targetItem = this.TargetItem;
				if (targetItem.FollowerChange < 0f)
				{
					targetItem.FollowerChange = Mathf.Lerp(targetItem.FollowerChange, 0f, Utilities.PerHour(effectiveness * num, delta, useGameTime) / (float)GameSettings.DaysPerMonth);
				}
			}
		}
		else if (this.MaxBudget <= 0f || this.Spent < this.MaxBudget)
		{
			float num4 = Utilities.PerDay(effectiveness * num, delta, useGameTime);
			float num5 = Utilities.PerDay(this.Cost, delta, useGameTime);
			this.Spent += num5;
			this.Progress += num4;
		}
	}

	public override float CompanyWork(float delta)
	{
		int num = 20;
		this.DoSubWork((float)num * base.CompanyWorker.AverageQuality, delta, false, false);
		return Utilities.PerDay(base.CompanyWorker.BusinessSavy.MapRange(0f, 1f, 4f, 2f, false) * (float)num * Employee.AverageWage * Employee.RoleSalary[4], delta, false);
	}

	public override bool CanUseCompany()
	{
		return true;
	}

	public override bool HasCompanyWork()
	{
		if (this.Type == MarketingPlan.TaskType.PressRelease && this.Progress >= 1f)
		{
			if (this.AutoDev)
			{
				this.StopMarketing();
			}
			return false;
		}
		if (this.Type == MarketingPlan.TaskType.PostMarket && this.MaxBudget > 0f && this.Spent >= this.MaxBudget)
		{
			return false;
		}
		if (this.Type == MarketingPlan.TaskType.Hype)
		{
			SoftwareWorkItem targetItem = this.TargetItem;
			if (targetItem.FollowerChange >= 0f || targetItem.Followers == 0f)
			{
				return false;
			}
		}
		return true;
	}

	public override Employee.EmployeeRole? GetBoostRole(Actor act)
	{
		if (act.employee.IsRole(Employee.RoleBit.Marketer))
		{
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Marketer);
		}
		return null;
	}

	public override int EmitType(Actor actor)
	{
		return 3;
	}

	public override string Category()
	{
		MarketingPlan.TaskType type = this.Type;
		if (type != MarketingPlan.TaskType.PostMarket)
		{
			if (type == MarketingPlan.TaskType.PressRelease)
			{
				return "Cost".Loc() + ": " + this.Spent.Currency(true);
			}
			if (type != MarketingPlan.TaskType.Hype)
			{
				return string.Empty;
			}
			SoftwareWorkItem targetItem = this.TargetItem;
			float followerChange = targetItem.FollowerChange;
			return (targetItem.Followers != 0f && followerChange < 0f) ? "FollowerChange".Loc(new object[]
			{
				(-followerChange * 24f * (float)GameSettings.DaysPerMonth / targetItem.Followers * 100f).ToString("F1")
			}) : "HypingInactiveStatus".Loc();
		}
		else
		{
			if (this.deal == 0u)
			{
				List<float> cashflow = this.TargetProd.GetCashflow();
				float num = (cashflow.Count <= 0) ? 0f : cashflow[cashflow.Count - 1];
				return string.Concat(new string[]
				{
					"MarketingBalance".Loc(),
					": ",
					(num - this.Spent).Currency(true),
					" (-",
					this.Spent.Currency(true),
					")"
				});
			}
			SoftwareProduct product = base.ActiveDeal.Product;
			if (product != null)
			{
				return string.Concat(new string[]
				{
					"Budget".Loc(),
					": ",
					this.Spent.Currency(true),
					"/",
					(Utilities.GetMarketingEffort(product.DevTime) * product.Quality * 0.05f * MarketingPlan.PostMarketingPrice / (float)GameSettings.DaysPerMonth).Currency(true)
				});
			}
			return "Budget".Loc() + ": " + this.Spent.Currency(true);
		}
	}

	public override string GetProgressLabel()
	{
		return (this.Type != MarketingPlan.TaskType.PostMarket) ? string.Empty : SoftwareType.GetAwarenessLabel(this.TargetProd.GetAwareness());
	}

	public override string CurrentStage()
	{
		MarketingPlan.TaskType type = this.Type;
		if (type == MarketingPlan.TaskType.PostMarket)
		{
			return "Marketing".Loc();
		}
		if (type == MarketingPlan.TaskType.Hype)
		{
			return "Hyping".Loc();
		}
		if (type != MarketingPlan.TaskType.PressRelease)
		{
			return string.Empty;
		}
		return "Writing press release".Loc();
	}

	public override float GetProgress()
	{
		if (this.Type == MarketingPlan.TaskType.PostMarket && this.deal > 0u)
		{
			SoftwareProduct targetProd = this.TargetProd;
			return this.Progress / (Utilities.GetMarketingEffort(targetProd.DevTime) * targetProd.Quality * 0.05f);
		}
		if (this.Type == MarketingPlan.TaskType.PostMarket && this.LastProgress >= 0f)
		{
			return (this.MaxBudget <= 0f) ? Mathf.Clamp01(this.Progress / this.LastProgress) : Mathf.Clamp01(this.Spent / this.MaxBudget);
		}
		if (this.Type == MarketingPlan.TaskType.PressRelease)
		{
			return this.Progress;
		}
		return 0f;
	}

	public void AddEffect(bool checkIfDone = true)
	{
		GameSettings.Instance.MyCompany.MakeTransaction(-this.Spent, Company.TransactionCategory.Marketing, null);
		if (this.Type == MarketingPlan.TaskType.PostMarket)
		{
			SoftwareProduct targetProd = this.TargetProd;
			targetProd.AddToMarketing(this.Progress);
			targetProd.Loss += this.Spent;
			this.Spent = 0f;
			this.LastProgress = this.Progress;
			this.Progress = 0f;
		}
		else if (this.Type == MarketingPlan.TaskType.PressRelease)
		{
			this.Finish();
			SoftwareWorkItem targetItem = this.TargetItem;
			if (targetItem != null)
			{
				targetItem.Loss += this.Spent;
			}
		}
	}

	public static float GetHypeFactor(float monthsLeft, bool releaseDate)
	{
		if (monthsLeft < 0f)
		{
			return (!releaseDate) ? 1f : 0.1f;
		}
		if (monthsLeft < 3f)
		{
			return 1f;
		}
		return Mathf.Pow(Math.Max(1f - (monthsLeft - 3f) / GameSettings.Instance.Difficulty.MapRange(0f, 2f, 32f, 16f, false), 0.4f), 2f);
	}

	public static float GetFollowerWish(float maxFollowers)
	{
		return Mathf.Min(maxFollowers, Mathf.Sqrt(maxFollowers / MarketSimulation.FollowerPopulation) * MarketSimulation.Population * 0.01f);
	}

	private void Finish()
	{
		if (this.Type == MarketingPlan.TaskType.PressRelease)
		{
			SoftwareWorkItem softwareWorkItem = this.TargetItem;
			if (softwareWorkItem is DesignDocument)
			{
				DesignDocument designDocument = (DesignDocument)softwareWorkItem;
				if (designDocument.Result != null)
				{
					softwareWorkItem = (designDocument.Result as SoftwareWorkItem);
				}
			}
			float num;
			if (softwareWorkItem is DesignDocument)
			{
				num = MarketingPlan.GetHypeFactor(Utilities.GetMonths(SDateTime.Now(), softwareWorkItem.DevStart + softwareWorkItem.DevTime), false);
			}
			else if (softwareWorkItem.ReleaseDate != null)
			{
				num = MarketingPlan.GetHypeFactor(Utilities.GetMonths(SDateTime.Now(), softwareWorkItem.ReleaseDate.Value), true);
			}
			else
			{
				num = MarketingPlan.GetHypeFactor(Utilities.GetMonths(SDateTime.Now(), softwareWorkItem.DevStart + softwareWorkItem.DevTime), false) * 0.25f;
			}
			float featureScore = GameSettings.Instance.simulation.GetFeatureScore(softwareWorkItem._type, softwareWorkItem._category, SDateTime.Now());
			float num2 = (featureScore != 0f) ? (softwareWorkItem.Type.FeatureScore(softwareWorkItem.Features) / featureScore) : 1f;
			float reputation = GameSettings.Instance.MyCompany.GetReputation(softwareWorkItem._type, softwareWorkItem._category);
			SoftwareProduct sequelTo = (softwareWorkItem.SequelTo == null) ? null : GameSettings.Instance.simulation.GetProduct(softwareWorkItem.SequelTo.Value, false);
			float number = SoftwareProduct.CalculateSequelBonus(sequelTo, softwareWorkItem.DevTime, 1f);
			float num3 = this.Progress * softwareWorkItem.PressReleaseEffect * reputation.WeightOne(0.95f) * softwareWorkItem.Innovation.WeightOne(0.75f) * number.WeightOne(0.25f) * num2 * num * this.MPotential;
			float followerWish = MarketingPlan.GetFollowerWish(softwareWorkItem.MaxFollowers);
			if (!this.AutoDev)
			{
				Newspaper.GeneratePressReleaseReview(new ArticleGenerator.PressReleaseData(GameSettings.Instance.MyCompany.Name, softwareWorkItem.SoftwareName, (!(softwareWorkItem is DesignDocument)) ? 1 : 0, softwareWorkItem.ReleaseDate, reputation, softwareWorkItem.Innovation, num2, this.Progress, this.MPotential, softwareWorkItem.PressReleaseEffect));
			}
			softwareWorkItem.Followers += num3 * followerWish * 0.25f;
			softwareWorkItem.FollowerChange += num3 * followerWish * 0.01f;
			softwareWorkItem.PressReleaseEffect = 0f;
		}
	}

	protected override void Cancelled()
	{
		base.Cancelled();
		if (base.ActiveDeal != null && this.GetProgress() == 0f)
		{
			HUD.Instance.dealWindow.CancelDeal(base.ActiveDeal, false);
			base.ActiveDeal = null;
		}
		this.AddEffect(false);
	}

	public void StopMarketing()
	{
		this.AddEffect(false);
		this.Kill(false);
	}

	public override float StressMultiplier()
	{
		return 0.25f;
	}

	public override string GetWorkTypeName()
	{
		return "Marketing";
	}

	public override void AddCost(float cost)
	{
		if (this.PreMarketing)
		{
			this.TargetItem.AddCost(cost);
		}
		else
		{
			this.TargetProd.AddLoss(cost);
		}
	}

	public float Progress;

	public float LastProgress = -1f;

	public float Cost;

	public float MPotential = 1f;

	public float MaxBudget;

	public float Spent;

	public uint TargetProduct;

	public readonly bool PreMarketing;

	public static float PostMarketingPrice = 50000f;

	private uint targetItemID;

	public MarketingPlan.TaskType Type;

	public enum TaskType
	{
		PostMarket,
		Hype,
		PressRelease
	}
}
