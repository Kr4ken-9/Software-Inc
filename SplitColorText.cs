﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SplitColorText : Text
{
	protected override void OnPopulateMesh(VertexHelper toFill)
	{
		base.OnPopulateMesh(toFill);
		int num = Mathf.Min(this.Letters, toFill.currentVertCount / 4);
		for (int i = 0; i < num; i++)
		{
			UIVertex simpleVert = UIVertex.simpleVert;
			for (int j = 0; j < 4; j++)
			{
				toFill.PopulateUIVertex(ref simpleVert, i * 4 + j);
				simpleVert.color = this.SplitColor;
				toFill.SetUIVertex(simpleVert, i * 4 + j);
			}
		}
	}

	public int Letters;

	public Color SplitColor;
}
