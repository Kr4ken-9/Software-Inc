﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

public static class NodePathFinding<T>
{
	public static int CmpFloat(float t1, float t2)
	{
		return (t1 - t2 >= 0f) ? 1 : -1;
	}

	public static List<PathNode<T>> FindPath(PathNode<T> start, PathNode<T> end, Func<T, T, float> Distance, Func<T, T, float> Heuristic, Func<object, bool> filter, T mid)
	{
		NodePathFinding<T>.openList.Clear();
		NodePathFinding<T>.closedList.Clear();
		NodePathFinding<T>.cameFrom.Clear();
		NodePathFinding<T>.cost.Clear();
		NodePathFinding<T>.ecost.Clear();
		NodePathFinding<T>.openList.Add(start);
		NodePathFinding<T>.cost[start] = 0f;
		NodePathFinding<T>.ecost.Add(NodePathFinding<T>.cost[start] + Heuristic(start.Point, end.Point), start);
		while (NodePathFinding<T>.openList.Count > 0)
		{
			PathNode<T> pathNode = NodePathFinding<T>.ecost.Pop();
			if (pathNode == end)
			{
				return NodePathFinding<T>.ReconstructPath(end);
			}
			NodePathFinding<T>.openList.Remove(pathNode);
			NodePathFinding<T>.closedList.Add(pathNode);
			foreach (PathNode<T> pathNode2 in pathNode.GetConnections())
			{
				if (!NodePathFinding<T>.closedList.Contains(pathNode2) && filter(pathNode2.Tag))
				{
					bool flag = pathNode2.NullWeight && pathNode2 != start && pathNode2 != end;
					bool flag2 = pathNode.NullWeight && pathNode != start && pathNode != end;
					float num = Heuristic(pathNode2.Point, end.Point);
					float num2 = NodePathFinding<T>.cost[pathNode] + ((!flag) ? ((!flag2) ? Distance(pathNode.Point, pathNode2.Point) : Distance(pathNode2.Point, mid)) : Distance(pathNode.Point, mid)) * pathNode2.Weight;
					if (!NodePathFinding<T>.openList.Contains(pathNode2) || num2 < NodePathFinding<T>.cost[pathNode2])
					{
						NodePathFinding<T>.cameFrom[pathNode2] = pathNode;
						NodePathFinding<T>.cost[pathNode2] = num2;
						NodePathFinding<T>.ecost.Add(NodePathFinding<T>.cost[pathNode2] + ((!flag) ? num : 10f) * pathNode2.Weight, pathNode2);
						NodePathFinding<T>.openList.Add(pathNode2);
					}
				}
			}
		}
		return null;
	}

	public static List<PathNode<T>> FindPath(PathNode<T> start, PathNode<T> end, Func<T, T, float> Distance, Func<T, T, T, float> Heuristic, Func<object, bool> filter, T mid)
	{
		NodePathFinding<T>.openList.Clear();
		NodePathFinding<T>.closedList.Clear();
		NodePathFinding<T>.cameFrom.Clear();
		NodePathFinding<T>.cost.Clear();
		NodePathFinding<T>.ecost.Clear();
		NodePathFinding<T>.openList.Add(start);
		NodePathFinding<T>.cost[start] = 0f;
		NodePathFinding<T>.ecost.Add(NodePathFinding<T>.cost[start] + Heuristic(start.Point, start.Point, end.Point), start);
		while (NodePathFinding<T>.openList.Count > 0)
		{
			PathNode<T> pathNode = NodePathFinding<T>.ecost.Pop();
			if (pathNode == end)
			{
				return NodePathFinding<T>.ReconstructPath(end);
			}
			NodePathFinding<T>.openList.Remove(pathNode);
			NodePathFinding<T>.closedList.Add(pathNode);
			foreach (PathNode<T> pathNode2 in pathNode.GetConnections())
			{
				if (!NodePathFinding<T>.closedList.Contains(pathNode2) && filter(pathNode2.Tag))
				{
					bool flag = pathNode2.NullWeight && pathNode2 != start && pathNode2 != end;
					bool flag2 = pathNode.NullWeight && pathNode != start && pathNode != end;
					float num = Heuristic(pathNode.Point, pathNode2.Point, end.Point);
					float num2 = NodePathFinding<T>.cost[pathNode] + ((!flag) ? ((!flag2) ? Distance(pathNode.Point, pathNode2.Point) : Distance(pathNode2.Point, mid)) : Distance(pathNode.Point, mid)) * pathNode2.Weight;
					if (!NodePathFinding<T>.openList.Contains(pathNode2) || num2 < NodePathFinding<T>.cost[pathNode2])
					{
						NodePathFinding<T>.cameFrom[pathNode2] = pathNode;
						NodePathFinding<T>.cost[pathNode2] = num2;
						NodePathFinding<T>.ecost.Add(NodePathFinding<T>.cost[pathNode2] + ((!flag) ? num : 10f) * pathNode2.Weight, pathNode2);
						NodePathFinding<T>.openList.Add(pathNode2);
					}
				}
			}
		}
		return null;
	}

	private static List<PathNode<T>> ReconstructPath(PathNode<T> currentNode)
	{
		List<PathNode<T>> list;
		if (NodePathFinding<T>.cameFrom.ContainsKey(currentNode))
		{
			list = NodePathFinding<T>.ReconstructPath(NodePathFinding<T>.cameFrom[currentNode]);
		}
		else
		{
			list = new List<PathNode<T>>();
		}
		list.Add(currentNode);
		return list;
	}

	static NodePathFinding()
	{
		// Note: this type is marked as 'beforefieldinit'.
		if (NodePathFinding<T>.<>f__mg$cache0 == null)
		{
			NodePathFinding<T>.<>f__mg$cache0 = new Func<float, float, int>(NodePathFinding<T>.CmpFloat);
		}
		NodePathFinding<T>.ecost = new SortedLinkedList<float, PathNode<T>>(NodePathFinding<T>.<>f__mg$cache0);
	}

	private static HashSet<PathNode<T>> openList = new HashSet<PathNode<T>>();

	private static HashSet<PathNode<T>> closedList = new HashSet<PathNode<T>>();

	private static Dictionary<PathNode<T>, PathNode<T>> cameFrom = new Dictionary<PathNode<T>, PathNode<T>>();

	private static Dictionary<PathNode<T>, float> cost = new Dictionary<PathNode<T>, float>();

	private static SortedLinkedList<float, PathNode<T>> ecost;

	[CompilerGenerated]
	private static Func<float, float, int> <>f__mg$cache0;

	public class DupKeyComp : IComparer<float>
	{
		public int Compare(float a, float b)
		{
			int num = a.CompareTo(b);
			return (num != 0) ? num : 1;
		}
	}
}
