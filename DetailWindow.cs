﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DetailWindow : MonoBehaviour
{
	public void Show(Actor emp, bool modal = false, bool interactable = true)
	{
		this._initializing = true;
		for (int i = 0; i < this.Roles.Length; i++)
		{
			this.Roles[i].isOn = emp.employee.IsRoleIndex(i);
			this.Roles[i].interactable = interactable;
		}
		this.Window.Modal = modal;
		emp.Snapshot();
		for (int j = 0; j < Actor.AffectorCount; j++)
		{
			this.Affectors[j].gameObject.SetActive(false);
		}
		this.CurrentEmployee = emp;
		string text = (this.CurrentEmployee.employee.NickName == null) ? this.CurrentEmployee.employee.Name : (this.CurrentEmployee.employee.NickName + "(" + this.CurrentEmployee.employee.Name + ")");
		this.Window.NonLocTitle = string.Concat(new object[]
		{
			text,
			" - ",
			"Age".Loc(),
			": ",
			this.CurrentEmployee.employee.AgeMonth / 12
		});
		this.Window.Show();
		this.Trait[0].Value = this.CurrentEmployee.employee.Autodidactic.MapRange(-1f, 1f, 0f, 1f, false);
		this.Trait[1].Value = this.CurrentEmployee.employee.Leadership.MapRange(-1f, 1f, 0f, 1f, false);
		this.Trait[2].Value = this.CurrentEmployee.employee.Diligence.MapRange(-1f, 1f, 0f, 1f, false);
		this.Personality.text = this.CurrentEmployee.employee.PersonalityTraits[0].LocTry() + " - " + this.CurrentEmployee.employee.PersonalityTraits[1].LocTry();
		this.SpecChart.SetContent(new Employee[]
		{
			emp.employee
		});
		this.SpecChart.MinSkillTeam = emp.GetTeam();
		this._initializing = false;
	}

	public void SetRoles()
	{
		if (!this._initializing)
		{
			int num = 0;
			for (int i = this.Roles.Length - 1; i >= 0; i--)
			{
				num <<= 1;
				if (this.Roles[i].isOn)
				{
					num |= 1;
				}
			}
			this.CurrentEmployee.ChangeRole((Employee.RoleBit)num);
		}
	}

	private void Awake()
	{
		this.Affectors = new GUIProgressBar[Actor.AffectorCount];
		for (int i = 0; i < Actor.AffectorCount; i++)
		{
			Actor.Affector affector = (Actor.Affector)i;
			GUIProgressBar guiprogressBar = this.CreateAffectBar(this.AffectPanel.transform);
			Text componentInChildren = guiprogressBar.GetComponentInChildren<Text>();
			componentInChildren.text = affector.ToString().Loc();
			guiprogressBar.gameObject.SetActive(false);
			this.Affectors[i] = guiprogressBar;
		}
	}

	private GUIProgressBar CreateAffectBar(Transform parent)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.AffectBarPrefab);
		GUIProgressBar component = gameObject.GetComponent<GUIProgressBar>();
		gameObject.transform.SetParent(parent, false);
		return component;
	}

	public void ChangeName()
	{
		WindowManager.SpawnInputDialog("NameChangePrompt".Loc(), "NameChangeTitle".Loc(), this.CurrentEmployee.employee.NickName ?? string.Empty, delegate(string x)
		{
			this.CurrentEmployee.employee.NickName = ((!string.IsNullOrEmpty(x)) ? x : null);
		}, null);
	}

	private void UpdateAffect()
	{
		int num = 1;
		float num2 = 0f;
		float num3 = 0f;
		foreach (KeyValuePair<int, float> keyValuePair in from x in this.CurrentEmployee.Affactors.Select((float x, int i) => new KeyValuePair<int, float>(i, x))
		orderby Mathf.Abs(x.Value) descending
		select x)
		{
			bool flag = keyValuePair.Value > -2f;
			if (flag)
			{
				this.Affectors[keyValuePair.Key].gameObject.SetActive(true);
				this.Affectors[keyValuePair.Key].Value = keyValuePair.Value;
				this.Affectors[keyValuePair.Key].transform.SetSiblingIndex(num);
				if (keyValuePair.Value < 0f)
				{
					num2 += -keyValuePair.Value;
				}
				else
				{
					num3 += keyValuePair.Value;
				}
				num++;
			}
			else
			{
				this.Affectors[keyValuePair.Key].gameObject.SetActive(false);
			}
		}
		this.Effectiveness.Value = this.CurrentEmployee.Effectiveness - 1f;
		this.EffectBar.SetValues(Mathf.Clamp(num3 * 6f, 0f, 6f), Mathf.Clamp(num2 * 6f, 0f, 6f));
	}

	private void UpdateThoughts()
	{
		int num = 0;
		float num2 = 0f;
		float num3 = 0f;
		bool flag = false;
		foreach (Employee.ThoughtEffect thoughtEffect in from x in this.CurrentEmployee.employee.Thoughts.List
		orderby x.Effect descending
		select x)
		{
			if (this.Thoughts.Count <= num)
			{
				GUIProgressBar guiprogressBar = this.CreateAffectBar(this.ThoughtPanel.transform);
				guiprogressBar.FromCenter = false;
				guiprogressBar.OnlyUseStart = true;
				this.Thoughts.Add(guiprogressBar);
				flag = true;
			}
			GUIProgressBar guiprogressBar2 = this.Thoughts[num];
			guiprogressBar2.StartColor = ((!thoughtEffect.Mood.Negative) ? HUD.ThemeColors[0] : HUD.ThemeColors[2]);
			guiprogressBar2.gameObject.SetActive(true);
			guiprogressBar2.GetComponentInChildren<Text>().text = thoughtEffect.Thought.Loc();
			guiprogressBar2.Value = thoughtEffect.Effect;
			if (thoughtEffect.Mood.Negative)
			{
				num2 += thoughtEffect.Effect;
			}
			else
			{
				num3 += thoughtEffect.Effect;
			}
			num++;
		}
		for (int i = num; i < this.Thoughts.Count; i++)
		{
			this.Thoughts[i].gameObject.SetActive(false);
			flag = true;
		}
		this.Satisfaction.Value = this.CurrentEmployee.employee.JobSatisfaction - 1f;
		this.SatisfactionBar.SetValues(Mathf.Clamp(num3 * 6f, 0f, 3f), Mathf.Clamp(num2 * 6f, 0f, 3f));
		if (flag)
		{
			this.ThoughtScroll.verticalNormalizedPosition = Mathf.Clamp01(this.ThoughtScroll.verticalNormalizedPosition);
		}
	}

	private void Update()
	{
		for (int i = 0; i < Employee.RoleCount; i++)
		{
			this.Skill[i].Value = this.CurrentEmployee.employee.GetSkillI(i);
		}
		this.UpdateThoughts();
		this.UpdateAffect();
	}

	public GUIWindow Window;

	public Text Personality;

	public Toggle[] Roles;

	public GUIProgressBar[] Skill;

	public DotBar[] Trait;

	public SpecializationChart SpecChart;

	public GameObject AffectBarPrefab;

	[NonSerialized]
	public GUIProgressBar[] Affectors;

	public List<GUIProgressBar> Thoughts = new List<GUIProgressBar>();

	public GUIProgressBar Satisfaction;

	public GUIProgressBar Effectiveness;

	public GameObject AffectPanel;

	public GameObject ThoughtPanel;

	public PosNegBar SatisfactionBar;

	public PosNegBar EffectBar;

	public ScrollRect ThoughtScroll;

	[NonSerialized]
	public Actor CurrentEmployee;

	[NonSerialized]
	private bool _initializing;
}
