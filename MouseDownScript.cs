﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MouseDownScript : MonoBehaviour, IPointerDownHandler, IPointerClickHandler, IEventSystemHandler
{
	public void OnPointerDown(PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Left)
		{
			this.OnDown.Invoke();
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Right)
		{
			this.OnDownRight.Invoke();
		}
	}

	public UnityEvent OnDown;

	public UnityEvent OnDownRight;
}
