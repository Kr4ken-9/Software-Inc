﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DependencyLayoutGroup : MaskableGraphic
{
	protected override void OnPopulateMesh(VertexHelper h)
	{
		h.Clear();
		if (this.Deps != null && this.Deps.Count > 0)
		{
			float spacing = base.GetComponent<HorizontalLayoutGroup>().spacing;
			float num = base.rectTransform.rect.height / 2f;
			foreach (KeyValuePair<RectTransform, HashSet<RectTransform>> keyValuePair in this.Deps)
			{
				RectTransform component = keyValuePair.Key.parent.GetComponent<RectTransform>();
				float x = component.anchoredPosition.x + component.sizeDelta.x;
				float y = component.anchoredPosition.y + component.sizeDelta.y / 2f + keyValuePair.Key.anchoredPosition.y + num;
				foreach (RectTransform rectTransform in keyValuePair.Value)
				{
					RectTransform component2 = rectTransform.parent.GetComponent<RectTransform>();
					float x2 = component2.anchoredPosition.x;
					float y2 = component2.anchoredPosition.y + component2.sizeDelta.y / 2f + rectTransform.anchoredPosition.y + num;
					float num2 = Mathf.Lerp(0.2f, 0.9f, -keyValuePair.Key.anchoredPosition.y / component.sizeDelta.y);
					float x3 = Mathf.Lerp(x2 - spacing, x2, 1f - num2);
					UMLMiniGame.DrawLine(new Vector2(x, y), new Vector2(x3, y), 2f, this.color, h);
					UMLMiniGame.DrawLine(new Vector2(x3, y), new Vector2(x3, y2), 2f, this.color, h);
					UMLMiniGame.DrawArrow(new Vector2(x3, y2), new Vector2(x2, y2), h, this.color, 8f);
				}
			}
		}
	}

	public void Clear()
	{
		for (int i = 0; i < this.Containers.Count; i++)
		{
			UnityEngine.Object.Destroy(this.Containers[i]);
		}
		this.Containers.Clear();
		this.Deps.Clear();
		this.SetVerticesDirty();
	}

	public void InitContent(Dictionary<RectTransform, HashSet<RectTransform>> Dependencies)
	{
		this.Deps = Dependencies;
		List<List<RectTransform>> list = new List<List<RectTransform>>();
		List<RectTransform> list2 = (from x in Dependencies.Keys
		where !Dependencies.Any((KeyValuePair<RectTransform, HashSet<RectTransform>> y) => y.Value.Contains(x))
		select x).ToList<RectTransform>();
		HashSet<RectTransform> hashSet = new HashSet<RectTransform>(list2);
		while (list2.Count > 0)
		{
			List<RectTransform> list3 = list2.ToList<RectTransform>();
			list2.Clear();
			List<RectTransform> list4 = new List<RectTransform>();
			list.Add(list4);
			foreach (RectTransform rectTransform in list3)
			{
				list4.Add(rectTransform);
				foreach (RectTransform item in Dependencies[rectTransform])
				{
					if (hashSet.Contains(item))
					{
						foreach (List<RectTransform> list5 in list)
						{
							list5.Remove(item);
						}
					}
					hashSet.Add(item);
					list2.Add(item);
				}
			}
		}
		for (int i = 0; i < list.Count; i++)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.DependencyPanel);
			this.Containers.Add(gameObject);
			for (int j = 0; j < list[i].Count; j++)
			{
				list[i][j].SetParent(gameObject.transform, false);
			}
			gameObject.transform.SetParent(base.transform, false);
		}
		this.SetVerticesDirty();
	}

	public GameObject DependencyPanel;

	[NonSerialized]
	private Dictionary<RectTransform, HashSet<RectTransform>> Deps = new Dictionary<RectTransform, HashSet<RectTransform>>();

	[NonSerialized]
	public List<GameObject> Containers = new List<GameObject>();
}
