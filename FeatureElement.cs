﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FeatureElement : MonoBehaviour
{
	public void Init(SoftwareType t, Feature f)
	{
		this.SWType = t;
		this.SWFeat = f;
		this.Label.text = this.SWFeat.Name.SWFeat(t.Name);
		if (this.SWFeat.Research != null && this.SWFeat.PatentOwner != 0u)
		{
			Text label = this.Label;
			label.text += string.Format("({0}%)", (this.SWFeat.Royalty * 100f).ToString("N0"));
		}
	}

	public FeatureElement AddFeature(SoftwareType type, Feature feature)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.SubFeaturePrefab);
		FeatureElement component = gameObject.GetComponent<FeatureElement>();
		component.Init(type, feature);
		gameObject.transform.SetParent(this.SubFeaturePanel.transform, false);
		this.SubFeaturePanel.gameObject.SetActive(true);
		return component;
	}

	public void UpdateIP(SoftwareProduct ip)
	{
		this.ToggleBackground.color = ((ip == null || ip.Traded || !ip.Features.Contains(this.SWFeat.Name)) ? Color.white : new Color(1f, 0.6f, 0.4f));
		if (ip != null && ip.Features.Contains(this.SWFeat.Name))
		{
			this.MainToggle.isOn = true;
		}
	}

	public void UpdateAvailability(Dictionary<string, FeatureElement> others, SoftwareProduct[] OSs, Dictionary<string, SoftwareProduct> needs, string cat)
	{
		bool flag = true;
		if (!this.SWFeat.IsUnlocked(TimeOfDay.Instance.Year, cat))
		{
			this.Tipper.TooltipDescription = "FeatureUnlockWarning".Loc(new object[]
			{
				this.SWFeat.GetUnlock(cat)
			});
			flag = false;
		}
		if (flag)
		{
			for (Feature feature = this.SWFeat.InverseFromLookup(this.SWType, cat); feature != null; feature = feature.InverseFromLookup(this.SWType, cat))
			{
				if (others[feature.Name].MainToggle.isOn)
				{
					flag = false;
					this.Tipper.TooltipDescription = "FeatureFromWarning".Loc(new object[]
					{
						feature.Name.SWFeat(this.SWType.Name)
					});
					break;
				}
			}
		}
		if (flag)
		{
			using (IEnumerator<KeyValuePair<string, string>> enumerator = (from x in this.SWFeat.Dependencies
			orderby (!x.Key.Equals(this.SWType.Name)) ? 0 : 1
			select x).GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					KeyValuePair<string, string> dep = enumerator.Current;
					Feature feature2 = GameSettings.Instance.SoftwareTypes[dep.Key].Features[dep.Value];
					if (!feature2.IsUnlocked(null, TimeOfDay.Instance.Year, GameSettings.Instance.SoftwareTypes))
					{
						flag = false;
						this.Tipper.TooltipDescription = string.Format("FeatureDependencyUnlockWarning".Loc(), dep.Value.SWFeat(dep.Key), dep.Key.LocSW());
						break;
					}
					if (dep.Key.Equals(this.SWType.Name))
					{
						bool flag2 = false;
						foreach (FeatureElement featureElement in from x in this.SWType.FeatureUpgrades[dep.Value]
						where this.SWType.Features[x].IsCompatible(cat)
						select others[x])
						{
							if (featureElement.MainToggle.isOn)
							{
								flag2 = true;
								break;
							}
						}
						if (!flag2)
						{
							flag = false;
							this.Tipper.TooltipDescription = "FeatureDependencyWarning".Loc(new object[]
							{
								dep.Value.SWFeat(dep.Key)
							});
							break;
						}
					}
					else
					{
						if (dep.Key.Equals("Operating System") && !OSs.All((SoftwareProduct x) => SoftwareType.DependencyMet(dep.Value, x)))
						{
							flag = false;
							this.Tipper.TooltipDescription = "FeatureDependencyOSWarning".Loc(new object[]
							{
								dep.Value.SWFeat(dep.Key)
							});
							break;
						}
						if (this.SWType.OSLimit != null && dep.Key.Equals("Operating System"))
						{
							if (!(from x in GameSettings.Instance.simulation.GetProductsWithMock()
							where x._type.Equals(dep.Key) && x._category.Equals(this.SWType.OSLimit)
							select x).Any((SoftwareProduct x) => SoftwareType.DependencyMet(dep.Value, x)))
							{
								flag = false;
								this.Tipper.TooltipDescription = "FeatureDependencyMarketWarning".Loc(new object[]
								{
									dep.Value.SWFeat(dep.Key),
									dep.Key.LocSW()
								});
								break;
							}
						}
						else if (!(from x in GameSettings.Instance.simulation.GetProductsWithMock()
						where x._type.Equals(dep.Key)
						select x).Any((SoftwareProduct x) => SoftwareType.DependencyMet(dep.Value, x)))
						{
							flag = false;
							this.Tipper.TooltipDescription = "FeatureDependencyMarketWarning".Loc(new object[]
							{
								dep.Value.SWFeat(dep.Key),
								dep.Key.LocSW()
							});
							break;
						}
						if (needs.ContainsKey(dep.Key) && !SoftwareType.DependencyMet(dep.Value, needs[dep.Key]))
						{
							flag = false;
							this.Tipper.TooltipDescription = "FeatureDependencyPickedWarning".Loc(new object[]
							{
								dep.Value.SWFeat(dep.Key),
								dep.Key.LocSW()
							});
							break;
						}
					}
				}
			}
		}
		if (!flag && this.MainToggle.isOn)
		{
			this.MainToggle.isOn = false;
		}
		if (flag)
		{
			string[] feature3 = Localization.GetFeature(this.SWType, this.SWFeat.Name);
			this.Tipper.TooltipDescription = feature3[1];
			if (this.SWFeat.PatentOwner > 0u && this.SWFeat.PatentOwner != GameSettings.Instance.MyCompany.ID)
			{
				GUIToolTipper tipper = this.Tipper;
				string tooltipDescription = tipper.TooltipDescription;
				tipper.TooltipDescription = string.Concat(new string[]
				{
					tooltipDescription,
					" (",
					(this.SWFeat.Royalty * 100f).ToString("F"),
					"% ",
					"Royalties".Loc(),
					")"
				});
			}
		}
		this.MainToggle.interactable = flag;
	}

	public GameObject SubFeaturePrefab;

	public Text Label;

	public GUIToolTipper Tipper;

	public Toggle MainToggle;

	public Image ToggleBackground;

	[NonSerialized]
	public SoftwareType SWType;

	[NonSerialized]
	public Feature SWFeat;

	[NonSerialized]
	public Dictionary<Feature, Toggle> SubFeatures = new Dictionary<Feature, Toggle>();

	public GameObject SubFeaturePanel;
}
