﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SmileyChart : MaskableGraphic, IPointerEnterHandler, IPointerExitHandler, IEventSystemHandler
{
	public static string ScoreString(float score)
	{
		return (Mathf.Round(score * 100f) / 10f).ToString("0.#");
	}

	public override Texture mainTexture
	{
		get
		{
			return this.tex;
		}
	}

	protected override void OnPopulateMesh(VertexHelper vh)
	{
		vh.Clear();
		Vector2 vector = new Vector2(-base.rectTransform.pivot.x * base.rectTransform.rect.width, (1f - base.rectTransform.pivot.y) * base.rectTransform.rect.height);
		if (this.Scores.Count > 0)
		{
			float num = 0f;
			float num2;
			if (base.rectTransform.rect.width < (float)(this.Scores.Count * 32))
			{
				num2 = ((float)this.Scores.Count * base.rectTransform.rect.width + base.rectTransform.rect.width - (float)(32 * this.Scores.Count)) / (float)(this.Scores.Count * this.Scores.Count);
			}
			else
			{
				num2 = base.rectTransform.rect.width / (float)this.Scores.Count;
				num = num2 / 2f - 16f;
			}
			for (int i = this.Scores.Count - 1; i > -1; i--)
			{
				float num3 = (float)i / (float)Mathf.Max(1, this.Scores.Count - 1);
				num3 *= 0.9f;
				num3 = Mathf.Max(0f, this.size - num3) / (1f - num3);
				this.DrawSmiley(new Rect(vector.x + (float)i * num2 + num + 16f * (1f - num3), vector.y - 32f + 16f * (1f - num3), 32f * num3, 32f * num3), this.Scores[i].Score, vh);
			}
		}
	}

	private void DrawSmiley(Rect r, float score, VertexHelper vh)
	{
		int num = Mathf.Clamp(Mathf.FloorToInt(score * (float)this.Smileys), 0, this.Smileys - 1);
		int num2 = num % this.Dimensions;
		int num3 = this.Dimensions - Mathf.FloorToInt((float)num / (float)this.Dimensions);
		float num4 = 1f / (float)this.Dimensions;
		Color c = this.Colors.Evaluate(score);
		vh.AddUIVertexQuad(new UIVertex[]
		{
			new UIVertex
			{
				color = c,
				position = new Vector3(r.xMin, r.yMin, 0f),
				uv0 = new Vector2((float)num2 * num4, (float)num3 * num4 - num4)
			},
			new UIVertex
			{
				color = c,
				position = new Vector3(r.xMax, r.yMin, 0f),
				uv0 = new Vector2((float)num2 * num4 + num4, (float)num3 * num4 - num4)
			},
			new UIVertex
			{
				color = c,
				position = new Vector3(r.xMax, r.yMax, 0f),
				uv0 = new Vector2((float)num2 * num4 + num4, (float)num3 * num4)
			},
			new UIVertex
			{
				color = c,
				position = new Vector3(r.xMin, r.yMax, 0f),
				uv0 = new Vector2((float)num2 * num4, (float)num3 * num4)
			}
		});
	}

	private void UpdateTooltip(SmileyChart.SmileyData data)
	{
		Tooltip.UpdateToolTip("Score".Loc() + ": " + SmileyChart.ScoreString(data.Score) + "/10", data.ToString());
	}

	public void Update()
	{
		if (!Mathf.Approximately(this.size, 1f))
		{
			this.size = Mathf.Lerp(this.size, 1f, Time.deltaTime * 8f);
			this.SetVerticesDirty();
		}
		if (this._toolTip)
		{
			if (Tooltip.IsShowing)
			{
				Vector2 vector;
				if (RectTransformUtility.ScreenPointToLocalPointInRectangle(base.rectTransform, Input.mousePosition, null, out vector))
				{
					float num = vector.x + base.rectTransform.pivot.x * base.rectTransform.rect.width;
					if (base.rectTransform.rect.width < (float)(this.Scores.Count * 32))
					{
						if (num < 32f)
						{
							this.UpdateTooltip(this.Scores[0]);
						}
						else
						{
							float num2 = (base.rectTransform.rect.width - 32f) / (float)(this.Scores.Count - 1);
							int value = Mathf.FloorToInt((num - 32f) / num2) + 1;
							this.UpdateTooltip(this.Scores[Mathf.Clamp(value, 0, this.Scores.Count - 1)]);
						}
					}
					else
					{
						float num3 = base.rectTransform.rect.width / (float)this.Scores.Count;
						int value2 = Mathf.FloorToInt(num / num3);
						this.UpdateTooltip(this.Scores[Mathf.Clamp(value2, 0, this.Scores.Count - 1)]);
					}
				}
			}
			else
			{
				this._toolTip = false;
			}
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (this.Scores.Count > 0)
		{
			this._toolTip = true;
			Tooltip.SetToolTip("0", null, base.rectTransform);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		this._toolTip = false;
	}

	public static string[] BalanceLabels = new string[]
	{
		"BalanceAmount1",
		"BalanceAmount2",
		"BalanceAmount3",
		"BalanceAmount4",
		"BalanceAmount5"
	};

	public List<SmileyChart.SmileyData> Scores = new List<SmileyChart.SmileyData>();

	public Gradient Colors;

	public int Smileys;

	public int Dimensions;

	public float size;

	private bool _toolTip;

	public Texture tex;

	[Serializable]
	public class SmileyData
	{
		public SmileyData()
		{
		}

		public SmileyData(string name, float skill, float score, float bias)
		{
			this.Name = name;
			this.Skill = skill;
			this.Score = score;
			this.Bias = bias;
		}

		private string BiasString()
		{
			if (this.Bias < -0.1f)
			{
				return string.Format("NegativePostfixBias".Loc(), SmileyChart.BalanceLabels[(-this.Bias).MapRange(0.1f, 1f, 0f, 1f, false).Quantize(SmileyChart.BalanceLabels.Length)].Loc());
			}
			if (this.Bias > 0.1f)
			{
				return string.Format("PositivePostfixBias".Loc(), SmileyChart.BalanceLabels[this.Bias.MapRange(0.1f, 1f, 0f, 1f, false).Quantize(SmileyChart.BalanceLabels.Length)].Loc());
			}
			return "BiasNone".Loc();
		}

		public override string ToString()
		{
			return string.Concat(new string[]
			{
				this.Name,
				"\n",
				"Skill".Loc(),
				": ",
				SmileyChart.BalanceLabels[this.Skill.Quantize(SmileyChart.BalanceLabels.Length)].Loc(),
				"\n",
				"Bias".Loc(),
				": ",
				this.BiasString()
			});
		}

		public string Name;

		public float Skill;

		public float Score;

		public float Bias;
	}
}
