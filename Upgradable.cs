﻿using System;
using System.Collections;
using UnityEngine;

public class Upgradable : MonoBehaviour
{
	public string GetDescription()
	{
		return string.Concat(new object[]
		{
			"State".Loc(),
			": ",
			Mathf.Round(this.Quality * 100f),
			"%"
		});
	}

	public void PowerToggled(bool ison)
	{
		if (this.DisableObjs != null && this.DisableObjs.Length > 0)
		{
			for (int i = 0; i < this.DisableObjs.Length; i++)
			{
				this.DisableObjs[i].SetActive(ison);
			}
		}
		if (this.TheScreen != null)
		{
			if (this.OnMat == null)
			{
				this.TheScreen.GetComponent<Renderer>().material.color = ((!ison) ? Color.white : Color.blue);
			}
			else
			{
				this.TheScreen.GetComponent<Renderer>().sharedMaterial = ((!ison) ? this.OffMat : this.OnMat);
			}
		}
		if (this.ChangeColorOffSecondary || this.ChangeColorOffTertiary)
		{
			this.furn.TurnOffColor(this.ChangeColorOffSecondary && !ison, this.ChangeColorOffTertiary && !ison);
		}
	}

	private void Awake()
	{
		this.furn = base.GetComponent<Furniture>();
		this.HasAnim = (this.Anim != null);
	}

	private void Start()
	{
		if (this.furn == null || this.furn.isTemporary)
		{
			if (this.furn.isTemporary)
			{
				this.PowerToggled(true);
			}
			return;
		}
		this.server = base.GetComponent<Server>();
		if (this.CanBreak)
		{
			GameObject gameObject = this.SmokePosition ?? base.gameObject;
			this.SmokeEmitter = UnityEngine.Object.Instantiate<GameObject>(MaterialBank.Instance.SmokeParticleSystem).GetComponent<ParticleSystem>();
			this.SmokeEmitter.transform.position = gameObject.transform.position;
			this.SmokeEmitter.transform.rotation = gameObject.transform.rotation;
			this.SmokeEmitter.transform.parent = base.transform;
			this.smokeRender = this.SmokeEmitter.GetComponent<Renderer>();
		}
		this.PowerToggled(this.furn.IsOn);
		this.LastUpdate = TimeOfDay.GetDateLocked();
		if (!this.furn.Deserialized)
		{
			this.LastRepair = TimeOfDay.GetDateLocked();
		}
	}

	private void Degrade()
	{
		if (this.UseAdvancedUpdate())
		{
			if (this.ModifiableAtrophy && this.AtrophyModifier == 0f)
			{
				return;
			}
			float num = Utilities.GetMonths(this.LastUpdate, SDateTime.Now()) / this.TimeToAtrophy;
			if (num > 0f)
			{
				if (this.AffectedByTemp)
				{
					float temperature = this.furn.Parent.Temperature;
					if (temperature > 15f)
					{
						num *= 1f + (temperature - 12f) / 20f;
						if (temperature > 28f && !HUD.Instance.popupManager.PopupIDDict.ContainsKey(9))
						{
							HUD.Instance.AddPopupMessage("ElectronicHeatWarning".Loc(), "Exclamation", PopupManager.PopUpAction.GotoFurn, this.furn.DID, PopupManager.NotificationSound.Issue, 0.25f, PopupManager.PopupIDs.ElectronicHeat, 1);
						}
					}
				}
				if (this.ModifiableAtrophy)
				{
					num *= this.AtrophyModifier;
				}
				this.Quality = Mathf.Max(0f, this.Quality - num * UnityEngine.Random.Range(0.25f, 2f));
			}
		}
		else
		{
			this.Quality = Mathf.Min(this.Quality, Utilities.GetMonths(this.LastRepair, SDateTime.Now()).MapRange(0f, this.TimeToAtrophy, 1f, 0f, true));
		}
	}

	public bool RepairMe()
	{
		if (!this.Broken && this.Quality >= 0.99f)
		{
			return true;
		}
		this.RepairTime += Utilities.PerHour(Mathf.Clamp(20f / this.TimeToAtrophy, 0.5f, 2f), true);
		if (this.RepairTime >= 0f)
		{
			this.RepairTime = 0f;
			this.LastRepair = SDateTime.Now();
			this.Quality = 1f;
			if (this.CanBreak)
			{
				this.SmokeEmitter.Stop();
				this.Broken = false;
			}
			if (this.furn.AlwaysOn)
			{
				this.furn.IsOn = true;
			}
			if (this.server != null)
			{
				Server.CalculatePowerNow.Add(this.server.Rep);
			}
			return true;
		}
		return false;
	}

	private bool UseAdvancedUpdate()
	{
		return this.ModifiableAtrophy || !this.DegradeAlways || this.AffectedByTemp;
	}

	public void UpdateMe()
	{
		if (this.furn.isTemporary)
		{
			return;
		}
		if (this.CanBreak)
		{
			if (this.SmokeEmitter.isPlaying && GameSettings.GameSpeed == 0f)
			{
				this.SmokeEmitter.Pause();
			}
			if (this.SmokeEmitter.isPaused && GameSettings.GameSpeed != 0f)
			{
				this.SmokeEmitter.Play();
			}
		}
		if (this.HasAnim)
		{
			IEnumerator enumerator = this.Anim.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					AnimationState animationState = (AnimationState)obj;
					animationState.speed = ((!this.furn.IsOn || this.Broken) ? 0f : GameSettings.GameSpeed);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}
		if (GameSettings.GameSpeed > 0f && !this.furn.PlacedInEditMode && (!GameSettings.Instance.RentMode || this.furn.InRentMode))
		{
			if (this.DegradeAlways || this.furn.IsOn)
			{
				this.Degrade();
			}
			if (this.UseAdvancedUpdate())
			{
				this.LastUpdate = SDateTime.Now();
			}
		}
		if (this.CanBreak)
		{
			this.smokeRender.enabled = (GameSettings.Instance.ActiveFloor == this.furn.Parent.Floor);
		}
		if (this.CanBreak && this.Quality == 0f && !this.Broken)
		{
			this.SmokeEmitter.Play();
			this.Broken = true;
			if (this.server != null)
			{
				Server.CalculatePowerNow.Add(this.server.Rep);
			}
			this.furn.IsOn = false;
			for (int i = 0; i < this.furn.InteractionPoints.Length; i++)
			{
				this.furn.InteractionPoints[i].ClearQueue();
			}
			if (this.furn.ITFix)
			{
				HUD.Instance.AddPopupMessage("ITBrokeWarning".Loc(), "Exclamation", PopupManager.PopUpAction.GotoFurn, this.furn.DID, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.Computer, 1);
				TutorialSystem.Instance.StartTutorial("Staff", false);
			}
			else
			{
				HUD.Instance.AddPopupMessage("FurnitureBrokeWarning".Loc(), "Exclamation", PopupManager.PopUpAction.GotoFurn, this.furn.DID, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.Furniture, 1);
				TutorialSystem.Instance.StartTutorial("Staff", false);
			}
		}
	}

	public void Deserialize(WriteDictionary dictionary)
	{
		this.Quality = dictionary.Get<float>("Quality", 1f);
		this.LastRepair = dictionary.Get<SDateTime>("LastRepair", TimeOfDay.GetDateLocked());
		this.RepairTime = (float)dictionary.Get<int>("RepairTime", 0);
		this.AtrophyModifier = dictionary.Get<float>("AtrophyModifier", 1f);
	}

	public void Serialize(WriteDictionary dictionary)
	{
		dictionary["Quality"] = this.Quality;
		dictionary["LastRepair"] = this.LastRepair;
		dictionary["RepairTime"] = this.RepairTime;
		dictionary["AtrophyModifier"] = this.AtrophyModifier;
	}

	public GameObject TheScreen;

	public GameObject SmokePosition;

	public ParticleSystem SmokeEmitter;

	public float Quality = 1f;

	public float RepairTime;

	public float AtrophyModifier = 1f;

	public SDateTime LastRepair;

	public string[] Descriptions;

	public GameObject[] DisableObjs;

	public float TimeToAtrophy = 12f;

	public Material OnMat;

	public Material OffMat;

	public bool DegradeAlways = true;

	public bool ModifiableAtrophy;

	public bool AffectedByTemp;

	public bool Broken;

	public bool CanBreak = true;

	public bool ChangeColorOffSecondary;

	public bool ChangeColorOffTertiary;

	public Furniture furn;

	private Server server;

	private Renderer smokeRender;

	public Animation Anim;

	[NonSerialized]
	public bool HasAnim;

	[NonSerialized]
	public SDateTime LastUpdate;
}
