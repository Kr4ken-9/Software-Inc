﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class SelectorController : MonoBehaviour
{
	private static void SellAction(global::Selectable[] xs, SelectorController y)
	{
		HintController.Instance.Show(HintController.Hints.DeleteKeyHintHint);
		WindowManager.Instance.ShowMessageBox("SellMsg".Loc(new object[]
		{
			xs.Length
		}), true, DialogWindow.DialogType.Question, delegate
		{
			List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
			HashSet<Furniture> hashSet = new HashSet<Furniture>();
			foreach (Furniture furniture in from x in (from x in xs
			where x != null
			select x).OfType<Furniture>()
			orderby x.GetSnappingDepth()
			select x)
			{
				if (!hashSet.Contains(furniture))
				{
					list.Add(new UndoObject.UndoAction(furniture, false));
					hashSet.Add(furniture);
					foreach (Furniture furniture2 in furniture.IterateSnap(null))
					{
						list.Add(new UndoObject.UndoAction(furniture2, false));
						hashSet.Add(furniture2);
					}
					y.Selected.Remove(furniture);
					UnityEngine.Object.Destroy(furniture.gameObject);
				}
			}
			if (list.Count > 0)
			{
				GameSettings.Instance.AddUndo(list.ToArray());
			}
			y.DoPostSelectChecks();
		}, "Sell furniture", null);
	}

	private static void DestroyAction(global::Selectable[] xs, SelectorController y)
	{
		HintController.Instance.Show(HintController.Hints.DeleteKeyHintHint);
		WindowManager.Instance.ShowMessageBox("BulldozeMsg".Loc(new object[]
		{
			xs.Length
		}), true, DialogWindow.DialogType.Question, delegate
		{
			bool flag = false;
			HashSet<Furniture> hashSet = new HashSet<Furniture>();
			HashSet<RoomSegment> segments = new HashSet<RoomSegment>();
			List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
			List<UndoObject.UndoAction> list2 = new List<UndoObject.UndoAction>();
			HashSet<Room> hashSet2 = (from x in xs
			where x != null
			select x).OfType<Room>().ToHashSet<Room>();
			using (IEnumerator<Room> enumerator = (from x in hashSet2
			orderby x.Floor descending
			select x).GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					Room x = enumerator.Current;
					if (GameSettings.Instance.sRoomManager.CanDestroy(x))
					{
						list2.Add(new UndoObject.UndoAction(x, false, 0f));
						List<RoomSegment> segments2 = x.GetSegments(hashSet2);
						list.AddRange(from z in segments2
						where !segments.Contains(z)
						select new UndoObject.UndoAction(z, false));
						segments.AddRange(segments2);
						hashSet.AddRange(x.GetFurnitures());
						y.Selected.Remove(x);
						UnityEngine.Object.Destroy(x.gameObject);
						GameSettings.Instance.sRoomManager.Rooms.RemoveAll((Room z) => z == x);
					}
					else
					{
						flag = true;
					}
				}
			}
			list2.Reverse();
			list2.AddRange(list);
			list2.AddRange(from z in hashSet
			orderby z.GetSnappingDepth()
			select new UndoObject.UndoAction(z, false));
			if (list2.Count > 0)
			{
				GameSettings.Instance.AddUndo(list2.ToArray());
			}
			y.DoPostSelectChecks();
			if (flag)
			{
				WindowManager.Instance.ShowMessageBox("CannotBulldozeSupport".Loc(), false, DialogWindow.DialogType.Error);
			}
		}, "Bulldoze buildings", null);
	}

	private static void DismantleAction(global::Selectable[] xs, SelectorController y)
	{
		HintController.Instance.Show(HintController.Hints.DeleteKeyHintHint);
		WindowManager.Instance.ShowMessageBox("DismantleMsg".Loc(new object[]
		{
			xs.Length
		}), true, DialogWindow.DialogType.Question, delegate
		{
			List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
			foreach (RoomSegment roomSegment in from x in xs
			where x != null
			select x.GetComponent<RoomSegment>() into x
			where x != null
			select x)
			{
				list.Add(new UndoObject.UndoAction(roomSegment, false));
				y.Selected.Remove(roomSegment);
				UnityEngine.Object.Destroy(roomSegment.gameObject);
			}
			if (list.Count > 0)
			{
				GameSettings.Instance.AddUndo(list.ToArray());
			}
			y.DoPostSelectChecks();
		}, "Dismantle segments", null);
	}

	private static void ChangeTeamAction(global::Selectable[] xs, SelectorController y)
	{
		List<Actor> acts = xs.OfType<Actor>().ToList<Actor>();
		HashSet<string> hashSet = (from x in acts
		where x.GetTeam() != null
		select x.Team).ToHashSet<string>();
		HUD.Instance.TeamSelectWindow.Show(true, (hashSet.Count != 1) ? null : hashSet.First<string>(), delegate(string[] x)
		{
			foreach (Actor actor in from z in acts
			where z != null
			select z)
			{
				actor.Team = ((x.Length != 0) ? x[0] : null);
			}
		}, null);
	}

	private static void ChangeRoomTeamAction(global::Selectable[] xs, SelectorController y)
	{
		List<Room> rooms = xs.OfType<Room>().ToList<Room>();
		HashSet<string> selected = (from x in rooms.SelectMany((Room x) => x.Teams)
		select x.Name).ToHashSet<string>();
		HUD.Instance.TeamSelectWindow.Show(false, selected, delegate(string[] x)
		{
			List<Team> newTeam = x.SelectNotNull((string z) => GameSettings.Instance.sActorManager.Teams.GetOrNull(z)).ToList<Team>();
			foreach (Room room in from z in rooms
			where z != null
			select z)
			{
				room.UpdateTeams(newTeam);
			}
		}, null);
	}

	private static void GroupRooms(global::Selectable[] xs, SelectorController y)
	{
		List<Room> rooms = xs.OfType<Room>().ToList<Room>();
		if (rooms.Count > 0)
		{
			List<string> groups = GameSettings.Instance.GetRoomGroups().ToList<string>();
			if (groups.Count == 0)
			{
				WindowManager.Instance.ShowMessageBox("NoRoomGroupPrompt".Loc(), true, DialogWindow.DialogType.Question, delegate
				{
					HUD.Instance.roomGroupWindow.Show(rooms);
				}, null, null);
			}
			else
			{
				y.selectWindow.Show("Room groups", groups, delegate(int i)
				{
					RoomGroup roomGroup = (i >= 0) ? GameSettings.Instance.GetRoomGroup(groups[i]) : null;
					foreach (Room room in rooms)
					{
						GameSettings.Instance.RemoveRoomFromGroups(room);
						if (roomGroup == null)
						{
							room.RoomGroup = null;
						}
						else
						{
							roomGroup.AddRoom(room);
						}
					}
				}, true, true, true, false, null);
			}
		}
	}

	private static void LimitUseAction(global::Selectable[] xs, SelectorController y)
	{
		y.selectWindow.Show("Usage", (from Room.RoomLimits x in Enum.GetValues(typeof(Room.RoomLimits))
		orderby (int)x
		select x.ToString()).ToArray<string>(), delegate(int z)
		{
			foreach (Room room in (from x in xs
			where x != null
			select x).OfType<Room>())
			{
				room.ChangeRole(z - 4);
			}
		}, false, true, true, true, null);
	}

	private static void SelectBuildingAction(global::Selectable[] xs, SelectorController y)
	{
		foreach (global::Selectable selectable in xs)
		{
			selectable.Highlight(false, false);
			y.Selected.Remove(selectable);
		}
		foreach (Room room in xs.OfType<Room>())
		{
			if (!y.Selected.Contains(room))
			{
				y.Selected.AddRange(GameSettings.Instance.sRoomManager.GetConnected(room, false, true).Cast<global::Selectable>());
			}
		}
		y.DoPostSelectChecks();
	}

	private static void SelectBuildingFloorAction(global::Selectable[] xs, SelectorController y)
	{
		foreach (global::Selectable selectable in xs)
		{
			selectable.Highlight(false, false);
			y.Selected.Remove(selectable);
		}
		foreach (Room room in xs.OfType<Room>())
		{
			if (!y.Selected.Contains(room))
			{
				y.Selected.AddRange(GameSettings.Instance.sRoomManager.GetConnected(room, true, true).Cast<global::Selectable>());
			}
		}
		y.DoPostSelectChecks();
	}

	private static void ChangeRoleAction(global::Selectable[] xs, SelectorController y)
	{
		EmployeeWindow.ChangeRolesNow((from x in xs
		where x != null
		select x.GetComponent<Actor>()).ToList<Actor>());
	}

	private static void SendHomeAction(global::Selectable[] xs, SelectorController y)
	{
		foreach (Actor actor in from x in xs
		select x.GetComponent<Actor>() into x
		where x != null
		select x)
		{
			if (actor.isActiveAndEnabled)
			{
				actor.GoHomeNow = true;
			}
		}
	}

	public static void FurnitureColor(global::Selectable[] xs, SelectorController y)
	{
		Furniture[] selected = (from x in xs
		select x.GetComponent<Furniture>() into x
		where x != null
		select x).ToArray<Furniture>();
		if (selected.Length == 0)
		{
			return;
		}
		for (int i = 0; i < selected.Length; i++)
		{
			selected[i].MeshCombine(false);
		}
		string[] array = SelectorController.FixFurnitureTranslation(selected);
		List<Color> list = (from x in selected
		where x.ColorPrimaryEnabled
		select x).SelectMany((Furniture x) => new Color[]
		{
			x.ColorPrimaryDefault,
			x.ColorPrimary
		}).Distinct<Color>().ToList<Color>();
		list.AddRange((from x in selected
		where x.ColorSecondaryEnabled
		select x).SelectMany((Furniture x) => new Color[]
		{
			x.ColorSecondaryDefault,
			x.ColorSecondary
		}).Distinct<Color>());
		list.AddRange((from x in selected
		where x.ColorTertiaryEnabled
		select x).SelectMany((Furniture x) => new Color[]
		{
			x.ColorTertiaryDefault,
			x.ColorTertiary
		}).Distinct<Color>());
		List<string> list2 = new List<string>();
		list2.Add(array[0]);
		List<Action<Color>> list3 = new List<Action<Color>>
		{
			delegate(Color z)
			{
				foreach (Furniture furniture in from x in selected
				where x != null && x.ColorPrimaryEnabled
				select x)
				{
					furniture.ColorPrimary = z;
				}
			},
			delegate(Color z)
			{
				foreach (Furniture furniture in from x in selected
				where x != null && x.ColorSecondaryEnabled
				select x)
				{
					furniture.ColorSecondary = z;
				}
			},
			delegate(Color z)
			{
				foreach (Furniture furniture in from x in selected
				where x != null && x.ColorTertiaryEnabled
				select x)
				{
					furniture.ColorTertiary = z;
				}
			}
		};
		List<Color> list4 = new List<Color>
		{
			selected[0].ColorPrimary,
			selected[0].ColorSecondary,
			selected[0].ColorTertiary
		};
		if (selected.Any((Furniture x) => x.ColorSecondaryEnabled))
		{
			list2.Add(array[1]);
		}
		else
		{
			list3.RemoveAt(1);
			list4.RemoveAt(1);
		}
		if (selected.Any((Furniture x) => x.ColorTertiaryEnabled))
		{
			list2.Add(array[2]);
		}
		else
		{
			list3.RemoveAt(list3.Count - 1);
			list4.RemoveAt(list4.Count - 1);
		}
		string[] tabs = list2.ToArray();
		GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
		{
			new UndoObject.UndoAction(selected)
		});
		WindowManager.SpawnColorDialog(tabs, list3.ToArray(), list4.ToArray(), list.Distinct<Color>().ToArray<Color>(), delegate
		{
			foreach (Furniture furniture in from x in selected
			where x != null
			select x)
			{
				furniture.MeshCombine(true);
				furniture.RefreshHighlight();
			}
		});
	}

	private static void TypesInRoom(global::Selectable[] xs, SelectorController y)
	{
		HintController.Instance.Show(HintController.Hints.HintSelectFurnitureType);
		y.Selected.ForEach(delegate(global::Selectable x)
		{
			x.Highlight(false, false);
		});
		y.Selected.Clear();
		Furniture[] array = xs.SelectNotNull((global::Selectable x) => x.GetComponent<Furniture>()).ToArray<Furniture>();
		bool flag = array.Length == 0;
		HashSet<string> hashSet = (from x in array
		select x.Type).ToHashSet<string>();
		HashSet<Room> hashSet2 = (from x in array
		select x.Parent).ToHashSet<Room>();
		hashSet2.AddRange(xs.SelectNotNull((global::Selectable x) => x.GetComponent<Room>()));
		foreach (Room room in hashSet2)
		{
			List<Furniture> furnitures = room.GetFurnitures();
			for (int i = 0; i < furnitures.Count; i++)
			{
				Furniture furniture = furnitures[i];
				if (!furniture.IsSelectionRestricted())
				{
					if (flag || hashSet.Contains(furniture.Type))
					{
						y.Selected.Add(furniture.GetComponent<global::Selectable>());
					}
				}
			}
		}
		y.DoPostSelectChecks();
	}

	private static void AutoAssignComputers(List<Room> xs, bool askTeam)
	{
		if (askTeam)
		{
			List<string> teams = GameSettings.Instance.sActorManager.Teams.Keys.ToList<string>();
			if (teams.Count == 1)
			{
				List<Room> source = (from x in xs
				where x.Teams.Count == 0
				select x).ToList<Room>();
				HashSet<Furniture> comps = source.SelectMany((Room x) => from y in x.GetFurniture("Computer")
				where y.OwnedBy == null
				select y).ToHashSet<Furniture>();
				SelectorController.AutoAssignCompTeam(new HashSet<Team>
				{
					GameSettings.Instance.sActorManager.Teams[teams[0]]
				}, comps);
			}
			else
			{
				SelectorController.Instance.selectWindow.Show("Team", teams, delegate(int i)
				{
					List<Room> source3 = (from x in xs
					where x.Teams.Count == 0
					select x).ToList<Room>();
					HashSet<Furniture> comps3 = source3.SelectMany((Room x) => from y in x.GetFurniture("Computer")
					where y.OwnedBy == null
					select y).ToHashSet<Furniture>();
					SelectorController.AutoAssignCompTeam(new HashSet<Team>
					{
						GameSettings.Instance.sActorManager.Teams[teams[i]]
					}, comps3);
				}, false, true, true, false, null);
			}
		}
		else
		{
			List<Room> source2 = (from x in xs
			where x.Teams.Count > 0
			select x).ToList<Room>();
			HashSet<Furniture> comps2 = source2.SelectMany((Room x) => from y in x.GetFurniture("Computer")
			where y.OwnedBy == null
			select y).ToHashSet<Furniture>();
			SelectorController.AutoAssignCompTeam(source2.SelectMany((Room x) => x.Teams).ToHashSet<Team>(), comps2);
		}
	}

	private static void AutoAssignCompTeam(HashSet<Team> teams, HashSet<Furniture> comps)
	{
		if (comps.Count == 0)
		{
			return;
		}
		List<Actor> source = teams.SelectMany((Team x) => from y in x.GetEmployeesDirect()
		where !y.Owns.Any((Furniture z) => z.Type.Equals("Computer"))
		select y).ToList<Actor>();
		foreach (Actor actor in from x in source
		orderby x.employee.GetSkill(x.employee.GetRoleOrNatural(true, false)) descending
		select x)
		{
			if (comps.Count == 0)
			{
				break;
			}
			Actor emp1 = actor;
			Furniture furniture = comps.Where((Furniture x) => x.Parent.AllowedInRoom(emp1)).MaxInstance((Furniture x) => x.RoleBuffs[(int)emp1.employee.GetRoleOrNatural(true, false)]);
			if (furniture != null)
			{
				furniture.OwnedBy = actor;
				actor.Owns.Add(furniture);
				comps.Remove(furniture);
			}
		}
	}

	public static void MergeRooms(global::Selectable[] xs, SelectorController y)
	{
		List<Room> list = (from x in xs
		where x != null
		select x.GetComponent<Room>() into x
		where x != null
		select x).ToList<Room>();
		if (list.Count > 1)
		{
			List<Room> list2 = new List<Room>();
			List<Room> list3 = new List<Room>();
			bool outDoor = list[0].Outdoors;
			list.RemoveAll((Room x) => x.Outdoors != outDoor);
			for (int i = 0; i < list.Count; i++)
			{
				if (!list[i].TryFixEdges())
				{
					list.RemoveAt(i);
					i--;
				}
			}
			if (list.Count <= 1)
			{
				return;
			}
			list2.Add(list[0]);
			list.RemoveAt(0);
			while (list2.Count > 0)
			{
				Room room = list2.First<Room>();
				list2.Remove(room);
				list3.Add(room);
				for (int j = 0; j < list.Count; j++)
				{
					if (room.CanMerge(list[j]))
					{
						list2.Add(list[j]);
						list.RemoveAt(j);
						j--;
					}
				}
			}
			if (list.Count == 0 && list3.Count > 1)
			{
				List<UndoObject.UndoAction> list4 = new List<UndoObject.UndoAction>();
				int num = 1;
				List<global::Selectable> list5 = new List<global::Selectable>();
				while (num > 0 && list3.Count > 1)
				{
					num = 0;
					for (int k = 1; k < list3.Count; k++)
					{
						if (list3[0].CanMerge(list3[k]))
						{
							Dictionary<WallSnap, UndoObject.UndoAction> snaps = list3[0].PrepareSplit(true);
							List<Vector2> split = list3[0].MergeWith(list3[k], snaps, list4);
							list4.Add(new UndoObject.UndoAction(list3[0], list3[k], split));
							list5.Remove(list3[k]);
							num++;
							list3.RemoveAt(k);
							k--;
						}
						else
						{
							list5.Add(list3[k]);
						}
					}
				}
				list4.Reverse();
				GameSettings.Instance.AddUndo(list4.ToArray());
				y.Selected.Clear();
				y.Selected.Add(list3[0]);
				y.Selected.AddRange(list5);
				y.DoPostSelectChecks();
			}
		}
	}

	private static void DuplicateAction(global::Selectable[] xs, SelectorController y)
	{
		BuildController.Instance.ClearBuild(false, false, false, false);
		List<Furniture> list = (from x in xs
		select x.GetComponent<Furniture>() into x
		where x != null
		select x).ToList<Furniture>();
		if (list.Count > 0)
		{
			SelectorController.RecursiveRemoveChildren(list);
			Furniture furn = list.FirstOrDefault<Furniture>();
			list.RemoveAll((Furniture x) => x.Parent != furn.Parent);
			if (list.Count > 1)
			{
				Vector3 pos = new Vector3(list.Average((Furniture x) => x.OriginalPosition.x), list[0].OriginalPosition.y, list.Average((Furniture x) => x.OriginalPosition.z));
				list = (from x in list
				orderby (!x.WallFurn) ? 0 : 1, (x.OriginalPosition - pos).magnitude
				select x).ToList<Furniture>();
				furn = list[0];
			}
			else
			{
				HintController.Instance.Show(HintController.Hints.HintFurnitureCopyMultiple);
			}
			list.Remove(furn);
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(BuildController.Instance.FurnitureBuilderPrefab);
			FurnitureBuilder component = gameObject.GetComponent<FurnitureBuilder>();
			BuildController.Instance.CurrentFurnitureBuilder = gameObject;
			component.FurnPrefab = furn.gameObject;
			component.IsProto = true;
			component.CopyProto = true;
			foreach (Furniture furniture in list)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(BuildController.Instance.FurnitureBuilderPrefab);
				FurnitureBuilder component2 = gameObject2.GetComponent<FurnitureBuilder>();
				component2.FurnPrefab = furniture.gameObject;
				component2.IsProto = true;
				component2.CopyProto = true;
				component2.Parent = component;
				component.Children.Add(component2);
			}
		}
	}

	private static void MaterialAction(global::Selectable[] xs, SelectorController y)
	{
		List<Room> list = (from x in xs
		select x.GetComponent<Room>() into x
		where x != null
		select x).ToList<Room>();
		if (list.Count > 0)
		{
			HUD.Instance.textureWindow.Show(list);
		}
	}

	private static void EducateAction(global::Selectable[] xs, SelectorController y)
	{
		HUD.Instance.educationWindow.Show(xs.OfType<Actor>());
	}

	private static void DismissAction(global::Selectable[] xs, SelectorController y)
	{
		HintController.Instance.Show(HintController.Hints.DeleteKeyHintHint);
		WindowManager.Instance.ShowMessageBox("DismissMsg".Loc(new object[]
		{
			xs.Length
		}), true, DialogWindow.DialogType.Question, delegate
		{
			foreach (Actor actor in from x in xs
			where x != null
			select x.GetComponent<Actor>() into x
			where x != null
			select x)
			{
				actor.Fire();
			}
		}, "Fire employees", null);
	}

	private static void MoveAction(global::Selectable[] xs, SelectorController y)
	{
		BuildController.Instance.ClearBuild(false, false, false, false);
		List<Furniture> list = (from x in xs
		where x != null && x.gameObject != null
		select x.GetComponent<Furniture>() into x
		where x != null
		select x).ToList<Furniture>();
		SelectorController.RecursiveRemoveChildren(list);
		Furniture furn = list.FirstOrDefault<Furniture>();
		if (furn != null)
		{
			list.RemoveAll((Furniture x) => x.Parent != furn.Parent);
			if (list.Count > 1)
			{
				Vector3 pos = new Vector3(list.Average((Furniture x) => x.OriginalPosition.x), list[0].OriginalPosition.y, list.Average((Furniture x) => x.OriginalPosition.z));
				list = (from x in list
				orderby (!x.WallFurn) ? 0 : 1, (x.OriginalPosition - pos).magnitude
				select x).ToList<Furniture>();
				furn = list[0];
			}
			list.Remove(furn);
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(BuildController.Instance.FurnitureBuilderPrefab);
			BuildController.Instance.CurrentFurnitureBuilder = gameObject;
			FurnitureBuilder component = gameObject.GetComponent<FurnitureBuilder>();
			component.FurnPrefab = furn.gameObject;
			component.IsProto = true;
			foreach (Furniture furniture in list)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(BuildController.Instance.FurnitureBuilderPrefab);
				FurnitureBuilder component2 = gameObject2.GetComponent<FurnitureBuilder>();
				component2.FurnPrefab = furniture.gameObject;
				component2.IsProto = true;
				component2.Parent = component;
				component.Children.Add(component2);
			}
		}
	}

	private static void RoomColorAction(global::Selectable[] xs, SelectorController y)
	{
		Room[] rooms = (from x in xs
		select x.GetComponent<Room>() into x
		where x != null
		select x).ToArray<Room>();
		if (rooms.Length > 0)
		{
			bool flag = !GameSettings.Instance.EditMode && GameSettings.Instance.RentMode;
			Color[] array;
			if (flag)
			{
				array = rooms.SelectMany((Room x) => new Color[]
				{
					x.InsideColor,
					x.FloorColor
				}).Distinct<Color>().ToArray<Color>();
			}
			else
			{
				array = rooms.SelectMany((Room x) => new Color[]
				{
					x.InsideColor,
					x.OutsideColor,
					x.FloorColor
				}).Distinct<Color>().ToArray<Color>();
			}
			Color[] defaults = array;
			string[] array3;
			if (flag)
			{
				string[] array2 = new string[2];
				array2[0] = "Interior".Loc();
				array3 = array2;
				array2[1] = "Floor".Loc();
			}
			else
			{
				string[] array4 = new string[3];
				array4[0] = "Exterior".Loc();
				array4[1] = "Interior".Loc();
				array3 = array4;
				array4[2] = "Floor".Loc();
			}
			string[] tabs = array3;
			List<Action<Color>> list = new List<Action<Color>>();
			if (!flag)
			{
				list.Add(delegate(Color color)
				{
					Room[] rooms;
					foreach (Room room in rooms)
					{
						room.OutsideColor = color;
					}
				});
			}
			list.Add(delegate(Color color)
			{
				Room[] rooms;
				foreach (Room room in rooms)
				{
					room.InsideColor = color;
				}
			});
			list.Add(delegate(Color color)
			{
				Room[] rooms;
				foreach (Room room in rooms)
				{
					room.FloorColor = color;
				}
			});
			Color[] array6;
			if (flag)
			{
				Color[] array5 = new Color[2];
				array5[0] = rooms[0].InsideColor;
				array6 = array5;
				array5[1] = rooms[0].FloorColor;
			}
			else
			{
				Color[] array7 = new Color[3];
				array7[0] = rooms[0].OutsideColor;
				array7[1] = rooms[0].InsideColor;
				array6 = array7;
				array7[2] = rooms[0].FloorColor;
			}
			Color[] startColors = array6;
			GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
			{
				new UndoObject.UndoAction(rooms.ToList<Room>(), false)
			});
			WindowManager.SpawnColorDialog(tabs, list, startColors, defaults, null);
		}
	}

	private static void ConnectServersAction(global::Selectable[] xs, SelectorController y)
	{
		List<Server> reps = (from x in xs
		select x.GetComponent<Server>() into x
		where x != null
		select x).ToList<Server>();
		if (reps.Count > 1)
		{
			HashSet<IServerItem> hashSet = reps.SelectMany((Server x) => x.items).ToHashSet<IServerItem>();
			Server server = (from x in reps
			where x.Rep == x
			select x).MaxInstance((Server x) => (float)x.Children.Count);
			server = (server ?? reps[0]);
			if (server != server.Rep)
			{
				server.CancelWire();
			}
			else
			{
				List<Server> list = (from x in server.Children
				where !reps.Contains(x)
				select x).ToList<Server>();
				if (list.Count > 0)
				{
					list[0].CancelWire();
					for (int i = 1; i < list.Count; i++)
					{
						list[i].WireTo(list[0], false);
					}
				}
			}
			for (int j = 0; j < reps.Count; j++)
			{
				if (!(reps[j] == server) && !server.Children.Contains(reps[j]))
				{
					reps[j].WireTo(server, true);
				}
			}
			foreach (IServerItem item in hashSet)
			{
				GameSettings.Instance.RegisterWithServer(server.ServerName, item, false);
			}
			Server.CalculatePowerNow.Add(server);
			HUD.Instance.serverWindow.UpdateServerList();
		}
	}

	private static void PairRoom(global::Selectable[] xs, SelectorController y)
	{
		List<Actor> staff = (from x in xs.OfType<Actor>()
		where x.AItype != AI<Actor>.AIType.Employee
		select x).ToList<Actor>();
		if (staff.Count > 0)
		{
			List<string> groups = GameSettings.Instance.GetRoomGroups().ToList<string>();
			if (groups.Count == 0)
			{
				WindowManager.Instance.ShowMessageBox("NoRoomGroupPrompt".Loc(), true, DialogWindow.DialogType.Question, delegate
				{
					HUD.Instance.roomGroupWindow.Window.Show();
				}, null, null);
			}
			else
			{
				bool[] selected = (from x in groups
				select staff.Any((Actor z) => z.AssignedRoomGroups.Contains(x))).ToArray<bool>();
				y.selectWindow.ShowMulti("Room groups", groups, selected, delegate(int[] r)
				{
					List<string> range = (from x in r
					select groups[x]).ToList<string>();
					foreach (Actor actor in staff)
					{
						actor.AssignedRoomGroups.Clear();
						actor.AssignedRoomGroups.AddRange(range);
					}
				}, true, false, false);
			}
		}
	}

	private static void SaveRoomStyle(global::Selectable[] xs, SelectorController y)
	{
		Room r = xs.OfType<Room>().FirstOrDefault<Room>();
		if (r != null)
		{
			WindowManager.SpawnInputDialog("SaveStylePrompt".Loc(), string.Empty, "Room style".Loc(), delegate(string x)
			{
				GameSettings.Instance.RoomStyles.Add(new RoomStyle(x, r));
			}, null);
		}
	}

	private static void ApplyRoomStyle(global::Selectable[] xs, SelectorController y)
	{
		List<Room> rs = xs.OfType<Room>().ToList<Room>();
		if (rs.Count > 0)
		{
			bool outdoor = rs[0].Outdoors;
			rs.RemoveAll((Room x) => x.Outdoors != outdoor);
			List<RoomStyle> styles = (from x in GameSettings.Instance.RoomStyles
			where x.OutdoorStyle == outdoor
			select x).ToList<RoomStyle>();
			y.selectWindow.Show("Room style", from x in styles
			select x.StyleName, delegate(int i)
			{
				List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>
				{
					new UndoObject.UndoAction(rs, false),
					new UndoObject.UndoAction(rs, true)
				};
				RoomStyle roomStyle = (i >= 0) ? styles[i] : ((!outdoor) ? GameSettings.Instance.DefaultIndoorRoomStyle : GameSettings.Instance.DefaultOutdoorRoomStyle);
				foreach (Room room in rs)
				{
					roomStyle.Apply(room, list);
				}
				GameSettings.Instance.AddUndo(list.ToArray());
			}, true, true, true, false, delegate(int i)
			{
				GameSettings.Instance.RoomStyles.Remove(styles[i]);
			});
		}
	}

	private static void DefaultStyleAction(global::Selectable[] xs, SelectorController y)
	{
		HintController.Instance.Show(HintController.Hints.HintCopyColor);
		foreach (global::Selectable selectable in xs)
		{
			Furniture component = selectable.GetComponent<Furniture>();
			if (component != null)
			{
				if (component.ColorPrimaryEnabled)
				{
					GameSettings.Instance.ColorDefaults[component.DefaultColorGroup + "Primary"] = component.ColorPrimary;
				}
				if (component.ColorSecondaryEnabled)
				{
					GameSettings.Instance.ColorDefaults[component.DefaultColorGroup + "Secondary"] = component.ColorSecondary;
				}
				if (component.ColorTertiaryEnabled)
				{
					GameSettings.Instance.ColorDefaults[component.DefaultColorGroup + "Tertiary"] = component.ColorTertiary;
				}
			}
		}
	}

	private static void ResetDefaultStyleAction(global::Selectable[] xs, SelectorController y)
	{
		List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
		Furniture[] array = (from x in xs
		select x.GetComponent<Furniture>() into x
		where x != null
		select x).ToArray<Furniture>();
		if (array.Length > 0)
		{
			list.Add(new UndoObject.UndoAction(array));
		}
		foreach (Furniture furniture in array)
		{
			if (furniture.ColorPrimaryEnabled)
			{
				furniture.ColorPrimary = GameSettings.Instance.GetDefaultColor(furniture.DefaultColorGroup + "Primary", furniture.ColorPrimaryDefault);
			}
			if (furniture.ColorSecondaryEnabled)
			{
				furniture.ColorSecondary = GameSettings.Instance.GetDefaultColor(furniture.DefaultColorGroup + "Secondary", furniture.ColorSecondaryDefault);
			}
			if (furniture.ColorTertiaryEnabled)
			{
				furniture.ColorTertiary = GameSettings.Instance.GetDefaultColor(furniture.DefaultColorGroup + "Tertiary", furniture.ColorTertiaryDefault);
			}
		}
		if (list.Count > 0)
		{
			GameSettings.Instance.AddUndo(list.ToArray());
		}
	}

	private static void SelectWallAction(global::Selectable[] xs, SelectorController y)
	{
		IEnumerable<RoomSegment> source = (from x in xs
		where x != null
		select x.GetComponent<RoomSegment>() into x
		where x != null
		select x).SelectMany((RoomSegment x) => x.WallPosition.First<KeyValuePair<WallEdge, float>>().Key.Children[x.WallPosition.Last<KeyValuePair<WallEdge, float>>().Key].OfType<RoomSegment>()).Distinct<RoomSegment>();
		y.Highligt(false);
		y.Selected.Clear();
		y.Selected.AddRange(source.OfType<global::Selectable>());
		y.DoPostSelectChecks();
	}

	private static void ChangeSalaryAction(global::Selectable[] xs, SelectorController y)
	{
		HUD.Instance.wageWindow.List.Items.Clear();
		HUD.Instance.wageWindow.List.Items.AddRange((from x in xs
		select x.GetComponent<Actor>() into x
		where x != null && !x.WorksForFree()
		select x).Cast<object>());
		if (HUD.Instance.wageWindow.List.Items.Count > 0)
		{
			HUD.Instance.wageWindow.Show(false);
		}
	}

	private static void SelectStaffAction(global::Selectable[] xs, SelectorController y)
	{
		foreach (Room room in xs.OfType<Room>())
		{
			room.Highlight(false, false);
			y.Selected.Remove(room);
			y.Selected.AddRange(room.Occupants.Cast<global::Selectable>());
		}
		y.DoPostSelectChecks();
	}

	private static void ReplaceFurnAction(global::Selectable[] xs, SelectorController y)
	{
		List<Furniture> list = (from x in xs
		where x != null
		select x.GetComponent<Furniture>() into x
		where x != null && !string.IsNullOrEmpty(x.UpgradeTo)
		orderby x.GetSnappingDepth() descending
		select x).ToList<Furniture>();
		if (list.Count > 0)
		{
			y.FurnReplacer.Show(list);
		}
	}

	private static void AssignParkingAction(global::Selectable[] xs, SelectorController y)
	{
		y.selectWindow.Show("ActionAssignParking", RoadNode.ParkingAssignStrings, delegate(int i)
		{
			foreach (RoadNode roadNode in from x in xs
			where x != null
			select x.GetComponent<RoadNode>() into x
			where x != null
			select x)
			{
				roadNode.Assign = (RoadNode.ParkingAssign)i;
			}
		}, false, true, true, true, null);
	}

	private static void SelectParkedPeopleAction(global::Selectable[] xs, SelectorController y)
	{
		HashSet<RoadNode> parking = (from x in xs
		where x != null
		select x.GetComponent<RoadNode>() into x
		where x != null && x.Taken
		select x).ToHashSet<RoadNode>();
		if (parking.Count > 0)
		{
			y.Highligt(false);
			y.Selected.Clear();
			y.Selected.AddRange((from x in RoadManager.Instance.Cars
			where x != null && x.Parked
			select x.GetComponent<NormalCar>() into x
			where x != null && x.GetGoal() != null && parking.Contains(x.GetGoal())
			select x).SelectMany((NormalCar x) => x.Car.GetOccupants().OfType<global::Selectable>()));
			y.DoPostSelectChecks();
		}
	}

	private static void SelectNearParkingAction(global::Selectable[] xs, SelectorController y)
	{
		List<RoadNode> list = (from x in xs
		where x != null
		select x.GetComponent<RoadNode>() into x
		where x != null
		select x).ToList<RoadNode>();
		HashSet<global::Selectable> hashSet = new HashSet<global::Selectable>();
		HashSet<RoadSegment> touch = new HashSet<RoadSegment>();
		foreach (RoadNode roadNode in list)
		{
			SelectorController.AddSurrounding(hashSet, touch, Mathf.FloorToInt(roadNode.transform.position.x / RoadManager.Instance.RoadSize), Mathf.FloorToInt(roadNode.transform.position.z / RoadManager.Instance.RoadSize));
		}
		y.Highligt(false);
		y.Selected.Clear();
		y.Selected.AddRange(hashSet);
		y.DoPostSelectChecks();
	}

	private static void AddSurrounding(HashSet<global::Selectable> park, HashSet<RoadSegment> touch, int x, int y)
	{
		RoadSegment segment = RoadManager.Instance.GetSegment(x, y);
		if (segment != null && segment.Parking.Length > 0 && !touch.Contains(segment))
		{
			touch.Add(segment);
			bool flag = false;
			for (int i = 0; i < segment.Parking.Length; i++)
			{
				RoadNode roadNode = segment.Parking[i];
				if (!roadNode.IsSelectionRestricted())
				{
					flag = true;
					park.Add(roadNode);
				}
			}
			if (flag)
			{
				SelectorController.AddSurrounding(park, touch, x - 1, y);
				SelectorController.AddSurrounding(park, touch, x + 1, y);
				SelectorController.AddSurrounding(park, touch, x, y - 1);
				SelectorController.AddSurrounding(park, touch, x, y + 1);
			}
		}
	}

	private static void DetailsAction(global::Selectable[] xs, SelectorController y)
	{
		Actor actor = xs.OfType<Actor>().FirstOrDefault<Actor>();
		if (actor != null)
		{
			HUD.Instance.DetailWindow.Show(actor, false, true);
		}
	}

	private static void SelectTeamAction(global::Selectable[] xs, SelectorController y)
	{
		foreach (Actor actor in xs.OfType<Actor>())
		{
			if (actor.GetTeam() != null)
			{
				actor.GetTeam().GetEmployees().ToList<Actor>().ForEach(delegate(Actor z)
				{
					if (!y.Selected.Contains(z.GetComponent<global::Selectable>()))
					{
						y.Selected.Add(z.GetComponent<global::Selectable>());
					}
				});
				y.DoPostSelectChecks();
			}
		}
	}

	private static void SelectOwnedAction(global::Selectable[] xs, SelectorController y)
	{
		y.Selected.Clear();
		foreach (Actor actor in xs.OfType<Actor>())
		{
			actor.Highlight(false, false);
			actor.Owns.ForEach(delegate(Furniture z)
			{
				if (!y.Selected.Contains(z))
				{
					y.Selected.Add(z);
				}
			});
		}
		y.DoPostSelectChecks();
	}

	private static void UnpairAction(global::Selectable[] xs, SelectorController y)
	{
		foreach (Furniture furniture in xs.OfType<Furniture>())
		{
			if (furniture.OwnedBy != null)
			{
				furniture.OwnedBy.Owns.Remove(furniture);
				furniture.OwnedBy = null;
			}
		}
	}

	private static void PairUse(global::Selectable[] xs, SelectorController y)
	{
		foreach (Actor actor in xs.OfType<Actor>())
		{
			Furniture furniture = (!(actor.UsingPoint == null)) ? actor.UsingPoint.Parent : null;
			if (furniture != null && furniture.CanAssign)
			{
				furniture.OwnedBy = actor;
				actor.Owns.Add(furniture);
			}
		}
	}

	private static void ToggleRentable(global::Selectable[] xs, SelectorController y)
	{
		foreach (Room room in (from x in xs.OfType<Room>()
		select x.ParentRoom ?? x).Distinct<Room>())
		{
			room.Rentable = !room.Rentable;
		}
	}

	private static void TogglePlayerOwned(global::Selectable[] xs, SelectorController y)
	{
		foreach (Room room in (from x in xs.OfType<Room>()
		select x.ParentRoom ?? x).Distinct<Room>())
		{
			room.SetPlayerOwned(!room.PlayerOwned, null);
		}
	}

	private static void GroupRentRooms(global::Selectable[] xs, SelectorController y)
	{
		List<Room> list = xs.OfType<Room>().ToList<Room>();
		if (list.Count > 0)
		{
			list[0].UnGroup();
			for (int i = 1; i < list.Count; i++)
			{
				SelectorController.GroupTo(list[0], list[i]);
			}
		}
	}

	private static void GroupTo(Room parent, Room child)
	{
		child.UnGroup();
		child.Rentable = parent.Rentable;
		child.SetPlayerOwned(parent.PlayerOwned, null);
		child.ParentRoom = parent;
		parent.ChildrenRooms.Add(child);
	}

	private static void AutoGroupRentRooms(global::Selectable[] xs, SelectorController y)
	{
		HashSet<Room> rs = xs.OfType<Room>().ToHashSet<Room>();
		Room outside = GameSettings.Instance.sRoomManager.Outside;
		rs.Remove(outside);
		PathNode<Vector3> firstPathNode = outside.GetFirstPathNode();
		Dictionary<Room, HashSet<Room>> dictionary = new Dictionary<Room, HashSet<Room>>();
		HashSet<Room> hashSet = new HashSet<Room>();
		foreach (Room room in rs)
		{
			if (room.Rentable && !hashSet.Contains(room))
			{
				PathNode<Vector3> firstPathNode2 = room.GetFirstPathNode();
				List<PathNode<Vector3>> list = NodePathFinding<Vector3>.FindPath(firstPathNode2, firstPathNode, (Vector3 x, Vector3 z) => (x - z).magnitude, (Vector3 x, Vector3 z) => (x - z).magnitude, (object x) => x == outside || !(x is Room) || rs.Contains((Room)x), (firstPathNode2.Point + firstPathNode.Point) * 0.5f);
				if (list != null)
				{
					hashSet.Add(room);
					List<Room> list2 = (from x in list
					select x.Tag).OfType<Room>().ToList<Room>();
					if (list2.Count == 2)
					{
						dictionary[room] = new HashSet<Room>();
					}
					else
					{
						int i;
						for (i = list2.Count - 2; i > 0; i--)
						{
							if (list2[i].Rentable)
							{
								break;
							}
						}
						Room room2 = list2[i];
						hashSet.Add(room2);
						HashSet<Room> hashSet2 = null;
						if (!dictionary.TryGetValue(room2, out hashSet2))
						{
							hashSet2 = new HashSet<Room>();
							dictionary[room2] = hashSet2;
						}
						for (int j = 0; j < i; j++)
						{
							Room item = list2[j];
							hashSet2.Add(item);
							hashSet.Add(item);
						}
					}
				}
			}
		}
		foreach (KeyValuePair<Room, HashSet<Room>> keyValuePair in dictionary)
		{
			keyValuePair.Key.UnGroup();
			foreach (Room child in keyValuePair.Value)
			{
				SelectorController.GroupTo(keyValuePair.Key, child);
			}
		}
	}

	public static void RecursiveRemoveChildren(List<Furniture> furns)
	{
		for (int i = 0; i < furns.Count; i++)
		{
			SnapPoint snappedTo = furns[i].SnappedTo;
			while (snappedTo != null)
			{
				if (furns.Contains(snappedTo.Parent))
				{
					furns.RemoveAt(i);
					i--;
					break;
				}
				snappedTo = snappedTo.Parent.SnappedTo;
			}
		}
	}

	private void Start()
	{
		BusScript.Present = false;
		SelectorController.Instance = this;
		GameSettings.IsQuitting = false;
		bool flag = false;
		RoomMaterialController.Clear();
		if (GameData.LoadAnyOnLoad)
		{
			GameSettings.Instance.HasToFinalizeTimers = true;
			flag = true;
			GameSettings.GameSpeed = 0f;
			HUD.Instance.ShouldShowSkipHint = false;
			TimeProbe.BeginTime("Load game time:");
			try
			{
				if (GameData.LoadFile != null && !GameData.LoadFile.BuildingOnly)
				{
					GameSettings.DaysPerMonth = GameData.LoadFile.DaysPerMonth;
				}
				else
				{
					GameSettings.DaysPerMonth = GameData.DaysPerMonth;
				}
				if (GameData.CompanyData != null)
				{
					if (GameData.LoadFile == null)
					{
						GameReader.LoadGame(GameReader.DeserializeDictionaries(GameData.CompanyData), null, GameReader.LoadMode.Company);
					}
					else
					{
						GameReader.LoadGame((!GameData.LoadBackup) ? GameData.LoadFile.FilePath : (GameData.LoadFile.FilePath + ".bak"), GameData.LoadFile, GameReader.LoadMode.Building);
						Dictionary<uint, object> deserializedObjects = Writeable.DeserializedObjects;
						Writeable.DeserializedObjects = new Dictionary<uint, object>();
						GameReader.LoadGame(GameReader.DeserializeDictionaries(GameData.CompanyData), null, GameReader.LoadMode.Company);
						uint num = deserializedObjects.Max((KeyValuePair<uint, object> x) => x.Key);
						Writeable.IDCount = num + 1u;
						List<object> list = Writeable.DeserializedObjects.Values.ToList<object>();
						Writeable.DeserializedObjects = deserializedObjects;
						foreach (object obj in list)
						{
							Writeable writeable = (Writeable)obj;
							writeable.DID = Writeable.GetNextID();
							Writeable.DeserializedObjects[writeable.DID] = obj;
						}
					}
					GameData.CompanyData = null;
				}
				else
				{
					GameReader.LoadGame((!GameData.LoadBackup) ? GameData.LoadFile.FilePath : (GameData.LoadFile.FilePath + ".bak"), GameData.LoadFile, (!GameData.LoadFile.BuildingOnly) ? GameReader.LoadMode.Full : GameReader.LoadMode.Building);
					if (!GameData.LoadFile.Readonly && (!GameData.LoadFile.BuildingOnly || GameData.EditMode))
					{
						GameSettings.Instance.AssociatedSave = GameData.LoadFile;
					}
				}
			}
			catch (Exception ex)
			{
				Exception e2 = ex;
				Exception e = e2;
				if (GameData.LoadFile != null && GameData.LoadFile.IsOlder())
				{
					WindowManager.Instance.ShowMessageBox(string.Format("OldSaveGameError".Loc(), GameData.LoadFile.GameVersion, Versioning.VersionString), false, DialogWindow.DialogType.Error);
				}
				else
				{
					ErrorLogging.LoggedErrors = 1;
					Debug.LogException(e);
					WindowManager.Instance.ShowMessageBox("LoadGameFail2".Loc(), false, DialogWindow.DialogType.Error, delegate
					{
						FeedbackWindow.Instance.Show(FeedbackWindow.ReportTypes.Exception, true, true, new string[]
						{
							Path.GetFullPath(GameData.LoadFile.FilePath)
						});
						FeedbackWindow.Instance.IncludeSave.gameObject.SetActive(false);
						FeedbackWindow.Instance.Exception = e.ToString();
					}, null, null);
				}
				ErrorLogging.FirstOfScene = false;
				ErrorLogging errorLogging = UnityEngine.Object.FindObjectOfType<ErrorLogging>();
				if (errorLogging != null)
				{
					UnityEngine.Object.DestroyImmediate(errorLogging.gameObject);
				}
			}
			HUD.Instance.serverWindow.UpdateServerList();
			HUD.Instance.roomGroupWindow.UpdateList();
			if (GameData.LoadBuildingOnLoad)
			{
				RoadManager.Instance.UpdateRoadVisibility();
			}
			GameData.LoadAnyOnLoad = false;
			TimeProbe.FinalizeTime("Load game time:");
			GameSettings.Instance.sRoomManager.Rooms.ForEach(delegate(Room x)
			{
				x.UpdateColors();
			});
			foreach (WorkItem workItem in from x in GameSettings.Instance.MyCompany.WorkItems
			orderby x.SiblingIndex
			select x)
			{
				workItem.MakeWorkItem();
			}
			if (GameSettings.Instance.Distribution != null)
			{
				GameSettings.Instance.RegisterWithServer(GameSettings.Instance.Distribution.ServerName, GameSettings.Instance.Distribution, true);
			}
			GameData.LoadYear = 0;
		}
		else
		{
			GameSettings.GameSpeed = 1f;
		}
		if (!GameSettings.Instance.EditMode)
		{
			TutorialSystem.Instance.StartTutorial("Welcome", false);
		}
		BuildController.Instance.UpdateGridVisual();
		HUD.Instance.UpdateCashflow();
		if (GameSettings.Instance.SerializedEvents != null)
		{
			HUD.Instance.insuranceWindow.Terminations.Items.AddRange(GameSettings.Instance.SerializedEvents.Cast<object>());
		}
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		GameSettings.Instance.OptimizeTrees();
		if (!flag)
		{
			RoadManager.Instance.UpdateTreeRemoval();
		}
		Debug.Log("Tree combine time: " + (Time.realtimeSinceStartup - realtimeSinceStartup));
		SelectorController.UpdateInfoPanel();
		HUD.Instance.UpdateFurnitureButtons();
		HUD.Instance.FixUpperDayPanel();
		HUD.Instance.mainReputataionBars.company = GameSettings.Instance.MyCompany;
		if (GameSettings.Instance.RentMode && !GameSettings.Instance.EditMode)
		{
			GameSettings.Instance.DirtyRentGrid.AddRange(from x in GameSettings.Instance.sRoomManager.Rooms
			where x.PlayerOwned
			select x.Floor);
		}
		this.RelocateButton.SetActive(!GameSettings.Instance.EditMode);
	}

	public void InvokeAction()
	{
		List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
		foreach (global::Selectable selectable in this.Selected.SelectNotNull((global::Selectable x) => x.PanelActionDivert()).Distinct<global::Selectable>())
		{
			selectable.InvokePanelAction(list);
		}
		if (list.Count > 0)
		{
			GameSettings.Instance.AddUndo(list.ToArray());
		}
		SelectorController.UpdateInfoPanel();
	}

	public static void UpdateInfoPanel(string text, string[] ExtraText, string[] Icons, string[] tooltips, Color[] Colors, string panelAction, string panelTip, bool special = true, bool forceRow = false)
	{
		if (SelectorController.Instance == null || GameSettings.IsQuitting)
		{
			return;
		}
		SelectorController.Instance.InfoText.text = text;
		if (panelAction == null)
		{
			SelectorController.Instance.PanelButton.SetActive(false);
		}
		else
		{
			SelectorController.Instance.PanelButton.SetActive(true);
			SelectorController.Instance.PanelButtonText.text = panelAction.Loc();
			SelectorController.Instance.PanelButtonTip.TooltipDescription = (panelTip ?? string.Empty);
		}
		if (ExtraText == null)
		{
			SelectorController.Instance.StatPanel.SetActive(false);
			SelectorController.Instance.BigPanel.SetActive(false);
		}
		else if (ExtraText.Length == 1 && !forceRow)
		{
			SelectorController.Instance.StatPanel.SetActive(false);
			SelectorController.Instance.BigPanel.SetActive(true);
			SelectorController.Instance.BigText.text = ExtraText[0];
			SelectorController.Instance.BigImage.sprite = IconManager.GetIcon(Icons[0]);
		}
		else if (ExtraText.Length < 5)
		{
			SelectorController.Instance.StatPanel.SetActive(true);
			SelectorController.Instance.BigPanel.SetActive(false);
			for (int i = 0; i < ExtraText.Length; i++)
			{
				SelectorController.Instance.StatText[i].text = ExtraText[i];
				SelectorController.Instance.StatImages[i].sprite = IconManager.GetIcon(Icons[i]);
				SelectorController.Instance.StatImages[i].color = ((Colors != null) ? Colors[i] : new Color(0.3f, 0.3f, 0.3f));
				GUIToolTipper component = SelectorController.Instance.StatText[i].transform.parent.GetComponent<GUIToolTipper>();
				component.ToolTipValue = string.Empty;
				component.TooltipDescription = string.Empty;
				if (tooltips != null)
				{
					if (tooltips[i].Length > 0 && tooltips[i][0] == '#')
					{
						int num = tooltips[i].IndexOf("\n", StringComparison.Ordinal);
						component.ToolTipValue = tooltips[i].Substring(1, num);
						component.TooltipDescription = tooltips[i].Substring(num);
					}
					else
					{
						component.ToolTipValue = tooltips[i];
					}
				}
			}
			for (int j = ExtraText.Length; j < 4; j++)
			{
				SelectorController.Instance.StatText[j].text = string.Empty;
				SelectorController.Instance.StatImages[j].color = new Color(0f, 0f, 0f, 0f);
				GUIToolTipper component2 = SelectorController.Instance.StatText[j].transform.parent.GetComponent<GUIToolTipper>();
				component2.ToolTipValue = string.Empty;
				component2.TooltipDescription = string.Empty;
			}
		}
		else if (ExtraText.Length == 5)
		{
			SelectorController.Instance.StatPanel.SetActive(true);
			SelectorController.Instance.BigPanel.SetActive(true);
			SelectorController.Instance.BigText.text = ExtraText[0];
			SelectorController.Instance.BigImage.sprite = IconManager.GetIcon(Icons[0]);
			for (int k = 0; k < 4; k++)
			{
				SelectorController.Instance.StatText[k].text = ExtraText[k + 1];
				SelectorController.Instance.StatImages[k].sprite = IconManager.GetIcon(Icons[k + 1]);
				SelectorController.Instance.StatImages[k].color = ((Colors != null) ? Colors[k] : new Color(0.3f, 0.3f, 0.3f));
				GUIToolTipper component3 = SelectorController.Instance.StatText[k].transform.parent.GetComponent<GUIToolTipper>();
				component3.ToolTipValue = string.Empty;
				component3.TooltipDescription = string.Empty;
				if (tooltips != null)
				{
					if (tooltips[k].Length > 0 && tooltips[k][0] == '#')
					{
						int num2 = tooltips[k].IndexOf("\n", StringComparison.Ordinal);
						component3.ToolTipValue = tooltips[k].Substring(1, num2);
						component3.TooltipDescription = tooltips[k].Substring(num2 + 1);
					}
					else
					{
						component3.ToolTipValue = tooltips[k];
					}
				}
			}
		}
		SelectorController.Instance.SpecialInfo = special;
	}

	public static void UpdateInfoPanel()
	{
		if (SelectorController.Instance == null || GameSettings.IsQuitting)
		{
			return;
		}
		int num = SelectorController.Instance.Selected.Count((global::Selectable x) => x != null && x.gameObject != null);
		if (num == 0)
		{
			SelectorController.UpdateInfoPanel(string.Empty, null, null, null, null, null, null, false, false);
		}
		else if (num == 1)
		{
			global::Selectable selectable = SelectorController.Instance.Selected.First((global::Selectable x) => x != null && x.gameObject != null);
			SelectorController.UpdateInfoPanel(selectable.GetInfo(), selectable.GetExtendedInfo(), selectable.GetExtendedIconInfo(), selectable.GetExtendedTooltipInfo(), selectable.GetExtendedColorInfo(), selectable.GetPanelActionName(), selectable.GetPanelActionTip(), false, false);
		}
		else if (num > 1)
		{
			Dictionary<SelectorController.SelectionTypes, int> selection = SelectorController.GetSelection();
			List<string> list = new List<string>();
			List<string> list2 = new List<string>();
			List<string> list3 = new List<string>();
			int num2 = 0;
			int count = selection.Count;
			foreach (KeyValuePair<SelectorController.SelectionTypes, int> keyValuePair in (from x in selection
			orderby (int)x.Key
			select x).ToList<KeyValuePair<SelectorController.SelectionTypes, int>>())
			{
				if (num2 == 3 && selection.Count > 1)
				{
					break;
				}
				switch (keyValuePair.Key)
				{
				case SelectorController.SelectionTypes.Employee:
					list2.Add("MoreEmployees");
					list3.Add("Employees".Loc());
					break;
				case SelectorController.SelectionTypes.Room:
					list2.Add("Structure");
					list3.Add("Rooms".Loc());
					break;
				case SelectorController.SelectionTypes.Furniture:
					list2.Add("Furniture");
					list3.Add("Furniture".Loc());
					break;
				case SelectorController.SelectionTypes.Segment:
					list2.Add("Door");
					list3.Add("WallSegments".Loc());
					break;
				case SelectorController.SelectionTypes.Parking:
					list2.Add("Road");
					list3.Add("Parking".Loc());
					break;
				}
				list.Add(keyValuePair.Value.ToString());
				selection.Remove(keyValuePair.Key);
				num2++;
			}
			if (count == 1)
			{
				global::Selectable selectable2 = SelectorController.Instance.Selected.First((global::Selectable x) => x != null && x.gameObject != null);
				string multiIcon = selectable2.GetMultiIcon();
				if (multiIcon != null)
				{
					list2.Add(multiIcon);
					list3.Add(selectable2.GetMultiDesc());
					list.Add(selectable2.GetMultiValue(from x in SelectorController.Instance.Selected
					where x != null && x.gameObject != null
					select x));
				}
			}
			if (selection.Count > 0)
			{
				list2.Add("Cogs");
				list3.Add("Various".Loc());
				list.Add(selection.Sum((KeyValuePair<SelectorController.SelectionTypes, int> x) => x.Value).ToString());
			}
			string panelAction;
			string panelTip;
			SelectorController.GetPanelAction(out panelAction, out panelTip);
			SelectorController.UpdateInfoPanel(string.Empty, list.ToArray(), list2.ToArray(), list3.ToArray(), null, panelAction, panelTip, false, true);
		}
	}

	private static void GetPanelAction(out string actionName, out string tip)
	{
		actionName = null;
		tip = null;
		foreach (global::Selectable selectable in SelectorController.Instance.Selected)
		{
			string panelActionName = selectable.GetPanelActionName();
			if (panelActionName == null)
			{
				actionName = null;
				tip = null;
				break;
			}
			if (actionName == null)
			{
				tip = selectable.GetPanelActionTip();
				actionName = panelActionName;
			}
			else if (!panelActionName.Equals(actionName))
			{
				actionName = null;
				tip = null;
				break;
			}
		}
	}

	private static Dictionary<SelectorController.SelectionTypes, int> GetSelection()
	{
		Dictionary<SelectorController.SelectionTypes, int> dictionary = new Dictionary<SelectorController.SelectionTypes, int>();
		foreach (global::Selectable selectable in from x in SelectorController.Instance.Selected
		where x != null && x.gameObject != null
		select x)
		{
			if (selectable is Actor)
			{
				dictionary.AddUp(SelectorController.SelectionTypes.Employee, 1);
			}
			else if (selectable is Room)
			{
				dictionary.AddUp(SelectorController.SelectionTypes.Room, 1);
			}
			else if (selectable is Furniture)
			{
				dictionary.AddUp(SelectorController.SelectionTypes.Furniture, 1);
			}
			else if (selectable is RoomSegment)
			{
				dictionary.AddUp(SelectorController.SelectionTypes.Segment, 1);
			}
			else if (selectable is RoadNode)
			{
				dictionary.AddUp(SelectorController.SelectionTypes.Parking, 1);
			}
		}
		return dictionary;
	}

	public float ShiftMid(float val, float mid)
	{
		if (val > mid)
		{
			return (val - mid) / (1f - mid);
		}
		return val / mid - 1f;
	}

	private void LateUpdate()
	{
		if (!this.SpecialInfo)
		{
			SelectorController.UpdateInfoPanel();
		}
		if (this.Selected.Count == 1)
		{
			global::Selectable selectable = this.Selected.First<global::Selectable>();
			Actor actor = selectable as Actor;
			if (actor != null)
			{
				if (!actor.employee.Founder && actor.AItype == AI<Actor>.AIType.Employee)
				{
					this.NeedPanel.SetActive(true);
					this.NeedBars[0].Value = this.ShiftMid(actor.employee.Energy, 0.0914397f);
					this.NeedBars[1].Value = this.ShiftMid(actor.employee.Hunger, 0.0914397f);
					this.NeedBars[2].Value = this.ShiftMid(actor.employee.Bladder, 0.0559124f);
					this.NeedBars[3].Value = actor.employee.Stress * 2f - 1f;
					this.NeedBars[4].Value = actor.employee.Social * 2f - 1f;
				}
				else
				{
					this.NeedPanel.SetActive(false);
				}
			}
			else
			{
				this.NeedPanel.SetActive(false);
			}
		}
		else
		{
			this.NeedPanel.SetActive(false);
		}
		this.overGUI = GUICheck.OverGUI;
		if (InputController.GetKeyUp(InputController.Keys.GotoEmp, false) && BuildController.Instance.CanChangeFloor())
		{
			IEnumerable<Actor> source = this.Selected.OfType<Actor>();
			if (source.Count<Actor>() == 1)
			{
				Actor actor2 = source.First<Actor>();
				if (actor2.isActiveAndEnabled)
				{
					GameSettings.Instance.ActiveFloor = actor2.Floor;
					Furniture.UpdateEdgeDetection();
					GameSettings.Instance.sRoomManager.ChangeFloor();
					CameraScript.GotoPos(new Vector2(actor2.transform.position.x, actor2.transform.position.z));
				}
			}
		}
		if (this.Selected.Count > 0 && InputController.GetKeyUp(InputController.Keys.Destroy, false))
		{
			SelectorController.<LateUpdate>c__AnonStorey22 <LateUpdate>c__AnonStorey = new SelectorController.<LateUpdate>c__AnonStorey22();
			<LateUpdate>c__AnonStorey.$this = this;
			<LateUpdate>c__AnonStorey.actors = this.Selected.OfType<Actor>().ToList<Actor>();
			<LateUpdate>c__AnonStorey.actors.RemoveAll((Actor x) => x.employee.Founder);
			SelectorController.<LateUpdate>c__AnonStorey22 <LateUpdate>c__AnonStorey2 = <LateUpdate>c__AnonStorey;
			HashSet<Room> rooms;
			if (!GameSettings.Instance.EditMode && GameSettings.Instance.RentMode)
			{
				rooms = new HashSet<Room>();
			}
			else
			{
				rooms = (from x in this.Selected.OfType<Room>()
				where x != null
				select x).ToHashSet<Room>();
			}
			<LateUpdate>c__AnonStorey2.rooms = rooms;
			<LateUpdate>c__AnonStorey.furniture = (from x in this.Selected.OfType<Furniture>()
			where x.IsPlayerControlled() && x.Parent.IsPlayerControlled()
			select x).ToList<Furniture>();
			<LateUpdate>c__AnonStorey.furniture.RemoveAll((Furniture x) => <LateUpdate>c__AnonStorey.rooms.Contains(x.Parent));
			<LateUpdate>c__AnonStorey.segments = ((GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode) ? this.Selected.OfType<RoomSegment>().ToList<RoomSegment>() : new List<RoomSegment>());
			<LateUpdate>c__AnonStorey.segments.RemoveAll((RoomSegment x) => (x.ParentRooms[0] != null && <LateUpdate>c__AnonStorey.rooms.Contains(x.ParentRooms[0])) || (x.ParentRooms[1] != null && <LateUpdate>c__AnonStorey.rooms.Contains(x.ParentRooms[1])));
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendLine("AreYouSure".Loc() + ":");
			if (<LateUpdate>c__AnonStorey.actors.Count > 0)
			{
				stringBuilder.AppendLine("DismissMsg2".Loc(new object[]
				{
					<LateUpdate>c__AnonStorey.actors.Count
				}));
			}
			if (<LateUpdate>c__AnonStorey.rooms.Count > 0)
			{
				stringBuilder.AppendLine("BulldozeMsg2".Loc(new object[]
				{
					<LateUpdate>c__AnonStorey.rooms.Count
				}));
			}
			if (<LateUpdate>c__AnonStorey.furniture.Count > 0)
			{
				stringBuilder.AppendLine("SellMsg2".Loc(new object[]
				{
					<LateUpdate>c__AnonStorey.furniture.Count
				}));
			}
			if (<LateUpdate>c__AnonStorey.segments.Count > 0)
			{
				stringBuilder.AppendLine("DismantleMsg2".Loc(new object[]
				{
					<LateUpdate>c__AnonStorey.segments.Count
				}));
			}
			if (<LateUpdate>c__AnonStorey.actors.Count + <LateUpdate>c__AnonStorey.rooms.Count + <LateUpdate>c__AnonStorey.furniture.Count + <LateUpdate>c__AnonStorey.segments.Count > 0)
			{
				WindowManager.Instance.ShowMessageBox(stringBuilder.ToString(), true, DialogWindow.DialogType.Question, delegate
				{
					BuildController.Instance.ClearBuild(false, false, false, false);
					List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
					bool flag = false;
					foreach (Actor actor3 in from x in <LateUpdate>c__AnonStorey.actors
					where x != null
					select x)
					{
						actor3.Fire();
					}
					HashSet<Furniture> hashSet = new HashSet<Furniture>();
					HashSet<RoomSegment> segmentss = new HashSet<RoomSegment>();
					List<UndoObject.UndoAction> list2 = new List<UndoObject.UndoAction>();
					using (IEnumerator<Room> enumerator2 = (from x in <LateUpdate>c__AnonStorey.rooms
					where x != null
					orderby x.Floor descending
					select x).GetEnumerator())
					{
						while (enumerator2.MoveNext())
						{
							Room x = enumerator2.Current;
							if (GameSettings.Instance.sRoomManager.CanDestroy(x))
							{
								list.Add(new UndoObject.UndoAction(x, false, 0f));
								List<RoomSegment> segments = x.GetSegments(<LateUpdate>c__AnonStorey.rooms);
								list2.AddRange(from z in segments
								where !segmentss.Contains(z)
								select new UndoObject.UndoAction(z, false));
								segmentss.AddRange(segments);
								hashSet.AddRange(x.GetFurnitures());
								<LateUpdate>c__AnonStorey.$this.Selected.Remove(x);
								UnityEngine.Object.Destroy(x.gameObject);
								GameSettings.Instance.sRoomManager.Rooms.RemoveAll((Room y) => y == x);
							}
							else
							{
								flag = true;
							}
						}
					}
					list.Reverse();
					list.AddRange(list2);
					list.AddRange(from y in hashSet
					orderby y.GetSnappingDepth()
					select new UndoObject.UndoAction(y, false));
					foreach (Furniture furniture in from x in <LateUpdate>c__AnonStorey.furniture
					where x != null
					orderby x.GetSnappingDepth()
					select x)
					{
						if (!hashSet.Contains(furniture))
						{
							list.Add(new UndoObject.UndoAction(furniture, false));
							hashSet.Add(furniture);
							foreach (Furniture furniture2 in furniture.IterateSnap(null))
							{
								list.Add(new UndoObject.UndoAction(furniture2, false));
								hashSet.Add(furniture2);
							}
							<LateUpdate>c__AnonStorey.$this.Selected.Remove(furniture);
							UnityEngine.Object.Destroy(furniture.gameObject);
						}
					}
					foreach (RoomSegment roomSegment in from x in <LateUpdate>c__AnonStorey.segments
					where x != null
					select x)
					{
						list.Add(new UndoObject.UndoAction(roomSegment, false));
						<LateUpdate>c__AnonStorey.$this.Selected.Remove(roomSegment);
						UnityEngine.Object.Destroy(roomSegment.gameObject);
					}
					if (list.Count > 0)
					{
						GameSettings.Instance.AddUndo(list.ToArray());
					}
					<LateUpdate>c__AnonStorey.$this.DoPostSelectChecks();
					<LateUpdate>c__AnonStorey.$this.ToggleRightClickMenu(false);
					if (flag)
					{
						WindowManager.Instance.ShowMessageBox("CannotBulldozeSupport".Loc(), false, DialogWindow.DialogType.Error);
					}
				}, "Delete button", null);
			}
		}
		this.SelectionCode();
	}

	private HashSet<string> GetTypes()
	{
		HashSet<string> hashSet = new HashSet<string>();
		foreach (global::Selectable selectable in from x in this.Selected
		where x != null
		select x)
		{
			if (selectable is Furniture)
			{
				hashSet.Add(((Furniture)selectable).Type);
			}
			else if (selectable is RoomSegment)
			{
				hashSet.Add(((RoomSegment)selectable).Type);
			}
			else if (selectable is Room)
			{
				hashSet.Add("Room");
			}
			else if (selectable is Actor)
			{
				hashSet.Add("Actor");
			}
			else if (selectable is RoadNode)
			{
				hashSet.Add("Parking");
			}
		}
		return hashSet;
	}

	private string GetSelectionType(global::Selectable sel)
	{
		if (sel is Furniture)
		{
			return ((Furniture)sel).Type;
		}
		if (sel is RoomSegment)
		{
			return ((RoomSegment)sel).Type;
		}
		if (sel is Room)
		{
			return "Room";
		}
		if (sel is Actor)
		{
			return "Actor";
		}
		if (sel is RoadNode)
		{
			return "Parking";
		}
		return null;
	}

	private void SelectionCode()
	{
		if (GameSettings.FreezeGame || BuildController.Instance.IsActive())
		{
			return;
		}
		if (!WindowManager.HasModal && !GameSettings.Instance.WireMode && !CameraScript.Instance.wasDragging && this.overGUI && InputController.GetKeyUp(InputController.Keys.ContextMenu, false) && SelectorController.CanClick)
		{
			if (this.Selected.Count > 0)
			{
				this.ToggleRightClickMenu(true);
			}
			return;
		}
		if (this.overGUI || !SelectorController.CanClick || WindowManager.HasModal)
		{
			SelectorController.CanClick = true;
			return;
		}
		Ray ray = CameraScript.Instance.mainCam.ScreenPointToRay(Input.mousePosition);
		foreach (global::Selectable selectable in this.Selected)
		{
			Actor actor = selectable as Actor;
			if (actor != null && actor.isActiveAndEnabled)
			{
				string highestThought = actor.employee.GetHighestThought();
				if (highestThought != null)
				{
					for (int i = 0; i < actor.Colliders.Length; i++)
					{
						BoxCollider boxCollider = actor.Colliders[i];
						RaycastHit raycastHit;
						if (boxCollider.Raycast(ray, out raycastHit, 150f))
						{
							ThoughtBubble.Instance.SetThought(highestThought.Loc(), actor.NeckBone);
							break;
						}
					}
				}
			}
		}
		if (GameSettings.Instance.WireMode)
		{
			SelectorController.UpdateInfoPanel("WireModeLongHint".Loc(), null, null, null, null, null, null, false, false);
			if (InputController.GetKeyUp(InputController.Keys.NormalSelection, false))
			{
				bool flag = false;
				RaycastHit[] source = Physics.RaycastAll(ray);
				foreach (RaycastHit raycastHit2 in from x in source
				orderby x.distance
				select x)
				{
					Server component = raycastHit2.collider.GetComponent<Server>();
					if (component != null && component.GetComponent<Furniture>().Parent.Floor == GameSettings.Instance.ActiveFloor)
					{
						if (this.SelectedServer != null)
						{
							if (this.SelectedServer == component)
							{
								this.SelectedServer.CancelWire();
								this.SelectedServer.Selected = false;
								this.SelectedServer = null;
							}
							else
							{
								this.SelectedServer.WireTo(component, false);
								UISoundFX.PlaySFX("ServerConnect", -1f, 0f);
								this.SelectedServer.Selected = false;
								this.SelectedServer = null;
							}
						}
						else
						{
							this.SelectedServer = component;
							this.SelectedServer.Selected = true;
						}
						flag = true;
						break;
					}
				}
				if (!flag && this.SelectedServer != null)
				{
					this.SelectedServer.CancelWire();
					this.SelectedServer.Selected = false;
					this.SelectedServer = null;
				}
			}
			if (Input.GetMouseButtonUp(1) && this.SelectedServer != null)
			{
				this.SelectedServer.Selected = false;
				this.SelectedServer.ReWire();
				this.SelectedServer = null;
			}
			return;
		}
		bool keyUp = InputController.GetKeyUp(InputController.Keys.NormalSelection, false);
		bool key = InputController.GetKey(InputController.Keys.SweepSelect, false);
		if (InputController.GetKeyDown(InputController.Keys.SweepSelect, false))
		{
			this.FirstSweep = true;
			this.SweepDeselectMode = false;
		}
		bool keyUp2 = InputController.GetKeyUp(InputController.Keys.MultipleSelect, false);
		if (key || keyUp || keyUp2)
		{
			HashSet<string> hashSet = (!key) ? null : this.GetTypes();
			int num = 512;
			Plane plane = new Plane(Vector3.up, Vector3.zero);
			float distance = 0f;
			Vector3 vector = Vector3.zero;
			Vector2 zero = Vector2.zero;
			Room room = null;
			if (!key || hashSet.Contains("Room") || hashSet.Count == 0)
			{
				foreach (Room room2 in from x in GameSettings.Instance.sRoomManager.Rooms
				where (GameSettings.Instance.ActiveFloor < 0 || x.Floor > -1) && x.Floor <= GameSettings.Instance.ActiveFloor
				orderby x.Floor descending
				select x)
				{
					if (room2.Floor != num)
					{
						num = room2.Floor;
						plane = new Plane(Vector3.up, Vector3.up * (float)room2.Floor * 2f);
						plane.Raycast(ray, out distance);
						vector = ray.GetPoint(distance);
						zero = new Vector2(vector.x, vector.z);
					}
					if (room2.IsInside(zero))
					{
						room = room2;
						break;
					}
				}
			}
			if (key && !this.FirstSweep && room != null && this.SweepDeselectMode != this.Selected.Contains(room))
			{
				room = null;
			}
			RaycastHit[] source2 = Physics.RaycastAll(ray);
			bool flag2 = false;
			foreach (RaycastHit raycastHit3 in from x in source2
			orderby x.distance
			select x)
			{
				global::Selectable component2 = raycastHit3.collider.GetComponent<global::Selectable>();
				if (component2 == null && raycastHit3.rigidbody != null)
				{
					component2 = raycastHit3.rigidbody.GetComponent<global::Selectable>();
				}
				if (component2 != null && component2.IsSelectable())
				{
					if (component2.enabled && component2.CanSelect && component2.transform.position.y <= (float)(GameSettings.Instance.ActiveFloor * 2 + 1) && (!(room != null) || component2.transform.position.y >= (float)(room.Floor * 2 - 1)))
					{
						if (!key)
						{
							if (!keyUp2)
							{
								if (HUD.Instance.BuildMode && this.Selected.Count == 1 && Time.timeSinceLevelLoad - this.LastSelectTime < 0.3f && this.Selected.Contains(component2) && component2.GetActions().Contains("Move"))
								{
									SelectorController.RightClickActions["Move"].GroupAction(this.Selected.ToArray<global::Selectable>(), this);
									flag2 = true;
									break;
								}
								this.Highligt(false);
								this.Selected.Clear();
							}
							if (!this.Selected.Contains(component2))
							{
								this.LastSelectTime = Time.timeSinceLevelLoad;
								UISoundFX.PlaySFX("ObjectHighlight", -1f, 0f);
								this.Selected.Add(component2);
								if (keyUp2)
								{
									HintController.Instance.Show(HintController.Hints.HintQuickSelect);
								}
							}
							else if (keyUp2)
							{
								component2.Highlight(false, false);
								this.Selected.Remove(component2);
							}
							flag2 = true;
							break;
						}
						if (this.FirstSweep)
						{
							this.SweepDeselectMode = this.Selected.Contains(component2);
						}
						if (this.SweepDeselectMode == this.Selected.Contains(component2) && (hashSet.Count == 0 || hashSet.Contains(this.GetSelectionType(component2))))
						{
							if (this.SweepDeselectMode)
							{
								component2.Highlight(false, false);
								this.Selected.Remove(component2);
							}
							else
							{
								UISoundFX.PlaySFX("ObjectHighlight", -1f, 0f);
								this.Selected.Add(component2);
							}
							this.FirstSweep = false;
							flag2 = true;
							break;
						}
					}
				}
			}
			if (!flag2 && room != null)
			{
				if (key)
				{
					if (this.FirstSweep)
					{
						this.SweepDeselectMode = this.Selected.Contains(room);
					}
					if (this.SweepDeselectMode)
					{
						room.Highlight(false, false);
						this.Selected.Remove(room);
					}
					else
					{
						UISoundFX.PlaySFX("ObjectHighlight", -1f, 0f);
						this.Selected.Add(room);
					}
					this.FirstSweep = false;
					flag2 = true;
				}
				else
				{
					if (!keyUp2)
					{
						this.Highligt(false);
						this.Selected.Clear();
					}
					if (!this.Selected.Contains(room))
					{
						UISoundFX.PlaySFX("ObjectHighlight", -1f, 0f);
						this.Selected.Add(room);
						if (keyUp2)
						{
							HintController.Instance.Show(HintController.Hints.HintQuickSelect);
						}
					}
					else if (keyUp2)
					{
						room.Highlight(false, false);
						this.Selected.Remove(room);
					}
				}
				flag2 = true;
			}
			if (!flag2 && !keyUp2 && !key)
			{
				this.Highligt(false);
				this.Selected.Clear();
			}
			this.DoPostSelectChecks();
		}
		if (InputController.GetKeyUp(InputController.Keys.ContextMenu, false) && !CameraScript.Instance.wasDragging)
		{
			if (this.Selected.Count < 2)
			{
				int num2 = 512;
				Plane plane2 = new Plane(Vector3.up, Vector3.zero);
				float distance2 = 0f;
				Vector3 vector2 = Vector3.zero;
				Vector2 zero2 = Vector2.zero;
				Room room3 = null;
				foreach (Room room4 in from x in GameSettings.Instance.sRoomManager.Rooms
				where (GameSettings.Instance.ActiveFloor < 0 || x.Floor > -1) && x.Floor <= GameSettings.Instance.ActiveFloor
				orderby x.Floor descending
				select x)
				{
					if (room4.Floor != num2)
					{
						num2 = room4.Floor;
						plane2 = new Plane(Vector3.up, Vector3.up * (float)room4.Floor * 2f);
						plane2.Raycast(ray, out distance2);
						vector2 = ray.GetPoint(distance2);
						zero2 = new Vector2(vector2.x, vector2.z);
					}
					if (room4.IsInside(zero2))
					{
						room3 = room4;
						break;
					}
				}
				RaycastHit[] source3 = Physics.RaycastAll(ray);
				bool flag3 = false;
				foreach (RaycastHit raycastHit4 in from x in source3
				orderby x.distance
				select x)
				{
					global::Selectable component3 = raycastHit4.collider.GetComponent<global::Selectable>();
					if (component3 == null && raycastHit4.rigidbody != null)
					{
						component3 = raycastHit4.rigidbody.GetComponent<global::Selectable>();
					}
					if (component3 != null && component3.IsSelectable())
					{
						if (component3.enabled && component3.CanSelect && component3.transform.position.y <= (float)(GameSettings.Instance.ActiveFloor * 2 + 1) && (!(room3 != null) || component3.transform.position.y >= (float)(room3.Floor * 2)))
						{
							if (this.Selected.Contains(component3))
							{
								flag3 = true;
								break;
							}
							this.Highligt(false);
							this.Selected.Clear();
							UISoundFX.PlaySFX("ObjectHighlight", -1f, 0f);
							this.Selected.Add(component3);
							this.DoPostSelectChecks();
							flag3 = true;
							break;
						}
					}
				}
				if (!flag3 && room3 != null && !this.Selected.Contains(room3))
				{
					this.Highligt(false);
					this.Selected.Clear();
					UISoundFX.PlaySFX("ObjectHighlight", -1f, 0f);
					this.Selected.Add(room3);
					this.DoPostSelectChecks();
					flag3 = true;
				}
			}
			if (this.Selected.Count > 0)
			{
				this.ToggleRightClickMenu(true);
			}
		}
	}

	public Room GetRoomMouseCollision(Ray mRay, int floor, bool includeSel)
	{
		float distance = 0f;
		Plane plane = new Plane(Vector3.up, Vector3.up * (float)floor * 2f);
		plane.Raycast(mRay, out distance);
		Vector3 point = mRay.GetPoint(distance);
		Vector2 p = new Vector2(point.x, point.z);
		foreach (Room room in from x in GameSettings.Instance.sRoomManager.Rooms
		where x.Floor == floor
		select x)
		{
			if (room.IsInside(p))
			{
				if (!includeSel && this.Selected.Contains(room))
				{
					return null;
				}
				return room;
			}
		}
		return null;
	}

	public void DoPostSelectChecks()
	{
		this.Highligt(true);
		SelectorController.UpdateInfoPanel();
		if (HUD.Instance != null && HUD.Instance.SelectionFilterToggle.isOn)
		{
			HUD.Instance.OnWorkItemToggle();
		}
	}

	public void PopulateRightClickMenu()
	{
		HashSet<string> hashSet = new HashSet<string>();
		foreach (global::Selectable selectable in this.Selected)
		{
			hashSet.AddRange(selectable.GetActions());
		}
		Dictionary<string, SelectorController.RightClickAction> dictionary = hashSet.ToDictionary((string x) => x, (string x) => SelectorController.RightClickActions[x]);
		if (!GameSettings.Instance.EditMode)
		{
			List<Actor> list = (from x in this.Selected.OfType<Actor>()
			where x.AItype == AI<Actor>.AIType.Employee
			select x).ToList<Actor>();
			List<Furniture> fc = (from x in this.Selected.OfType<Furniture>()
			where x.CanAssign
			select x).ToList<Furniture>();
			if (fc.Count > 0)
			{
				if (list.Count == 1)
				{
					List<Actor> acs1 = list;
					dictionary.Add("Pair", new SelectorController.RightClickAction(SelectorController.ACTCAT.NULL | SelectorController.ACTCAT.FURN | SelectorController.ACTCAT.EMP, "FurniturePlus", SelectorController.ContextButtonGroup.Assign, delegate
					{
						Actor actor = acs1[0];
						if (actor != null)
						{
							actor.Owns.AddRange(fc);
							fc.ForEach(delegate(Furniture z)
							{
								z.OwnedBy = actor;
							});
						}
					}));
				}
				else if (list.Count == 0)
				{
					dictionary.Add("PairDirect", new SelectorController.RightClickAction(SelectorController.ACTCAT.NULL | SelectorController.ACTCAT.FURN, "FurniturePlus", SelectorController.ContextButtonGroup.Assign, delegate
					{
						List<Actor> acts = (from x in GameSettings.Instance.sActorManager.Actors
						orderby x.employee.ExtraName
						select x).ToList<Actor>();
						this.selectWindow.Show("Assign", from z in acts
						select z.employee.ExtraName, delegate(int i)
						{
							acts[i].Owns.AddRange(fc);
							fc.ForEach(delegate(Furniture z)
							{
								z.OwnedBy = acts[i];
							});
						}, false, true, true, false, null);
					}));
				}
			}
			List<Room> fcs = (from x in this.Selected.OfType<Room>()
			where x.IsPlayerControlled()
			select x).ToList<Room>();
			if (fcs.Count > 0)
			{
				if (fcs.Any((Room x) => x.Teams.Count > 0))
				{
					dictionary.Add("AutoAssign", new SelectorController.RightClickAction(SelectorController.ACTCAT.ROOM, "Computer", SelectorController.ContextButtonGroup.Assign, delegate
					{
						SelectorController.AutoAssignComputers(fcs, false);
					}));
				}
				if (fcs.All((Room x) => x.Teams.Count == 0))
				{
					dictionary.Add("AutoAssign", new SelectorController.RightClickAction(SelectorController.ACTCAT.ROOM, "Computer", SelectorController.ContextButtonGroup.Assign, delegate
					{
						SelectorController.AutoAssignComputers(fcs, true);
					}));
				}
			}
		}
		using (Dictionary<string, SelectorController.RightClickAction>.Enumerator enumerator2 = dictionary.GetEnumerator())
		{
			while (enumerator2.MoveNext())
			{
				KeyValuePair<string, SelectorController.RightClickAction> action = enumerator2.Current;
				SelectorController $this = this;
				Action action2 = action.Value.DirectAction ?? delegate
				{
					action.Value.GroupAction((from x in $this.Selected
					where x != null && x.gameObject != null && x.GetActions().Contains(action.Key)
					select x).ToArray<global::Selectable>(), $this);
				};
				this.rcPanel.AddButton("Action" + action.Key, action.Value.Icon, action2, action.Value.Category, action.Value.Order);
			}
		}
	}

	public static string[] FixFurnitureTranslation(Furniture[] furn)
	{
		List<string[]> list = (from x in furn
		select new string[]
		{
			(!x.PrimaryColorName.Equals("Primary")) ? ("FC" + x.PrimaryColorName).LocDef(x.PrimaryColorName) : null,
			(!x.SecondaryColorName.Equals("Secondary")) ? ("FC" + x.SecondaryColorName).LocDef(x.SecondaryColorName) : null,
			(!x.TertiaryColorName.Equals("Tertiary")) ? ("FC" + x.TertiaryColorName).LocDef(x.TertiaryColorName) : null
		}).ToList<string[]>();
		HashSet<string> hashSet = new HashSet<string>();
		HashSet<string> hashSet2 = new HashSet<string>();
		HashSet<string> hashSet3 = new HashSet<string>();
		for (int i = 0; i < list.Count; i++)
		{
			if (list[i][0] != null)
			{
				hashSet.Add(list[i][0]);
			}
			if (list[i][1] != null)
			{
				hashSet2.Add(list[i][1]);
			}
			if (list[i][2] != null)
			{
				hashSet3.Add(list[i][2]);
			}
		}
		return new string[]
		{
			(hashSet.Count != 1) ? "Primary".Loc() : hashSet.First<string>(),
			(hashSet2.Count != 1) ? "Secondary".Loc() : hashSet2.First<string>(),
			(hashSet3.Count != 1) ? "Tertiary".Loc() : hashSet3.First<string>()
		};
	}

	public void ToggleRightClickMenu(bool enable)
	{
		if (enable)
		{
			this.PopulateRightClickMenu();
			this.rcPanel.Activate(Input.mousePosition);
		}
		else
		{
			this.rcPanel.Deactivate();
		}
	}

	public void Highligt(bool highligt)
	{
		this.SecondaryHighlights.RemoveAll((global::Selectable x) => x == null || x.gameObject == null);
		this.Selected.RemoveAll((global::Selectable x) => x == null || x.gameObject == null);
		this.SecondaryHighlights.ForEach(delegate(global::Selectable x)
		{
			x.Highlight(false, true);
		});
		this.SecondaryHighlights.Clear();
		this.Selected.ForEach(delegate(global::Selectable x)
		{
			x.Highlight(highligt, false);
		});
	}

	public string[] GetDescription()
	{
		IEnumerable<global::Selectable> source = from x in this.Selected
		where x != null && x.gameObject != null
		select x;
		string[] array = new string[4];
		array[0] = source.Count((global::Selectable x) => x.GetComponent<Actor>() != null).ToString();
		array[1] = source.Count((global::Selectable x) => x.GetComponent<Room>() != null).ToString();
		array[2] = source.Count((global::Selectable x) => x.GetComponent<Furniture>() != null).ToString();
		array[3] = source.Count((global::Selectable x) => x.GetComponent<RoomSegment>() != null).ToString();
		return array;
	}

	public void SaveRoomText()
	{
		Room room = this.Selected.OfType<Room>().FirstOrDefault<Room>();
		if (room != null)
		{
			MiscMsg.SendMsg("RoomTest", room.ConvertToString());
		}
	}

	public void UpdateTeamSelection(bool active)
	{
		if (active)
		{
			HashSet<string> hashSet = new HashSet<string>();
			foreach (global::Selectable selectable in this.Selected)
			{
				if (selectable != null)
				{
					Room room = selectable as Room;
					if (room != null)
					{
						hashSet.AddRange(from x in room.Teams
						select x.Name);
						for (int i = 0; i < room.Occupants.Count; i++)
						{
							Actor actor = room.Occupants[i];
							if (actor.AItype == AI<Actor>.AIType.Employee && actor.Team != null)
							{
								hashSet.Add(actor.Team);
							}
						}
					}
					else
					{
						Actor actor2 = selectable as Actor;
						if (actor2 != null && actor2.Team != null)
						{
							hashSet.Add(actor2.Team);
						}
					}
				}
			}
			this.SelectedTeams = hashSet;
		}
		else
		{
			this.SelectedTeams = null;
		}
	}

	static SelectorController()
	{
		// Note: this type is marked as 'beforefieldinit'.
		Dictionary<string, SelectorController.RightClickAction> dictionary = new Dictionary<string, SelectorController.RightClickAction>();
		Dictionary<string, SelectorController.RightClickAction> dictionary2 = dictionary;
		string key = "Default Style";
		SelectorController.ACTCAT category = SelectorController.ACTCAT.FURN;
		string icon = "Checkmark";
		SelectorController.ContextButtonGroup order = SelectorController.ContextButtonGroup.Style;
		if (SelectorController.<>f__mg$cache0 == null)
		{
			SelectorController.<>f__mg$cache0 = new Action<global::Selectable[], SelectorController>(SelectorController.DefaultStyleAction);
		}
		dictionary2.Add(key, new SelectorController.RightClickAction(category, icon, order, SelectorController.<>f__mg$cache0));
		Dictionary<string, SelectorController.RightClickAction> dictionary3 = dictionary;
		string key2 = "Save Style";
		SelectorController.ACTCAT category2 = SelectorController.ACTCAT.ROOM;
		string icon2 = "Disk";
		SelectorController.ContextButtonGroup order2 = SelectorController.ContextButtonGroup.Style;
		if (SelectorController.<>f__mg$cache1 == null)
		{
			SelectorController.<>f__mg$cache1 = new Action<global::Selectable[], SelectorController>(SelectorController.SaveRoomStyle);
		}
		dictionary3.Add(key2, new SelectorController.RightClickAction(category2, icon2, order2, SelectorController.<>f__mg$cache1));
		Dictionary<string, SelectorController.RightClickAction> dictionary4 = dictionary;
		string key3 = "Apply Style";
		SelectorController.ACTCAT category3 = SelectorController.ACTCAT.ROOM;
		string icon3 = "Pipette";
		SelectorController.ContextButtonGroup order3 = SelectorController.ContextButtonGroup.Style;
		if (SelectorController.<>f__mg$cache2 == null)
		{
			SelectorController.<>f__mg$cache2 = new Action<global::Selectable[], SelectorController>(SelectorController.ApplyRoomStyle);
		}
		dictionary4.Add(key3, new SelectorController.RightClickAction(category3, icon3, order3, SelectorController.<>f__mg$cache2));
		Dictionary<string, SelectorController.RightClickAction> dictionary5 = dictionary;
		string key4 = "Room Color";
		SelectorController.ACTCAT category4 = SelectorController.ACTCAT.ROOM;
		string icon4 = "Brush";
		SelectorController.ContextButtonGroup order4 = SelectorController.ContextButtonGroup.Style;
		if (SelectorController.<>f__mg$cache3 == null)
		{
			SelectorController.<>f__mg$cache3 = new Action<global::Selectable[], SelectorController>(SelectorController.RoomColorAction);
		}
		dictionary5.Add(key4, new SelectorController.RightClickAction(category4, icon4, order4, SelectorController.<>f__mg$cache3));
		Dictionary<string, SelectorController.RightClickAction> dictionary6 = dictionary;
		string key5 = "Material";
		SelectorController.ACTCAT category5 = SelectorController.ACTCAT.ROOM;
		string icon5 = "Brick";
		SelectorController.ContextButtonGroup order5 = SelectorController.ContextButtonGroup.Style;
		if (SelectorController.<>f__mg$cache4 == null)
		{
			SelectorController.<>f__mg$cache4 = new Action<global::Selectable[], SelectorController>(SelectorController.MaterialAction);
		}
		dictionary6.Add(key5, new SelectorController.RightClickAction(category5, icon5, order5, SelectorController.<>f__mg$cache4));
		Dictionary<string, SelectorController.RightClickAction> dictionary7 = dictionary;
		string key6 = "Educate";
		SelectorController.ACTCAT category6 = SelectorController.ACTCAT.EMP;
		string icon6 = "Education";
		SelectorController.ContextButtonGroup order6 = SelectorController.ContextButtonGroup.Manage;
		if (SelectorController.<>f__mg$cache5 == null)
		{
			SelectorController.<>f__mg$cache5 = new Action<global::Selectable[], SelectorController>(SelectorController.EducateAction);
		}
		dictionary7.Add(key6, new SelectorController.RightClickAction(category6, icon6, order6, SelectorController.<>f__mg$cache5));
		Dictionary<string, SelectorController.RightClickAction> dictionary8 = dictionary;
		string key7 = "Dismiss";
		SelectorController.ACTCAT category7 = SelectorController.ACTCAT.EMP;
		string icon7 = "Trash";
		SelectorController.ContextButtonGroup order7 = SelectorController.ContextButtonGroup.Manipulate;
		if (SelectorController.<>f__mg$cache6 == null)
		{
			SelectorController.<>f__mg$cache6 = new Action<global::Selectable[], SelectorController>(SelectorController.DismissAction);
		}
		dictionary8.Add(key7, new SelectorController.RightClickAction(category7, icon7, order7, SelectorController.<>f__mg$cache6));
		Dictionary<string, SelectorController.RightClickAction> dictionary9 = dictionary;
		string key8 = "Sell";
		SelectorController.ACTCAT category8 = SelectorController.ACTCAT.FURN;
		string icon8 = "Trash";
		SelectorController.ContextButtonGroup order8 = SelectorController.ContextButtonGroup.Manipulate;
		if (SelectorController.<>f__mg$cache7 == null)
		{
			SelectorController.<>f__mg$cache7 = new Action<global::Selectable[], SelectorController>(SelectorController.SellAction);
		}
		dictionary9.Add(key8, new SelectorController.RightClickAction(category8, icon8, order8, SelectorController.<>f__mg$cache7));
		Dictionary<string, SelectorController.RightClickAction> dictionary10 = dictionary;
		string key9 = "Dismantle";
		SelectorController.ACTCAT category9 = SelectorController.ACTCAT.SEG;
		string icon9 = "Trash";
		SelectorController.ContextButtonGroup order9 = SelectorController.ContextButtonGroup.Manipulate;
		if (SelectorController.<>f__mg$cache8 == null)
		{
			SelectorController.<>f__mg$cache8 = new Action<global::Selectable[], SelectorController>(SelectorController.DismantleAction);
		}
		dictionary10.Add(key9, new SelectorController.RightClickAction(category9, icon9, order9, SelectorController.<>f__mg$cache8));
		Dictionary<string, SelectorController.RightClickAction> dictionary11 = dictionary;
		string key10 = "Destroy";
		SelectorController.ACTCAT category10 = SelectorController.ACTCAT.ROOM;
		string icon10 = "Trash";
		SelectorController.ContextButtonGroup order10 = SelectorController.ContextButtonGroup.Manipulate;
		if (SelectorController.<>f__mg$cache9 == null)
		{
			SelectorController.<>f__mg$cache9 = new Action<global::Selectable[], SelectorController>(SelectorController.DestroyAction);
		}
		dictionary11.Add(key10, new SelectorController.RightClickAction(category10, icon10, order10, SelectorController.<>f__mg$cache9));
		Dictionary<string, SelectorController.RightClickAction> dictionary12 = dictionary;
		string key11 = "Change Team";
		SelectorController.ACTCAT category11 = SelectorController.ACTCAT.EMP;
		string icon11 = "Recycle";
		SelectorController.ContextButtonGroup order11 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cacheA == null)
		{
			SelectorController.<>f__mg$cacheA = new Action<global::Selectable[], SelectorController>(SelectorController.ChangeTeamAction);
		}
		dictionary12.Add(key11, new SelectorController.RightClickAction(category11, icon11, order11, SelectorController.<>f__mg$cacheA));
		Dictionary<string, SelectorController.RightClickAction> dictionary13 = dictionary;
		string key12 = "Change Room Team";
		SelectorController.ACTCAT category12 = SelectorController.ACTCAT.ROOM;
		string icon12 = "Recycle";
		SelectorController.ContextButtonGroup order12 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cacheB == null)
		{
			SelectorController.<>f__mg$cacheB = new Action<global::Selectable[], SelectorController>(SelectorController.ChangeRoomTeamAction);
		}
		dictionary13.Add(key12, new SelectorController.RightClickAction(category12, icon12, order12, SelectorController.<>f__mg$cacheB));
		Dictionary<string, SelectorController.RightClickAction> dictionary14 = dictionary;
		string key13 = "Limit Use";
		SelectorController.ACTCAT category13 = SelectorController.ACTCAT.ROOM;
		string icon13 = "Stop";
		SelectorController.ContextButtonGroup order13 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cacheC == null)
		{
			SelectorController.<>f__mg$cacheC = new Action<global::Selectable[], SelectorController>(SelectorController.LimitUseAction);
		}
		dictionary14.Add(key13, new SelectorController.RightClickAction(category13, icon13, order13, SelectorController.<>f__mg$cacheC));
		Dictionary<string, SelectorController.RightClickAction> dictionary15 = dictionary;
		string key14 = "Select Building";
		SelectorController.ACTCAT category14 = SelectorController.ACTCAT.ROOM;
		string icon14 = "Skyskraper";
		SelectorController.ContextButtonGroup order14 = SelectorController.ContextButtonGroup.Select;
		if (SelectorController.<>f__mg$cacheD == null)
		{
			SelectorController.<>f__mg$cacheD = new Action<global::Selectable[], SelectorController>(SelectorController.SelectBuildingAction);
		}
		dictionary15.Add(key14, new SelectorController.RightClickAction(category14, icon14, order14, SelectorController.<>f__mg$cacheD));
		Dictionary<string, SelectorController.RightClickAction> dictionary16 = dictionary;
		string key15 = "Change Role";
		SelectorController.ACTCAT category15 = SelectorController.ACTCAT.EMP;
		string icon15 = "BuildMode";
		SelectorController.ContextButtonGroup order15 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cacheE == null)
		{
			SelectorController.<>f__mg$cacheE = new Action<global::Selectable[], SelectorController>(SelectorController.ChangeRoleAction);
		}
		dictionary16.Add(key15, new SelectorController.RightClickAction(category15, icon15, order15, SelectorController.<>f__mg$cacheE));
		Dictionary<string, SelectorController.RightClickAction> dictionary17 = dictionary;
		string key16 = "Send home";
		SelectorController.ACTCAT category16 = SelectorController.ACTCAT.NULL | SelectorController.ACTCAT.EMP | SelectorController.ACTCAT.STAFF;
		string icon16 = "ArrowRight";
		SelectorController.ContextButtonGroup order16 = SelectorController.ContextButtonGroup.Manage;
		if (SelectorController.<>f__mg$cacheF == null)
		{
			SelectorController.<>f__mg$cacheF = new Action<global::Selectable[], SelectorController>(SelectorController.SendHomeAction);
		}
		dictionary17.Add(key16, new SelectorController.RightClickAction(category16, icon16, order16, SelectorController.<>f__mg$cacheF));
		Dictionary<string, SelectorController.RightClickAction> dictionary18 = dictionary;
		string key17 = "Furniture color";
		SelectorController.ACTCAT category17 = SelectorController.ACTCAT.FURN;
		string icon17 = "Brush";
		SelectorController.ContextButtonGroup order17 = SelectorController.ContextButtonGroup.Style;
		if (SelectorController.<>f__mg$cache10 == null)
		{
			SelectorController.<>f__mg$cache10 = new Action<global::Selectable[], SelectorController>(SelectorController.FurnitureColor);
		}
		dictionary18.Add(key17, new SelectorController.RightClickAction(category17, icon17, order17, SelectorController.<>f__mg$cache10));
		Dictionary<string, SelectorController.RightClickAction> dictionary19 = dictionary;
		string key18 = "Types in Room";
		SelectorController.ACTCAT category18 = SelectorController.ACTCAT.NULL | SelectorController.ACTCAT.FURN | SelectorController.ACTCAT.ROOM;
		string icon18 = "Furniture";
		SelectorController.ContextButtonGroup order18 = SelectorController.ContextButtonGroup.Select;
		if (SelectorController.<>f__mg$cache11 == null)
		{
			SelectorController.<>f__mg$cache11 = new Action<global::Selectable[], SelectorController>(SelectorController.TypesInRoom);
		}
		dictionary19.Add(key18, new SelectorController.RightClickAction(category18, icon18, order18, SelectorController.<>f__mg$cache11));
		Dictionary<string, SelectorController.RightClickAction> dictionary20 = dictionary;
		string key19 = "Details";
		SelectorController.ACTCAT category19 = SelectorController.ACTCAT.EMP;
		string icon19 = "Employee";
		SelectorController.ContextButtonGroup order19 = SelectorController.ContextButtonGroup.Manage;
		if (SelectorController.<>f__mg$cache12 == null)
		{
			SelectorController.<>f__mg$cache12 = new Action<global::Selectable[], SelectorController>(SelectorController.DetailsAction);
		}
		dictionary20.Add(key19, new SelectorController.RightClickAction(category19, icon19, order19, SelectorController.<>f__mg$cache12));
		Dictionary<string, SelectorController.RightClickAction> dictionary21 = dictionary;
		string key20 = "Change Salary";
		SelectorController.ACTCAT category20 = SelectorController.ACTCAT.EMP;
		string icon20 = "Money";
		SelectorController.ContextButtonGroup order20 = SelectorController.ContextButtonGroup.Manage;
		if (SelectorController.<>f__mg$cache13 == null)
		{
			SelectorController.<>f__mg$cache13 = new Action<global::Selectable[], SelectorController>(SelectorController.ChangeSalaryAction);
		}
		dictionary21.Add(key20, new SelectorController.RightClickAction(category20, icon20, order20, SelectorController.<>f__mg$cache13));
		Dictionary<string, SelectorController.RightClickAction> dictionary22 = dictionary;
		string key21 = "Select Team";
		SelectorController.ACTCAT category21 = SelectorController.ACTCAT.EMP;
		string icon21 = "MoreEmployees";
		SelectorController.ContextButtonGroup order21 = SelectorController.ContextButtonGroup.Select;
		if (SelectorController.<>f__mg$cache14 == null)
		{
			SelectorController.<>f__mg$cache14 = new Action<global::Selectable[], SelectorController>(SelectorController.SelectTeamAction);
		}
		dictionary22.Add(key21, new SelectorController.RightClickAction(category21, icon21, order21, SelectorController.<>f__mg$cache14));
		Dictionary<string, SelectorController.RightClickAction> dictionary23 = dictionary;
		string key22 = "Select Owned";
		SelectorController.ACTCAT category22 = SelectorController.ACTCAT.EMP;
		string icon22 = "MoreFurniture";
		SelectorController.ContextButtonGroup order22 = SelectorController.ContextButtonGroup.Select;
		if (SelectorController.<>f__mg$cache15 == null)
		{
			SelectorController.<>f__mg$cache15 = new Action<global::Selectable[], SelectorController>(SelectorController.SelectOwnedAction);
		}
		dictionary23.Add(key22, new SelectorController.RightClickAction(category22, icon22, order22, SelectorController.<>f__mg$cache15));
		Dictionary<string, SelectorController.RightClickAction> dictionary24 = dictionary;
		string key23 = "Select Staff";
		SelectorController.ACTCAT category23 = SelectorController.ACTCAT.ROOM;
		string icon23 = "EmployeeRoom";
		SelectorController.ContextButtonGroup order23 = SelectorController.ContextButtonGroup.Select;
		if (SelectorController.<>f__mg$cache16 == null)
		{
			SelectorController.<>f__mg$cache16 = new Action<global::Selectable[], SelectorController>(SelectorController.SelectStaffAction);
		}
		dictionary24.Add(key23, new SelectorController.RightClickAction(category23, icon23, order23, SelectorController.<>f__mg$cache16));
		Dictionary<string, SelectorController.RightClickAction> dictionary25 = dictionary;
		string key24 = "Unpair";
		SelectorController.ACTCAT category24 = SelectorController.ACTCAT.FURN;
		string icon24 = "FurnitureMinus";
		SelectorController.ContextButtonGroup order24 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache17 == null)
		{
			SelectorController.<>f__mg$cache17 = new Action<global::Selectable[], SelectorController>(SelectorController.UnpairAction);
		}
		dictionary25.Add(key24, new SelectorController.RightClickAction(category24, icon24, order24, SelectorController.<>f__mg$cache17));
		Dictionary<string, SelectorController.RightClickAction> dictionary26 = dictionary;
		string key25 = "Pair Use";
		SelectorController.ACTCAT category25 = SelectorController.ACTCAT.EMP;
		string icon25 = "EmployeeFurniture";
		SelectorController.ContextButtonGroup order25 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache18 == null)
		{
			SelectorController.<>f__mg$cache18 = new Action<global::Selectable[], SelectorController>(SelectorController.PairUse);
		}
		dictionary26.Add(key25, new SelectorController.RightClickAction(category25, icon25, order25, SelectorController.<>f__mg$cache18));
		Dictionary<string, SelectorController.RightClickAction> dictionary27 = dictionary;
		string key26 = "RoomPair";
		SelectorController.ACTCAT category26 = SelectorController.ACTCAT.STAFF;
		string icon26 = "StructurePlus";
		SelectorController.ContextButtonGroup order26 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache19 == null)
		{
			SelectorController.<>f__mg$cache19 = new Action<global::Selectable[], SelectorController>(SelectorController.PairRoom);
		}
		dictionary27.Add(key26, new SelectorController.RightClickAction(category26, icon26, order26, SelectorController.<>f__mg$cache19));
		Dictionary<string, SelectorController.RightClickAction> dictionary28 = dictionary;
		string key27 = "ReplaceFurn";
		SelectorController.ACTCAT category27 = SelectorController.ACTCAT.FURN;
		string icon27 = "Recycle";
		SelectorController.ContextButtonGroup order27 = SelectorController.ContextButtonGroup.Manipulate;
		if (SelectorController.<>f__mg$cache1A == null)
		{
			SelectorController.<>f__mg$cache1A = new Action<global::Selectable[], SelectorController>(SelectorController.ReplaceFurnAction);
		}
		dictionary28.Add(key27, new SelectorController.RightClickAction(category27, icon27, order27, SelectorController.<>f__mg$cache1A));
		Dictionary<string, SelectorController.RightClickAction> dictionary29 = dictionary;
		string key28 = "Move";
		SelectorController.ACTCAT category28 = SelectorController.ACTCAT.FURN;
		string icon28 = "ArrowRight";
		SelectorController.ContextButtonGroup order28 = SelectorController.ContextButtonGroup.Manipulate;
		if (SelectorController.<>f__mg$cache1B == null)
		{
			SelectorController.<>f__mg$cache1B = new Action<global::Selectable[], SelectorController>(SelectorController.MoveAction);
		}
		dictionary29.Add(key28, new SelectorController.RightClickAction(category28, icon28, order28, SelectorController.<>f__mg$cache1B));
		Dictionary<string, SelectorController.RightClickAction> dictionary30 = dictionary;
		string key29 = "ConnectServers";
		SelectorController.ACTCAT category29 = SelectorController.ACTCAT.FURN;
		string icon29 = "Wires";
		SelectorController.ContextButtonGroup order29 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache1C == null)
		{
			SelectorController.<>f__mg$cache1C = new Action<global::Selectable[], SelectorController>(SelectorController.ConnectServersAction);
		}
		dictionary30.Add(key29, new SelectorController.RightClickAction(category29, icon29, order29, SelectorController.<>f__mg$cache1C));
		Dictionary<string, SelectorController.RightClickAction> dictionary31 = dictionary;
		string key30 = "MergeRooms";
		SelectorController.ACTCAT category30 = SelectorController.ACTCAT.ROOM;
		string icon30 = "Room";
		SelectorController.ContextButtonGroup order30 = SelectorController.ContextButtonGroup.Manipulate;
		if (SelectorController.<>f__mg$cache1D == null)
		{
			SelectorController.<>f__mg$cache1D = new Action<global::Selectable[], SelectorController>(SelectorController.MergeRooms);
		}
		dictionary31.Add(key30, new SelectorController.RightClickAction(category30, icon30, order30, SelectorController.<>f__mg$cache1D));
		Dictionary<string, SelectorController.RightClickAction> dictionary32 = dictionary;
		string key31 = "SelectWall";
		SelectorController.ACTCAT category31 = SelectorController.ACTCAT.SEG;
		string icon31 = "Door";
		SelectorController.ContextButtonGroup order31 = SelectorController.ContextButtonGroup.Select;
		if (SelectorController.<>f__mg$cache1E == null)
		{
			SelectorController.<>f__mg$cache1E = new Action<global::Selectable[], SelectorController>(SelectorController.SelectWallAction);
		}
		dictionary32.Add(key31, new SelectorController.RightClickAction(category31, icon31, order31, SelectorController.<>f__mg$cache1E));
		Dictionary<string, SelectorController.RightClickAction> dictionary33 = dictionary;
		string key32 = "Duplicate";
		SelectorController.ACTCAT category32 = SelectorController.ACTCAT.FURN;
		string icon32 = "MoreFurniture";
		SelectorController.ContextButtonGroup order32 = SelectorController.ContextButtonGroup.Manipulate;
		if (SelectorController.<>f__mg$cache1F == null)
		{
			SelectorController.<>f__mg$cache1F = new Action<global::Selectable[], SelectorController>(SelectorController.DuplicateAction);
		}
		dictionary33.Add(key32, new SelectorController.RightClickAction(category32, icon32, order32, SelectorController.<>f__mg$cache1F));
		Dictionary<string, SelectorController.RightClickAction> dictionary34 = dictionary;
		string key33 = "ResetDefaultStyle";
		SelectorController.ACTCAT category33 = SelectorController.ACTCAT.FURN;
		string icon33 = "Pipette";
		SelectorController.ContextButtonGroup order33 = SelectorController.ContextButtonGroup.Style;
		if (SelectorController.<>f__mg$cache20 == null)
		{
			SelectorController.<>f__mg$cache20 = new Action<global::Selectable[], SelectorController>(SelectorController.ResetDefaultStyleAction);
		}
		dictionary34.Add(key33, new SelectorController.RightClickAction(category33, icon33, order33, SelectorController.<>f__mg$cache20));
		Dictionary<string, SelectorController.RightClickAction> dictionary35 = dictionary;
		string key34 = "SelectBuildingFloor";
		SelectorController.ACTCAT category34 = SelectorController.ACTCAT.ROOM;
		string icon34 = "ArrowRight";
		SelectorController.ContextButtonGroup order34 = SelectorController.ContextButtonGroup.Select;
		if (SelectorController.<>f__mg$cache21 == null)
		{
			SelectorController.<>f__mg$cache21 = new Action<global::Selectable[], SelectorController>(SelectorController.SelectBuildingFloorAction);
		}
		dictionary35.Add(key34, new SelectorController.RightClickAction(category34, icon34, order34, SelectorController.<>f__mg$cache21));
		Dictionary<string, SelectorController.RightClickAction> dictionary36 = dictionary;
		string key35 = "AssignParking";
		SelectorController.ACTCAT category35 = SelectorController.ACTCAT.PARK;
		string icon35 = "EmployeePlus";
		SelectorController.ContextButtonGroup order35 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache22 == null)
		{
			SelectorController.<>f__mg$cache22 = new Action<global::Selectable[], SelectorController>(SelectorController.AssignParkingAction);
		}
		dictionary36.Add(key35, new SelectorController.RightClickAction(category35, icon35, order35, SelectorController.<>f__mg$cache22));
		Dictionary<string, SelectorController.RightClickAction> dictionary37 = dictionary;
		string key36 = "SelectParkedPeople";
		SelectorController.ACTCAT category36 = SelectorController.ACTCAT.PARK;
		string icon36 = "MoreEmployees";
		SelectorController.ContextButtonGroup order36 = SelectorController.ContextButtonGroup.Select;
		if (SelectorController.<>f__mg$cache23 == null)
		{
			SelectorController.<>f__mg$cache23 = new Action<global::Selectable[], SelectorController>(SelectorController.SelectParkedPeopleAction);
		}
		dictionary37.Add(key36, new SelectorController.RightClickAction(category36, icon36, order36, SelectorController.<>f__mg$cache23));
		Dictionary<string, SelectorController.RightClickAction> dictionary38 = dictionary;
		string key37 = "SelectNearParking";
		SelectorController.ACTCAT category37 = SelectorController.ACTCAT.PARK;
		string icon37 = "Road";
		SelectorController.ContextButtonGroup order37 = SelectorController.ContextButtonGroup.Select;
		if (SelectorController.<>f__mg$cache24 == null)
		{
			SelectorController.<>f__mg$cache24 = new Action<global::Selectable[], SelectorController>(SelectorController.SelectNearParkingAction);
		}
		dictionary38.Add(key37, new SelectorController.RightClickAction(category37, icon37, order37, SelectorController.<>f__mg$cache24));
		Dictionary<string, SelectorController.RightClickAction> dictionary39 = dictionary;
		string key38 = "GroupRooms";
		SelectorController.ACTCAT category38 = SelectorController.ACTCAT.ROOM;
		string icon38 = "StructurePlus";
		SelectorController.ContextButtonGroup order38 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache25 == null)
		{
			SelectorController.<>f__mg$cache25 = new Action<global::Selectable[], SelectorController>(SelectorController.GroupRooms);
		}
		dictionary39.Add(key38, new SelectorController.RightClickAction(category38, icon38, order38, SelectorController.<>f__mg$cache25));
		Dictionary<string, SelectorController.RightClickAction> dictionary40 = dictionary;
		string key39 = "ToggleRentable";
		SelectorController.ACTCAT category39 = SelectorController.ACTCAT.ROOM;
		string icon39 = "Money";
		SelectorController.ContextButtonGroup order39 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache26 == null)
		{
			SelectorController.<>f__mg$cache26 = new Action<global::Selectable[], SelectorController>(SelectorController.ToggleRentable);
		}
		dictionary40.Add(key39, new SelectorController.RightClickAction(category39, icon39, order39, SelectorController.<>f__mg$cache26));
		Dictionary<string, SelectorController.RightClickAction> dictionary41 = dictionary;
		string key40 = "TogglePlayerOwned";
		SelectorController.ACTCAT category40 = SelectorController.ACTCAT.ROOM;
		string icon40 = "Employee";
		SelectorController.ContextButtonGroup order40 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache27 == null)
		{
			SelectorController.<>f__mg$cache27 = new Action<global::Selectable[], SelectorController>(SelectorController.TogglePlayerOwned);
		}
		dictionary41.Add(key40, new SelectorController.RightClickAction(category40, icon40, order40, SelectorController.<>f__mg$cache27));
		Dictionary<string, SelectorController.RightClickAction> dictionary42 = dictionary;
		string key41 = "GroupRentRooms";
		SelectorController.ACTCAT category41 = SelectorController.ACTCAT.ROOM;
		string icon41 = "StructurePlus";
		SelectorController.ContextButtonGroup order41 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache28 == null)
		{
			SelectorController.<>f__mg$cache28 = new Action<global::Selectable[], SelectorController>(SelectorController.GroupRentRooms);
		}
		dictionary42.Add(key41, new SelectorController.RightClickAction(category41, icon41, order41, SelectorController.<>f__mg$cache28));
		Dictionary<string, SelectorController.RightClickAction> dictionary43 = dictionary;
		string key42 = "AutoGroupRentRooms";
		SelectorController.ACTCAT category42 = SelectorController.ACTCAT.ROOM;
		string icon42 = "Recycle";
		SelectorController.ContextButtonGroup order42 = SelectorController.ContextButtonGroup.Assign;
		if (SelectorController.<>f__mg$cache29 == null)
		{
			SelectorController.<>f__mg$cache29 = new Action<global::Selectable[], SelectorController>(SelectorController.AutoGroupRentRooms);
		}
		dictionary43.Add(key42, new SelectorController.RightClickAction(category42, icon42, order42, SelectorController.<>f__mg$cache29));
		SelectorController.RightClickActions = dictionary;
		SelectorController.CanClick = true;
	}

	public static Dictionary<string, string> CategoryIcons = new Dictionary<string, string>
	{
		{
			"Furniture",
			"Furniture"
		},
		{
			"Room",
			"Room"
		},
		{
			"Employee",
			"Employee"
		},
		{
			"Staff",
			"Staff"
		},
		{
			"Segment",
			"Door"
		},
		{
			"Parking",
			"Road"
		}
	};

	public static string[] Categories = new string[]
	{
		null,
		"Furniture",
		"Room",
		"Employee",
		"Staff",
		"Segment",
		"Parking"
	};

	public static Dictionary<string, SelectorController.RightClickAction> RightClickActions;

	public MultiSelectWindow selectWindow;

	public static SelectorController Instance;

	[NonSerialized]
	public HashSet<global::Selectable> Selected = new HashSet<global::Selectable>();

	public static bool CanClick;

	public InputField SalaryText;

	private bool overGUI;

	public AudioClip Place;

	public Text InfoText;

	public Text PanelButtonText;

	public GUIToolTipper PanelButtonTip;

	public Image[] StatImages;

	public Text[] StatText;

	public GameObject StatPanel;

	public GameObject BigPanel;

	public GameObject PanelButton;

	public GameObject RelocateButton;

	public Material PrimaryHighlightMat;

	public Material SecondaryHighlightMat;

	public RightClickPanel rcPanel;

	public Image BigImage;

	public Text BigText;

	public bool SpecialInfo;

	public Server SelectedServer;

	[NonSerialized]
	public HashSet<global::Selectable> SecondaryHighlights = new HashSet<global::Selectable>();

	public GameObject NeedPanel;

	public GUIProgressBar[] NeedBars;

	public FurnitureReplaceWindow FurnReplacer;

	public float LastSelectTime;

	public bool FirstSweep;

	public bool SweepDeselectMode;

	[NonSerialized]
	public HashSet<string> SelectedTeams;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache0;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache1;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache2;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache3;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache4;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache5;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache6;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache7;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache8;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache9;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cacheA;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cacheB;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cacheC;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cacheD;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cacheE;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cacheF;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache10;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache11;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache12;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache13;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache14;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache15;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache16;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache17;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache18;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache19;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache1A;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache1B;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache1C;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache1D;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache1E;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache1F;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache20;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache21;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache22;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache23;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache24;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache25;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache26;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache27;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache28;

	[CompilerGenerated]
	private static Action<global::Selectable[], SelectorController> <>f__mg$cache29;

	public enum SelectionTypes
	{
		Employee,
		Room,
		Furniture,
		Segment,
		Parking
	}

	[Flags]
	public enum ACTCAT
	{
		NULL = 1,
		FURN = 2,
		ROOM = 4,
		EMP = 8,
		STAFF = 16,
		SEG = 32,
		PARK = 64
	}

	public enum ContextButtonGroup
	{
		Group = -1,
		Style = 2,
		Manage = 1,
		Manipulate = 0,
		Assign = 3,
		Select
	}

	public class RightClickAction
	{
		public RightClickAction(SelectorController.ACTCAT category, string icon, SelectorController.ContextButtonGroup order, Action<global::Selectable[], SelectorController> action)
		{
			this.Category = category;
			this.Icon = icon;
			this.Order = order;
			this.GroupAction = action;
		}

		public RightClickAction(SelectorController.ACTCAT category, string icon, SelectorController.ContextButtonGroup order, Action action)
		{
			this.Category = category;
			this.Icon = icon;
			this.Order = order;
			this.DirectAction = action;
		}

		public string Icon;

		public SelectorController.ACTCAT Category;

		public SelectorController.ContextButtonGroup Order;

		public Action<global::Selectable[], SelectorController> GroupAction;

		public Action DirectAction;
	}
}
