﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ComplaintWindow : MonoBehaviour
{
	public void Show(Actor emp, string[] problems, float demand, float severity)
	{
		this.actor = emp;
		this.Problems = problems;
		this.Severity = severity;
		this.Want = this.actor.employee.Salary + demand;
		GameSettings.ForcePause = true;
		GameSettings.FreezeGame = true;
		this.AboutText.text = "ComplaintDialogInfo".Loc(new object[]
		{
			this.actor.employee.Name,
			Mathf.FloorToInt(this.actor.employee.Age),
			this.actor.employee.RoleString,
			(this.actor.Team != null) ? this.actor.Team : "None".Loc(),
			(TimeOfDay.Instance.Year - this.actor.employee.Hired.Year).ToString()
		});
		this.LastSalary.text = "Current salary".Loc() + ": " + this.actor.employee.Salary.Currency(true);
		this.WantedSalary.text = "Demanded salary".Loc() + ": " + this.Want.Currency(true);
		this.actor.Snapshot();
		this.Portrait.texture = HUD.Instance.Portraits.Cam.targetTexture;
		this.ComplaintText.text = string.Join("\n", problems);
		string arg = (this.actor.employee.NickName == null) ? this.actor.employee.Name : (this.actor.employee.NickName + "(" + this.actor.employee.Name + ")");
		this.Window.NonLocTitle = string.Format("EmployeeQuitTitle".Loc(), arg);
		this.Window.Show();
	}

	public void Accept()
	{
		this.actor.employee.ChangeSalary(this.Want, this.Want, this.actor, false);
		this.Window.Close();
	}

	public void Reject()
	{
		this.actor.Fire();
		if (UnityEngine.Random.value * this.Severity > 0.25f)
		{
			GameSettings.Instance.MyCompany.ChangeBusinessRep(-0.25f, "Disgruntled employee", 1f);
			Newspaper.GenerateEmployeeComplaint(this.actor.employee, Newspaper.MakeList(this.Problems));
		}
		this.Window.Close();
	}

	private void Start()
	{
		this.Window.OnClose = delegate
		{
			GameSettings.ForcePause = false;
		};
	}

	public void Details()
	{
		HUD.Instance.DetailWindow.Show(this.actor, true, false);
	}

	public GUIWindow Window;

	public Text AboutText;

	public Text ComplaintText;

	public Text LastSalary;

	public Text WantedSalary;

	public RawImage Portrait;

	[NonSerialized]
	public Actor actor;

	[NonSerialized]
	private string[] Problems = new string[0];

	private float Want;

	private float Severity;
}
