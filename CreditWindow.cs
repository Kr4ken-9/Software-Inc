﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CreditWindow : MonoBehaviour
{
	private void Start()
	{
		if (this.EULA)
		{
			this.text.text = GameData.LoadFullTextAsset(this.textAsset);
		}
		else
		{
			string[] t = GameData.LoadTextAsset(this.textAsset);
			for (int i = 0; i < t.Length; i++)
			{
				if (t[i].StartsWith("http://"))
				{
					GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
					gameObject.GetComponentInChildren<Text>().text = t[i].Replace("http://www.", string.Empty);
					int i1 = i;
					gameObject.GetComponent<Button>().onClick.AddListener(delegate
					{
						Application.OpenURL(t[i1]);
					});
					gameObject.transform.SetParent(this.MainPanel, false);
				}
				else
				{
					this.AddLabel(t[i]);
				}
			}
			this.AddLabel("Engine");
			this.AddLabel("*Unity3D " + Application.unityVersion + " Pro");
		}
		if (this.EULA)
		{
			this.Window.Close();
		}
	}

	private void AddLabel(string value)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.TextPrefab);
		Text component = gameObject.GetComponent<Text>();
		component.text = value.Replace("*", string.Empty);
		if (value.StartsWith("*"))
		{
			component.fontSize = 16;
		}
		else
		{
			component.color = this.HeaderColor;
		}
		gameObject.transform.SetParent(this.MainPanel, false);
	}

	public void Close()
	{
		if (this.EULA)
		{
			PlayerPrefs.SetInt("EULA", this.Version);
			PlayerPrefs.Save();
		}
		this.Window.Close();
	}

	public GameObject TextPrefab;

	public GameObject ButtonPrefab;

	public int Version;

	public GUIWindow Window;

	public Text text;

	public string textAsset;

	public bool EULA;

	public Transform MainPanel;

	public Color HeaderColor;
}
