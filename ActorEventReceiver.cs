﻿using System;
using UnityEngine;

public class ActorEventReceiver : MonoBehaviour
{
	public void TakeItemNow(string item)
	{
		this.Parent.GetItem(item, true);
	}

	public void HammerHit()
	{
		if (this.Parent.MayPlaySound())
		{
			this.Parent.AudioComp.PlayOneShot(this.Parent.HammerHitSFX);
		}
	}

	public void Interact()
	{
		if (this.Parent.UsingPoint != null)
		{
			this.Parent.UsingPoint.Parent.InteractStart();
		}
	}

	public void OpenVanDoor()
	{
		if (this.Parent.MyCar != null && this.Parent.MyCar.SpawnPoints.Length > 2)
		{
			this.Parent.MyCar.SpawnPoints[2].OpenDoor();
		}
		this.Parent.anim.SetEnum("AnimControl", Actor.AnimationStates.Idle);
	}

	public void CloseCarDoor()
	{
		if (this.Parent.MyCar != null)
		{
			this.Parent.MyCar.SpawnPoints[this.Parent.CarSpawnID].CloseDoor();
		}
	}

	public void OpenCarDoor()
	{
		if (this.Parent.MyCar != null)
		{
			this.Parent.MyCar.SpawnPoints[this.Parent.CarSpawnID].OpenDoor();
		}
	}

	public void CarEntered()
	{
		if (this.Parent.MyCar != null)
		{
			CarSpawn carSpawn = this.Parent.MyCar.SpawnPoints[this.Parent.CarSpawnID];
			if (!carSpawn.AutoCloseDoor)
			{
				carSpawn.CloseDoor();
			}
			carSpawn.Occupants.Remove(this.Parent);
			this.Parent.MyCar = null;
		}
		if (this.Parent.AIScript.currentNode.Name.Equals("Despawn"))
		{
			this.Parent.AIScript.currentNode.Run(this.Parent);
			this.Parent.AIScript.currentNode = AI<Actor>.DummyNode;
		}
	}

	public Actor Parent;
}
