﻿using System;
using System.Collections.Generic;
using System.IO;
using Steamworks;
using UnityEngine;

public class FurnitureMod : IWorkshopItem
{
	public FurnitureMod(string root, List<GameObject> furniture, string itemTitle)
	{
		this.Root = root;
		this.Furniture = furniture;
		this.ItemTitle = itemTitle;
	}

	public string ItemTitle { get; set; }

	public PublishedFileId_t? SteamID { get; set; }

	public bool SetTitle
	{
		get
		{
			return this._setTitle;
		}
		set
		{
			this._setTitle = value;
		}
	}

	public bool CanUpload
	{
		get
		{
			return this._canUpload;
		}
		set
		{
			this._canUpload = value;
		}
	}

	public string GetWorkshopType()
	{
		return "Furniture";
	}

	public string FolderPath()
	{
		return Path.GetFullPath(this.Root);
	}

	public string[] GetValidExts()
	{
		return new string[]
		{
			"xml",
			"png",
			"obj",
			"txt"
		};
	}

	public string[] ExtraTags()
	{
		return new string[0];
	}

	public string GetThumbnail()
	{
		string text = Path.Combine(this.FolderPath(), "Thumbnail.png");
		return (!File.Exists(text)) ? null : text;
	}

	public string Root;

	private bool _canUpload = true;

	private bool _setTitle = true;

	public List<GameObject> Furniture;
}
