﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class DatePicker : MonoBehaviour
{
	public SDateTime CurrentDate
	{
		get
		{
			return new SDateTime((!this.UseDays) ? 0 : this.Day.Selected, this.Month.Selected, this.Year.Selected + this.GetFirstYear());
		}
		set
		{
			if (this._disableUpdate)
			{
				return;
			}
			this._dateSet = true;
			this._disableUpdate = true;
			this.InitCombos();
			this.UpdateCombos();
			SDateTime d = SDateTime.Now() + new SDateTime(0, 0, this.YearsIntoFuture);
			if (value > d)
			{
				this._disableUpdate = false;
				this.CurrentDate = SDateTime.Now();
				return;
			}
			if (this.UseDays)
			{
				this.Day.Selected = value.Day;
			}
			this.Month.Selected = value.Month;
			int num = value.Year;
			int firstYear = this.GetFirstYear();
			num = Mathf.Max(firstYear, num);
			num -= firstYear;
			this.Year.Selected = num;
			this._disableUpdate = false;
		}
	}

	private void InitCombos()
	{
		if (!this._combosSet)
		{
			if (this.AutoDay)
			{
				this.UseDays = (GameSettings.DaysPerMonth > 1);
			}
			this.Day.gameObject.SetActive(this.UseDays);
			if (this.UseDays)
			{
				this.Day.UpdateContent<string>(from x in Enumerable.Range(1, GameSettings.DaysPerMonth)
				select x.ToString());
				this.Day.OnSelectedChanged.AddListener(delegate
				{
					if (!this._disableUpdate)
					{
						this.DateChanged.Invoke(this.CurrentDate);
					}
					this.CurrentDate = this.CurrentDate;
				});
			}
			this.Month.UpdateContent<string>(from x in Enumerable.Range(0, 12)
			select SDateTime.Months[x].Loc());
			this.Month.OnSelectedChanged.AddListener(delegate
			{
				if (!this._disableUpdate)
				{
					this.DateChanged.Invoke(this.CurrentDate);
				}
				this.CurrentDate = this.CurrentDate;
			});
			this._combosSet = true;
		}
	}

	public void Start()
	{
		this.InitCombos();
		this.Year.OnSelectedChanged.AddListener(delegate
		{
			if (!this._disableUpdate)
			{
				this.DateChanged.Invoke(this.CurrentDate);
			}
			this.CurrentDate = this.CurrentDate;
		});
		if (!this._dateSet)
		{
			this.CurrentDate = SDateTime.Now();
		}
	}

	private int GetFirstYear()
	{
		int a = (this.MaxYearsBack < 0) ? 0 : Mathf.Max(0, SDateTime.Now().Year - this.MaxYearsBack);
		int b = this.MinYear - SDateTime.BaseYear;
		return Mathf.Max(a, b);
	}

	public void UpdateCombos()
	{
		int num = SDateTime.Now().Year + this.YearsIntoFuture;
		List<string> list = new List<string>();
		for (int i = this.GetFirstYear(); i <= num; i++)
		{
			list.Add((i + SDateTime.BaseYear).ToString());
		}
		this.Year.UpdateContent<string>(list);
	}

	public GUICombobox Day;

	public GUICombobox Month;

	public GUICombobox Year;

	public int YearsIntoFuture;

	public int MaxYearsBack = -1;

	public int MinYear = 1970;

	public bool AutoDay = true;

	public bool UseDays = true;

	private bool _disableUpdate;

	private bool _dateSet;

	private bool _combosSet;

	public DatePicker.DateEvent DateChanged;

	[Serializable]
	public class DateEvent : UnityEvent<SDateTime>
	{
	}
}
