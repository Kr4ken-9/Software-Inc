﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TableScript : MonoBehaviour
{
	public int TableReserved
	{
		get
		{
			if (this.Parent == null || this.Parent == this)
			{
				if (this._tableReserved > -1 && this.FreeChairs > 0 && this.UsedChairs == 0)
				{
					this.UpdateStatus();
					if (this.UsedChairs == 0)
					{
						this._tableReserved = -1;
					}
				}
				return this._tableReserved;
			}
			return this.Parent.TableReserved;
		}
		set
		{
			if (this.Parent == null || this.Parent == this)
			{
				this._tableReserved = value;
			}
			else
			{
				this.Parent.TableReserved = value;
			}
		}
	}

	private void Start()
	{
		if (this.FurnComp.isTemporary)
		{
			return;
		}
		if (this.Parent == null)
		{
			this.Parent = this;
		}
		if (!this.FurnComp.Deserialized)
		{
			Vector3 vector = Utilities.HSVToRGB(UnityEngine.Random.Range(0f, 360f), 0.75f, 1f);
			this.color = new Color(vector.x, vector.y, vector.z);
		}
		if (this.Parent == this)
		{
			if (this.FurnComp.Deserialized && this.FurnComp.UsableForTableGroup())
			{
				this.FurnComp.Parent.TableParents.Add(this);
			}
			this.UpdateStatus();
		}
	}

	public void PostDeserialize()
	{
		Furniture furniture = Writeable.STGetDeserializedObject(this._serializedParent) as Furniture;
		if (furniture != null && !this.FurnComp.isTemporary)
		{
			this.Parent = furniture.GetComponent<TableScript>();
		}
		this.Children.AddRange(from x in this._serializedTableGroup
		select Writeable.STGetDeserializedObject(x) as Furniture into x
		where x != null && !x.isTemporary
		select x.GetComponent<TableScript>());
	}

	public void UpdateStatus()
	{
		if (this.Parent != null && this.Parent != this)
		{
			this.Parent.UpdateStatus();
			return;
		}
		this.FreeChairs = 0;
		this.FreeEmptyChairs = 0;
		this.UsedChairs = 0;
		foreach (Furniture furniture in this.GetFreeChairs())
		{
			this.FreeChairs += furniture.InteractionPoints.Length;
			if (furniture.SnappedTo == null)
			{
				goto IL_B3;
			}
			if (furniture.SnappedTo.Links.All((SnapPoint z) => z.UsedBy == null))
			{
				goto IL_B3;
			}
			IL_C8:
			for (int i = 0; i < furniture.InteractionPoints.Length; i++)
			{
				if (furniture.InteractionPoints[i].UsedBy != null)
				{
					this.UsedChairs++;
				}
			}
			continue;
			IL_B3:
			this.FreeEmptyChairs += furniture.InteractionPoints.Length;
			goto IL_C8;
		}
	}

	public IEnumerable<TableScript> GetChildren()
	{
		if (this.Parent == null || this.Parent == this)
		{
			yield return this;
			foreach (TableScript child in this.Children)
			{
				yield return child;
			}
		}
		else
		{
			yield return this.Parent;
			foreach (TableScript child2 in this.Parent.Children)
			{
				yield return child2;
			}
		}
		yield break;
	}

	public int CountFreeChairs(bool empty, bool used)
	{
		return Mathf.Max(0, ((!empty) ? this.FreeChairs : this.FreeEmptyChairs) - ((!used) ? this.UsedChairs : 0));
	}

	public Furniture GetFreeChair(Actor act, bool empty = false)
	{
		return this.GetFreeChairs().FirstOrDefault(delegate(Furniture x)
		{
			if (empty && !(x.SnappedTo == null))
			{
				if (!x.SnappedTo.Links.All((SnapPoint z) => z.UsedBy == null))
				{
					return false;
				}
			}
			return x.CanUse(act, "Use");
		});
	}

	public IEnumerable<Furniture> GetFreeChairs()
	{
		foreach (TableScript item in this.GetChildren())
		{
			foreach (Furniture ch in item.GetChairs(true))
			{
				yield return ch;
			}
		}
		yield break;
	}

	public void ReserveTables(bool reserve, bool open = false)
	{
		if (reserve)
		{
			this.TableReserved = ((!open) ? 1 : 0);
		}
		else
		{
			this.TableReserved = -1;
		}
	}

	public Holdable GetHoldable(SnapPoint point)
	{
		return (from x in this.Items
		where x.Key == point
		select x.Value).FirstOrDefault<Holdable>();
	}

	public bool PlaceHoldable(Holdable item, SnapPoint snapPoint)
	{
		if (this.Items.Any((KeyValuePair<SnapPoint, Holdable> x) => x.Key == snapPoint))
		{
			return false;
		}
		this.Items.Add(new KeyValuePair<SnapPoint, Holdable>(snapPoint, item));
		item.transform.parent = base.transform;
		if (snapPoint.UsedBy != null)
		{
			Quaternion rotation = snapPoint.UsedBy.transform.rotation;
			item.transform.position = snapPoint.transform.position + rotation * new Vector3(-0.4f, 0f, 0.4f);
			item.transform.rotation = rotation;
		}
		else
		{
			item.transform.position = snapPoint.transform.position;
			item.transform.rotation = Quaternion.identity;
		}
		item.Parent = this;
		return true;
	}

	public IEnumerable<Furniture> GetChairs(bool onlyEmpty)
	{
		if (this.FurnComp.Type.Equals("Chair"))
		{
			yield return this.FurnComp;
		}
		else
		{
			for (int i = 0; i < this.FurnComp.SnapPoints.Length; i++)
			{
				SnapPoint snap = this.FurnComp.SnapPoints[i];
				if (snap.UsedBy != null && snap.UsedBy.Type.Equals("Chair"))
				{
					if (onlyEmpty && snap.Links.Count != 0)
					{
						if (!snap.Links.All((SnapPoint z) => z.UsedBy == null || !z.UsedBy.DisableTableGrouping))
						{
							goto IL_143;
						}
					}
					yield return snap.UsedBy;
				}
				IL_143:;
			}
		}
		yield break;
	}

	private void FixedUpdate()
	{
		if (this.FurnComp.isTemporary)
		{
			return;
		}
		bool flag = !this.FurnComp.Parent.IsReferenceNull() && this.FurnComp.Parent.Floor == GameSettings.Instance.ActiveFloor;
		for (int i = 0; i < this.Items.Count; i++)
		{
			KeyValuePair<SnapPoint, Holdable> keyValuePair = this.Items[i];
			if (keyValuePair.Value == null || keyValuePair.Value.gameObject == null)
			{
				this.Items.RemoveAt(i);
				i--;
			}
			else
			{
				Renderer[] renderers = keyValuePair.Value.Renderers;
				if (renderers.Length > 0 && renderers[0].enabled != flag)
				{
					for (int j = 0; j < renderers.Length; j++)
					{
						renderers[j].enabled = flag;
					}
				}
			}
		}
	}

	public bool IsOnlyUser(Actor act)
	{
		if (this.UsedChairs > 1)
		{
			return false;
		}
		foreach (TableScript tableScript in from x in this.GetChildren()
		select x)
		{
			foreach (Furniture furniture in tableScript.GetChairs(false))
			{
				InteractionPoint interactionPoint = furniture.GetInteractionPoint("Use", false);
				if (interactionPoint != null && interactionPoint.UsedBy != null && interactionPoint.UsedBy != act)
				{
					return false;
				}
			}
		}
		return true;
	}

	public void DecoupleHoldable(Holdable item)
	{
		for (int i = 0; i < this.Items.Count; i++)
		{
			if (this.Items[i].Value == item)
			{
				this.Items.RemoveAt(i);
				i--;
			}
		}
	}

	private void OnDestroy()
	{
		if (GameSettings.IsQuitting)
		{
			return;
		}
		Furniture component = base.GetComponent<Furniture>();
		if (component.isTemporary)
		{
			return;
		}
		if (component.Parent != null)
		{
			component.Parent.RemoveFurniture(component);
			component.Parent.RemoveTable(this);
		}
		foreach (KeyValuePair<SnapPoint, Holdable> keyValuePair in this.Items)
		{
			UnityEngine.Object.Destroy(keyValuePair.Value.gameObject);
		}
	}

	public void Init()
	{
		this.FurnComp.Parent.AddTable(new Vector2(this.FurnComp.transform.position.x, this.FurnComp.transform.position.z), this);
	}

	public void SerializeState(WriteDictionary dict)
	{
		dict["TableChildren"] = (from x in this.Children
		select x.FurnComp.DID).ToArray<uint>();
		dict["TableReserveStatus"] = this._tableReserved;
		dict["TableParent"] = ((!(this.Parent == null)) ? this.Parent.FurnComp.DID : this.FurnComp.DID);
		dict["TableGroupColor"] = this.color;
	}

	public void DeserializeState(WriteDictionary dict)
	{
		this._serializedTableGroup = dict.Get<uint[]>("TableChildren", new uint[0]);
		this._serializedParent = dict.Get<uint>("TableParent", 0u);
		this.color = dict.Get<SVector3>("TableGroupColor", new SVector3(1f, 1f, 1f, 1f));
		this._tableReserved = dict.Get<int>("TableReserveStatus", -1);
	}

	private void OnDrawGizmos()
	{
		float radius = 0.1f;
		if (this.Parent == null || this.Parent == this)
		{
			radius = 0.2f;
			int tableReserved = this._tableReserved;
			if (tableReserved != -1)
			{
				if (tableReserved != 0)
				{
					if (tableReserved == 1)
					{
						Gizmos.color = Color.black;
					}
				}
				else
				{
					Gizmos.color = Color.white;
				}
			}
			else
			{
				Gizmos.color = this.color;
			}
		}
		else
		{
			int tableReserved2 = this.Parent._tableReserved;
			if (tableReserved2 != -1)
			{
				if (tableReserved2 != 0)
				{
					if (tableReserved2 == 1)
					{
						Gizmos.color = Color.black;
					}
				}
				else
				{
					Gizmos.color = Color.white;
				}
			}
			else
			{
				Gizmos.color = this.Parent.color;
			}
		}
		Gizmos.DrawSphere(base.transform.position + Vector3.up * 1.5f, radius);
		Gizmos.color = Color.white;
	}

	[NonSerialized]
	private List<KeyValuePair<SnapPoint, Holdable>> Items = new List<KeyValuePair<SnapPoint, Holdable>>();

	public Furniture FurnComp;

	[NonSerialized]
	public HashSet<TableScript> Children = new HashSet<TableScript>();

	public TableScript Parent;

	private Color color;

	private int _tableReserved = -1;

	public int FreeChairs;

	public int FreeEmptyChairs;

	public int UsedChairs;

	[NonSerialized]
	private uint[] _serializedTableGroup;

	[NonSerialized]
	private uint _serializedParent;
}
