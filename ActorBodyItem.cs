﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActorBodyItem : MonoBehaviour
{
	public string GetColorName()
	{
		switch (this.Type)
		{
		case ActorBodyItem.BodyType.Legs:
			return "Legs";
		case ActorBodyItem.BodyType.Torso:
			return "Torso";
		case ActorBodyItem.BodyType.Accessory:
			return this.PrettyColorName;
		case ActorBodyItem.BodyType.Head:
			return (this.Gender != ActorBodyItem.GenderType.Female) ? "Facial hair" : "Makeup";
		case ActorBodyItem.BodyType.Eyebrows:
			return "Eyebrows";
		case ActorBodyItem.BodyType.Hair:
			return "Hair";
		default:
			return "N/A";
		}
	}

	private void Awake()
	{
		Transform[] componentsInChildren = base.GetComponentsInChildren<Transform>(true);
		this.Children = new GameObject[componentsInChildren.Length];
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			this.Children[i] = componentsInChildren[i].gameObject;
		}
	}

	public Color GetColor(string cName)
	{
		return this.rend.material.GetColor(this.Colormap.First((ActorBodyItem.ColorMapping x) => x.ColorName.Equals(cName)).MaterialSlot);
	}

	public void SetColor(string cName, Color color)
	{
		ActorBodyItem.ColorMapping colorMapping = this.Colormap.FirstOrDefault((ActorBodyItem.ColorMapping x) => x.ColorName.Equals(cName));
		if (colorMapping != null)
		{
			this.rend.material.SetColor(colorMapping.MaterialSlot, color);
		}
	}

	private void OnDestroy()
	{
		foreach (GameObject gameObject in this.Children)
		{
			if (gameObject != null)
			{
				UnityEngine.Object.Destroy(gameObject);
			}
		}
		if (this.IsFaceTexture)
		{
			Transform parent = base.transform.parent;
			if (parent != null)
			{
				IStylable component = parent.GetComponent<IStylable>();
				if (component != null)
				{
					ActorBodyItem actorBodyItem = component.BodyItems.FirstOrDefault((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head);
					if (actorBodyItem != null && actorBodyItem.rend != null && actorBodyItem.rend.material.GetTexture("_OverlayTex") == this.FaceTexture)
					{
						actorBodyItem.rend.material.SetTexture("_OverlayTex", null);
					}
				}
			}
		}
	}

	public ActorBodyItem.BodyItemObject Save()
	{
		return new ActorBodyItem.BodyItemObject(this);
	}

	public ActorBodyItem.BodyItemObject Save(Dictionary<string, Color> colors)
	{
		return new ActorBodyItem.BodyItemObject(this, colors);
	}

	public void Load(ActorBodyItem.BodyItemObject item)
	{
		using (Dictionary<string, SVector3>.Enumerator enumerator = item.Colors.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				KeyValuePair<string, SVector3> color = enumerator.Current;
				ActorBodyItem.ColorMapping colorMapping = this.Colormap.FirstOrDefault((ActorBodyItem.ColorMapping x) => x.ColorName.Equals(color.Key));
				if (colorMapping != null)
				{
					this.rend.material.SetColor(colorMapping.MaterialSlot, color.Value);
				}
			}
		}
		if (this.Blends != null && this.Blends.Length > 0)
		{
			SkinnedMeshRenderer component = this.rend.GetComponent<SkinnedMeshRenderer>();
			using (Dictionary<string, float>.Enumerator enumerator2 = item.Blends.GetEnumerator())
			{
				while (enumerator2.MoveNext())
				{
					KeyValuePair<string, float> blend = enumerator2.Current;
					ActorBodyItem.BlendKeys blendKeys = this.Blends.First((ActorBodyItem.BlendKeys x) => x.BlendName.Equals(blend.Key));
					if (blendKeys.doubleKey)
					{
						if (blend.Value < 0f)
						{
							component.SetBlendShapeWeight(blendKeys.Index, -blend.Value);
						}
						else
						{
							component.SetBlendShapeWeight(blendKeys.Index2, blend.Value);
						}
					}
					else
					{
						component.SetBlendShapeWeight(blendKeys.Index, blend.Value);
					}
				}
			}
		}
	}

	public bool Hidden;

	public bool KeepLocalPosition;

	public string Name;

	public string Category;

	public string PrettyColorName;

	public string[] LOD2Part;

	public string[] LOD2ColorKey;

	public bool CanDeselect;

	public ActorBodyItem.BodyType Type;

	public ActorBodyItem.GenderType Gender;

	public ActorBodyItem.GUICategory guiCategory;

	public Sprite Thumbnail;

	public Texture2D FaceTexture;

	public bool IsFaceTexture;

	public ActorBodyItem.ColorMapping[] Colormap;

	public ActorBodyItem.BlendKeys[] Blends;

	public bool IsRigged;

	public string RootBone;

	public Renderer rend;

	private GameObject[] Children;

	[Serializable]
	public class ColorMapping
	{
		public string ColorName;

		public string MaterialSlot;

		public string LogicalCategory;
	}

	[Serializable]
	public class BlendKeys
	{
		public string BlendName;

		public int Index;

		public int Index2;

		public bool doubleKey;
	}

	[Serializable]
	public class BodyItemObject
	{
		public BodyItemObject(XMLParser.XMLNode node)
		{
			this.Key = node.Name.Replace("_", " ");
			this.Colors = new Dictionary<string, SVector3>();
			foreach (XMLParser.XMLNode xmlnode in node.GetNode("Colors", true).Children)
			{
				Color white = Color.white;
				ColorUtility.TryParseHtmlString("#" + xmlnode.Value, out white);
				this.Colors[xmlnode.Name.Replace("_", " ")] = white;
			}
			this.Blends = new Dictionary<string, float>();
			foreach (XMLParser.XMLNode xmlnode2 in node.GetNode("Blends", true).Children)
			{
				this.Blends[xmlnode2.Name.Replace("_", " ")] = xmlnode2.Value.ConvertToFloatDef(0f);
			}
		}

		public BodyItemObject(ActorBodyItem item)
		{
			this.Key = item.Type.ToString() + item.Name;
			this.Colors = new Dictionary<string, SVector3>();
			foreach (ActorBodyItem.ColorMapping colorMapping in item.Colormap)
			{
				Color color = item.rend.material.GetColor(colorMapping.MaterialSlot);
				this.Colors[colorMapping.ColorName] = color;
			}
			this.Blends = new Dictionary<string, float>();
			if (item.Blends != null && item.Blends.Length > 0)
			{
				SkinnedMeshRenderer component = item.rend.GetComponent<SkinnedMeshRenderer>();
				foreach (ActorBodyItem.BlendKeys blendKeys in item.Blends)
				{
					if (blendKeys.doubleKey)
					{
						float blendShapeWeight = component.GetBlendShapeWeight(blendKeys.Index);
						float blendShapeWeight2 = component.GetBlendShapeWeight(blendKeys.Index2);
						this.Blends[blendKeys.BlendName] = blendShapeWeight2 - blendShapeWeight;
					}
					else
					{
						this.Blends[blendKeys.BlendName] = component.GetBlendShapeWeight(blendKeys.Index);
					}
				}
			}
		}

		public BodyItemObject(ActorBodyItem item, Dictionary<string, Color> colors)
		{
			this.Key = item.Type.ToString() + item.Name;
			this.Colors = new Dictionary<string, SVector3>();
			foreach (KeyValuePair<string, Color> keyValuePair in colors)
			{
				this.Colors[keyValuePair.Key] = keyValuePair.Value;
			}
			this.Blends = new Dictionary<string, float>();
			if (item.Blends != null)
			{
				foreach (ActorBodyItem.BlendKeys blendKeys in item.Blends)
				{
					this.Blends[blendKeys.BlendName] = ((!blendKeys.doubleKey) ? UnityEngine.Random.value : (UnityEngine.Random.value * 2f - 1f)) * 100f;
				}
			}
		}

		public BodyItemObject()
		{
		}

		public XMLParser.XMLNode Serialize()
		{
			XMLParser.XMLNode xmlnode = new XMLParser.XMLNode("Colors");
			XMLParser.XMLNode xmlnode2 = new XMLParser.XMLNode("Blends");
			XMLParser.XMLNode result = new XMLParser.XMLNode(this.Key.Replace(" ", "_"), new XMLParser.XMLNode[]
			{
				xmlnode,
				xmlnode2
			});
			foreach (KeyValuePair<string, SVector3> keyValuePair in this.Colors)
			{
				xmlnode.Children.Add(new XMLParser.XMLNode(keyValuePair.Key.Replace(" ", "_"), ColorUtility.ToHtmlStringRGB(keyValuePair.Value), null));
			}
			foreach (KeyValuePair<string, float> keyValuePair2 in this.Blends)
			{
				xmlnode2.Children.Add(new XMLParser.XMLNode(keyValuePair2.Key.Replace(" ", "_"), keyValuePair2.Value.ToString("N"), null));
			}
			return result;
		}

		public string Key;

		public Dictionary<string, SVector3> Colors;

		public Dictionary<string, float> Blends;
	}

	public enum BodyType
	{
		Legs,
		Torso,
		Accessory,
		Head,
		Eyebrows,
		Hair
	}

	public enum GenderType
	{
		Male,
		Female,
		Both
	}

	public enum GUICategory
	{
		NA,
		Hair,
		Face,
		Torso,
		Legs,
		Accessory
	}
}
