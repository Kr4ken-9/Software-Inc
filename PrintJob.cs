﻿using System;
using UnityEngine;

[Serializable]
public class PrintJob
{
	public PrintJob()
	{
	}

	public PrintJob(uint id)
	{
		this.ID = id;
		this.DealID = null;
	}

	public PrintJob(uint id, float priority)
	{
		this.ID = id;
		this.Priority = priority;
		this.DealID = null;
	}

	public PrintJob(uint id, uint dealID)
	{
		this.ID = id;
		this.DealID = new uint?(dealID);
	}

	public string StockableName
	{
		get
		{
			IStockable stockable = this.GetStockable();
			return (stockable != null) ? stockable.GetName() : string.Empty;
		}
	}

	public string CompanyName
	{
		get
		{
			IStockable stockable = this.GetStockable();
			if (stockable != null)
			{
				Company company = stockable.GetCompany();
				return (company != null) ? company.Name : string.Empty;
			}
			return string.Empty;
		}
	}

	public uint PhysicalCopies
	{
		get
		{
			IStockable stockable = this.GetStockable();
			return (stockable != null) ? stockable.PhysicalCopies : 0u;
		}
	}

	public IStockable GetStockable()
	{
		return GameSettings.Instance.simulation.GetStockable(this.ID);
	}

	public int PrintPerMonth()
	{
		return this.IsActive() ? Mathf.RoundToInt(HUD.Instance.distributionWindow.PrintSpeed * (this.Priority / HUD.Instance.distributionWindow.PrioritySum)) : 0;
	}

	public bool IsActive()
	{
		return this.Maximum == null || this.Maximum.Value > GameSettings.Instance.GetPrintsInStorage(this.ID, true);
	}

	public string PrintPerMonthLabel()
	{
		int num = this.PrintPerMonth();
		string text = num.ToString("N0");
		PrintDeal deal = this.GetDeal();
		if (deal == null)
		{
			return text;
		}
		string arg = "#00FF00FF";
		if (deal.CurrentAmount <= deal.Goal)
		{
			uint num2 = deal.Goal - deal.CurrentAmount;
			if (Utilities.GetMonths(SDateTime.Now(), deal.EndDate) * (float)num >= num2)
			{
				return text;
			}
			arg = "#FF0000FF";
		}
		return string.Format("<color={0}>", arg) + text + "</color>";
	}

	public PrintDeal GetDeal()
	{
		Deal deal;
		if (this.DealID != null && HUD.Instance.dealWindow.AllDeals.TryGetValue(this.DealID.Value, out deal))
		{
			PrintDeal printDeal = deal as PrintDeal;
			if (printDeal != null)
			{
				return printDeal;
			}
		}
		return null;
	}

	public string GetGoal()
	{
		PrintDeal deal = this.GetDeal();
		if (this.Limit != null)
		{
			return this.Limit.Value.ToString("N0");
		}
		if (this.Maximum != null)
		{
			return "<" + this.Maximum.Value.ToString("N0");
		}
		if (deal == null)
		{
			return "Not applicable".Loc();
		}
		if (deal.CurrentAmount > deal.Goal)
		{
			return "+" + (deal.CurrentAmount - deal.Goal).ToString("N0");
		}
		return (deal.Goal - deal.CurrentAmount).ToString("N0");
	}

	public float GetGoalSort()
	{
		PrintDeal deal = this.GetDeal();
		if (this.Limit != null)
		{
			return this.Limit.Value;
		}
		if (this.Maximum != null)
		{
			return this.Maximum.Value;
		}
		if (deal != null)
		{
			return deal.Goal - deal.CurrentAmount;
		}
		return 0f;
	}

	public string GetDeadline()
	{
		PrintDeal deal = this.GetDeal();
		if (deal != null)
		{
			return deal.EndDate.ToCompactString2();
		}
		return "Not applicable".Loc();
	}

	public int GetDeadlineOrder()
	{
		PrintDeal deal = this.GetDeal();
		if (deal != null)
		{
			return deal.EndDate.ToInt();
		}
		return -1;
	}

	public uint ID;

	public uint? DealID;

	public uint? Limit;

	public uint? Maximum;

	public float Priority = 1f;
}
