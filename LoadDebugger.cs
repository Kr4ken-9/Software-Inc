﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LoadDebugger : MonoBehaviour
{
	private void Awake()
	{
		if (LoadDebugger.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		LoadDebugger.Instance = this;
	}

	public static void AddError(string msg)
	{
		if (LoadDebugger.Instance != null)
		{
			LoadDebugger.Instance.MainText.text = "<color=#FF0000>" + msg + "</color>" + LoadDebugger.Instance.Rest();
			LoadDebugger.Instance.Errors = true;
		}
	}

	private string Rest()
	{
		if (this._first)
		{
			this._first = false;
			return string.Empty;
		}
		return "\n" + LoadDebugger.Instance.MainText.text;
	}

	public static void AddInfo(string msg, string color)
	{
		if (LoadDebugger.Instance != null)
		{
			LoadDebugger.Instance.MainText.text = string.Concat(new string[]
			{
				"<color=",
				color,
				">",
				msg,
				"</color>",
				LoadDebugger.Instance.Rest()
			});
		}
	}

	public static void AddInfo(string msg)
	{
		if (LoadDebugger.Instance != null)
		{
			LoadDebugger.Instance.MainText.text = msg + LoadDebugger.Instance.Rest();
		}
	}

	private void OnDestroy()
	{
		if (LoadDebugger.Instance == this)
		{
			LoadDebugger.Instance = null;
		}
	}

	public Text MainText;

	public static LoadDebugger Instance;

	public bool Errors;

	[NonSerialized]
	private bool _first = true;
}
