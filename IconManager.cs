﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class IconManager : MonoBehaviour
{
	private void Start()
	{
		IconManager.instance = this;
		this.AllIcons = new Dictionary<string, Sprite>();
		for (int i = 0; i < this.IconObjects.Length; i++)
		{
			this.AllIcons.Add(this.IconObjects[i].Name, this.IconObjects[i].Icon);
		}
	}

	private void OnDestroy()
	{
		if (IconManager.instance == this)
		{
			IconManager.instance = null;
		}
	}

	public static Sprite GetIcon(string name)
	{
		return (!(IconManager.instance == null)) ? IconManager.instance.AllIcons[name] : null;
	}

	public IconManager.IconObject[] IconObjects;

	private Dictionary<string, Sprite> AllIcons;

	private static IconManager instance;

	[Serializable]
	public struct IconObject
	{
		public IconObject(string name, Sprite icon)
		{
			this.Name = name;
			this.Icon = icon;
		}

		public string Name;

		public Sprite Icon;
	}
}
