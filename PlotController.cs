﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlotController : MonoBehaviour
{
	public PlotArea CurrentPlot
	{
		get
		{
			return this._currentPlot;
		}
		set
		{
			if (this._currentPlot != null && this._currentPlot.PlotObject != null)
			{
				this._currentPlot.PlotObject.UpdatePlayerOwned();
			}
			this._currentPlot = value;
			if (this._currentPlot != null && this._currentPlot.PlotObject != null)
			{
				this._currentPlot.PlotObject.Renderer.material.color = this._currentPlot.PlotColor.ToColor().Alpha(1f);
			}
		}
	}

	public void EnableCancelButton(bool enable)
	{
		if (!this._changingSliders)
		{
			this.CancelButton.SetActive(enable);
		}
	}

	private void Awake()
	{
		if (PlotController.Instance != null)
		{
			UnityEngine.Object.Destroy(PlotController.Instance);
		}
		PlotController.Instance = this;
	}

	private void OnDestroy()
	{
		if (PlotController.Instance == this)
		{
			PlotController.Instance = null;
		}
	}

	public void Buy()
	{
		if (this.CurrentPlot != null)
		{
			float price = this.CurrentPlot.Price;
			if (this.CurrentPlot.PlayerOwned)
			{
				if (this.CurrentPlot.MonthsLeft == 0)
				{
					GameSettings.Instance.MyCompany.MakeTransaction(price, Company.TransactionCategory.Construction, "Plot");
					List<UndoObject.UndoAction> list = GameSettings.Instance.SellPlot(this.CurrentPlot);
					list.Insert(0, new UndoObject.UndoAction(this.CurrentPlot, price));
					GameSettings.Instance.AddUndo(list.ToArray());
				}
				else
				{
					float num = this.CurrentPlot.Price - (float)this.CurrentPlot.MonthsLeft * this.CurrentPlot.Monthly;
					if (GameSettings.Instance.MyCompany.CanMakeTransaction(num))
					{
						GameSettings.Instance.MyCompany.MakeTransaction(num, Company.TransactionCategory.Construction, "Plot");
						List<UndoObject.UndoAction> list2 = GameSettings.Instance.SellPlot(this.CurrentPlot);
						list2.Insert(0, new UndoObject.UndoAction(this.CurrentPlot, num));
						GameSettings.Instance.AddUndo(list2.ToArray());
						this.CurrentPlot.MonthsLeft = 0;
					}
				}
			}
			else
			{
				float addonCost = this.CurrentPlot.AddonCost;
				List<UndoObject.UndoAction> list3 = new List<UndoObject.UndoAction>();
				if (this.UpfrontSlider.value == 1f)
				{
					if (GameSettings.Instance.MyCompany.CanMakeTransaction(-price))
					{
						GameSettings.Instance.MyCompany.MakeTransaction(-price, Company.TransactionCategory.Construction, "Plot");
						list3.AddRange(GameSettings.Instance.BuyPlot(this.CurrentPlot));
						list3.Add(new UndoObject.UndoAction(this.CurrentPlot, price, addonCost));
						GameSettings.Instance.AddUndo(list3.ToArray());
					}
				}
				else
				{
					float num2 = price * this.UpfrontSlider.value;
					if (GameSettings.Instance.MyCompany.CanMakeTransaction(-num2))
					{
						GameSettings.Instance.MyCompany.MakeTransaction(-num2, Company.TransactionCategory.Construction, "Plot");
						float num3 = price - num2;
						float interest = this.GetInterest();
						float num4 = this.MonthSlider.value * 6f;
						this.CurrentPlot.MonthsLeft = (int)num4;
						this.CurrentPlot.Monthly = num3 / num4 * (1f + interest);
						list3.AddRange(GameSettings.Instance.BuyPlot(this.CurrentPlot));
						list3.Add(new UndoObject.UndoAction(this.CurrentPlot, num2, addonCost));
						GameSettings.Instance.AddUndo(list3.ToArray());
					}
				}
			}
			this.EnableCancelButton(false);
		}
		this.UpdateText();
	}

	public void Toggle()
	{
		base.gameObject.SetActive(!base.gameObject.activeSelf);
	}

	private void OnEnable()
	{
		if (GameSettings.Instance != null)
		{
			if (GameSettings.Instance.RentMode)
			{
				base.gameObject.SetActive(false);
				return;
			}
			foreach (PlotArea plotArea in GameSettings.Instance.GetPlots())
			{
				plotArea.PlotObject.EdgeRenderer.enabled = true;
			}
		}
		if (BuildController.Instance != null)
		{
			BuildController.Instance.ClearBuild(false, false, true, false);
		}
	}

	private void OnDisable()
	{
		if (GameSettings.IsQuitting)
		{
			return;
		}
		if (this.PlotPanel != null)
		{
			this.PlotPanel.SetActive(false);
		}
		this.CurrentPlot = null;
		if (GameSettings.Instance != null)
		{
			foreach (PlotArea plotArea in GameSettings.Instance.GetPlots())
			{
				if (plotArea.PlotObject != null)
				{
					plotArea.PlotObject.EdgeRenderer.enabled = false;
				}
			}
		}
		if (HUD.Instance != null)
		{
			HUD.Instance.UpdateBorderOverlay();
		}
	}

	public void UpdateText()
	{
		List<string> list = new List<string>();
		List<string> list2 = new List<string>();
		list.AddRange(new string[]
		{
			"Plot area".Loc(),
			"Price".Loc()
		});
		list2.AddRange(new string[]
		{
			this.CurrentPlot.Area.ToString("N0") + " m2",
			this.CurrentPlot.Price.Currency(true)
		});
		if (this.CurrentPlot.AddonCost > 0f)
		{
			list.Add("Added valuation".Loc());
			list2.Add(this.CurrentPlot.AddonCost.Currency(true));
		}
		this.ButtonLabel.text = ((!this.CurrentPlot.PlayerOwned) ? "Buy".Loc() : "Sell".Loc());
		if (this.CurrentPlot.PlayerOwned)
		{
			this.PaymentPanel.SetActive(false);
			if (this.CurrentPlot.MonthsLeft > 0)
			{
				list.AddRange(new string[]
				{
					"Monthly".Loc(),
					"Months left".Loc(),
					"Balance".Loc()
				});
				list2.AddRange(new string[]
				{
					this.CurrentPlot.Monthly.Currency(true),
					this.CurrentPlot.MonthsLeft.ToString(),
					(this.CurrentPlot.Price - (float)this.CurrentPlot.MonthsLeft * this.CurrentPlot.Monthly).Currency(true)
				});
			}
		}
		else
		{
			this.PaymentPanel.SetActive(true);
			if (this.UpfrontSlider.value < 1f)
			{
				float price = this.CurrentPlot.Price;
				float num = price * this.UpfrontSlider.value;
				float num2 = price - num;
				float interest = this.GetInterest();
				float num3 = this.MonthSlider.value * 6f;
				float num4 = num2 / num3 * (1f + interest);
				list.AddRange(new string[]
				{
					"Down payment".Loc(),
					"Monthly".Loc(),
					"Months".Loc(),
					"Total".Loc()
				});
				list2.AddRange(new string[]
				{
					num.Currency(true),
					num4.Currency(true),
					num3.ToString("N0"),
					(num3 * num4 + num).Currency(true)
				});
			}
		}
		this.Info.SetData(list.ToArray(), list2.ToArray());
	}

	private float GetInterest()
	{
		return 0.06f + Mathf.Pow(this.MonthSlider.value / 12f, 1.2f) / 2f;
	}

	private void Update()
	{
		if (GameSettings.FreezeGame)
		{
			this.CurrentPlot = null;
			this.PlotPanel.SetActive(false);
			return;
		}
		if (Input.GetMouseButton(1))
		{
			base.gameObject.SetActive(false);
			return;
		}
		if (this.CurrentPlot != null && this.CancelButton.activeSelf)
		{
			this.MoveWindow(this.CurrentPlot);
			return;
		}
		if (this.CurrentPlot != null && RectTransformUtility.RectangleContainsScreenPoint(this.rect, Input.mousePosition))
		{
			GUICheck.OverGUI = false;
			this.MoveWindow(this.CurrentPlot);
			return;
		}
		Vector2 mouseProj = HUD.Instance.GetMouseProj(0f, true);
		if (this.CurrentPlot != null && Utilities.IsInside(mouseProj, this.CurrentPlot.Polygon))
		{
			this.MoveWindow(this.CurrentPlot);
			return;
		}
		foreach (PlotArea plotArea in GameSettings.Instance.GetPlots())
		{
			if (Utilities.IsInside(mouseProj, plotArea.Polygon))
			{
				this.Header.color = plotArea.PlotColor;
				this.MoveWindow(plotArea);
				this.CurrentPlot = plotArea;
				this._changingSliders = true;
				this.MonthSlider.value = 10f;
				float price = plotArea.Price;
				this.UpfrontSlider.value = ((!GameSettings.Instance.MyCompany.CanMakeTransaction(-price)) ? Mathf.Clamp01(GameSettings.Instance.MyCompany.Money * 0.25f / plotArea.Price) : 1f);
				this._changingSliders = false;
				this.UpdateText();
				this.PlotPanel.SetActive(true);
				return;
			}
		}
		this.CurrentPlot = null;
		this.CancelButton.SetActive(false);
		this.PlotPanel.SetActive(false);
	}

	private void MoveWindow(PlotArea plot)
	{
		Vector3 vector = CameraScript.Instance.mainCam.WorldToScreenPoint(plot.Center + Vector3.up * ((float)GameSettings.Instance.ActiveFloor * 2f));
		float num = this.rect.rect.height / 2f;
		float x = HUD.Instance.MainContentPanel.offsetMin.x;
		this.rect.anchoredPosition = new Vector2(Mathf.Clamp(vector.x - x, 128f, (float)Screen.width - 128f), Mathf.Clamp((float)(-(float)Screen.height) + vector.y, (float)(-(float)Screen.height) + 256f + num, -48f - num));
	}

	public static PlotController Instance;

	public RectTransform rect;

	public GameObject PlotPanel;

	public GameObject CancelButton;

	public GameObject PaymentPanel;

	public Image Header;

	public Text ButtonLabel;

	public Slider UpfrontSlider;

	public Slider MonthSlider;

	public VarValueSheet Info;

	[NonSerialized]
	private PlotArea _currentPlot;

	private bool _changingSliders;
}
