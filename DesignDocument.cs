﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class DesignDocument : SoftwareWorkItem
{
	public DesignDocument()
	{
	}

	public DesignDocument(string name) : base(name)
	{
	}

	public DesignDocument(string name, string type, string category, Dictionary<string, uint> needs, uint[] os, float price, SDateTime start, Company company, uint? sequelTo, bool inHouse, float loss, string[] features, ContractWork contract, string server, string server2, bool tut = true) : base(name, type, category, needs, os, price, start, company, sequelTo, inHouse, loss, features, contract, server, server2)
	{
		for (int i = 0; i < this.SpecQuality.Length; i++)
		{
			if (this.SpecDevTime[i, 0] > 0f)
			{
				this.WorkNeeded++;
			}
		}
		if (tut)
		{
			TutorialSystem.Instance.StartTutorial("Design work", false);
		}
	}

	public static SoftwareAlpha DirectToApha(string name, string type, string category, Dictionary<string, uint> needs, uint[] os, float price, SDateTime start, Company company, uint? sequelTo, bool inHouse, float loss, string[] features, ContractWork contract, string server, string server2, int maxBugs)
	{
		SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[type];
		SoftwareProduct softwareProduct = (sequelTo == null) ? null : GameSettings.Instance.simulation.GetProduct(sequelTo.Value, false);
		softwareProduct = ((softwareProduct == null || softwareProduct.Traded) ? null : softwareProduct);
		uint reach = softwareType.GetReach(category, features, os);
		Dictionary<string, float> designResult = softwareType.GetSpecializationMonths(features, category, os, softwareProduct).ToDictionary((KeyValuePair<string, float> x) => x.Key, (KeyValuePair<string, float> x) => 0f);
		return new SoftwareAlpha(name, type, category, needs, features, os, price, start, designResult, 0f, 0f, company, sequelTo, inHouse, loss, contract, server, server2, -1, new SHashSet<uint>(), 0f, reach, 0f, null, maxBugs);
	}

	public static SoftwareWorkItem CreateWork(string name, string type, string category, Dictionary<string, uint> needs, uint[] os, float price, SDateTime start, Company company, uint? sequelTo, bool inHouse, float loss, string[] features, ContractWork contract, string server, string server2, bool tut = true)
	{
		if (GameSettings.Instance.SoftwareTypes[type].CodeArtRatio(features) == 0f)
		{
			return DesignDocument.DirectToApha(name, type, category, needs, os, price, start, company, sequelTo, inHouse, loss, features, contract, server, server2, 0);
		}
		return new DesignDocument(name, type, category, needs, os, price, start, company, sequelTo, inHouse, loss, features, contract, server, server2, tut);
	}

	public override float GetProgress()
	{
		float num = 0f;
		float num2 = 0f;
		for (int i = 0; i < this.SpecQuality.Length; i++)
		{
			float num3 = this.SpecDevTime[i, 0];
			if (num3 > 0f)
			{
				num += this.SpecQuality[i];
				num2 += num3;
			}
		}
		return (num2 != 0f) ? (num / num2 / 2f) : 0f;
	}

	public override float GetMax()
	{
		return 0f;
	}

	public override float GetWorkScore()
	{
		float progress = this.GetProgress();
		if (progress > 1f)
		{
			return 2f - progress;
		}
		return progress;
	}

	public override Color BackColor
	{
		get
		{
			return new Color(0f, 0f, 0.75f);
		}
	}

	public override WorkItem.HasWorkReturn HasWork(Actor actor)
	{
		if (!this.Enabled)
		{
			this.Working.Remove(actor.DID);
			return WorkItem.HasWorkReturn.Ignore;
		}
		if (!actor.employee.IsRole(Employee.RoleBit.Designer))
		{
			this.Working.Remove(actor.DID);
			return WorkItem.HasWorkReturn.NotApplicable;
		}
		bool flag = this.GetProgress() < 1f && (actor.employee.GetSkill(Employee.EmployeeRole.Designer) < 1f || this.GetProgress() < 0.5f);
		if (flag)
		{
			this.Working.Add(actor.DID);
		}
		else
		{
			this.Working.Remove(actor.DID);
		}
		if (!this.HasFinished && this.DoneWork >= this.WorkNeeded)
		{
			this.HasFinished = true;
		}
		return (!flag) ? WorkItem.HasWorkReturn.Finished : WorkItem.HasWorkReturn.True;
	}

	public bool AllZero()
	{
		for (int i = 0; i < this.SpecQuality.Length; i++)
		{
			if (this.SpecQuality[i] > 0f)
			{
				return false;
			}
		}
		return true;
	}

	public bool AnyCode()
	{
		for (int i = 0; i < this.SpecDevTime.Length; i++)
		{
			if (this.SpecDevTime[i, 0] > 0f)
			{
				return true;
			}
		}
		return false;
	}

	public override string GetProgressLabel()
	{
		if (this.contract != null || this.deal > 0u)
		{
			return string.Empty;
		}
		return "\n" + "FollowerAmount".Loc(new object[]
		{
			Mathf.RoundToInt(base.Followers).ToString("N0")
		});
	}

	public override string CurrentStage()
	{
		float num = (0.5f - Mathf.Abs(this.GetProgress() - 0.5f)) * 2f;
		if (this.contract != null)
		{
			return (!this.AllZero()) ? this.contract.GetStatus(num * this.MaxQual, this.DevStart, false) : "DesignWaitLabel".Loc();
		}
		if (base.ActiveDeal != null && base.ActiveDeal.Incoming)
		{
			return base.ActiveDeal.GetStatus() + "\n" + SoftwareType.GetQualityLabel(num * this.MaxQual);
		}
		return "Quality".Loc() + ": " + SoftwareType.GetQualityLabel(num * this.MaxQual);
	}

	public override string GetIcon()
	{
		return "Paper";
	}

	public override Color GetProgressColor()
	{
		float num = (0.5f - Mathf.Abs(this.GetProgress() - 0.5f)) * 2f;
		return base.GetProgressColor() * num + new Color(0.8392157f, 0.521568656f, 0.521568656f, 0.6784314f) * (1f - num);
	}

	public override void DoWork(Actor ac, float effectiveness, float delta)
	{
		this.EverWorked.Add(ac.DID);
		if (effectiveness < 0f)
		{
			return;
		}
		float skill = ac.employee.GetSkill(Employee.EmployeeRole.Designer);
		base.RecordSkill(Employee.EmployeeRole.Designer, skill, delta);
		effectiveness *= ac.GetPCAddonBonus((!ac.employee.IsRole(Employee.RoleBit.Lead)) ? Employee.EmployeeRole.Designer : Employee.EmployeeRole.Lead) * SoftwareType.GetEmployeeCountEffect(Mathf.Max(1, this.Working.Count), this.DevTime * this.CodeArtRatio, true);
		if (this.contract != null)
		{
			effectiveness *= 2f;
		}
		else
		{
			effectiveness *= 1.5f;
		}
		effectiveness *= GameSettings.Instance.Difficulty.MapRange(0f, 2f, 4f, 1f, false);
		float num = (!ac.employee.IsRole(Employee.RoleBit.Lead)) ? 1f : 0.25f;
		if (this.AllZero())
		{
			this.DevStart = new SDateTime(0, 0, TimeOfDay.Instance.Day, TimeOfDay.Instance.Month, TimeOfDay.Instance.Year);
		}
		float num2 = Utilities.PerHour(0.142857149f, delta, true) * 2f;
		num2 /= (float)GameSettings.DaysPerMonth;
		num2 /= (float)this.WorkNeeded;
		if (this.DoneWork >= this.WorkNeeded)
		{
			this.HasFinished = true;
			if (!ac.employee.IsRole(Employee.RoleBit.Lead))
			{
				float num3 = 1f - skill;
				for (int i = 0; i < this.SpecQuality.Length; i++)
				{
					float num4 = this.SpecDevTime[i, 0];
					if (num4 > 0f)
					{
						this.SpecQuality[i] = Mathf.Min(this.SpecQuality[i] + Mathf.Max(ac.employee.GetSpecialization(Employee.EmployeeRole.Designer, this.Specs[i], true), 0.001f) * num2 * effectiveness * num3, num4 * 2f);
					}
				}
			}
		}
		else
		{
			float num5 = 0f;
			for (int j = 0; j < 2; j++)
			{
				for (int k = 0; k < this.SpecQuality.Length; k++)
				{
					if (j == 1 && num5 == 0f)
					{
						break;
					}
					float num6 = this.SpecDevTime[k, 0];
					if (num6 > 0f)
					{
						float num7 = num6 + 0.0001f;
						float num8 = this.SpecQuality[k];
						float num9 = (j != 0) ? 0f : (num2 * effectiveness * num);
						if (num8 > num7 || Mathf.Approximately(num8, num7))
						{
							num5 += num9;
						}
						else
						{
							float num10 = num8 + Mathf.Max(ac.employee.GetSpecialization(Employee.EmployeeRole.Designer, this.Specs[k], true), 0.001f) * (num9 + num5);
							if (num10 > num7 || Mathf.Approximately(num10, num7))
							{
								this.DoneWork++;
								this.SpecQuality[k] = num7;
								num5 += num9 - (num7 - num8);
							}
							else
							{
								this.SpecQuality[k] = num10;
								num5 = 0f;
							}
						}
					}
				}
			}
		}
		if (!ac.employee.IsRole(Employee.RoleBit.Lead))
		{
			float num11 = skill * 0.8f;
			float num12 = 1f / Mathf.Max(0.5f, effectiveness);
			this.Delay += Mathf.Max(0f, num12 * Utilities.PerHour(1f - num11, delta, true) * 2f);
		}
		ac.employee.AddToSpecializations(Employee.EmployeeRole.Designer, this.SpecDevTime, this.Specs, delta, 0f);
	}

	public override float GetWorkBoost(Employee.EmployeeRole role, float currentSkill)
	{
		return base.GetWorkBoost(role, currentSkill) + this.Stability;
	}

	public override Employee.EmployeeRole? GetBoostRole(Actor act)
	{
		if (act.employee.IsRole(Employee.RoleBit.Designer))
		{
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Designer);
		}
		return null;
	}

	public Dictionary<string, float> GetResult()
	{
		Dictionary<string, float> dictionary = new Dictionary<string, float>();
		for (int i = 0; i < this.Specs.Length; i++)
		{
			float num = this.SpecDevTime[i, 0];
			float num2 = (num != 0f) ? (this.SpecQuality[i] / num) : 0f;
			if (num2 > 1f)
			{
				num2 = 2f - num2;
			}
			dictionary[this.Specs[i]] = num2;
		}
		return dictionary;
	}

	public override object PromoteAction()
	{
		if (base.ActiveDeal != null && base.ActiveDeal.Incoming)
		{
			this.Kill(false);
			return null;
		}
		float bugs = (1f - this.GetWorkScore()) * ((float)this.MaxBugs / 2f);
		SoftwareAlpha softwareAlpha = new SoftwareAlpha(base.Name, this._type, this._category, this.Needs, this.Features, this.OSs, this.Price, this.DevStart, this.GetResult(), Mathf.Clamp(this.Delay, 0f, 0.5f), bugs, this.MyCompany, this.SequelTo, this.InHouse, this.Loss, this.contract, this.Server, this.Server2, (!(this.guiItem == null)) ? this.guiItem.transform.GetSiblingIndex() : -1, this.EverWorked, base.Followers, this.MaxFollowers, this.FollowerChange, this.ReleaseDate, this.MaxBugs);
		this.Result = softwareAlpha;
		if (this.deal == 0u && this.contract == null && this._type.Equals("Operating System"))
		{
			SoftwareProduct p = softwareAlpha.CreateMock();
			GameSettings.Instance.simulation.AddProduct(p, true);
		}
		if (!this.AutoDev)
		{
			foreach (Team team in base.GetDevTeams())
			{
				this.Result.AddDevTeam(team, false);
			}
			if (base.Followers == 0f && !this.InHouse && this.contract == null)
			{
				HUD.Instance.AddPopupMessage("NoPreMarketWarning".Loc(new object[]
				{
					this.SoftwareName
				}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 0.5f, PopupManager.PopupIDs.None, 1);
			}
		}
		this.Result.Collapsed = this.Collapsed;
		this.Result.Hidden = base.Hidden;
		this.Result.Priority = this.Priority;
		softwareAlpha.CheckCompetency();
		List<MarketingPlan> list = (from x in GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>()
		where x.TargetItem == this
		select x).ToList<MarketingPlan>();
		for (int i = 0; i < list.Count; i++)
		{
			if (this.AutoDev && list[i].Type == MarketingPlan.TaskType.PressRelease)
			{
				list[i].StopMarketing();
			}
			else
			{
				list[i].TargetItem = (SoftwareWorkItem)this.Result;
			}
		}
		this.MyCompany.WorkItems.Add(this.Result);
		this.Kill(false);
		return this.Result;
	}

	public override float StressMultiplier()
	{
		return 1f;
	}

	public override void Kill(bool wasCancelled = false)
	{
		List<MarketingPlan> list = (from x in GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>()
		where x.TargetItem == this
		select x).ToList<MarketingPlan>();
		for (int i = 0; i < list.Count; i++)
		{
			list[i].Kill(false);
		}
		GameSettings.Instance.FollowerSimulation.Remove(this);
		if (HUD.Instance.marketingWindow.TargetWork == this)
		{
			HUD.Instance.marketingWindow.Window.Close();
		}
		base.FixAutoDev();
		base.Kill(wasCancelled);
	}

	protected override void Cancelled()
	{
		base.Cancelled();
		if (base.Followers > 0f && this.ReleaseDate != null)
		{
			GameSettings.Instance.MyCompany.AddFans(-Mathf.CeilToInt(base.Followers * 0.75f), this._type, this._category);
		}
		if (this.contract != null)
		{
			ContractResult item = new ContractResult(this.contract, true, 0, 0f, Utilities.GetDaysFlat(this.DevStart, SDateTime.Now()), 0f);
			HUD.Instance.contractWindow.ContractResults.Items.Add(item);
		}
		if (base.ActiveDeal != null && this.AllZero())
		{
			HUD.Instance.dealWindow.CancelDeal(base.ActiveDeal, false);
			base.ActiveDeal = null;
		}
	}

	public override string GetWorkTypeName()
	{
		return "Design";
	}

	public override string HightlightButton()
	{
		return (!this.HasFinished) ? null : "Develop";
	}

	public override int EmitType(Actor actor)
	{
		return 2;
	}

	public int DoneWork;

	public WorkItem Result;

	public SHashSet<uint> EverWorked = new SHashSet<uint>();

	public bool HasFinished;

	public int WorkNeeded;
}
