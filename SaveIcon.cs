﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SaveIcon : MonoBehaviour
{
	private void Awake()
	{
		SaveIcon.Instance = this;
		base.gameObject.SetActive(false);
	}

	private void Update()
	{
		this.Icon.color = this.ColorAnimation.Evaluate(Time.time % this.ColorSpeed / this.ColorSpeed);
		float num = Time.time - this.BeginTime;
		this.Icon.color = new Color(this.Icon.color.r, this.Icon.color.g, this.Icon.color.b, this.Fade.Evaluate(num / this.LiveTime));
		if (num > this.LiveTime)
		{
			base.gameObject.SetActive(false);
		}
	}

	public static void Show(string name)
	{
		if (SaveIcon.Instance != null)
		{
			SaveIcon.Instance.Label.text = name;
			SaveIcon.Instance.BeginTime = Time.time;
			SaveIcon.Instance.gameObject.SetActive(true);
		}
	}

	private void OnDestroy()
	{
		if (SaveIcon.Instance == this)
		{
			SaveIcon.Instance = null;
		}
	}

	public static SaveIcon Instance;

	public Image Icon;

	private float BeginTime;

	public float LiveTime;

	public float ColorSpeed;

	public Text Label;

	public AnimationCurve Fade;

	public Gradient ColorAnimation;
}
