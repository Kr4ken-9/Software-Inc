﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CritterController : MonoBehaviour
{
	private void Awake()
	{
		if (CritterController.Instance != null)
		{
			UnityEngine.Object.Destroy(CritterController.Instance.gameObject);
		}
		CritterController.Instance = this;
		this.MaxCountDown = this.SpawnCountDown;
	}

	private void OnDestroy()
	{
		if (CritterController.Instance == this)
		{
			CritterController.Instance = null;
		}
	}

	public void PopulateCritter(string type, int count)
	{
		GameObject gameObject = this.CritterTypes.FirstOrDefault((GameObject x) => x.GetComponent<ICritter>().GetTypeName().Equals(type));
		if (gameObject != null)
		{
			for (int i = 0; i < count; i++)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(gameObject);
				gameObject2.SetActive(false);
				this.CritterPool.Add(gameObject2.GetComponent<ICritter>());
			}
		}
	}

	public static bool ShouldGoHome(ICritter c)
	{
		float temperature = TimeOfDay.Instance.Temperature;
		float lightLevel = TimeOfDay.LightLevel;
		return temperature < c.OptimalMinWeather() || temperature > c.OptimalMaxWeather() || lightLevel < c.OptimalMinLight() || lightLevel > c.OptimalMaxLight();
	}

	public void ActivateAll(bool activate)
	{
		for (int i = 0; i < this.ActiveCritters.Count; i++)
		{
			ICritter critter = this.ActiveCritters[i];
			critter.GetGameObject().SetActive(activate);
		}
	}

	private void Update()
	{
		this.SpawnCountDown -= Time.deltaTime * GameSettings.GameSpeed;
		if (this.SpawnCountDown < 0f)
		{
			this.SpawnCountDown = this.MaxCountDown;
			float temperature = TimeOfDay.Instance.Temperature;
			float lightLevel = TimeOfDay.LightLevel;
			for (int i = 0; i < this.CritterPool.Count; i++)
			{
				ICritter critter = this.CritterPool[i];
				if (temperature >= critter.OptimalMinWeather() && temperature <= critter.OptimalMaxWeather() && lightLevel >= critter.OptimalMinLight() && lightLevel <= critter.OptimalMaxLight())
				{
					critter.ResetPlace();
					critter.GetGameObject().SetActive(true);
					critter.Spawn();
					this.CritterPool.RemoveAt(i);
					this.ActiveCritters.Add(critter);
					break;
				}
			}
		}
		if (GameSettings.Instance.ActiveFloor > -1 != this.IsActive)
		{
			this.IsActive = (GameSettings.Instance.ActiveFloor > -1);
			this.ActivateAll(this.IsActive);
		}
		if (!this.IsActive)
		{
			return;
		}
		for (int j = 0; j < this.ActiveCritters.Count; j++)
		{
			ICritter critter2 = this.ActiveCritters[j];
			if (critter2.ShouldUpdate() && critter2.UpdateMe())
			{
				critter2.GetGameObject().SetActive(false);
				this.CritterPool.Add(critter2);
				this.ActiveCritters.RemoveAt(j);
				j--;
			}
		}
	}

	public GameObject[] CritterTypes;

	[NonSerialized]
	private List<ICritter> CritterPool = new List<ICritter>();

	[NonSerialized]
	private List<ICritter> ActiveCritters = new List<ICritter>();

	public float SpawnCountDown = 5f;

	[NonSerialized]
	private float MaxCountDown;

	public static CritterController Instance;

	private bool IsActive = true;
}
