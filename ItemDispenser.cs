﻿using System;
using System.Linq;
using UnityEngine;

public class ItemDispenser : MonoBehaviour
{
	private void Start()
	{
		ItemDispenser.Instance = this;
	}

	public GameObject Dispense(string name)
	{
		GameObject gameObject = this.Items.FirstOrDefault((GameObject x) => x.name.Equals(name));
		if (gameObject != null)
		{
			GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(gameObject);
			gameObject2.name = gameObject2.name.Replace("(Clone)", string.Empty);
			return gameObject2;
		}
		throw new UnityException("Tried to dispense non-existent item " + name);
	}

	private void Update()
	{
	}

	public static ItemDispenser Instance;

	public GameObject[] Items;
}
