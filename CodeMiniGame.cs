﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CodeMiniGame : MonoBehaviour
{
	private void Start()
	{
	}

	public bool ValidChar(char c, int pos)
	{
		for (int i = 0; i < this.CodeObjects.Count; i++)
		{
			if (pos < this.CodeObjects[i].Label.text.Length && this.CodeObjects[i].Label.Letters == pos && this.CodeObjects[i].Label.text[pos] == c)
			{
				return true;
			}
		}
		return false;
	}

	public void Show(SoftwareAlpha work)
	{
		this.PlayButtonText.text = "CodeGameHint".Loc();
		this.Multiplier = 1f;
		this.Work = work;
		WindowManager.HasModal = true;
		InputController.InputEnabled = false;
		this.PauseGame();
		this.MainCanvas.SetActive(true);
		this.InfoText.text = string.Format("{0}: {1}\n{2}: {3}x", new object[]
		{
			"Lives left".Loc(),
			this.Work.MiniGameLives,
			"Multiplier".Loc(),
			this.Multiplier.ToString("N2")
		});
	}

	public void Hide()
	{
		this.CloseMode = false;
		WindowManager.HasModal = (WindowManager.ModalWindows.Count > 0);
		InputController.InputEnabled = true;
		this.ClearBoard();
		GameSettings.GameSpeed = 0f;
		this.MainCanvas.SetActive(false);
	}

	public void PauseButtonClick()
	{
		if (this.CloseMode)
		{
			this.Hide();
		}
		else
		{
			this.PlayGame();
		}
	}

	public void PlayGame()
	{
		HUD.Instance.GameSpeed = 2;
		this.PauseButton.SetActive(false);
	}

	public void PauseGame()
	{
		HUD.Instance.GameSpeed = 0;
		this.PauseButton.SetActive(true);
	}

	public void ClearBoard()
	{
		for (int i = 0; i < this.CodeObjects.Count; i++)
		{
			this.UnusedWords.Add(this.CodeObjects[i].Label.text);
			UnityEngine.Object.Destroy(this.CodeObjects[i].gameObject);
		}
		this.CodeObjects.Clear();
		this.InputBox.text = string.Empty;
	}

	private void Update()
	{
		if (Input.GetKeyUp(KeyCode.Escape) || GameSettings.ForcePause)
		{
			this.Hide();
			return;
		}
		if (this.CloseMode)
		{
			if (GameSettings.GameSpeed > 0f)
			{
				this.Hide();
			}
			return;
		}
		if (this.Work == null || this.Work.GetProgress() >= 1f || this.Work.InDelay || this.Work.InBeta || this.Work.MiniGameLives <= 0)
		{
			this.CloseMode = true;
			this.PauseButton.SetActive(true);
			this.PlayButtonText.text = "CodeMiniGameEnd".Loc();
			GameSettings.GameSpeed = 0f;
			return;
		}
		this.WorkProgress.Value = this.Work.GetProgress();
		if (InputController.GetKeyDown(InputController.Keys.Pause, true))
		{
			this.PauseGame();
		}
		if (GameSettings.GameSpeed == 0f && (InputController.GetKeyDown(InputController.Keys.Speed1, true) || InputController.GetKeyDown(InputController.Keys.Speed2, true) || InputController.GetKeyDown(InputController.Keys.Speed3, true)))
		{
			this.PlayGame();
		}
		if (GameSettings.GameSpeed == 0f)
		{
			return;
		}
		this.InfoText.text = string.Format("{0}: {1}\n{2}: {3}x", new object[]
		{
			"Lives left".Loc(),
			this.Work.MiniGameLives,
			"Multiplier".Loc(),
			this.Multiplier.ToString("N2")
		});
		if (Input.inputString.Length > 0)
		{
			char c = Input.inputString.ToUpper()[0];
			if (this.ValidChar(c, this.InputBox.text.Length))
			{
				Text inputBox = this.InputBox;
				inputBox.text += Input.inputString.ToUpper()[0];
				for (int i = 0; i < this.CodeObjects.Count; i++)
				{
					this.CodeObjects[i].UpdateColor(this.InputBox.text);
				}
			}
			else
			{
				this.Multiplier = 1f;
			}
		}
		CodeObject codeObject = this.CodeObjects.FirstOrDefault((CodeObject x) => x.Label.Letters == x.Label.text.Length);
		if (codeObject != null)
		{
			this.InputBox.text = string.Empty;
			for (int j = 0; j < this.CodeObjects.Count; j++)
			{
				this.CodeObjects[j].UpdateColor(this.InputBox.text);
			}
			this.UnusedWords.Add(codeObject.Label.text);
			this.CodeObjects.Remove(codeObject);
			codeObject.DestroyMe();
			this.Work.AddQuality(0.001f * (float)codeObject.Label.text.Length * this.Multiplier, 1f, false);
			this.Multiplier += 0.01f;
		}
		for (int k = 0; k < this.CodeObjects.Count; k++)
		{
			this.CodeObjects[k].Rect.anchoredPosition += new Vector2(0f, -this.WordSpeed * Time.deltaTime);
			if (this.CodeObjects[k].Rect.anchoredPosition.y < -this.Rect.rect.height + this.CodeObjects[k].Label.preferredHeight)
			{
				this.InputBox.text = string.Empty;
				for (int l = 0; l < this.CodeObjects.Count; l++)
				{
					this.CodeObjects[l].UpdateColor(this.InputBox.text);
				}
				this.UnusedWords.Add(this.CodeObjects[k].Label.text);
				this.CodeObjects[k].DestroyMe();
				this.CodeObjects.RemoveAt(k);
				k--;
				this.Multiplier = 1f;
				this.Work.MiniGameLives--;
			}
		}
		this.wordTimer -= Time.deltaTime * (1f + this.Work.GetProgress());
		if (this.wordTimer <= 0f)
		{
			this.wordTimer = this.WordTime;
			if (this.UnusedWords.Count > 0)
			{
				int index = UnityEngine.Random.Range(0, this.UnusedWords.Count);
				string text = this.UnusedWords[index];
				this.UnusedWords.RemoveAt(index);
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.CodeObjectPrefab);
				CodeObject component = gameObject.GetComponent<CodeObject>();
				component.Label.text = text;
				component.transform.SetParent(base.transform);
				component.Rect.anchoredPosition = new Vector2(UnityEngine.Random.Range(component.Label.preferredWidth / 2f, this.Rect.rect.width - component.Label.preferredWidth / 2f), 24f);
				this.CodeObjects.Add(component);
			}
		}
	}

	public Text InputBox;

	public GameObject CodeObjectPrefab;

	public RectTransform Rect;

	public float WordTime;

	public float WordSpeed;

	private float wordTimer;

	[NonSerialized]
	public List<string> UnusedWords = new List<string>
	{
		"PUBLIC",
		"PRIVATE",
		"STATIC",
		"VOID",
		"BOOL",
		"BYTE",
		"FLOAT",
		"CLASS",
		"SERIALIZATION",
		"ANONYMOUS",
		"VIRTUAL",
		"INTERFACE",
		"ABSTRACT",
		"BREAK",
		"WHILE",
		"SWITCH",
		"CASE",
		"CONTINUE",
		"HASH",
		"TAG",
		"STRING",
		"ARRAY",
		"SELECT",
		"FROM",
		"VECTOR",
		"MATRIX",
		"WHERE",
		"ORDER",
		"GROUP",
		"DISTINCT",
		"UNION",
		"END",
		"GOTO",
		"BEGIN",
		"ELSE",
		"DOUBLE",
		"LONG",
		"DECIMAL"
	};

	[NonSerialized]
	public List<CodeObject> CodeObjects = new List<CodeObject>();

	public GameObject MainCanvas;

	[NonSerialized]
	public SoftwareAlpha Work;

	public GUIProgressBar WorkProgress;

	public Text InfoText;

	public float Multiplier = 1f;

	public GameObject PauseButton;

	public Text PlayButtonText;

	public bool CloseMode;
}
