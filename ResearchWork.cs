﻿using System;
using UnityEngine;

[Serializable]
public class ResearchWork : WorkItem
{
	public ResearchWork(string name) : base(name, null, -1)
	{
	}

	public ResearchWork()
	{
	}

	public ResearchWork(string feat, string sw) : base("Researching".Loc(), null, -1)
	{
		this.Feature = feat;
		this.Software = sw;
		Feature feature = GameSettings.Instance.SoftwareTypes[this.Software].Features[this.Feature];
		this.CodeCat = feature.GetCategory(true);
		this.ArtCat = feature.GetCategory(false);
		float num = feature.DevTime * 12f;
		this.ArtMax = (1f - feature.CodeArtRatio) * num / 2f;
		this.CodeMax = feature.CodeArtRatio * num / 2f;
		this.DesignMax = num / 2f;
		this.UpgFactor = Mathf.Sqrt(1f + 0.02f / feature.DevTime);
	}

	public Feature FeatureRef
	{
		get
		{
			if (this._featureRef == null)
			{
				this._featureRef = GameSettings.Instance.SoftwareTypes[this.Software].Features[this.Feature];
			}
			return this._featureRef;
		}
	}

	public override Color BackColor
	{
		get
		{
			return new Color(0.95f, 0.4f, 0.13f);
		}
	}

	public override string GetWorkTypeName()
	{
		return "Research";
	}

	public void UpdateValid()
	{
		if (this.FeatureRef.Researched)
		{
			HUD.Instance.AddPopupMessage(string.Format("PatentTooSlow".Loc(), this.Software.LocSW(), this.Feature.SWFeat(this.Software)), "Info", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.None, 6);
			this.Kill(false);
		}
	}

	private void UpdateSkill(Employee employee, float delta)
	{
		if (employee.IsRole(Employee.RoleBit.Programmer))
		{
			employee.AddToSpecialization(Employee.EmployeeRole.Programmer, this.FeatureRef.GetCategory(true), this.CodeMax, delta, false);
		}
		if (employee.IsRole(Employee.RoleBit.Artist))
		{
			employee.AddToSpecialization(Employee.EmployeeRole.Artist, this.FeatureRef.GetCategory(false), this.ArtMax, delta, false);
		}
		if (employee.IsRole(Employee.RoleBit.Designer))
		{
			employee.AddToSpecialization(Employee.EmployeeRole.Designer, this.FeatureRef.GetCategory(true), this.DesignMax, delta, false);
		}
	}

	private float ModEffectiveness(Actor ac, float eff)
	{
		float num = 0f;
		float num2 = 0f;
		if (ac.employee.IsRole(Employee.RoleBit.Programmer))
		{
			num += ac.GetPCAddonBonus(Employee.EmployeeRole.Programmer);
			num2 += 1f;
		}
		if (ac.employee.IsRole(Employee.RoleBit.Artist))
		{
			num += ac.GetPCAddonBonus(Employee.EmployeeRole.Artist);
			num2 += 1f;
		}
		if (ac.employee.IsRole(Employee.RoleBit.Designer))
		{
			num += ac.GetPCAddonBonus(Employee.EmployeeRole.Designer);
			num2 += 1f;
		}
		if (ac.employee.IsRole(Employee.RoleBit.Lead))
		{
			num += ac.GetPCAddonBonus(Employee.EmployeeRole.Lead);
			num2 += 1f;
		}
		return (num2 != 0f) ? (eff * (num / num2)) : 1f;
	}

	public override void DoWork(Actor actor, float effect, float delta)
	{
		effect = Utilities.PerDay(effect * this.ModEffectiveness(actor, effect) * ((!actor.employee.IsRole(Employee.RoleBit.Lead)) ? 1f : 0.5f), delta, true);
		if (this.LastDate.Equals(SDateTime.Now(), true))
		{
			this.LastSpeedBoost = Mathf.Pow(this.UpgFactor, Utilities.GetMonths(new SDateTime(this.FeatureRef.Unlock.Value), SDateTime.Now()));
		}
		effect *= this.LastSpeedBoost;
		float num = 3f;
		bool flag = false;
		bool flag2 = false;
		bool flag3 = false;
		if (this.ArtProgress >= this.ArtMax || !actor.employee.IsRole(Employee.RoleBit.Artist))
		{
			num -= 1f;
		}
		else
		{
			flag = true;
		}
		if (this.CodeProgress >= this.CodeMax || !actor.employee.IsRole(Employee.RoleBit.Programmer))
		{
			num -= 1f;
		}
		else
		{
			flag2 = true;
		}
		if (this.DesignProgress >= this.DesignMax || !actor.employee.IsRole(Employee.RoleBit.Designer))
		{
			num -= 1f;
		}
		else
		{
			flag3 = true;
		}
		if (!flag && !flag3 && !flag2)
		{
			return;
		}
		if (flag3)
		{
			this.DesignProgress = Mathf.Min(this.DesignMax, this.DesignProgress + effect * actor.employee.GetSpecialization(Employee.EmployeeRole.Designer, this.CodeCat, false) / num);
			base.RecordSkill(Employee.EmployeeRole.Designer, actor, delta / num);
		}
		if (flag2)
		{
			this.CodeProgress = Mathf.Min(this.CodeMax, this.CodeProgress + effect * actor.employee.GetSpecialization(Employee.EmployeeRole.Programmer, this.CodeCat, false) / num);
			base.RecordSkill(Employee.EmployeeRole.Programmer, actor, delta / num);
		}
		if (flag)
		{
			this.ArtProgress = Mathf.Min(this.ArtMax, this.ArtProgress + effect * actor.employee.GetSpecialization(Employee.EmployeeRole.Artist, this.ArtCat, false) / num);
			base.RecordSkill(Employee.EmployeeRole.Artist, actor, delta / num);
		}
		this.UpdateSkill(actor.employee, delta * ((!actor.employee.IsRole(Employee.RoleBit.Lead)) ? 1f : 0.5f));
	}

	public override Employee.EmployeeRole? GetBoostRole(Actor act)
	{
		bool flag = act.employee.IsRole(Employee.RoleBit.Designer);
		bool flag2 = act.employee.IsRole(Employee.RoleBit.Programmer);
		bool flag3 = act.employee.IsRole(Employee.RoleBit.Artist);
		if (flag)
		{
			if (flag2)
			{
				if (flag3)
				{
					float value = UnityEngine.Random.value;
					if (value < 0.5f)
					{
						flag2 = false;
						flag3 = false;
					}
					else if (value < 0.75f)
					{
						flag = false;
						flag3 = false;
					}
					else
					{
						flag2 = false;
						flag = false;
					}
				}
				else
				{
					float value2 = UnityEngine.Random.value;
					if (value2 < 0.75f)
					{
						flag2 = false;
					}
					else
					{
						flag = false;
					}
				}
			}
			else
			{
				if (!flag3)
				{
					return new Employee.EmployeeRole?(Employee.EmployeeRole.Designer);
				}
				float value3 = UnityEngine.Random.value;
				if (value3 < 0.75f)
				{
					flag3 = false;
				}
				else
				{
					flag = false;
				}
			}
		}
		else if (flag3)
		{
			if (!flag2)
			{
				return new Employee.EmployeeRole?(Employee.EmployeeRole.Artist);
			}
			float value4 = UnityEngine.Random.value;
			if (value4 < 0.5f)
			{
				flag3 = false;
			}
			else
			{
				flag2 = false;
			}
		}
		else if (flag2)
		{
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Programmer);
		}
		if (flag2)
		{
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Programmer);
		}
		if (flag3)
		{
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Artist);
		}
		if (flag)
		{
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Designer);
		}
		return null;
	}

	public override WorkItem.HasWorkReturn HasWork(Actor actor)
	{
		bool flag = false;
		bool flag2 = false;
		if (actor.employee.IsRole(Employee.RoleBit.Designer) && this.HasSkill(actor, true, false, false))
		{
			flag = true;
			if (this.DesignProgress < this.DesignMax)
			{
				flag2 = true;
			}
		}
		if (!flag2 && actor.employee.IsRole(Employee.RoleBit.Programmer) && this.HasSkill(actor, false, true, false))
		{
			flag = true;
			if (this.CodeProgress < this.CodeMax)
			{
				flag2 = true;
			}
		}
		if (!flag2 && actor.employee.IsRole(Employee.EmployeeRole.Artist) && this.HasSkill(actor, false, false, true))
		{
			flag = true;
			if (this.ArtProgress < this.ArtMax)
			{
				flag2 = true;
			}
		}
		if (flag2)
		{
			return WorkItem.HasWorkReturn.True;
		}
		return (!flag) ? WorkItem.HasWorkReturn.NotApplicable : WorkItem.HasWorkReturn.Finished;
	}

	public bool HasSkill(Actor act, bool design, bool code, bool art)
	{
		return (design && act.employee.GetSpecialization(Employee.EmployeeRole.Designer, this.CodeCat, false) > 0f) || (code && this.CodeMax > 0f && act.employee.GetSpecialization(Employee.EmployeeRole.Programmer, this.CodeCat, false) > 0f) || (art && this.ArtMax > 0f && act.employee.GetSpecialization(Employee.EmployeeRole.Artist, this.ArtCat, false) > 0f);
	}

	public void PatentNow()
	{
		if (this.GetProgress() == 1f)
		{
			Feature feature = GameSettings.Instance.SoftwareTypes[this.Software].Features[this.Feature];
			if (!feature.Researched)
			{
				feature.TransferPatent(GameSettings.Instance.MyCompany);
			}
			this.Kill(false);
		}
	}

	public override float StressMultiplier()
	{
		return 0.5f;
	}

	public override string Category()
	{
		return this.Software.LocSW() + ": " + this.Feature.SWFeat(this.Software);
	}

	public override string CurrentStage()
	{
		float progress = this.GetProgress();
		if (progress < 0.25f)
		{
			return "ResearchPhase1".Loc();
		}
		if (progress < 0.75f)
		{
			return "ResearchPhase2".Loc();
		}
		return "ResearchPhase3".Loc();
	}

	public override string GetIcon()
	{
		return "Research";
	}

	public override float GetProgress()
	{
		return (this.ArtProgress + this.DesignProgress + this.CodeProgress) / (this.ArtMax + this.DesignMax + this.CodeMax);
	}

	public override string HightlightButton()
	{
		return (this.GetProgress() != 1f) ? null : "Patent";
	}

	public override int EmitType(Actor actor)
	{
		if (actor.employee.IsRole(Employee.RoleBit.Designer))
		{
			return 2;
		}
		return (!actor.employee.IsRole(Employee.EmployeeRole.Artist)) ? 0 : 1;
	}

	public float DesignMax;

	public float DesignProgress;

	public float ArtMax;

	public float ArtProgress;

	public float CodeMax;

	public float CodeProgress;

	public float UpgFactor = 1f;

	public string Software;

	public string Feature;

	public string CodeCat;

	public string ArtCat;

	private SDateTime LastDate = new SDateTime(0);

	private float LastSpeedBoost = 1f;

	[NonSerialized]
	private Feature _featureRef;
}
