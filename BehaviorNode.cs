﻿using System;

public class BehaviorNode<T>
{
	public BehaviorNode(string name, Func<T, int> run, bool atomic, int id = -1)
	{
		this.Name = name;
		this.Run = run;
		this.Atomic = atomic;
		this.SpecialID = id;
	}

	public string Name;

	public BehaviorNode<T> Success;

	public BehaviorNode<T> Failure;

	public bool Atomic;

	public Func<T, int> Run;

	public int SpecialID;
}
