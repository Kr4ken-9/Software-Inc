﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SnapPoint : MonoBehaviour
{
	public Furniture UsedBy
	{
		get
		{
			return this._usedBy;
		}
		set
		{
			Furniture usedBy = this._usedBy;
			this._usedBy = value;
			if (usedBy != null)
			{
				this.Parent.NotifySnapPoint(this, usedBy, false);
			}
			if (this._usedBy != null)
			{
				this.Parent.NotifySnapPoint(this, this._usedBy, true);
			}
		}
	}

	private void Awake()
	{
		if (this.InitLinks != null)
		{
			for (int i = 0; i < this.InitLinks.Length; i++)
			{
				SnapPoint snapPoint = this.InitLinks[i];
				this.Links.Add(snapPoint);
				snapPoint.Links.Add(this);
			}
		}
	}

	public Vector3 GetRealPos()
	{
		return Matrix4x4.TRS(this.Parent.OriginalPosition, this.Parent.transform.rotation, Vector3.one).MultiplyPoint(base.transform.localPosition);
	}

	public void UpdateValid(bool threaded)
	{
		if (this.Parent != null && this.Parent.Parent != null)
		{
			Vector3 vector = (!threaded) ? this.GetRealPos() : this.pos;
			this.IsValid = (!this.CheckValid || FurnitureBuilder.IsValid(new Vector2[]
			{
				new Vector2(vector.x, vector.z)
			}, vector.y % 2f - 0.001f, vector.y % 2f + 0.001f, this.Parent.Parent, this.Parent.WallFurn, new Furniture[]
			{
				this.Parent
			}));
		}
		else
		{
			this.IsValid = false;
		}
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		if (this.InitLinks == null || this.InitLinks.Length == 0)
		{
			bool flag = false;
			foreach (SnapPoint snapPoint in this.Parent.SnapPoints)
			{
				if (snapPoint != this && snapPoint.InitLinks != null && snapPoint.InitLinks.Contains(this))
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				Gizmos.DrawCube(base.transform.position, Vector3.one * 0.1f);
			}
		}
		Gizmos.color = Color.white;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.white;
		if (this.InitLinks != null && this.InitLinks.Length > 0)
		{
			for (int i = 0; i < this.InitLinks.Length; i++)
			{
				Gizmos.DrawLine(base.transform.position, this.InitLinks[i].transform.position);
			}
		}
		else
		{
			foreach (SnapPoint snapPoint in this.Links)
			{
				Gizmos.DrawLine(base.transform.position, snapPoint.transform.position);
			}
		}
	}

	public string Name;

	public int Id;

	public bool CheckValid = true;

	public SnapPoint[] InitLinks;

	[NonSerialized]
	public HashSet<SnapPoint> Links = new HashSet<SnapPoint>();

	private Furniture _usedBy;

	public Furniture Parent;

	public bool IsValid = true;

	[NonSerialized]
	public Vector3 pos;
}
