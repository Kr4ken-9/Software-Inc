﻿using System;

public class PathNode<T>
{
	public PathNode(T p, object t)
	{
		this.Point = p;
		this.Tag = t;
	}

	public PathNode(PathNode<T> pathNode)
	{
		this.Point = pathNode.Point;
		this.Tag = pathNode.Tag;
		this.Connections.AddRange(pathNode.Connections);
	}

	public object Tag2
	{
		get
		{
			return this._tag2 ?? this.Tag;
		}
		set
		{
			this._tag2 = value;
		}
	}

	public override int GetHashCode()
	{
		return this.Point.GetHashCode();
	}

	public void AddConnection(PathNode<T> target)
	{
		this.Connections.Add(target);
	}

	public void RemoveConnection(PathNode<T> target)
	{
		this.Connections.Remove(target);
	}

	public void RemoveConnection(object tag)
	{
		this.Connections.RemoveAll((PathNode<T> x) => x.Tag == tag);
	}

	public SHashSet<PathNode<T>> GetConnections()
	{
		return this.Connections;
	}

	public T Point;

	private object _tag2;

	public object Tag;

	private SHashSet<PathNode<T>> Connections = new SHashSet<PathNode<T>>();

	public bool NullWeight;

	public float Weight = 1f;
}
