﻿using System;
using Steamworks;

public class DummyWorkshopItem : IWorkshopItem
{
	public DummyWorkshopItem(string type, string path, string itemTitle, PublishedFileId_t? steamID)
	{
		this.Type = type;
		this.Path = path;
		this.ItemTitle = itemTitle;
		this.SteamID = steamID;
	}

	public string ItemTitle { get; set; }

	public PublishedFileId_t? SteamID { get; set; }

	public bool SetTitle { get; set; }

	public bool CanUpload
	{
		get
		{
			return false;
		}
		set
		{
		}
	}

	public string GetWorkshopType()
	{
		return this.Type;
	}

	public string FolderPath()
	{
		return this.Path;
	}

	public string[] GetValidExts()
	{
		return new string[0];
	}

	public string[] ExtraTags()
	{
		return new string[0];
	}

	public string GetThumbnail()
	{
		return string.Empty;
	}

	public string Type;

	public string Path;
}
