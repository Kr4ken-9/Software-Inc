﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ColorWindow : MonoBehaviour
{
	public void UpdateHSMode()
	{
		Options.ColorBrightnessSlider = this.HSMode.isOn;
		this.ColorMapImg.material = ((!this.HSMode.isOn) ? this.SBMat : null);
		this.SliderBack.sprite = ((!this.HSMode.isOn) ? this.Hue : this.Brightness);
		if (!this.HSMode.isOn)
		{
			this.SliderBack.color = Color.white;
		}
		this.SetColor(this.MainColor.color);
	}

	public void Init(string[] tabs, IList<Action<Color>> actions, Color[] colors, Color[] Defaults = null, Action extraClose = null)
	{
		this.HSMode.isOn = Options.ColorBrightnessSlider;
		this.ExtraClose = extraClose;
		this.ColorActions = actions.Take(tabs.Length).ToArray<Action<Color>>();
		this.Changed = new bool[tabs.Length];
		this.Colors = colors.Take(tabs.Length).ToArray<Color>();
		int l = tabs.Length;
		Color[] c = (Color[])colors.Clone();
		this.Window.OnClose = delegate
		{
			for (int j = 0; j < l; j++)
			{
				if (this.Changed[j])
				{
					this.ColorActions[j](c[j]);
				}
			}
			ColorWindow.Open = false;
			if (this.ExtraClose != null)
			{
				this.ExtraClose();
			}
		};
		this.SetColor(this.Colors[this.ActiveTab]);
		if (Defaults != null && Defaults.Length > 0)
		{
			foreach (Color col2 in Defaults.Distinct<Color>())
			{
				Color col = col2;
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
				gameObject.transform.SetParent(this.ColorButtonPanel.transform, false);
				gameObject.GetComponent<Image>().color = col;
				gameObject.GetComponent<Button>().onClick.AddListener(delegate
				{
					this.SetColor(col);
				});
			}
		}
		else
		{
			this.ColorButtonPanel.SetActive(false);
			RectTransform component = this.Window.GetComponent<RectTransform>();
			component.sizeDelta -= new Vector2(40f, 0f);
			this.ApplyButton.sizeDelta = this.ApplyButton.sizeDelta - new Vector2(40f, 0f);
			this.ApplyButton.anchoredPosition = this.ApplyButton.anchoredPosition - new Vector2(20f, 0f);
		}
		if (tabs.Length > 1)
		{
			for (int i = 0; i < tabs.Length; i++)
			{
				int k = i;
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.TogglePrefab);
				gameObject2.transform.SetParent(this.TabButtonPanel.transform, false);
				gameObject2.GetComponentInChildren<Text>().text = tabs[i];
				Toggle component2 = gameObject2.GetComponent<Toggle>();
				component2.isOn = (i == 0);
				component2.onValueChanged.AddListener(delegate(bool x)
				{
					this.ChangeTab(k);
				});
				component2.group = this.TabButtonPanel.GetComponent<ToggleGroup>();
			}
		}
		else
		{
			this.TabButtonPanel.SetActive(false);
			RectTransform component3 = this.Window.GetComponent<RectTransform>();
			component3.sizeDelta = new Vector2(component3.sizeDelta.x, component3.sizeDelta.y - 32f);
			Vector2 anchoredPosition = this.MainPanel.anchoredPosition;
			this.MainPanel.anchoredPosition = anchoredPosition + new Vector2(0f, 32f);
		}
		this.Initializing = false;
		ColorWindow.Open = true;
		this.UpdateHSMode();
	}

	public void ChangeTab(int newTab)
	{
		this.ActiveTab = newTab;
		this.Initializing = true;
		this.SetColor(this.Colors[this.ActiveTab]);
		this.Initializing = false;
	}

	public void SetHex()
	{
		if (!this._hexChange)
		{
			this._hexChange = true;
			this.HexText.text = ColorUtility.ToHtmlStringRGB(this.MainColor.color);
			this._hexChange = false;
		}
	}

	public void HexUpdate()
	{
		if (!this.Initializing && !this._hexChange)
		{
			this._hexChange = true;
			Color color;
			if (ColorUtility.TryParseHtmlString("#" + this.HexText.text, out color))
			{
				this.SetColor(color);
			}
			this._hexChange = false;
		}
	}

	private void SetColor(Color color)
	{
		Vector3 vector = Utilities.RGBToHSV(color);
		if (this.HSMode.isOn)
		{
			this.DarknessSlider.value = 1f - vector.z;
			this.ColorPoint.anchoredPosition = new Vector3((1f - vector.y) * 256f, (!float.IsNaN(vector.x)) ? (-vector.x * 256f) : 0f);
		}
		else
		{
			this.DarknessSlider.value = ((!float.IsNaN(vector.x)) ? vector.x : 0f);
			this.ColorPoint.anchoredPosition = new Vector3(vector.z * 256f, -vector.y * 256f);
		}
		this.UpdateColor();
	}

	public void ColorMapClick()
	{
		Vector2 anchoredPosition = new Vector2(Input.mousePosition.x / Options.UISize - this.ColorMap.position.x / Options.UISize + 128f, Input.mousePosition.y / Options.UISize - this.ColorMap.position.y / Options.UISize - 128f);
		anchoredPosition = new Vector2(Mathf.Clamp(anchoredPosition.x, 0f, 256f), Mathf.Clamp(anchoredPosition.y, -256f, 0f));
		this.ColorPoint.anchoredPosition = anchoredPosition;
		this.UpdateColor();
	}

	public void UpdateColor()
	{
		Vector3 vector = (!this.HSMode.isOn) ? Utilities.HSVToRGB(this.DarknessSlider.value * 360f, -this.ColorPoint.anchoredPosition.y / 256f, this.ColorPoint.anchoredPosition.x / 256f) : Utilities.HSVToRGB(-this.ColorPoint.anchoredPosition.y / 256f * 360f, 1f - this.ColorPoint.anchoredPosition.x / 256f, 1f - this.DarknessSlider.value);
		this.MainColor.color = new Color(vector.x, vector.y, vector.z);
		Vector3 vector2 = (!this.HSMode.isOn) ? Utilities.HSVToRGB(this.DarknessSlider.value * 360f, 1f, 1f) : Utilities.HSVToRGB(-this.ColorPoint.anchoredPosition.y / 256f * 360f, 1f - this.ColorPoint.anchoredPosition.x / 256f, 1f);
		Color color = new Color(vector2.x, vector2.y, vector2.z, 1f);
		this.SetHex();
		if (this.HSMode.isOn)
		{
			this.ColorMapImg.color = new Color(1f - this.DarknessSlider.value, 1f - this.DarknessSlider.value, 1f - this.DarknessSlider.value, 1f);
			this.SliderBack.color = color;
		}
		else
		{
			this.ColorMapImg.material.color = color;
		}
		if (!this.Initializing)
		{
			this.Changed[this.ActiveTab] = true;
			this.Colors[this.ActiveTab] = this.MainColor.color;
			this.ColorActions[this.ActiveTab](this.Colors[this.ActiveTab]);
		}
	}

	public void Apply()
	{
		ColorWindow.Open = false;
		this.Window.OnClose = null;
		if (this.ExtraClose != null)
		{
			this.ExtraClose();
		}
		for (int i = 0; i < this.Colors.Length; i++)
		{
			if (this.Changed[i])
			{
				this.ColorActions[i](this.Colors[i]);
			}
		}
		UnityEngine.Object.Destroy(this.Window.gameObject);
	}

	public GUIWindow Window;

	public Slider DarknessSlider;

	public RectTransform ColorPoint;

	public RectTransform ColorMap;

	public Image MainColor;

	public Image ColorMapImg;

	public Image SliderBack;

	public Color[] Colors;

	public bool[] Changed;

	public GameObject ButtonPrefab;

	public GameObject TogglePrefab;

	public GameObject ColorButtonPanel;

	public GameObject TabButtonPanel;

	public InputField HexText;

	public RectTransform MainPanel;

	public RectTransform ApplyButton;

	public int ActiveTab;

	[NonSerialized]
	private Action ExtraClose;

	private bool Initializing = true;

	public static bool Open;

	public Material SBMat;

	public Sprite Brightness;

	public Sprite Hue;

	public Toggle HSMode;

	private Action<Color>[] ColorActions;

	private bool _hexChange;
}
