﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class AutoDevWorkItem : WorkItem
{
	public AutoDevWorkItem()
	{
	}

	public AutoDevWorkItem(string name) : base(name, null, -1)
	{
	}

	public AutoDevWorkItem(string name, Actor leader) : base(name, null, -1)
	{
		this.Leader = leader;
	}

	public Actor Leader
	{
		get
		{
			return this._leader;
		}
		set
		{
			if (value == null && this._leader == null)
			{
				return;
			}
			if (value != null && value.DID == this.LeaderID)
			{
				this._leader = value;
				if (!this._leader.AutoDevs.Contains(this))
				{
					this._leader.AutoDevs.Add(this);
				}
				return;
			}
			if (this._leader != null)
			{
				this._leader.AutoDevs.Remove(this);
				this._lastLeader = this._leader.DID;
			}
			this.LeaderID = ((!(value == null)) ? value.DID : 0u);
			this._leader = value;
			if (this._leader != null)
			{
				if (this._leader.DID != this._lastLeader)
				{
					this.DevActions = 0f;
					this.MarketingActions = 0f;
					this.DesignActions = 0f;
				}
				if (!this._leader.AutoDevs.Contains(this))
				{
					this._leader.AutoDevs.Add(this);
				}
			}
		}
	}

	public SoftwareProduct GetPastRelease(int i)
	{
		return GameSettings.Instance.simulation.GetProduct(this.PastReleases[i], false);
	}

	public void AddPastRelease(SoftwareProduct p)
	{
		for (int i = 0; i < this.PastReleases.Count; i++)
		{
			if (this.GetPastRelease(i).GetLatestSuccessor() == p)
			{
				this.PastReleases.RemoveAt(i);
				i--;
			}
		}
		this.PastReleases.Add(p.ID);
	}

	public SoftwareProduct GetValidSequel()
	{
		if (this.PastReleases.Count == 0)
		{
			return null;
		}
		for (int i = this.PastReleases.Count - 1; i > -1; i--)
		{
			SoftwareProduct latestSuccessor = this.GetPastRelease(i).GetLatestSuccessor();
			if (GameSettings.Instance.CanMakeSequel(latestSuccessor, GameSettings.Instance.MyCompany))
			{
				return latestSuccessor;
			}
			if (this.SingleIP)
			{
				return null;
			}
		}
		return null;
	}

	public SoftwareProduct GetLatestRelease()
	{
		return (this.PastReleases.Count <= 0) ? null : this.GetPastRelease(this.PastReleases.Count - 1);
	}

	public float MaxActions()
	{
		if (this.Leader == null)
		{
			return 0f;
		}
		return Mathf.Floor(this.Leader.employee.GetSkill(Employee.EmployeeRole.Lead).MapRange(0f, 1f, 3f, 10f, false));
	}

	public void RefreshTeams()
	{
		foreach (AutoDevWorkItem.AutoDevItem autoDevItem in this.Items)
		{
			if (!autoDevItem.Queued)
			{
				if (autoDevItem.Alpha == null)
				{
					this.AssignTeams(autoDevItem.Document, this.DesignTeams);
				}
				else
				{
					this.AssignTeams(autoDevItem.Alpha, (!autoDevItem.Secondary) ? this.SDevTeams : this.SecondaryDevTeams);
				}
			}
		}
	}

	public override Color BackColor
	{
		get
		{
			return new Color(0f, 0.75f, 0.75f);
		}
	}

	public override string GetIcon()
	{
		return "Recycle";
	}

	public HashSet<Team> GetTeams(HashSet<string> names)
	{
		HashSet<Team> hashSet = new HashSet<Team>();
		foreach (string key in names)
		{
			Team item = null;
			if (GameSettings.Instance.sActorManager.Teams.TryGetValue(key, out item))
			{
				hashSet.Add(item);
			}
		}
		return hashSet;
	}

	public void AssignTeams(WorkItem item, HashSet<string> teams)
	{
		HashSet<Team> teams2 = this.GetTeams(teams);
		foreach (Team team in item.GetDevTeams())
		{
			if (!teams2.Contains(team))
			{
				item.RemoveDevTeam(team);
			}
		}
		foreach (Team team2 in teams2)
		{
			item.AddDevTeam(team2, false);
		}
	}

	public override WorkItem.HasWorkReturn HasWork(Actor actor)
	{
		if (actor == this.Leader)
		{
			float num = this.MaxActions();
			return (this.DesignActions >= num && this.DevActions >= num && this.MarketingActions >= num) ? WorkItem.HasWorkReturn.Waiting : WorkItem.HasWorkReturn.True;
		}
		return WorkItem.HasWorkReturn.Ignore;
	}

	public override void DoWork(Actor actor, float effectiveness, float delta)
	{
		if (this.Leader != null)
		{
			effectiveness *= actor.GetPCAddonBonus(Employee.EmployeeRole.Lead);
			float a = this.MaxActions();
			this.DesignActions = Mathf.Min(a, this.DesignActions + Utilities.PerDay(effectiveness * this.Leader.employee.GetSkill(Employee.EmployeeRole.Designer).MapRange(0f, 1f, 0.25f, 1f, false) * 8f, delta, true));
			this.DevActions = Mathf.Min(a, this.DevActions + Utilities.PerDay(effectiveness * this.Leader.employee.GetSkill(Employee.EmployeeRole.Programmer).MapRange(0f, 1f, 0.25f, 1f, false) * 8f, delta, true));
			this.MarketingActions = Mathf.Min(a, this.MarketingActions + Utilities.PerDay(effectiveness * this.Leader.employee.GetSkill(Employee.EmployeeRole.Marketer).MapRange(0f, 1f, 0.25f, 1f, false) * 8f, delta, true));
		}
	}

	public override float GetWorkBoost(Employee.EmployeeRole role, float currentSkil)
	{
		return 1f;
	}

	public override Employee.EmployeeRole? GetBoostRole(Actor act)
	{
		return new Employee.EmployeeRole?(Employee.EmployeeRole.Lead);
	}

	public override int EmitType(Actor actor)
	{
		return 2;
	}

	public bool CanDoAction(ref float points, float cost)
	{
		if (points >= cost)
		{
			points -= cost;
			return true;
		}
		return false;
	}

	public SDateTime GetCutDate(AutoDevWorkItem.AutoDevItem item, float progress)
	{
		return item.ActualStartDate + (item.MonthsToSpend * progress - item.AlreadyDev);
	}

	private void RefreshHype(AutoDevWorkItem.AutoDevItem item)
	{
		if (this.Hype)
		{
			if ((item.Hype == null || (item.Hype != null && item.Hype.Done)) && this.MarketingActions > 1f)
			{
				MarketingPlan marketingPlan = GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>().FirstOrDefault((MarketingPlan x) => x.Type == MarketingPlan.TaskType.Hype && x.TargetItem == item.SWWorkItem);
				if (marketingPlan != null)
				{
					item.Hype = marketingPlan;
				}
				else
				{
					SoftwareWorkItem softwareWorkItem = item.Alpha ?? item.Document;
					item.Hype = new MarketingPlan(softwareWorkItem, MarketingPlan.TaskType.Hype, 0f, 0f, (!(softwareWorkItem.guiItem == null)) ? (softwareWorkItem.guiItem.transform.GetSiblingIndex() + 1) : -1);
					item.Hype.AutoDev = true;
					item.Hype.Hidden = true;
					this.AssignTeams(item.Hype, this.MarketingTeams);
					GameSettings.Instance.MyCompany.WorkItems.Add(item.Hype);
					this.MarketingActions -= 1f;
				}
			}
		}
		else if (item.Hype != null)
		{
			item.Hype.Kill(false);
			item.Hype = null;
		}
	}

	public void AssignProject(SoftwareWorkItem doc)
	{
		doc.AutoDev = true;
		doc.Hidden = true;
		GameSettings.Instance.MyCompany.WorkItems.Add(doc);
		AutoDevWorkItem.AutoDevItem item = new AutoDevWorkItem.AutoDevItem(doc, this);
		this.Items.Add(item);
	}

	public override void PauseChange()
	{
		for (int i = 0; i < this.Items.Count; i++)
		{
			AutoDevWorkItem.AutoDevItem autoDevItem = this.Items[i];
			if (!autoDevItem.Queued)
			{
				if (base.Paused)
				{
					autoDevItem.AlreadyDev += Utilities.GetMonths(autoDevItem.ActualStartDate, SDateTime.Now());
				}
				else
				{
					autoDevItem.ActualStartDate = SDateTime.Now();
				}
				autoDevItem.SWWorkItem.Paused = base.Paused;
				if (autoDevItem.Hype != null)
				{
					autoDevItem.Hype.Paused = base.Paused;
				}
			}
		}
		for (int j = 0; j < this.MarketingItems.Count; j++)
		{
			this.MarketingItems[j].Paused = base.Paused;
		}
		for (int k = 0; k < this.SupportItems.Count; k++)
		{
			this.SupportItems[k].Paused = base.Paused;
		}
	}

	private void SetDev(AutoDevWorkItem.AutoDevItem item, bool value, ref bool primary, ref bool secondary)
	{
		if (item.Secondary)
		{
			secondary = value;
		}
		else
		{
			primary = value;
		}
	}

	private SDateTime? GetNextReleaseDate(AutoDevWorkItem.AutoDevItem exclude)
	{
		SDateTime? result = null;
		for (int i = 0; i < this.Items.Count; i++)
		{
			AutoDevWorkItem.AutoDevItem autoDevItem = this.Items[i];
			if (autoDevItem != exclude && !autoDevItem.Queued)
			{
				SDateTime? releaseDate = autoDevItem.ReleaseDate;
				if (releaseDate != null && (result == null || result.Value > releaseDate.Value))
				{
					result = releaseDate;
				}
			}
		}
		return result;
	}

	public void Update()
	{
		if (this.Leader == null || base.Paused)
		{
			return;
		}
		bool flag = false;
		bool flag2 = false;
		bool flag3 = false;
		for (int i = 0; i < this.Items.Count; i++)
		{
			AutoDevWorkItem.AutoDevItem autoDevItem = this.Items[i];
			if (!autoDevItem.Queued)
			{
				if (autoDevItem.Alpha != null)
				{
					bool flag4 = false;
					this.SetDev(autoDevItem, true, ref flag2, ref flag3);
					if (autoDevItem.Alpha.InBeta)
					{
						if (this.DevActions >= 3f && SDateTime.Now() > this.GetCutDate(autoDevItem, 1f) - new SDateTime(0, 3, 0, 0, 0))
						{
							this.EndMarketingFor(autoDevItem.Alpha);
							object[] array = (object[])autoDevItem.Alpha.PromoteAction();
							if (array != null)
							{
								this.NextReleaseDate = this.GetNextReleaseDate(autoDevItem);
								if (autoDevItem.Secondary)
								{
									this.LastAlpha2 = null;
								}
								else
								{
									this.LastAlpha = null;
								}
								this.DevActions -= 3f;
								SupportWork supportWork = (SupportWork)array[0];
								if (this.AutoSupport)
								{
									supportWork.AutoDev = true;
									supportWork.Hidden = true;
									this.SupportItems.Add(supportWork);
								}
								this.AssignTeams(supportWork, this.SupportTeams);
								SoftwareProduct softwareProduct = (SoftwareProduct)array[1];
								this.AddPastRelease(softwareProduct);
								if (this.PostMarketing)
								{
									MarketingPlan marketingPlan = new MarketingPlan(0f, softwareProduct.ID, softwareProduct.Name);
									this.AssignTeams(marketingPlan, this.PostMarketingTeams);
									GameSettings.Instance.MyCompany.WorkItems.Add(marketingPlan);
									if (this.HandleMarketing)
									{
										marketingPlan.AutoDev = true;
										marketingPlan.Hidden = true;
										this.MarketingItems.Add(marketingPlan);
									}
								}
								this.Items.Remove(autoDevItem);
								i--;
								this.SetDev(autoDevItem, false, ref flag2, ref flag3);
								if (this.PhysicalCopies > 0u)
								{
									uint num = this.PhysicalCopies;
									if (this.PhysicalCopyRel)
									{
										num = (uint)(softwareProduct.Followers * (this.PhysicalCopies / 100f));
									}
									GameSettings.Instance.MyCompany.MakeTransaction((float)(-(float)((ulong)num)) * MarketSimulation.PhysicalCopyPrice, Company.TransactionCategory.Distribution, "Copy order");
									softwareProduct.PhysicalCopies += num;
								}
								HUD.Instance.AddPopupMessage("ProjectManagementRelease".Loc(new object[]
								{
									base.Name,
									softwareProduct._category.LocSWC(softwareProduct._type),
									softwareProduct._type.LocSW(),
									softwareProduct.Name
								}), "Info", PopupManager.PopUpAction.OpenProductDetails, softwareProduct.ID, PopupManager.NotificationSound.Good, 0f, PopupManager.PopupIDs.None, 6);
								flag4 = true;
							}
						}
					}
					else if (!autoDevItem.Alpha.InDelay && this.DevActions >= 1f && (autoDevItem.Alpha.HasFinished || SDateTime.Now() > this.GetCutDate(autoDevItem, SoftwareType.DesignRatio + 0.75f * (1f - SoftwareType.DesignRatio))))
					{
						autoDevItem.Alpha.PromoteAction();
						if (autoDevItem.Alpha.InDelay)
						{
							this.DevActions -= 1f;
							if (this.PrintingCopies > 0u && !autoDevItem.hasPrinted)
							{
								uint value = this.PrintingCopies;
								if (this.PrintingCopyRel)
								{
									value = (uint)(autoDevItem.Alpha.Followers * (this.PrintingCopies / 100f));
								}
								autoDevItem.hasPrinted = true;
								PrintJob printJob = new PrintJob(autoDevItem.Alpha.ForceID(), 1f);
								printJob.Limit = new uint?(value);
								GameSettings.Instance.PrintOrders[printJob.ID] = printJob;
								HUD.Instance.distributionWindow.RefreshOrders();
							}
						}
					}
					if (!flag4 && !autoDevItem.PressRelease && this.MarketingTeams.Count > 0 && this.MarketingActions > 1f && SDateTime.Now() > this.GetCutDate(autoDevItem, SoftwareType.DesignRatio + (1f - SoftwareType.DesignRatio) * 0.5f))
					{
						MarketingWindow marketingWindow = HUD.Instance.marketingWindow;
						float cost = marketingWindow.PressOptionCost.Sum();
						float potential = marketingWindow.PressOptionEffect.Sum();
						autoDevItem.PressRelease = true;
						MarketingPlan marketingPlan2 = new MarketingPlan(autoDevItem.Alpha, MarketingPlan.TaskType.PressRelease, cost, potential, (!(autoDevItem.Alpha.guiItem == null)) ? (autoDevItem.Alpha.guiItem.transform.GetSiblingIndex() + 1) : -1);
						marketingPlan2.AutoDev = true;
						marketingPlan2.Hidden = true;
						this.AssignTeams(marketingPlan2, this.MarketingTeams);
						GameSettings.Instance.MyCompany.WorkItems.Add(marketingPlan2);
						this.MarketingActions -= 1f;
					}
					if (!flag4 && !autoDevItem.PressBuild && this.MarketingTeams.Count > 0 && this.MarketingActions > 1f && SDateTime.Now() > this.GetCutDate(autoDevItem, SoftwareType.DesignRatio + (1f - SoftwareType.DesignRatio) * 0.75f))
					{
						GameSettings.Instance.PressBuildQueue.Add(autoDevItem.Alpha);
						autoDevItem.PressBuild = true;
						this.MarketingActions -= 1f;
					}
					if (!flag4)
					{
						this.RefreshHype(autoDevItem);
					}
				}
				else
				{
					flag = true;
					if (autoDevItem.Document.HasFinished || SDateTime.Now() > this.GetCutDate(autoDevItem, SoftwareType.DesignRatio))
					{
						this.EndMarketingFor(autoDevItem.Document);
						object obj = autoDevItem.Document.PromoteAction();
						if (obj != null)
						{
							this.LastDesign = null;
							SoftwareAlpha softwareAlpha = (SoftwareAlpha)obj;
							softwareAlpha.AutoDev = true;
							softwareAlpha.Hidden = true;
							AutoDevWorkItem.AutoDevItem autoDevItem2 = new AutoDevWorkItem.AutoDevItem(softwareAlpha, this);
							autoDevItem2.MonthsToSpend = autoDevItem.MonthsToSpend;
							autoDevItem2.AlreadyDev = Utilities.GetMonths(autoDevItem.ActualStartDate, SDateTime.Now());
							autoDevItem2.Hype = autoDevItem.Hype;
							this.Items.Add(autoDevItem2);
							this.Items.Remove(autoDevItem);
							i--;
							flag = false;
						}
					}
					if (flag && !autoDevItem.PressRelease && this.MarketingTeams.Count > 0 && this.MarketingActions > 1f && SDateTime.Now() > this.GetCutDate(autoDevItem, SoftwareType.DesignRatio * 0.5f))
					{
						MarketingWindow marketingWindow2 = HUD.Instance.marketingWindow;
						float cost2 = marketingWindow2.PressOptionCost.Sum();
						float potential2 = marketingWindow2.PressOptionEffect.Sum();
						autoDevItem.PressRelease = true;
						MarketingPlan marketingPlan3 = new MarketingPlan(autoDevItem.Document, MarketingPlan.TaskType.PressRelease, cost2, potential2, (!(autoDevItem.Document.guiItem == null)) ? (autoDevItem.Document.guiItem.transform.GetSiblingIndex() + 1) : -1);
						marketingPlan3.AutoDev = true;
						marketingPlan3.Hidden = true;
						this.AssignTeams(marketingPlan3, this.MarketingTeams);
						GameSettings.Instance.MyCompany.WorkItems.Add(marketingPlan3);
						this.MarketingActions -= 1f;
					}
					if (flag)
					{
						this.RefreshHype(autoDevItem);
					}
				}
			}
		}
		if (this.DevActions > 1f && ((!flag2 && this.SDevTeams.Count > 0) || (!flag3 && this.SecondaryDevTeams.Count > 0)))
		{
			AutoDevWorkItem.AutoDevItem autoDevItem3 = this.Items.FirstOrDefault((AutoDevWorkItem.AutoDevItem x) => x.Queued && x.Alpha != null);
			if (autoDevItem3 != null)
			{
				autoDevItem3.ActualStartDate = SDateTime.Now();
				autoDevItem3.SWWorkItem.ReleaseDate = new SDateTime?(SDateTime.Max(new SDateTime[]
				{
					SDateTime.Now() + 1,
					this.GetCutDate(autoDevItem3, 1f)
				}));
				if (this.NextReleaseDate == null || this.NextReleaseDate.Value > autoDevItem3.ReleaseDate.Value)
				{
					this.NextReleaseDate = autoDevItem3.ReleaseDate;
				}
				autoDevItem3.Queued = false;
				if (!flag2 && this.SDevTeams.Count > 0)
				{
					this.AssignTeams(autoDevItem3.Alpha, this.SDevTeams);
					this.LastAlpha = autoDevItem3.Name;
				}
				else
				{
					this.AssignTeams(autoDevItem3.Alpha, this.SecondaryDevTeams);
					autoDevItem3.Secondary = true;
					this.LastAlpha2 = autoDevItem3.Name;
				}
				this.DevActions -= 1f;
			}
		}
		if (!flag && this.DesignActions > 2f && this.DesignTeams.Count > 0)
		{
			AutoDevWorkItem.AutoDevItem autoDevItem4 = this.Items.FirstOrDefault((AutoDevWorkItem.AutoDevItem x) => x.Queued && x.Document != null);
			if (autoDevItem4 != null)
			{
				autoDevItem4.ActualStartDate = SDateTime.Now();
				autoDevItem4.Queued = false;
				this.LastDesign = autoDevItem4.Name;
				this.AssignTeams(autoDevItem4.Document, this.DesignTeams);
				this.DesignActions -= 2f;
			}
			else if (this.AutoProject && this.DesignActions > 3f)
			{
				SoftwareWorkItem softwareWorkItem = this.GenerateProduct();
				if (softwareWorkItem != null)
				{
					softwareWorkItem.AutoDev = true;
					softwareWorkItem.Hidden = true;
					GameSettings.Instance.MyCompany.WorkItems.Add(softwareWorkItem);
					this.Items.Add(new AutoDevWorkItem.AutoDevItem(softwareWorkItem, this));
					this.DesignActions -= 3f;
				}
			}
		}
	}

	private void EndMarketingFor(WorkItem i)
	{
		foreach (MarketingPlan marketingPlan in (from x in GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>()
		where x.TargetItem == i
		select x).ToList<MarketingPlan>())
		{
			marketingPlan.StopMarketing();
		}
	}

	public void UpdateSupportMarket()
	{
		for (int i = 0; i < this.SupportItems.Count; i++)
		{
			SupportWork supportWork = this.SupportItems[i];
			SoftwareProduct targetProduct = supportWork.TargetProduct;
			if (targetProduct.Bugs == 0 || targetProduct.Userbase == 0)
			{
				supportWork.CancelSupport();
				this.SupportItems.RemoveAt(i);
				i--;
			}
		}
		int j = 0;
		while (j < this.MarketingItems.Count)
		{
			MarketingPlan marketingPlan = this.MarketingItems[j];
			SoftwareProduct targetProd = marketingPlan.TargetProd;
			List<float> cashflow = targetProd.GetCashflow();
			float maxBudget = 0f;
			if (cashflow.Count <= 0)
			{
				goto IL_E5;
			}
			if (cashflow[cashflow.Count - 1] >= 1000f)
			{
				maxBudget = cashflow[cashflow.Count - 1] / 4f;
				goto IL_E5;
			}
			marketingPlan.StopMarketing();
			this.MarketingItems.RemoveAt(j);
			j--;
			IL_EE:
			j++;
			continue;
			IL_E5:
			marketingPlan.MaxBudget = maxBudget;
			goto IL_EE;
		}
	}

	public SoftwareWorkItem GenerateProduct()
	{
		SoftwareProduct softwareProduct = (!this.OnlySequels) ? this.GetLatestRelease() : this.GetValidSequel();
		if (softwareProduct == null)
		{
			if (this.PastReleases.Count == 0 && this.Items.Count == 0)
			{
				HUD.Instance.AddPopupMessage("ProjectManagementProjectWarning".Loc(new object[]
				{
					base.Name
				}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 0.5f, PopupManager.PopupIDs.ProjectManagement, 1);
			}
			return null;
		}
		string type = softwareProduct._type;
		string category = softwareProduct._category;
		uint? sequelTo = (!this.OnlySequels) ? null : new uint?(softwareProduct.ID);
		float budget = GameSettings.Instance.MyCompany.Money * 0.25f;
		Dictionary<string, SoftwareProduct> needs = this.GetNeeds(ref budget, softwareProduct);
		if (needs != null)
		{
			bool osspecific = GameSettings.Instance.SoftwareTypes[type].OSSpecific;
			List<SoftwareProduct> oss = this.GetOSs(budget, needs, softwareProduct);
			if ((oss != null || !osspecific) && !GameSettings.Instance.SoftwareTypes[type].ForceIssueBool(category, needs, oss))
			{
				Dictionary<string, uint> needs3 = needs.ToDictionary((KeyValuePair<string, SoftwareProduct> x) => x.Key, (KeyValuePair<string, SoftwareProduct> x) => x.Value.ID);
				uint[] array;
				if (osspecific)
				{
					array = (from x in oss
					select x.ID).ToArray<uint>();
				}
				else
				{
					array = null;
				}
				uint[] os = array;
				SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[type];
				string[] features = softwareType.GenerateFeatures(1f, category, needs3, os, SDateTime.Now(), this.MainServer != null);
				string[] needs2 = softwareType.GetNeeds(features, category);
				needs = needs2.ToDictionary((string x) => x, (string x) => needs[x]);
				needs3 = needs.ToDictionary((KeyValuePair<string, SoftwareProduct> x) => x.Key, (KeyValuePair<string, SoftwareProduct> x) => x.Value.ID);
				float num = 0f;
				foreach (KeyValuePair<string, SoftwareProduct> keyValuePair in needs)
				{
					num += keyValuePair.Value.GetLicenseCost(GameSettings.Instance.MyCompany);
					keyValuePair.Value.TransferLicense(GameSettings.Instance.MyCompany, SDateTime.Now());
				}
				if (osspecific)
				{
					foreach (SoftwareProduct softwareProduct2 in oss)
					{
						num += softwareProduct2.GetLicenseCost(GameSettings.Instance.MyCompany);
						softwareProduct2.TransferLicense(GameSettings.Instance.MyCompany, SDateTime.Now());
					}
				}
				SoftwareWorkItem softwareWorkItem = DesignDocument.CreateWork((sequelTo == null) ? GameSettings.Instance.simulation.GenerateProductName(softwareType, category) : GameSettings.Instance.simulation.GenerateProductSequalName(softwareProduct.Name), type, category, needs3, os, SimulatedCompany.PickPrice(softwareType.Name, category, softwareType.DevTime(features, category, null), 0.75f, 0f, SDateTime.Now()), SDateTime.Now(), GameSettings.Instance.MyCompany, sequelTo, false, num, features, null, this.MainServer, this.SCMServer, false);
				this.LastLicensePaid = num;
				softwareWorkItem.Hidden = true;
				return softwareWorkItem;
			}
		}
		return null;
	}

	public Dictionary<string, SoftwareProduct> GetNeeds(ref float budget, SoftwareProduct proto)
	{
		SDateTime time = SDateTime.Now();
		SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[proto._type];
		string[] needs = softwareType.GetNeeds(proto._category);
		Dictionary<string, HashSet<string>> dictionary = new Dictionary<string, HashSet<string>>();
		foreach (Feature feature in from x in softwareType.Features.Values
		where x.Forced && x.IsCompatible(proto._category)
		select x)
		{
			foreach (KeyValuePair<string, string> keyValuePair in feature.Dependencies)
			{
				dictionary.Append(keyValuePair.Key, keyValuePair.Value);
			}
		}
		float num = budget;
		Dictionary<string, SoftwareProduct> dictionary2 = new Dictionary<string, SoftwareProduct>();
		int num2 = needs.Length + ((!GameSettings.Instance.SoftwareTypes[proto._type].OSSpecific) ? 0 : 1);
		string[] array = needs;
		for (int j = 0; j < array.Length; j++)
		{
			string text = array[j];
			float localBudget = budget;
			string localNeed = text;
			int i1 = num2;
			IEnumerable<SoftwareProduct> source;
			HashSet<string> feats;
			if (dictionary.TryGetValue(text, out feats))
			{
				source = from x in GameSettings.Instance.simulation.GetAllProducts()
				where x.Type.Name.Equals(localNeed) && x.GetLicenseCost() <= localBudget / (float)i1 && feats.All((string y) => x.Features.Contains(y))
				select x;
			}
			else
			{
				source = from x in GameSettings.Instance.simulation.GetAllProducts()
				where x.Type.Name.Equals(localNeed) && x.GetLicenseCost() <= localBudget / (float)i1
				select x;
			}
			IEnumerable<SoftwareProduct> enumerable = from x in source
			where Utilities.GetMonths(x.Release, SDateTime.Now()) < 60f
			select x;
			if (enumerable.Any<SoftwareProduct>())
			{
				source = enumerable;
			}
			if (!source.Any<SoftwareProduct>())
			{
				if (text.Equals(softwareType.OSNeed))
				{
					return null;
				}
			}
			else
			{
				SoftwareProduct softwareProduct = (from x in source
				orderby (!this.UseOwnLicenses) ? 0 : ((!x.DevCompany.Player) ? 1 : 0), Utilities.RandomRange(0.99f, 1f) * x.GetQuality(GameSettings.Instance.simulation.GetQuality(x, time, false), time, false) * x.RelativeFeatureScore(GameSettings.Instance.simulation, time) descending
				select x).FirstOrDefault<SoftwareProduct>();
				num -= softwareProduct.GetLicenseCost();
				dictionary2.Add(text, softwareProduct);
				num2--;
			}
		}
		budget = num;
		return dictionary2;
	}

	public List<SoftwareProduct> GetOSs(float budget, Dictionary<string, SoftwareProduct> needs, SoftwareProduct proto)
	{
		SoftwareType type = GameSettings.Instance.SoftwareTypes[proto._type];
		if (!type.OSSpecific)
		{
			return null;
		}
		IEnumerable<SoftwareProduct> source;
		if (type.OSNeed == null)
		{
			source = from x in GameSettings.Instance.simulation.GetAllProducts()
			where "Operating System".Equals(x._type)
			select x;
		}
		else
		{
			source = GameSettings.Instance.simulation.GetCompatibleOS(needs[type.OSNeed], false, null);
		}
		if (type.OSLimit != null)
		{
			source = from x in source
			where x._category.Equals(type.OSLimit)
			select x;
		}
		SDateTime time = SDateTime.Now();
		List<SoftwareProduct> source2 = source.ToList<SoftwareProduct>();
		List<SoftwareProduct> list = (from x in source2
		where (time - x.Release).Year < 5
		select x).ToList<SoftwareProduct>();
		if (list.Count > 0)
		{
			source2 = list;
		}
		float num = budget;
		List<SoftwareProduct> list2 = new List<SoftwareProduct>();
		int num2 = 0;
		float maxFeat = 0f;
		SoftwareType softwareType = GameSettings.Instance.SoftwareTypes["Operating System"];
		foreach (KeyValuePair<string, SoftwareCategory> keyValuePair in softwareType.Categories)
		{
			maxFeat = Mathf.Max(maxFeat, GameSettings.Instance.simulation.GetFeatureScore(softwareType.Name, keyValuePair.Key, time));
		}
		foreach (SoftwareProduct softwareProduct in from x in source2
		orderby (!this.UseOwnLicenses) ? 0 : ((!x.DevCompany.Player) ? 1 : 0), (float)x.Userbase * (x.FeatureScore / maxFeat) descending
		select x)
		{
			num -= softwareProduct.GetLicenseCost();
			if (num <= 0f)
			{
				break;
			}
			list2.Add(softwareProduct);
			num2++;
			if (num2 == 3)
			{
				break;
			}
		}
		return (list2.Count != 0) ? list2 : null;
	}

	public override void Kill(bool wasCancelled = false)
	{
		this.Leader = null;
		for (int i = this.Items.Count - 1; i > -1; i--)
		{
			SoftwareWorkItem softwareWorkItem = this.Items[i].Alpha ?? this.Items[i].Document;
			softwareWorkItem.Kill(false);
		}
		foreach (MarketingPlan marketingPlan in this.MarketingItems)
		{
			marketingPlan.AutoDev = false;
			marketingPlan.Hidden = false;
		}
		foreach (SupportWork supportWork in this.SupportItems)
		{
			supportWork.AutoDev = false;
			supportWork.Hidden = false;
		}
		if (HUD.Instance.AutoDevWindow.Window.Shown && HUD.Instance.AutoDevWindow.Work == this)
		{
			HUD.Instance.AutoDevWindow.Window.Close();
		}
		base.Kill(wasCancelled);
	}

	public override string CurrentStage()
	{
		return string.Concat(new string[]
		{
			"Designing".Loc(),
			": ",
			this.LastDesign ?? "None".Loc(),
			"\n",
			"Developing".Loc(),
			": ",
			this.GetAlphaWork()
		});
	}

	private string GetAlphaWork()
	{
		if (this.LastAlpha != null && this.LastAlpha2 != null)
		{
			return "2 products".Loc();
		}
		if (this.LastAlpha != null)
		{
			return this.LastAlpha;
		}
		if (this.LastAlpha2 != null)
		{
			return this.LastAlpha2;
		}
		return "None".Loc();
	}

	public override int GUIWorkItemType()
	{
		return 1;
	}

	public override string Category()
	{
		return "Next release".Loc() + ": " + ((this.NextReleaseDate == null) ? "None".Loc() : this.NextReleaseDate.Value.ToCompactString());
	}

	public void OpenOptions()
	{
		HUD.Instance.AutoDevWindow.Show(this);
	}

	public override float StressMultiplier()
	{
		return 2f;
	}

	public override string GetWorkTypeName()
	{
		return "Project management";
	}

	public override string GetTeam()
	{
		return (!(this.Leader == null)) ? this.Leader.employee.FullName : null;
	}

	public float LastLicensePaid;

	public string LastDesign;

	public string LastAlpha;

	public string LastAlpha2;

	private SDateTime? NextReleaseDate;

	public List<SupportWork> SupportItems = new List<SupportWork>();

	public List<MarketingPlan> MarketingItems = new List<MarketingPlan>();

	public List<uint> PastReleases = new List<uint>();

	public string MainServer;

	public string SCMServer;

	public SHashSet<string> DesignTeams = new SHashSet<string>();

	public SHashSet<string> SDevTeams = new SHashSet<string>();

	public SHashSet<string> SecondaryDevTeams = new SHashSet<string>();

	public SHashSet<string> MarketingTeams = new SHashSet<string>();

	public SHashSet<string> PostMarketingTeams = new SHashSet<string>();

	public SHashSet<string> SupportTeams = new SHashSet<string>();

	public bool Hype = true;

	public bool AutoProject;

	public bool OnlySequels;

	public bool PostMarketing;

	public bool HandleMarketing;

	public bool SingleIP;

	public bool UseOwnLicenses;

	public bool AutoSupport = true;

	public bool PhysicalCopyRel;

	public bool PrintingCopyRel;

	public float DevTimeMultiplier = 1f;

	public float DevActions;

	public float DesignActions;

	public float MarketingActions;

	public uint PhysicalCopies;

	public uint PrintingCopies;

	private uint _lastLeader;

	public EventList<AutoDevWorkItem.AutoDevItem> Items = new EventList<AutoDevWorkItem.AutoDevItem>();

	[NonSerialized]
	private Actor _leader;

	public uint LeaderID;

	[Serializable]
	public class AutoDevItem
	{
		public AutoDevItem()
		{
		}

		public AutoDevItem(SoftwareWorkItem work, AutoDevWorkItem self)
		{
			if (work is SoftwareAlpha)
			{
				this.Alpha = (SoftwareAlpha)work;
			}
			else
			{
				this.Document = (DesignDocument)work;
			}
			int[] optimalEmployeeCount = work.Type.GetOptimalEmployeeCount(this.SWWorkItem.DevTime, this.SWWorkItem.CodeArtRatio);
			this.MonthsToSpend = GameData.ProjectDevTime(optimalEmployeeCount[0], optimalEmployeeCount[1], this.SWWorkItem.DevTime, this.SWWorkItem.CodeArtRatio) * self.DevTimeMultiplier;
		}

		public string Phase()
		{
			if (this.Alpha != null)
			{
				return (!this.Alpha.InBeta) ? "Alpha" : "Beta";
			}
			return "Design";
		}

		public int PhaseOrder()
		{
			if (this.Alpha != null)
			{
				return (!this.Alpha.InBeta) ? 1 : 2;
			}
			return 0;
		}

		public SoftwareWorkItem SWWorkItem
		{
			get
			{
				return this.Document ?? this.Alpha;
			}
		}

		public SDateTime? ReleaseDate
		{
			get
			{
				return (this.Alpha != null) ? this.Alpha.ReleaseDate : this.Document.ReleaseDate;
			}
		}

		public string ReleaseDateText
		{
			get
			{
				SDateTime? releaseDate = this.ReleaseDate;
				if (releaseDate != null)
				{
					return releaseDate.Value.ToCompactString();
				}
				if (this.Queued)
				{
					return "None".Loc();
				}
				return (this.ActualStartDate + (this.MonthsToSpend - this.AlreadyDev)).ToCompactString();
			}
		}

		public int ReleaseDateInt
		{
			get
			{
				SDateTime? releaseDate = this.ReleaseDate;
				return (releaseDate == null) ? -1 : releaseDate.Value.ToInt();
			}
		}

		public SDateTime StartDate
		{
			get
			{
				return (this.Alpha != null) ? this.Alpha.DevStart : this.Document.DevStart;
			}
		}

		public string Name
		{
			get
			{
				return (this.Alpha != null) ? this.Alpha.SoftwareName : this.Document.SoftwareName;
			}
		}

		public bool Design
		{
			get
			{
				return this.Alpha == null;
			}
		}

		public SDateTime ReleaseBy()
		{
			SoftwareWorkItem softwareWorkItem = this.Alpha ?? this.Document;
			if (softwareWorkItem.ReleaseDate != null)
			{
				return softwareWorkItem.ReleaseDate.Value;
			}
			return softwareWorkItem.DevStart + softwareWorkItem.DevTime;
		}

		public void Release()
		{
			SoftwareWorkItem swworkItem = this.SWWorkItem;
			AutoDevWorkItem autoDevWorkItem = GameSettings.Instance.MyCompany.WorkItems.OfType<AutoDevWorkItem>().FirstOrDefault((AutoDevWorkItem x) => x.Items.Contains(this));
			if (autoDevWorkItem != null)
			{
				autoDevWorkItem.Items.Remove(this);
				if (swworkItem.SoftwareName.Equals(autoDevWorkItem.LastDesign))
				{
					autoDevWorkItem.LastDesign = null;
				}
				if (swworkItem.SoftwareName.Equals(autoDevWorkItem.LastAlpha))
				{
					autoDevWorkItem.LastAlpha = null;
				}
				if (swworkItem.SoftwareName.Equals(autoDevWorkItem.LastAlpha2))
				{
					autoDevWorkItem.LastAlpha2 = null;
				}
			}
			swworkItem.AutoDev = false;
			swworkItem.Hidden = false;
			foreach (MarketingPlan marketingPlan in GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>())
			{
				if (marketingPlan.TargetItem == swworkItem)
				{
					marketingPlan.Hidden = false;
					marketingPlan.AutoDev = false;
				}
			}
		}

		public void Cancel()
		{
			this.SWWorkItem.Kill(true);
		}

		public SoftwareAlpha Alpha;

		public DesignDocument Document;

		public MarketingPlan Hype;

		public bool PressRelease;

		public bool PressBuild;

		public bool Queued = true;

		public bool Secondary;

		public bool hasPrinted;

		public SDateTime ActualStartDate;

		public float AlreadyDev;

		public float MonthsToSpend;
	}
}
