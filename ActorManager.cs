﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActorManager
{
	public ActorManager()
	{
		this.Actors.OnChange = new Action(this.UpdateActorWindow);
		this.Staff.OnChange = new Action(this.UpdateStaffWindow);
	}

	public void UpdateActorWindow()
	{
		if (HUD.Instance != null && HUD.Instance.employeeWindow != null && HUD.Instance.employeeWindow.EmployeeList != null)
		{
			HUD.Instance.employeeWindow.UpdateEmployeeList();
		}
	}

	public void UpdateStaffWindow()
	{
		if (HUD.Instance != null && HUD.Instance.staffWindow != null && HUD.Instance.staffWindow.StaffList != null)
		{
			this.Staff.Update<object>(HUD.Instance.staffWindow.StaffList.Items);
		}
	}

	public void RemoveTeam(string name)
	{
		if (this.Teams.ContainsKey(name))
		{
			Team team = this.Teams[name];
			team.GetEmployees().ToList<Actor>().ForEach(delegate(Actor x)
			{
				x.Team = null;
			});
			GameSettings.Instance.MyCompany.WorkItems.ForEach(delegate(WorkItem x)
			{
				x.RemoveDevTeam(team);
			});
			this.Teams.Remove(name);
			GameSettings.Instance.RemoveTeamAssociation(team);
		}
	}

	public void RenameTeam(string name, string newName)
	{
		if (this.Teams.ContainsKey(newName))
		{
			WindowManager.Instance.ShowMessageBox("TeamNameError".Loc(), false, DialogWindow.DialogType.Error);
			return;
		}
		if (this.Teams.ContainsKey(name))
		{
			Team oldTeam = this.Teams[name];
			Team newTeam = new Team(newName);
			newTeam.HR = oldTeam.HR;
			newTeam.WorkEnd = oldTeam.WorkEnd;
			newTeam.WorkStart = oldTeam.WorkStart;
			newTeam.Talking = oldTeam.Talking;
			newTeam.Meeting = oldTeam.Meeting;
			newTeam.VacationMonth = oldTeam.VacationMonth;
			newTeam.VacationSpread = oldTeam.VacationSpread;
			this.Teams[newName] = newTeam;
			TableScript meetingTable = oldTeam.MeetingTable;
			oldTeam.MeetingTable = null;
			oldTeam.HR = new HRManagement();
			oldTeam.GetEmployees().ToList<Actor>().ForEach(delegate(Actor x)
			{
				x.Team = newName;
			});
			newTeam.MeetingTable = meetingTable;
			GameSettings.Instance.MyCompany.WorkItems.ForEach(delegate(WorkItem x)
			{
				x.SwitchDevTeam(oldTeam, newTeam);
			});
			this.Teams.Remove(name);
			GameSettings.Instance.SwitchTeamAssociation(oldTeam, newTeam);
			HUD.Instance.TeamWindow.TeamList.Items = GameSettings.Instance.sActorManager.Teams.Values.Cast<object>().ToList<object>();
		}
	}

	public List<Actor> GetAwaiting()
	{
		return this.Awaiting.Keys.ToList<Actor>();
	}

	public SDateTime? GetArriveTime(Actor actor)
	{
		SDateTime value;
		if (!this.Awaiting.TryGetValue(actor, out value))
		{
			return null;
		}
		return new SDateTime?(value);
	}

	public void AddToAwaiting(Actor actor, SDateTime time, bool replace = false, bool mainThread = true)
	{
		if (replace || !this.Awaiting.ContainsKey(actor))
		{
			if (replace && (!mainThread || !actor.isActiveAndEnabled))
			{
				for (int i = 0; i < RoadManager.Instance.Cars.Count; i++)
				{
					CarScript carScript = RoadManager.Instance.Cars[i];
					for (int j = 0; j < carScript.SpawnPoints.Length; j++)
					{
						carScript.SpawnPoints[j].Occupants.Remove(actor);
					}
				}
			}
			this.Awaiting[actor] = time;
		}
	}

	public void RemoveFromAwaiting(Actor actor)
	{
		this.Awaiting.Remove(actor);
		this.ReadyForBus.Remove(actor);
	}

	public static Func<Room, float> PriorityFunctions(AI<Actor>.AIType aiType)
	{
		switch (aiType)
		{
		case AI<Actor>.AIType.Janitor:
			return (Room x) => (from y in x.GetFurnitures()
			where y.HasUpg && !y.ITFix
			select y.upg.Quality).MinOrDefault(1f);
		case AI<Actor>.AIType.Cleaning:
			return (Room x) => x.DirtScore;
		case AI<Actor>.AIType.IT:
			return (Room x) => (from y in x.GetFurnitures()
			where y.HasUpg && y.ITFix
			select y.upg.Quality).MinOrDefault(1f);
		case AI<Actor>.AIType.Receptionist:
			return (Room x) => (float)((x.GetFurniture("Desk").Count <= 0) ? 1 : 0);
		case AI<Actor>.AIType.Guest:
			return (Room x) => (float)((x.GetFurniture("Desk").Count <= 0) ? 1 : 0);
		case AI<Actor>.AIType.Cook:
			return (Room x) => (float)((x.GetFurniture("Stove").Count <= 0) ? 1 : 0);
		case AI<Actor>.AIType.Courier:
			return (Room x) => (float)((x.GetFurniture("ProductPrinter").Count <= 0 && x.GetFurniture("Pallet").Count <= 0) ? 1 : 0);
		default:
			return (Room x) => 1f;
		}
	}

	private bool Compatible(Actor self, Actor other)
	{
		switch (self.AItype)
		{
		case AI<Actor>.AIType.Janitor:
		case AI<Actor>.AIType.Cleaning:
		case AI<Actor>.AIType.IT:
		case AI<Actor>.AIType.Receptionist:
			return other.AItype == AI<Actor>.AIType.Cleaning || other.AItype == AI<Actor>.AIType.Janitor || other.AItype == AI<Actor>.AIType.IT || other.AItype == AI<Actor>.AIType.Receptionist;
		case AI<Actor>.AIType.Cook:
			return other.AItype == AI<Actor>.AIType.Cook;
		case AI<Actor>.AIType.Courier:
			return other.AItype == AI<Actor>.AIType.Courier;
		}
		return false;
	}

	private RoadNode.ParkingAssign ActorToType(Actor self)
	{
		switch (self.AItype)
		{
		case AI<Actor>.AIType.Employee:
			return RoadNode.ParkingAssign.Employees;
		case AI<Actor>.AIType.Janitor:
		case AI<Actor>.AIType.Cleaning:
		case AI<Actor>.AIType.IT:
		case AI<Actor>.AIType.Receptionist:
			return RoadNode.ParkingAssign.Staff;
		case AI<Actor>.AIType.Guest:
			return RoadNode.ParkingAssign.Guests;
		case AI<Actor>.AIType.Cook:
			return RoadNode.ParkingAssign.Cooks;
		case AI<Actor>.AIType.Courier:
			return RoadNode.ParkingAssign.Deliveries;
		default:
			return RoadNode.ParkingAssign.Anyone;
		}
	}

	public void DoUpdate()
	{
		this.Deletes.Clear();
		foreach (KeyValuePair<Actor, SDateTime> keyValuePair in this.Awaiting)
		{
			if (keyValuePair.Key.employee.Fired)
			{
				if (keyValuePair.Key != null)
				{
					UnityEngine.Object.Destroy(keyValuePair.Key.gameObject);
				}
				this.Deletes.Add(keyValuePair.Key);
			}
		}
		for (int i = 0; i < this.Deletes.Count; i++)
		{
			this.Awaiting.Remove(this.Deletes[i]);
		}
		this.Deletes.Clear();
		HashSet<Actor> hashSet = new HashSet<Actor>();
		foreach (KeyValuePair<Actor, SDateTime> keyValuePair2 in this.Awaiting)
		{
			if ((keyValuePair2.Value - SDateTime.Now()).ToInt() < 0)
			{
				Actor key = keyValuePair2.Key;
				if (!hashSet.Contains(key) && !this.Deletes.Contains(key))
				{
					CarScript carScript = RoadManager.Instance.SendCar(key, ActorManager.PriorityFunctions(key.AItype), this.ActorToType(key));
					if (carScript == null || !carScript.gameObject.activeSelf)
					{
						if (key.AItype == AI<Actor>.AIType.Courier)
						{
							HUD.Instance.AddPopupMessage("CourierParkError".Loc(), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.CourierParking, 1);
						}
						else if (this.ReadyForBus.Count < 16)
						{
							this.Deletes.Add(key);
							this.ReadyForBus.Add(key);
						}
						else
						{
							HUD.Instance.AddPopupMessage("BusFullWarning".Loc(), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Issue, 0f, PopupManager.PopupIDs.Bus, 1);
						}
					}
					else
					{
						this.AvailableCache.Clear();
						this.Deletes.Add(key);
						if (key.AItype == AI<Actor>.AIType.Employee)
						{
							if (key.Team != null)
							{
								Actor localActor = key;
								SDateTime time = SDateTime.Now();
								this.AvailableCache.AddRange(from x in this.Awaiting
								where x.Value.Day == time.Day && x.Value.Month == time.Month && x.Value.Year == time.Year && x.Key != localActor && x.Key.Team != null && x.Key.Team.Equals(localActor.Team) && !this.Deletes.Contains(x.Key)
								select x.Key);
							}
						}
						else if (key.AItype != AI<Actor>.AIType.Guest)
						{
							Actor localActor = key;
							SDateTime time = SDateTime.Now();
							this.AvailableCache.AddRange(from x in this.Awaiting
							where x.Key != localActor && this.Compatible(localActor, x.Key) && x.Value.Day == time.Day && x.Value.Month == time.Month && x.Value.Year == time.Year && localActor.StaffOn == x.Key.StaffOn && !this.Deletes.Contains(x.Key)
							select x.Key);
						}
						int num = 0;
						int num2 = 0;
						while (num2 < this.AvailableCache.Count && num < carScript.Capacity - 1)
						{
							if (!hashSet.Contains(this.AvailableCache[num2]))
							{
								if (carScript.AddOccupant(this.AvailableCache[num2], true) == null)
								{
									break;
								}
								num++;
								hashSet.Add(this.AvailableCache[num2]);
								this.Deletes.Add(this.AvailableCache[num2]);
							}
							num2++;
						}
						hashSet.Add(key);
					}
				}
			}
		}
		for (int j = 0; j < this.Deletes.Count; j++)
		{
			this.Awaiting.Remove(this.Deletes[j]);
		}
		int num3 = 0;
		for (int k = 0; k < this.Actors.Count; k++)
		{
			Actor actor = this.Actors[k];
			if (!actor.isActiveAndEnabled && Mathf.Abs(TimeOfDay.Instance.Hour - actor.SpawnTime) > 2 && !this.ReadyForBus.Contains(actor) && !hashSet.Contains(actor) && !this.Awaiting.ContainsKey(actor) && !RoadManager.Instance.Cars.Any((CarScript x) => x.ContainsOccupant(actor)))
			{
				SDateTime time3 = new SDateTime(0, actor.SpawnTime, TimeOfDay.Instance.Day, TimeOfDay.Instance.Month, TimeOfDay.Instance.Year);
				this.AddToAwaiting(actor, time3, false, true);
				num3++;
			}
		}
		for (int l = 0; l < this.Staff.Count; l++)
		{
			Actor actor = this.Staff[l];
			if (!actor.isActiveAndEnabled && Mathf.Abs(TimeOfDay.Instance.Hour - actor.SpawnTime) > 2 && !this.ReadyForBus.Contains(actor) && !hashSet.Contains(actor) && !this.Awaiting.ContainsKey(actor) && !RoadManager.Instance.Cars.Any((CarScript x) => x.ContainsOccupant(actor)))
			{
				SDateTime time2 = new SDateTime(0, actor.SpawnTime, TimeOfDay.Instance.Day, TimeOfDay.Instance.Month, TimeOfDay.Instance.Year);
				this.AddToAwaiting(actor, time2, false, true);
				num3++;
			}
		}
		if (num3 > 0)
		{
			Debug.Log("Added {0} missing poeople to awaiting".Format(new object[]
			{
				num3
			}));
		}
	}

	public EventList<Actor> Actors = new EventList<Actor>();

	public EventList<Actor> Staff = new EventList<Actor>();

	public HashSet<Actor> ReadyForBus = new HashSet<Actor>();

	public HashSet<Actor> Guests = new HashSet<Actor>();

	public HashSet<Actor> ReadyForHome = new HashSet<Actor>();

	private Dictionary<Actor, SDateTime> Awaiting = new Dictionary<Actor, SDateTime>();

	public Dictionary<string, Team> Teams = new Dictionary<string, Team>
	{
		{
			"Core",
			new Team("Core")
		}
	};

	[NonSerialized]
	private List<Actor> Deletes = new List<Actor>();

	[NonSerialized]
	private List<Actor> AvailableCache = new List<Actor>();
}
