﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GUIPieChart : Graphic
{
	public void UpdateCachedPie()
	{
		this.DrawPieChart(this.CachedData);
		this.SetVerticesDirty();
	}

	protected override void OnPopulateMesh(VertexHelper h)
	{
		if (!this.Cached)
		{
			this.DrawPieChart(this.CachedData);
		}
		if (this.CachedData == null || this.CachedData.Count == 0)
		{
			h.Clear();
			return;
		}
		h.Clear();
		Utilities.VBOToHelper(this.CachedData, h);
	}

	private void DrawPieChart(List<UIVertex> vbo)
	{
		Vector2 vector = Vector2.zero - base.rectTransform.pivot;
		Vector2 vector2 = Vector2.one - base.rectTransform.pivot;
		vector = new Vector2(vector.x * base.rectTransform.rect.width, vector.y * base.rectTransform.rect.height);
		vector2 = new Vector2(vector2.x * base.rectTransform.rect.width, vector2.y * base.rectTransform.rect.height);
		Vector2 center = new Vector2(vector.x + (vector2.x - vector.x) / 2f, vector.y + (vector2.y - vector.y) / 2f);
		float num = Mathf.Min(vector2.x - vector.x, vector2.y - vector.y) / 2f;
		vbo.Clear();
		float num2 = this.Values.Sum();
		if (num2 <= 0f)
		{
			return;
		}
		float num3 = 0f;
		for (int i = 0; i < this.Values.Count; i++)
		{
			float num4 = this.Values[i] / num2;
			float num5 = num4 * 3.14159274f * 2f;
			int sections = Mathf.Max(1, Mathf.RoundToInt(num4 * (float)GUIPieChart.PieSections));
			this.DrawArc(num3, num3 + num5, sections, center, num, this.Colors[i % this.Colors.Count], vbo, center.y - num, center.y + num);
			num3 += num5;
		}
	}

	private void DrawArc(float start, float end, int sections, Vector2 center, float radius, Color color, List<UIVertex> vbo, float top, float bottom)
	{
		UIVertex simpleVert = UIVertex.simpleVert;
		float num = (end - start) / (float)sections;
		for (int i = 0; i < sections; i++)
		{
			simpleVert.position = new Vector2(center.x, center.y);
			float time = (simpleVert.position.y - bottom) / (top - bottom);
			simpleVert.color = this.MultCol(color, this.DarkCurve.Evaluate(time), this.TransparentCurve.Evaluate(time));
			vbo.Add(simpleVert);
			float f = start + (float)i * num;
			simpleVert.position = new Vector2(center.x + Mathf.Cos(f) * radius, center.y + Mathf.Sin(f) * radius);
			time = (simpleVert.position.y - bottom) / (top - bottom);
			simpleVert.color = this.MultCol(color, this.DarkCurve.Evaluate(time), this.TransparentCurve.Evaluate(time));
			vbo.Add(simpleVert);
			f = start + (float)i * num + num / 2f;
			simpleVert.position = new Vector2(center.x + Mathf.Cos(f) * radius, center.y + Mathf.Sin(f) * radius);
			time = (simpleVert.position.y - bottom) / (top - bottom);
			simpleVert.color = this.MultCol(color, this.DarkCurve.Evaluate(time), this.TransparentCurve.Evaluate(time));
			vbo.Add(simpleVert);
			f = start + (float)i * num + num;
			simpleVert.position = new Vector2(center.x + Mathf.Cos(f) * radius, center.y + Mathf.Sin(f) * radius);
			time = (simpleVert.position.y - bottom) / (top - bottom);
			simpleVert.color = this.MultCol(color, this.DarkCurve.Evaluate(time), this.TransparentCurve.Evaluate(time));
			vbo.Add(simpleVert);
		}
	}

	private Color MultCol(Color c, float t, float t2)
	{
		return new Color(c.r * t, c.g * t, c.b * t, c.a * t2);
	}

	public List<float> Values = new List<float>
	{
		1f
	};

	public List<Color> Colors = new List<Color>
	{
		Color.white
	};

	public List<UIVertex> CachedData = new List<UIVertex>();

	public AnimationCurve DarkCurve;

	public AnimationCurve TransparentCurve;

	public bool Cached;

	public static int PieSections = 20;
}
