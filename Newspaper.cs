﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Newspaper : MonoBehaviour
{
	public string[] GetSections()
	{
		return (from x in Enum.GetValues(typeof(Newspaper.Section)).OfType<Newspaper.Section>()
		orderby (int)x
		select x.ToString() + " section").ToArray<string>();
	}

	private void Start()
	{
		Newspaper.Instance = this;
		this.InitializeSections();
		this.SectionSelector.UpdateContent<string>(this.GetSections());
		this.SectionSelector.Selected = 0;
		this.rect.sizeDelta = new Vector2(512f, -64f);
	}

	public void InitializeSections()
	{
		if (this.Stories == null)
		{
			this.Stories = new Dictionary<SDateTime, List<Newspaper.Story>>();
		}
	}

	public void AddReminder(string content, bool showNow = false)
	{
		if (showNow)
		{
			if (!this.Show)
			{
				this.ReminderPanel.SetActive(true);
				this.ReminderText.text = content;
			}
		}
		else
		{
			this.Reminders.Add(content);
			this.showReminder = true;
		}
	}

	private void OnDestroy()
	{
		Newspaper.Instance = null;
	}

	public void UpdateAllStories()
	{
		Newspaper.UpdateStories();
	}

	public static void UpdateStories()
	{
		if (Newspaper.Instance == null)
		{
			return;
		}
		Newspaper.Instance.PrefabCounter = 0;
		Newspaper.Instance.ColumnCounter = 0;
		List<Newspaper.Story> list = null;
		if (Newspaper.Instance.Stories.TryGetValue(Newspaper.Instance.CurrentDate, out list))
		{
			Newspaper.Section selected = (Newspaper.Section)Newspaper.Instance.SectionSelector.Selected;
			foreach (Newspaper.Story story in list)
			{
				if (selected == Newspaper.Section.All || story.Section == selected)
				{
					Newspaper.Instance.AddStory(story);
				}
			}
		}
		for (int i = Newspaper.Instance.PrefabCounter; i < Newspaper.Instance.CurrentContent.Count; i++)
		{
			Newspaper.Instance.CurrentContent[i].gameObject.SetActive(false);
		}
		Newspaper.Instance.ScrollArea.verticalNormalizedPosition = 1f;
	}

	public static void StoryRollover(SDateTime time, bool clear = true, bool presim = false)
	{
		if (Newspaper.Instance == null)
		{
			return;
		}
		if (!presim)
		{
			if (Newspaper.Instance.showReminder)
			{
				if (!Newspaper.Instance.Show)
				{
					Newspaper.Instance.ReminderPanel.SetActive(true);
					Newspaper.Instance.ReminderText.text = string.Join("\n", Newspaper.Instance.Reminders.ToArray<string>());
				}
				Newspaper.Instance.Reminders.Clear();
				Newspaper.Instance.showReminder = false;
			}
			else
			{
				Newspaper.Instance.ReminderPanel.SetActive(false);
			}
		}
	}

	private void AddNewStory(SDateTime date, Newspaper.Story story)
	{
		date = date.Simplify();
		List<Newspaper.Story> list = null;
		if (!this.Stories.TryGetValue(date, out list))
		{
			list = new List<Newspaper.Story>();
			this.Stories[date] = list;
		}
		list.Add(story);
	}

	public static void GenerateProductReview(SoftwareProduct product)
	{
		if (Newspaper.Instance == null)
		{
			return;
		}
		if (product.DevCompany.Player)
		{
			Newspaper.Instance.AddReminder("ReviewReminder".Loc(), false);
		}
		Newspaper.Instance.AddNewStory(SDateTime.Now() + new SDateTime(1, 0, 0), new Newspaper.Story("SoftwareReviewHeader".Loc(new object[]
		{
			product.Name
		}), ArticleGenerator.GenerateSoftwareReview(product), Newspaper.Section.Review, null, float.PositiveInfinity));
	}

	public static void GeneratePressbuildReview(ArticleGenerator.PressBuildReviewData d)
	{
		if (Newspaper.Instance == null)
		{
			return;
		}
		Newspaper.Instance.AddReminder("PressBuildReviewReminder".Loc(), false);
		Newspaper.Instance.AddNewStory(SDateTime.Now(), new Newspaper.Story("PressBuildReviewHeader".Loc(new object[]
		{
			d.Product
		}), ArticleGenerator.GeneratePressBuildReview(d), Newspaper.Section.Review, null, float.PositiveInfinity));
	}

	public static void GeneratePressReleaseReview(ArticleGenerator.PressReleaseData d)
	{
		if (Newspaper.Instance == null)
		{
			return;
		}
		Newspaper.Instance.AddReminder("PressReleaseReviewReminder".Loc(), true);
		Newspaper.Instance.AddNewStory(SDateTime.Now(), new Newspaper.Story("PressReleaseReviewHeader".Loc(new object[]
		{
			d.Company,
			d.Product
		}), ArticleGenerator.GeneratePressReleaseReview(d), Newspaper.Section.Review, null, float.PositiveInfinity));
	}

	public static void GenerateStockBuyout(Company target, Company[] sources, float amount)
	{
		if (Newspaper.Instance == null)
		{
			return;
		}
		float priority = target.Fans * Mathf.Max(1f, amount);
		if (sources.Contains(GameSettings.Instance.MyCompany))
		{
			Newspaper.Instance.AddReminder("Your company was mentioned in regards to a company takeover.", false);
			priority = float.PositiveInfinity;
		}
		string[] values = sources.SelectInPlace((Company x) => x.Name);
		string title = string.Format(GameData.SentenceGen["TakeOver"].GenerateSentence("StartTitle", new float[]
		{
			amount,
			target.BusinessReputation,
			(float)sources.Length,
			(float)target.Products.Count
		}), new object[]
		{
			target.Name,
			Newspaper.MakeList(values),
			target.Products.Count.ToString(),
			target.Money.Currency(true)
		});
		string content = string.Format(GameData.SentenceGen["TakeOver"].GenerateSentence("Start", new float[]
		{
			amount,
			target.BusinessReputation,
			(float)sources.Length,
			(float)target.Products.Count
		}), new object[]
		{
			target.Name,
			Newspaper.MakeList(values),
			target.Products.Count.ToString(),
			target.Money.Currency(true)
		});
		Newspaper.Instance.AddNewStory(SDateTime.Now() + new SDateTime(0, 23, 0, 0, 0), new Newspaper.Story(title, content, Newspaper.Section.Business, null, priority));
	}

	public static void GenerateEmployeeComplaint(Employee employee, string complaint)
	{
		if (Newspaper.Instance == null)
		{
			return;
		}
		Newspaper.Instance.AddReminder("A disgruntled employee went to the press.", false);
		string title = string.Format(GameData.SentenceGen["Scandal"].GenerateSentence("TitleStart", new float[]
		{
			0f,
			1f
		}), GameSettings.Instance.MyCompany.Name, complaint, employee.FullName);
		string content = string.Format(GameData.SentenceGen["Scandal"].GenerateSentence("ContentStart", new float[]
		{
			0f,
			1f
		}), GameSettings.Instance.MyCompany.Name, complaint, employee.FullName);
		Newspaper.Instance.AddNewStory(SDateTime.Now() + new SDateTime(1, 0, 0), new Newspaper.Story(title, content, Newspaper.Section.Industry, null, float.PositiveInfinity));
	}

	public static void GenerateGrowth(Company company, float amount)
	{
		if (Newspaper.Instance == null)
		{
			return;
		}
		float priority = amount / company.Money * company.Fans;
		string title = string.Format("Large growth this month for {0}", company.Name);
		string content = string.Format("{0} made a whopping {1} this month, making them worth {2} with {3} releases in their catalog.", new object[]
		{
			company.Name,
			amount.Currency(true),
			company.GetMoneyWithInsurance(true).Currency(true),
			company.Products.Count.ToString()
		});
		Newspaper.Instance.AddNewStory(SDateTime.Now(), new Newspaper.Story(title, content, Newspaper.Section.Business, null, priority));
	}

	public static string MakeList(IList<string> values)
	{
		if (values.Count == 0)
		{
			return "Nobody".Loc();
		}
		if (values.Count == 1)
		{
			return values[0];
		}
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < values.Count - 1; i++)
		{
			if (i > 0)
			{
				stringBuilder.Append(", ");
			}
			stringBuilder.Append(values[i]);
		}
		stringBuilder.Append("AndSeperator".Loc());
		stringBuilder.Append(values[values.Count - 1]);
		return stringBuilder.ToString();
	}

	private void AddStory(Newspaper.Story story)
	{
		GUIStory guistory;
		if (this.PrefabCounter < this.CurrentContent.Count)
		{
			guistory = this.CurrentContent[this.PrefabCounter];
			guistory.gameObject.SetActive(true);
		}
		else
		{
			guistory = UnityEngine.Object.Instantiate<GameObject>(this.StoryPrefab).GetComponent<GUIStory>();
			this.CurrentContent.Add(guistory);
		}
		guistory.Title.text = story.Title;
		guistory.Content.text = story.Content;
		guistory.transform.SetParent(this.Columns[this.ColumnCounter].transform, false);
		this.ColumnCounter = (this.ColumnCounter + 1) % this.Columns.Length;
		this.PrefabCounter++;
	}

	public void ShowNow(bool show)
	{
		if (this.Show == show || DOTween.IsTweening(this.rect))
		{
			return;
		}
		this.ReminderPanel.SetActive(false);
		if (show)
		{
			this.SetDate(SDateTime.Now(), true, false);
			this.ScrollArea.verticalNormalizedPosition = 1f;
			ShortcutExtensions46.DOSizeDelta(this.rect, new Vector2(512f, 512f), 0.2f, true);
			UISoundFX.PlaySFX("NewspaperOpen", -1f, 0f);
		}
		else
		{
			ShortcutExtensions46.DOSizeDelta(this.rect, new Vector2(512f, -64f), 0.2f, true);
			UISoundFX.PlaySFX("NewspaperClose", -1f, 0f);
		}
		this.Show = show;
	}

	public void SetDate(SDateTime date, bool force, bool back)
	{
		SDateTime sd = SDateTime.Now().Simplify();
		date = date.Simplify();
		if (force || (SDateTime.Now() > date && date.Year > 0))
		{
			if (!force)
			{
				while (!date.Equals(sd, true) && !this.ValidDate(date) && date.Year >= 0)
				{
					if (back)
					{
						date += new SDateTime(-1, 0, 0);
					}
					else
					{
						date += new SDateTime(1, 0, 0);
					}
				}
				if (date.Year < 0)
				{
					return;
				}
			}
			this.CurrentDate = date;
			this.DatePick.CurrentDate = date;
			Newspaper.UpdateStories();
		}
	}

	private bool ValidDate(SDateTime date)
	{
		Newspaper.Section selected = (Newspaper.Section)this.SectionSelector.Selected;
		if (selected == Newspaper.Section.All)
		{
			return this.Stories.ContainsKey(date);
		}
		List<Newspaper.Story> list = null;
		if (this.Stories.TryGetValue(date, out list))
		{
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i].Section == selected)
				{
					return true;
				}
			}
			return false;
		}
		return false;
	}

	public void ChangeDateDirect(SDateTime date)
	{
		this.SetDate(date, true, false);
	}

	public void ChangeDate(int change)
	{
		this.SetDate(this.CurrentDate + new SDateTime(change, 0, 0), false, change < 0);
	}

	private void Update()
	{
		if (this.Show && !this.SectionSelector.IsShown && Input.GetMouseButton(0) && !RectTransformUtility.RectangleContainsScreenPoint(this.rect, Input.mousePosition, null))
		{
			this.ShowNow(false);
		}
	}

	public RectTransform rect;

	private bool Show;

	public GameObject[] Columns;

	public GameObject ReminderPanel;

	public GameObject StoryPrefab;

	public ScrollRect ScrollArea;

	public GUICombobox SectionSelector;

	public Text ReminderText;

	private int ColumnCounter;

	[NonSerialized]
	public Dictionary<SDateTime, List<Newspaper.Story>> Stories = new Dictionary<SDateTime, List<Newspaper.Story>>();

	private List<GUIStory> CurrentContent = new List<GUIStory>();

	public static Newspaper Instance;

	private HashSet<string> Reminders = new HashSet<string>();

	private bool showReminder;

	[NonSerialized]
	public SDateTime CurrentDate;

	[NonSerialized]
	private int PrefabCounter;

	public DatePicker DatePick;

	public enum Section
	{
		All,
		Review,
		Industry,
		Business
	}

	[Serializable]
	public struct Story
	{
		public Story(string title, string content, Newspaper.Section section, string image, float priority)
		{
			this.Title = title;
			this.Content = content;
			this.Section = section;
			this.Image = image;
			this.Priority = priority;
		}

		public string Title;

		public string Content;

		public string Image;

		public Newspaper.Section Section;

		public float Priority;
	}
}
