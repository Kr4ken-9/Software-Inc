﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ErrorLogging : MonoBehaviour
{
	private void Awake()
	{
		if (ErrorLogging.Created)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else
		{
			this.unbind = true;
			ErrorLogging.Created = true;
			Application.logMessageReceived += this.HandleLog;
			SceneManager.activeSceneChanged += this.SceneChange;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		}
	}

	private void SceneChange(Scene arg0, Scene scene)
	{
		ErrorLogging.SceneChanging = false;
	}

	private void Start()
	{
		if (ErrorLogging.First)
		{
			Options.UpdateResolution();
			ErrorLogging.First = false;
		}
	}

	private void OnDestroy()
	{
		if (this.unbind)
		{
			Application.logMessageReceived -= this.HandleLog;
			SceneManager.activeSceneChanged -= this.SceneChange;
		}
	}

	public static void AddException(Exception ex)
	{
		object manuals = ErrorLogging.Manuals;
		lock (manuals)
		{
			ErrorLogging.Manuals.Add(ex);
		}
	}

	private void Update()
	{
		if (ErrorLogging.Manuals.Count > 0)
		{
			object manuals = ErrorLogging.Manuals;
			lock (manuals)
			{
				for (int i = 0; i < ErrorLogging.Manuals.Count; i++)
				{
					Debug.LogException(ErrorLogging.Manuals[i]);
				}
				ErrorLogging.Manuals.Clear();
			}
		}
	}

	private void HandleLog(string logString, string stackTrace, LogType type)
	{
		if (ErrorLogging.LoggedErrors > 50)
		{
			Application.logMessageReceived -= this.HandleLog;
			this.unbind = false;
			return;
		}
		if (type == LogType.Exception || type == LogType.Error)
		{
			string msg = logString + "\n" + stackTrace;
			if (!this.Logged.Contains(msg.GetHashCode()) && !msg.Contains("Failed to read input report") && !msg.Contains("Failed to create device file") && !stackTrace.Contains("UnityEngine.RectTransform.set_anchorMin") && !logString.Equals("RenderTexture.Create failed: width & height must be larger than 0"))
			{
				if (GameSettings.Instance != null)
				{
					GameSettings.Instance.Errors.Add(msg);
				}
				if (!msg.StartsWith("Ignore:") && !msg.Contains("Point on constrained edge not supported yet") && Options.AskReporting && ErrorLogging.LoggedErrors == 0 && FeedbackWindow.Instance != null)
				{
					if (GameSettings.Instance != null)
					{
						GameSettings.GameSpeed = 0f;
					}
					DialogWindow d = WindowManager.SpawnDialog();
					d.Show("ExceptionMessage".Loc(), false, DialogWindow.DialogType.Error, new KeyValuePair<string, Action>[]
					{
						new KeyValuePair<string, Action>("Yes", delegate
						{
							FeedbackWindow.Instance.Show(FeedbackWindow.ReportTypes.Exception, false, true, new string[0]);
							FeedbackWindow.Instance.Exception = msg;
							d.Window.Close();
						}),
						new KeyValuePair<string, Action>("See error", delegate
						{
							WindowManager.SpawnDialog(msg, true, DialogWindow.DialogType.Information);
						}),
						new KeyValuePair<string, Action>("No", delegate
						{
							d.Window.Close();
						}),
						new KeyValuePair<string, Action>("Never", delegate
						{
							Options.AskReporting = false;
							d.Window.Close();
						})
					});
				}
				base.StartCoroutine(this.PutLog(msg));
				ErrorLogging.LoggedErrors++;
			}
		}
	}

	private IEnumerator PutLog(string msg)
	{
		WWWForm form = new WWWForm();
		form.AddField("key", "SwincDebug");
		form.AddField("msg", msg);
		if (ErrorLogging.FirstOfScene)
		{
			form.AddField("first", "1");
			ErrorLogging.FirstOfScene = false;
		}
		form.AddField("client", Versioning.VersionString);
		form.AddField("version", Versioning.SimpleVersionString);
		form.AddField("modded", (!ErrorLogging.Modded) ? "0" : "1");
		WWW www = new WWW("https://SoftwareInc.Coredumping.com/ErrorLog.php", form);
		int hash = msg.GetHashCode();
		this.Logged.Add(hash);
		yield return www;
		if (!string.IsNullOrEmpty(www.error))
		{
			this.Logged.Remove(hash);
		}
		yield break;
	}

	public static bool First = true;

	public static bool Created = false;

	public static int LoggedErrors = 0;

	public static bool FirstOfScene = true;

	public static bool Modded = false;

	public static bool SceneChanging = false;

	private static readonly List<Exception> Manuals = new List<Exception>();

	public List<int> Logged = new List<int>();

	private bool unbind;
}
