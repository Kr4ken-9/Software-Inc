﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MultiSelectWindow : MonoBehaviour
{
	private void UpdateScrollVisibility()
	{
		bool flag = this.ScrollPanel.rect.height < (float)(this.BCount * 28 + 4 + ((!this.SearchBar.gameObject.activeSelf) ? 0 : 28));
		this.ScrollR.verticalScrollbar = ((!flag) ? null : this.Scrollbar);
		this.Scrollbar.gameObject.SetActive(flag);
		this.ContentPanel.offsetMax = new Vector2((float)((!flag) ? 0 : -18), this.ContentPanel.offsetMax.y);
		if (!flag)
		{
			this.ScrollR.verticalNormalizedPosition = 1f;
		}
	}

	private void DestroyButton(GameObject button)
	{
		button.transform.SetParent(null, false);
		button.SetActive(false);
		Button[] componentsInChildren = button.GetComponentsInChildren<Button>(true);
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			componentsInChildren[i].onClick.RemoveAllListeners();
		}
		this.buttonPool.Push(button);
	}

	private void DestroyToggle(GameObject toggle)
	{
		toggle.transform.SetParent(null, false);
		toggle.SetActive(false);
		toggle.GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
		this.togglePool.Push(toggle);
	}

	public void ShowMulti(string title, IEnumerable<string> values, bool[] selected, Action<int[]> func, bool modal = true, bool Localize = false, bool inverse = false)
	{
		this._toggleFinal = func;
		this.toggleMode = true;
		this._inverseToggle = inverse;
		this.ToggleButton.SetActive(true);
		this.ScrollPanel.offsetMin = new Vector2(this.ScrollPanel.offsetMin.x, 28f);
		this.ScrollR.verticalNormalizedPosition = 1f;
		if (this.Window.OnClose != null)
		{
			this.Window.OnClose();
			this.Window.OnClose = null;
		}
		List<string> list = values.ToList<string>();
		if (list.Count == 0)
		{
			return;
		}
		this.options.Clear();
		this.buttons.ForEach(delegate(GameObject x)
		{
			this.DestroyButton(x);
		});
		this.buttons.Clear();
		this.toggles.ForEach(delegate(GameObject x)
		{
			this.DestroyToggle(x);
		});
		this.toggles.Clear();
		bool flag = list.Count > 10;
		this.SearchBar.gameObject.SetActive(flag);
		this.Window.Title = title;
		this.Window.Modal = modal;
		float num = this.Window.MinSize.x - 32f;
		float b3 = 0f;
		this.AllToggle = this.CreateToggle("All".Loc(), out b3).GetComponent<Toggle>();
		num = Mathf.Max(num, b3);
		Toggle allToggle = this.AllToggle;
		bool isOn;
		if (selected != null)
		{
			isOn = !selected.Any((bool x) => !x);
		}
		else
		{
			isOn = false;
		}
		allToggle.isOn = isOn;
		this.AllToggle.onValueChanged.AddListener(delegate(bool b)
		{
			foreach (GameObject gameObject in this.toggles)
			{
				if (gameObject != this.AllToggle.gameObject)
				{
					gameObject.GetComponent<Toggle>().isOn = (b && gameObject.activeSelf);
				}
			}
		});
		this.BCount = 1;
		int num2 = 0;
		foreach (string text in list)
		{
			this.BCount++;
			float b2 = 0f;
			string text2 = (!Localize) ? text : text.Loc();
			this.options.Add(text2.ToLower());
			Toggle component = this.CreateToggle(text2, out b2).GetComponent<Toggle>();
			num = Mathf.Max(num, b2);
			component.isOn = (selected != null && selected[num2]);
			num2++;
		}
		this.Window.rectTransform.sizeDelta = new Vector2(num + 32f, Mathf.Max(this.Window.MinSize.y, (float)(Mathf.Min(16, this.BCount) * 28 + ((!flag) ? 64 : 92))));
		this.Window.Show();
		this.UpdateScrollVisibility();
		if (flag)
		{
			this.SearchBar.text = string.Empty;
			this.SearchBar.Select();
		}
	}

	public void ToggleOK()
	{
		if (this._toggleFinal != null)
		{
			List<int> list = new List<int>();
			for (int i = 1; i < this.toggles.Count; i++)
			{
				if (this._inverseToggle ^ this.toggles[i].GetComponent<Toggle>().isOn)
				{
					list.Add(i - 1);
				}
			}
			this._toggleFinal(list.ToArray());
		}
		this.Window.Close();
	}

	public void Show(string title, IEnumerable<string> values, Action<int> func, bool nullOption, bool closeOnChoice = true, bool modal = true, bool Localize = false, Action<int> deleteElement = null)
	{
		this.toggleMode = false;
		this.ToggleButton.SetActive(false);
		this.ScrollPanel.offsetMin = new Vector2(this.ScrollPanel.offsetMin.x, 20f);
		this.ScrollR.verticalNormalizedPosition = 1f;
		if (this.Window.OnClose != null)
		{
			this.Window.OnClose();
			this.Window.OnClose = null;
		}
		List<string> list = values.ToList<string>();
		if (list.Count == 0 && !nullOption)
		{
			return;
		}
		this.options.Clear();
		this.buttons.ForEach(new Action<GameObject>(this.DestroyButton));
		this.buttons.Clear();
		this.toggles.ForEach(new Action<GameObject>(this.DestroyToggle));
		this.toggles.Clear();
		bool flag = list.Count > 10;
		this.SearchBar.gameObject.SetActive(flag);
		this.Window.Title = title;
		this.Window.Modal = modal;
		float num = this.Window.MinSize.x - 32f;
		this.BCount = 0;
		if (nullOption)
		{
			this.BCount++;
			float b3 = 0f;
			string text = "None".Loc();
			this.options.Add(text.ToLower());
			Button[] componentsInChildren = this.CreateButton(text, out b3).GetComponentsInChildren<Button>(true);
			num = Mathf.Max(num, b3);
			componentsInChildren[0].onClick.AddListener(delegate
			{
				func(-1);
				if (closeOnChoice)
				{
					this.Window.Close();
				}
			});
		}
		int num2 = 0;
		foreach (string text2 in list)
		{
			this.BCount++;
			float b2 = 0f;
			string text3 = (!Localize) ? text2 : text2.Loc();
			this.options.Add(text3.ToLower());
			GameObject b = this.CreateButton(text3, out b2);
			Button[] componentsInChildren2 = b.GetComponentsInChildren<Button>(true);
			num = Mathf.Max(num, b2);
			int j = num2;
			componentsInChildren2[0].onClick.AddListener(delegate
			{
				func(j);
				if (closeOnChoice)
				{
					this.Window.Close();
				}
			});
			if (deleteElement != null)
			{
				componentsInChildren2[1].gameObject.SetActive(true);
				componentsInChildren2[1].onClick.AddListener(delegate
				{
					deleteElement(j);
					this.DestroyButton(b);
				});
			}
			num2++;
		}
		this.Window.rectTransform.sizeDelta = new Vector2(num + 32f, Mathf.Max(this.Window.MinSize.y, (float)(Mathf.Min(16, this.BCount) * 28 + ((!flag) ? 52 : 80))));
		this.Window.Show();
		this.UpdateScrollVisibility();
		if (flag)
		{
			this.SearchBar.text = string.Empty;
			this.SearchBar.Select();
		}
	}

	public void Show(string title, IEnumerable<string> values, Action<int> func, bool nullOption, bool closeOnChoice, bool modal, Action OnClose, bool Localize = false)
	{
		this.Show(title, values, func, nullOption, closeOnChoice, modal, Localize, null);
		this.Window.OnClose = delegate
		{
			OnClose();
			this.Window.OnClose = null;
		};
	}

	private void Start()
	{
		this.Window.OnSizeChanged = new Action(this.UpdateScrollVisibility);
	}

	public void SearchSubmit()
	{
		if (!this.toggleMode && (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter)))
		{
			for (int i = 0; i < this.buttons.Count; i++)
			{
				if (this.options[i].Contains(this.SearchBar.text.ToLower()))
				{
					this.buttons[i].GetComponentsInChildren<Button>()[0].onClick.Invoke();
					InputController.InputEnabled = true;
					break;
				}
			}
			this.SearchBar.ActivateInputField();
		}
	}

	public void SearchChanged()
	{
		this.BCount = ((!this.toggleMode) ? 0 : 1);
		List<GameObject> list = (!this.toggleMode) ? this.buttons : this.toggles;
		for (int i = (!this.toggleMode) ? 0 : 1; i < list.Count; i++)
		{
			bool flag = this.options[(!this.toggleMode) ? i : (i - 1)].Contains(this.SearchBar.text.ToLower());
			list[i].SetActive(flag);
			if (flag)
			{
				this.BCount++;
			}
		}
		this.UpdateScrollVisibility();
	}

	private GameObject CreateButton(string label, out float w)
	{
		GameObject gameObject = (this.buttonPool.Count <= 0) ? UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab) : this.buttonPool.Pop();
		this.buttons.Add(gameObject);
		Button[] componentsInChildren = gameObject.GetComponentsInChildren<Button>(true);
		componentsInChildren[1].gameObject.SetActive(false);
		Text componentInChildren = gameObject.GetComponentInChildren<Text>();
		componentInChildren.text = label;
		w = componentInChildren.preferredWidth;
		gameObject.transform.SetParent(this.Panel.transform, false);
		gameObject.SetActive(true);
		return gameObject;
	}

	private GameObject CreateToggle(string label, out float w)
	{
		GameObject gameObject = (this.togglePool.Count <= 0) ? UnityEngine.Object.Instantiate<GameObject>(this.TogglePrefab) : this.togglePool.Pop();
		this.toggles.Add(gameObject);
		Text componentInChildren = gameObject.GetComponentInChildren<Text>();
		componentInChildren.text = label;
		w = componentInChildren.preferredWidth + 23f;
		gameObject.transform.SetParent(this.Panel.transform, false);
		gameObject.SetActive(true);
		return gameObject;
	}

	public GUIWindow Window;

	public GameObject Panel;

	public GameObject ButtonPrefab;

	public GameObject TogglePrefab;

	public GameObject ToggleButton;

	private List<GameObject> buttons = new List<GameObject>();

	private List<GameObject> toggles = new List<GameObject>();

	private Stack<GameObject> buttonPool = new Stack<GameObject>();

	private Stack<GameObject> togglePool = new Stack<GameObject>();

	private List<string> options = new List<string>();

	public RectTransform ScrollPanel;

	public RectTransform ContentPanel;

	public Scrollbar Scrollbar;

	public ScrollRect ScrollR;

	public InputField SearchBar;

	private int BCount;

	private bool toggleMode;

	private bool _inverseToggle;

	private Action<int[]> _toggleFinal;

	public Toggle AllToggle;
}
