﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ReceptionAI : AI<Actor>
{
	public override void InitTree()
	{
		string name = "Spawn";
		if (ReceptionAI.<>f__mg$cache0 == null)
		{
			ReceptionAI.<>f__mg$cache0 = new Func<Actor, int>(ReceptionAI.Spawn);
		}
		BehaviorNode<Actor> behaviorNode = new BehaviorNode<Actor>(name, ReceptionAI.<>f__mg$cache0, true, -1);
		string name2 = "GoHome";
		if (ReceptionAI.<>f__mg$cache1 == null)
		{
			ReceptionAI.<>f__mg$cache1 = new Func<Actor, int>(Actor.GoHomeState);
		}
		BehaviorNode<Actor> behaviorNode2 = new BehaviorNode<Actor>(name2, ReceptionAI.<>f__mg$cache1, true, -1);
		string name3 = "Despawn";
		if (ReceptionAI.<>f__mg$cache2 == null)
		{
			ReceptionAI.<>f__mg$cache2 = new Func<Actor, int>(ReceptionAI.Despawn);
		}
		BehaviorNode<Actor> behaviorNode3 = new BehaviorNode<Actor>(name3, ReceptionAI.<>f__mg$cache2, true, -1);
		string name4 = "IsOff";
		if (ReceptionAI.<>f__mg$cache3 == null)
		{
			ReceptionAI.<>f__mg$cache3 = new Func<Actor, int>(ReceptionAI.IsOff);
		}
		BehaviorNode<Actor> behaviorNode4 = new BehaviorNode<Actor>(name4, ReceptionAI.<>f__mg$cache3, false, -1);
		string name5 = "GoToReceptionDesk";
		if (ReceptionAI.<>f__mg$cache4 == null)
		{
			ReceptionAI.<>f__mg$cache4 = new Func<Actor, int>(ReceptionAI.GoToReceptionDesk);
		}
		BehaviorNode<Actor> behaviorNode5 = new BehaviorNode<Actor>(name5, ReceptionAI.<>f__mg$cache4, true, -1);
		string name6 = "WaitAtDesk";
		if (ReceptionAI.<>f__mg$cache5 == null)
		{
			ReceptionAI.<>f__mg$cache5 = new Func<Actor, int>(ReceptionAI.WaitAtDesk);
		}
		BehaviorNode<Actor> behaviorNode6 = new BehaviorNode<Actor>(name6, ReceptionAI.<>f__mg$cache5, true, -1);
		string name7 = "Loiter";
		if (ReceptionAI.<>f__mg$cache6 == null)
		{
			ReceptionAI.<>f__mg$cache6 = new Func<Actor, int>(ReceptionAI.Loiter);
		}
		BehaviorNode<Actor> behaviorNode7 = new BehaviorNode<Actor>(name7, ReceptionAI.<>f__mg$cache6, true, 1);
		string name8 = "GoHomeBusStop";
		if (ReceptionAI.<>f__mg$cache7 == null)
		{
			ReceptionAI.<>f__mg$cache7 = new Func<Actor, int>(Actor.GoHomeBusStop);
		}
		BehaviorNode<Actor> behaviorNode8 = new BehaviorNode<Actor>(name8, ReceptionAI.<>f__mg$cache7, true, -1);
		string name9 = "ShouldUseBus";
		if (ReceptionAI.<>f__mg$cache8 == null)
		{
			ReceptionAI.<>f__mg$cache8 = new Func<Actor, int>(Actor.ShouldUseBus);
		}
		BehaviorNode<Actor> behaviorNode9 = new BehaviorNode<Actor>(name9, ReceptionAI.<>f__mg$cache8, true, -1);
		this.BehaviorNodes.Add("Spawn", behaviorNode);
		this.BehaviorNodes.Add("GoHome", behaviorNode2);
		this.BehaviorNodes.Add("Despawn", behaviorNode3);
		this.BehaviorNodes.Add("IsOff", behaviorNode4);
		this.BehaviorNodes.Add("GoToReceptionDesk", behaviorNode5);
		this.BehaviorNodes.Add("WaitAtDesk", behaviorNode6);
		this.BehaviorNodes.Add("Loiter", behaviorNode7);
		this.BehaviorNodes.Add("GoHomeBusStop", behaviorNode8);
		this.BehaviorNodes.Add("ShouldUseBus", behaviorNode9);
		behaviorNode.Success = behaviorNode5;
		behaviorNode5.Success = behaviorNode6;
		behaviorNode5.Failure = behaviorNode7;
		behaviorNode6.Success = behaviorNode4;
		behaviorNode4.Success = behaviorNode9;
		behaviorNode4.Failure = behaviorNode5;
		behaviorNode2.Success = behaviorNode3;
		behaviorNode2.Failure = behaviorNode8;
		behaviorNode3.Success = AI<Actor>.DummyNode;
		behaviorNode7.Success = behaviorNode4;
		behaviorNode9.Success = behaviorNode8;
		behaviorNode9.Failure = behaviorNode2;
		behaviorNode8.Success = behaviorNode3;
		behaviorNode8.Failure = behaviorNode7;
	}

	private static int Spawn(Actor self)
	{
		if (self.CurrentPath == null)
		{
			return 2;
		}
		return (!self.WalkPath()) ? 1 : 2;
	}

	private static int GoToReceptionDesk(Actor self)
	{
		int num = self.GoToFurniture("Desk", "Use", -1, true, null, false, true, null);
		if (self.UsingPoint == null)
		{
			return 0;
		}
		if (num == 2)
		{
			self.anim.SetEnum("AnimControl", self.UsingPoint.Animation);
			self.TurnToFurniture();
		}
		return num;
	}

	private static int WaitAtDesk(Actor self)
	{
		if (self.UsingPoint == null)
		{
			return 2;
		}
		ReceptionDesk component = self.UsingPoint.Parent.GetComponent<ReceptionDesk>();
		component.Queue.RemoveAll((Actor x) => x == null);
		if (self.GoHomeNow)
		{
			foreach (Actor actor in component.Queue)
			{
				actor.CleaningRoom = null;
			}
			self.UsingPoint = null;
			return 2;
		}
		InteractionPoint interactionPoint = self.UsingPoint.Parent.GetInteractionPoint("Visit", true);
		if (interactionPoint.UsedBy == null && component.Queue.Count > 0)
		{
			Actor actor2 = component.Queue[0];
			component.Queue.Remove(actor2);
			actor2.UsingPoint = interactionPoint;
			interactionPoint.UsedBy = actor2;
		}
		return 1;
	}

	private static int Despawn(Actor self)
	{
		foreach (InteractionPoint interactionPoint in self.InQueue.Values)
		{
			interactionPoint.RemoveFromQueue(self);
		}
		self.InQueue.Clear();
		GameSettings.Instance.StaffSalaryDue += self.GetRealSalary() / (float)GameSettings.DaysPerMonth;
		if (self.OnCall)
		{
			UnityEngine.Object.Destroy(self.gameObject);
		}
		else
		{
			SDateTime sdateTime = SDateTime.Now();
			GameSettings.Instance.sActorManager.AddToAwaiting(self, new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), self.StaffOn - 1, sdateTime.Day + ((self.StaffOn <= TimeOfDay.Instance.Hour) ? 1 : 0), sdateTime.Month, sdateTime.Year), false, true);
			self.OnDespawn();
		}
		return 2;
	}

	private static int IsOff(Actor self)
	{
		if ((self.UsingPoint == null || self.UsingPoint.Parent.GetInteractionPoint("Visit", true) == null) && self.GoHomeNow)
		{
			self.FreeLoiterTable();
			if (self.UsingPoint != null)
			{
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
			return 2;
		}
		return 0;
	}

	private static int Loiter(Actor self)
	{
		return self.HandleLoiter(false);
	}

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache5;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache6;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache7;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache8;
}
