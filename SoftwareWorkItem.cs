﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
public abstract class SoftwareWorkItem : WorkItem
{
	public SoftwareWorkItem(string name) : base(name, null, -1)
	{
	}

	public SoftwareWorkItem()
	{
	}

	public SoftwareWorkItem(string name, string type, string category, Dictionary<string, uint> needs, uint[] os, float price, SDateTime start, Company company, uint? sequelTo, bool inHouse, float loss, string[] feat, ContractWork contract, string server, string server2) : base(name, contract, -1)
	{
		this.SoftwareName = name;
		this._type = type;
		this._category = category;
		this.Needs = needs;
		this.OSs = os;
		this.Price = price;
		this.DevStart = start;
		this.Features = feat;
		this.DevTime = this.Type.DevTime(this.Features, this._category, null) + (float)((!this.Type.OSSpecific) ? 0 : (this.OSs.Length - 1));
		this.Server = server;
		this.Server2 = server2;
		this.CodeArtRatio = this.Type.CodeArtRatio(this.Features);
		float[] balance = this.Type.GetBalance(this.Features);
		float num = 0f;
		for (int i = 0; i < balance.Length; i++)
		{
			num += balance[i];
		}
		this.Innovation = balance[0] / num;
		this.Stability = balance[1] / num;
		this.Usability = balance[2] / num;
		this.MaxBugs = SoftwareWorkItem.GetMaximumBugs(this.DevTime, this.Stability);
		this.MyCompany = company;
		this.SequelTo = sequelTo;
		this.InHouse = inHouse;
		this.Loss = loss;
		SoftwareProduct softwareProduct = (this.SequelTo == null) ? null : GameSettings.Instance.simulation.GetProduct(this.SequelTo.Value, false);
		this.ReEvaluateMaxFollowers();
		softwareProduct = ((softwareProduct == null || softwareProduct.Traded) ? null : softwareProduct);
		this.InitSpecs(this.Type.GetSpecializationMonthsCodeArt(feat, this._category, this.OSs, softwareProduct), false);
		this.SpecQuality = new float[this.Specs.Length];
		this.MaxQual = Mathf.Min(1f, this.Type.MaxQuality(this.Needs, this.Features, this._category));
		GameSettings.Instance.FollowerSimulation.Add(this);
	}

	public SoftwareWorkItem(string name, string type, string category, Dictionary<string, uint> needs, uint[] os, float price, SDateTime start, Company company, uint? sequelTo, string[] feat, bool inHouse, float loss, ContractWork contract, string server, string server2, Dictionary<string, float> spec, int siblingIndex, float followers, uint maxFollowers, float followerChange, SDateTime? releaseDate, int maxBugs) : base(name, contract, siblingIndex)
	{
		this.SoftwareName = name;
		this._type = type;
		this._category = category;
		this.Needs = needs;
		this.OSs = os;
		this.Price = price;
		this.DevStart = start;
		this.Features = feat;
		this.Needs = this.Type.GetNeeds(this.Features, this._category).ToDictionary((string x) => x, (string x) => this.Needs[x]);
		this.DevTime = this.Type.DevTime(this.Features, this._category, null) + (float)((!this.Type.OSSpecific) ? 0 : (this.OSs.Length - 1));
		this.Server = server;
		this.Server2 = server2;
		this.ReleaseDate = releaseDate;
		this.CodeArtRatio = this.Type.CodeArtRatio(this.Features);
		this.MaxBugs = maxBugs;
		float[] balance = this.Type.GetBalance(this.Features);
		float num = balance.Sum();
		this.Innovation = balance[0] / num;
		this.Stability = balance[1] / num;
		this.Usability = balance[2] / num;
		this.MyCompany = company;
		this.SequelTo = sequelTo;
		this.InHouse = inHouse;
		this.Loss = loss;
		this.MaxQual = Mathf.Min(1f, this.Type.MaxQuality(this.Needs, this.Features, this._category));
		this.MaxFollowers = maxFollowers;
		if (this.MaxFollowers == 0u)
		{
			this.MaxFollowers = 1u;
		}
		this.Followers = followers;
		this.FollowerChange = followerChange;
		SoftwareProduct softwareProduct = (this.SequelTo == null) ? null : GameSettings.Instance.simulation.GetProduct(this.SequelTo.Value, false);
		softwareProduct = ((softwareProduct == null || softwareProduct.Traded) ? null : softwareProduct);
		this.InitSpecs(this.Type.GetSpecializationMonthsCodeArt(feat, this._category, this.OSs, softwareProduct), true);
		this.SpecQuality = new float[this.Specs.Length];
		for (int i = 0; i < this.Specs.Length; i++)
		{
			this.SpecQuality[i] = spec[this.Specs[i]];
		}
		GameSettings.Instance.FollowerSimulation.Add(this);
	}

	public float Followers
	{
		get
		{
			return this._followers;
		}
		set
		{
			this._followers = Mathf.Clamp(value, 0f, this.MaxFollowers);
		}
	}

	public float PremarketingBoost
	{
		get
		{
			return Mathf.Clamp01(this.Followers / this.MaxFollowers);
		}
	}

	public SoftwareType Type
	{
		get
		{
			return GameSettings.Instance.SoftwareTypes[this._type];
		}
	}

	public SoftwareCategory SWCategory
	{
		get
		{
			return this.Type.Categories[this._category];
		}
	}

	public virtual object PromoteAction()
	{
		return null;
	}

	public override string Category()
	{
		string text = ((!this._category.Equals("Default")) ? (this._category.LocSWC(this._type) + " ") : string.Empty) + this._type.LocSW();
		if (this.ReleaseDate != null)
		{
			return this.ReleaseDate.Value.ToVeryCompactString() + " - " + text;
		}
		return text;
	}

	private void InitSpecs(Dictionary<string, float[]> months, bool alpha)
	{
		this.Specs = months.Keys.ToArray<string>();
		this.SpecDevTime = new float[this.Specs.Length, 2];
		int num = 0;
		SoftwareType type = this.Type;
		for (int i = 0; i < this.Specs.Length; i++)
		{
			float[] array = months[this.Specs[i]];
			if (array[0] > 0f)
			{
				num++;
			}
			this.SpecDevTime[i, 0] = array[0];
			this.SpecDevTime[i, 1] = array[1];
		}
		if (alpha && type.OSSpecific && num > 0)
		{
			float num2 = (float)(this.OSs.Length - 1) / (float)num;
			if (num2 > 0f)
			{
				for (int j = 0; j < this.Specs.Length; j++)
				{
					if (this.SpecDevTime[j, 0] > 0f)
					{
						this.SpecDevTime[j, 0] += num2;
					}
				}
			}
		}
	}

	public void ReEvaluateMaxFollowers()
	{
		this.MaxFollowers = (uint)(GameSettings.Instance.simulation.GetFollowerReach(this._type, this._category, this.Features, this.OSs) * GameSettings.Instance.MyCompany.GetReputation(this._type, this._category).WeightOne(0.9f));
		if (this.MaxFollowers == 0u)
		{
			this.MaxFollowers = 1u;
		}
		this.Followers = this.Followers;
	}

	public static float Lateness(SDateTime releaseDate)
	{
		return Utilities.GetMonths(new SDateTime(0, 0, 0, releaseDate.Month + 1, releaseDate.Year), SDateTime.Now());
	}

	public void SimulateFollowers(float delta)
	{
		if (this.deal != 0u || this.contract != null)
		{
			return;
		}
		this.PressBuildEffect = Mathf.Clamp01(this.PressBuildEffect + Utilities.PerDay(1f / this.DevTime, delta, false));
		this.PressReleaseEffect = Mathf.Clamp01(this.PressReleaseEffect + Utilities.PerDay(1f / this.DevTime, delta, false));
		if (this.ReleaseDate != null)
		{
			float num = SoftwareWorkItem.Lateness(this.ReleaseDate.Value);
			if (num > 0f)
			{
				if (this.Followers > 0f)
				{
					this.Followers -= Utilities.PerDay(this.Followers * (0.1f + num), delta, false);
					HUD.Instance.AddPopupMessage("LateProductRelease".Loc(new object[]
					{
						this.SoftwareName
					}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.LateProduct, 1);
				}
				return;
			}
		}
		this.Followers += Utilities.PerHour(this.FollowerChange, delta, false) / (float)GameSettings.DaysPerMonth;
		this.FollowerChange = Mathf.Lerp(this.FollowerChange, -this.Followers * 0.01f, Utilities.PerDay(1f, delta, false));
	}

	public static int GetMaximumBugs(float devTime, float stability)
	{
		float num = Utilities.RandomGaussClamped(1f - stability, 0.2f).MapRange(0f, 1f, 0.75f, 1.25f, false);
		return Mathf.RoundToInt(Mathf.Max(0f, devTime * SoftwareAlpha.BugLimitFactor * num));
	}

	[OnDeserialized]
	private void OnDeserilize(StreamingContext context)
	{
		if (this.Working == null)
		{
			this.Working = new SHashSet<uint>();
		}
	}

	public override void DevTeamChange()
	{
		this.UpdateWorking();
	}

	public override void PauseChange()
	{
		this.UpdateWorking();
	}

	public void UpdateWorking()
	{
		this.Working.Clear();
		if (base.Paused)
		{
			return;
		}
		foreach (string key in this.DevTeams)
		{
			Team team;
			if (GameSettings.Instance.sActorManager.Teams.TryGetValue(key, out team))
			{
				List<Actor> employeesDirect = team.GetEmployeesDirect();
				for (int i = 0; i < employeesDirect.Count; i++)
				{
					if (employeesDirect[i].isActiveAndEnabled && this.HasWork(employeesDirect[i]) == WorkItem.HasWorkReturn.True)
					{
						this.Working.Add(employeesDirect[i].DID);
					}
					else
					{
						this.Working.Remove(employeesDirect[i].DID);
					}
				}
			}
		}
	}

	public override int GUIWorkItemType()
	{
		return 1;
	}

	public void FixAutoDev()
	{
		if (this.AutoDev)
		{
			foreach (AutoDevWorkItem autoDevWorkItem in GameSettings.Instance.MyCompany.WorkItems.OfType<AutoDevWorkItem>())
			{
				for (int i = 0; i < autoDevWorkItem.Items.Count; i++)
				{
					AutoDevWorkItem.AutoDevItem autoDevItem = autoDevWorkItem.Items[i];
					if (autoDevItem.SWWorkItem == this)
					{
						autoDevWorkItem.Items.Remove(autoDevItem);
						if (this.SoftwareName.Equals(autoDevWorkItem.LastDesign))
						{
							autoDevWorkItem.LastDesign = null;
						}
						if (this.SoftwareName.Equals(autoDevWorkItem.LastAlpha))
						{
							autoDevWorkItem.LastAlpha = null;
						}
						if (this.SoftwareName.Equals(autoDevWorkItem.LastAlpha2))
						{
							autoDevWorkItem.LastAlpha2 = null;
						}
						return;
					}
				}
			}
		}
	}

	public override void AddCost(float cost)
	{
		this.Loss += cost;
	}

	public string SoftwareName;

	public string _type;

	public string _category;

	public Dictionary<string, uint> Needs;

	public uint[] OSs;

	public readonly float Innovation;

	public readonly float Stability;

	public readonly float Usability;

	public SDateTime DevStart;

	public float Delay;

	public int MaxBugs;

	public readonly float DevTime;

	public readonly Company MyCompany;

	public readonly uint? SequelTo;

	public readonly bool InHouse;

	public string[] Features;

	public readonly string Server;

	public string Server2;

	public float Loss;

	public float Price;

	public float MaxQual;

	public float PressReleaseEffect = 1f;

	public float PressBuildEffect = 1f;

	private float _followers;

	public uint MaxFollowers;

	public float FollowerChange;

	public SDateTime? ReleaseDate;

	public float CodeArtRatio;

	public SHashSet<uint> Working = new SHashSet<uint>();

	public string[] Specs;

	public float[,] SpecDevTime;

	public float[] SpecQuality;
}
