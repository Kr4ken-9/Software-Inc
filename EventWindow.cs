﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EventWindow : MonoBehaviour
{
	public void UpdateEvents()
	{
	}

	private void AddEventElement(string icon, string desc)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.EventElementPrefab);
		EventElement component = gameObject.GetComponent<EventElement>();
		component.Icon = icon;
		component.Description.text = desc;
		component.transform.SetParent(this.ContentPanel.transform, false);
		this.Events.Add(component);
	}

	public void Serialize(WriteDictionary result)
	{
		result["UpEvents"] = (from x in this.Events
		select new string[]
		{
			x.Icon,
			x.Description.text
		}).ToArray<string[]>();
	}

	public void Deserialize(WriteDictionary result)
	{
		foreach (EventElement eventElement in this.Events)
		{
			UnityEngine.Object.Destroy(eventElement.gameObject);
		}
		this.Events.Clear();
		string[][] array = result.Get<string[][]>("UpEvents", new string[0][]);
		foreach (string array3 in array)
		{
			this.AddEventElement(array3[0], array3[1]);
		}
	}

	public GUIWindow Window;

	public GameObject ContentPanel;

	public GameObject EventElementPrefab;

	public List<EventElement> Events = new List<EventElement>();

	public Image ButtonIcon;

	public Color Important;

	public Color NotImportant;
}
