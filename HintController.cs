﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HintController : MonoBehaviour
{
	private void Start()
	{
		if (HintController.Instance != null)
		{
			UnityEngine.Object.Destroy(HintController.Instance.gameObject);
		}
		HintController.Instance = this;
		base.gameObject.SetActive(false);
	}

	public void Show(HintController.Hints hint)
	{
		if (base.gameObject.activeSelf)
		{
			return;
		}
		this.CurrentHint = (int)hint;
		if (Options.HintsEnabled && Options.HintEnabled(this.CurrentHint))
		{
			this.rect.anchoredPosition = new Vector2(this.rect.anchoredPosition.x, -this.rect.sizeDelta.y);
			ShortcutExtensions46.DOAnchorPos(this.rect, new Vector2(this.rect.anchoredPosition.x, 0f), 0.2f, true);
			this.MainText.text = hint.ToString().Loc();
			base.gameObject.SetActive(true);
		}
	}

	public void Dismiss()
	{
		base.gameObject.SetActive(false);
		Options.UpdateHint(this.CurrentHint, false);
	}

	private void OnDestroy()
	{
		if (HintController.Instance == this)
		{
			HintController.Instance = null;
		}
	}

	public static HintController Instance;

	public RectTransform rect;

	public Text MainText;

	private int CurrentHint = -1;

	public enum Hints
	{
		HintEmployeeAssign,
		HintStaffAssign,
		HintMultipleBuild,
		HintQuickSelect,
		HintSelectFurnitureType,
		HintCopyColor,
		HintProductRelease,
		HintFurnitureCopyMultiple,
		HintEmployeeEducation,
		HintEmployeeSelection,
		HintAutoScaleSegment,
		HintElevatorBeam,
		HintGridSnapToWall,
		HintWorkItemActions,
		HintTeamSizeEffectiveness,
		DeleteKeyHintHint,
		LampTestHint
	}
}
