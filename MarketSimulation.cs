﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

[Serializable]
public class MarketSimulation
{
	public MarketSimulation(bool simulate, SDateTime time)
	{
		this.SimulateCompanies = simulate;
	}

	public MarketSimulation()
	{
	}

	public static float GetPhysicalVsDigital(SDateTime time)
	{
		int num = Mathf.Max(0, time.RealYear - 1970);
		if (num >= MarketSimulation.PhysicalVsDigital.Length - 1)
		{
			return MarketSimulation.PhysicalVsDigital[MarketSimulation.PhysicalVsDigital.Length - 1];
		}
		float num2 = ((float)time.Month + (float)time.Day / (float)GameSettings.DaysPerMonth) / 12f;
		return MarketSimulation.PhysicalVsDigital[num + 1] * num2 + MarketSimulation.PhysicalVsDigital[num] * (1f - num2);
	}

	public int CompanyCount
	{
		get
		{
			return this.AllCompanies.Count;
		}
	}

	public IStockable GetStockable(uint id)
	{
		IStockable result;
		if (this._stockableCache.TryGetValue(id, out result))
		{
			return result;
		}
		SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(id, false);
		if (product != null)
		{
			this._stockableCache[id] = product;
			return product;
		}
		foreach (WorkItem workItem in GameSettings.Instance.MyCompany.WorkItems)
		{
			SoftwareAlpha softwareAlpha = workItem as SoftwareAlpha;
			if (softwareAlpha != null && softwareAlpha.SWID != null && softwareAlpha.SWID.Value == id)
			{
				this._stockableCache[id] = softwareAlpha;
				return softwareAlpha;
			}
		}
		foreach (SimulatedCompany simulatedCompany in GameSettings.Instance.simulation.Companies.Values)
		{
			for (int i = 0; i < simulatedCompany.Releases.Count; i++)
			{
				SimulatedCompany.ProductPrototype productPrototype = simulatedCompany.Releases[i];
				if (productPrototype.SWID != null && productPrototype.SWID == id)
				{
					this._stockableCache[id] = productPrototype;
					return productPrototype;
				}
			}
		}
		return null;
	}

	public void ClearStockable(uint ID)
	{
		this._stockableCache.Remove(ID);
	}

	public IEnumerable<Company> GetAllCompanies()
	{
		for (int i = 0; i < this.AllCompanies.Count; i++)
		{
			yield return this.AllCompanies[i];
		}
		yield break;
	}

	public void RemoveMock(SoftwareProduct p)
	{
		this.MockProducts.Remove(p.ID);
	}

	public void AddProduct(SoftwareProduct p, bool mock)
	{
		if (mock)
		{
			this.MockProducts[p.ID] = p;
		}
		else
		{
			this.Products[p.ID] = p;
		}
	}

	public void UpgradeFromMock(SoftwareProduct p)
	{
		SoftwareProduct product = this.GetProduct(p.ID, true);
		if (product != null && product.IsMock)
		{
			product.MockSucceeded = true;
			this.MockProducts.Remove(p.ID);
		}
		this.AddProduct(p, false);
	}

	public SoftwareProduct GetProduct(uint ID, bool withMock)
	{
		SoftwareProduct result;
		if (this.Products.TryGetValue(ID, out result))
		{
			return result;
		}
		if (withMock && this.MockProducts.TryGetValue(ID, out result))
		{
			return result;
		}
		return null;
	}

	public Dictionary<uint, SoftwareProduct>.ValueCollection GetAllProducts()
	{
		return this.Products.Values;
	}

	public IEnumerable<SoftwareProduct> GetProductsWithMock()
	{
		foreach (SoftwareProduct value in this.Products.Values)
		{
			yield return value;
		}
		foreach (SoftwareProduct value2 in this.MockProducts.Values)
		{
			yield return value2;
		}
		yield break;
	}

	public void BindCompanyUpdate(EventList<object> target)
	{
		this.AllCompanies.OnChange = delegate
		{
			this.AllCompanies.Update<object>(target);
		};
	}

	public uint GetProductIDFromName(string name)
	{
		SoftwareProduct softwareProduct = this.Products.Values.FirstOrDefault((SoftwareProduct x) => x.Name.Equals(name));
		if (softwareProduct == null)
		{
			throw new UnityException("Failed to find ID for " + name);
		}
		return softwareProduct.ID;
	}

	public void AddCompany(Company company)
	{
		this.AllCompanies.Add(company);
		this.AllCompanyDict[company.ID] = company;
		if (company is SimulatedCompany)
		{
			this.Companies.Add(company.ID, company as SimulatedCompany);
		}
	}

	public void RemoveCompany(Company company)
	{
		foreach (uint id in company.Subsidiaries)
		{
			Company company2 = this.GetCompany(id);
			company2.OwnerCompany = null;
		}
		company.OwnerCompany = null;
		this.AllCompanyDict.Remove(company.ID);
		this.Companies.Remove(company.ID);
		if (company is EventCompany)
		{
			this.EventCompanies.Remove(company as EventCompany);
		}
		this.AllCompanies.Remove(company);
	}

	public uint GetID()
	{
		uint result = this.nextID;
		this.nextID += 1u;
		return result;
	}

	public uint GetCompanyID()
	{
		uint result = this.nextCompanyID;
		this.nextCompanyID += 1u;
		return result;
	}

	public uint GetDealID()
	{
		uint result = this.nextDealID;
		this.nextDealID += 1u;
		return result;
	}

	public Company GetCompany(uint id)
	{
		Company result;
		if (this.AllCompanyDict.TryGetValue(id, out result))
		{
			return result;
		}
		return null;
	}

	public KeyValuePair<Company, float> FindBuyer(uint client, uint target, float cost)
	{
		foreach (SimulatedCompany simulatedCompany in from x in this.Companies.Values
		where x != null && !x.IsSubsidiary() && x.Money * 0.2f > cost
		orderby x.OwnedStock.Count
		select x)
		{
			if (!simulatedCompany.Bankrupt && simulatedCompany.ID != target && simulatedCompany.ID != client)
			{
				return new KeyValuePair<Company, float>(simulatedCompany, cost);
			}
		}
		SimulatedCompany simulatedCompany2 = (from x in this.Companies.Values
		where x != null && !x.Bankrupt && x.ID != target && x.ID != client && x.Money > 5000f
		select x).MaxInstance((SimulatedCompany x) => x.Money);
		return (simulatedCompany2 != null) ? new KeyValuePair<Company, float>(simulatedCompany2, simulatedCompany2.Money * 0.2f) : new KeyValuePair<Company, float>(null, 0f);
	}

	public Company FindBuyer(IPDeal deal)
	{
		float worth = deal.Worth();
		return (from x in this.Companies.Values
		where !x.IsSubsidiary()
		select x).FirstOrDefault((SimulatedCompany x) => x.Money * 0.01f > worth);
	}

	public float GetQuality(SoftwareProduct product, SDateTime time, bool market)
	{
		return this.GetQuality(product._type, product._category, time, market);
	}

	private float GetPerOSQuality(SoftwareProduct product, SDateTime time, out uint osCov)
	{
		if (product.OSs == null || product.OSs.Count == 0)
		{
			osCov = 0u;
			return product.GetQuality(this.GetQuality(product, time, true), time, true);
		}
		float weightedQualityAddition = product.GetWeightedQualityAddition(time, true);
		float num = 0f;
		osCov = 0u;
		float num2 = 0f;
		List<SoftwareProduct> compatibleOS = this.GetCompatibleOS(product, false, null);
		for (int i = 0; i < compatibleOS.Count; i++)
		{
			num2 = Mathf.Max(num2, (float)compatibleOS[i].Userbase);
		}
		KeyValuePair<string, string> key = new KeyValuePair<string, string>(product._type, product._category);
		for (int j = 0; j < compatibleOS.Count; j++)
		{
			SoftwareProduct softwareProduct = compatibleOS[j];
			if (softwareProduct.Userbase > 0)
			{
				float num3 = 1f;
				osCov += (uint)softwareProduct.Userbase;
				Dictionary<KeyValuePair<string, string>, float> orNull = this.PerOSQuality.GetOrNull(softwareProduct.ID);
				if (orNull != null)
				{
					float orDefault = orNull.GetOrDefault(key, 0f);
					if (orDefault > 0f && orDefault >= weightedQualityAddition)
					{
						num3 = weightedQualityAddition / orDefault;
					}
				}
				num += num3 * ((float)softwareProduct.Userbase / num2);
			}
		}
		return num;
	}

	public float GetQuality(string type, string category, SDateTime time, bool market)
	{
		if (!time.Equals(this.LastCache, true))
		{
			this.CacheQuality(time);
		}
		float result = 1f;
		if (market)
		{
			this.CachedMarketQuality.TryGetValue(new KeyValuePair<string, string>(type, category), out result);
		}
		else
		{
			this.CachedQuality.TryGetValue(new KeyValuePair<string, string>(type, category), out result);
		}
		return result;
	}

	public float GetFeatureScore(SoftwareProduct product, SDateTime time)
	{
		return this.GetFeatureScore(product._type, product._category, time);
	}

	public float GetFeatureScore(string type, string category, SDateTime time)
	{
		if (!time.Equals(this.LastFCache, true))
		{
			this.CacheFeatures();
			this.LastFCache = time;
		}
		return this.NewCachedFeatures.GetOrDefault(new KeyValuePair<string, string>(type, category), 0f);
	}

	public float GetMaxAwareness(SoftwareProduct p)
	{
		if (this.Awareness.Count == 0)
		{
			this.CalculateAwareness();
		}
		return this.Awareness[new KeyValuePair<string, string>(p._type, p._category)];
	}

	private void CalculateAwareness()
	{
		foreach (SoftwareType softwareType in GameSettings.Instance.SoftwareTypes.Values)
		{
			foreach (string value in softwareType.Categories.Keys)
			{
				this.Awareness[new KeyValuePair<string, string>(softwareType.Name, value)] = 1f;
			}
		}
		foreach (SoftwareProduct softwareProduct in this.Products.Values)
		{
			KeyValuePair<string, string> key = new KeyValuePair<string, string>(softwareProduct._type, softwareProduct._category);
			this.Awareness[key] = Mathf.Max(this.Awareness[key], softwareProduct.GetRealAwareness());
		}
	}

	private void CacheQuality(SDateTime time)
	{
		this.LastCache = time;
		this.CachedQuality.Clear();
		this.CachedMarketQuality.Clear();
		this.PerOSQuality.Clear();
		this.CalculateAwareness();
		Dictionary<KeyValuePair<string, string>, float> dict = new Dictionary<KeyValuePair<string, string>, float>();
		foreach (SimulatedCompany simulatedCompany in this.Companies.Values)
		{
			SimulatedCompany simulatedCompany2 = simulatedCompany;
			for (int i = 0; i < simulatedCompany.Releases.Count; i++)
			{
				SimulatedCompany.ProductPrototype productPrototype = simulatedCompany.Releases[i];
				float value = Utilities.GetHypeFactor(productPrototype.DevTime, productPrototype.Reception, simulatedCompany2.GetReputation(productPrototype.Type, productPrototype.Category), productPrototype.Price, productPrototype.ReleaseDate, time) * MarketSimulation.HypeFactor;
				dict.AddUp(new KeyValuePair<string, string>(productPrototype.Type, productPrototype.Category), value);
				if (productPrototype.OSs != null)
				{
					KeyValuePair<string, string> key = new KeyValuePair<string, string>(productPrototype.Type, productPrototype.Category);
					for (int j = 0; j < productPrototype.OSs.Length; j++)
					{
						SoftwareProduct product = this.GetProduct(productPrototype.OSs[j], false);
						if (product != null && product.Userbase > 0)
						{
							this.PerOSQuality.Append(product.ID).AddUp(key, value);
						}
					}
				}
			}
		}
		foreach (SoftwareWorkItem softwareWorkItem in GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareWorkItem>())
		{
			if (softwareWorkItem.ReleaseDate != null)
			{
				float value2 = Utilities.GetHypeFactor(softwareWorkItem.DevTime, softwareWorkItem.PremarketingBoost, GameSettings.Instance.MyCompany.GetReputation(softwareWorkItem._type, softwareWorkItem._category), softwareWorkItem.Price, softwareWorkItem.ReleaseDate.Value, time) * MarketSimulation.HypeFactor;
				dict.AddUp(new KeyValuePair<string, string>(softwareWorkItem._type, softwareWorkItem._category), value2);
				if (softwareWorkItem.OSs != null)
				{
					KeyValuePair<string, string> key2 = new KeyValuePair<string, string>(softwareWorkItem._type, softwareWorkItem._category);
					for (int k = 0; k < softwareWorkItem.OSs.Length; k++)
					{
						SoftwareProduct product2 = this.GetProduct(softwareWorkItem.OSs[k], false);
						if (product2 != null && product2.Userbase > 0)
						{
							this.PerOSQuality.Append(product2.ID).AddUp(key2, value2);
						}
					}
				}
			}
		}
		List<SoftwareProduct> list = new List<SoftwareProduct>();
		foreach (SoftwareType softwareType in GameSettings.Instance.SoftwareTypes.Values)
		{
			foreach (string value3 in softwareType.Categories.Keys)
			{
				float num = 0f;
				float num2 = 0f;
				KeyValuePair<string, string> key3 = new KeyValuePair<string, string>(softwareType.Name, value3);
				foreach (SoftwareProduct softwareProduct in this.Products.Values)
				{
					if (!softwareProduct.InHouse && !softwareProduct.DevCompany.Bankrupt && softwareProduct._type.Equals(softwareType.Name) && softwareProduct._category.Equals(value3))
					{
						num2 += softwareProduct.GetWeightedQualityAddition(time, false);
						float weightedQualityAddition = softwareProduct.GetWeightedQualityAddition(time, true);
						num += weightedQualityAddition;
						if (softwareType.OSSpecific && softwareProduct.OSs != null && weightedQualityAddition > 0f)
						{
							List<SoftwareProduct> compatibleOS = this.GetCompatibleOS(softwareProduct, false, list);
							for (int l = 0; l < compatibleOS.Count; l++)
							{
								SoftwareProduct softwareProduct2 = compatibleOS[l];
								if (softwareProduct2.Userbase > 0)
								{
									this.PerOSQuality.Append(softwareProduct2.ID).AddUp(key3, weightedQualityAddition);
								}
							}
							list.Clear();
						}
					}
				}
				this.CachedQuality[key3] = num2;
				this.CachedMarketQuality[key3] = Mathf.Min(num, dict.GetOrDefault(key3, 0f)) + num;
			}
		}
	}

	private void CacheFeatures()
	{
		this.NewCachedFeatures.Clear();
		foreach (KeyValuePair<string, SoftwareType> keyValuePair in GameSettings.Instance.SoftwareTypes)
		{
			foreach (KeyValuePair<string, SoftwareCategory> keyValuePair2 in keyValuePair.Value.Categories)
			{
				float num = 0f;
				foreach (KeyValuePair<uint, SoftwareProduct> keyValuePair3 in this.Products)
				{
					if (keyValuePair3.Value._type.Equals(keyValuePair.Key) && keyValuePair3.Value._category.Equals(keyValuePair2.Key))
					{
						num = Mathf.Max(num, keyValuePair3.Value.FeatureScore);
					}
				}
				this.NewCachedFeatures[new KeyValuePair<string, string>(keyValuePair.Key, keyValuePair2.Key)] = num;
			}
		}
	}

	public void InitialReleases(SDateTime time)
	{
		foreach (CompanyType companyType in GameSettings.Instance.CompanyTypes.Values)
		{
			if (companyType.Force != null)
			{
				string[] array = companyType.Force.Split(new char[]
				{
					','
				});
				SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[array[0]];
				SoftwareCategory softwareCategory = (softwareType.Categories.Count != 1) ? softwareType.Categories[array[1]] : softwareType.Categories.Values.First<SoftwareCategory>();
				if (softwareType.IsUnlocked(time.Year) && softwareCategory.IsUnlocked(time.Year))
				{
					SimulatedCompany simulatedCompany = new SimulatedCompany(this.GenerateCompanyName(companyType.NameGen), time, companyType, companyType.GetTypes(), 1f);
					this.AddCompany(simulatedCompany);
					SoftwareProduct softwareProduct = simulatedCompany.ReleaseNow(softwareType, softwareCategory, time);
					if (softwareProduct != null)
					{
						this.Products.Add(softwareProduct.ID, softwareProduct);
					}
				}
			}
		}
	}

	private float TimeAudienceRentention(SDateTime release, SDateTime time, float retention)
	{
		float months = Utilities.GetMonths(release, time);
		int num = 60;
		if (months > (float)num)
		{
			return Mathf.Pow(retention, months - (float)num);
		}
		return 1f;
	}

	private int[] FixSales(int sales, SoftwareProduct p, float physical, float refunds, int follows, float quality)
	{
		if (sales == 0)
		{
			p.MissedPhysicalSales = 0;
			return new int[]
			{
				0,
				0,
				0
			};
		}
		int num = (follows != 0) ? Mathf.RoundToInt((float)follows * Mathf.Pow(Mathf.Max(0f, 1f - quality * 1.4f), 1.5f)) : 0;
		int num2 = Mathf.Min(sales, Mathf.Max(Mathf.FloorToInt((float)sales * refunds), num));
		int num3 = Mathf.RoundToInt((float)num2 * physical);
		int num4 = Mathf.RoundToInt((float)sales * physical);
		int num5 = sales - num4;
		int num6 = Mathf.Min(num5, num2 - num3);
		if (p.PhysicalCopies == 0u)
		{
			if (num4 > 0 && p.DevCompany.Player && !p.Traded)
			{
				HUD.Instance.AddPopupMessage("LostSalesPopup".Loc(new object[]
				{
					p.Name
				}), "Exclamation", PopupManager.PopUpAction.OpenProductDetails, p.ID, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.None, 1);
			}
			p.MissedPhysicalSales = Mathf.Max(0, num4 - num3);
			return new int[]
			{
				0,
				num5 - num6,
				num6
			};
		}
		if ((long)num4 > (long)((ulong)p.PhysicalCopies))
		{
			if (p.DevCompany.Player && !p.Traded)
			{
				HUD.Instance.AddPopupMessage("LostSalesPopup".Loc(new object[]
				{
					p.Name
				}), "Exclamation", PopupManager.PopUpAction.OpenProductDetails, p.ID, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.None, 1);
			}
			num4 = (int)p.PhysicalCopies;
			num3 = Mathf.Max(Mathf.FloorToInt((float)num4 * refunds), Mathf.FloorToInt(physical * (float)num));
			p.MissedPhysicalSales = Mathf.Max(0, num4 - num3 - (int)p.PhysicalCopies);
			p.PhysicalCopies = 0u;
		}
		else
		{
			p.MissedPhysicalSales = 0;
			p.PhysicalCopies -= (uint)num4;
		}
		num3 = Mathf.Min(num4, num3);
		p.PhysicalCopies += (uint)num3;
		return new int[]
		{
			num4 - num3,
			num5 - num6,
			num6 + num3
		};
	}

	private bool PreSimValid(SoftwareProduct p)
	{
		List<float> cashflow = p.GetCashflow();
		List<int> unitSales = p.GetUnitSales(true);
		List<int> unitSales2 = p.GetUnitSales(false);
		List<int> refunds = p.GetRefunds();
		if (unitSales.Count < 5 || (unitSales.Count == cashflow.Count && cashflow[cashflow.Count - 1] > 0f) || unitSales[unitSales.Count - 1] > 0 || unitSales2[unitSales2.Count - 1] > 0 || refunds[refunds.Count - 1] > 0)
		{
			return true;
		}
		p.KillAwareness();
		return false;
	}

	public void SimulateMonth(SDateTime time, bool presim = false)
	{
		if (this.SimulateCompanies)
		{
			List<SoftwareProduct> list = new List<SoftwareProduct>();
			int num = 0;
			foreach (SimulatedCompany simulatedCompany in this.Companies.Values.ToArray<SimulatedCompany>())
			{
				if (this.Companies.ContainsKey(simulatedCompany.ID))
				{
					if (simulatedCompany.Bankrupt)
					{
						HUD.Instance.dealWindow.CancelCompanyDeals(simulatedCompany);
						this.Companies.Remove(simulatedCompany.ID);
						if (simulatedCompany.Products.Count == 0)
						{
							foreach (KeyValuePair<string, string> key6 in simulatedCompany.Patents.ToList<KeyValuePair<string, string>>())
							{
								key6.GetFeature().TransferPatent(null);
							}
							this.RemoveCompany(simulatedCompany);
						}
					}
					else
					{
						simulatedCompany.Simulate(time, list);
						for (int j = num; j < list.Count; j++)
						{
							this.Products.Add(list[j].ID, list[j]);
							this.ClearStockable(list[j].ID);
						}
						num = list.Count;
					}
				}
			}
			for (int k = 0; k < list.Count; k++)
			{
				SoftwareProduct softwareProduct = list[k];
				if (!softwareProduct.DevCompany.IsPlayerOwned())
				{
					if (softwareProduct.ServerReq > 0f && softwareProduct.ServerReq * softwareProduct.DevTime * Utilities.RandomGauss(1f - GameSettings.Instance.MyCompany.BusinessReputation * 0.5f, 0.2f) < GameSettings.Instance.MyCompany.BusinessReputation * 50f)
					{
						ServerDeal deal = new ServerDeal(softwareProduct);
						HUD.Instance.dealWindow.AddDeal(deal);
					}
					if (softwareProduct.DevTime * Utilities.RandomGauss(1f - GameSettings.Instance.MyCompany.BusinessReputation * 0.5f, 0.2f) < GameSettings.Instance.MyCompany.BusinessReputation * 50f)
					{
						WorkDeal deal2;
						if (Utilities.RandomValue > 0.5f)
						{
							deal2 = new WorkDeal(softwareProduct, WorkDeal.WorkType.Marketing, time, Mathf.CeilToInt(softwareProduct.DevTime / 4f));
						}
						else
						{
							deal2 = new WorkDeal(softwareProduct, WorkDeal.WorkType.Support, time, Mathf.CeilToInt(softwareProduct.DevTime * 2f));
						}
						HUD.Instance.dealWindow.AddDeal(deal2);
					}
				}
			}
		}
		float physicalVsDigital = MarketSimulation.GetPhysicalVsDigital(time);
		this.SimulateEventCompanies(time);
		foreach (SimulatedCompany simulatedCompany2 in this.Companies.Values)
		{
			simulatedCompany2.SimulateDistribution(this, time, physicalVsDigital);
		}
		List<SoftwareProduct> list2 = (from x in this.Products.Values
		where !x.InHouse && !x.DevCompany.Bankrupt && (!presim || this.PreSimValid(x))
		select x).ToList<SoftwareProduct>();
		if (presim && list2.Count > MarketSimulation.MaxPresim)
		{
			for (int l = list2.Count - MarketSimulation.MaxPresim - 1; l >= 0; l--)
			{
				list2[l].AddToCashflow(0, 0, 0, 0f, time);
			}
			for (int m = 0; m < MarketSimulation.MaxPresim; m++)
			{
				list2[m] = list2[list2.Count - MarketSimulation.MaxPresim + m];
			}
			for (int n = list2.Count - 1; n >= MarketSimulation.MaxPresim; n--)
			{
				list2.RemoveAt(n);
			}
		}
		List<SoftwareProduct> list3 = list2;
		Dictionary<uint, Dictionary<KeyValuePair<string, string>, float>> dict = new Dictionary<uint, Dictionary<KeyValuePair<string, string>, float>>();
		Dictionary<KeyValuePair<string, string>, int> dictionary = new Dictionary<KeyValuePair<string, string>, int>();
		Dictionary<KeyValuePair<string, string>, float> dictionary2 = new Dictionary<KeyValuePair<string, string>, float>();
		foreach (KeyValuePair<string, SoftwareType> keyValuePair in GameSettings.Instance.SoftwareTypes)
		{
			foreach (KeyValuePair<string, SoftwareCategory> keyValuePair2 in keyValuePair.Value.Categories)
			{
				KeyValuePair<string, string> key = new KeyValuePair<string, string>(keyValuePair.Key, keyValuePair2.Key);
				dictionary[key] = 0;
				List<SoftwareProduct> list4 = (from x in list3
				where x._type.Equals(key.Key) && x._category.Equals(key.Value) && Utilities.GetMonths(x.Release, time) < 60f
				select x).ToList<SoftwareProduct>();
				Dictionary<KeyValuePair<string, string>, float> dictionary3 = dictionary2;
				KeyValuePair<string, string> key2 = key;
				float value;
				if (list4.Count > 0)
				{
					float? num2 = list4.AverageOutlier((SoftwareProduct x) => x.Price / x.DevTime, 0.5f);
					value = ((num2 == null) ? Utilities.GetBasePrice(keyValuePair.Key, keyValuePair2.Key, 1f, 1f) : num2.Value);
				}
				else
				{
					value = Utilities.GetBasePrice(keyValuePair.Key, keyValuePair2.Key, 1f, 1f);
				}
				dictionary3[key2] = value;
			}
		}
		List<SoftwareProduct> list5;
		if (presim)
		{
			list5 = (from x in this.Products.Values
			where !x.InHouse && !x.DevCompany.Bankrupt
			select x).ToList<SoftwareProduct>();
		}
		else
		{
			list5 = list3;
		}
		List<SoftwareProduct> list6 = list5;
		for (int num3 = 0; num3 < list6.Count; num3++)
		{
			SoftwareProduct softwareProduct2 = list6[num3];
			Dictionary<KeyValuePair<string, string>, int> dictionary4;
			KeyValuePair<string, string> key3;
			(dictionary4 = dictionary)[key3 = new KeyValuePair<string, string>(softwareProduct2._type, softwareProduct2._category)] = dictionary4[key3] + softwareProduct2.Userbase;
		}
		float num4 = 0f;
		for (int num5 = 0; num5 < list6.Count; num5++)
		{
			SoftwareProduct softwareProduct3 = list6[num5];
			float retention = softwareProduct3.Category.Retention;
			float p = (softwareProduct3.Userbase != 0 && softwareProduct3.Sequel == null) ? (retention + (1f - retention) * 0.99f * softwareProduct3.RelativeFeatureScore(this, time) * this.TimeAudienceRentention(softwareProduct3.Release, time, retention) * Mathf.Min(1f, (float)softwareProduct3.Userbase / (float)dictionary[new KeyValuePair<string, string>(softwareProduct3._type, softwareProduct3._category)] / 0.25f)) : retention;
			softwareProduct3.Userbase = Mathf.FloorToInt((float)softwareProduct3.Userbase * p.SpreadPercentage(GameSettings.DaysPerMonth));
			if (softwareProduct3._type.Equals("Operating System"))
			{
				num4 += (float)softwareProduct3.Userbase;
			}
			softwareProduct3.SimulateAwareness();
		}
		this.CacheQuality(time);
		float num6 = MarketSimulation.TimeFactor[time.Month];
		uint num7 = 0u;
		uint num8 = 0u;
		foreach (SoftwareProduct softwareProduct4 in from x in list3
		orderby (!x._type.Equals("Operating System")) ? 0 : 1
		select x)
		{
			KeyValuePair<string, string> keyValuePair3 = new KeyValuePair<string, string>(softwareProduct4._type, softwareProduct4._category);
			if (softwareProduct4.OSs != null && !softwareProduct4._type.Equals("Operating System"))
			{
				for (int num9 = 0; num9 < softwareProduct4.OSs.Count; num9++)
				{
					uint key4 = softwareProduct4.OSs[num9];
					Dictionary<KeyValuePair<string, string>, float> dict2 = dict.Append(key4);
					KeyValuePair<string, string> key5 = keyValuePair3;
					float weightedQualityAddition = softwareProduct4.GetWeightedQualityAddition(time, true);
					if (MarketSimulation.<>f__mg$cache0 == null)
					{
						MarketSimulation.<>f__mg$cache0 = new Func<float, float, float>(Mathf.Max);
					}
					dict2.AddTo(key5, weightedQualityAddition, MarketSimulation.<>f__mg$cache0);
				}
			}
			uint num10 = 0u;
			SoftwareType type = softwareProduct4.Type;
			float num11 = this.GetPerOSQuality(softwareProduct4, time, out num10);
			if (!float.IsNaN(num11) && !float.IsInfinity(num11))
			{
				if (softwareProduct4._type.Equals("Operating System"))
				{
					float num12 = 0f;
					Dictionary<KeyValuePair<string, string>, float> orNull = dict.GetOrNull(softwareProduct4.ID);
					if (orNull != null)
					{
						num12 = orNull.Average(delegate(KeyValuePair<KeyValuePair<string, string>, float> x)
						{
							float orDefault = this.CachedMarketQuality.GetOrDefault(x.Key, 0f);
							if (orDefault == 0f)
							{
								return 0f;
							}
							return x.Value / orDefault;
						});
					}
					if (!float.IsInfinity(num12) && !float.IsNaN(num12))
					{
						float num13 = softwareProduct4.GetTime(time, 0.1f) * 0.5f;
						num11 += num13 * num12;
					}
				}
				float months = Utilities.GetMonths(softwareProduct4.Release, time);
				float num14 = Mathf.Pow(softwareProduct4.InnovationPoint(), months);
				int num15 = 0;
				float num17;
				if (!softwareProduct4.OpenSource)
				{
					float num16 = Mathf.Pow(softwareProduct4.DevTimePoint(), months);
					num17 = num11 * num16 * (float)MarketSimulation.MagicFactor * softwareProduct4.Category.Popularity.WeightOne(0.1f) * softwareProduct4.RealQuality * num6;
					if (softwareProduct4.DevCompany.Player)
					{
						num17 *= GameSettings.Instance.Difficulty.MapRange(0f, 2f, 2f, 1f, false);
					}
					num17 /= (float)GameSettings.DaysPerMonth;
					int num18 = 0;
					if (softwareProduct4.Followers > 0u)
					{
						num18 = ((softwareProduct4.Followers >= 100u) ? Mathf.RoundToInt(softwareProduct4.Followers * Utilities.RandomGaussClamped(0.25f, 0.1f) / (float)GameSettings.DaysPerMonth) : ((int)softwareProduct4.Followers));
						if ((long)num18 >= (long)((ulong)softwareProduct4.Followers))
						{
							softwareProduct4.Followers = 0u;
						}
						else
						{
							softwareProduct4.Followers -= (uint)num18;
						}
						if (num18 > 0)
						{
							num15 = Mathf.RoundToInt(Mathf.Clamp01(1f - softwareProduct4.Quality * 2f) * (float)num18);
						}
						if ((float)num18 > num17)
						{
							num17 = (float)num18;
						}
					}
					if (type.OSSpecific)
					{
						float num19 = (type.OSLimit == null) ? num4 : ((float)dictionary[new KeyValuePair<string, string>("Operating System", type.OSLimit)]);
						float num20 = num17;
						num17 = ((num10 >= softwareProduct4.UnitSum) ? Mathf.Min(num10 - softwareProduct4.UnitSum, Mathf.Round(num17)) : 0f);
						uint productID = softwareProduct4.ID;
						if (!softwareProduct4.HadOSWarning && !softwareProduct4.Traded && softwareProduct4.DevCompany.Player && num17 == 0f && num20 > 1000f && num10 / num19 < 0.75f && GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwarePort>().None((SoftwarePort x) => x._product == productID) && this.CheckAnyValidOS(softwareProduct4, type))
						{
							softwareProduct4.HadOSWarning = true;
							HUD.Instance.AddPopupMessage("ProductOSAudienceZero".Loc(new object[]
							{
								softwareProduct4.Name
							}), "Info", PopupManager.PopUpAction.OpenProductDetails, softwareProduct4.ID, PopupManager.NotificationSound.Warning, 0.5f, PopupManager.PopupIDs.None, 1);
						}
					}
					else
					{
						num17 = Mathf.Min(Mathf.Max(0f, softwareProduct4.Category.Popularity * MarketSimulation.Population - softwareProduct4.UnitSum), Mathf.Round(num17));
					}
					num17 = Mathf.Max(0f, num17);
					float num21 = softwareProduct4.Price / softwareProduct4.DevTime / dictionary2[keyValuePair3];
					if (num21 > 1.75f)
					{
						num17 *= num21.MapRange(1.75f, 2f, 1f, 0f, true);
						if (num21 > 2f)
						{
							num17 = 0f;
						}
						else
						{
							num17 *= 1f - (num21 - 1.75f) * 4f;
						}
					}
					float refunds = Mathf.Pow(Mathf.Min(0.75f, (float)softwareProduct4.Bugs / (softwareProduct4.DevTime * SoftwareAlpha.BugLimitFactor)), 2f);
					int[] array2 = this.FixSales(Mathf.RoundToInt(num17), softwareProduct4, physicalVsDigital, refunds, num18, softwareProduct4.Quality);
					if (GameSettings.Instance.Distribution != null && (softwareProduct4.DevCompany.Player || softwareProduct4.DevCompany.IsPlayerOwned() || softwareProduct4.DevCompany.DistributionDeal != null) && GameSettings.Instance.Distribution.LastLoad < 1f)
					{
						array2[1] = Mathf.RoundToInt((float)array2[1] * GameSettings.Instance.Distribution.LastLoad);
					}
					num7 += (uint)array2[1];
					int num22 = array2[0] + array2[1];
					float num23 = (float)array2[0] * softwareProduct4.Price;
					float num24 = (float)array2[1] * softwareProduct4.Price;
					float num25 = num23 + num24;
					float num26 = num23 * MarketSimulation.DistributionStandardCut;
					float num27 = 0f;
					if (GameSettings.Instance.Distribution != null && (softwareProduct4.DevCompany.Player || softwareProduct4.DevCompany.IsPlayerOwned()))
					{
						num8 += (uint)array2[1];
					}
					else if (softwareProduct4.DevCompany.DistributionDeal != null)
					{
						num8 += (uint)array2[1];
						float num28 = num24 * softwareProduct4.DevCompany.DistributionDeal.Value;
						num27 += num28;
						GameSettings.Instance.MyCompany.MakeTransaction(num28, Company.TransactionCategory.Distribution, "Digital store");
					}
					else
					{
						num27 += num24 * MarketSimulation.DistributionStandardCut;
					}
					float num29 = 0f;
					if (num25 > 0f)
					{
						List<KeyValuePair<uint, Feature>> royalties = softwareProduct4.GetRoyalties();
						if (royalties != null)
						{
							for (int num30 = 0; num30 < royalties.Count; num30++)
							{
								Company company = this.GetCompany(royalties[num30].Key);
								if (company != null)
								{
									float num31 = num25 * royalties[num30].Value.Royalty;
									if (company.Player)
									{
										royalties[num30].Value.Income += num31;
									}
									company.MakeTransaction(num31, Company.TransactionCategory.Royalties, softwareProduct4.Name);
									num29 += num31;
								}
							}
						}
					}
					if (!float.IsNaN(num25) && !float.IsInfinity(num25))
					{
						softwareProduct4.Userbase += num22;
						if (type.OSSpecific && (long)softwareProduct4.Userbase > (long)((ulong)num10))
						{
							softwareProduct4.Userbase = (int)num10;
						}
						softwareProduct4.AddToCashflow(array2[1], array2[0], array2[2], num25 - num27 - num26 - num29, time);
						softwareProduct4.DevCompany.MakeTransaction(num25, Company.TransactionCategory.Sales, null);
						softwareProduct4.DevCompany.MakeTransaction(-num26, Company.TransactionCategory.Distribution, "Physical store cut");
						softwareProduct4.DevCompany.MakeTransaction(-num27, Company.TransactionCategory.Distribution, "Digital store cut");
						softwareProduct4.DevCompany.MakeTransaction(-num29, Company.TransactionCategory.Royalties, softwareProduct4.Name);
					}
				}
				else
				{
					num17 = num11 * (float)MarketSimulation.MagicFactor * softwareProduct4.Category.Popularity.WeightOne(0.15f);
					num17 /= (float)GameSettings.DaysPerMonth;
					if (type.OSSpecific)
					{
						num17 = Mathf.Min(Mathf.Max(0f, num10 - softwareProduct4.UnitSum), Mathf.Round(num17));
					}
					else
					{
						num17 = Mathf.Min(Mathf.Max(0f, softwareProduct4.Category.Popularity * MarketSimulation.Population - softwareProduct4.UnitSum), Mathf.Round(num17));
					}
					num17 = Mathf.Max(0f, num17);
					int[] array3 = this.FixSales((int)num17, softwareProduct4, physicalVsDigital, 0f, 0, 1f);
					softwareProduct4.Userbase += array3[0] + array3[1];
					if (type.OSSpecific && (long)softwareProduct4.Userbase > (long)((ulong)num10))
					{
						softwareProduct4.Userbase = (int)num10;
					}
					softwareProduct4.AddToCashflow(array3[1], array3[0], 0, 0f, time);
				}
				if (!softwareProduct4.Traded && num17 > 0f)
				{
					float num32 = 1f + 0.2f * (softwareProduct4.UsabilityPoint() - 0.4f) * num14;
					float num33 = (0.5f + num14 * 0.5f) * num11 * softwareProduct4.RandomPoint() + num32;
					num33 *= Mathf.Lerp(1f, softwareProduct4.SequelBonus, softwareProduct4.GetTime(time, 0.15f));
					int num34 = Mathf.RoundToInt((num33 - 1f) * num17 * 4f / (float)GameSettings.DaysPerMonth) - num15;
					int monthsFlat = Utilities.GetMonthsFlat(softwareProduct4.Release, time);
					if (num34 != 0)
					{
						for (int num35 = softwareProduct4.Rep.Count; num35 < monthsFlat; num35++)
						{
							softwareProduct4.Rep.Add(0f);
						}
						if (softwareProduct4.Rep.Count == 0)
						{
							softwareProduct4.Rep.Add(0f);
						}
						List<float> rep;
						int index;
						(rep = softwareProduct4.Rep)[index = softwareProduct4.Rep.Count - 1] = rep[index] + (float)num34;
					}
					else if (softwareProduct4.Rep.Count > 0 && softwareProduct4.Rep[softwareProduct4.Rep.Count - 1] > 0f)
					{
						softwareProduct4.Rep.Add(0f);
					}
					softwareProduct4.DevCompany.AddFans(num34, softwareProduct4._type, softwareProduct4._category);
				}
			}
		}
		if (GameSettings.Instance.Distribution != null)
		{
			GameSettings.Instance.Distribution.ItemSales = num8;
		}
		float item = num8 / num7;
		this.PlayerDigitalShare.RemoveAt(0);
		this.PlayerDigitalShare.Add(item);
		if (this.SimulateCompanies)
		{
			this.GenerateCompany(time);
		}
	}

	private void SimulateEventCompanies(SDateTime time)
	{
		foreach (EventCompany eventCompany in from x in this.EventCompanies
		where Utilities.GetMonthsFlat(x.Founded, time) == 0
		select x)
		{
			this.AllCompanies.Add(eventCompany);
			this.AllCompanyDict[eventCompany.ID] = eventCompany;
		}
		foreach (EventCompany eventCompany2 in from x in this.EventCompanies
		where Utilities.GetMonthsFlat(x.Founded, time) >= 0
		select x)
		{
			foreach (SoftwareProduct softwareProduct in eventCompany2.Simulate(time, this))
			{
				this.Products.Add(softwareProduct.ID, softwareProduct);
			}
		}
	}

	private bool CheckAnyValidOS(SoftwareProduct product, SoftwareType type)
	{
		IEnumerable<SoftwareProduct> enumerable;
		if (type.OSNeed != null)
		{
			enumerable = this.GetCompatibleOS(product.Needs[type.OSNeed], true);
		}
		else
		{
			enumerable = from x in this.GetProductsWithMock()
			where x._type.Equals("Operating System")
			select x;
		}
		IEnumerable<SoftwareProduct> enumerable2 = enumerable;
		if (type.OSLimit != null)
		{
			enumerable2 = from x in enumerable2
			where x._category.Equals(type.OSLimit)
			select x;
		}
		HashSet<string> hashSet = new HashSet<string>();
		foreach (Feature feature in from z in product.Features
		select type.Features[z])
		{
			feature.FindDependencies("Operating System", hashSet, GameSettings.Instance.SoftwareTypes, type);
		}
		foreach (SoftwareProduct softwareProduct in enumerable2)
		{
			if (softwareProduct.Userbase >= 1000)
			{
				if (!product.OSs.Contains(softwareProduct.ID))
				{
					bool flag = false;
					foreach (string feature2 in hashSet)
					{
						if (!SoftwareType.DependencyMet(feature2, softwareProduct))
						{
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public List<SoftwareProduct> GetCompatibleOS(uint p, bool withMock = false)
	{
		return this.GetCompatibleOS(this.Products[p], withMock, null);
	}

	public List<SoftwareProduct> GetCompatibleOS(SoftwareProduct p, bool withMock = false, List<SoftwareProduct> reusableList = null)
	{
		MarketSimulation.CompatibleCache.Clear();
		for (int i = 0; i < p.OSs.Count; i++)
		{
			SoftwareProduct product = this.GetProduct(p.OSs[i], withMock);
			if (product != null)
			{
				MarketSimulation.CompatibleCache.Add(product);
				this.GetSequels(product, MarketSimulation.CompatibleCache, 3, true);
			}
		}
		if (reusableList != null)
		{
			reusableList.AddRange(MarketSimulation.CompatibleCache);
			return reusableList;
		}
		return MarketSimulation.CompatibleCache.ToList<SoftwareProduct>();
	}

	public void GetSequels(SoftwareProduct p, HashSet<SoftwareProduct> output, int limit = -1, bool sameCategory = true)
	{
		SoftwareProduct sequel = p.Sequel;
		int num = limit;
		while (sequel != null && num != 0)
		{
			if (!sameCategory || sequel._category.Equals(p._category))
			{
				output.Add(sequel);
			}
			sequel = sequel.Sequel;
			num--;
		}
	}

	public uint GetOSCoverage(IList<uint> OSs)
	{
		MarketSimulation.CoverageCache.Clear();
		uint num = 0u;
		for (int i = 0; i < OSs.Count; i++)
		{
			SoftwareProduct product = this.GetProduct(OSs[i], true);
			SoftwareProduct softwareProduct = product;
			int num2 = 4;
			while (softwareProduct != null && num2 != 0 && !MarketSimulation.CoverageCache.Contains(softwareProduct))
			{
				MarketSimulation.CoverageCache.Add(softwareProduct);
				if (softwareProduct._category.Equals(product._category))
				{
					num += (uint)this.GetUserBase(softwareProduct);
				}
				softwareProduct = softwareProduct.Sequel;
				num2--;
			}
		}
		return num;
	}

	private int GetUserBase(SoftwareProduct os)
	{
		return (!os.IsMock) ? os.Userbase : Mathf.FloorToInt(os.MockWork.Followers);
	}

	private void GenerateCompany(SDateTime time)
	{
		using (IEnumerator<CompanyType> enumerator = (from x in GameSettings.Instance.CompanyTypes.Values
		where x.IsValid(time.Year)
		select x).GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				CompanyType type = enumerator.Current;
				int num = this.Companies.Values.Count((SimulatedCompany x) => x.Type == type);
				if ((num < type.Max || type.Max == 0) && (num < type.Min || Utilities.RandomValue < type.PerYear / (12f * (float)GameSettings.DaysPerMonth)))
				{
					SimulatedCompany company = new SimulatedCompany(this.GenerateCompanyName(type.NameGen), time, type, type.GetTypes(), Utilities.RandomGaussClamped(0.5f + GameSettings.Instance.Difficulty.MapRange(0f, 2f, 0.25f, 0.5f, false), 0.2f));
					this.AddCompany(company);
				}
			}
		}
	}

	public string GenerateCompanyName(string nameGen)
	{
		RandomNameGenerator randomNameGenerator = GameSettings.Instance.RNG[nameGen ?? "Company"];
		string[] result = new string[]
		{
			randomNameGenerator.GenerateName()
		};
		int num = 0;
		while (this.AllCompanies.Any((Company x) => x.Name.Equals(result[0])) || this.EventCompanies.Any((EventCompany x) => x.Name.Equals(result[0])))
		{
			if (num > 10000)
			{
				Debug.Log("Tried 10,000 times to find company name and failed");
				while (this.AllCompanies.Any((Company x) => x.Name.Equals(result[0])) || this.EventCompanies.Any((EventCompany x) => x.Name.Equals(result[0])))
				{
					result[0] = result[0] + "E";
				}
				return result[0];
			}
			result[0] = randomNameGenerator.GenerateName();
			num++;
		}
		return result[0];
	}

	public string GenerateProductName(SoftwareType type, string cat)
	{
		return this.GenerateProductName(type, type.Categories[cat]);
	}

	public string GenerateProductName(SoftwareType type, SoftwareCategory cat)
	{
		RandomNameGenerator nameGen = cat.GetNameGen(GameSettings.Instance);
		string text = nameGen.GenerateName();
		HashSet<string> hashSet = (from x in this.GetProductsWithMock()
		select x.Name).ToHashSet<string>();
		hashSet.AddRange(from x in GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareWorkItem>()
		select x.SoftwareName);
		hashSet.AddRange(this.Companies.Values.SelectMany((SimulatedCompany x) => from z in x.Releases
		select z.Name));
		hashSet.AddRange(this.Companies.Values.SelectMany((SimulatedCompany x) => from z in x.ProjectQueue
		select z.Name));
		for (int i = 0; i < 1000; i++)
		{
			if (!hashSet.Contains(text))
			{
				return text;
			}
			text = nameGen.GenerateName();
		}
		Debug.Log("Tried 1000 times to generate product name and failed!");
		return text;
	}

	public string GenerateProductSequalName(string input)
	{
		HashSet<string> hashSet = (from x in this.GetProductsWithMock()
		select x.Name).ToHashSet<string>();
		hashSet.AddRange(from x in GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareWorkItem>()
		select x.SoftwareName);
		hashSet.AddRange(this.Companies.Values.SelectMany((SimulatedCompany x) => from z in x.Releases
		select z.Name));
		hashSet.AddRange(this.Companies.Values.SelectMany((SimulatedCompany x) => from z in x.ProjectQueue
		select z.Name));
		string[] array = input.Split(new char[]
		{
			' '
		});
		int num;
		string str;
		if (int.TryParse(array[array.Length - 1], out num))
		{
			str = string.Join(" ", array.Take(array.Length - 1).ToArray<string>());
			num++;
		}
		else
		{
			str = input;
			num = 2;
		}
		while (hashSet.Contains(input))
		{
			input = str + " " + num.ToString();
			num++;
		}
		return input;
	}

	public uint GetFollowerReach(string type, string category, string[] features, IList<uint> OSs)
	{
		SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[type];
		SoftwareCategory softwareCategory = softwareType.Categories[category];
		uint reach = softwareType.GetReach(softwareCategory, features, OSs);
		uint num = (uint)(MarketSimulation.FollowerPopulation * softwareCategory.Popularity);
		return (num >= reach) ? reach : num;
	}

	public void TurnLoss()
	{
		foreach (KeyValuePair<uint, SoftwareProduct> keyValuePair in this.Products)
		{
			keyValuePair.Value.TurnLoss();
		}
	}

	private Dictionary<uint, SoftwareProduct> Products = new Dictionary<uint, SoftwareProduct>();

	private Dictionary<uint, SoftwareProduct> MockProducts = new Dictionary<uint, SoftwareProduct>();

	public Dictionary<uint, SimulatedCompany> Companies = new Dictionary<uint, SimulatedCompany>();

	public List<EventCompany> EventCompanies = new List<EventCompany>();

	private EventList<Company> AllCompanies = new EventList<Company>();

	private Dictionary<uint, Company> AllCompanyDict = new Dictionary<uint, Company>();

	public static readonly int MagicFactor = 96543;

	public static readonly int MagicRepFactor = 500000;

	public static readonly uint Population = 145613576u;

	public static readonly uint FollowerPopulation = 7613576u;

	public static readonly float HypeFactor = 0.05f;

	public readonly bool SimulateCompanies;

	private SDateTime LastCache = new SDateTime(-1, -1);

	private SDateTime LastFCache = new SDateTime(-1, -1);

	private Dictionary<KeyValuePair<string, string>, float> CachedMarketQuality = new Dictionary<KeyValuePair<string, string>, float>();

	private Dictionary<KeyValuePair<string, string>, float> CachedQuality = new Dictionary<KeyValuePair<string, string>, float>();

	private Dictionary<uint, Dictionary<KeyValuePair<string, string>, float>> PerOSQuality = new Dictionary<uint, Dictionary<KeyValuePair<string, string>, float>>();

	private Dictionary<KeyValuePair<string, string>, float> Awareness = new Dictionary<KeyValuePair<string, string>, float>();

	private Dictionary<KeyValuePair<string, string>, float> NewCachedFeatures = new Dictionary<KeyValuePair<string, string>, float>();

	private uint nextID = 1u;

	private uint nextCompanyID = 1u;

	private uint nextDealID = 1u;

	public Dictionary<string, Dictionary<string, int>> ConsumerBuckets;

	[NonSerialized]
	private Dictionary<uint, IStockable> _stockableCache = new Dictionary<uint, IStockable>();

	public static float[] TimeFactor = new float[]
	{
		1.02850676f,
		0.939027131f,
		0.9897778f,
		1.04507375f,
		1.04944372f,
		1.00489283f,
		0.948164344f,
		0.9280029f,
		0.982416451f,
		1.11593866f,
		1.27689135f,
		1.33464694f
	};

	public static float[] PhysicalVsDigital = new float[]
	{
		1f,
		0.99f,
		0.99f,
		0.99f,
		0.99f,
		0.99f,
		0.98f,
		0.98f,
		0.98f,
		0.98f,
		0.97f,
		0.97f,
		0.97f,
		0.96f,
		0.96f,
		0.96f,
		0.95f,
		0.95f,
		0.95f,
		0.94f,
		0.94f,
		0.94f,
		0.93f,
		0.93f,
		0.92f,
		0.92f,
		0.92f,
		0.91f,
		0.91f,
		0.9f,
		0.9f,
		0.89f,
		0.88f,
		0.88f,
		0.87f,
		0.86f,
		0.86f,
		0.85f,
		0.83f,
		0.8f,
		0.71f,
		0.68f,
		0.59f,
		0.53f,
		0.48f,
		0.44f,
		0.43f,
		0.42f,
		0.41f,
		0.41f,
		0.4f,
		0.39f
	};

	public List<float> PlayerDigitalShare = new List<float>
	{
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f,
		0f
	};

	public static float DistributionStandardCut = 0.3f;

	public static float PhysicalCopyPrice = 2f;

	private static int MaxPresim = 50;

	private static readonly HashSet<SoftwareProduct> CoverageCache = new HashSet<SoftwareProduct>();

	private static readonly HashSet<SoftwareProduct> CompatibleCache = new HashSet<SoftwareProduct>();

	[CompilerGenerated]
	private static Func<float, float, float> <>f__mg$cache0;
}
