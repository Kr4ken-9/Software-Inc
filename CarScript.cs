﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

public class CarScript : Writeable, IHasSpeed
{
	public void Reset()
	{
		this.WaitingFor.Clear();
		this.Parked = false;
		this.AudioE = true;
		this.LightsE = true;
		this.CanDestroy = true;
		this.Target = null;
		this.UpdateEmission(0f);
		for (int i = 0; i < this.Lights.Length; i++)
		{
			this.Lights[i].enabled = true;
		}
		for (int j = 0; j < this.SpawnPoints.Length; j++)
		{
			CarSpawn carSpawn = this.SpawnPoints[j];
			carSpawn.Reset();
		}
		NormalCar component = base.GetComponent<NormalCar>();
		if (component != null)
		{
			component.Reset();
		}
		BusScript component2 = base.GetComponent<BusScript>();
		if (component2 != null)
		{
			component2.Reset();
		}
	}

	public void UpdateColor(Color color)
	{
		this.MatBlock.SetColor("_Color1", color);
		for (int i = 0; i < this.CarRender.Length; i++)
		{
			this.CarRender[i].SetPropertyBlock(this.MatBlock);
		}
	}

	public void UpdateEmission(float emish)
	{
		this.MatBlock.SetFloat("_Emission", emish);
		for (int i = 0; i < this.CarRender.Length; i++)
		{
			this.CarRender[i].SetPropertyBlock(this.MatBlock);
		}
	}

	public void Init()
	{
		NormalCar component = base.GetComponent<NormalCar>();
		if (component != null)
		{
			component.Init();
		}
		BusScript component2 = base.GetComponent<BusScript>();
		if (component2 != null)
		{
			component2.Init();
		}
		for (int i = 0; i < this.Lights.Length; i++)
		{
			this.Lights[i].enabled = false;
		}
		TimeOfDay.Instance.canSkip = TimeOfDay.Instance.CanSkip();
	}

	public void DestroyEvent()
	{
		NormalCar component = base.GetComponent<NormalCar>();
		if (component != null)
		{
			component.DestroyEvent();
		}
		foreach (CarSpawn carSpawn in this.SpawnPoints)
		{
			foreach (Actor actor in carSpawn.Occupants)
			{
				if (actor.MyCar == this)
				{
					actor.MyCar = null;
				}
			}
			carSpawn.Occupants.Clear();
		}
	}

	private void Awake()
	{
		base.InitWritable();
		foreach (AudioSource audioSource in base.GetComponentsInChildren<AudioSource>())
		{
			audioSource.volume = 1f;
		}
		AudioSource[] componentsInChildren;
		this.AudioComp = componentsInChildren[0];
		this.SFX = componentsInChildren[1];
		this.MatBlock = new MaterialPropertyBlock();
		for (int j = 0; j < this.CarRender.Length; j++)
		{
			this.CarRender[j].SetPropertyBlock(this.MatBlock);
		}
		for (int k = 0; k < this.SpawnPoints.Length; k++)
		{
			this.SpawnPoints[k].ID = k;
			this.SpawnPoints[k].Parent = this;
		}
	}

	public bool AllDoorsClosed()
	{
		foreach (CarSpawn carSpawn in this.SpawnPoints)
		{
			if (carSpawn.OpenAmount > 0f)
			{
				return false;
			}
		}
		return true;
	}

	public void PlaySFX(AudioClip clip)
	{
		this.SFX.PlayOneShot(clip);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.activeInHierarchy)
		{
			if (other.tag.Equals("Carstop"))
			{
				CarScript component = other.transform.parent.GetComponent<CarScript>();
				if (!component.WaitingFor.Contains(this) && !component.Parked)
				{
					this.WaitingFor.Add(component);
				}
				return;
			}
			Rigidbody attachedRigidbody = other.attachedRigidbody;
			Actor actor = (!(attachedRigidbody != null)) ? null : attachedRigidbody.GetComponent<Actor>();
			if (actor != null && actor.isActiveAndEnabled && actor.currentRoom == GameSettings.Instance.sRoomManager.Outside)
			{
				this.WaitingFor.Add(actor);
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag.Equals("Carstop"))
		{
			CarScript component = other.transform.parent.GetComponent<CarScript>();
			this.WaitingFor.Remove(component);
			return;
		}
		Rigidbody attachedRigidbody = other.attachedRigidbody;
		Actor actor = (!(attachedRigidbody != null)) ? null : attachedRigidbody.GetComponent<Actor>();
		if (actor != null)
		{
			this.WaitingFor.Remove(actor);
		}
	}

	public float GetMaxSpeed(float angle)
	{
		if (this.WaitingFor.Count == 0)
		{
			return 0f;
		}
		int num = 0;
		float num2 = float.MaxValue;
		for (int i = 0; i < this.WaitingFor.Count; i++)
		{
			GameObject gameObject = this.WaitingFor[i].GetGameObject();
			if (gameObject == null || !gameObject.activeInHierarchy)
			{
				this.WaitingFor.RemoveAt(i);
				i--;
			}
			else
			{
				num++;
				num2 = Mathf.Min(this.WaitingFor[i].GetSpeed(angle), num2);
			}
		}
		return (num <= 0) ? 0f : num2;
	}

	private void OnDestroy()
	{
		if (GameSettings.IsQuitting)
		{
			return;
		}
		RoadManager.Instance.Cars.Remove(this);
		TimeOfDay.Instance.canSkip = TimeOfDay.Instance.CanSkip();
	}

	public Actor FirstActor()
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			foreach (Actor actor in this.SpawnPoints[i].Occupants)
			{
				if (actor != null)
				{
					return actor;
				}
			}
		}
		return null;
	}

	public bool ContainsOccupant(Actor x)
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			CarSpawn carSpawn = this.SpawnPoints[i];
			if (carSpawn.Occupants.Contains(x))
			{
				return true;
			}
		}
		return false;
	}

	public bool AnyOccupants()
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			if (this.SpawnPoints[i].Occupants.Count > 0)
			{
				return true;
			}
		}
		return false;
	}

	public bool AnyLiveOccupants(bool checkActive = false)
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			foreach (Actor actor in this.SpawnPoints[i].Occupants)
			{
				if (actor != null && (!checkActive || actor.isActiveAndEnabled))
				{
					return true;
				}
			}
		}
		return false;
	}

	public bool AnyDeadOccupants()
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			foreach (Actor actor in this.SpawnPoints[i].Occupants)
			{
				if (actor != null && !actor.isActiveAndEnabled)
				{
					return true;
				}
			}
		}
		return false;
	}

	public bool IsSpawning()
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			CarSpawn carSpawn = this.SpawnPoints[i];
			if (carSpawn.isSpawning)
			{
				return true;
			}
		}
		return false;
	}

	public CarSpawn AddOccupant(Actor actor, bool goingOut)
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			CarSpawn carSpawn = this.SpawnPoints[i];
			if (((goingOut && carSpawn.CanGoOut) || (!goingOut && carSpawn.CanGoIn)) && carSpawn.Occupants.Count < carSpawn.Capacity)
			{
				carSpawn.Occupants.Add(actor);
				return carSpawn;
			}
		}
		return null;
	}

	public CarSpawn ForceAddOccupant(Actor actor)
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			CarSpawn carSpawn = this.SpawnPoints[i];
			if (carSpawn.Occupants.Count < carSpawn.Capacity)
			{
				carSpawn.Occupants.Add(actor);
				return carSpawn;
			}
		}
		return null;
	}

	public void ForEachOccupant(Action<Actor> action)
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			foreach (Actor obj in this.SpawnPoints[i].Occupants)
			{
				action(obj);
			}
		}
	}

	public IEnumerable<Actor> GetOccupants()
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			foreach (Actor actor in this.SpawnPoints[i].Occupants)
			{
				yield return actor;
			}
		}
		yield break;
	}

	public void BindOccupants()
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			foreach (Actor actor in this.SpawnPoints[i].Occupants)
			{
				if (actor != null && (actor.MyCar != this || actor.CarSpawnID != i))
				{
					if (actor.MyCar != null)
					{
						actor.MyCar.SpawnPoints[actor.CarSpawnID].Occupants.Remove(actor);
					}
					actor.MyCar = this;
					actor.CarSpawnID = i;
				}
			}
		}
	}

	public void BeginSpawn()
	{
		base.StartCoroutine(this.SpawnActors());
	}

	private IEnumerator SpawnActors()
	{
		for (int i = 0; i < this.SpawnPoints.Length; i++)
		{
			if (this.SpawnPoints[i].Occupants.Count > 0 && this.SpawnPoints[i].CanGoOut)
			{
				this.SpawnPoints[i].BeginSpawn();
				while (GameSettings.GameSpeed == 0f)
				{
					yield return new WaitForSeconds(0.1f);
				}
				yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 1f) / GameSettings.GameSpeed);
			}
		}
		yield break;
	}

	private void Update()
	{
		for (int i = 0; i < this.AllRenders.Length; i++)
		{
			this.AllRenders[i].enabled = (GameSettings.Instance.ActiveFloor >= 0);
		}
		if (GameSettings.GameSpeed > 0f)
		{
			if (!Mathf.Approximately(this.CurrentSpeed, 0f))
			{
				Quaternion rhs = Quaternion.Euler(0f, 0f, Time.deltaTime * GameSettings.GameSpeed * this.CurrentSpeed * 40f);
				Quaternion rhs2 = Quaternion.Euler(0f, 0f, -Time.deltaTime * GameSettings.GameSpeed * this.CurrentSpeed * 40f);
				for (int j = 0; j < this.RightWheels.Length; j++)
				{
					this.RightWheels[j].transform.rotation *= rhs;
				}
				for (int k = 0; k < this.LeftWheels.Length; k++)
				{
					this.LeftWheels[k].transform.rotation *= rhs2;
				}
			}
			if (!this.Parked && this.WaitingFor.Count > 0)
			{
				for (int l = 0; l < this.WaitingFor.Count; l++)
				{
					bool flag = false;
					IHasSpeed hasSpeed = this.WaitingFor[l];
					GameObject gameObject = hasSpeed.GetGameObject();
					if (hasSpeed == null || gameObject == null || !gameObject.activeInHierarchy)
					{
						flag = true;
					}
					if (!flag)
					{
						CarScript carScript = hasSpeed as CarScript;
						if (carScript != null && carScript.Parked)
						{
							flag = true;
						}
					}
					if (!flag)
					{
						Actor actor = hasSpeed as Actor;
						if (actor != null && (!actor.isActiveAndEnabled || actor.currentRoom != GameSettings.Instance.sRoomManager.Outside))
						{
							flag = true;
						}
					}
					if (flag)
					{
						this.WaitingFor.RemoveAt(l);
						l--;
					}
				}
			}
		}
		if (this.AudioComp.isPlaying && GameSettings.GameSpeed == 0f)
		{
			this.AudioComp.Pause();
		}
		if (!this.AudioComp.isPlaying && GameSettings.GameSpeed > 0f)
		{
			this.AudioComp.Play();
		}
		AudioMixerGroup outputAudioMixerGroup = (!(GameSettings.Instance.sRoomManager.CameraRoom == GameSettings.Instance.sRoomManager.Outside)) ? AudioManager.InGameHighPass : AudioManager.InGameNormal;
		this.AudioComp.outputAudioMixerGroup = outputAudioMixerGroup;
		this.AudioComp.pitch = 0.9f + this.CurrentSpeed / this.Speed * 0.3f;
		this.SFX.outputAudioMixerGroup = outputAudioMixerGroup;
		this.AudioComp.volume = Mathf.Lerp(this.AudioComp.volume, (!this.AudioE || GameSettings.GameSpeed <= 0f) ? 0f : (3f / (float)HUD.Instance.GameSpeed / 3f), Time.deltaTime * GameSettings.GameSpeed * 10f);
		int hour = TimeOfDay.Instance.Hour;
		if ((hour > 19 || hour < 7) && this.LightsE && GameSettings.Instance.ActiveFloor >= 0)
		{
			if (!this.Lights[0].enabled)
			{
				this.UpdateEmission(1f);
				for (int m = 0; m < this.Lights.Length; m++)
				{
					this.Lights[m].enabled = true;
				}
			}
		}
		else if (this.Lights[0].enabled)
		{
			this.UpdateEmission(0f);
			for (int n = 0; n < this.Lights.Length; n++)
			{
				this.Lights[n].enabled = false;
			}
		}
	}

	public override string WriteName()
	{
		return "Car";
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		base.transform.position = dictionary.Get<SVector3>("position", new SVector3(0f, 0f, 0f, 0f));
		base.transform.position = new Vector3(base.transform.position.x, 0f, base.transform.position.z);
		base.transform.rotation = dictionary.Get<SVector3>("rotation", new SVector3(0f, 0f, 0f, 1f));
		if (dictionary.Contains("Actors"))
		{
			foreach (Actor actor in (from x in dictionary.Get<List<uint>>("Actors", new List<uint>())
			select (Actor)base.GetDeserializedObject(x)).ToHashSet<Actor>())
			{
				this.ForceAddOccupant(actor);
			}
		}
		List<KeyValuePair<int, uint>> list = dictionary.Get<List<KeyValuePair<int, uint>>>("Occupants", new List<KeyValuePair<int, uint>>());
		for (int i = 0; i < list.Count; i++)
		{
			KeyValuePair<int, uint> keyValuePair = list[i];
			Actor actor2 = base.GetDeserializedObject(keyValuePair.Value) as Actor;
			if (actor2 != null)
			{
				this.SpawnPoints[keyValuePair.Key].Occupants.Add(actor2);
			}
		}
		this.CurrentSpeed = dictionary.Get<float>("CurrentSpeed", 0f);
		this.Parked = dictionary.Get<bool>("Parked", false);
		this.AudioE = dictionary.Get<bool>("AudioE", true);
		this.LightsE = dictionary.Get<bool>("LightsE", true);
		this.CanDestroy = dictionary.Get<bool>("CanDestroy", false);
		NormalCar component = base.GetComponent<NormalCar>();
		if (component != null && !component.Deserialize(dictionary))
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return null;
		}
		BusScript component2 = base.GetComponent<BusScript>();
		if (component2 != null)
		{
			component2.Deserialize(dictionary);
		}
		return this;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["position"] = base.transform.position;
		dictionary["rotation"] = base.transform.rotation;
		if (mode == GameReader.LoadMode.Full)
		{
			dictionary["Occupants"] = this.SpawnPoints.SelectMany((CarSpawn x) => from y in x.Occupants
			select new KeyValuePair<int, uint>(x.ID, y.DID)).ToList<KeyValuePair<int, uint>>();
		}
		dictionary["CurrentSpeed"] = this.CurrentSpeed;
		dictionary["Parked"] = this.Parked;
		dictionary["AudioE"] = this.AudioE;
		dictionary["LightsE"] = this.LightsE;
		dictionary["CanDestroy"] = this.CanDestroy;
		dictionary["CarIdx"] = this.CarIdx;
		NormalCar component = base.GetComponent<NormalCar>();
		if (component != null)
		{
			component.Serialize(dictionary);
		}
		BusScript component2 = base.GetComponent<BusScript>();
		if (component2 != null)
		{
			component2.Serialize(dictionary);
		}
		base.SerializeMe(dictionary, mode);
	}

	public GameObject GetGameObject()
	{
		return (!(this == null)) ? base.gameObject : null;
	}

	public float GetAngle()
	{
		float x = base.transform.forward.x;
		float magnitude = base.transform.forward.magnitude;
		return Mathf.Acos(x / magnitude);
	}

	public float GetSpeed(float angle)
	{
		if (Mathf.Abs(angle - this.GetAngle()) < 0.3926991f)
		{
			return this.CurrentSpeed - 0.05f;
		}
		return 0f;
	}

	public Light[] Lights;

	public AudioSource AudioComp;

	private AudioSource SFX;

	public bool Parked;

	public bool AudioE = true;

	public bool LightsE = true;

	public bool CanDestroy;

	public float Speed;

	public float CurrentSpeed;

	public int CarIdx;

	public List<IHasSpeed> WaitingFor = new List<IHasSpeed>();

	public int Capacity;

	public RoadNode Target;

	public Renderer[] CarRender;

	public MaterialPropertyBlock MatBlock;

	public Renderer[] AllRenders;

	public Transform[] LeftWheels;

	public Transform[] RightWheels;

	public CarSpawn[] SpawnPoints;
}
