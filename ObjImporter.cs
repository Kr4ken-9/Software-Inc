﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using OrbCreationExtensions;
using UnityEngine;

public class ObjImporter
{
	public static GameObject Import(string objString)
	{
		return ObjImporter.Import(objString, Quaternion.identity, new Vector3(1f, 1f, 1f), Vector3.zero);
	}

	public static GameObject Import(string objString, Quaternion rotate, Vector3 scale, Vector3 translate)
	{
		return ObjImporter.Import(objString, null, null, rotate, scale, translate, false, false, false);
	}

	public static GameObject Import(string objString, string mtlString, Hashtable textures)
	{
		return ObjImporter.Import(objString, mtlString, textures, Quaternion.identity, Vector3.one, Vector3.zero, false, false, false);
	}

	public static GameObject Import(string objString, string mtlString, Hashtable textures, Quaternion rotate, Vector3 scale, Vector3 translate, bool gameObjectPerGroup = false, bool subMeshPerGroup = false, bool usesRightHandedCoordinates = false)
	{
		List<Hashtable> geometries = ObjImporter.ImportGeometry(objString, gameObjectPerGroup, subMeshPerGroup);
		Hashtable[] matSpecs = ObjImporter.ImportMaterialSpecs(mtlString);
		ObjImporter.PutTexturesInMaterialSpecs(matSpecs, textures);
		return ObjImporter.MakeGameObject(geometries, matSpecs, rotate, scale, translate, usesRightHandedCoordinates);
	}

	public static List<Mesh> ImportMeshes(string objString, bool usesRightHandedCoordinates = false, bool mergeSubmeshes = true)
	{
		List<Hashtable> geometries = ObjImporter.ImportGeometry(objString, false, false);
		return ObjImporter.MakeMesh(geometries, usesRightHandedCoordinates, mergeSubmeshes);
	}

	public static GameObject Import(string objString, string mtlString, Texture2D[] textures)
	{
		return ObjImporter.Import(objString, Quaternion.identity, Vector3.one, Vector3.zero, mtlString, textures, false, false, false);
	}

	public static GameObject Import(string objString, Quaternion rotate, Vector3 scale, Vector3 translate, string mtlString, Texture2D[] textures, bool gameObjectPerGroup = false, bool subMeshPerGroup = false, bool usesRightHandedCoordinates = false)
	{
		List<Hashtable> geometries = ObjImporter.ImportGeometry(objString, gameObjectPerGroup, subMeshPerGroup);
		Hashtable[] matSpecs = ObjImporter.ImportMaterialSpecs(mtlString);
		ObjImporter.PutTexturesInMaterialSpecs(matSpecs, textures);
		return ObjImporter.MakeGameObject(geometries, matSpecs, rotate, scale, translate, usesRightHandedCoordinates);
	}

	public static IEnumerator ImportInBackground(string objString, string mtlString, Hashtable textures, Action<GameObject> result, bool gameObjectPerGroup = false, bool subMeshPerGroup = false, bool usesRightHandedCoordinates = false)
	{
		yield return null;
		Hashtable info = new Hashtable();
		info["objString"] = objString;
		info["gameObjectPerGroup"] = gameObjectPerGroup;
		info["subMeshPerGroup"] = subMeshPerGroup;
		info["usesRightHandedCoordinates"] = usesRightHandedCoordinates;
		if (ObjImporter.<>f__mg$cache0 == null)
		{
			ObjImporter.<>f__mg$cache0 = new ParameterizedThreadStart(ObjImporter.ImportGeometryInBackground);
		}
		Thread thread = new Thread(ObjImporter.<>f__mg$cache0);
		thread.Start(info);
		while (!info.ContainsKey("ready"))
		{
			yield return new WaitForSeconds(0.1f);
		}
		Hashtable[] matSpecs = ObjImporter.ImportMaterialSpecs(mtlString);
		yield return null;
		ObjImporter.PutTexturesInMaterialSpecs(matSpecs, textures);
		yield return null;
		GameObject importedGameObject = ObjImporter.MakeGameObject((List<Hashtable>)info["geometries"], matSpecs, Quaternion.identity, Vector3.one, Vector3.zero, usesRightHandedCoordinates);
		result(importedGameObject);
		yield break;
	}

	public static IEnumerator ImportInBackground(string objString, string mtlString, Hashtable textures, Quaternion rotate, Vector3 scale, Vector3 translate, Action<GameObject> result, bool gameObjectPerGroup = false, bool subMeshPerGroup = false, bool usesRightHandedCoordinates = false)
	{
		yield return null;
		Hashtable info = new Hashtable();
		info["objString"] = objString;
		info["gameObjectPerGroup"] = gameObjectPerGroup;
		info["subMeshPerGroup"] = subMeshPerGroup;
		if (ObjImporter.<>f__mg$cache1 == null)
		{
			ObjImporter.<>f__mg$cache1 = new ParameterizedThreadStart(ObjImporter.ImportGeometryInBackground);
		}
		Thread thread = new Thread(ObjImporter.<>f__mg$cache1);
		thread.Start(info);
		while (!info.ContainsKey("ready"))
		{
			yield return new WaitForSeconds(0.1f);
		}
		Hashtable[] matSpecs = ObjImporter.ImportMaterialSpecs(mtlString);
		yield return null;
		ObjImporter.PutTexturesInMaterialSpecs(matSpecs, textures);
		yield return null;
		GameObject importedGameObject = ObjImporter.MakeGameObject((List<Hashtable>)info["geometries"], matSpecs, rotate, scale, translate, usesRightHandedCoordinates);
		result(importedGameObject);
		yield break;
	}

	private static void ImportGeometryInBackground(object data)
	{
		string objString = (string)((Hashtable)data)["objString"];
		bool gameObjectPerGroup = (bool)((Hashtable)data)["gameObjectPerGroup"];
		bool subMeshPerGroup = (bool)((Hashtable)data)["subMeshPerGroup"];
		((Hashtable)data)["geometries"] = ObjImporter.ImportGeometry(objString, gameObjectPerGroup, subMeshPerGroup);
		((Hashtable)data)["ready"] = true;
	}

	private static List<Hashtable> ImportGeometry(string objString, bool gameObjectPerGroup, bool subMeshPerGroup)
	{
		objString += "\n";
		List<Hashtable> list = new List<Hashtable>();
		List<Vector3> list2 = new List<Vector3>();
		List<Vector3> list3 = new List<Vector3>();
		List<Vector2> list4 = new List<Vector2>();
		List<Vector3> list5 = new List<Vector3>();
		List<Vector3> list6 = new List<Vector3>();
		List<Vector2> list7 = new List<Vector2>();
		List<Hashtable> list8 = new List<Hashtable>();
		Hashtable hashtable = new Hashtable();
		List<int> list9 = new List<int>();
		hashtable["triangles"] = list9;
		string text = string.Empty;
		string text2 = string.Empty;
		string text3 = string.Empty;
		string value = string.Empty;
		hashtable["name"] = value;
		list8.Add(hashtable);
		Hashtable hashtable2 = new Hashtable();
		hashtable2["topLevelName"] = text;
		hashtable2["name"] = text2;
		hashtable2["rawVs"] = list2;
		hashtable2["rawNs"] = list3;
		hashtable2["rawUs"] = list4;
		hashtable2["vertices"] = list5;
		hashtable2["normals"] = list6;
		hashtable2["uvs"] = list7;
		hashtable2["subMeshes"] = list8;
		int[] array = null;
		bool flag = true;
		for (int i = 0; i < objString.Length; i++)
		{
			char c = objString[i];
			if (c == '\n')
			{
				flag = true;
			}
			else if (flag && c == 'o' && i < objString.Length - 2 && objString[i + 1] == ' ')
			{
				int num = objString.IndexOfEndOfLine(i + 2);
				if (num > i + 2)
				{
					text2 = objString.Substring(i + 2, num - i - 2).Trim();
					if (text.Length <= 0)
					{
						text = text2;
					}
					hashtable2["topLevelName"] = text;
					if (list2.Count > 0)
					{
						list.Add(hashtable2);
						hashtable2 = new Hashtable();
						hashtable2["topLevelName"] = text;
						hashtable2["name"] = text2;
						hashtable2["rawVs"] = list2;
						hashtable2["rawNs"] = list3;
						hashtable2["rawUs"] = list4;
						hashtable2["vertices"] = list5;
						hashtable2["normals"] = list6;
						hashtable2["uvs"] = list7;
						list8 = new List<Hashtable>();
						hashtable2["subMeshes"] = list8;
						hashtable = new Hashtable();
						list9 = new List<int>();
						hashtable["triangles"] = list9;
						list8.Add(hashtable);
					}
					else
					{
						hashtable2["name"] = text2;
					}
					i = num - 1;
				}
			}
			else if (flag && c == 'g' && i < objString.Length - 2 && objString[i + 1] == ' ')
			{
				int num2 = objString.IndexOfEndOfLine(i + 2);
				if (num2 > i + 2)
				{
					if (gameObjectPerGroup)
					{
						if (list9.Count > 0 && list2.Count > 0)
						{
							list.Add(hashtable2);
							hashtable2 = new Hashtable();
							hashtable2["topLevelName"] = text;
							hashtable2["rawVs"] = list2;
							hashtable2["rawNs"] = list3;
							hashtable2["rawUs"] = list4;
							hashtable2["vertices"] = list5;
							hashtable2["normals"] = list6;
							hashtable2["uvs"] = list7;
							list8 = new List<Hashtable>();
							hashtable2["subMeshes"] = list8;
							hashtable = new Hashtable();
							list9 = new List<int>();
							hashtable["triangles"] = list9;
							list8.Add(hashtable);
							text2 = string.Empty;
						}
					}
					else if (list9.Count > 0 && subMeshPerGroup)
					{
						hashtable = new Hashtable();
						list9 = new List<int>();
						hashtable["triangles"] = list9;
						list8.Add(hashtable);
					}
					text3 = objString.Substring(i + 2, num2 - i - 2).Trim();
					if (text2.Length <= 0)
					{
						text2 = text3;
					}
					hashtable2["name"] = text2;
					i = num2 - 1;
				}
			}
			else if (flag && c == 'u' && i < objString.Length - 7 && objString.Substring(i, 7) == "usemtl ")
			{
				int num3 = objString.IndexOfEndOfLine(i + 7);
				if (num3 > i + 7)
				{
					string text4 = objString.Substring(i + 7, num3 - i - 7).Trim();
					int j;
					for (j = 0; j < list8.Count; j++)
					{
						Hashtable hashtable3 = list8[j];
						if ((string)hashtable3["name"] == text4)
						{
							hashtable = hashtable3;
							list9 = (List<int>)hashtable3["triangles"];
							break;
						}
					}
					if (list9.Count > 0 && j >= list8.Count)
					{
						hashtable = new Hashtable();
						list9 = new List<int>();
						hashtable["triangles"] = list9;
						list8.Add(hashtable);
					}
					value = text4;
					hashtable["name"] = value;
					i = num3 - 1;
				}
			}
			else if (flag && c == 'v' && i < objString.Length - 2 && objString[i + 1] == ' ')
			{
				i++;
				int num4 = objString.IndexOfEndOfLine(i);
				if (num4 > i)
				{
					Vector3 vector3FromObjString = ObjImporter.GetVector3FromObjString(objString.Substring(i, num4 - i).Trim());
					list2.Add(vector3FromObjString);
					i = num4 - 1;
				}
			}
			else if (flag && c == 'v' && i < objString.Length - 2 && objString[i + 1] == 'n' && objString[i + 2] == ' ')
			{
				i += 2;
				int num5 = objString.IndexOfEndOfLine(i);
				if (num5 > i)
				{
					Vector3 vector3FromObjString2 = ObjImporter.GetVector3FromObjString(objString.Substring(i, num5 - i).Trim());
					list3.Add(vector3FromObjString2);
					i = num5 - 1;
				}
			}
			else if (flag && c == 'v' && i < objString.Length - 2 && objString[i + 1] == 't' && objString[i + 2] == ' ')
			{
				i += 2;
				int num6 = objString.IndexOfEndOfLine(i);
				if (num6 > i)
				{
					Vector2 vector2FromObjString = ObjImporter.GetVector2FromObjString(objString.Substring(i, num6 - i).Trim());
					list4.Add(vector2FromObjString);
					i = num6 - 1;
				}
			}
			else if (flag && c == 'f' && i < objString.Length - 2 && objString[i + 1] == ' ')
			{
				i++;
				int num7 = objString.IndexOfEndOfLine(i);
				if (num7 > i)
				{
					if (array == null)
					{
						array = new int[list2.Count];
						for (int k = 0; k < array.Length; k++)
						{
							array[k] = -1;
						}
					}
					if (array.Length < list2.Count)
					{
						int num8 = array.Length;
						Array.Resize<int>(ref array, list2.Count);
						for (int l = num8; l < array.Length; l++)
						{
							array[l] = -1;
						}
					}
					List<int[]> faceIndexesFromObjString = ObjImporter.GetFaceIndexesFromObjString(objString.Substring(i, num7 - i).Trim());
					Vector3 vector = new Vector3(0f, 0f, 0f);
					Vector3 vector2 = new Vector3(0f, 0f, -1f);
					Vector2 vector3 = new Vector2(0f, 0f);
					List<int> list10 = new List<int>();
					for (int m = 0; m < faceIndexesFromObjString.Count; m++)
					{
						int[] array2 = faceIndexesFromObjString[m];
						if (array2.Length > 0)
						{
							int num9 = array2[0];
							if (num9 < 0)
							{
								num9 += list2.Count;
							}
							if (num9 >= 0 && num9 < list2.Count)
							{
								vector = list2[num9];
								if (array2[1] < 0)
								{
									array2[1] += list4.Count;
								}
								if (array2[1] >= 0 && array2[1] < list4.Count)
								{
									vector3 = list4[array2[1]];
								}
								if (array2[2] < 0)
								{
									array2[2] += list3.Count;
								}
								if (array2[2] >= 0 && array2[2] < list3.Count)
								{
									vector2 = list3[array2[2]];
								}
								int num10 = array[num9];
								if (num10 >= 0)
								{
									Vector3 n = vector2;
									Vector3 v = vector3;
									if (list6.Count > num10)
									{
										n = list6[num10];
									}
									if (list7.Count > num10)
									{
										v = list7[num10];
									}
									if (list5.Count > num10)
									{
										Vector3 v2 = list5[num10];
										if (!ObjImporter.IsSameVertex(vector, vector2, vector3, v2, n, v))
										{
											num10 = list5.Count;
										}
									}
								}
								else
								{
									num10 = list5.Count;
								}
								if (num10 >= list5.Count)
								{
									list5.Add(vector);
									if (list3.Count > 0)
									{
										list6.Add(vector2);
									}
									list7.Add(vector3);
									array[num9] = num10;
								}
								list10.Add(num10);
							}
							else
							{
								ObjImporter.Log(string.Concat(new object[]
								{
									"Bad vertex index:",
									num9,
									" at:",
									m
								}));
							}
						}
					}
					if (list10.Count > 2)
					{
						ObjImporter.PolygonIntoTriangle(list10.ToArray(), ref list9);
					}
					i = num7 - 1;
				}
			}
			if (c != ' ' && c != '\r' && c != '\n' && c != '\t')
			{
				flag = false;
			}
		}
		if (list5.Count > 0)
		{
			list.Add(hashtable2);
		}
		return list;
	}

	public static Hashtable[] ImportMaterialSpecs(string mtlString)
	{
		List<Hashtable> list = new List<Hashtable>();
		Hashtable hashtable = new Hashtable();
		bool flag = true;
		if (mtlString == null)
		{
			mtlString = string.Empty;
		}
		mtlString += "\n";
		for (int i = 0; i < mtlString.Length; i++)
		{
			char c = mtlString[i];
			if (c == '\n')
			{
				flag = true;
			}
			else if (flag && c == 'n' && i < mtlString.Length - 7 && mtlString.Substring(i, 7) == "newmtl ")
			{
				i += 7;
				int num = mtlString.IndexOfEndOfLine(i);
				if (num > i)
				{
					if (hashtable.ContainsKey("name"))
					{
						list.Add(hashtable);
						hashtable = new Hashtable();
						hashtable["diffuse"] = Color.white;
					}
					hashtable["name"] = mtlString.Substring(i, num - i).Trim();
					i = num - 1;
				}
			}
			else if (flag && c == 'K' && i < mtlString.Length - 3 && mtlString.Substring(i, 3) == "Kd ")
			{
				i += 2;
				int num2 = mtlString.IndexOfEndOfLine(i);
				if (num2 > i)
				{
					Vector3 vector3FromObjString = ObjImporter.GetVector3FromObjString(mtlString.Substring(i, num2 - i).Trim());
					Vector4 v = vector3FromObjString;
					v.w = 1f;
					hashtable["diffuse"] = v;
					i = num2 - 1;
				}
			}
			else if (flag && c == 'K' && i < mtlString.Length - 3 && mtlString.Substring(i, 3) == "Ks ")
			{
				i += 2;
				int num3 = mtlString.IndexOfEndOfLine(i);
				if (num3 > i)
				{
					Vector3 vector3FromObjString2 = ObjImporter.GetVector3FromObjString(mtlString.Substring(i, num3 - i).Trim());
					Vector4 v2 = vector3FromObjString2;
					v2.w = 1f;
					hashtable["specular"] = v2;
					i = num3 - 1;
				}
			}
			else if (flag && c == 'd' && i < mtlString.Length - 2 && mtlString[i + 1] == ' ')
			{
				i += 2;
				int num4 = mtlString.IndexOfEndOfLine(i);
				if (num4 > i)
				{
					float a = mtlString.Substring(i, num4 - i).Trim().MakeFloat();
					Color color = Color.white;
					if (hashtable.ContainsKey("diffuse"))
					{
						color = (Color)hashtable["diffuse"];
					}
					color.a = a;
					hashtable["diffuse"] = color;
					i = num4 - 1;
				}
			}
			else if (flag && c == 'T' && i < mtlString.Length - 3 && mtlString.Substring(i, 3) == "Tr ")
			{
				i += 3;
				int num5 = mtlString.IndexOfEndOfLine(i);
				if (num5 > i)
				{
					float a2 = mtlString.Substring(i, num5 - i).Trim().MakeFloat();
					Color color2 = Color.white;
					if (hashtable.ContainsKey("diffuse"))
					{
						color2 = (Color)hashtable["diffuse"];
					}
					color2.a = a2;
					hashtable["diffuse"] = color2;
					i = num5 - 1;
				}
			}
			else if (flag && c == 'N' && i < mtlString.Length - 3 && mtlString.Substring(i, 3) == "Ns ")
			{
				i += 3;
				int num6 = mtlString.IndexOfEndOfLine(i);
				if (num6 > i)
				{
					float num7 = mtlString.Substring(i, num6 - i).Trim().MakeFloat();
					if (num7 > 0f)
					{
						hashtable["specularity"] = 1f / num7;
					}
					i = num6 - 1;
				}
			}
			else if (flag && c == 'm' && i < mtlString.Length - 7 && mtlString.Substring(i, 7) == "map_Kd ")
			{
				i += 7;
				int num8 = mtlString.IndexOfEndOfLine(i);
				if (num8 > i)
				{
					hashtable["mainTexName"] = mtlString.Substring(i, num8 - i).Trim();
					i = num8 - 1;
				}
			}
			else if (flag && c == 'm' && i < mtlString.Length - 6 && mtlString.Substring(i, 7) == "map_d ")
			{
				i += 6;
				int num9 = mtlString.IndexOfEndOfLine(i);
				if (num9 > i)
				{
					hashtable["alphaTexName"] = mtlString.Substring(i, num9 - i).Trim();
					i = num9 - 1;
				}
			}
			if (c != ' ' && c != '\r' && c != '\n' && c != '\t')
			{
				flag = false;
			}
		}
		if (hashtable.ContainsKey("name"))
		{
			list.Add(hashtable);
		}
		return list.ToArray();
	}

	public static void PutTexturesInMaterialSpecs(Hashtable[] matSpecs, Hashtable textures)
	{
		int num = 0;
		while (textures != null && num < matSpecs.Length)
		{
			if (matSpecs[num].ContainsKey("mainTexName"))
			{
				string key = (string)matSpecs[num]["mainTexName"];
				if (textures.ContainsKey(key))
				{
					matSpecs[num]["mainTex"] = (Texture2D)textures[key];
				}
			}
			num++;
		}
	}

	public static void PutTexturesInMaterialSpecs(Hashtable[] matSpecs, Texture2D[] textures)
	{
		int num = 0;
		int num2 = 0;
		while (textures != null && num2 < matSpecs.Length)
		{
			if (matSpecs[num2].ContainsKey("mainTexName") && textures.Length > num)
			{
				matSpecs[num2]["mainTex"] = textures[num++];
			}
			num2++;
		}
	}

	public static void PutMaterialSpecsInMaterial(Material mat, Hashtable[] matSpecs)
	{
		for (int i = 0; i < matSpecs.Length; i++)
		{
			string a = (string)matSpecs[i]["name"];
			if (a == mat.name)
			{
				string text = "Diffuse";
				if (matSpecs[i].ContainsKey("specular") || matSpecs[i].ContainsKey("specularity"))
				{
					text = "Specular";
				}
				if (matSpecs[i].ContainsKey("diffuse") && ((Color)matSpecs[i]["diffuse"]).a < 1f)
				{
					text = "Transparent/" + text;
				}
				else if (matSpecs[i].ContainsKey("mainTex") && ((Texture2D)matSpecs[i]["mainTex"]).HasTransparency())
				{
					text = "Transparent/" + text;
				}
				mat.shader = Shader.Find(text);
				if (matSpecs[i].ContainsKey("mainTex") && mat.HasProperty("_MainTex"))
				{
					mat.SetTexture("_MainTex", (Texture2D)matSpecs[i]["mainTex"]);
				}
				if (matSpecs[i].ContainsKey("diffuse") && mat.HasProperty("_Color"))
				{
					mat.SetColor("_Color", (Color)matSpecs[i]["diffuse"]);
				}
				if (matSpecs[i].ContainsKey("specular") && mat.HasProperty("_SpecColor"))
				{
					mat.SetColor("_SpecColor", (Color)matSpecs[i]["specular"]);
				}
				if (matSpecs[i].ContainsKey("specularity") && mat.HasProperty("_Shininess"))
				{
					mat.SetFloat("_Shininess", (float)matSpecs[i]["specularity"]);
				}
			}
		}
	}

	private static List<Mesh> MakeMesh(List<Hashtable> geometries, bool usesRightHandedCoordinates, bool mergeSubmeshes)
	{
		int num = 65534;
		List<Mesh> list = new List<Mesh>();
		for (int i = 0; i < geometries.Count; i++)
		{
			Hashtable hashtable = geometries[i];
			string text = hashtable.GetString("name");
			if (text.Length <= 0)
			{
				text = "obj" + i;
			}
			List<Vector3> list2 = (List<Vector3>)hashtable["vertices"];
			list2 = list2.GetRange(0, list2.Count);
			List<Vector3> list3 = (List<Vector3>)hashtable["normals"];
			List<Vector2> list4 = (List<Vector2>)hashtable["uvs"];
			List<Hashtable> list5 = (List<Hashtable>)hashtable["subMeshes"];
			if (usesRightHandedCoordinates)
			{
				ObjImporter.FlipXAxis(ref list2);
				if (list3 != null)
				{
					ObjImporter.FlipXAxis(ref list3);
				}
			}
			int num2 = 0;
			int j = 0;
			bool flag;
			do
			{
				flag = false;
				int[] array = new int[list2.Count];
				for (int k = 0; k < array.Length; k++)
				{
					array[k] = -1;
				}
				List<int> list6 = new List<int>();
				List<List<int>> list7 = new List<List<int>>();
				while (num2 < list5.Count && !flag)
				{
					Hashtable hashtable2 = list5[num2];
					List<int> list8 = (List<int>)hashtable2["triangles"];
					List<int> list9 = new List<int>();
					while (j < list8.Count)
					{
						int num3 = list8[j];
						int num4 = list8[j + 1];
						int num5 = list8[j + 2];
						if (usesRightHandedCoordinates)
						{
							num4 = num3;
							num3 = list8[j + 1];
						}
						if (array[num3] < 0)
						{
							if (list6.Count > num - 3)
							{
								flag = true;
								break;
							}
							array[num3] = list6.Count;
							list6.Add(num3);
						}
						if (array[num4] < 0)
						{
							if (list6.Count > num - 2)
							{
								flag = true;
								break;
							}
							array[num4] = list6.Count;
							list6.Add(num4);
						}
						if (array[num5] < 0)
						{
							if (list6.Count > num - 1)
							{
								flag = true;
								break;
							}
							array[num5] = list6.Count;
							list6.Add(num5);
						}
						list9.Add(array[num3]);
						list9.Add(array[num4]);
						list9.Add(array[num5]);
						j += 3;
					}
					if (list9.Count > 0)
					{
						list7.Add(list9);
					}
					j += 3;
					if (j < list8.Count)
					{
						break;
					}
					j = 0;
					num2++;
				}
				if (list6.Count > 0)
				{
					Mesh mesh = new Mesh();
					Vector3[] array2 = new Vector3[list6.Count];
					for (int l = 0; l < list6.Count; l++)
					{
						array2[l] = list2[list6[l]];
					}
					mesh.vertices = array2;
					if (list3.Count > 0)
					{
						Vector3[] array3 = new Vector3[list6.Count];
						for (int m = 0; m < list6.Count; m++)
						{
							array3[m] = list3[list6[m]];
						}
						mesh.normals = array3;
					}
					if (list4.Count > 0)
					{
						Vector2[] array4 = new Vector2[list6.Count];
						for (int n = 0; n < list6.Count; n++)
						{
							array4[n] = list4[list6[n]];
						}
						mesh.uv = array4;
					}
					if (mergeSubmeshes)
					{
						mesh.subMeshCount = 1;
						mesh.triangles = list7.SelectMany((List<int> x) => x).ToArray<int>();
					}
					else
					{
						mesh.subMeshCount = list7.Count;
						for (int num6 = 0; num6 < list7.Count; num6++)
						{
							mesh.SetTriangles(list7[num6].ToArray(), num6);
						}
					}
					if (list3.Count <= 0)
					{
						mesh.RecalculateNormals();
					}
					mesh.RecalculateTangents();
					mesh.RecalculateBounds();
					list.Add(mesh);
				}
			}
			while (flag);
		}
		return list;
	}

	private static GameObject MakeGameObject(List<Hashtable> geometries, Hashtable[] matSpecs, Quaternion rotate, Vector3 scale, Vector3 translate, bool usesRightHandedCoordinates)
	{
		GameObject gameObject = null;
		int num = 65534;
		string text = string.Empty;
		if (geometries.Count > 0)
		{
			Hashtable hash = geometries[0];
			text = hash.GetString("topLevelName");
			if (text.Length <= 0)
			{
				text = "Imported OBJ file";
			}
		}
		if (geometries.Count > 1)
		{
			gameObject = new GameObject(text);
		}
		for (int i = 0; i < geometries.Count; i++)
		{
			Hashtable hashtable = geometries[i];
			string text2 = hashtable.GetString("name");
			if (text2.Length <= 0)
			{
				text2 = "obj" + i;
			}
			List<Vector3> list = (List<Vector3>)hashtable["vertices"];
			list = list.GetRange(0, list.Count);
			List<Vector3> list2 = (List<Vector3>)hashtable["normals"];
			List<Vector2> list3 = (List<Vector2>)hashtable["uvs"];
			List<Hashtable> list4 = (List<Hashtable>)hashtable["subMeshes"];
			if (usesRightHandedCoordinates)
			{
				ObjImporter.FlipXAxis(ref list);
				if (list2 != null)
				{
					ObjImporter.FlipXAxis(ref list2);
				}
			}
			if (rotate != Quaternion.identity)
			{
				ObjImporter.RotateVertices(ref list, rotate);
			}
			if (scale != Vector3.zero)
			{
				ObjImporter.ScaleVertices(ref list, scale);
			}
			if (translate != Vector3.zero)
			{
				ObjImporter.TranslateVertices(ref list, translate);
			}
			int num2 = 0;
			int j = 0;
			int num3 = 0;
			bool flag = false;
			for (;;)
			{
				bool flag2 = false;
				int[] array = new int[list.Count];
				for (int k = 0; k < array.Length; k++)
				{
					array[k] = -1;
				}
				List<int> list5 = new List<int>();
				List<List<int>> list6 = new List<List<int>>();
				List<Material> list7 = new List<Material>();
				while (num2 < list4.Count && !flag2)
				{
					Hashtable hashtable2 = list4[num2];
					List<int> list8 = (List<int>)hashtable2["triangles"];
					List<int> list9 = new List<int>();
					int num4 = 0;
					while (j < list8.Count)
					{
						int num5 = list8[j];
						int num6 = list8[j + 1];
						int num7 = list8[j + 2];
						if (usesRightHandedCoordinates)
						{
							num6 = num5;
							num5 = list8[j + 1];
						}
						if (array[num5] < 0)
						{
							if (list5.Count > num - 3)
							{
								flag2 = true;
								break;
							}
							array[num5] = list5.Count;
							list5.Add(num5);
						}
						if (array[num6] < 0)
						{
							if (list5.Count > num - 2)
							{
								flag2 = true;
								break;
							}
							array[num6] = list5.Count;
							list5.Add(num6);
						}
						if (array[num7] < 0)
						{
							if (list5.Count > num - 1)
							{
								flag2 = true;
								break;
							}
							array[num7] = list5.Count;
							list5.Add(num7);
						}
						list9.Add(array[num5]);
						list9.Add(array[num6]);
						list9.Add(array[num7]);
						num4 += 3;
						j += 3;
					}
					if (list9.Count > 0)
					{
						Material material = new Material(Shader.Find("Diffuse"));
						material.SetColor("_Color", Color.white);
						material.name = (string)hashtable2["name"];
						if (material.name.Length <= 0)
						{
							material.name = "mat" + list7.Count;
						}
						list7.Add(material);
						ObjImporter.PutMaterialSpecsInMaterial(material, matSpecs);
						list6.Add(list9);
					}
					j += 3;
					if (j < list8.Count)
					{
						break;
					}
					j = 0;
					num2++;
				}
				if (flag2)
				{
					flag = flag2;
				}
				if (list5.Count > 0)
				{
					string text3 = text;
					if (geometries.Count > 1)
					{
						text3 = text2;
					}
					if (flag && gameObject == null)
					{
						gameObject = new GameObject(text3);
					}
					Mesh mesh = new Mesh();
					Vector3[] array2 = new Vector3[list5.Count];
					for (int l = 0; l < list5.Count; l++)
					{
						array2[l] = list[list5[l]];
					}
					mesh.vertices = array2;
					if (list2.Count > 0)
					{
						Vector3[] array3 = new Vector3[list5.Count];
						for (int m = 0; m < list5.Count; m++)
						{
							array3[m] = list2[list5[m]];
						}
						mesh.normals = array3;
					}
					if (list3.Count > 0)
					{
						Vector2[] array4 = new Vector2[list5.Count];
						for (int n = 0; n < list5.Count; n++)
						{
							array4[n] = list3[list5[n]];
						}
						mesh.uv = array4;
					}
					mesh.subMeshCount = list6.Count;
					for (int num8 = 0; num8 < list6.Count; num8++)
					{
						mesh.SetTriangles(list6[num8].ToArray(), num8);
					}
					if (list2.Count <= 0)
					{
						mesh.RecalculateNormals();
					}
					mesh.RecalculateTangents();
					mesh.RecalculateBounds();
					if (flag && gameObject != null)
					{
						text3 = text3 + "_part" + num3;
					}
					mesh.name = text3;
					GameObject gameObject2 = new GameObject(text3);
					MeshRenderer meshRenderer = gameObject2.AddComponent<MeshRenderer>();
					MeshFilter meshFilter = gameObject2.AddComponent<MeshFilter>();
					meshFilter.sharedMesh = mesh;
					meshRenderer.sharedMaterials = list7.ToArray();
					if (gameObject == null)
					{
						gameObject = gameObject2;
					}
					else
					{
						gameObject2.transform.SetParent(gameObject.transform);
					}
				}
				if (!flag2)
				{
					break;
				}
				num3++;
			}
		}
		return gameObject;
	}

	private static void FlipXAxis(ref List<Vector3> vs)
	{
		for (int i = 0; i < vs.Count; i++)
		{
			Vector3 value = vs[i];
			value.x *= -1f;
			vs[i] = value;
		}
	}

	private static Vector3 GetVector3FromObjString(string str)
	{
		Vector3 result = new Vector3(0f, 0f, 0f);
		int num = 0;
		for (int i = 0; i < 3; i++)
		{
			int num2 = str.IndexOf(' ', num);
			if (num2 < 0)
			{
				num2 = str.Length;
			}
			result[i] = str.Substring(num, num2 - num).MakeFloat();
			num = str.EndOfCharRepetition(num2);
		}
		return result;
	}

	private static Vector2 GetVector2FromObjString(string str)
	{
		Vector2 result = new Vector2(0f, 0f);
		int num = 0;
		for (int i = 0; i < 2; i++)
		{
			int num2 = str.IndexOf(' ', num);
			if (num2 < 0)
			{
				num2 = str.Length;
			}
			result[i] = str.Substring(num, num2 - num).MakeFloat();
			num = str.EndOfCharRepetition(num2);
		}
		return result;
	}

	private static List<int[]> GetFaceIndexesFromObjString(string str)
	{
		List<int[]> list = new List<int[]>();
		int num;
		for (int i = 0; i < str.Length; i = str.EndOfCharRepetition(num))
		{
			num = str.IndexOf(' ', i);
			if (num < 0)
			{
				num = str.Length;
			}
			list.Add(ObjImporter.GetFaceCornerIndexesFromObjString(str.Substring(i, num - i).Trim()));
		}
		return list;
	}

	private static int[] GetFaceCornerIndexesFromObjString(string str)
	{
		int[] array = new int[3];
		int i = 0;
		int j;
		for (j = 0; j < 3; j++)
		{
			array[j] = -1;
		}
		j = 0;
		while (i < str.Length)
		{
			int num = str.IndexOf('/', i);
			if (num < 0)
			{
				num = str.Length;
			}
			array[j] = str.Substring(i, num - i).MakeInt();
			if (array[j] > 0)
			{
				array[j]--;
			}
			j++;
			i = num + 1;
		}
		return array;
	}

	private static void TranslateVertices(ref List<Vector3> vertices, Vector3 translate)
	{
		for (int i = 0; i < vertices.Count; i++)
		{
			List<Vector3> list;
			int index;
			(list = vertices)[index = i] = list[index] + translate;
		}
	}

	private static void ScaleVertices(ref List<Vector3> vertices, Vector3 scale)
	{
		for (int i = 0; i < vertices.Count; i++)
		{
			Vector3 value = vertices[i];
			value.x *= scale.x;
			value.y *= scale.y;
			value.z *= scale.z;
			vertices[i] = value;
		}
	}

	private static void RotateVertices(ref List<Vector3> vertices, Quaternion rotate)
	{
		for (int i = 0; i < vertices.Count; i++)
		{
			vertices[i] = rotate * vertices[i];
		}
	}

	private static bool IsSameVertex(Vector3 v1, Vector3 n1, Vector2 u1, Vector3 v2, Vector3 n2, Vector2 u2)
	{
		return v1 == v2 && n1 == n2 && u1 == u2;
	}

	private static void PolygonIntoTriangle(int[] polygon, ref List<int> triangles)
	{
		if (polygon.Length < 3)
		{
			return;
		}
		if (polygon.Length == 3)
		{
			for (int i = 0; i < 3; i++)
			{
				triangles.Add(polygon[i]);
			}
		}
		else
		{
			int num = 0;
			int num2 = polygon.Length / 2;
			int[] array = new int[num2 + 1];
			int[] array2 = new int[polygon.Length - num2 + 1];
			int j;
			for (j = 0; j < array.Length; j++)
			{
				array[j] = polygon[num++];
			}
			array2[0] = polygon[num - 1];
			for (j = 1; j < array2.Length - 1; j++)
			{
				array2[j] = polygon[num++];
			}
			array2[j] = polygon[0];
			ObjImporter.PolygonIntoTriangle(array, ref triangles);
			ObjImporter.PolygonIntoTriangle(array2, ref triangles);
		}
	}

	private static void Log(int[] vs)
	{
		string text = string.Empty;
		for (int i = 0; i < vs.Length; i++)
		{
			text = text + vs[i] + "\n";
		}
		Debug.Log(text + DateTime.Now.ToString("yyy/MM/dd hh:mm:ss.fff"));
	}

	private static void Log(Vector3[] vs)
	{
		string text = string.Empty;
		for (int i = 0; i < vs.Length; i++)
		{
			text = text + vs[i] + "\n";
		}
		Debug.Log(text + DateTime.Now.ToString("yyy/MM/dd hh:mm:ss.fff"));
	}

	private static void Log(string str)
	{
		Debug.Log(str + "\n" + DateTime.Now.ToString("yyy/MM/dd hh:mm:ss.fff"));
	}

	[CompilerGenerated]
	private static ParameterizedThreadStart <>f__mg$cache0;

	[CompilerGenerated]
	private static ParameterizedThreadStart <>f__mg$cache1;
}
