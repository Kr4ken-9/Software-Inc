﻿using System;
using SSAA;
using UnityEngine;

public class SuperSampling_SSAA : MonoBehaviour
{
	private void OnEnable()
	{
		internal_SSAA internal_SSAA = base.gameObject.AddComponent<internal_SSAA>();
		internal_SSAA.hideFlags = (HideFlags.HideInHierarchy | HideFlags.HideInInspector | HideFlags.DontSaveInEditor | HideFlags.NotEditable | HideFlags.DontSaveInBuild | HideFlags.DontUnloadUnusedAsset);
		internal_SSAA.UseDynamicOutputResolution = this.UseDynamicOutputResolution;
		internal_SSAA.Filter = this.Filter;
		internal_SSAA.ChangeScale(this.Scale);
	}

	private void OnDisable()
	{
		internal_SSAA component = base.gameObject.GetComponent<internal_SSAA>();
		if (component != null)
		{
			UnityEngine.Object.Destroy(component);
		}
	}

	public float Scale;

	public bool unlocked;

	public SSAAFilter Filter = 2;

	public bool UseDynamicOutputResolution;
}
