﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CoredumpingGenerator
{
	public static void GenerateLogo(float width, float height, float spacingX, float spacingY, string msg, Action<Vector4, bool> builder, byte[,] pos)
	{
		int num = 0;
		byte[] array = CoredumpingGenerator.StringToBit(msg);
		for (int i = 0; i < pos.GetLength(0); i++)
		{
			for (int j = 0; j < pos.GetLength(1); j++)
			{
				if (pos[i, j] != 0)
				{
					builder(new Vector4((float)j * (width + spacingX), (float)i * (height + spacingY), width, height), array[num % array.Length] == 1);
					num++;
				}
			}
		}
	}

	public static void GenerateLogo(float width, float height, float spacingX, float spacingY, string msg, Action<Vector4, bool> builder)
	{
		CoredumpingGenerator.GenerateLogo(width, height, spacingX, spacingY, msg, builder, CoredumpingGenerator.LogoData);
	}

	private static byte[] StringToBit(string msg)
	{
		List<byte> list = new List<byte>();
		foreach (char key in msg.ToLower())
		{
			byte[] collection = new byte[5];
			CoredumpingGenerator.CharToB.TryGetValue(key, out collection);
			list.AddRange(collection);
		}
		return list.ToArray();
	}

	public static string BitToString(byte[] bits)
	{
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < bits.Length; i += 5)
		{
			byte[] tb = bits.Skip(i).Take(5).ToArray<byte>();
			KeyValuePair<char, byte[]> keyValuePair = CoredumpingGenerator.CharToB.FirstOrDefault((KeyValuePair<char, byte[]> x) => x.Value.SequenceEqual(tb));
			if (keyValuePair.Key == '\0')
			{
				stringBuilder.Append('*');
			}
			else
			{
				stringBuilder.Append(keyValuePair.Key);
			}
		}
		return stringBuilder.ToString();
	}

	static CoredumpingGenerator()
	{
		// Note: this type is marked as 'beforefieldinit'.
		Dictionary<char, byte[]> dictionary = new Dictionary<char, byte[]>();
		dictionary.Add(' ', new byte[5]);
		dictionary.Add('a', new byte[]
		{
			0,
			0,
			0,
			0,
			1
		});
		Dictionary<char, byte[]> dictionary2 = dictionary;
		char key = 'b';
		byte[] array = new byte[5];
		array[3] = 1;
		dictionary2.Add(key, array);
		dictionary.Add('c', new byte[]
		{
			0,
			0,
			0,
			1,
			1
		});
		Dictionary<char, byte[]> dictionary3 = dictionary;
		char key2 = 'd';
		byte[] array2 = new byte[5];
		array2[2] = 1;
		dictionary3.Add(key2, array2);
		dictionary.Add('e', new byte[]
		{
			0,
			0,
			1,
			0,
			1
		});
		Dictionary<char, byte[]> dictionary4 = dictionary;
		char key3 = 'f';
		byte[] array3 = new byte[5];
		array3[2] = 1;
		array3[3] = 1;
		dictionary4.Add(key3, array3);
		dictionary.Add('g', new byte[]
		{
			0,
			0,
			1,
			1,
			1
		});
		Dictionary<char, byte[]> dictionary5 = dictionary;
		char key4 = 'h';
		byte[] array4 = new byte[5];
		array4[1] = 1;
		dictionary5.Add(key4, array4);
		dictionary.Add('i', new byte[]
		{
			0,
			1,
			0,
			0,
			1
		});
		Dictionary<char, byte[]> dictionary6 = dictionary;
		char key5 = 'j';
		byte[] array5 = new byte[5];
		array5[1] = 1;
		array5[3] = 1;
		dictionary6.Add(key5, array5);
		dictionary.Add('k', new byte[]
		{
			0,
			1,
			0,
			1,
			1
		});
		Dictionary<char, byte[]> dictionary7 = dictionary;
		char key6 = 'l';
		byte[] array6 = new byte[5];
		array6[1] = 1;
		array6[2] = 1;
		dictionary7.Add(key6, array6);
		dictionary.Add('m', new byte[]
		{
			0,
			1,
			1,
			0,
			1
		});
		dictionary.Add('n', new byte[]
		{
			0,
			1,
			1,
			1,
			0
		});
		dictionary.Add('o', new byte[]
		{
			0,
			1,
			1,
			1,
			1
		});
		Dictionary<char, byte[]> dictionary8 = dictionary;
		char key7 = 'p';
		byte[] array7 = new byte[5];
		array7[0] = 1;
		dictionary8.Add(key7, array7);
		dictionary.Add('q', new byte[]
		{
			1,
			0,
			0,
			0,
			1
		});
		Dictionary<char, byte[]> dictionary9 = dictionary;
		char key8 = 'r';
		byte[] array8 = new byte[5];
		array8[0] = 1;
		array8[3] = 1;
		dictionary9.Add(key8, array8);
		dictionary.Add('s', new byte[]
		{
			1,
			0,
			0,
			1,
			1
		});
		Dictionary<char, byte[]> dictionary10 = dictionary;
		char key9 = 't';
		byte[] array9 = new byte[5];
		array9[0] = 1;
		array9[2] = 1;
		dictionary10.Add(key9, array9);
		dictionary.Add('u', new byte[]
		{
			1,
			0,
			1,
			0,
			1
		});
		dictionary.Add('v', new byte[]
		{
			1,
			0,
			1,
			1,
			0
		});
		dictionary.Add('w', new byte[]
		{
			1,
			0,
			1,
			1,
			1
		});
		Dictionary<char, byte[]> dictionary11 = dictionary;
		char key10 = 'x';
		byte[] array10 = new byte[5];
		array10[0] = 1;
		array10[1] = 1;
		dictionary11.Add(key10, array10);
		dictionary.Add('y', new byte[]
		{
			1,
			1,
			0,
			0,
			1
		});
		dictionary.Add('z', new byte[]
		{
			1,
			1,
			0,
			1,
			0
		});
		CoredumpingGenerator.CharToB = dictionary;
	}

	public static byte[,] LogoData = new byte[,]
	{
		{
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		},
		{
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0
		},
		{
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1
		},
		{
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1
		},
		{
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1
		},
		{
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			1
		},
		{
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1
		},
		{
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			1,
			0
		}
	};

	public static byte[,] LogoSquareData = new byte[,]
	{
		{
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			0,
			0
		},
		{
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			1,
			1,
			0,
			0
		},
		{
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0
		},
		{
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0
		},
		{
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			0
		},
		{
			0,
			1,
			1,
			1,
			1,
			1,
			0,
			1,
			1,
			1,
			1,
			1,
			1,
			1,
			0
		}
	};

	private static Dictionary<char, byte[]> CharToB;
}
