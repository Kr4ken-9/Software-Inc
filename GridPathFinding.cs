﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class GridPathFinding
{
	public static List<SplineInterp.Point2D> FindPath(SplineInterp.Point2D start, SplineInterp.Point2D end, ref bool[,] grid, bool allowDiag)
	{
		for (int i = 0; i < grid.GetLength(1); i++)
		{
			for (int j = 0; j < grid.GetLength(0); j++)
			{
				GridPathFinding.ClosedGrid[j, i] = false;
				GridPathFinding.OpenGrid[j, i] = false;
			}
		}
		bool flag = grid[end.X, end.Y];
		grid[end.X, end.Y] = true;
		GridPathFinding.OpenCount = 0;
		GridPathFinding.cameFrom.Clear();
		GridPathFinding.cost.Clear();
		GridPathFinding.ecost.Clear();
		GridPathFinding.sorted.Clear();
		GridPathFinding.SetOpen(start.X, start.Y, true);
		GridPathFinding.cost[start.GetHashCode()] = 0f;
		float num = GridPathFinding.cost[start.GetHashCode()] + GridPathFinding.Dist3(start.X, start.Y, end.X, end.Y);
		GridPathFinding.ecost[start.GetHashCode()] = num;
		GridPathFinding.sorted.AddNode(start, num);
		while (GridPathFinding.OpenCount > 0)
		{
			SplineInterp.Point2D min = GridPathFinding.sorted.GetMin();
			if (min == end)
			{
				grid[end.X, end.Y] = flag;
				return GridPathFinding.ReconstructPath(GridPathFinding.cameFrom, end);
			}
			GridPathFinding.SetOpen(min.X, min.Y, false);
			GridPathFinding.ClosedGrid[min.X, min.Y] = true;
			GridPathFinding.sorted.RemoveNode(min);
			GridPathFinding.InnerLoop(min, end, ref grid, allowDiag);
		}
		grid[end.X, end.Y] = flag;
		return null;
	}

	private static void SetOpen(int x, int y, bool open)
	{
		if (open && !GridPathFinding.OpenGrid[x, y])
		{
			GridPathFinding.OpenCount++;
		}
		else if (!open && GridPathFinding.OpenGrid[x, y])
		{
			GridPathFinding.OpenCount--;
		}
		GridPathFinding.OpenGrid[x, y] = open;
	}

	private static void InnerLoop(SplineInterp.Point2D current, SplineInterp.Point2D end, ref bool[,] grid, bool allowDiag)
	{
		if (current.Y > 0 && grid[current.X, current.Y - 1])
		{
			GridPathFinding.InnerCheck(current, end, ref grid, current.X, current.Y - 1);
		}
		if (current.X > 0 && grid[current.X - 1, current.Y])
		{
			GridPathFinding.InnerCheck(current, end, ref grid, current.X - 1, current.Y);
		}
		if (current.Y < grid.GetLength(1) - 1 && grid[current.X, current.Y + 1])
		{
			GridPathFinding.InnerCheck(current, end, ref grid, current.X, current.Y + 1);
		}
		if (current.X < grid.GetLength(0) - 1 && grid[current.X + 1, current.Y])
		{
			GridPathFinding.InnerCheck(current, end, ref grid, current.X + 1, current.Y);
		}
		if (current.Y > 0 && current.X > 0 && (allowDiag || (grid[current.X - 1, current.Y] && grid[current.X, current.Y - 1])) && grid[current.X - 1, current.Y - 1])
		{
			GridPathFinding.InnerCheck(current, end, ref grid, current.X - 1, current.Y - 1);
		}
		if (current.Y < grid.GetLength(1) - 1 && current.X > 0 && (allowDiag || (grid[current.X - 1, current.Y] && grid[current.X, current.Y + 1])) && grid[current.X - 1, current.Y + 1])
		{
			GridPathFinding.InnerCheck(current, end, ref grid, current.X - 1, current.Y + 1);
		}
		if (current.Y > 0 && current.X < grid.GetLength(0) - 1 && (allowDiag || (grid[current.X + 1, current.Y] && grid[current.X, current.Y - 1])) && grid[current.X + 1, current.Y - 1])
		{
			GridPathFinding.InnerCheck(current, end, ref grid, current.X + 1, current.Y - 1);
		}
		if (current.Y < grid.GetLength(1) - 1 && current.X < grid.GetLength(0) - 1 && (allowDiag || (grid[current.X + 1, current.Y] && grid[current.X, current.Y + 1])) && grid[current.X + 1, current.Y + 1])
		{
			GridPathFinding.InnerCheck(current, end, ref grid, current.X + 1, current.Y + 1);
		}
	}

	private static void InnerCheck(SplineInterp.Point2D current, SplineInterp.Point2D end, ref bool[,] grid, int x, int y)
	{
		if (GridPathFinding.ClosedGrid[x, y])
		{
			return;
		}
		float num = GridPathFinding.cost[current.GetHashCode()] + GridPathFinding.Dist2(current.X, current.Y, x, y);
		SplineInterp.Point2D point2D = new SplineInterp.Point2D(x, y);
		bool flag = !GridPathFinding.OpenGrid[x, y];
		if (flag || num < GridPathFinding.cost[point2D.GetHashCode()])
		{
			GridPathFinding.cameFrom[point2D] = current;
			GridPathFinding.cost[point2D.GetHashCode()] = num;
			float num2 = GridPathFinding.cost[point2D.GetHashCode()] + GridPathFinding.Dist3(x, y, end.X, end.Y);
			GridPathFinding.ecost[point2D.GetHashCode()] = num2;
			if (flag)
			{
				GridPathFinding.SetOpen(point2D.X, point2D.Y, true);
				GridPathFinding.sorted.AddNode(point2D, num2);
			}
		}
	}

	private static List<SplineInterp.Point2D> ReconstructPath(Dictionary<SplineInterp.Point2D, SplineInterp.Point2D> cameFrom, SplineInterp.Point2D currentNode)
	{
		List<SplineInterp.Point2D> list;
		if (cameFrom.ContainsKey(currentNode))
		{
			list = GridPathFinding.ReconstructPath(cameFrom, cameFrom[currentNode]);
		}
		else
		{
			list = new List<SplineInterp.Point2D>();
		}
		list.Add(currentNode);
		return list;
	}

	private static SplineInterp.Point2D GetMin(Dictionary<int, SplineInterp.Point2D> nodes)
	{
		float num = float.PositiveInfinity;
		SplineInterp.Point2D result = default(SplineInterp.Point2D);
		foreach (KeyValuePair<int, SplineInterp.Point2D> keyValuePair in nodes)
		{
			float num2 = GridPathFinding.ecost[keyValuePair.Value.GetHashCode()];
			if (num2 < num)
			{
				num = num2;
				result = keyValuePair.Value;
			}
		}
		return result;
	}

	public static float Dist(int x1, int y1, int x2, int y2)
	{
		int num = x1 - x2;
		int num2 = y1 - y2;
		return Mathf.Sqrt((float)(num * num + num2 * num2));
	}

	public static float Dist2(int x1, int y1, int x2, int y2)
	{
		int num = x1 - x2;
		int num2 = y1 - y2;
		return (float)(num * num + num2 * num2);
	}

	public static float Dist3(int x1, int y1, int x2, int y2)
	{
		int a = Mathf.Abs(x1 - x2);
		int b = Mathf.Abs(y1 - y2);
		return (float)Mathf.Max(a, b);
	}

	public static float Dist4(int x1, int y1, int x2, int y2)
	{
		int num = x1 - x2;
		int num2 = y1 - y2;
		return (float)(num + num2);
	}

	private static Dictionary<SplineInterp.Point2D, SplineInterp.Point2D> cameFrom = new Dictionary<SplineInterp.Point2D, SplineInterp.Point2D>(100);

	private static Dictionary<int, float> cost = new Dictionary<int, float>(100);

	private static Dictionary<int, float> ecost = new Dictionary<int, float>(100);

	private static GridPathFinding.SortedList sorted = new GridPathFinding.SortedList();

	private static bool[,] ClosedGrid = new bool[512, 512];

	private static bool[,] OpenGrid = new bool[512, 512];

	private static int OpenCount = 0;

	private class SortedList
	{
		public void AddNode(SplineInterp.Point2D p, float val)
		{
			if (this.Root == null)
			{
				GridPathFinding.SortedList.ListNode listNode = new GridPathFinding.SortedList.ListNode(p, val);
				this.Root = listNode;
				this.Map.Add(p, listNode);
				return;
			}
			GridPathFinding.SortedList.ListNode listNode2 = null;
			if (this.Map.TryGetValue(p, out listNode2))
			{
				listNode2.Key = val;
				if (listNode2.Previous != null)
				{
					listNode2.Previous.Next = listNode2.Next;
				}
				if (listNode2.Next != null)
				{
					listNode2.Next.Previous = listNode2.Previous;
				}
				listNode2.Previous = null;
				listNode2.Next = null;
				if (val < this.Root.Key)
				{
					listNode2.Next = this.Root;
					this.Root.Previous = listNode2;
					this.Root = listNode2;
					return;
				}
			}
			else
			{
				listNode2 = new GridPathFinding.SortedList.ListNode(p, val);
				this.Map.Add(p, listNode2);
			}
			if (val < this.Root.Key)
			{
				listNode2.Next = this.Root;
				this.Root.Previous = listNode2;
				this.Root = listNode2;
				return;
			}
			GridPathFinding.SortedList.ListNode listNode3 = this.Root;
			GridPathFinding.SortedList.ListNode next;
			for (next = this.Root.Next; next != null; next = next.Next)
			{
				if (val < next.Key)
				{
					listNode2.Previous = next.Previous;
					if (next.Previous != null)
					{
						next.Previous.Next = listNode2;
					}
					listNode2.Next = next;
					next.Previous = listNode2;
					break;
				}
				listNode3 = next;
			}
			if (next == null)
			{
				listNode3.Next = listNode2;
				listNode2.Previous = listNode3;
			}
		}

		public void Clear()
		{
			this.Root = null;
			this.Map.Clear();
		}

		public void RemoveNode(SplineInterp.Point2D p)
		{
			GridPathFinding.SortedList.ListNode listNode = null;
			if (this.Map.TryGetValue(p, out listNode))
			{
				if (this.Root == listNode)
				{
					this.Root = listNode.Next;
					if (listNode.Next != null)
					{
						listNode.Next.Previous = null;
					}
				}
				else
				{
					if (listNode.Previous != null)
					{
						listNode.Previous.Next = listNode.Next;
					}
					if (listNode.Next != null)
					{
						listNode.Next.Previous = listNode.Previous;
					}
				}
				this.Map.Remove(p);
			}
		}

		public SplineInterp.Point2D GetMin()
		{
			return this.Root.Value;
		}

		private GridPathFinding.SortedList.ListNode Root;

		private Dictionary<SplineInterp.Point2D, GridPathFinding.SortedList.ListNode> Map = new Dictionary<SplineInterp.Point2D, GridPathFinding.SortedList.ListNode>();

		private class ListNode
		{
			public ListNode(SplineInterp.Point2D value, float key)
			{
				this.Value = value;
				this.Key = key;
			}

			public float Key;

			public SplineInterp.Point2D Value;

			public GridPathFinding.SortedList.ListNode Next;

			public GridPathFinding.SortedList.ListNode Previous;
		}
	}
}
