﻿using System;
using UnityEngine;

public class RectEvent : MonoBehaviour
{
	private void Start()
	{
		this.rect = base.GetComponent<RectTransform>();
	}

	public void MoveY(float units)
	{
		this.rect.anchoredPosition = new Vector2(this.rect.anchoredPosition.x, this.rect.anchoredPosition.y + units);
	}

	public void MoveX(float units)
	{
		this.rect.anchoredPosition = new Vector2(this.rect.anchoredPosition.x + units, this.rect.anchoredPosition.y);
	}

	private RectTransform rect;
}
