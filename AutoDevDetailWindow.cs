﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AutoDevDetailWindow : MonoBehaviour
{
	private void Start()
	{
		this.Window.NonLocTitle = this.workItem.GetDevTeams().First<Team>().Name + "Automation".Loc();
	}

	private void Update()
	{
		bool flag = false;
		string arg = null;
		string text = null;
		string cat = null;
		this.LicenseCost.text = this.workItem.LastLicensePaid.Currency(true);
		List<SoftwareProduct> source = new List<SoftwareProduct>();
		bool flag2 = true;
		if (flag2)
		{
			this.UpdateFeatures();
		}
		if (flag2 && flag)
		{
			this.MainDesc.text = string.Format("AutoDevProjDetails2".Loc(), arg, text.LocSW(), cat.LocSWC(text));
		}
		else if (flag2)
		{
			string text2 = "Not applicable".Loc();
			this.MainDesc.text = string.Format("AutoDevProjDetails2".Loc(), text2, text2, text2);
		}
		for (int i = 0; i < this.WorkItemList.Items.Count; i++)
		{
			WorkItem workItem = this.WorkItemList.Items[i] as WorkItem;
			if (workItem != null && workItem.Done)
			{
				this.WorkItemList.Items.RemoveAt(i);
				i--;
			}
		}
		if (flag2)
		{
			this.LicenseList.Items.Clear();
			this.LicenseList.Items.AddRange(source.Distinct<SoftwareProduct>().Cast<object>());
		}
	}

	private void UpdateFeatures()
	{
	}

	private void UpdateListWith(uint w)
	{
		WorkItem w2 = GameSettings.Instance.MyCompany.WorkItems.FirstOrDefault((WorkItem x) => x.ID == w);
		this.UpdateListWith(w2);
	}

	private void UpdateListWith(WorkItem w)
	{
		if (w != null && !this.WorkItemList.Items.Contains(w))
		{
			this.WorkItemList.Items.Add(w);
		}
	}

	public GUIWindow Window;

	public GUIListView WorkItemList;

	public GUIListView FeatureList;

	public GUIListView LicenseList;

	public Text MainDesc;

	public Text LicenseCost;

	[NonSerialized]
	public AutoDevWorkItem workItem;

	[NonSerialized]
	public SoftwareWorkItem lastProj;
}
