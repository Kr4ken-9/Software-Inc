﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TreeGen : Writeable, IHasVector
{
	public Bounds bounds
	{
		get
		{
			return new Bounds(base.GetComponent<Renderer>().bounds.center, this.size);
		}
	}

	private CombineInstance GenerateProtoMesh(Vector3 start, Vector3 stop, float width, float width2, bool first)
	{
		CombineInstance result = default(CombineInstance);
		result.mesh = this.ProtoMesh;
		Vector3 forward = stop - start;
		if (stop.y == start.y)
		{
			start -= Vector3.up * width;
			stop -= Vector3.up * width;
		}
		else if (!first)
		{
			Vector3 b = new Vector3(this.Sign(forward.x) * width2, 0f, this.Sign(forward.z) * width2);
			start -= Vector3.up * width2 + b;
			stop -= b;
		}
		forward = stop - start;
		result.transform = Matrix4x4.TRS(Vector3.Lerp(start, stop, 0.5f), Quaternion.LookRotation(forward), new Vector3(width, width, forward.magnitude));
		return result;
	}

	private void GenerateLeaves(Vector3 pos)
	{
		GameObject gameObject = new GameObject("Leaves");
		MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
		meshFilter.mesh = this.ProtoMesh;
		Renderer renderer = gameObject.AddComponent<MeshRenderer>();
		renderer.material = this.LeafMat;
		float d = UnityEngine.Random.Range(this.LeafMin, this.LeafMax);
		gameObject.transform.parent = base.transform;
		gameObject.transform.localPosition = pos;
		gameObject.transform.rotation = Quaternion.LookRotation(UnityEngine.Random.onUnitSphere);
		gameObject.transform.localScale = Vector3.one * d;
	}

	private float Sign(float input)
	{
		if (Mathf.Approximately(input, 0f))
		{
			return 0f;
		}
		return (float)((input <= 0f) ? -1 : 1);
	}

	public void InitNow()
	{
		if (!this.isInitiated)
		{
			if (this.Root == null)
			{
				if (this.Type == 0 || this.Type == 2)
				{
					this.Root = this.GenerateTree(new Vector3(0f, UnityEngine.Random.Range(this.MinHeight, this.MaxHeight), 0f), 0, false, true);
					this.Root = new TreeGen.TreeNode(Vector3.zero, this.MaxWidth)
					{
						Children = 
						{
							this.Root
						}
					};
				}
				else
				{
					this.Root = this.GenerateTree2();
				}
			}
			if (this.TreeMesh.sharedMesh == null)
			{
				if (this.Type == 0 || this.Type == 2)
				{
					this.GenerateTreeMesh();
					this.ChangeLeaveSize(this.currentSize);
				}
				else
				{
					this.GenerateTreeMesh2();
				}
			}
			base.GetComponentsInChildren<Renderer>().ToList<Renderer>().ForEach(delegate(Renderer x)
			{
				x.enabled = !this.Cached;
			});
			base.GetComponent<Renderer>().enabled = this.ForceVisible;
			foreach (Transform transform in base.GetComponentsInChildren<Transform>())
			{
				if (!(transform.gameObject == base.gameObject))
				{
					float value = UnityEngine.Random.Range(this.LeafMin, this.LeafMax);
					this.Leaves.Add(transform.gameObject, value);
				}
			}
			this.isInitiated = true;
		}
	}

	public IEnumerable<MeshFilter> GetAllLeaves()
	{
		foreach (MeshFilter leaf in base.GetComponentsInChildren<MeshFilter>())
		{
			if (!(leaf.gameObject == base.gameObject))
			{
				yield return leaf;
			}
		}
		yield break;
	}

	private void Start()
	{
		base.InitWritable();
		this.InitNow();
	}

	public void ChangeLeaveSize(float val)
	{
		foreach (KeyValuePair<GameObject, float> keyValuePair in this.Leaves)
		{
			keyValuePair.Key.transform.localScale = Vector3.one * keyValuePair.Value * val;
		}
	}

	private void GenerateTreeMesh()
	{
		List<CombineInstance> list = new List<CombineInstance>();
		this.subTreeMesh(this.Root, this.Root.Children[0], list, 0f, true);
		Mesh mesh = new Mesh();
		mesh.CombineMeshes(list.ToArray());
		this.TreeMesh.sharedMesh = mesh;
		this.size = this.TreeMesh.sharedMesh.bounds.size;
	}

	private void GenerateTreeMesh2()
	{
		float width = this.Root.Width;
		CombineInstance combineInstance = default(CombineInstance);
		combineInstance.mesh = this.ProtoMesh;
		combineInstance.transform = Matrix4x4.TRS(Vector3.up * width / 2f, Quaternion.identity, new Vector3(0.3f, width, 0.3f));
		Mesh mesh = new Mesh();
		mesh.CombineMeshes(new CombineInstance[]
		{
			combineInstance
		});
		this.TreeMesh.sharedMesh = mesh;
		TreeGen.TreeNode treeNode = this.Root;
		float num = 0f;
		while (treeNode.Children.Count > 0)
		{
			TreeGen.TreeNode treeNode2 = treeNode.Children[0];
			GameObject gameObject = new GameObject("Leaves");
			MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
			meshFilter.mesh = this.ProtoMesh2;
			Renderer renderer = gameObject.AddComponent<MeshRenderer>();
			renderer.material = this.LeafMat;
			gameObject.transform.parent = base.transform;
			gameObject.transform.localPosition = new Vector3(0f, treeNode2.Pos.y, 0f);
			gameObject.transform.rotation = Quaternion.Euler(0f, treeNode2.Pos.x, 0f);
			gameObject.transform.localScale = Vector3.one * treeNode2.Pos.z;
			num = Mathf.Max(treeNode2.Pos.z, num);
			treeNode = treeNode2;
		}
		this.size = new Vector3(num + 0.4f, this.TreeMesh.sharedMesh.bounds.size.y, num + 0.4f);
	}

	private void subTreeMesh(TreeGen.TreeNode parent, TreeGen.TreeNode current, List<CombineInstance> instances, float width, bool first = true)
	{
		instances.Add(this.GenerateProtoMesh(parent.Pos, current.Pos, parent.Width, width, first));
		foreach (TreeGen.TreeNode current2 in current.Children)
		{
			this.subTreeMesh(current, current2, instances, parent.Width, false);
		}
		if (current.Children.Count == 0 && this.Type == 0)
		{
			this.GenerateLeaves(current.Pos);
		}
	}

	private void Update()
	{
		if (this.currentSize != this.LeafSize)
		{
			this.currentSize = Mathf.Lerp(this.currentSize, this.LeafSize, 0.1f);
			if (Mathf.Approximately(this.currentSize, this.LeafSize))
			{
				this.currentSize = this.LeafSize;
			}
			this.ChangeLeaveSize(this.currentSize);
		}
	}

	private TreeGen.TreeNode GenerateTree2()
	{
		float num = UnityEngine.Random.Range(this.MinHeight, this.MaxHeight);
		float num2 = num - UnityEngine.Random.Range(this.MinRange, this.MaxRange);
		TreeGen.TreeNode treeNode = new TreeGen.TreeNode(Vector3.zero, num);
		TreeGen.TreeNode treeNode2 = treeNode;
		float num3 = num2 / (float)this.MaxNodeHeight;
		float num4 = (this.LeafMax - this.LeafMin) / (float)this.MaxNodeHeight;
		for (int i = 0; i < this.MaxNodeHeight; i++)
		{
			TreeGen.TreeNode treeNode3 = new TreeGen.TreeNode(new Vector3(UnityEngine.Random.Range(0f, 360f), num - (float)i * num3, this.LeafMin + (float)i * num4), 0f);
			treeNode2.Children.Add(treeNode3);
			treeNode2 = treeNode3;
		}
		return treeNode;
	}

	private TreeGen.TreeNode GenerateTree(Vector3 pos, int level, bool OnlyUp, bool first = true)
	{
		float num = 1f - (float)Mathf.Min(level + 1, this.MaxNodeHeight + 1) / (float)(this.MaxNodeHeight + 2);
		float width = this.MinWidth + num * (this.MaxWidth - this.MinWidth);
		TreeGen.TreeNode treeNode = new TreeGen.TreeNode(pos, width);
		if (level < this.MaxNodeHeight && !OnlyUp)
		{
			if (first || UnityEngine.Random.Range(0, 4) != 0)
			{
				TreeGen.TreeNode item = this.GenerateTree(pos + new Vector3(UnityEngine.Random.Range(this.MinRange, this.MaxRange) * num, 0f, 0f), level + UnityEngine.Random.Range(1, this.MaxNodeHeight - level + 1), true, false);
				treeNode.Children.Add(item);
			}
			if (first || UnityEngine.Random.Range(0, 4) != 0)
			{
				TreeGen.TreeNode item2 = this.GenerateTree(pos - new Vector3(UnityEngine.Random.Range(this.MinRange, this.MaxRange) * num, 0f, 0f), level + UnityEngine.Random.Range(1, this.MaxNodeHeight - level + 1), true, false);
				treeNode.Children.Add(item2);
			}
			if (first || UnityEngine.Random.Range(0, 4) != 0)
			{
				TreeGen.TreeNode item3 = this.GenerateTree(pos + new Vector3(0f, 0f, UnityEngine.Random.Range(this.MinRange, this.MaxRange) * num), level + UnityEngine.Random.Range(1, this.MaxNodeHeight - level + 1), true, false);
				treeNode.Children.Add(item3);
			}
			if (first || UnityEngine.Random.Range(0, 4) != 0)
			{
				TreeGen.TreeNode item4 = this.GenerateTree(pos - new Vector3(0f, 0f, UnityEngine.Random.Range(this.MinRange, this.MaxRange) * num), level + UnityEngine.Random.Range(1, this.MaxNodeHeight - level + 1), true, false);
				treeNode.Children.Add(item4);
			}
		}
		if (OnlyUp || (level < this.MaxNodeHeight && UnityEngine.Random.Range(0, 2) == 0))
		{
			TreeGen.TreeNode item5 = this.GenerateTree(pos + new Vector3(0f, UnityEngine.Random.Range(this.MinHeight, this.MaxHeight) * num, 0f), level + UnityEngine.Random.Range(1, this.MaxNodeHeight - level + 1), false, false);
			treeNode.Children.Add(item5);
		}
		return treeNode;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["Root"] = this.Root;
		dictionary["Position"] = base.transform.position;
		dictionary["Rotation"] = base.transform.rotation;
		dictionary["LeafSize"] = this.LeafSize;
		dictionary["Type"] = this.Type;
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		this.Root = (TreeGen.TreeNode)dictionary["Root"];
		base.transform.position = ((SVector3)dictionary["Position"]).ToVector3();
		base.transform.rotation = ((SVector3)dictionary["Rotation"]).ToQuaternion();
		this.LeafSize = (float)dictionary["LeafSize"];
		this.currentSize = this.LeafSize;
		this.Type = dictionary.Get<int>("Type", 0);
		return this;
	}

	public override string WriteName()
	{
		return "Tree";
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawCube(this.bounds.center, this.bounds.size);
	}

	public Vector2 GetPos()
	{
		return base.transform.position.FlattenVector3();
	}

	public float MinHeight;

	public float MaxHeight;

	public float MinRange;

	public float MaxRange;

	public float MinWidth;

	public float MaxWidth;

	public float LeafMin;

	public float LeafMax;

	public int MaxNodeHeight;

	public int Type;

	public Mesh ProtoMesh;

	public Mesh ProtoMesh2;

	public Material WoodMat;

	public Material LeafMat;

	public float LeafSize = 1f;

	public bool Cached;

	private float currentSize = 1f;

	private bool isInitiated;

	[NonSerialized]
	private TreeGen.TreeNode Root;

	private Dictionary<GameObject, float> Leaves = new Dictionary<GameObject, float>();

	public bool ForceVisible;

	public Vector3 size;

	public MeshFilter TreeMesh;

	[Serializable]
	public class TreeNode
	{
		public TreeNode(Vector3 pos, float width)
		{
			this.Pos = pos;
			this.Width = width;
			this.Children = new List<TreeGen.TreeNode>();
		}

		public TreeNode()
		{
		}

		public Vector3 Pos
		{
			get
			{
				return this.pos.ToVector3();
			}
			set
			{
				this.pos = value;
			}
		}

		private SVector3 pos;

		public List<TreeGen.TreeNode> Children;

		public float Width;
	}
}
