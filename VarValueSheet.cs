﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VarValueSheet : Graphic, IScrollHandler, IEventSystemHandler
{
	protected override void OnPopulateMesh(VertexHelper h)
	{
		h.Clear();
		Utilities.VBOToHelper(this.MeshData, h);
	}

	protected override void OnRectTransformDimensionsChange()
	{
		this.UpdateScroll();
		this.UpdateText();
	}

	private float CorrectHeight(float height)
	{
		if (Options.UISize > 1f && Options.UISize < 2f && Mathf.RoundToInt(Options.UISize * 10f % 2f) == 0)
		{
			return height - 1f;
		}
		return height;
	}

	private void GenerateMesh()
	{
		this.MeshData.Clear();
		Vector2 vector = Vector2.zero - base.rectTransform.pivot;
		Vector2 vector2 = Vector2.one - base.rectTransform.pivot;
		vector = new Vector2(vector.x * base.rectTransform.rect.width, vector.y * base.rectTransform.rect.height);
		vector2 = new Vector2(vector2.x * base.rectTransform.rect.width, vector2.y * base.rectTransform.rect.height);
		int num = this.MaxItems();
		int num2 = Mathf.Max(0, this.vars.Length - num);
		int num3 = Mathf.FloorToInt(this.Scroll.value * (float)num2);
		num3 = Mathf.Min(num2, num3);
		float num4 = this.CorrectHeight(this.TextHeight);
		for (int i = 0; i < num; i++)
		{
			Color c = ((i + num3) % 2 != 0) ? this.C2 : this.C1;
			UIVertex item = default(UIVertex);
			item.position = new Vector3(vector.x, vector2.y - (float)i * num4, 0f);
			item.color = c;
			this.MeshData.Add(item);
			item = default(UIVertex);
			item.position = new Vector3(vector2.x, vector2.y - (float)i * num4, 0f);
			item.color = c;
			this.MeshData.Add(item);
			item = default(UIVertex);
			item.position = new Vector3(vector2.x, vector2.y - (float)i * num4 - num4, 0f);
			item.color = c;
			this.MeshData.Add(item);
			item = default(UIVertex);
			item.position = new Vector3(vector.x, vector2.y - (float)i * num4 - num4, 0f);
			item.color = c;
			this.MeshData.Add(item);
		}
		float num5 = base.rectTransform.rect.height % num4;
		if (num5 > 0f)
		{
			Color c2 = ((num + num3) % 2 != 0) ? this.C2 : this.C1;
			UIVertex item2 = default(UIVertex);
			item2.position = new Vector3(vector.x, vector2.y - (float)num * num4, 0f);
			item2.color = c2;
			this.MeshData.Add(item2);
			item2 = default(UIVertex);
			item2.position = new Vector3(vector2.x, vector2.y - (float)num * num4, 0f);
			item2.color = c2;
			this.MeshData.Add(item2);
			item2 = default(UIVertex);
			item2.position = new Vector3(vector2.x, vector2.y - (float)num * num4 - num5, 0f);
			item2.color = c2;
			this.MeshData.Add(item2);
			item2 = default(UIVertex);
			item2.position = new Vector3(vector.x, vector2.y - (float)num * num4 - num5, 0f);
			item2.color = c2;
			this.MeshData.Add(item2);
		}
		this.SetVerticesDirty();
	}

	public void OnScroll()
	{
		this.UpdateText();
		this.GenerateMesh();
	}

	public int MaxItems()
	{
		return Mathf.FloorToInt(base.rectTransform.rect.height / this.CorrectHeight(this.TextHeight));
	}

	public void SetData(string[] var, string[] value)
	{
		this.vars = var;
		this.values = value;
		this.UpdateScroll();
		this.Scroll.value = 0f;
		this.UpdateText();
	}

	public void SetData(string data)
	{
		string[] arr = data.Split(new char[]
		{
			'\n'
		});
		string[][] arr2 = arr.SelectInPlace((string x) => x.Split(new char[]
		{
			'\t'
		}));
		this.vars = arr2.SelectInPlace((string[] x) => x[0]);
		this.values = arr2.SelectInPlace((string[] x) => x[1]);
		this.UpdateScroll();
		this.Scroll.value = 0f;
		this.UpdateText();
	}

	public void UpdateValues(string[] value)
	{
		this.values = value;
		this.UpdateText();
	}

	public void UpdateText()
	{
		if (this.ValueText.rectTransform == null)
		{
			return;
		}
		int num = this.MaxItems();
		int num2 = Mathf.Max(0, this.vars.Length - num);
		int num3 = Mathf.FloorToInt(this.Scroll.value * (float)num2);
		num3 = Mathf.Min(num2, num3);
		this.VarText.text = string.Join("\n", this.vars.Skip(num3).Take(num).ToArray<string>());
		this.ValueText.text = string.Join("\n", this.values.Skip(num3).Take(num).ToArray<string>());
		float num4 = base.rectTransform.rect.width % 1f;
		this.ValueText.rectTransform.anchoredPosition = new Vector2(-7f + num4, this.ValueText.rectTransform.anchoredPosition.y);
		this.ValueText.rectTransform.sizeDelta = new Vector2(-23f + num4, this.ValueText.rectTransform.sizeDelta.y);
	}

	public void UpdateScroll()
	{
		int num = Mathf.Max(0, this.vars.Length - this.MaxItems());
		this.Scroll.numberOfSteps = num + 1;
		this.Scroll.size = 1f / (float)(num + 1);
		this.GenerateMesh();
	}

	public void OnScroll(PointerEventData eventData)
	{
		int num = Mathf.Max(0, this.vars.Length - this.MaxItems());
		if (num > 0)
		{
			this.Scroll.value -= eventData.scrollDelta.y * (1f / (float)num);
		}
	}

	public Text VarText;

	public Text ValueText;

	public Scrollbar Scroll;

	public Color C1;

	public Color C2;

	[NonSerialized]
	private string[] vars = new string[0];

	[NonSerialized]
	private string[] values = new string[0];

	[NonSerialized]
	private List<UIVertex> MeshData = new List<UIVertex>();

	public float TextHeight = 24f;
}
