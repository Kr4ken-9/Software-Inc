﻿using System;
using UnityEngine;

public class RoadLightScript : MonoBehaviour
{
	private void Start()
	{
		this.localLight.enabled = false;
		this.Cone.gameObject.SetActive(false);
	}

	private bool ToggleNow()
	{
		if (!HUD.Instance.IsReferenceNull() && HUD.Instance.BuildMode)
		{
			return HUD.Instance.SunSlider.value < 0.5f;
		}
		return TimeOfDay.Instance.Hour > 19 || TimeOfDay.Instance.Hour < 7;
	}

	private void Update()
	{
		bool flag = GameSettings.Instance.ActiveFloor >= 0;
		if (this.rend.enabled != flag)
		{
			this.rend.enabled = flag;
		}
		bool flag2 = GameSettings.Instance.ActiveFloor >= 0 && this.ToggleNow();
		if (this.On != flag2)
		{
			this.On = flag2;
			this.localLight.enabled = this.On;
			this.Cone.gameObject.SetActive(this.On);
			Color color = this.LightColor.Evaluate(UnityEngine.Random.value);
			this.localLight.color = color;
			this.Cone.material.color = color;
		}
		if (this.localLight.enabled && (this.localLight.shadows == LightShadows.Hard ^ Options.MoreShadow))
		{
			this.localLight.shadows = ((!Options.MoreShadow) ? LightShadows.None : LightShadows.Hard);
		}
	}

	private bool On;

	public float Intensity = 1f;

	public Light localLight;

	public Renderer rend;

	public Renderer Cone;

	public Gradient LightColor;
}
