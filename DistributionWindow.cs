﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DistributionWindow : MonoBehaviour
{
	private void Start()
	{
		this.Chart.ToolTipFunc = ((int i, float f) => (SDateTime.Now() - new SDateTime(0, 0, 20 - i, 0, 0)).ToCompactString2() + ": " + (f * 100f).ToString("N2") + "%");
		this.Window.OnSizeChanged = delegate
		{
			this.Chart.UpdateCachedLines();
		};
		this.UpdateOrders();
	}

	public void LimitSelected()
	{
		List<PrintJob> sel = (from x in this.OrderList.GetSelected<PrintJob>()
		where x.DealID == null
		select x).ToList<PrintJob>();
		if (sel.Count > 0)
		{
			uint? num = null;
			foreach (PrintJob printJob in sel)
			{
				if (printJob.Limit != null && (num == null || printJob.Limit.Value > num.Value))
				{
					num = new uint?(printJob.Limit.Value);
				}
			}
			WindowManager.SpawnInputDialog("LimitCopyPrompt".Loc(), "Maximum copies".Loc(), (num == null) ? "-1" : num.Value.ToString("N0"), delegate(string x)
			{
				if (x.Trim().Equals("-1"))
				{
					foreach (PrintJob printJob2 in sel)
					{
						printJob2.Limit = null;
					}
				}
				else
				{
					try
					{
						uint value = Convert.ToUInt32(x.Replace(",", string.Empty));
						foreach (PrintJob printJob3 in sel)
						{
							printJob3.Limit = new uint?(value);
							printJob3.Maximum = null;
						}
					}
					catch (Exception)
					{
					}
				}
			}, null);
		}
	}

	public void MaximumSelected()
	{
		List<PrintJob> sel = (from x in this.OrderList.GetSelected<PrintJob>()
		where x.DealID == null
		select x).ToList<PrintJob>();
		if (sel.Count > 0)
		{
			uint? num = null;
			foreach (PrintJob printJob in sel)
			{
				if (printJob.Maximum != null && (num == null || printJob.Maximum.Value > num.Value))
				{
					num = new uint?(printJob.Maximum.Value);
				}
			}
			WindowManager.SpawnInputDialog("MaximumCopyPrompt".Loc(), "Maximum copies".Loc(), (num == null) ? "-1" : num.Value.ToString("N0"), delegate(string x)
			{
				if (x.Trim().Equals("-1"))
				{
					foreach (PrintJob printJob2 in sel)
					{
						printJob2.Maximum = null;
					}
				}
				else
				{
					try
					{
						uint value = Convert.ToUInt32(x.Replace(",", string.Empty));
						foreach (PrintJob printJob3 in sel)
						{
							printJob3.Maximum = new uint?(value);
							printJob3.Limit = null;
						}
					}
					catch (Exception)
					{
					}
				}
			}, null);
		}
	}

	public void CancelSelected()
	{
		PrintJob[] selected = this.OrderList.GetSelected<PrintJob>();
		if (selected.Length > 0)
		{
			for (int i = 0; i < selected.Length; i++)
			{
				GameSettings.Instance.CancelPrintOrder(selected[i].ID, true);
			}
			HUD.Instance.distributionWindow.RefreshOrders();
		}
	}

	public void Show(PrintJob order = null)
	{
		if (this.Window.Shown && order == null)
		{
			this.Window.Close();
			return;
		}
		this.Window.Show();
		this.Chart.Values = new List<List<float>>
		{
			GameSettings.Instance.simulation.PlayerDigitalShare
		};
		this.Chart.UpdateCachedLines();
		this.UpdateDistributionDeals();
		this.UpdateStoreButton();
		HUD.Instance.distributionWindow.RefreshOrders();
		int num = this.OrderList.ActualItems.IndexOf(order);
		if (num >= 0)
		{
			this.OrderList.ClearSelected();
			this.OrderList.Select(num);
			this.OrderList.KeepIdxInView(num);
		}
		TutorialSystem.Instance.StartTutorial("Distribution", false);
	}

	public void OpenDistribution()
	{
		if (GameSettings.Instance.Distribution == null)
		{
			string[] servers = GameSettings.Instance.GetServerNames();
			if (servers.Length > 0)
			{
				WindowManager.Instance.ShowMessageBox("OnlineStoreConfirmation".Loc(), true, DialogWindow.DialogType.Question, delegate
				{
					SelectorController.Instance.selectWindow.Show("Server", servers, delegate(int i)
					{
						GameSettings.Instance.Distribution = new PlayerDistribution();
						GameSettings.Instance.RegisterWithServer(servers[i], GameSettings.Instance.Distribution, true);
						this.UpdateStoreButton();
					}, false, true, true, false, null);
				}, null, null);
			}
			else
			{
				WindowManager.Instance.ShowMessageBox("OnlineStoreServerError".Loc(), false, DialogWindow.DialogType.Error);
			}
		}
		else
		{
			WindowManager.Instance.ShowMessageBox("OnlineStoreCloseConfirmation".Loc(), true, DialogWindow.DialogType.Question, delegate
			{
				GameSettings.Instance.DeregisterServerItem(GameSettings.Instance.Distribution);
				GameSettings.Instance.Distribution = null;
				this.UpdateStoreButton();
				foreach (Company company in GameSettings.Instance.simulation.GetAllCompanies())
				{
					if (company.DistributionDeal != null)
					{
						company.DistributionDeal = null;
						company.LastDistributionOffer = SDateTime.Now();
					}
				}
				this.UpdateDistributionDeals();
			}, null, null);
		}
	}

	private void UpdateStoreButton()
	{
		this.OpenStoreButtonLabel.text = ((GameSettings.Instance.Distribution != null) ? "Close digital distribution platform".Loc() : "Open digital distribution platform".Loc());
	}

	private void Update()
	{
		this.UpdateOrders();
	}

	private void UpdateOrders()
	{
		float num = GameSettings.Instance.ProductPrinters.SumSafe((ProductPrinter x) => x.ActalPrintSpeed() * 24f * (float)GameSettings.DaysPerMonth);
		this.PrintSpeed = GameSettings.Instance.ProductPrinters.SumSafe((ProductPrinter x) => x.ActalPrintSpeed() * 24f * (float)GameSettings.DaysPerMonth * (float)x.PrintAmount);
		Text printingCapacity = this.PrintingCapacity;
		string format = "{0}: {1}{2} / {5}{2}\n{3}: {4}{2}";
		object[] array = new object[6];
		array[0] = "Printing capacity".Loc();
		array[1] = "CopiesPostfix".Loc(new object[]
		{
			Mathf.RoundToInt(this.PrintSpeed).ToString("N0")
		});
		array[2] = "PerMonth".Loc();
		array[3] = "Shipping capacity".Loc();
		int num2 = 4;
		string input = "BoxPostfix";
		object[] array2 = new object[1];
		array2[0] = (GameSettings.Instance.sActorManager.Staff.Count((Actor x) => !x.OnCall && x.AItype == AI<Actor>.AIType.Courier) * CourierAI.MaxBoxes).ToString("N0");
		array[num2] = input.Loc(array2);
		array[5] = "BoxPostfix".Loc(new object[]
		{
			num.ToString("N0")
		});
		printingCapacity.text = string.Format(format, array);
		this.PrioritySum = GameSettings.Instance.PrintOrders.Values.SumSafe((PrintJob x) => x.Priority);
		if (this.PrioritySum == 0f)
		{
			this.PrioritySum = 1f;
		}
	}

	public void RefreshOrders()
	{
		HashSet<PrintJob> hashSet = this.OrderList.Items.Cast<PrintJob>().ToHashSet<PrintJob>();
		HashSet<PrintJob> hashSet2 = GameSettings.Instance.PrintOrders.Values.ToHashSet<PrintJob>();
		foreach (PrintJob item in hashSet)
		{
			if (!hashSet2.Contains(item))
			{
				this.OrderList.Items.Remove(item);
			}
		}
		foreach (PrintJob item2 in hashSet2)
		{
			if (!hashSet.Contains(item2))
			{
				this.OrderList.Items.Add(item2);
			}
		}
		this.OrderList.UpdateElements();
	}

	public void UpdateDistributionDeals()
	{
		this.DistDealList.Items = (from x in GameSettings.Instance.simulation.GetAllCompanies()
		where x.DistributionDeal != null
		select x).Cast<object>().ToList<object>();
	}

	public GUIWindow Window;

	public GUIListView List;

	public GUIListView DistDealList;

	public GUILineChart Chart;

	public GUICombobox StartServer;

	public GUICombobox CurrentServer;

	public GameObject StartPanel;

	public GameObject RunningPanel;

	public Slider RevCut;

	public InputField ChannelName;

	public Text RevCutText;

	public Text OfflineButton;

	public Text PrintingCapacity;

	public Button DealButton;

	public GUIListView OrderList;

	public Text OpenStoreButtonLabel;

	public float PrintSpeed;

	public float PrioritySum = 1f;
}
