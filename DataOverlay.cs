﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class DataOverlay : MonoBehaviour
{
	private static Color Rent(Room room)
	{
		if (GameSettings.Instance.RentMode || GameSettings.Instance.EditMode)
		{
			return (!room.Rentable) ? Color.black : ((!room.PlayerOwned) ? DataOverlay.GetRoomColor(room) : Color.white);
		}
		return Color.white;
	}

	private static Color Insulation(Room room)
	{
		if (room.Insulation <= 1f)
		{
			return Color.Lerp(HUD.ThemeColors[0], HUD.ThemeColors[1], room.Insulation.MapRange(0.5f, 1f, 0f, 1f, false));
		}
		return Color.Lerp(HUD.ThemeColors[1], HUD.ThemeColors[2], room.Insulation - 1f);
	}

	private static Color RoomSatisfaction(Room room)
	{
		if (!room.Occupants.Any((Actor x) => x.AItype == AI<Actor>.AIType.Employee))
		{
			return HUD.ThemeColors[1];
		}
		return Color.Lerp(HUD.ThemeColors[2], HUD.ThemeColors[0], (from x in room.Occupants
		where x.AItype == AI<Actor>.AIType.Employee
		select x).Average((Actor x) => x.employee.JobSatisfaction));
	}

	private static Color RoomEffectiveness(Room room)
	{
		if (!room.Occupants.Any((Actor x) => x.AItype == AI<Actor>.AIType.Employee))
		{
			return HUD.ThemeColors[1];
		}
		return Color.Lerp(HUD.ThemeColors[2], HUD.ThemeColors[0], (from x in room.Occupants
		where x.AItype == AI<Actor>.AIType.Employee
		select x).Average((Actor x) => x.Effectiveness));
	}

	private static Color RoomTemperature(Room room)
	{
		if (room.Temperature > 21f)
		{
			return Color.Lerp(HUD.ThemeColors[0], HUD.ThemeColors[2], (room.Temperature - 21f) / 24f);
		}
		return Color.Lerp(HUD.ThemeColors[0], HUD.ThemeColors[1], (21f - room.Temperature) / 24f);
	}

	private static Color RoomMaintenance(Room room)
	{
		float num = 1f;
		bool flag = false;
		List<Furniture> furnitures = room.GetFurnitures();
		for (int i = 0; i < furnitures.Count; i++)
		{
			Furniture furniture = furnitures[i];
			if (furniture.HasUpg)
			{
				num = Mathf.Min(furniture.upg.Quality, num);
				flag = true;
			}
		}
		return (!flag) ? HUD.ThemeColors[1] : Color.Lerp(HUD.ThemeColors[2], HUD.ThemeColors[0], num);
	}

	public static Color GetRoomColor(Room r)
	{
		Room room = r.ParentRoom ?? r;
		float h = Mathf.Abs(room.Center.x * room.Center.y + (float)(room.Floor * 20)) % 360f;
		Vector3 vector = Utilities.HSVToRGB(h, 0.75f, 1f);
		return new Color(vector.x, vector.y, vector.z, 1f);
	}

	public static bool HasActive
	{
		get
		{
			return DataOverlay.Instance != null && DataOverlay.Instance.ActiveOverlay != null;
		}
	}

	private void Awake()
	{
		if (DataOverlay.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		DataOverlay.Instance = this;
	}

	private void Start()
	{
		foreach (string text in DataOverlay.OverlayFuncs.Keys)
		{
			Toggle toggle = UnityEngine.Object.Instantiate<Toggle>(this.TogglePrefab);
			toggle.transform.SetParent(this.TogglePanel, false);
			toggle.isOn = false;
			toggle.GetComponentInChildren<Text>().text = text.Loc();
			toggle.name = text;
			string overlay1 = text;
			toggle.onValueChanged.AddListener(delegate(bool x)
			{
				if (x)
				{
					this.ActivateFunc(overlay1);
				}
				else if (!this.MainToggleGroup.AnyTogglesOn())
				{
					this.ActivateFunc(null);
				}
			});
			toggle.group = this.MainToggleGroup;
			this.DataToggles[text] = toggle;
		}
	}

	private void OnDestroy()
	{
		if (DataOverlay.Instance == this)
		{
			DataOverlay.Instance = null;
		}
	}

	public void ActivateFunc(string func)
	{
		if (this.ActiveOverlay == null)
		{
			this.ActivateTime = Time.timeSinceLevelLoad;
		}
		this.ActiveOverlay = ((func != null) ? DataOverlay.OverlayFuncs[func] : null);
		this.ActiveOverlayName = func;
		if (this.ActiveOverlay != null)
		{
			Toggle orNull = this.DataToggles.GetOrNull(this.ActiveOverlayName);
			if (orNull != null)
			{
				orNull.isOn = true;
			}
			this.grPanel.Gradients = this.ActiveOverlay.Categories.Categories.SelectMany((DataOverlay.ColorCategory x) => from y in x.Colors
			select new KeyValuePair<Color, bool>(y, x.Interpolate)).ToList<KeyValuePair<Color, bool>>();
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this.ActiveOverlay.Categories.Categories.Length; i++)
			{
				DataOverlay.ColorCategory colorCategory = this.ActiveOverlay.Categories.Categories[i];
				if (colorCategory.Colors.Length == 1)
				{
					stringBuilder.AppendLine(colorCategory.Name.Loc());
				}
				else
				{
					int num = colorCategory.Colors.Length / 2;
					if (colorCategory.Colors.Length % 2 == 0)
					{
						for (int j = 0; j < num - 1; j++)
						{
							stringBuilder.AppendLine(string.Empty);
						}
					}
					else
					{
						for (int k = 0; k < num; k++)
						{
							stringBuilder.AppendLine(string.Empty);
						}
					}
					stringBuilder.AppendLine(colorCategory.Name.Loc());
					for (int l = 0; l < num; l++)
					{
						stringBuilder.AppendLine(string.Empty);
					}
				}
			}
			this.DataDesc.text = stringBuilder.ToString();
		}
		else
		{
			this.grPanel.Gradients.Clear();
			this.DataDesc.text = string.Empty;
			foreach (KeyValuePair<string, Toggle> keyValuePair in this.DataToggles)
			{
				keyValuePair.Value.isOn = false;
			}
		}
		this.grPanel.SetVerticesDirty();
		if (HUD.Instance != null)
		{
			HUD.Instance.DataOverlayToggle.isOn = (this.ActiveOverlay != null);
		}
	}

	public Color GetColor(Color color)
	{
		if (this.ActiveOverlay == null)
		{
			return color;
		}
		float num = Mathf.Clamp01((Time.timeSinceLevelLoad - this.ActivateTime) * 4f);
		if (num == 1f)
		{
			return DataOverlay.Surroundings;
		}
		return Color.Lerp(color, DataOverlay.Surroundings, num);
	}

	public Color GetColor(Room room, Color defaultColor, Color currentColor)
	{
		if (this.ActiveOverlay == null)
		{
			return defaultColor;
		}
		return Color.Lerp(currentColor, this.ActiveOverlay.Func(room), Time.deltaTime * 5f);
	}

	static DataOverlay()
	{
		// Note: this type is marked as 'beforefieldinit'.
		Dictionary<string, DataOverlay.Overlay> dictionary = new Dictionary<string, DataOverlay.Overlay>();
		Dictionary<string, DataOverlay.Overlay> dictionary2 = dictionary;
		string key = "Satisfaction";
		DataOverlay.ColorCategoryHolder cats = new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("Not applicable", false, new Color[]
			{
				HUD.ThemeColors[1]
			}),
			new DataOverlay.ColorCategory("Bad", new Color[]
			{
				HUD.ThemeColors[2]
			}),
			new DataOverlay.ColorCategory("Good", new Color[]
			{
				HUD.ThemeColors[0]
			})
		});
		if (DataOverlay.<>f__mg$cache0 == null)
		{
			DataOverlay.<>f__mg$cache0 = new Func<Room, Color>(DataOverlay.RoomSatisfaction);
		}
		dictionary2.Add(key, new DataOverlay.Overlay(cats, DataOverlay.<>f__mg$cache0));
		Dictionary<string, DataOverlay.Overlay> dictionary3 = dictionary;
		string key2 = "Effectiveness";
		DataOverlay.ColorCategoryHolder cats2 = new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("Not applicable", false, new Color[]
			{
				HUD.ThemeColors[1]
			}),
			new DataOverlay.ColorCategory("Bad", new Color[]
			{
				HUD.ThemeColors[2]
			}),
			new DataOverlay.ColorCategory("Good", new Color[]
			{
				HUD.ThemeColors[0]
			})
		});
		if (DataOverlay.<>f__mg$cache1 == null)
		{
			DataOverlay.<>f__mg$cache1 = new Func<Room, Color>(DataOverlay.RoomEffectiveness);
		}
		dictionary3.Add(key2, new DataOverlay.Overlay(cats2, DataOverlay.<>f__mg$cache1));
		dictionary.Add("Lighting", new DataOverlay.Overlay(new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("Bad", new Color[]
			{
				HUD.ThemeColors[2]
			}),
			new DataOverlay.ColorCategory("Good", new Color[]
			{
				HUD.ThemeColors[0]
			})
		}), (Room room) => Color.Lerp(HUD.ThemeColors[2], HUD.ThemeColors[0], 1f - room.DarknessLevel)));
		Dictionary<string, DataOverlay.Overlay> dictionary4 = dictionary;
		string key3 = "Temperature";
		DataOverlay.ColorCategoryHolder cats3 = new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("Too cold", new Color[]
			{
				HUD.ThemeColors[1]
			}),
			new DataOverlay.ColorCategory("Just perfect", new Color[]
			{
				HUD.ThemeColors[0]
			}),
			new DataOverlay.ColorCategory("Too hot", new Color[]
			{
				HUD.ThemeColors[2]
			})
		});
		if (DataOverlay.<>f__mg$cache2 == null)
		{
			DataOverlay.<>f__mg$cache2 = new Func<Room, Color>(DataOverlay.RoomTemperature);
		}
		dictionary4.Add(key3, new DataOverlay.Overlay(cats3, DataOverlay.<>f__mg$cache2));
		dictionary.Add("Environment", new DataOverlay.Overlay(new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("Bad", new Color[]
			{
				HUD.ThemeColors[2]
			}),
			new DataOverlay.ColorCategory("Good", new Color[]
			{
				HUD.ThemeColors[0]
			})
		}), (Room room) => Color.Lerp(HUD.ThemeColors[2], HUD.ThemeColors[0], room.GetEnvironment())));
		dictionary.Add("Electricity", new DataOverlay.Overlay(new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("High consumption", new Color[]
			{
				HUD.ThemeColors[2]
			}),
			new DataOverlay.ColorCategory("No consumption", new Color[]
			{
				HUD.ThemeColors[0]
			})
		}), (Room room) => Color.Lerp(HUD.ThemeColors[2], HUD.ThemeColors[0], 1f - Mathf.Clamp01(room.GetFurnitures().Sum((Furniture x) => x.LastWattUse) / 100f))));
		dictionary.Add("Water", new DataOverlay.Overlay(new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("High consumption", new Color[]
			{
				HUD.ThemeColors[2]
			}),
			new DataOverlay.ColorCategory("No consumption", new Color[]
			{
				HUD.ThemeColors[0]
			})
		}), (Room room) => Color.Lerp(HUD.ThemeColors[2], HUD.ThemeColors[0], 1f - Mathf.Clamp01(room.GetFurnitures().Sum((Furniture x) => x.LastWaterUse) / 50f))));
		Dictionary<string, DataOverlay.Overlay> dictionary5 = dictionary;
		string key4 = "Maintenance";
		DataOverlay.ColorCategoryHolder cats4 = new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("Not applicable", false, new Color[]
			{
				HUD.ThemeColors[1]
			}),
			new DataOverlay.ColorCategory("Bad", new Color[]
			{
				HUD.ThemeColors[2]
			}),
			new DataOverlay.ColorCategory("Good", new Color[]
			{
				HUD.ThemeColors[0]
			})
		});
		if (DataOverlay.<>f__mg$cache3 == null)
		{
			DataOverlay.<>f__mg$cache3 = new Func<Room, Color>(DataOverlay.RoomMaintenance);
		}
		dictionary5.Add(key4, new DataOverlay.Overlay(cats4, DataOverlay.<>f__mg$cache3));
		Dictionary<string, DataOverlay.Overlay> dictionary6 = dictionary;
		string key5 = "Insulation";
		DataOverlay.ColorCategoryHolder cats5 = new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("Bad", new Color[]
			{
				HUD.ThemeColors[2]
			}),
			new DataOverlay.ColorCategory("Good", new Color[]
			{
				HUD.ThemeColors[1]
			}),
			new DataOverlay.ColorCategory("Great", new Color[]
			{
				HUD.ThemeColors[0]
			})
		});
		if (DataOverlay.<>f__mg$cache4 == null)
		{
			DataOverlay.<>f__mg$cache4 = new Func<Room, Color>(DataOverlay.Insulation);
		}
		dictionary6.Add(key5, new DataOverlay.Overlay(cats5, DataOverlay.<>f__mg$cache4));
		dictionary.Add("Room grouping", new DataOverlay.Overlay(new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("No group", false, new Color[]
			{
				HUD.ThemeColors[2]
			}),
			new DataOverlay.ColorCategory("Is grouped", false, new Color[]
			{
				HUD.ThemeColors[0]
			})
		}), (Room room) => (room.RoomGroup != null) ? HUD.ThemeColors[0] : HUD.ThemeColors[2]));
		Dictionary<string, DataOverlay.Overlay> dictionary7 = dictionary;
		string key6 = "Rent";
		DataOverlay.ColorCategoryHolder cats6 = new DataOverlay.ColorCategoryHolder(new DataOverlay.ColorCategory[]
		{
			new DataOverlay.ColorCategory("Player owned", false, new Color[]
			{
				Color.white
			}),
			new DataOverlay.ColorCategory("CantLease", false, new Color[]
			{
				Color.black
			}),
			new DataOverlay.ColorCategory("CanLease", new Color[]
			{
				HUD.ThemeColors[2],
				HUD.ThemeColors[0],
				HUD.ThemeColors[1]
			})
		});
		if (DataOverlay.<>f__mg$cache5 == null)
		{
			DataOverlay.<>f__mg$cache5 = new Func<Room, Color>(DataOverlay.Rent);
		}
		dictionary7.Add(key6, new DataOverlay.Overlay(cats6, DataOverlay.<>f__mg$cache5));
		DataOverlay.OverlayFuncs = dictionary;
		DataOverlay.Surroundings = new Color(0.7f, 0.7f, 0.7f);
	}

	public static Dictionary<string, DataOverlay.Overlay> OverlayFuncs;

	public static Color Surroundings;

	public static DataOverlay Instance;

	public string ActiveOverlayName;

	public DataOverlay.Overlay ActiveOverlay;

	public GUIWindow Window;

	public RectTransform TogglePanel;

	public ToggleGroup MainToggleGroup;

	public Toggle TogglePrefab;

	public GradientPanel grPanel;

	public Text DataDesc;

	[NonSerialized]
	public Dictionary<string, Toggle> DataToggles = new Dictionary<string, Toggle>();

	public float ActivateTime;

	[CompilerGenerated]
	private static Func<Room, Color> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Room, Color> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<Room, Color> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<Room, Color> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<Room, Color> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<Room, Color> <>f__mg$cache5;

	public class ColorCategory
	{
		public ColorCategory(string name, bool interp, params Color[] colors)
		{
			this.Name = name;
			this.Colors = colors;
			this.Interpolate = interp;
		}

		public ColorCategory(string name, params Color[] colors)
		{
			this.Name = name;
			this.Colors = colors;
			this.Interpolate = true;
		}

		public string Name;

		public Color[] Colors;

		public bool Interpolate;
	}

	public class ColorCategoryHolder
	{
		public ColorCategoryHolder(params DataOverlay.ColorCategory[] cats)
		{
			this.Categories = cats;
		}

		public DataOverlay.ColorCategory[] Categories;
	}

	public class Overlay
	{
		public Overlay(DataOverlay.ColorCategoryHolder cats, Func<Room, Color> func)
		{
			this.Func = func;
			this.Categories = cats;
		}

		public Func<Room, Color> Func;

		public DataOverlay.ColorCategoryHolder Categories;
	}
}
