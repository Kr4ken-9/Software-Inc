﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class JanitorAI : AI<Actor>
{
	public override void InitTree()
	{
		string name = "Spawn";
		if (JanitorAI.<>f__mg$cache0 == null)
		{
			JanitorAI.<>f__mg$cache0 = new Func<Actor, int>(JanitorAI.Spawn);
		}
		BehaviorNode<Actor> behaviorNode = new BehaviorNode<Actor>(name, JanitorAI.<>f__mg$cache0, true, -1);
		string name2 = "Loiter";
		if (JanitorAI.<>f__mg$cache1 == null)
		{
			JanitorAI.<>f__mg$cache1 = new Func<Actor, int>(JanitorAI.Loiter);
		}
		BehaviorNode<Actor> behaviorNode2 = new BehaviorNode<Actor>(name2, JanitorAI.<>f__mg$cache1, true, 1);
		string name3 = "FindRepair";
		if (JanitorAI.<>f__mg$cache2 == null)
		{
			JanitorAI.<>f__mg$cache2 = new Func<Actor, int>(JanitorAI.FindRepair);
		}
		BehaviorNode<Actor> behaviorNode3 = new BehaviorNode<Actor>(name3, JanitorAI.<>f__mg$cache2, false, -1);
		string name4 = "GoToRepair";
		if (JanitorAI.<>f__mg$cache3 == null)
		{
			JanitorAI.<>f__mg$cache3 = new Func<Actor, int>(JanitorAI.GoToRepair);
		}
		BehaviorNode<Actor> behaviorNode4 = new BehaviorNode<Actor>(name4, JanitorAI.<>f__mg$cache3, true, -1);
		string name5 = "Repair";
		if (JanitorAI.<>f__mg$cache4 == null)
		{
			JanitorAI.<>f__mg$cache4 = new Func<Actor, int>(JanitorAI.Repair);
		}
		BehaviorNode<Actor> behaviorNode5 = new BehaviorNode<Actor>(name5, JanitorAI.<>f__mg$cache4, true, -1);
		string name6 = "IsOff";
		if (JanitorAI.<>f__mg$cache5 == null)
		{
			JanitorAI.<>f__mg$cache5 = new Func<Actor, int>(JanitorAI.IsOff);
		}
		BehaviorNode<Actor> behaviorNode6 = new BehaviorNode<Actor>(name6, JanitorAI.<>f__mg$cache5, false, -1);
		string name7 = "GoHome";
		if (JanitorAI.<>f__mg$cache6 == null)
		{
			JanitorAI.<>f__mg$cache6 = new Func<Actor, int>(Actor.GoHomeState);
		}
		BehaviorNode<Actor> behaviorNode7 = new BehaviorNode<Actor>(name7, JanitorAI.<>f__mg$cache6, true, -1);
		string name8 = "Despawn";
		if (JanitorAI.<>f__mg$cache7 == null)
		{
			JanitorAI.<>f__mg$cache7 = new Func<Actor, int>(JanitorAI.Despawn);
		}
		BehaviorNode<Actor> behaviorNode8 = new BehaviorNode<Actor>(name8, JanitorAI.<>f__mg$cache7, true, -1);
		string name9 = "GoHomeBusStop";
		if (JanitorAI.<>f__mg$cache8 == null)
		{
			JanitorAI.<>f__mg$cache8 = new Func<Actor, int>(Actor.GoHomeBusStop);
		}
		BehaviorNode<Actor> behaviorNode9 = new BehaviorNode<Actor>(name9, JanitorAI.<>f__mg$cache8, true, -1);
		string name10 = "ShouldUseBus";
		if (JanitorAI.<>f__mg$cache9 == null)
		{
			JanitorAI.<>f__mg$cache9 = new Func<Actor, int>(Actor.ShouldUseBus);
		}
		BehaviorNode<Actor> behaviorNode10 = new BehaviorNode<Actor>(name10, JanitorAI.<>f__mg$cache9, true, -1);
		this.BehaviorNodes.Add("Spawn", behaviorNode);
		this.BehaviorNodes.Add("Loiter", behaviorNode2);
		this.BehaviorNodes.Add("FindRepair", behaviorNode3);
		this.BehaviorNodes.Add("GoToRepair", behaviorNode4);
		this.BehaviorNodes.Add("Repair", behaviorNode5);
		this.BehaviorNodes.Add("IsOff", behaviorNode6);
		this.BehaviorNodes.Add("GoHome", behaviorNode7);
		this.BehaviorNodes.Add("Despawn", behaviorNode8);
		this.BehaviorNodes.Add("GoHomeBusStop", behaviorNode9);
		this.BehaviorNodes.Add("ShouldUseBus", behaviorNode10);
		behaviorNode.Success = behaviorNode3;
		behaviorNode3.Success = behaviorNode4;
		behaviorNode3.Failure = behaviorNode2;
		behaviorNode4.Success = behaviorNode5;
		behaviorNode5.Success = behaviorNode6;
		behaviorNode6.Success = behaviorNode10;
		behaviorNode6.Failure = behaviorNode3;
		behaviorNode7.Success = behaviorNode8;
		behaviorNode7.Failure = behaviorNode9;
		behaviorNode8.Success = AI<Actor>.DummyNode;
		behaviorNode2.Success = behaviorNode6;
		behaviorNode10.Success = behaviorNode9;
		behaviorNode10.Failure = behaviorNode7;
		behaviorNode9.Success = behaviorNode8;
		behaviorNode9.Failure = behaviorNode2;
	}

	private static int Spawn(Actor self)
	{
		if (self.CurrentPath == null)
		{
			return 2;
		}
		return (!self.WalkPath()) ? 1 : 2;
	}

	private static int Loiter(Actor self)
	{
		if (self.GoHomeNow)
		{
			return 2;
		}
		return self.HandleLoiter(false);
	}

	private static int FindRepair(Actor self)
	{
		if (self.LastCheckWait > 0f)
		{
			return 0;
		}
		IEnumerable<Room> enumerable;
		if (self.HasAssignedRooms)
		{
			enumerable = self.GetAssignedRooms();
		}
		else if (self.currentRoom == GameSettings.Instance.sRoomManager.Outside)
		{
			enumerable = from x in GameSettings.Instance.sRoomManager.Rooms
			orderby (from y in x.GetFurnitures()
			where !y.ITFix
			select y.HasUpg ? y.upg.Quality : 1f).MinOrDefault(1f)
			select x;
		}
		else
		{
			enumerable = from x in GameSettings.Instance.sRoomManager.GetConnectedRooms(self.currentRoom)
			select x.Key;
		}
		IEnumerable<Room> enumerable2 = enumerable;
		bool flag = false;
		List<Furniture> list = new List<Furniture>();
		foreach (Room room in enumerable2)
		{
			if (!room.NavmeshRebuildStarted)
			{
				if (room.ToiletInUse())
				{
					flag = true;
				}
				else
				{
					List<Furniture> furnitures = room.GetFurnitures();
					for (int i = 0; i < furnitures.Count; i++)
					{
						Furniture furniture = furnitures[i];
						if (furniture != null && furniture.HasUpg && !furniture.ITFix && furniture.upg.Quality < 0.75f && furniture.TestAvailable(true, null))
						{
							list.Add(furniture);
						}
					}
					if (list.Count > 0)
					{
						foreach (Furniture furniture2 in from x in list
						orderby x.PathFailCount, x.upg.Quality
						select x)
						{
							InteractionPoint interactionPoint = furniture2.GetInteractionPoint(self, "Repair");
							if (interactionPoint != null)
							{
								if (self.PathToFurniture(interactionPoint, true))
								{
									self.FreeLoiterTable();
									return 2;
								}
								HUD.Instance.AddPopupMessage("FurnitureBlockedStaff".Loc(), "Exclamation", PopupManager.PopUpAction.GotoFurn, furniture2.DID, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.BlockedOff, 1);
							}
						}
					}
					list.Clear();
				}
			}
		}
		if (!flag && self.OnCall)
		{
			self.GoHomeNow = true;
		}
		self.LastCheckWait = 30f;
		return 0;
	}

	private static int GoToRepair(Actor self)
	{
		bool flag = self.WalkPath();
		if (flag)
		{
			self.TurnToFurniture();
		}
		return (!flag) ? 1 : 2;
	}

	private static int Repair(Actor self)
	{
		bool flag;
		if (self.UsingPoint != null)
		{
			self.anim.SetInteger("AnimControl", 11);
			self.anim.SetInteger("SubAnim", self.UsingPoint.subAnimation);
			flag = self.UsingPoint.Parent.upg.RepairMe();
			if (flag)
			{
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
		}
		else
		{
			flag = true;
		}
		return (!flag) ? 1 : 2;
	}

	private static int IsOff(Actor self)
	{
		if (self.GoHomeNow)
		{
			self.FreeLoiterTable();
			if (self.UsingPoint != null)
			{
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
			return 2;
		}
		return 0;
	}

	private static int Despawn(Actor self)
	{
		foreach (InteractionPoint interactionPoint in self.InQueue.Values)
		{
			interactionPoint.RemoveFromQueue(self);
		}
		self.InQueue.Clear();
		GameSettings.Instance.StaffSalaryDue += self.GetRealSalary() / (float)GameSettings.DaysPerMonth;
		if (self.OnCall)
		{
			UnityEngine.Object.Destroy(self.gameObject);
		}
		else
		{
			SDateTime sdateTime = SDateTime.Now();
			GameSettings.Instance.sActorManager.AddToAwaiting(self, new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), self.StaffOn - 1, sdateTime.Day + ((self.StaffOn <= TimeOfDay.Instance.Hour) ? 1 : 0), sdateTime.Month, sdateTime.Year), false, true);
			self.OnDespawn();
		}
		return 2;
	}

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache5;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache6;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache7;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache8;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache9;
}
