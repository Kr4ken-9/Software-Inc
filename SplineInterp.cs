﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class SplineInterp
{
	public static List<Vector2> SoftenCorners(List<SplineInterp.Point2D> Path)
	{
		List<Vector2> list = new List<Vector2>(Path.Count);
		list.Add(new Vector2((float)Path[0].X, (float)Path[0].Y));
		for (int i = 1; i < Path.Count - 1; i++)
		{
			int num = Path[i - 1].X - Path[i].X;
			int num2 = Path[i - 1].Y - Path[i].Y;
			int num3 = Path[i].X - Path[i + 1].X;
			int num4 = Path[i].Y - Path[i + 1].Y;
			if (num == num3 && num2 == num4)
			{
				list.Add(Path[i]);
			}
			else
			{
				Vector2 x = SplineInterp.Lerp(Path[i - 1], Path[i + 1]);
				list.Add(SplineInterp.Lerp(x, Path[i]));
			}
		}
		list.Add(new Vector2((float)Path[Path.Count - 1].X, (float)Path[Path.Count - 1].Y));
		return list;
	}

	private static Vector2 Lerp(Vector2 x, Vector2 y)
	{
		float x2 = (x.x + y.x) / 2f;
		float y2 = (x.y + y.y) / 2f;
		return new Vector2(x2, y2);
	}

	public static List<Vector2> Interp(List<Vector2> input, int steps, float tension)
	{
		List<Vector2> list = new List<Vector2>();
		for (int i = 0; i < input.Count - 1; i++)
		{
			for (int j = 0; j < steps; j++)
			{
				float num = (float)j / (float)steps;
				float num2 = num * num;
				float num3 = num2 * num;
				float d = 2f * num3 - 3f * num2 + 1f;
				float d2 = -2f * num3 + 3f * num2;
				float d3 = num3 - 2f * num2 + num;
				float d4 = num3 - num2;
				Vector2 vector = input[Mathf.Max(0, i - 1)];
				Vector2 vector2 = input[Mathf.Min(input.Count - 1, i + 1)];
				Vector2 a = new Vector2(tension * (vector2.x - vector.x), tension * (vector2.y - vector.y));
				vector = input[Mathf.Max(0, i)];
				vector2 = input[Mathf.Min(input.Count - 1, i + 2)];
				Vector2 a2 = new Vector2(tension * (vector2.x - vector.x), tension * (vector2.y - vector.y));
				list.Add(d * input[i] + d2 * input[i + 1] + d3 * a + d4 * a2);
			}
		}
		list.Add(input[input.Count - 1]);
		return list;
	}

	public static List<Vector3> Interp(List<Vector3> input, int steps, float tension)
	{
		List<Vector3> list = new List<Vector3>();
		for (int i = 0; i < input.Count - 1; i++)
		{
			for (int j = 0; j < steps; j++)
			{
				float num = (float)j / (float)steps;
				float num2 = num * num;
				float num3 = num2 * num;
				float d = 2f * num3 - 3f * num2 + 1f;
				float d2 = -2f * num3 + 3f * num2;
				float d3 = num3 - 2f * num2 + num;
				float d4 = num3 - num2;
				Vector3 vector = input[Mathf.Max(0, i - 1)];
				Vector3 vector2 = input[Mathf.Min(input.Count - 1, i + 1)];
				Vector3 a = new Vector3(tension * (vector2.x - vector.x), tension * (vector2.y - vector.y), tension * (vector2.z - vector.z));
				vector = input[Mathf.Max(0, i)];
				vector2 = input[Mathf.Min(input.Count - 1, i + 2)];
				Vector3 a2 = new Vector3(tension * (vector2.x - vector.x), tension * (vector2.y - vector.y), tension * (vector2.z - vector.z));
				list.Add(d * input[i] + d2 * input[i + 1] + d3 * a + d4 * a2);
			}
		}
		list.Add(input[input.Count - 1]);
		return list;
	}

	public struct Point2D
	{
		public Point2D(int x, int y)
		{
			this.X = x;
			this.Y = y;
		}

		public static implicit operator Vector2(SplineInterp.Point2D p)
		{
			return new Vector2((float)p.X, (float)p.Y);
		}

		public static implicit operator SplineInterp.Point2D(Vector2 v)
		{
			return new SplineInterp.Point2D(Mathf.FloorToInt(v.x), Mathf.FloorToInt(v.y));
		}

		public static bool operator ==(SplineInterp.Point2D p1, SplineInterp.Point2D p2)
		{
			return p1.X == p2.X && p1.Y == p2.Y;
		}

		public static bool operator !=(SplineInterp.Point2D p1, SplineInterp.Point2D p2)
		{
			return p1.X != p2.X || p1.Y != p2.Y;
		}

		public override int GetHashCode()
		{
			return 32768 * this.X + this.Y;
		}

		public bool Equals(SplineInterp.Point2D p)
		{
			return this == p;
		}

		public override bool Equals(object obj)
		{
			return obj.GetHashCode() == this.GetHashCode();
		}

		public readonly int X;

		public readonly int Y;
	}
}
