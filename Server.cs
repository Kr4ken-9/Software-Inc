﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Server : Writeable, IServerHost
{
	public string TServerName
	{
		get
		{
			return this._serverName;
		}
		set
		{
			this._serverName = value;
			this.TextObj.text = this._serverName;
			if (this.Rep == this)
			{
				GameSettings.Instance.AddServer(this);
			}
		}
	}

	public string ServerName
	{
		get
		{
			return this.TServerName;
		}
		set
		{
			this.TServerName = value;
		}
	}

	public Server Rep
	{
		get
		{
			if (this._rep == null)
			{
				this._rep = this;
			}
			return this._rep;
		}
		set
		{
			this._rep = (value ?? this);
		}
	}

	public List<IServerItem> Items
	{
		get
		{
			return this.items;
		}
		set
		{
		}
	}

	public Furniture furn
	{
		get
		{
			if (this._furn == null)
			{
				this._furn = base.GetComponent<Furniture>();
			}
			return this._furn;
		}
	}

	public float PowerSum
	{
		get
		{
			return this._powerSum;
		}
		set
		{
			this._powerSum = value;
		}
	}

	public int Count
	{
		get
		{
			return this.Children.Count + 1;
		}
		set
		{
		}
	}

	public float Available
	{
		get
		{
			return this._available;
		}
		set
		{
			this._available = value;
		}
	}

	private Upgradable Upg
	{
		get
		{
			return (!(this.furn == null)) ? this.furn.upg : null;
		}
	}

	private void Start()
	{
		if (this.furn == null || this.furn.isTemporary)
		{
			return;
		}
		TutorialSystem.Instance.StartTutorial("Server", false);
		this.PowerSum = this.Power;
		base.InitWritable();
		this.MainCam = Camera.main;
		if (!this.Deserialized)
		{
			if (!this.PreWired)
			{
				this.Rep = this;
				this.ServerName = GameSettings.Instance.GenerateServerName();
			}
			Vector3 vector = Utilities.HSVToRGB(UnityEngine.Random.Range(0f, 360f), 0.75f, 1f);
			this.color = new Color(vector.x, vector.y, vector.z, 1f);
			if (!this.PreWired)
			{
				this.Wire.color = this.color;
			}
		}
		Server.CalculatePowerNow.Add(this.Rep);
	}

	public void CalculatePowerN()
	{
		if (this.Rep == null)
		{
			this.Rep = this;
		}
		if (this.Rep != this)
		{
			this.Rep.CalculatePowerN();
		}
		else
		{
			this.Children.RemoveAll((Server x) => x == null);
			float powerSum = this.PowerSum;
			bool flag;
			if (!this.furn.HasUpg || !this.Upg.Broken)
			{
				flag = this.Children.Any((Server x) => x.furn.HasUpg && x.Upg.Broken);
			}
			else
			{
				flag = true;
			}
			bool flag2 = flag;
			if (flag2)
			{
				this.PowerSum = 0f;
			}
			else
			{
				this.PowerSum = this.Children.Sum((Server x) => x.Power) + this.Rep.Power;
			}
			if (powerSum != this.PowerSum)
			{
				this.Children.ForEach(delegate(Server x)
				{
					x.PowerSum = this.PowerSum;
				});
			}
		}
	}

	public override string ToString()
	{
		return (!(this.Rep == null)) ? this.Rep.ServerName : this.ServerName;
	}

	private void OnDestroy()
	{
		if (GameSettings.IsQuitting)
		{
			return;
		}
		Furniture component = base.GetComponent<Furniture>();
		if (component == null || !component.isTemporary)
		{
			this.CancelWire();
			if (GameSettings.Instance != null)
			{
				GameSettings.Instance.RemoveServer(this);
			}
		}
	}

	private void Update()
	{
		if (this.furn.isTemporary)
		{
			return;
		}
		if (GameSettings.Instance.WireMode && (this.furn.Parent.Floor <= GameSettings.Instance.ActiveFloor || this.Selected))
		{
			this.Wire.gameObject.SetActive(this.Rep != this || this.Selected);
			this.TextObj.gameObject.SetActive(this.Rep == this);
			if (this.Rep == this)
			{
				this.TextObj.transform.rotation = Quaternion.LookRotation(new Vector3(base.transform.position.x - this.MainCam.transform.position.x, 0f, base.transform.position.z - this.MainCam.transform.position.z));
			}
			if (this.Selected)
			{
				this.Wire.color = this.color;
				Ray ray = this.MainCam.ScreenPointToRay(Input.mousePosition);
				Plane plane = new Plane(Vector3.up, Vector3.up * (float)(GameSettings.Instance.ActiveFloor * 2 + 1));
				float distance = 0f;
				plane.Raycast(ray, out distance);
				Vector3 point = ray.GetPoint(distance);
				this.Wire.SetPos(new Vector3(this.furn.transform.position.x, (float)(this.furn.Parent.Floor * 2), this.furn.transform.position.z), new Vector3(point.x, (float)(GameSettings.Instance.ActiveFloor * 2), point.z));
			}
		}
		else
		{
			this.Wire.gameObject.SetActive(false);
			this.TextObj.gameObject.SetActive(false);
		}
	}

	public void CancelWire()
	{
		if (GameSettings.Instance == null || HUD.Instance == null)
		{
			return;
		}
		this.WiredTo = null;
		if (this.Rep != this)
		{
			this.Rep.Children.Remove(this);
			Server.FixWiringNow.Add(this.Rep);
			Server.CalculatePowerNow.Add(this.Rep);
		}
		if (this.Children.Count((Server x) => x != null && x.gameObject != null) > 0)
		{
			List<Server> list = (from x in this.Children
			where x != null && x.gameObject != null
			select x).ToList<Server>();
			list[0].ServerName = this.ServerName;
			GameSettings.Instance.RemoveServer(this);
			list[0].CancelWire();
			for (int i = 1; i < list.Count; i++)
			{
				list[i].CancelWire();
				list[i].WireTo(list[0], false);
			}
			for (int j = 0; j < this.Items.Count; j++)
			{
				list[0].Items.Add(this.Items[j]);
			}
			this.Items.Clear();
			this.Children.Clear();
			Server.CalculatePowerNow.Add(list[0]);
			HUD.Instance.serverWindow.UpdateServerList();
		}
		else if (this.Items.Count > 0)
		{
			for (int k = this.Items.Count - 1; k >= 0; k--)
			{
				GameSettings.Instance.RegisterWithServer(null, this.Items[k], true);
			}
			this.Items.Clear();
		}
		this.Rep = this;
		if (this.ServerName != null)
		{
			GameSettings.Instance.AddServer(this);
		}
		this.Wire.color = this.color;
		Server.CalculatePowerNow.Add(this.Rep);
	}

	public void ReWire()
	{
		if (this.WiredTo != null)
		{
			this.Wire.SetPos(new Vector3(this.furn.OriginalPosition.x, (float)(this.furn.Parent.Floor * 2), this.furn.OriginalPosition.z), new Vector3(this.WiredTo.furn.OriginalPosition.x, (float)(this.WiredTo.furn.Parent.Floor * 2), this.WiredTo.furn.OriginalPosition.z));
		}
		if (this.Rep != null)
		{
			this.Wire.color = this.Rep.color;
		}
	}

	public void ReWireAll()
	{
		if (this.Rep != null)
		{
			this.Rep.ReWire();
			foreach (Server server in this.Rep.Children)
			{
				server.ReWire();
			}
		}
	}

	public void FixWiringN(HashSet<Server> visited)
	{
		this.WiredTo = null;
		if (this.Rep != this)
		{
			HashSet<Server> hashSet = this.Rep.Children.ToHashSet<Server>();
			hashSet.Add(this.Rep);
			hashSet.Remove(this);
			foreach (Server server in from x in hashSet
			orderby (x.transform.position - base.transform.position).sqrMagnitude
			select x)
			{
				if (server.WiredTo != this)
				{
					this.WiredTo = server;
					visited.Clear();
					if (!this.WiredTo.CheckLoop(this, visited))
					{
						break;
					}
				}
			}
		}
		this.ReWire();
	}

	private bool CheckLoop(Server initial, HashSet<Server> visited)
	{
		if (visited.Contains(this))
		{
			this.WiredTo = null;
			this.ReWire();
			return false;
		}
		visited.Add(this);
		if (this.WiredTo == this)
		{
			this.WiredTo = null;
			this.ReWire();
			return false;
		}
		return this.WiredTo == initial || (!(this.WiredTo == null) && this.WiredTo.CheckLoop(initial, visited));
	}

	public void WireTo(Server server, bool DeferPowerCalc = false)
	{
		if (server.Rep == this)
		{
			this.Wire.color = this.Rep.color;
			return;
		}
		if (this.Rep == server || this.Rep == server.Rep)
		{
			Server.FixWiringNow.Add(this.Rep);
			return;
		}
		if (this.Children.Count == 0 && this.Items.Count > 0)
		{
			for (int i = 0; i < this.items.Count; i++)
			{
				GameSettings.Instance.RegisterWithServer(server.Rep.ServerName, this.Items[i], true);
			}
			this.Items.Clear();
		}
		this.CancelWire();
		GameSettings.Instance.RemoveServer(this);
		this.Rep = server.Rep;
		if (!this.Rep.Children.Contains(this))
		{
			this.Rep.Children.Add(this);
		}
		Server.FixWiringNow.Add(this.Rep);
		if (!DeferPowerCalc)
		{
			Server.CalculatePowerNow.Add(this.Rep);
		}
	}

	public override string WriteName()
	{
		return "Server";
	}

	public override void PostDeserialize()
	{
		TimeProbe.BeginTime("Server init time:");
		this.Rep = ((this.tempRep != 0u) ? ((Server)base.GetDeserializedObject(this.tempRep)) : null);
		this.WiredTo = ((this.tempWiredTo != 0u) ? ((Server)base.GetDeserializedObject(this.tempWiredTo)) : null);
		this.ReWire();
		this.Children = (from x in this.tempChildren
		select (Server)base.GetDeserializedObject(x)).ToHashSet<Server>();
		if (this.serializedFallback != null)
		{
			Server server = GameSettings.Instance.GetServer(this.serializedFallback);
			if (server != null)
			{
				this.Fallback = server;
			}
		}
		if (this.Rep == this)
		{
			GameSettings.Instance.ValidateServer(this);
			if (GameSettings.Instance.GetServer(this.ServerName) != this)
			{
				GameSettings.Instance.AddServer(this);
			}
		}
		TimeProbe.EndTime("Server init time:");
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		this.color = dictionary.Get<SVector3>("Color", new SVector3(1f, 1f, 1f, 1f)).ToColor();
		this._serverName = dictionary.Get<string>("Name", "Server X");
		this.TextObj.text = this._serverName;
		this.tempRep = dictionary.Get<uint>("Rep", this.DID);
		this.tempWiredTo = dictionary.Get<uint>("WiredTo", 0u);
		this.tempChildren = dictionary.Get<uint[]>("Children", new uint[0]);
		this.serializedFallback = dictionary.Get<string>("Fallback", null);
		return this;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["Rep"] = this.Rep.DID;
		dictionary["WiredTo"] = ((!(this.WiredTo == null)) ? this.WiredTo.DID : 0u);
		dictionary["Color"] = this.color;
		dictionary["Name"] = this.ServerName;
		dictionary["Children"] = (from x in this.Children
		select x.DID).ToArray<uint>();
		dictionary["Fallback"] = ((this.Fallback != null) ? this.Fallback.ServerName : null);
	}

	public IServerHost Fallback { get; set; }

	public static HashSet<Server> FixWiringNow = new HashSet<Server>();

	public static HashSet<Server> CalculatePowerNow = new HashSet<Server>();

	public static float ISPCost = 50f;

	private string _serverName;

	private Server _rep;

	public Server WiredTo;

	public HashSet<Server> Children = new HashSet<Server>();

	public List<IServerItem> items = new List<IServerItem>();

	public WireScript Wire;

	private Camera MainCam;

	[NonSerialized]
	private Furniture _furn;

	public TextMesh TextObj;

	public Color color;

	public bool Selected;

	public float Power = 1f;

	public float _powerSum = 1f;

	public float _available = 1f;

	[NonSerialized]
	public bool PreWired;

	private uint tempRep;

	private uint tempWiredTo;

	private uint[] tempChildren;

	private string serializedFallback;
}
