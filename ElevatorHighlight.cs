﻿using System;
using UnityEngine;

public class ElevatorHighlight : MonoBehaviour
{
	private void Start()
	{
		base.transform.position = new Vector3(base.transform.position.x, (float)GameSettings.MaxFloor, base.transform.position.z);
		base.transform.localScale = new Vector3(0.5f, (float)(GameSettings.MaxFloor * 2 + 4), 0.5f);
	}

	public void UpdateBeam(Furniture Parent, bool force = false)
	{
		if (GameSettings.Instance.ActiveFloor != this.lastFloor || force)
		{
			if (Parent.Parent.Floor == GameSettings.Instance.ActiveFloor)
			{
				bool flag = Parent.GetConnectedElevator(true) != null || Parent.GetConnectedElevator(false) != null;
				this.rend.sharedMaterial = ((!flag) ? this.Bad : this.Good);
				return;
			}
			Furniture furniture = Parent;
			while (furniture != null && furniture.Parent != null && furniture.Parent.Floor != GameSettings.Instance.ActiveFloor)
			{
				furniture = furniture.GetConnectedElevator(furniture.Parent.Floor < GameSettings.Instance.ActiveFloor);
			}
			this.rend.sharedMaterial = ((!(furniture == null)) ? this.Good : this.Bad);
			this.lastFloor = GameSettings.Instance.ActiveFloor;
		}
	}

	public Material Good;

	public Material Bad;

	public Renderer rend;

	private int lastFloor = -5;
}
