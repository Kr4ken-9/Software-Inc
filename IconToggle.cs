﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class IconToggle : MonoBehaviour
{
	private void Awake()
	{
		this.toggle = base.GetComponent<Toggle>();
		this.OnValueChange();
	}

	public void OnValueChange()
	{
		this.TargetGraphic.color = ((!this.toggle.isOn) ? this.Off : this.On);
	}

	public Image TargetGraphic;

	public Color Off;

	public Color On;

	private Toggle toggle;
}
