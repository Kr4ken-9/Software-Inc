﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LoanWindow : MonoBehaviour
{
	public void UpdateLoans()
	{
		this.LoanList.Items = GameSettings.Instance.Loans.Cast<object>().ToList<object>();
	}

	public void Show()
	{
		if (this.Window.Shown)
		{
			this.Window.Close();
			return;
		}
		this.AmountSlider.value = 0f;
		this.PeriodSlider.value = 1f;
		this.UpdateLoans();
		this.UpdateText();
		this.Window.Show();
	}

	public void LoanClick()
	{
		if (this.AmountSlider.value > 0f)
		{
			LoanWindow.TakeLoan((int)this.PeriodSlider.value, (int)this.AmountSlider.value);
			this.PeriodSlider.value = 1f;
			this.AmountSlider.value = 0f;
			this.UpdateText();
		}
	}

	private void Update()
	{
		int num = this.MaxLoan();
		this.AmountSlider.maxValue = (float)num;
		this.AmountSlider.interactable = (num > 0);
	}

	public static void TakeLoan(int months, int amount)
	{
		if (amount == 0)
		{
			return;
		}
		float num = (float)amount * LoanWindow.factor;
		float num2 = LoanWindow.CalculateInterest(months, amount);
		float value = num2 * num + num / (float)months;
		GameSettings.Instance.Loans.Add(new KeyValuePair<int, float>(months, value));
		GameSettings.Instance.MyCompany.MakeTransaction(num, Company.TransactionCategory.Loan, null);
		if (HUD.Instance != null && HUD.Instance.loanWindow != null)
		{
			HUD.Instance.loanWindow.UpdateLoans();
		}
	}

	public int MaxLoan()
	{
		int b = LoanWindow.maxLoan - Mathf.CeilToInt(GameSettings.Instance.Loans.Sum((KeyValuePair<int, float> x) => (float)x.Key * x.Value / LoanWindow.factor));
		return Mathf.Max(0, b);
	}

	public static float CalculateInterest(int months, int amount)
	{
		return 0.01f + (float)months / 24f / 100f + (1f - (float)amount / 100f) / 40f;
	}

	public void UpdateText()
	{
		float num = this.AmountSlider.value * LoanWindow.factor;
		this.AmountText.text = num.Currency(true);
		this.PeriodText.text = "MonthPostfix".LocPlural((int)this.PeriodSlider.value);
		float num2 = LoanWindow.CalculateInterest((int)this.PeriodSlider.value, (int)this.AmountSlider.value);
		float num3 = num2 * num + num / this.PeriodSlider.value;
		float x = num3 * this.PeriodSlider.value;
		this.SummaryText.text = string.Format(string.Concat(new string[]
		{
			"Interest".Loc(),
			": {0}% - ",
			"Monthly".Loc(),
			": {2}\n",
			"Total".Loc(),
			": {1}"
		}), (num2 * 100f).ToString("F"), x.Currency(true), num3.Currency(true));
	}

	public static float factor = 1000f;

	public static int maxLoan = 100;

	public GUIWindow Window;

	public GUIListView LoanList;

	public Slider AmountSlider;

	public Slider PeriodSlider;

	public Text AmountText;

	public Text PeriodText;

	public Text SummaryText;
}
