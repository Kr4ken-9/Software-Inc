﻿using System;
using UnityEngine;

public interface IHasSpeed
{
	GameObject GetGameObject();

	float GetSpeed(float angle);
}
