﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StockButton : MonoBehaviour
{
	private void Update()
	{
		if (this.CompanyID > 0u)
		{
			Company company = GameSettings.Instance.simulation.GetCompany(this.CompanyID);
			if (company != null)
			{
				Stock stock = company.Stocks[this.Stock];
				if (stock == null)
				{
					this.ButtonImage.color = Color.white;
					this.ChangeText.enabled = false;
					this.CompanyText.text = "Stock".Loc();
					this.ButtonText.text = company.CompanyStockPrice.Currency(true);
				}
				else
				{
					this.ButtonImage.color = HUD.ThemeColors[this.GetColorIdx(company, stock.Owner) % HUD.ThemeColors.Length];
					this.ChangeText.enabled = true;
					this.ChangeText.text = ((stock.Change <= 0f) ? string.Empty : "+") + (stock.Change * 100f).ToString("F0") + "%";
					this.CompanyText.text = stock.OwnerCompany.Name;
					this.ButtonText.text = stock.CurrentWorth.Currency(true);
				}
			}
		}
	}

	private int GetColorIdx(Company cmp, uint stockCmp)
	{
		int num = 0;
		for (int i = 0; i < cmp.Stocks.Length; i++)
		{
			if (cmp.Stocks[i] != null)
			{
				if (cmp.Stocks[i].Owner == stockCmp)
				{
					return num;
				}
				num++;
			}
		}
		return num;
	}

	public void Action()
	{
		if (this.CompanyID > 0u)
		{
			float num = GameSettings.Instance.Loans.SumSafe((KeyValuePair<int, float> x) => (float)x.Key * x.Value);
			Company cmp = GameSettings.Instance.simulation.GetCompany(this.CompanyID);
			if (cmp != null)
			{
				Stock stock = cmp.Stocks[this.Stock];
				if (stock == null)
				{
					if (GameSettings.Instance.MyCompany.CanMakeTransaction(-cmp.CompanyStockPrice - num))
					{
						cmp.BuyStock(GameSettings.Instance.MyCompany, this.Stock, cmp.CompanyStockPrice);
					}
					else if (GameSettings.Instance.MyCompany.CanMakeTransaction(-cmp.CompanyStockPrice))
					{
						WindowManager.Instance.ShowMessageBox("StockLoanError".Loc(), true, DialogWindow.DialogType.Error);
					}
				}
				else if (this.Stock == 0)
				{
					if (Utilities.GetMonthsFlat(cmp.Founded, SDateTime.Now()) <= 12)
					{
						WindowManager.Instance.ShowMessageBox("CompanyBuyoutRestriction".Loc(), false, DialogWindow.DialogType.Error);
					}
					else if (GameSettings.Instance.MyCompany.CanMakeTransaction(-stock.CurrentWorth - num))
					{
						if (cmp.CanBuyOut(GameSettings.Instance.MyCompany))
						{
							WindowManager.Instance.ShowMessageBox("SubsidiaryPrompt".Loc(new object[]
							{
								cmp.Name
							}), true, DialogWindow.DialogType.Question, new KeyValuePair<string, Action>[]
							{
								new KeyValuePair<string, Action>("Takeover", delegate
								{
									cmp.BuyStock(GameSettings.Instance.MyCompany, 0, stock.CurrentWorth);
								}),
								new KeyValuePair<string, Action>("Subsidiary", delegate
								{
									int num2 = Mathf.FloorToInt(GameSettings.Instance.MyCompany.BusinessReputation * (float)Company.BusinessRepStars);
									int num3 = GameSettings.Instance.simulation.GetAllCompanies().Count((Company x) => x.IsPlayerOwned());
									if (num3 >= num2)
									{
										WindowManager.Instance.ShowMessageBox("SubsidiaryRepError".Loc(new object[]
										{
											num2
										}), true, DialogWindow.DialogType.Error);
									}
									else
									{
										WindowManager.SpawnInputDialog("SubsidiaryDeposit".Loc(), "Subsidiary".Loc(), (GameSettings.Instance.MyCompany.Money * 0.1f).CurrencyMul().ToString("N0"), delegate(string x)
										{
											try
											{
												float num4 = ((float)Convert.ToDouble(x)).FromCurrency();
												if (num4 < 1f)
												{
													WindowManager.Instance.ShowMessageBox("SubsidiaryDepositError".Loc(), true, DialogWindow.DialogType.Error);
												}
												else if (stock.CurrentWorth + num4 > GameSettings.Instance.MyCompany.Money - 100f)
												{
													WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), true, DialogWindow.DialogType.Error);
												}
												else
												{
													GameSettings.Instance.MyCompany.MakeTransaction(-stock.CurrentWorth - num4, Company.TransactionCategory.Stocks, null);
													float money = cmp.Money;
													cmp.MakeTransaction(num4, Company.TransactionCategory.Intercompany, null);
													cmp.MakeTransaction(-money, Company.TransactionCategory.Dividends, null);
													cmp.MakeSubsidiary(GameSettings.Instance.MyCompany);
													CompanyDetailWindow companyDetailWindow = HUD.Instance.companyWindow.GetCompanyDetailWindow(cmp);
													if (companyDetailWindow != null)
													{
														companyDetailWindow.UpdateStocks();
														companyDetailWindow.UpdateDistributionButton();
													}
												}
											}
											catch (Exception)
											{
											}
										}, null);
									}
								})
							});
						}
						else
						{
							WindowManager.Instance.ShowMessageBox("StockBuyOutError".Loc(), false, DialogWindow.DialogType.Error);
						}
					}
				}
				else if (stock.OwnerCompany.Player)
				{
					KeyValuePair<Company, float> buyer = GameSettings.Instance.simulation.FindBuyer(0u, cmp.ID, stock.CurrentWorth);
					if (buyer.Key != null)
					{
						if (buyer.Value < stock.CurrentWorth)
						{
							WindowManager.Instance.ShowMessageBox("StockLowBid".Loc(new object[]
							{
								buyer.Key.Name,
								buyer.Value.Currency(true),
								stock.InitialPrice.Currency(true),
								stock.CurrentWorth.Currency(true)
							}), true, DialogWindow.DialogType.Question, delegate
							{
								cmp.TradeStock(buyer.Key, stock, buyer.Value);
							}, null, null);
						}
						else
						{
							cmp.TradeStock(buyer.Key, stock, buyer.Value);
						}
					}
					else
					{
						WindowManager.Instance.ShowMessageBox("NoStockInterest".Loc(), false, DialogWindow.DialogType.Information);
					}
				}
				else if (GameSettings.Instance.MyCompany.CanMakeTransaction(-stock.CurrentWorth - num))
				{
					cmp.TradeStock(GameSettings.Instance.MyCompany, stock, stock.CurrentWorth);
				}
				else if (GameSettings.Instance.MyCompany.CanMakeTransaction(-stock.CurrentWorth))
				{
					WindowManager.Instance.ShowMessageBox("StockLoanError".Loc(), true, DialogWindow.DialogType.Error);
				}
			}
		}
	}

	public uint CompanyID;

	public int Stock;

	public Button ActionButton;

	public Image ButtonImage;

	public Text ButtonText;

	public Text CompanyText;

	public Text ChangeText;
}
