﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ReputationTopBar : MonoBehaviour, IPointerEnterHandler, IEventSystemHandler
{
	private void Update()
	{
		if (this._isOpen)
		{
			this.UpdateBars();
			int num = Mathf.Clamp(this._bars.Count((PosNegBar x) => x.gameObject.activeSelf) * 24, 0, Screen.height - 128);
			this.DropdownPanel.sizeDelta = new Vector2(this.DropdownPanel.sizeDelta.x, Mathf.Lerp(this.DropdownPanel.sizeDelta.y, (float)num, Time.deltaTime * 20f));
			if (!RectTransformUtility.RectangleContainsScreenPoint(this.DropdownPanel, Input.mousePosition, null) && !RectTransformUtility.RectangleContainsScreenPoint(this.SelfRect, Input.mousePosition, null))
			{
				this._isOpen = false;
			}
		}
		else
		{
			this.DropdownPanel.sizeDelta = new Vector2(this.DropdownPanel.sizeDelta.x, Mathf.Lerp(this.DropdownPanel.sizeDelta.y, 0f, Time.deltaTime * 20f));
		}
	}

	public void UpdateBars()
	{
		if (GameSettings.Instance == null)
		{
			return;
		}
		int num = 0;
		foreach (KeyValuePair<string, Company.RepEffectItem> keyValuePair in from x in GameSettings.Instance.MyCompany.RepEffects
		where x.Value.IsRelevant()
		orderby x.Value.GetValue(true) + x.Value.GetValue(false) descending
		select x)
		{
			if (this._bars.Count <= num)
			{
				this._bars.Add(this.MakeBar());
			}
			PosNegBar posNegBar = this._bars[num];
			posNegBar.gameObject.SetActive(true);
			posNegBar.GetComponentInChildren<Text>().text = keyValuePair.Key.Loc();
			posNegBar.SetValues(this.ConvertValue(keyValuePair.Value.GetValue(true)), this.ConvertValue(keyValuePair.Value.GetValue(false)));
			num++;
		}
		for (int i = num; i < this._bars.Count; i++)
		{
			this._bars[i].gameObject.SetActive(false);
		}
	}

	private float ConvertValue(float val)
	{
		return (val != 0f) ? (Mathf.Sqrt(val.MapRange(0f, 0.5f, 0.01f, 1f, true)) * 8f) : 0f;
	}

	private PosNegBar MakeBar()
	{
		PosNegBar posNegBar = UnityEngine.Object.Instantiate<PosNegBar>(this.RepItemPrefab);
		posNegBar.transform.SetParent(this.ContentRect, false);
		return posNegBar;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		this._isOpen = true;
	}

	public PosNegBar RepItemPrefab;

	public RectTransform ContentRect;

	public RectTransform DropdownPanel;

	public RectTransform SelfRect;

	private bool _isOpen;

	private readonly List<PosNegBar> _bars = new List<PosNegBar>();
}
