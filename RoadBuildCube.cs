﻿using System;
using UnityEngine;

public class RoadBuildCube : MonoBehaviour
{
	private void Start()
	{
		if (RoadBuildCube.Instance != null)
		{
			UnityEngine.Object.Destroy(RoadBuildCube.Instance.gameObject);
		}
		RoadBuildCube.Instance = this;
		base.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		if (RoadBuildCube.Instance == this)
		{
			RoadBuildCube.Instance = null;
		}
	}

	private void OnEnable()
	{
		if (HUD.Instance != null)
		{
			BuildController.Instance.ClearBuild(false, true, false, false);
			this.Arrow.SetActive(false);
			if (GameSettings.Instance.ActiveFloor < 0)
			{
				GameSettings.Instance.ActiveFloor = 0;
				Furniture.UpdateEdgeDetection();
				GameSettings.Instance.sRoomManager.ChangeFloor();
			}
		}
	}

	private void OnDisable()
	{
		if (CostDisplay.Instance != null)
		{
			CostDisplay.Instance.Hide();
		}
	}

	public void ShowArrow(float rot)
	{
		this.Arrow.SetActive(true);
		this.Arrow.transform.rotation = Quaternion.Euler(0f, rot, 0f);
	}

	private void Update()
	{
		SelectorController.CanClick = false;
		if (GameSettings.FreezeGame)
		{
			CostDisplay.Instance.Hide();
			return;
		}
		if (Input.GetMouseButtonUp(1))
		{
			base.gameObject.SetActive(false);
			return;
		}
		Ray ray = CameraScript.Instance.mainCam.ScreenPointToRay(Input.mousePosition);
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		float distance = 0f;
		plane.Raycast(ray, out distance);
		Vector3 point = ray.GetPoint(distance);
		this.x = (int)(point.x / RoadManager.Instance.RoadSize);
		this.y = (int)(point.z / RoadManager.Instance.RoadSize);
		this.x = Mathf.Clamp(this.x, 0, RoadManager.Instance.RoadMap.GetLength(0) - 1);
		this.y = Mathf.Clamp(this.y, 0, RoadManager.Instance.RoadMap.GetLength(1) - 1);
		if (this.x != this.prevX || this.y != this.prevY)
		{
			if (Input.GetMouseButton(0))
			{
				float num = 1f;
				Vector2 vector = new Vector2((float)(this.x - this.lastX), (float)(this.y - this.lastY));
				float pitch = num + Mathf.Min(vector.magnitude / 8f, 1f);
				UISoundFX.PlaySFX("Tick", pitch, 0f);
			}
			else
			{
				UISoundFX.PlaySFX("Tick", -1f, 0f);
			}
		}
		this.prevX = this.x;
		this.prevY = this.y;
		if (!GUICheck.OverGUI)
		{
			if (Input.GetMouseButtonDown(0))
			{
				this.lastX = this.x;
				this.lastY = this.y;
				return;
			}
			if (Input.GetMouseButtonUp(0))
			{
				Rect buildRect = this.GetBuildRect();
				float num2 = 0f;
				int num3 = (int)buildRect.xMin;
				while ((float)num3 < buildRect.xMax)
				{
					int num4 = (int)buildRect.yMin;
					while ((float)num4 < buildRect.yMax)
					{
						num2 += ((!this.GetValid(num3, num4)) ? 0f : RoadBuildCube.RoadCost);
						num4++;
					}
					num3++;
				}
				if (num2 > 0f && GameSettings.Instance.MyCompany.CanMakeTransaction(-num2))
				{
					GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
					{
						new UndoObject.UndoAction((int)buildRect.x, (int)buildRect.y, (int)buildRect.width, (int)buildRect.height, num2)
					});
					if (buildRect.width > 1f || buildRect.height > 1f)
					{
						RoadManager.Instance.PlaceRoad(buildRect, this.Type);
					}
					else
					{
						RoadManager.Instance.PlaceRoad((int)buildRect.x, (int)buildRect.y, this.Type);
					}
					GameSettings.Instance.sRoomManager.Outside.DirtyNavMesh = true;
					GameSettings.Instance.MyCompany.MakeTransaction(-num2, Company.TransactionCategory.Construction, "Road");
					UISoundFX.PlaySFX("PlaceRoom", -1f, 0f);
					UISoundFX.PlaySFX("Kaching", -1f, 0f);
					CostDisplay.Instance.FloatAway();
				}
				else
				{
					UISoundFX.PlaySFX("BuildError", -1f, 0f);
				}
				return;
			}
			if (Input.GetMouseButton(0))
			{
				Rect buildRect2 = this.GetBuildRect();
				float num5 = 0f;
				int num6 = (int)buildRect2.xMin;
				while ((float)num6 < buildRect2.xMax)
				{
					int num7 = (int)buildRect2.yMin;
					while ((float)num7 < buildRect2.yMax)
					{
						num5 += ((!this.GetValid(num6, num7)) ? 0f : RoadBuildCube.RoadCost);
						num7++;
					}
					num6++;
				}
				this.rend.sharedMaterial = ((num5 <= 0f || !GameSettings.Instance.MyCompany.CanMakeTransaction(-num5)) ? this.InvalidMat : this.ValidMat);
				if (num5 > 0f)
				{
					CostDisplay.Instance.Show(num5, base.transform.position + Vector3.up * 2f);
				}
				else
				{
					CostDisplay.Instance.Hide();
				}
				base.transform.position = new Vector3(buildRect2.x * RoadManager.Instance.RoadSize + buildRect2.width * RoadManager.Instance.RoadSize / 2f, 0f, buildRect2.y * RoadManager.Instance.RoadSize + buildRect2.height * RoadManager.Instance.RoadSize / 2f);
				base.transform.localScale = new Vector3(buildRect2.width * RoadManager.Instance.RoadSize, base.transform.localScale.y, buildRect2.height * RoadManager.Instance.RoadSize);
				return;
			}
		}
		bool flag = this.GetValid(this.x, this.y) && GameSettings.Instance.MyCompany.CanMakeTransaction(-RoadBuildCube.RoadCost);
		this.rend.sharedMaterial = ((!flag) ? this.InvalidMat : this.ValidMat);
		if (flag)
		{
			CostDisplay.Instance.Show(RoadBuildCube.RoadCost, base.transform.position + Vector3.up * 2f);
		}
		else
		{
			CostDisplay.Instance.Hide();
		}
		base.transform.localScale = new Vector3(8f, base.transform.localScale.y, 8f);
		base.transform.position = new Vector3((float)this.x * RoadManager.Instance.RoadSize + RoadManager.Instance.RoadSize / 2f, 0f, (float)this.y * RoadManager.Instance.RoadSize + RoadManager.Instance.RoadSize / 2f);
	}

	private Rect GetBuildRect()
	{
		int num = Mathf.Max(1, Mathf.Abs(this.lastX - this.x) + 1);
		int num2 = Mathf.Max(1, Mathf.Abs(this.lastY - this.y) + 1);
		bool flag = true;
		if (this.Type == 1)
		{
			if (num > num2)
			{
				num2 = 1;
			}
			else
			{
				flag = false;
				num = 1;
			}
		}
		int num3 = (this.Type != 1 || flag) ? Mathf.Min(this.x, this.lastX) : this.lastX;
		int num4 = (this.Type != 1 || !flag) ? Mathf.Min(this.y, this.lastY) : this.lastY;
		return new Rect((float)num3, (float)num4, (float)num, (float)num2);
	}

	private bool GetValid(int x, int y)
	{
		return x > 0 && RoadManager.Instance.GetRoad(x, y) != this.Type && RoadManager.Instance.CheckFree(x, y);
	}

	public byte Type;

	public int x;

	public int y;

	public int lastX;

	public int lastY;

	public static RoadBuildCube Instance;

	public static float RoadCost = 5000f;

	public Material InvalidMat;

	public Material ValidMat;

	public Renderer rend;

	public GameObject Arrow;

	private int prevX;

	private int prevY;
}
