﻿using System;

public interface IServerItem
{
	bool CancelOnUnload();

	float GetLoadRequirement();

	void HandleLoad(float load);

	string GetDescription();

	void SerializeServer(string name);
}
