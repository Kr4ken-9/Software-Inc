﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class UISoundFX : MonoBehaviour
{
	private void Update()
	{
		if (this.FadeMusic)
		{
			this.FadeIn = 0f;
			this.MusicChannel.volume = Mathf.Lerp(this.MusicChannel.volume, 0f, Time.deltaTime * 4f);
			if (this.MusicChannel.volume < 0.01f)
			{
				this.MusicChannel.Stop();
				this.FadeMusic = false;
			}
			return;
		}
		if (this.FadeIn > 0f)
		{
			this.FadeIn = Mathf.Max(0f, this.FadeIn - Time.deltaTime);
			this.MusicChannel.volume = Mathf.Lerp(0f, this.Playing.Volume, 1f - this.FadeIn / this.FadeInMax);
			return;
		}
		if (this.CurrentMusicState != null && !this.MusicChannel.isPlaying)
		{
			List<UISoundFX.MusicInstance> orNull = this._music.GetOrNull(this.CurrentMusicState);
			bool flag = false;
			if (orNull != null)
			{
				for (int i = 0; i < orNull.Count; i++)
				{
					UISoundFX.MusicInstance musicInstance = orNull[i];
					if (musicInstance != this.Playing && musicInstance.CanPlay())
					{
						flag = true;
					}
				}
			}
			if (flag)
			{
				this.PlayMusic((from x in orNull
				where x != this.Playing && x.CanPlay()
				select x).GetRandom<UISoundFX.MusicInstance>());
			}
		}
	}

	public static void ChangeMusicState(string state)
	{
		if (UISoundFX.Instance != null)
		{
			float num = (state != null) ? UISoundFX.CanSkipWithin.GetOrDefault(state, 1000f) : 1000f;
			UISoundFX.Instance.CurrentMusicState = state;
			if (UISoundFX.Instance.FadeMusic && UISoundFX.Instance.Playing != null && UISoundFX.Instance.Playing.State.Equals(state))
			{
				UISoundFX.Instance.FadeMusic = false;
				UISoundFX.Instance.FadeInMax = 1f;
				UISoundFX.Instance.FadeIn = 1f - UISoundFX.Instance.MusicChannel.volume / UISoundFX.Instance.Playing.Volume;
			}
			else if (UISoundFX.Instance.MusicChannel.isPlaying && UISoundFX.Instance.MusicChannel.time < num)
			{
				UISoundFX.Instance.FadeMusic = true;
			}
		}
	}

	private void PlayMusic(UISoundFX.MusicInstance instance)
	{
		this.Playing = instance;
		this.Playing.Play();
		this.MusicChannel.clip = instance.Clip;
		this.MusicChannel.loop = instance.Looping;
		this.MusicChannel.volume = ((instance.FadeInTime <= 0f) ? instance.Volume : 0f);
		this.FadeIn = (this.FadeInMax = instance.FadeInTime);
		this.MusicChannel.Play();
	}

	private void Awake()
	{
		if (UISoundFX.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			base.enabled = false;
			return;
		}
		UISoundFX.Instance = this;
		SceneManager.sceneLoaded += delegate(Scene x, LoadSceneMode y)
		{
			UISoundFX.ChangeMusicState(x.name);
		};
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		this._soundEffects = this.SoundEffects.ToDictionary((UISoundFX.SoundFXInstance x) => x.Name, (UISoundFX.SoundFXInstance x) => x);
		IEnumerable<IGrouping<string, UISoundFX.MusicInstance>> source = from x in this.MusicTracks
		group x by x.State;
		Func<IGrouping<string, UISoundFX.MusicInstance>, string> keySelector = (IGrouping<string, UISoundFX.MusicInstance> x) => x.Key;
		if (UISoundFX.<>f__mg$cache0 == null)
		{
			UISoundFX.<>f__mg$cache0 = new Func<IGrouping<string, UISoundFX.MusicInstance>, List<UISoundFX.MusicInstance>>(Enumerable.ToList<UISoundFX.MusicInstance>);
		}
		this._music = source.ToDictionary(keySelector, UISoundFX.<>f__mg$cache0);
		for (int i = 0; i < this.ChannelCount; i++)
		{
			AudioSource audioSource = base.gameObject.AddComponent<AudioSource>();
			audioSource.outputAudioMixerGroup = this.UIMixerGroup;
			this._channels.Add(audioSource);
		}
		this._channelGroup = new string[this._channels.Count];
	}

	private void StopFromGroup(string group)
	{
		int num = 0;
		if (this._groupPlaying.TryGetValue(group, out num) && group.Equals(this._channelGroup[num]) && this._channels[num].isPlaying)
		{
			this._channels[num].Stop();
		}
	}

	public static void PlaySFX(string sfxName, float pitch = -1f, float pan = 0f)
	{
		if (UISoundFX.Instance == null)
		{
			return;
		}
		UISoundFX.SoundFXInstance soundFXInstance = UISoundFX.Instance._soundEffects[sfxName];
		if (soundFXInstance.LastPlayed > 0f && Time.realtimeSinceStartup - soundFXInstance.LastPlayed < soundFXInstance.Cooldown)
		{
			return;
		}
		for (int i = 0; i < UISoundFX.Instance._channels.Count; i++)
		{
			AudioSource audioSource = UISoundFX.Instance._channels[UISoundFX.Instance._playIndex];
			if (!audioSource.isPlaying)
			{
				if ((soundFXInstance.PitchShift || soundFXInstance.StopAtLast) && soundFXInstance.LastPlayed > 0f && Time.realtimeSinceStartup - soundFXInstance.LastPlayed >= soundFXInstance.IncrementCooldown)
				{
					soundFXInstance.Pitch = 1f;
					soundFXInstance.Index = 0;
				}
				if (soundFXInstance.Clips.Length > 1)
				{
					if (soundFXInstance.Randomize)
					{
						soundFXInstance.Index = (soundFXInstance.Index + UnityEngine.Random.Range(1, soundFXInstance.Clips.Length - 1)) % soundFXInstance.Clips.Length;
					}
					else if (!soundFXInstance.StopAtLast || soundFXInstance.Index < soundFXInstance.Clips.Length - 1)
					{
						soundFXInstance.Index = (soundFXInstance.Index + 1) % soundFXInstance.Clips.Length;
					}
				}
				audioSource.volume = soundFXInstance.Volume * UISoundFX.Instance.UIDbScale;
				audioSource.pitch = ((pitch <= 0f) ? soundFXInstance.Pitch : pitch);
				audioSource.clip = soundFXInstance.Clips[soundFXInstance.Index];
				audioSource.Play();
				audioSource.panStereo = pan;
				if (!string.IsNullOrEmpty(soundFXInstance.Group))
				{
					UISoundFX.Instance.StopFromGroup(soundFXInstance.Group);
					UISoundFX.Instance._groupPlaying[soundFXInstance.Group] = UISoundFX.Instance._playIndex;
					UISoundFX.Instance._channelGroup[UISoundFX.Instance._playIndex] = soundFXInstance.Group;
				}
				else
				{
					UISoundFX.Instance._channelGroup[UISoundFX.Instance._playIndex] = null;
				}
				if (soundFXInstance.PitchShift && (soundFXInstance.LastPlayed < 0f || Time.realtimeSinceStartup - soundFXInstance.LastPlayed < soundFXInstance.IncrementCooldown))
				{
					soundFXInstance.Pitch = Mathf.Min(soundFXInstance.MaxPitch, soundFXInstance.Pitch + (soundFXInstance.MaxPitch - 1f) / (float)soundFXInstance.PitchIncrement);
				}
				soundFXInstance.LastPlayed = Time.realtimeSinceStartup;
				UISoundFX.Instance._playIndex = (UISoundFX.Instance._playIndex + 1) % UISoundFX.Instance._channels.Count;
				break;
			}
			UISoundFX.Instance._playIndex = (UISoundFX.Instance._playIndex + 1) % UISoundFX.Instance._channels.Count;
		}
	}

	public static Dictionary<string, float> CanSkipWithin = new Dictionary<string, float>
	{
		{
			"BuildMode",
			5f
		},
		{
			"Spring",
			0f
		},
		{
			"Summer",
			0f
		},
		{
			"Autumn",
			0f
		},
		{
			"Winter",
			0f
		}
	};

	[NonSerialized]
	private Dictionary<string, UISoundFX.SoundFXInstance> _soundEffects;

	[NonSerialized]
	private Dictionary<string, List<UISoundFX.MusicInstance>> _music;

	[NonSerialized]
	private List<AudioSource> _channels = new List<AudioSource>();

	[NonSerialized]
	private string[] _channelGroup;

	[NonSerialized]
	private Dictionary<string, int> _groupPlaying = new Dictionary<string, int>();

	[NonSerialized]
	private int _playIndex;

	public static UISoundFX Instance;

	public List<UISoundFX.SoundFXInstance> SoundEffects = new List<UISoundFX.SoundFXInstance>();

	public List<UISoundFX.MusicInstance> MusicTracks = new List<UISoundFX.MusicInstance>();

	public int ChannelCount;

	public AudioMixerGroup UIMixerGroup;

	public AudioSource MusicChannel;

	public bool FadeMusic;

	public float FadeIn;

	public float FadeInMax;

	public float UIDbScale = 1f;

	[NonSerialized]
	private string CurrentMusicState;

	[NonSerialized]
	private UISoundFX.MusicInstance Playing;

	[CompilerGenerated]
	private static Func<IGrouping<string, UISoundFX.MusicInstance>, List<UISoundFX.MusicInstance>> <>f__mg$cache0;

	[Serializable]
	public class SoundFXInstance
	{
		public string Name;

		public AudioClip[] Clips;

		public bool PitchShift;

		public bool Randomize;

		public bool StopAtLast;

		public float Cooldown;

		public float MaxPitch;

		public float IncrementCooldown;

		public float Volume = 1f;

		public int PitchIncrement;

		public string Group;

		[NonSerialized]
		public float LastPlayed = -1f;

		[NonSerialized]
		public float Pitch = 1f;

		[NonSerialized]
		public int Index;
	}

	[Serializable]
	public class MusicInstance
	{
		public bool CanPlay()
		{
			return this.AlwaysPlay || this.LastPlayed == -1f || Time.realtimeSinceStartup - this.LastPlayed > 60f;
		}

		public void Play()
		{
			this.LastPlayed = Time.realtimeSinceStartup;
		}

		public string Name;

		public AudioClip Clip;

		public bool Looping;

		public bool AlwaysPlay;

		public string State;

		public float FadeInTime;

		public float Volume = 1f;

		[NonSerialized]
		private float LastPlayed = -1f;
	}
}
