﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Poly2Tri;
using UnityEngine;
using UnityEngine.Rendering;

public class Room : Selectable
{
	public void AddFurniture(Furniture furn)
	{
		this.MakeTemperatureDirty(true);
		this._furnitures.Add(furn);
		this.FurnitureTypes.Append(furn.Type, furn);
	}

	public bool RemoveFurniture(Furniture furn)
	{
		bool flag = this._furnitures.Remove(furn);
		HashList<Furniture> furniture = this.GetFurniture(furn.Type);
		furniture.Remove(furn);
		if (flag)
		{
			this.MakeTemperatureDirty(false);
		}
		return flag;
	}

	public HashList<Furniture> GetFurniture(string type)
	{
		return this.FurnitureTypes.GetOrDefault(type, Room.EmptyFurn);
	}

	public List<Furniture> GetFurnitures()
	{
		return this._furnitures.GetUnderlyingList();
	}

	public void UpdateRoom(bool navmesh, bool outermesh, bool innermesh, bool pathnodes, bool stateVars, bool floormesh = false, bool roofmesh = false)
	{
		if (innermesh && floormesh)
		{
			floormesh = false;
		}
		if (outermesh && roofmesh)
		{
			roofmesh = false;
		}
		if (this.NavmeshRebuildStarted && !this.NavmeshRebuilding && !navmesh)
		{
			this.NavmeshRebuildStarted = false;
			this.PostNavMesh();
		}
		else if (!this.NavmeshRebuilding && (navmesh || pathnodes))
		{
			this.NavmeshRebuildStarted = true;
			this.NavmeshRebuilding = true;
			Thread thread = new Thread(delegate
			{
				this.BuildNavMesh();
			});
			thread.Start();
		}
		if (outermesh)
		{
			this.UpdateOuteredges();
			this.Highlightables = null;
		}
		if (innermesh)
		{
			this.GenerateInnerPolygon();
			this.Highlightables = null;
		}
		if (floormesh)
		{
			this.UpdateFloor();
			this.Highlightables = null;
			this.UpdateVisibility();
		}
		if (roofmesh)
		{
			this.GenerateRoof();
			this.Highlightables = null;
			this.UpdateVisibility();
		}
		if (!this.NavmeshRebuilding && pathnodes)
		{
			this.UpdatePathNodes(true);
		}
		if (stateVars)
		{
			this.innerRecalculateStateVariables(this.stateRefreshNeighbours);
		}
	}

	private bool MakeBlack()
	{
		return !this.Outdoors && !GameSettings.Instance.EditMode && GameSettings.Instance.RentMode && this.Rentable && !this.PlayerOwned;
	}

	private void SetMaterial(MeshFilter rend, string mat, int colorID, bool canBlack)
	{
		if (rend != null)
		{
			bool flag = canBlack && this.MakeBlack();
			int materialID = RoomMaterialController.GetMaterialID((!flag) ? mat : "CannotRent");
			Mesh sharedMesh = rend.sharedMesh;
			Vector2[] uv = sharedMesh.uv2;
			int num = (!mat.Equals("None")) ? ((!flag) ? colorID : RoomMaterialController.Instance.BlackColorID) : RoomMaterialController.Instance.GroundColorID;
			for (int i = 0; i < uv.Length; i++)
			{
				uv[i] = new Vector2((float)num, (float)materialID);
			}
			sharedMesh.uv2 = uv;
		}
	}

	public string InsideMat
	{
		get
		{
			return this._insideMat;
		}
		set
		{
			if (this.InnerWalls != null)
			{
				this.SetMaterial(this.InnerWalls.GetComponent<MeshFilter>(), value, this._insideColorID, true);
			}
			this._insideMat = value;
		}
	}

	public string OutsideMat
	{
		get
		{
			return this._outsideMat;
		}
		set
		{
			if (this.OuterWalls != null && !this.Outdoors)
			{
				this.SetMaterial(this.OuterWalls.GetComponent<MeshFilter>(), value, this._outsideColorID, false);
			}
			this._outsideMat = value;
		}
	}

	public string FloorMat
	{
		get
		{
			return this._floorMat;
		}
		set
		{
			if ("None".Equals(value) && !this.Outdoors)
			{
				return;
			}
			if (this.FloorMesh != null)
			{
				this.SetMaterial(this.FloorMesh.GetComponent<MeshFilter>(), value, this._floorColorID, true);
			}
			this._floorMat = value;
		}
	}

	public Color InsideColor
	{
		get
		{
			return this._insideColor;
		}
		set
		{
			if (this._insideColorID == -1)
			{
				this._insideColorID = RoomMaterialController.TakeColor();
			}
			RoomMaterialController.WriteColor(this._insideColorID, value);
			this._insideColor = value;
		}
	}

	public Color OutsideColor
	{
		get
		{
			return this._outsideColor;
		}
		set
		{
			if (this.Outdoors && this.OuterWalls != null)
			{
				Renderer component = this.OuterWalls.GetComponent<Renderer>();
				component.material.color = value;
			}
			else
			{
				if (this._outsideColorID == -1)
				{
					this._outsideColorID = RoomMaterialController.TakeColor();
				}
				RoomMaterialController.WriteColor(this._outsideColorID, value);
			}
			this._outsideColor = value;
		}
	}

	public Color FloorColor
	{
		get
		{
			return this._floorColor;
		}
		set
		{
			if (this._floorColorID == -1)
			{
				this._floorColorID = RoomMaterialController.TakeColor();
			}
			RoomMaterialController.WriteColor(this._floorColorID, value);
			this._floorColor = value;
		}
	}

	public void SetFenceStyle(string value, List<UndoObject.UndoAction> undos)
	{
		this._fenceStyle = value;
		this.FenceHeight = ObjectDatabase.Instance.FenceStyles.First((ObjectDatabase.FenceStyle x) => x.Name.Equals(this._fenceStyle)).Height;
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[i];
			WallEdge key = this.Edges[(i + 1) % this.Edges.Count];
			HashSet<WallSnap> orNull = wallEdge.Children.GetOrNull(key);
			if (orNull != null)
			{
				foreach (WallSnap wallSnap in orNull)
				{
					if (!wallSnap.ValidSnap(false, null, false))
					{
						if (undos != null && wallSnap.BeenDestroyed)
						{
							undos.Add(new UndoObject.UndoAction(wallSnap, false));
						}
						wallSnap.DestroyMe();
					}
				}
			}
		}
		if (this.Outdoors)
		{
			this.DirtyOuterMesh = true;
		}
	}

	public string FenceStyle
	{
		get
		{
			return this._fenceStyle;
		}
	}

	public float DarknessLevel
	{
		get
		{
			if (this.Darkness != null)
			{
				return this._darknessLevel;
			}
			return 0f;
		}
	}

	public bool Rentable
	{
		get
		{
			return this._rentable;
		}
		set
		{
			if (this._rentable == value)
			{
				return;
			}
			this._rentable = value;
			if (!this._rentable)
			{
				this._playerOwned = false;
			}
			Room room = this.ParentRoom ?? this;
			if (room == this)
			{
				List<Room> childrenRooms = room.ChildrenRooms;
				for (int i = 0; i < childrenRooms.Count; i++)
				{
					childrenRooms[i].Rentable = value;
				}
			}
			else
			{
				room.Rentable = value;
			}
		}
	}

	public bool PlayerOwned
	{
		get
		{
			return this._playerOwned;
		}
	}

	public void SetPlayerOwned(bool playerOwned, List<UndoObject.UndoAction> undo)
	{
		if (this._playerOwned == playerOwned)
		{
			return;
		}
		if (this._playerOwned && !GameSettings.Instance.EditMode)
		{
			this.UpdateTeams(new Team[0]);
			this.ChangeRole(-1);
			this.ClearDirt();
		}
		this._playerOwned = playerOwned;
		if (!this.Outdoors)
		{
			this.InsideMat = this.InsideMat;
			this.FloorMat = this.FloorMat;
		}
		if (!GameSettings.Instance.EditMode && !this._playerOwned)
		{
			foreach (Furniture furniture in this._furnitures.ToList<Furniture>())
			{
				if (furniture.InRentMode && !furniture.PlacedInEditMode)
				{
					if (undo != null)
					{
						undo.Add(new UndoObject.UndoAction(furniture, false));
					}
					UnityEngine.Object.Destroy(furniture.gameObject);
				}
			}
		}
		if (this._playerOwned)
		{
			this._rentable = true;
		}
		Room room = this.ParentRoom ?? this;
		if (room == this)
		{
			List<Room> childrenRooms = room.ChildrenRooms;
			for (int i = 0; i < childrenRooms.Count; i++)
			{
				childrenRooms[i].SetPlayerOwned(playerOwned, undo);
			}
		}
		else
		{
			room.SetPlayerOwned(playerOwned, undo);
		}
	}

	public override string[] GetActions()
	{
		if (GameSettings.Instance.EditMode)
		{
			return Room.EditActions;
		}
		if (!GameSettings.Instance.RentMode)
		{
			return Room.actions;
		}
		if (this.Rentable)
		{
			return (!this.PlayerOwned) ? Room.NotOwnedRentableActions : Room.RentableActions;
		}
		return Room.NonRentable;
	}

	public override string GetInfo()
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (GameSettings.Instance.RentMode && this.Rentable)
		{
			stringBuilder.AppendLine("Rent".Loc() + ": " + this.GetRentPrice().Currency(true));
		}
		if (this.RoomGroup != null)
		{
			stringBuilder.AppendLine("Group".Loc() + ": " + this.RoomGroup);
		}
		return stringBuilder.ToString();
	}

	public float GetRentPrice()
	{
		Room room = this.ParentRoom ?? this;
		float num = BuildController.GetRoomCost(room.Edges, room.Area, room.Outdoors, room.Floor, false, true);
		for (int i = 0; i < room.ChildrenRooms.Count; i++)
		{
			Room room2 = room.ChildrenRooms[i];
			num += BuildController.GetRoomCost(room2.Edges, room2.Area, room2.Outdoors, room2.Floor, false, true);
		}
		return num;
	}

	public override string[] GetExtendedInfo()
	{
		if (!this.IsPlayerControlled())
		{
			return null;
		}
		string[] array = new string[5];
		int num = 0;
		string teamsString = this.GetTeamsString();
		string str = " (";
		Room.RoomLimits forceRole = (Room.RoomLimits)this.ForceRole;
		array[num] = teamsString + str + forceRole.ToString().Loc() + ")";
		array[1] = ((1f - this.DarknessLevel) * 400f).ToString("0") + " lux";
		array[2] = (this.GetEnvironment() * 100f).ToString("0") + "%";
		array[3] = this.Temperature.Temperature(false);
		array[4] = this.Area.ToString("N0") + " " + '㎡';
		return array;
	}

	public override string[] GetExtendedIconInfo()
	{
		if (!this.IsPlayerControlled())
		{
			return null;
		}
		return new string[]
		{
			"MoreEmployees",
			"Lightbulb",
			"Painting",
			"Thermometer",
			"Grid"
		};
	}

	public override string[] GetExtendedTooltipInfo()
	{
		if (!this.IsPlayerControlled())
		{
			return null;
		}
		return new string[]
		{
			"Lighting".Loc(),
			"Environment".Loc(),
			"Temperature".Loc(),
			"Area".Loc()
		};
	}

	public override string GetMultiIcon()
	{
		return "Grid";
	}

	public override string GetMultiDesc()
	{
		return "Area".Loc();
	}

	public override string GetMultiValue(IEnumerable<Selectable> selected)
	{
		float num = 0f;
		foreach (Room room in selected.OfType<Room>())
		{
			num += room.Area;
		}
		return num.ToString("N0") + " " + '㎡';
	}

	public void UnGroup()
	{
		Room room = this.ParentRoom ?? this;
		if (room == this)
		{
			if (this.ChildrenRooms.Count > 0)
			{
				Room room2 = this.ChildrenRooms[0];
				room2.ParentRoom = room2;
				for (int i = 1; i < this.ChildrenRooms.Count; i++)
				{
					Room room3 = this.ChildrenRooms[i];
					room3.ParentRoom = room2;
					room2.ChildrenRooms.Add(room3);
				}
				this.ChildrenRooms.Clear();
			}
		}
		else
		{
			room.ChildrenRooms.Remove(this);
		}
		this.ParentRoom = this;
	}

	public override IEnumerable<Selectable> GetRelated()
	{
		if (GameSettings.Instance.EditMode)
		{
			Room p = this.ParentRoom ?? this;
			yield return p;
			for (int i = 0; i < p.ChildrenRooms.Count; i++)
			{
				yield return p.ChildrenRooms[i];
			}
		}
		yield break;
	}

	public override string GetPanelActionName()
	{
		if (!GameSettings.Instance.EditMode && GameSettings.Instance.RentMode && this.Rentable)
		{
			return (!this.PlayerOwned) ? "Lease" : "Cancel lease";
		}
		return null;
	}

	public override string GetPanelActionTip()
	{
		if (!GameSettings.Instance.EditMode && GameSettings.Instance.RentMode && this.Rentable && this.PlayerOwned)
		{
			return "TerminateLeaseWarning".Loc();
		}
		return null;
	}

	public override void InvokePanelAction(List<UndoObject.UndoAction> undos)
	{
		if (GameSettings.Instance.RentMode && this.Rentable)
		{
			if (!this.PlayerOwned)
			{
				float rentPrice = this.GetRentPrice();
				if (GameSettings.Instance.MyCompany.CanMakeTransaction(-rentPrice))
				{
					GameSettings.Instance.MyCompany.MakeTransaction(-rentPrice, Company.TransactionCategory.Bills, "Rent");
					UISoundFX.PlaySFX("Kaching", -1f, 0f);
					undos.Add(new UndoObject.UndoAction(this, this.PlayerOwned));
					this.SetPlayerOwned(true, undos);
					GameSettings.Instance.DirtyRentGrid.Add(this.Floor);
					Furniture.UpdateEdgeDetection();
				}
				else
				{
					WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), false, DialogWindow.DialogType.Error);
				}
			}
			else
			{
				undos.Add(new UndoObject.UndoAction(this, this.PlayerOwned));
				this.SetPlayerOwned(false, undos);
				GameSettings.Instance.DirtyRentGrid.Add(this.Floor);
				Furniture.UpdateEdgeDetection();
			}
		}
	}

	public override Selectable PanelActionDivert()
	{
		return this.ParentRoom ?? this;
	}

	public float GetAuraValue(Furniture.AuraTypes type)
	{
		if (this.AuraValues == null || type >= (Furniture.AuraTypes)this.AuraValues.Length)
		{
			return 1f;
		}
		return 1f + this.AuraValues[(int)type];
	}

	public override Color[] GetExtendedColorInfo()
	{
		return new Color[]
		{
			base.GetColorStat((1f - this.DarknessLevel) * 2f),
			base.GetColorStat(this.GetEnvironment()),
			base.GetColorStat((1f - Mathf.Abs(21f - this.Temperature) / 24f) * 2f),
			base.GetColorStat(1f)
		};
	}

	public override string Description()
	{
		return "Rooms";
	}

	public void UpdateTeams(IEnumerable<Team> newTeam)
	{
		this.Teams.Clear();
		this.Teams.AddRange(newTeam);
		this.TeamText.tm.text = this.GetTeamsString();
		this.TeamText.InUse = (this.Teams.Count > 0);
	}

	public void AddTeam(Team newTeam)
	{
		this.Teams.Add(newTeam);
		this.TeamText.tm.text = this.GetTeamsString();
		this.TeamText.InUse = (this.Teams.Count > 0);
	}

	public string GetTeamsString()
	{
		return ((this.Teams.Count != 0) ? this.Teams.First<Team>().Name : "None".Loc()) + ((this.Teams.Count <= 1) ? string.Empty : ("+" + (this.Teams.Count - 1)));
	}

	public bool CompatibleWithTeam(Team team)
	{
		return this.Teams.Count == 0 || (team != null && this.Teams.Contains(team));
	}

	public bool ToiletInUse()
	{
		HashList<Furniture> furniture = this.GetFurniture("Toilet");
		for (int i = 0; i < furniture.Count; i++)
		{
			Furniture furniture2 = furniture[i];
			if ("Toilet".Equals(furniture2.Type) && furniture2.InteractionPoints[0].UsedBy != null)
			{
				return true;
			}
		}
		return false;
	}

	public void RecalculateTableGroups()
	{
		this.TableParents.Clear();
		List<List<TableScript>> list = (from x in this._furnitures
		where x != null && x.gameObject != null && x.Table != null && x.UsableForTableGroup()
		select x.GetComponent<TableScript>()).ToList<TableScript>().SimpleClustering((TableScript x, TableScript y) => (x.transform.position - y.transform.position).magnitude, 3f);
		for (int i = 0; i < list.Count; i++)
		{
			this.TableParents.Add(list[i][0]);
			list[i][0].Parent = list[i][0];
			list[i][0].Children.Clear();
			list[i][0].Children.AddRange(list[i].Skip(1));
			for (int j = 1; j < list[i].Count; j++)
			{
				list[i][j].Parent = list[i][0];
				list[i][j].Children.Clear();
			}
		}
		for (int k = 0; k < this._furnitures.Count; k++)
		{
			Furniture furniture = this._furnitures[k];
			if (furniture != null && furniture.gameObject != null && furniture.Table != null && !furniture.UsableForTableGroup())
			{
				furniture.Table.Parent = furniture.Table;
				furniture.Table.Children.Clear();
			}
		}
		for (int l = 0; l < this.TableParents.Count; l++)
		{
			this.TableParents[l].UpdateStatus();
		}
	}

	public void RemoveTable(TableScript table)
	{
		this.RecalculateTableGroups();
	}

	public void AddTable(Vector2 p, TableScript table)
	{
		this.RecalculateTableGroups();
	}

	public void RecalculateStateVariables(bool refreshNeighbours = false)
	{
		this.DirtyStateVariables = true;
		this.stateRefreshNeighbours = (this.stateRefreshNeighbours || refreshNeighbours);
	}

	public static float GetOutdoorNoise(Vector3 pos)
	{
		return (pos.y >= 0f) ? (GameData.EnvNoise[(int)GameSettings.Instance.EnvType] / (1f + pos.y)) : 0f;
	}

	private void CalculateNoisePropagation()
	{
		this.NoisePropagation.Clear();
		this.WallArea = 0f;
		if (this.Outdoors || this.Dummy)
		{
			this.WallArea = 1f;
			return;
		}
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[i];
			WallEdge wallEdge2 = this.Edges[(i + 1) % this.Edges.Count];
			float num = (wallEdge.Pos - wallEdge2.Pos).magnitude * 2f;
			this.WallArea += num;
			float num2 = num * 0.1f;
			Room key = GameSettings.Instance.sRoomManager.Outside;
			foreach (KeyValuePair<Room, WallEdge> keyValuePair in wallEdge2.Links)
			{
				if (keyValuePair.Value == wallEdge)
				{
					if (!keyValuePair.Key.Outdoors)
					{
						key = keyValuePair.Key;
					}
					break;
				}
			}
			HashSet<WallSnap> hashSet;
			if (wallEdge.Children.TryGetValue(wallEdge2, out hashSet))
			{
				foreach (WallSnap wallSnap in hashSet)
				{
					RoomSegment roomSegment = wallSnap as RoomSegment;
					if (roomSegment != null)
					{
						num2 += roomSegment.WallWidth * (roomSegment.Height2 - roomSegment.Height1) * roomSegment.NoiseFactor;
					}
				}
			}
			this.NoisePropagation.AddUp(key, num2);
		}
	}

	private void innerRecalculateStateVariables(bool refreshNeighbours)
	{
		if (GameSettings.Instance.sRoomManager.DisableMeshRebuild)
		{
			return;
		}
		this.DirtyStateVariables = false;
		this.stateRefreshNeighbours = false;
		if (this.Outdoors)
		{
			this.WindowDarkLevel = 1f;
			this.WindowDarkLevelNoCap = 1f;
			if (this._furnitures.Count > 0)
			{
				float num = 1.5f;
				for (int i = 0; i < this._furnitures.Count; i++)
				{
					Furniture furniture = this._furnitures[i];
					num *= furniture.Environment;
				}
				this.FurnEnvironment = num;
			}
			else
			{
				this.FurnEnvironment = 1.5f;
			}
			if (refreshNeighbours)
			{
				GameSettings.Instance.sRoomManager.PropagateLighting(this);
			}
			return;
		}
		this._furnitures.RemoveAll((Furniture x) => x == null);
		this.FurnitureTypes.ForEachEnum(delegate(KeyValuePair<string, HashList<Furniture>> x)
		{
			x.Value.RemoveAll((Furniture y) => y == null);
		});
		this.AuraValues = new float[Furniture.AuraCount];
		this.AuraCount = new int[Furniture.AuraCount];
		bool flag = true;
		for (int j = 0; j < this._furnitures.Count; j++)
		{
			Furniture furniture2 = this._furnitures[j];
			for (int k = 0; k < Furniture.AuraCount; k++)
			{
				if (flag)
				{
					this.AuraValues[k] = 0f;
					this.AuraCount[k] = 0;
					flag = false;
				}
				if (furniture2.AuraValues == null || furniture2.AuraValues.Length <= k)
				{
					break;
				}
				if (furniture2.AuraValues[k] != -1f)
				{
					this.AuraCount[k]++;
					this.AuraValues[k] += furniture2.AuraValues[k];
				}
			}
		}
		for (int l = 0; l < Furniture.AuraCount; l++)
		{
			if (this.AuraCount[l] > 0)
			{
				this.AuraValues[l] /= (float)this.AuraCount[l];
			}
		}
		this.Lamps = (from x in this._furnitures
		where x != null && x.Lighting > 0f
		select x).ToArray<Furniture>();
		List<RoomSegment> segments = this.GetSegments(null);
		float num2 = (from x in segments
		where x != null && (x.ParentRooms[0] == null || x.ParentRooms[1] == null)
		select x).Sum((RoomSegment x) => x.LightAddition);
		this.WindowDarkLevelNoCap = num2 / (this.Area / 4f);
		this.WindowDarkLevel = Mathf.Min(1f, this.WindowDarkLevelNoCap * 4f);
		if (this._furnitures.Count > 0)
		{
			float num3 = 1f;
			for (int m = 0; m < this._furnitures.Count; m++)
			{
				num3 *= this._furnitures[m].Environment;
			}
			this.FurnEnvironment = num3;
		}
		else
		{
			this.FurnEnvironment = 1f;
		}
		this.FurnEnvironment *= Mathf.Min(2f, 1f + this.WindowDarkLevelNoCap / 4f);
		if (refreshNeighbours)
		{
			GameSettings.Instance.sRoomManager.PropagateLighting(this);
		}
	}

	public List<RoomSegment> GetSegments(HashSet<Room> destroy = null)
	{
		HashSet<RoomSegment> hashSet = new HashSet<RoomSegment>();
		if (this.Dummy)
		{
			foreach (WallEdge wallEdge in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(0))
			{
				foreach (KeyValuePair<WallEdge, HashSet<WallSnap>> keyValuePair in wallEdge.Children)
				{
					if (!wallEdge.Links.ContainsValue(keyValuePair.Key))
					{
						hashSet.AddRange(keyValuePair.Value.OfType<RoomSegment>());
					}
				}
			}
		}
		else
		{
			WallEdge wallEdge2 = this.Edges[0];
			WallEdge wallEdge3 = wallEdge2;
			int num = 0;
			for (;;)
			{
				WallEdge wallEdge4 = wallEdge2.Links[this];
				if (wallEdge2.Children.ContainsKey(wallEdge4))
				{
					if (destroy != null)
					{
						foreach (RoomSegment roomSegment in wallEdge2.Children[wallEdge4].OfType<RoomSegment>())
						{
							Room room = wallEdge4.GetRoom(wallEdge2);
							if (room == null || destroy.Contains(room) || !roomSegment.ValidSnap(false, destroy, false))
							{
								hashSet.Add(roomSegment);
							}
						}
					}
					else
					{
						hashSet.AddRange(wallEdge2.Children[wallEdge4].OfType<RoomSegment>());
					}
				}
				wallEdge2 = wallEdge4;
				num++;
				if (num > this.Edges.Count * 2)
				{
					break;
				}
				if (wallEdge2 == wallEdge3)
				{
					goto IL_1D3;
				}
			}
			Debug.LogException(new Exception("Broken while loop"), this);
		}
		IL_1D3:
		return hashSet.ToList<RoomSegment>();
	}

	private float GetFurnTempPower(Furniture furn, float potential)
	{
		return potential * furn.upg.Quality.MapRange(0f, 0.5f, 0f, 1f, true);
	}

	public void MakeTemperatureDirty(bool instantly)
	{
		this._lastTempValueUpdate = ((!instantly) ? (Time.realtimeSinceStartup - 9.75f) : -1f);
	}

	private void CalculateInsulation()
	{
		if (this.Outdoors)
		{
			this.Insulation = 2f;
			return;
		}
		if (this.Floor < 0)
		{
			this.Insulation = 0.5f;
			return;
		}
		float num = 0f;
		float num2 = 0f;
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge e1 = this.Edges[i];
			WallEdge wallEdge = this.Edges[(i + 1) % this.Edges.Count];
			float magnitude = (wallEdge.Pos - e1.Pos).magnitude;
			num += magnitude;
			Room room = (from x in wallEdge.Links
			where x.Value == e1
			select x.Key).FirstOrDefault<Room>();
			if (room != null && !room.Outdoors)
			{
				num2 += magnitude * 0.5f;
			}
			else
			{
				float num3 = 0f;
				HashSet<WallSnap> source;
				if (e1.Children.TryGetValue(wallEdge, out source))
				{
					foreach (RoomSegment roomSegment in source.OfType<RoomSegment>())
					{
						num3 += roomSegment.LightAddition;
					}
				}
				num2 += magnitude + num3;
			}
		}
		this.Insulation = num2 / num;
	}

	private void UpdateTemperatureValues()
	{
		this._maxCoolingTemp = 0f;
		this._coolingControlArea = 0f;
		this._maxHeatingTemp = 0f;
		this._heatingControlArea = 0f;
		this._serverTemp = 0f;
		this._noThermo = 0f;
		this._noThermoArea = 0f;
		this._noThermoMax = 0f;
		this._noThermoAgg = 0f;
		this._lastTempValueUpdate = Time.realtimeSinceStartup;
		if (this.Outdoors)
		{
			return;
		}
		foreach (Furniture furniture in this.TempControllers)
		{
			float num = Mathf.Abs(furniture.HeatCoolPotential);
			if (furniture.HeatCoolPotential < 0f)
			{
				float num2 = Mathf.Min(1f, furniture.HeatCoolArea / furniture.TempControllerArea);
				this.TheoMaxCoolingTemp = Mathf.Max(this.TheoMaxCoolingTemp, num);
				this.TheoCoolingControlArea += furniture.HeatCoolArea * num2 * num;
				if (furniture.IsOn)
				{
					num = this.GetFurnTempPower(furniture, num);
					this._maxCoolingTemp = Mathf.Max(this._maxCoolingTemp, num);
					this._coolingControlArea += furniture.HeatCoolArea * num2 * num;
				}
			}
			else
			{
				float num3 = Mathf.Min(1f, furniture.HeatCoolArea / furniture.TempControllerArea);
				this.TheoMaxHeatingTemp = Mathf.Max(this.TheoMaxHeatingTemp, num);
				this.TheoHeatingControlArea += furniture.HeatCoolArea * num3 * num;
				if (furniture.IsOn)
				{
					num = this.GetFurnTempPower(furniture, num);
					this._maxHeatingTemp = Mathf.Max(this._maxHeatingTemp, num);
					this._heatingControlArea += furniture.HeatCoolArea * num3 * num;
				}
			}
		}
		for (int i = 0; i < this._furnitures.Count; i++)
		{
			Furniture furniture2 = this._furnitures[i];
			if (!furniture2.TemperatureController)
			{
				if (furniture2.HeatCoolPotential != 0f)
				{
					if (!furniture2.EqualizeTemperature)
					{
						float furnTempPower = this.GetFurnTempPower(furniture2, furniture2.HeatCoolPotential);
						float num4 = Mathf.Abs(furnTempPower);
						this._noThermo += furnTempPower;
						this._noThermoMax = Mathf.Max(this._noThermoMax, num4);
						this._noThermoAgg += num4;
						this._noThermoArea += furniture2.HeatCoolArea * num4;
					}
					else if (furniture2.HeatCoolPotential < 0f)
					{
						float num5 = Mathf.Abs(furniture2.HeatCoolPotential);
						this.TheoMaxCoolingTemp = Mathf.Max(this.TheoMaxCoolingTemp, num5);
						this.TheoCoolingControlArea += furniture2.HeatCoolArea * num5;
						num5 = this.GetFurnTempPower(furniture2, num5);
						if (furniture2.IsOn)
						{
							this._maxCoolingTemp = Mathf.Max(this._maxCoolingTemp, num5);
							this._coolingControlArea += furniture2.HeatCoolArea * num5;
						}
					}
					else
					{
						float num6 = Mathf.Abs(furniture2.HeatCoolPotential);
						this.TheoMaxHeatingTemp = Mathf.Max(this.TheoMaxHeatingTemp, num6);
						this.TheoHeatingControlArea += furniture2.HeatCoolArea * num6;
						num6 = this.GetFurnTempPower(furniture2, num6);
						if (furniture2.IsOn)
						{
							this._maxHeatingTemp = Mathf.Max(this._maxHeatingTemp, num6);
							this._heatingControlArea += furniture2.HeatCoolArea * num6;
						}
					}
				}
				else if (furniture2.Type.Equals("Server") && furniture2.IsOn)
				{
					Server component = furniture2.GetComponent<Server>();
					this._serverTemp += component.Power.MapRange(50f, 10000f, 0.125f, 0.375f, false) * (1f - component.Rep.Available).MapRange(0f, 1f, 0.25f, 1f, false) / this.Area;
				}
			}
		}
		if (this._maxHeatingTemp > 0f)
		{
			this._heatingControlArea /= this._maxHeatingTemp;
		}
		if (this._maxCoolingTemp > 0f)
		{
			this._coolingControlArea /= this._maxCoolingTemp;
		}
		if (this.TheoMaxHeatingTemp > 0f)
		{
			this.TheoHeatingControlArea /= this.TheoMaxHeatingTemp;
		}
		if (this.TheoMaxCoolingTemp > 0f)
		{
			this.TheoCoolingControlArea /= this.TheoMaxCoolingTemp;
		}
		this.UpdateTemperature(false);
	}

	public void ResetTempUsage()
	{
		this.TempHeatUsageAgg = 0f;
		this.TempCoolUsageAgg = 0f;
		this.TempHeatUsageMax = 0f;
		this.TempCoolUsageMax = 0f;
	}

	public void UpdateTemperature(bool withCost)
	{
		if (this.Outdoors)
		{
			this.Temperature = TimeOfDay.Instance.Temperature;
			return;
		}
		this.Temperature = ((this.Floor != -1) ? TimeOfDay.Instance.Temperature : 5f) + this._serverTemp;
		bool flag = this.Temperature > 21f;
		float num = this.Area * this.Insulation;
		float num2 = (!flag) ? this._maxHeatingTemp : this._maxCoolingTemp;
		float num3 = (!flag) ? this._heatingControlArea : this._coolingControlArea;
		if (num2 != 0f)
		{
			float num4 = num2 * (num3 / num);
			if (flag)
			{
				if (withCost)
				{
					float num5 = Mathf.Min(1f, (this.Temperature - 21f) / num4);
					this.TempCoolUsageAgg += num5 * Utilities.PerDay(1f, 60f, false);
					this.TempCoolUsageMax = Mathf.Max(this.TempCoolUsageMax, num5);
				}
				this.Temperature = Mathf.Max(21f, this.Temperature - num4);
			}
			else
			{
				if (withCost)
				{
					float num6 = Mathf.Min(1f, (21f - this.Temperature) / num4);
					this.TempHeatUsageAgg += num6 * Utilities.PerDay(1f, 60f, false);
					this.TempHeatUsageMax = Mathf.Max(this.TempHeatUsageMax, num6);
				}
				this.Temperature = Mathf.Min(21f, this.Temperature + num4);
			}
		}
		if (this._noThermo != 0f)
		{
			this._noThermoArea = this._noThermoArea / this._noThermoMax * Mathf.Abs(this._noThermo / this._noThermoAgg);
			float num7 = this._noThermoMax * (this._noThermoArea / this.Area);
			if (this.Temperature > -10f)
			{
				this.Temperature = Mathf.Max(-10f, this.Temperature - num7);
			}
		}
	}

	public void UpdateColors()
	{
		if (DataOverlay.HasActive ^ this.DataOverlayMode)
		{
			this.DataOverlayMode = DataOverlay.HasActive;
			this.OutsideColor = this.OutsideColor;
			this.InsideColor = this.InsideColor;
			this.FloorColor = this.FloorColor;
		}
		else if (DataOverlay.HasActive)
		{
			if (!this.Outdoors)
			{
				RoomMaterialController.WriteColor(this._outsideColorID, DataOverlay.Instance.GetColor(this, this.OutsideColor, RoomMaterialController.GetColor(this._outsideColorID)));
				RoomMaterialController.WriteColor(this._insideColorID, DataOverlay.Instance.GetColor(this, this.InsideColor, RoomMaterialController.GetColor(this._insideColorID)));
			}
			RoomMaterialController.WriteColor(this._floorColorID, DataOverlay.Instance.GetColor(this, this.FloorColor, RoomMaterialController.GetColor(this._floorColorID)));
		}
	}

	private float LampCount()
	{
		float num = 0f;
		for (int i = 0; i < this.Lamps.Length; i++)
		{
			if (this.Lamps[i].IsOn || HUD.Instance.BuildMode)
			{
				num += this.Lamps[i].Lighting;
			}
		}
		return num;
	}

	private void UpdateDust()
	{
	}

	public void UpdateVisibility()
	{
		if (!this.Dummy)
		{
			bool flag = Utilities.InBasement(GameSettings.Instance.ActiveFloor) == Utilities.InBasement(this.Floor) || this.HasTwoFloor;
			if (this.InnerWalls != null)
			{
				if (this.IsSurrounded && !CameraScript.Instance.FlyMode)
				{
					this.InnerWalls.GetComponent<Renderer>().enabled = (this.Floor == GameSettings.Instance.ActiveFloor && flag);
				}
				else
				{
					this.InnerWalls.GetComponent<Renderer>().enabled = (this.Floor <= GameSettings.Instance.ActiveFloor && flag);
				}
			}
			if (this.OuterWalls != null)
			{
				Renderer component = this.OuterWalls.GetComponent<Renderer>();
				if (flag)
				{
					component.enabled = true;
					component.shadowCastingMode = ((this.Floor > GameSettings.Instance.ActiveFloor) ? ShadowCastingMode.ShadowsOnly : ShadowCastingMode.On);
				}
				else
				{
					component.enabled = false;
				}
			}
			if (this.FloorMesh != null)
			{
				if (this.IsSurrounded && !CameraScript.Instance.FlyMode)
				{
					this.FloorMesh.GetComponent<Renderer>().enabled = (this.Floor == GameSettings.Instance.ActiveFloor && flag);
				}
				else
				{
					this.FloorMesh.GetComponent<Renderer>().enabled = (this.Floor <= GameSettings.Instance.ActiveFloor && flag);
				}
			}
			if (this.DirtObject != null)
			{
				this.DirtObject.SetActive(GameSettings.Instance.ActiveFloor == this.Floor);
			}
			if (this.UpperWalls != null)
			{
				this.UpperWalls.GetComponent<Renderer>().enabled = (this.Floor == GameSettings.Instance.ActiveFloor);
				if (GameSettings.Instance.ActiveFloor == this.Floor)
				{
					this.UpperWalls.transform.position = new Vector3(0f, (GameSettings.WallsDown != GameSettings.WallState.Low && GameSettings.WallsDown != GameSettings.WallState.LowNoSeg) ? ((float)(this.Floor * 2)) : ((float)(this.Floor * 2) - 1.8f), 0f);
				}
			}
			if (this.Roof != null)
			{
				Renderer component2 = this.Roof.GetComponent<Renderer>();
				if (flag)
				{
					component2.enabled = true;
					component2.shadowCastingMode = ((this.Floor >= GameSettings.Instance.ActiveFloor) ? ShadowCastingMode.ShadowsOnly : ShadowCastingMode.TwoSided);
				}
				else
				{
					component2.enabled = false;
				}
			}
		}
	}

	private void OnApplicationQuit()
	{
		GameSettings.IsQuitting = true;
	}

	private void Update()
	{
		this.UpdateRoom(this.DirtyNavMesh, !this.Dummy && this.DirtyOuterMesh, !this.Dummy && this.DirtyInnerMesh, this.DirtyPathNodes, this.DirtyStateVariables, this.DirtyFloorMesh, this.DirtyRoofMesh);
		if (!this.Dummy)
		{
			this.CanSelect = (this.Floor > -1 || GameSettings.Instance.ActiveFloor == -1);
			this.Problems.Clear();
			if (this.Outdoors)
			{
				this._darknessLevel = 0f;
			}
			else
			{
				if (this._lastTempValueUpdate < 0f || Time.realtimeSinceStartup - this._lastTempValueUpdate > 10f)
				{
					this.UpdateTemperatureValues();
				}
				this.UpdateTemperature(false);
				float num = Mathf.Min(1f, this.LampCount() / (this.Area / 16f));
				this._darknessLevel = Mathf.Min((this.Floor != -1) ? (1f - Mathf.Min(1f, this.WindowDarkLevel + this.IndirectLighting) * TimeOfDay.LightLevel) : 1f, 1f - num);
				if (Mathf.Approximately(this._darknessLevel, 0f))
				{
					this.DarknessRend.enabled = false;
				}
				else
				{
					this.DarknessRend.sharedMaterial = MaterialBank.Instance.GetDarkness(this._darknessLevel);
					this.DarknessRend.enabled = ((!HUD.Instance.BuildMode || GameSettings.WallsDown == GameSettings.WallState.High) && ((this.Floor >= 0 || GameSettings.Instance.ActiveFloor < 0 || this.HasTwoFloor) && this.Floor <= GameSettings.Instance.ActiveFloor) && (this._darknessLevel > 0f || this.Dust > 0f));
				}
				if (this.IsPlayerControlled() && this.Floor == GameSettings.Instance.ActiveFloor && HUD.Instance.BuildMode && this.OuterWalls != null && this.OuterWalls.GetComponent<Renderer>().isVisible)
				{
					this.UpdateProblems();
				}
			}
			this.UpdateColors();
		}
	}

	public void UpdateFrameState()
	{
		this.UpdateDust();
	}

	private void UpdateProblems()
	{
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < this._furnitures.Count; i++)
		{
			Furniture furniture = this._furnitures[i];
			if (!furniture.TemperatureController)
			{
				if (furniture.Type.Equals("Computer"))
				{
					num2++;
				}
				else if (furniture.Type.Equals("Toilet"))
				{
					num++;
				}
			}
		}
		if (!this.Accessible)
		{
			this.Problems.Add("InaccessibleRoom".Loc());
		}
		if (num > 1 || ((this.Occupants.Count > 1 || num2 > 0 || !this.IsPrivate) && num > 0))
		{
			this.Problems.Add("PublicToiletWarning".Loc());
		}
		float num3 = this.Area * this.Insulation;
		if (num2 > 0)
		{
			if (TimeOfDay.Instance.CurrentWeather.MaximumTemperature > 21f && (num3 > this.TheoCoolingControlArea || TimeOfDay.Instance.CurrentWeather.MaximumTemperature - 23f > this.TheoMaxCoolingTemp * (this.TheoCoolingControlArea / num3)))
			{
				this.Problems.Add("RoomCoolingWarning".Loc());
			}
			else if (TimeOfDay.Instance.CurrentWeather.MinimumTemperature < 21f && (num3 > this.TheoHeatingControlArea || 19f - TimeOfDay.Instance.CurrentWeather.MinimumTemperature > this.TheoMaxHeatingTemp * (this.TheoHeatingControlArea / num3)))
			{
				this.Problems.Add("RoomHeatingWarning".Loc());
			}
		}
		if (num2 > 1 && (float)num2 / this.Area > 0.3f)
		{
			this.Problems.Add("RoomOvercrowdWarning".Loc());
		}
		if (num2 > 0)
		{
			if (this.WindowDarkLevel + this.IndirectLighting + this.Lamps.Sum((Furniture x) => x.Lighting) / (this.Area / 16f) < 0.75f)
			{
				this.Problems.Add("RoomLighingWaning".Loc());
			}
		}
	}

	private void Start()
	{
		if (!this.Deserialized)
		{
			this.TeamChange();
			this.ChangeRole(this.ForceRole);
		}
		this.AuraValues = new float[Furniture.AuraCount];
		for (int i = 0; i < Furniture.AuraCount; i++)
		{
			this.AuraValues[i] = 0f;
		}
		this.shadowMat = new Material(this.shadowMat);
		if (this.Edges != null && this.Edges.Count > 0)
		{
			Vector2 f = this.Edges[0].Pos;
			if (this.Edges.All((WallEdge x) => x.Pos == f))
			{
				UnityEngine.Object.Destroy(base.gameObject);
			}
		}
	}

	private void OnDestroy()
	{
		Room.UpdatePCNoisiness.Remove(this);
		if (GameSettings.IsQuitting || GameSettings.Instance == null || HUD.Instance == null)
		{
			return;
		}
		this.UnGroup();
		if (this.Floor < 1 && this._furnitures.Any((Furniture x) => x.TwoFloors && ((x.Parent == this && this.Floor == -1) || x.ExtraParent == this)))
		{
			TimeOfDay.Instance.GroundTopDirty = true;
		}
		RoomMaterialController.FreeColor(this._insideColorID);
		RoomMaterialController.FreeColor(this._outsideColorID);
		RoomMaterialController.FreeColor(this._floorColorID);
		List<WallEdge> p = this.Destroy();
		GameSettings.Instance.sRoomManager.AllSegments.RemoveAll((WallEdge x) => p.Contains(x));
		GameSettings.Instance.sRoomManager.Rooms.RemoveAll((Room x) => x == this);
		List<Furniture> list = this._furnitures.ToList<Furniture>();
		for (int i = 0; i < list.Count; i++)
		{
			Furniture furniture = list[i];
			UnityEngine.Object.Destroy(furniture.gameObject);
		}
		if (this.Floor == 0)
		{
			GameSettings.Instance.sRoomManager.Outside.DirtyNavMesh = true;
		}
		GameSettings.Instance.RemoveRoomFromGroups(this);
		GameSettings.Instance.sRoomManager.RemoveRoom(this);
		GameSettings.Instance.sRoomManager.RoomNearnessDirty = true;
		List<Actor> list2 = this.Occupants.ToList<Actor>();
		for (int j = 0; j < list2.Count; j++)
		{
			Actor actor = list2[j];
			if (actor != null && actor.gameObject != null && actor.currentRoom == this)
			{
				actor.currentRoom = null;
				actor.ResetState();
			}
		}
	}

	public void RefreshNoise()
	{
		if (this.Dummy)
		{
			return;
		}
		if (AudioVisualizer.Instance != null && AudioVisualizer.Instance.LastRoom == this)
		{
			AudioVisualizer.Instance.ForceRedraw();
		}
		Room.UpdatePCNoisiness.Add(this);
		for (int i = 0; i < this.Edges.Count; i++)
		{
			Room room = this.Edges[(i + 1) % this.Edges.Count].GetRoom(this.Edges[i]);
			if (room != null)
			{
				Room.UpdatePCNoisiness.Add(room);
			}
		}
	}

	private TriangleNode ClosestNav(Vector2 p)
	{
		if (this.NavMap.Length == 0)
		{
			return null;
		}
		if (this.BSPNavMap != null)
		{
			return this.BSPNavMap.GetNodes(p).MinInstance((TriangleNode x) => Utilities.ClosestPointOnTriangle(x.Points, p).SqrDist(p));
		}
		if (this.NavMap == null)
		{
			return null;
		}
		return this.NavMap.MinInstance((TriangleNode x) => Utilities.ClosestPointOnTriangle(x.Points, p).SqrDist(p));
	}

	private Vector2? FixNavPos(Vector2 p)
	{
		if (this.NavMap == null || this.NavMap.Length == 0 || this.GetNodeAt(p) != null)
		{
			return null;
		}
		TriangleNode triangleNode = this.NavMap.MinInstance((TriangleNode x) => x.Center.SqrDist(p));
		if (triangleNode != null)
		{
			return new Vector2?(Utilities.ClosestPointOnTriangle(triangleNode.Points, p));
		}
		return null;
	}

	public void FixActorPosition(Actor actor, bool stateReset = true)
	{
		if (this.Dummy)
		{
			return;
		}
		Vector2 vector = new Vector2(actor.transform.position.x, actor.transform.position.z);
		Vector2? vector2 = this.FixNavPos(vector);
		if (vector2 != null)
		{
			actor.transform.position = new Vector3(vector2.Value.x, actor.transform.position.y, vector2.Value.y);
			if (stateReset && vector2.Value.MaxDist(vector) > 0.05f)
			{
				actor.ResetState();
			}
		}
	}

	public void FixActorPositions()
	{
		if (this.Dummy)
		{
			return;
		}
		for (int i = 0; i < this.Occupants.Count; i++)
		{
			Actor actor = this.Occupants[i];
			if (actor.CurrentPath == null)
			{
				this.FixActorPosition(actor, true);
			}
		}
	}

	public TriangleNode GetNodeAt(Vector2 p)
	{
		if (this.NavMap == null)
		{
			return null;
		}
		if (this.BSPNavMap != null)
		{
			List<TriangleNode> nodes = this.BSPNavMap.GetNodes(p);
			for (int i = 0; i < nodes.Count; i++)
			{
				if (nodes[i].IsInside(p))
				{
					return nodes[i];
				}
			}
			return null;
		}
		for (int j = 0; j < this.NavMap.Length; j++)
		{
			if (this.NavMap[j].IsInside(p))
			{
				return this.NavMap[j];
			}
		}
		return null;
	}

	public bool FindPath(Vector3 Start, Vector3 Goal, ref List<Vector3> result, IRoomConnector a, IRoomConnector b)
	{
		if (a != null && b != null)
		{
			List<Vector3> orNull = this.CachedPaths.GetOrNull(new KeyValuePair<IRoomConnector, IRoomConnector>(a, b));
			if (orNull != null)
			{
				result.AddRange(orNull);
				return true;
			}
			orNull = this.CachedPaths.GetOrNull(new KeyValuePair<IRoomConnector, IRoomConnector>(b, a));
			if (orNull != null)
			{
				for (int i = orNull.Count - 1; i > -1; i--)
				{
					result.Add(orNull[i]);
				}
				return true;
			}
		}
		Vector2 p = Start.FlattenVector3();
		TriangleNode triangleNode = this.GetNodeAt(p);
		if (triangleNode == null)
		{
			triangleNode = this.ClosestNav(p);
			if (triangleNode == null)
			{
				return false;
			}
		}
		TriangleNode nodeAt = this.GetNodeAt(new Vector2(Goal.x, Goal.z));
		if (nodeAt == null)
		{
			return false;
		}
		if (triangleNode == nodeAt)
		{
			result.Add(new Vector3(Start.x, (float)(this.Floor * 2), Start.z));
			result.Add(new Vector3(Goal.x, (float)(this.Floor * 2), Goal.z));
			return true;
		}
		Vector2 center = triangleNode.Center;
		Vector2 center2 = nodeAt.Center;
		triangleNode.StartEnd = (nodeAt.StartEnd = true);
		triangleNode.Center = new Vector2(Start.x, Start.z);
		nodeAt.Center = new Vector2(Goal.x, Goal.z);
		TriangleNode.MainPathToggle += 1u;
		List<PathNode<TriangleNode>> list = NodePathFinding<TriangleNode>.FindPath(triangleNode.PathNode, nodeAt.PathNode, new Func<TriangleNode, TriangleNode, float>(this.TriangleDistance), new Func<TriangleNode, TriangleNode, TriangleNode, float>(this.TriangleHeuristic), (object x) => true, null);
		triangleNode.StartEnd = (nodeAt.StartEnd = false);
		triangleNode.Center = center;
		nodeAt.Center = center2;
		if (list == null)
		{
			return false;
		}
		object portalCache = Room.PortalCache;
		lock (portalCache)
		{
			Room.PortalCache.Clear();
			TriangleNode.GetPortals(list, Start.FlattenVector3(), Room.PortalCache);
			Room.PortalCache.Insert(0, new TriangleNode.Portal(new Vector2(Start.x, Start.z)));
			if (Room.PortalCache.Count > 1)
			{
				Vector2 p2 = Start.FlattenVector3();
				Vector2 left = Room.PortalCache[1].Left;
				Vector2 right = Room.PortalCache[1].Right;
				if (Utilities.isLeft(p2, left, right) > 0)
				{
					Room.PortalCache.Insert(1, new TriangleNode.Portal((p2.SqrDist(left) >= p2.SqrDist(right)) ? right : left));
				}
			}
			Room.PortalCache.Add(new TriangleNode.Portal(new Vector2(Goal.x, Goal.z)));
			if (a != null && b != null)
			{
				List<Vector3> list2 = (from x in TriangleNode.StringPull(Room.PortalCache)
				select new Vector3(x.x, (float)(this.Floor * 2), x.y)).ToList<Vector3>();
				this.CachedPaths[new KeyValuePair<IRoomConnector, IRoomConnector>(a, b)] = list2;
				result.AddRange(list2);
			}
			else
			{
				List<Vector2> source = TriangleNode.StringPull(Room.PortalCache);
				result.AddRange(from x in source
				select new Vector3(x.x, (float)(this.Floor * 2), x.y));
			}
		}
		return true;
	}

	private float TriangleDistance(TriangleNode a, TriangleNode b)
	{
		if (a == null || b == null)
		{
			return 0f;
		}
		Vector2 a2 = (a.PathToggle != TriangleNode.MainPathToggle) ? a.Center : a.preferredPoint;
		Vector2 b2 = (b.PathToggle != TriangleNode.MainPathToggle) ? b.Center : b.preferredPoint;
		return (a2 - b2).magnitude * a.Weight[b];
	}

	private float TriangleHeuristic(TriangleNode from, TriangleNode a, TriangleNode b)
	{
		if (a == null || b == null || a == b)
		{
			return 0f;
		}
		this.UpdateCenter(a, b);
		float num = (from != a) ? from.Weight[a] : 1f;
		return (a.preferredPoint - b.Center).magnitude * num;
	}

	private void UpdateCenter(TriangleNode current, TriangleNode end)
	{
		if (current.PathToggle != TriangleNode.MainPathToggle && !current.StartEnd)
		{
			current.PathToggle = TriangleNode.MainPathToggle;
			current.preferredPoint = Utilities.ClosestPointOnTriangle(current.Points, end.Center);
		}
	}

	private void EmitDirt(ParticleSystem dP)
	{
		if (dP != null)
		{
			for (int i = 0; i < this.Edges.Count; i++)
			{
				Vector2 pos = this.Edges[i].Pos;
				Vector2 pos2 = this.Edges[(i + 1) % this.Edges.Count].Pos;
				Vector3 a = new Vector3(pos.x, (float)(this.Floor * 2), pos.y);
				Vector3 a2 = new Vector3(pos2.x - pos.x, 0f, pos2.y - pos.y);
				Quaternion rotation = Quaternion.LookRotation(new Vector3(pos.x - pos2.x, 0f, pos.y - pos2.y));
				int num = Mathf.RoundToInt(a2.magnitude * 5f);
				float num2 = 0.5f;
				for (int j = 0; j < num; j++)
				{
					float value = UnityEngine.Random.value;
					dP.Emit(new ParticleSystem.EmitParams
					{
						position = a + a2 * value,
						velocity = Vector3.up * UnityEngine.Random.Range(2f * num2, 4f * num2) - rotation * Vector3.right * UnityEngine.Random.Range(2f * num2, 5f * num2) - rotation * Vector3.forward * (value - 0.5f) * UnityEngine.Random.Range(2f * num2, 5f * num2)
					}, 1);
				}
			}
		}
	}

	private void SetSizes()
	{
		this.UpdateBounds();
		this.Center = Utilities.GetPolygonCentroid(this.Edges);
		TeamTextScript teamText = this.TeamText;
		Vector3 vector = new Vector3(this.Center.x, (float)(this.Floor * 2 + 4), this.Center.y);
		this.TeamText.transform.position = vector;
		teamText.OrigPos = vector;
		TeamTextScript roleText = this.RoleText;
		vector = new Vector3(this.Center.x, (float)(this.Floor * 2) + 3.3f, this.Center.y);
		this.RoleText.transform.position = vector;
		roleText.OrigPos = vector;
		this.DustParticles.transform.position = new Vector3(this.Center.x, (float)(this.Floor * 2 + 1), this.Center.y);
		this.DustParticles.transform.localScale = new Vector3(this.RoomBounds.width * 2f, 1f, this.RoomBounds.height * 2f);
	}

	public void ChangeRole(int role)
	{
		Array values = Enum.GetValues(typeof(Room.RoomLimits));
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < values.Length; i++)
		{
			int a = (int)values.GetValue(i);
			num = Mathf.Min(a, num);
			num2 = Mathf.Max(a, num2);
		}
		role = Mathf.Clamp(role, num, num2);
		this.ForceRole = role;
		TextMesh tm = this.RoleText.tm;
		Room.RoomLimits roomLimits = (Room.RoomLimits)role;
		tm.text = roomLimits.ToString().Loc();
		this.RoleText.InUse = (this.ForceRole != -1);
	}

	public void Init(IEnumerable<WallEdge> s, int floor, ParticleSystem dP, bool dummy = false, bool OptimizeNow = true)
	{
		this.Dummy = dummy;
		this.Floor = floor;
		if (!dummy)
		{
			this.Edges = s.ToList<WallEdge>();
			for (int i = 0; i < this.Edges.Count; i++)
			{
				WallEdge wallEdge = this.Edges[i];
				wallEdge.Floor = floor;
			}
			if (Room.Clockwise(this.Edges))
			{
				this.Edges.Reverse();
			}
			GameSettings.Instance.sRoomManager.AddRoom(this);
		}
		this.TeamText.gameObject.SetActive(GameSettings.Instance.sRoomManager.TeamText);
		this.RoleText.gameObject.SetActive(GameSettings.Instance.sRoomManager.TeamText);
		if (this.Temperature == 0f)
		{
			this.Temperature = TimeOfDay.Instance.Temperature;
		}
		if (!dummy)
		{
			MeshFilter component = this.DirtObject.GetComponent<MeshFilter>();
			if (component.sharedMesh == null)
			{
				component.sharedMesh = new Mesh();
			}
			for (int j = 0; j < this.Edges.Count; j++)
			{
				WallEdge wallEdge2 = this.Edges[j];
				WallEdge value = this.Edges[(j + 1) % this.Edges.Count];
				wallEdge2.Links[this] = value;
			}
			if (!this.Deserialized && OptimizeNow)
			{
				this.OptimizeSegments();
			}
			this.EmitDirt(dP);
			foreach (Room room in this.Edges.SelectMany((WallEdge x) => x.Links.Keys).Distinct<Room>())
			{
				room.DirtyOuterMesh = true;
			}
			this.SetSizes();
			this.Area = Utilities.PolygonArea((from x in this.Edges
			select x.Pos).ToList<Vector2>());
			this.UpdateDirtScore(false);
			if (!this.Deserialized)
			{
				if (this.Outdoors)
				{
					GameSettings.Instance.DefaultOutdoorRoomStyle.Apply(this, null);
				}
				else
				{
					GameSettings.Instance.DefaultIndoorRoomStyle.Apply(this, null);
				}
			}
			GameSettings.Instance.sRoomManager.AddRoom(this);
			if (!this.Deserialized && this.Floor > -1 && this.Floor < 2)
			{
				this.RemoveTrees();
			}
		}
		else
		{
			this.UpdatePathNodes(false);
		}
	}

	public void RefreshEdges(List<UndoObject.UndoAction> undos, bool clone)
	{
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[i];
			WallEdge key = this.Edges[(i + 1) % this.Edges.Count];
			HashSet<WallSnap> hashSet = null;
			if (wallEdge.Children.TryGetValue(key, out hashSet))
			{
				foreach (WallSnap wallSnap in hashSet)
				{
					bool beenDestroyed = wallSnap.BeenDestroyed;
					if (!wallSnap.EdgeChanged(wallSnap.WallPosition.Keys.ToArray<WallEdge>(), clone) && undos != null && !beenDestroyed)
					{
						undos.Add(new UndoObject.UndoAction(wallSnap, false));
					}
				}
			}
		}
	}

	private void RemoveTrees()
	{
		HashSet<global::TreeInstance> hashSet = new HashSet<global::TreeInstance>();
		bool flag = false;
		foreach (global::TreeInstance treeInstance in GameSettings.Instance.TreeTree.Query(this.RoomBounds.Expand(6f, 6f)))
		{
			Vector2 pos = treeInstance.GetPos();
			StaticTree treeMesh = treeInstance.TreeMesh;
			float num = Mathf.Max(treeMesh.bounds.size.x, treeMesh.bounds.size.z);
			if (this.RoomBounds.ContainsEntirely(pos, num / 2f) && this.IsInside(pos, -num))
			{
				hashSet.Add(treeInstance);
				flag = true;
			}
		}
		if (flag)
		{
			foreach (global::TreeInstance treeInstance2 in hashSet)
			{
				treeInstance2.BelongsTo.RemoveTree(treeInstance2);
				GameSettings.Instance.Trees.Remove(treeInstance2);
				GameSettings.Instance.TreeTree.removeItem(treeInstance2);
			}
		}
	}

	private float FixRot(float rot)
	{
		rot %= 360f;
		return (rot != 360f) ? rot : 0f;
	}

	private Vector3I FixPos(Vector3I input, float rot)
	{
		return new Vector3I(input.x + ((!Mathf.Approximately(rot, 180f)) ? 0 : -1) + ((!Mathf.Approximately(rot, 0f)) ? 0 : 1), input.y, input.z + ((!Mathf.Approximately(rot, 270f)) ? 0 : 1) + ((!Mathf.Approximately(rot, 90f)) ? 0 : -1));
	}

	private static CombineInstance CombineFromMesh(MeshFilter mesh)
	{
		return new CombineInstance
		{
			mesh = mesh.sharedMesh,
			transform = mesh.transform.localToWorldMatrix
		};
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		this.RoomBounds = dictionary.Get<SRect>("RoomBounds", new SRect(0f, 0f, 0f, 0f)).ToRect();
		this.Floor = (int)dictionary["Floor"];
		this.RoomGroup = dictionary.Get<string>("RoomGroup", null);
		this.Outdoors = dictionary.Get<bool>("Outdoors", false);
		this.InsideMat = (string)dictionary["InsideMat"];
		this.OutsideMat = (string)dictionary["OutsideMat"];
		this.FloorMat = (string)dictionary["FloorMat"];
		this.InsideColor = ((SVector3)dictionary["InsideColor"]).ToColor();
		this.FloorColor = ((SVector3)dictionary["FloorColor"]).ToColor();
		this.OutsideColor = ((SVector3)dictionary["OutsideColor"]).ToColor();
		this._fenceStyle = dictionary.Get<string>("FenceStyle", "Concrete");
		this.FenceHeight = ObjectDatabase.Instance.FenceStyles.First((ObjectDatabase.FenceStyle x) => x.Name.Equals(this._fenceStyle)).Height;
		this.TempTeam = dictionary.Get<string[]>("Teams", new string[0]);
		this.Temperature = dictionary.Get<float>("Temperature", 0f);
		this.ChangeRole(dictionary.Get<int>("Role", -1));
		this.TempHeatUsageAgg = dictionary.Get<float>("TempHeatUsageAgg", 0f);
		this.TempHeatUsageMax = dictionary.Get<float>("TempHeatUsageMax", 0f);
		this.TempCoolUsageAgg = dictionary.Get<float>("TempCoolUsageAgg", 0f);
		this.TempCoolUsageAgg = dictionary.Get<float>("TempCoolUsageAgg", 0f);
		List<Vector2> list = (from x in dictionary.Get<SVector3[]>("DirtSpots", new SVector3[0])
		select x).ToList<Vector2>();
		List<float> list2 = dictionary.Get<float[]>("DirtAmount", new float[0]).ToList<float>();
		this.RefreshDirtQuad();
		for (int i = 0; i < list.Count; i++)
		{
			if (this.RoomBounds.Contains(list[i]))
			{
				this.AddNewDirt(list[i], list2[i]);
			}
		}
		this.Reservers = dictionary.Get<int>("Reservers", 0);
		this.SerializedParentRoom = dictionary.Get<uint>("ParentRoom", this.DID);
		this.SerializedChildrenRooms = dictionary.Get<uint[]>("ChildrenRooms", null);
		this._rentable = dictionary.Get<bool>("Rentable", true);
		this._playerOwned = dictionary.Get<bool>("PlayerOwned", false);
		return this;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["RoomBounds"] = (SRect)this.RoomBounds;
		dictionary["Floor"] = this.Floor;
		dictionary["RoomGroup"] = this.RoomGroup;
		dictionary["InsideMat"] = this.InsideMat;
		dictionary["OutsideMat"] = this.OutsideMat;
		dictionary["FloorMat"] = this.FloorMat;
		dictionary["InsideColor"] = this.InsideColor;
		dictionary["FloorColor"] = this.FloorColor;
		dictionary["OutsideColor"] = this.OutsideColor;
		dictionary["TempHeatUsageAgg"] = this.TempHeatUsageAgg;
		dictionary["TempHeatUsageMax"] = this.TempHeatUsageMax;
		dictionary["TempCoolUsageAgg"] = this.TempCoolUsageAgg;
		dictionary["TempCoolUsageAgg"] = this.TempCoolUsageAgg;
		if (mode == GameReader.LoadMode.Full)
		{
			dictionary["Teams"] = (from x in this.Teams
			select x.Name).ToArray<string>();
			dictionary["Role"] = this.ForceRole;
			dictionary["Temperature"] = this.Temperature;
			dictionary["DirtSpots"] = (from x in this.Dirts
			select x.Pos).ToArray<SVector3>();
			dictionary["DirtAmount"] = (from x in this.Dirts
			select x.Amount).ToArray<float>();
			dictionary["Reservers"] = this.Reservers;
		}
		dictionary["Outdoors"] = this.Outdoors;
		dictionary["FenceStyle"] = this.FenceStyle;
		dictionary["ParentRoom"] = ((!(this.ParentRoom == null)) ? this.ParentRoom.DID : this.DID);
		dictionary["ChildrenRooms"] = (from x in this.ChildrenRooms
		select x.DID).ToArray<uint>();
		dictionary["Rentable"] = this.Rentable;
		dictionary["PlayerOwned"] = this.PlayerOwned;
	}

	public override void PostDeserialize()
	{
		if (this.SerializedChildrenRooms != null)
		{
			this.ParentRoom = ((base.GetDeserializedObject(this.SerializedParentRoom) as Room) ?? this);
			this.ChildrenRooms = (from x in this.SerializedChildrenRooms
			select base.GetDeserializedObject(x) as Room into x
			where x != null
			select x).ToList<Room>();
		}
	}

	public override string WriteName()
	{
		return "Room";
	}

	public float GetEnvironment()
	{
		return Mathf.Clamp(this.FurnEnvironment * this.DirtScore, 0f, 2f);
	}

	public void CopyOverSettings(Room other)
	{
		other.Outdoors = this.Outdoors;
		other.SetFenceStyle(this.FenceStyle, null);
		other.FloorMat = this.FloorMat;
		other.FloorColor = this.FloorColor;
		other.InsideMat = this.InsideMat;
		other.InsideColor = this.InsideColor;
		other.OutsideMat = this.OutsideMat;
		other.OutsideColor = this.OutsideColor;
		other.UpdateTeams(this.Teams);
		other.ForceRole = this.ForceRole;
		other.UnGroup();
		other._rentable = this.Rentable;
		other._playerOwned = this.PlayerOwned;
		if (this.ParentRoom != this || this.ChildrenRooms.Count > 0)
		{
			other.ParentRoom = this.ParentRoom;
			this.ParentRoom.ChildrenRooms.Add(other);
		}
	}

	private void RefreshDirtQuad()
	{
		this.DirtTree = new QuadTree<Room.Dirt>(this.RoomBounds.Expand(1f, 1f), 1f, 4, 8);
		for (int i = 0; i < this.Dirts.Count; i++)
		{
			this.DirtTree.Insert(this.Dirts[i]);
		}
	}

	public List<Room.Dirt> QueryDirtQuad(Rect query)
	{
		if (this.DirtTree != null)
		{
			this.DirtTree.Query(query, QuadTree<Room.Dirt>.NonThreadSafeResult);
			return QuadTree<Room.Dirt>.NonThreadSafeResult;
		}
		return this.Dirts;
	}

	public float AddDirt(float x, float y, float amount = 0.5f, Vector2? dir = null)
	{
		if (amount > 0f && (this.DisableDirt || (GameSettings.Instance.RentMode && (!this.PlayerOwned || !this.Rentable))))
		{
			return amount;
		}
		Vector2 vector = new Vector2(x, y);
		Room.Dirt dirt = null;
		Rect query = new Rect(x - 1f, y - 1f, 2f, 2f);
		List<Room.Dirt> list = this.QueryDirtQuad(query);
		for (int i = 0; i < list.Count; i++)
		{
			Room.Dirt dirt2 = list[i];
			if ((amount < 0f || dirt2.Amount < 1f) && (vector - dirt2.Pos).sqrMagnitude <= ((amount <= 0f) ? 1f : 0.25f))
			{
				dirt = dirt2;
				break;
			}
		}
		if (dirt == null && amount > 0f)
		{
			if (!this.FixDirtPos(ref vector, dir))
			{
				return 0f;
			}
			query = new Rect(vector.x - 1f, vector.y - 1f, 2f, 2f);
			list = this.QueryDirtQuad(query);
			for (int j = 0; j < list.Count; j++)
			{
				if (list[j].Amount < 1f && (vector - list[j].Pos).sqrMagnitude <= 0.25f)
				{
					dirt = list[j];
					break;
				}
			}
		}
		if (dirt == null && amount > 0f)
		{
			this.AddNewDirt(vector, amount);
			this.UpdateDirtScore(true);
			return amount;
		}
		if (dirt == null)
		{
			return 0f;
		}
		if (amount > 0f)
		{
			if (dirt.Amount + amount > 1f)
			{
				float result = 1f - dirt.Amount;
				dirt.Amount = 1f;
				this.UpdateDirtTrans(dirt.Index);
				this.UpdateDirtScore(true);
				return result;
			}
			dirt.Amount += amount;
			this.UpdateDirtTrans(dirt.Index);
			this.UpdateDirtScore(true);
			return amount;
		}
		else
		{
			if (dirt.Amount + amount <= 0f)
			{
				this.RemoveDirt(dirt.Index);
				this.UpdateDirtScore(true);
				return 0f;
			}
			dirt.Amount += amount;
			this.UpdateDirtTrans(dirt.Index);
			this.UpdateDirtScore(true);
			return amount;
		}
	}

	public float GetDirt(float x, float y)
	{
		Vector2 a = new Vector2(x, y);
		List<Room.Dirt> list = this.QueryDirtQuad(new Rect(x - 1f, y - 1f, 2f, 2f));
		for (int i = 0; i < list.Count; i++)
		{
			if ((a - list[i].Pos).sqrMagnitude <= 1f)
			{
				return list[i].Amount;
			}
		}
		return 0f;
	}

	private void FurnitureValidCheck(List<UndoObject.UndoAction> undos)
	{
		List<Furniture> list = this._furnitures.ToList<Furniture>();
		for (int i = 0; i < list.Count; i++)
		{
			Furniture furniture = list[i];
			if (this.Outdoors)
			{
				if (!furniture.ValidOutdoors)
				{
					if (undos != null && !furniture.BeenDestroyed)
					{
						undos.Add(new UndoObject.UndoAction(furniture, false));
					}
					furniture.DestroyMe();
				}
			}
			else if (!furniture.ValidIndoors)
			{
				if (undos != null && !furniture.BeenDestroyed)
				{
					undos.Add(new UndoObject.UndoAction(furniture, false));
				}
				furniture.DestroyMe();
			}
		}
	}

	public void UpdateDirtScore(bool warning = true)
	{
		float num = 0f;
		for (int i = 0; i < this.Dirts.Count; i++)
		{
			num += this.Dirts[i].Amount;
		}
		num /= this.Area * 0.8f;
		this.DisableDirt = (num > 2f);
		this.DirtScore = Mathf.Clamp01(1f - num);
		if (warning && this.DirtScore < 0.1f)
		{
			HUD.Instance.AddPopupMessage("RoomDirtyWarning2".Loc(), "Exclamation", PopupManager.PopUpAction.GotoRoom, this.DID, PopupManager.NotificationSound.Issue, 0f, PopupManager.PopupIDs.Dirt, 1);
		}
	}

	private void UpdateDirtTrans(int num)
	{
		MeshFilter component = this.DirtObject.GetComponent<MeshFilter>();
		Vector2[] uv = component.sharedMesh.uv2;
		for (int i = 0; i < 4; i++)
		{
			uv[num * 4 + i] = new Vector2(this.Dirts[num].Amount, 0f);
		}
		component.sharedMesh.uv2 = uv;
	}

	public void ClearDirt()
	{
		int count = this.Dirts.Count;
		for (int i = 0; i < count; i++)
		{
			this.RemoveDirt(0);
		}
	}

	public void RefreshDirtNavmesh()
	{
		for (int i = 0; i < this.Dirts.Count; i++)
		{
			if (this.GetNodeAt(this.Dirts[i].Pos) == null)
			{
				this.RemoveDirt(i);
				i--;
			}
		}
		this.UpdateDirtScore(true);
	}

	public void RemoveDirtAt(Vector2 vec)
	{
		for (int i = 0; i < this.Dirts.Count; i++)
		{
			if ((vec - this.Dirts[i].Pos).sqrMagnitude <= 1f)
			{
				this.RemoveDirt(i);
				i--;
			}
		}
	}

	public bool AllowedInRoom(Actor act)
	{
		if (act.AItype != AI<Actor>.AIType.Employee)
		{
			return true;
		}
		if (GameSettings.Instance.RentMode && this.Rentable && !this.PlayerOwned)
		{
			return false;
		}
		if (this.Teams.Count == 0)
		{
			return this.ForceRole < 0 || (Employee.RoleToMask[this.ForceRole] & act.GetRole()) > Employee.RoleBit.None;
		}
		if (this.ForceRole < 0)
		{
			return this.CompatibleWithTeam(act.GetTeam());
		}
		return (Employee.RoleToMask[this.ForceRole] & act.GetRole()) > Employee.RoleBit.None && this.CompatibleWithTeam(act.GetTeam());
	}

	public int OrderByRole(int role)
	{
		if (role < 0)
		{
			return (this.ForceRole != role) ? 1 : 0;
		}
		if (this.ForceRole < 0)
		{
			return 1;
		}
		return ((this.ForceRole & role) <= 0) ? 1 : 0;
	}

	public bool AllowedInRoom(Team team, Employee.RoleBit role)
	{
		if (GameSettings.Instance.RentMode && this.Rentable && !this.PlayerOwned)
		{
			return false;
		}
		if (this.Teams.Count == 0)
		{
			return this.ForceRole < 0 || (Employee.RoleToMask[this.ForceRole] & role) > Employee.RoleBit.None;
		}
		if (this.ForceRole < 0)
		{
			return this.CompatibleWithTeam(team);
		}
		return (Employee.RoleToMask[this.ForceRole] & role) > Employee.RoleBit.None && this.CompatibleWithTeam(team);
	}

	public void RemoveDirt(int num)
	{
		if (this.DirtTree != null)
		{
			this.DirtTree.removeItem(this.Dirts[num]);
		}
		this.Dirts.RemoveAt(num);
		for (int i = num; i < this.Dirts.Count; i++)
		{
			this.Dirts[i].Index--;
		}
		MeshFilter component = this.DirtObject.GetComponent<MeshFilter>();
		List<Vector3> list = new List<Vector3>();
		List<Vector2> list2 = new List<Vector2>();
		List<Vector2> list3 = new List<Vector2>();
		List<int> list4 = new List<int>();
		List<Vector3> list5 = new List<Vector3>();
		component.sharedMesh.GetVertices(list);
		component.sharedMesh.GetUVs(0, list2);
		component.sharedMesh.GetUVs(1, list3);
		component.sharedMesh.GetTriangles(list4, 0);
		component.sharedMesh.GetNormals(list5);
		for (int j = 0; j < 4; j++)
		{
			list2.RemoveAt(num * 4);
			list3.RemoveAt(num * 4);
			list.RemoveAt(num * 4);
			list5.RemoveAt(num * 4);
		}
		for (int k = num * 6 + 6; k < list4.Count; k++)
		{
			List<int> list6;
			int index;
			(list6 = list4)[index = k] = list6[index] - 4;
		}
		for (int l = 0; l < 6; l++)
		{
			list4.RemoveAt(num * 6);
		}
		component.sharedMesh.SetTriangles(list4, 0);
		component.sharedMesh.SetVertices(list);
		component.sharedMesh.SetUVs(0, list2);
		component.sharedMesh.SetUVs(1, list3);
		component.sharedMesh.SetNormals(list5);
	}

	private bool FixDirtPos(ref Vector2 v, Vector2? dir)
	{
		List<Room.Dirt> list = this.QueryDirtQuad(new Rect(v.x - 1f, v.y - 1f, 2f, 2f));
		for (int i = 0; i < list.Count; i++)
		{
			Vector2 vector = v - list[i].Pos;
			if (vector.sqrMagnitude < 1f)
			{
				v += ((dir == null) ? vector.normalized : dir.Value.normalized) * UnityEngine.Random.value;
				break;
			}
		}
		TriangleNode nodeAt = this.GetNodeAt(v);
		if (nodeAt == null)
		{
			return false;
		}
		for (int j = 0; j < 3; j++)
		{
			if (nodeAt.Connections[j] == null)
			{
				Vector2? vector2 = Utilities.ProjectToLine(v, nodeAt.Points[j], nodeAt.Points[(j + 1) % 3]);
				if (vector2 == null)
				{
					return false;
				}
				Vector2 vector3 = v - vector2.Value;
				float num = 0.5f - vector3.magnitude;
				if (num > 0f)
				{
					v += vector3.normalized * num;
				}
			}
		}
		for (int k = 0; k < this.Edges.Count; k++)
		{
			Vector2 pos = this.Edges[k].Pos;
			Vector2 pos2 = this.Edges[(k + 1) % this.Edges.Count].Pos;
			Vector2? vector4 = Utilities.ProjectToLine(v, pos, pos2);
			if (vector4 != null)
			{
				Vector2 vector5 = v - vector4.Value;
				float num2 = 0.5f - vector5.magnitude;
				if (num2 > 0f)
				{
					v += vector5.normalized * num2;
				}
			}
		}
		return this.DirtTree == null || this.DirtTree.Rectangle.Contains(v);
	}

	private Vector2 FindClosestWallPos(Vector2 p, Vector2 Def)
	{
		float num = float.MaxValue;
		Vector2 result = Def;
		for (int i = 0; i < this.Edges.Count; i++)
		{
			int index = (i + 1) % this.Edges.Count;
			Vector2 pos = this.Edges[i].Pos;
			Vector2 pos2 = this.Edges[index].Pos;
			Vector2? vector = Utilities.ProjectToLine(p, pos, pos2);
			if (vector != null)
			{
				float sqrMagnitude = (vector.Value - p).sqrMagnitude;
				if (sqrMagnitude < num)
				{
					result = vector.Value;
					num = sqrMagnitude;
				}
			}
		}
		return result;
	}

	private void AddNewDirt(Vector2 pos, float amount)
	{
		Room.Dirt dirt = new Room.Dirt(pos, amount, this.Dirts.Count);
		if (this.DirtTree != null && !this.DirtTree.Insert(dirt))
		{
			return;
		}
		this.Dirts.Add(dirt);
		MeshFilter component = this.DirtObject.GetComponent<MeshFilter>();
		if (component.sharedMesh == null)
		{
			component.sharedMesh = new Mesh();
			component.sharedMesh.MarkDynamic();
		}
		List<Vector3> list = new List<Vector3>();
		List<Vector2> list2 = new List<Vector2>();
		List<Vector2> list3 = new List<Vector2>();
		List<int> list4 = new List<int>();
		List<Vector3> list5 = new List<Vector3>();
		component.sharedMesh.GetVertices(list);
		component.sharedMesh.GetUVs(0, list2);
		component.sharedMesh.GetUVs(1, list3);
		component.sharedMesh.GetTriangles(list4, 0);
		component.sharedMesh.GetNormals(list5);
		Quaternion rotation = Quaternion.Euler(0f, (float)UnityEngine.Random.Range(0, 360), 0f);
		Vector3 a = new Vector3(pos.x, (float)(this.Floor * 2), pos.y);
		float num = (float)UnityEngine.Random.Range(0, 2) / 2f;
		float num2 = (float)UnityEngine.Random.Range(0, 2) / 2f;
		list.Add(a + rotation * new Vector3(-0.5f, 0f, -0.5f));
		list.Add(a + rotation * new Vector3(0.5f, 0f, -0.5f));
		list.Add(a + rotation * new Vector3(0.5f, 0f, 0.5f));
		list.Add(a + rotation * new Vector3(-0.5f, 0f, 0.5f));
		list4.Add(list.Count - 2);
		list4.Add(list.Count - 3);
		list4.Add(list.Count - 4);
		list4.Add(list.Count - 4);
		list4.Add(list.Count - 1);
		list4.Add(list.Count - 2);
		list2.Add(new Vector2(num, num2));
		list2.Add(new Vector2(num + 0.5f, num2));
		list2.Add(new Vector2(num + 0.5f, num2 + 0.5f));
		list2.Add(new Vector2(num, num2 + 0.5f));
		list3.Add(new Vector2(amount, 0f));
		list3.Add(new Vector2(amount, 0f));
		list3.Add(new Vector2(amount, 0f));
		list3.Add(new Vector2(amount, 0f));
		list5.Add(Vector3.up);
		list5.Add(Vector3.up);
		list5.Add(Vector3.up);
		list5.Add(Vector3.up);
		component.sharedMesh.SetVertices(list);
		component.sharedMesh.SetTriangles(list4, 0);
		component.sharedMesh.SetUVs(0, list2);
		component.sharedMesh.SetUVs(1, list3);
		component.sharedMesh.SetNormals(list5);
	}

	private List<int> GetPathNodeCounts()
	{
		List<int> list = new List<int>(this.PathNodes.Count + 1);
		list.Add(this.PathNodes.Count);
		for (int i = 0; i < this.PathNodes.Count; i++)
		{
			PathNode<Vector3> pathNode = this.PathNodes[i];
			list.Add(pathNode.GetConnections().Count);
		}
		return list;
	}

	public IEnumerable<IRoomConnector> GetConnectors()
	{
		foreach (IRoomConnector item in from x in this.GetSegments(null).OfType<IRoomConnector>()
		where x.IsConnecter
		select x)
		{
			yield return item;
		}
		foreach (IRoomConnector item2 in from x in this._furnitures.OfType<IRoomConnector>()
		where x.IsConnecter
		select x)
		{
			yield return item2;
		}
		yield break;
	}

	private void UpdatePathNodes(bool recalculateGraph = true)
	{
		if (GameSettings.Instance == null || HUD.Instance == null || this.NavMap == null)
		{
			return;
		}
		this.CanClean = true;
		this.DirtyPathNodes = false;
		this.CachedPaths.Clear();
		object pathNodes = this.PathNodes;
		lock (pathNodes)
		{
			List<int> pathNodeCounts = this.GetPathNodeCounts();
			for (int i = 0; i < this.PathNodes.Count; i++)
			{
				PathNode<Vector3> pathNode = this.PathNodes[i];
				foreach (PathNode<Vector3> pathNode2 in pathNode.GetConnections())
				{
					pathNode2.RemoveConnection(pathNode);
				}
			}
			for (int j = 0; j < this.SubNodes.Count; j++)
			{
				PathNode<Vector3> pathNode3 = this.SubNodes[j];
				foreach (PathNode<Vector3> pathNode4 in pathNode3.GetConnections())
				{
					pathNode4.RemoveConnection(pathNode3);
				}
			}
			this.SubNodes.Clear();
			this.PathNodes.Clear();
			List<IRoomConnector> list = (from x in this.GetConnectors()
			where x.pathNode != null
			select x).ToList<IRoomConnector>();
			if (list.Count == 0)
			{
				PathNode<Vector3> pathNode5 = new PathNode<Vector3>(Vector3.zero, this);
				pathNode5.Tag2 = pathNode5;
				this.PathNodes.Add(pathNode5);
				if (recalculateGraph)
				{
					GameSettings.Instance.sRoomManager.RoomNearnessDirty = true;
				}
			}
			else
			{
				List<List<IRoomConnector>> list2 = new List<List<IRoomConnector>>();
				list2.Add(new List<IRoomConnector>
				{
					list[0]
				});
				list[0].UpdateBlocked();
				for (int k = 1; k < list.Count; k++)
				{
					list[k].UpdateBlocked();
					bool flag = false;
					for (int l = 0; l < list2.Count; l++)
					{
						if (this.NodesAccessible(list[k], list2[l][0]))
						{
							list2[l].Add(list[k]);
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						list2.Add(new List<IRoomConnector>
						{
							list[k]
						});
					}
				}
				for (int m = 0; m < list2.Count; m++)
				{
					PathNode<Vector3> pathNode6;
					if (this.Dummy)
					{
						pathNode6 = new PathNode<Vector3>(Vector3.zero, this);
						pathNode6.NullWeight = true;
					}
					else
					{
						pathNode6 = new PathNode<Vector3>(new Vector3(this.Center.x, (float)(this.Floor * 2) + 0.5f, this.Center.y), this);
					}
					for (int n = 0; n < list2[m].Count; n++)
					{
						IRoomConnector roomConnector = list2[m][n];
						roomConnector.pathNode.AddConnection(pathNode6);
						pathNode6.AddConnection(roomConnector.pathNode);
					}
					pathNode6.Tag2 = pathNode6;
					this.PathNodes.Add(pathNode6);
				}
				this.SubdividePathnodes();
				if (recalculateGraph)
				{
					List<int> pathNodeCounts2 = this.GetPathNodeCounts();
					if (pathNodeCounts2.Count == pathNodeCounts.Count)
					{
						bool flag2 = false;
						for (int num = 0; num < pathNodeCounts.Count; num++)
						{
							if (pathNodeCounts[num] != pathNodeCounts2[num])
							{
								flag2 = true;
								break;
							}
						}
						if (!flag2)
						{
							return;
						}
					}
					GameSettings.Instance.sRoomManager.RoomNearnessDirty = true;
				}
			}
		}
	}

	private void SubdividePathnodes()
	{
		for (int i = 0; i < this.PathNodes.Count; i++)
		{
			PathNode<Vector3>[] array = this.PathNodes[i].GetConnections().ToArray<PathNode<Vector3>>();
			for (int j = 0; j < array.Length; j++)
			{
				for (int k = j + 1; k < array.Length; k++)
				{
					PathNode<Vector3> pathNode = new PathNode<Vector3>((array[j].Point + array[k].Point) * 0.5f, this);
					pathNode.Tag2 = this.PathNodes[i];
					this.SubNodes.Add(pathNode);
					pathNode.AddConnection(array[j]);
					pathNode.AddConnection(array[k]);
					array[j].AddConnection(pathNode);
					array[k].AddConnection(pathNode);
				}
			}
		}
	}

	public PathNode<Vector3> GetFirstPathNode()
	{
		return (this.PathNodes.Count != 0) ? this.PathNodes[0] : new PathNode<Vector3>(Vector3.zero, this);
	}

	public PathNode<Vector3> GetAvailableNode(Vector3 pos)
	{
		if (this.PathNodes.Count == 0)
		{
			return new PathNode<Vector3>(Vector3.zero, this);
		}
		if (this.PathNodes.Count == 1)
		{
			return this.PathNodes[0];
		}
		List<Vector3> list = new List<Vector3>();
		for (int i = 0; i < this.PathNodes.Count; i++)
		{
			PathNode<Vector3> pathNode = this.PathNodes[i];
			IRoomConnector roomConnector = (IRoomConnector)pathNode.GetConnections().First<PathNode<Vector3>>().Tag;
			if (!roomConnector.IsNull)
			{
				Vector3 offsetPos = roomConnector.GetOffsetPos(this, false);
				if (this.FindPath(pos, offsetPos, ref list, null, roomConnector))
				{
					return pathNode;
				}
				list.Clear();
			}
		}
		return new PathNode<Vector3>(Vector3.zero, this);
	}

	private bool NodesAccessible(IRoomConnector d1, IRoomConnector d2)
	{
		Vector3 offsetPos = d1.GetOffsetPos(this, false);
		Vector3 offsetPos2 = d2.GetOffsetPos(this, false);
		List<Vector3> list = new List<Vector3>();
		return this.FindPath(offsetPos, offsetPos2, ref list, d1, d2);
	}

	public void CacheAllPaths()
	{
		List<IRoomConnector> list = (from x in this.GetConnectors()
		where x.pathNode != null
		select x).ToList<IRoomConnector>();
		List<Vector3> list2 = new List<Vector3>();
		for (int i = 0; i < list.Count; i++)
		{
			IRoomConnector roomConnector = list[i];
			for (int j = i + 1; j < list.Count; j++)
			{
				IRoomConnector roomConnector2 = list[j];
				this.FindPath(roomConnector.GetOffsetPos(this, false), roomConnector2.GetOffsetPos(this, false), ref list2, roomConnector, roomConnector2);
				list2.Clear();
			}
		}
	}

	private void Awake()
	{
		base.InitWritable();
	}

	private void OnDrawGizmos()
	{
		if (!this.Dummy && Example.EnableNearness)
		{
			Room room = (from x in SelectorController.Instance.Selected
			select x.GetComponent<Room>()).FirstOrDefault((Room x) => x != null);
			if (room != null)
			{
				List<KeyValuePair<Room, int>> connectedRooms = GameSettings.Instance.sRoomManager.GetConnectedRooms(room);
				KeyValuePair<Room, int> keyValuePair = connectedRooms.FirstOrDefault((KeyValuePair<Room, int> x) => x.Key == this);
				if (keyValuePair.Key != null && this.FloorMesh != null)
				{
					Gizmos.color = Color.Lerp(new Color(0f, 1f, 0f, 0.5f), new Color(1f, 0f, 0f, 0.5f), (float)keyValuePair.Value / 10f);
					Gizmos.DrawMesh(this.FloorMesh.GetComponent<MeshFilter>().mesh, 0, base.transform.position + Vector3.up * (float)(this.Floor * 2 + 1));
					Gizmos.color = Color.white;
				}
			}
		}
		if (this.Floor == GameSettings.Instance.ActiveFloor && !Example.CachedPaths)
		{
			Color[] array = new Color[]
			{
				Color.red,
				Color.green,
				Color.blue
			};
			int num = 0;
			foreach (PathNode<Vector3> pathNode in this.PathNodes)
			{
				Gizmos.color = array[num % array.Length];
				foreach (PathNode<Vector3> pathNode2 in pathNode.GetConnections())
				{
					Gizmos.DrawLine(pathNode.Point, pathNode2.Point);
				}
				num++;
			}
		}
		Gizmos.color = Color.white;
	}

	public void UpdateBounds()
	{
		float num = float.MaxValue;
		float num2 = float.MaxValue;
		float num3 = float.MinValue;
		float num4 = float.MinValue;
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[i];
			if (wallEdge.Pos.x < num)
			{
				num = wallEdge.Pos.x;
			}
			if (wallEdge.Pos.y < num2)
			{
				num2 = wallEdge.Pos.y;
			}
			if (wallEdge.Pos.x > num3)
			{
				num3 = wallEdge.Pos.x;
			}
			if (wallEdge.Pos.y > num4)
			{
				num4 = wallEdge.Pos.y;
			}
		}
		this.RoomBounds = new Rect(num, num2, num3 - num, num4 - num2);
	}

	private int isLeft(Vector2 p1, Vector2 p2, Vector2 p3)
	{
		float num = (p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y);
		if (num > 0f)
		{
			return 1;
		}
		if (num < 0f)
		{
			return -1;
		}
		return 0;
	}

	public static float LeftVal(Vector2 p1, Vector2 p2, Vector2 p3)
	{
		Vector2 vector = new Vector2(p2.x - p1.x, p2.y - p1.y);
		Vector2 vector2 = new Vector2(p3.x - p2.x, p3.y - p2.y);
		float num = Mathf.Atan2(vector.y, vector.x) - Mathf.Atan2(vector2.y, vector2.x);
		if (num > 3.14159274f)
		{
			num -= 6.28318548f;
		}
		if (num < -3.14159274f)
		{
			num += 6.28318548f;
		}
		return num;
	}

	public bool IsInside(Vector3 p)
	{
		return this.IsInside(new Vector2(p.x, p.z));
	}

	public Vector2[] GetExpanded(float expansion)
	{
		Vector2[] array = new Vector2[this.Edges.Count];
		int i = 0;
		WallEdge wallEdge = this.Edges[0];
		WallEdge wallEdge2 = wallEdge;
		while (i < this.Edges.Count)
		{
			WallEdge wallEdge3 = wallEdge.Links[this];
			WallEdge third = wallEdge3.Links[this];
			array[i] = Room.GetOffset(wallEdge, wallEdge3, third, expansion, true);
			wallEdge = wallEdge3;
			i++;
			if (wallEdge == wallEdge2)
			{
				return array;
			}
		}
		return this.BrokenWhileLoop<Vector2[]>(() => this.GetExpanded(expansion), array);
	}

	private void BrokenWhileLoop(Action Retry)
	{
		if (!this.HasTriedFix)
		{
			this.HasTriedFix = true;
			if (!this.TryFixEdges())
			{
				Debug.LogException(new Exception("Room has less than 3 walls after fix"), this);
				return;
			}
			Debug.LogException(new Exception("Ignore:Broken while loop"), this);
			Retry();
		}
		else
		{
			Debug.LogException(new Exception("Broken while loop after trying fix"), this);
		}
	}

	private T BrokenWhileLoop<T>(Func<T> Retry, T def)
	{
		if (this.HasTriedFix)
		{
			Debug.LogException(new Exception("Broken while loop after trying fix"), this);
			return def;
		}
		this.HasTriedFix = true;
		if (!this.TryFixEdges())
		{
			Debug.LogException(new Exception("Room has less than 3 walls after fix"), this);
			return def;
		}
		Debug.LogException(new Exception("Ignore:Broken while loop"), this);
		return Retry();
	}

	public bool TryFixEdges()
	{
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[i];
			WallEdge wallEdge2 = this.Edges[(i + 1) % this.Edges.Count];
			if (wallEdge == wallEdge2)
			{
				this.Edges.RemoveAt(i);
				i--;
			}
			else
			{
				wallEdge.Links[this] = wallEdge2;
			}
		}
		if (this.Edges.Count < 3)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return false;
		}
		return true;
	}

	public bool IsInside(Vector2 p, float expansion)
	{
		if (this.Edges == null || this.Edges.Count == 0)
		{
			return false;
		}
		if (p.x < this.RoomBounds.xMin + expansion - 0.01f || p.x > this.RoomBounds.xMax - expansion + 0.01f || p.y < this.RoomBounds.yMin + expansion - 0.01f || p.y > this.RoomBounds.yMax - expansion + 0.01f)
		{
			return false;
		}
		int num = 0;
		int num2 = 0;
		WallEdge wallEdge = this.Edges[0];
		WallEdge wallEdge2 = wallEdge;
		WallEdge wallEdge3 = wallEdge.Links[this];
		WallEdge third = wallEdge3.Links[this];
		Vector2 p2 = (expansion != 0f) ? Room.GetOffset(wallEdge, wallEdge3, third, expansion, true) : wallEdge3.Pos;
		for (;;)
		{
			WallEdge wallEdge4 = wallEdge.Links[this];
			WallEdge wallEdge5 = wallEdge4.Links[this];
			WallEdge third2 = wallEdge5.Links[this];
			Vector2 vector = (expansion != 0f) ? Room.GetOffset(wallEdge4, wallEdge5, third2, expansion, true) : wallEdge5.Pos;
			if (p2.y <= p.y)
			{
				if (vector.y > p.y && this.isLeft(p2, vector, p) > 0)
				{
					num2++;
				}
			}
			else if (vector.y <= p.y && this.isLeft(p2, vector, p) < 0)
			{
				num2--;
			}
			wallEdge = wallEdge4;
			num++;
			if (num > this.Edges.Count * 2)
			{
				break;
			}
			p2 = vector;
			if (wallEdge == wallEdge2)
			{
				goto Block_13;
			}
		}
		return this.BrokenWhileLoop<bool>(() => this.IsInside(p, expansion), false);
		Block_13:
		return num2 != 0;
	}

	public bool IsInside(Vector2 p)
	{
		if (this.Edges == null || this.Edges.Count == 0)
		{
			return false;
		}
		if (p.x < this.RoomBounds.xMin - 0.01f || p.x > this.RoomBounds.xMax + 0.01f || p.y < this.RoomBounds.yMin - 0.01f || p.y > this.RoomBounds.yMax + 0.01f)
		{
			return false;
		}
		int num = 0;
		int num2 = 0;
		WallEdge wallEdge = this.Edges[0];
		WallEdge wallEdge2 = wallEdge;
		for (;;)
		{
			WallEdge wallEdge3;
			if (!wallEdge.Links.TryGetValue(this, out wallEdge3))
			{
				if (!this.TryFixEdges())
				{
					break;
				}
				if (!wallEdge.Links.TryGetValue(this, out wallEdge3))
				{
					return false;
				}
			}
			if (wallEdge.Pos.y <= p.y)
			{
				if (wallEdge3.Pos.y > p.y && this.isLeft(wallEdge.Pos, wallEdge3.Pos, p) > 0)
				{
					num2++;
				}
			}
			else if (wallEdge3.Pos.y <= p.y && this.isLeft(wallEdge.Pos, wallEdge3.Pos, p) < 0)
			{
				num2--;
			}
			wallEdge = wallEdge3;
			num++;
			if (num > this.Edges.Count * 2)
			{
				goto Block_13;
			}
			if (wallEdge == wallEdge2)
			{
				goto Block_14;
			}
		}
		return false;
		Block_13:
		return this.BrokenWhileLoop<bool>(() => this.IsInside(p), false);
		Block_14:
		return num2 != 0;
	}

	public bool IsStrictlyInside(Vector2 p)
	{
		bool flag = false;
		bool flag2 = false;
		for (int i = 0; i < this.Edges.Count; i++)
		{
			Vector2 pos = this.Edges[i].Pos;
			Vector2 pos2 = this.Edges[(i + 1) % this.Edges.Count].Pos;
			if (this.IsValidInsideCheck(p, pos, pos2))
			{
				if (this.IsBetween(p.y, pos.y, pos2.y))
				{
					flag2 = !flag2;
				}
				else if (pos.y == p.y && flag && flag)
				{
					flag2 = !flag2;
				}
				flag = (pos2.y == p.y);
			}
		}
		return flag2;
	}

	private bool IsValidInsideCheck(Vector2 p, Vector2 a, Vector2 b)
	{
		if (a.x < p.x && b.x < p.x)
		{
			return true;
		}
		if (this.IsBetween(p.x, a.x, b.x))
		{
			float num = (b.y - a.y) / (b.x - a.x);
			float num2 = -num * a.x + a.y;
			float num3 = (p.y - num2) / num;
			return p.x > num3;
		}
		return false;
	}

	private bool IsBetween(float x, float a, float b)
	{
		if (a > b)
		{
			return x > b && x < a;
		}
		return x > a && x < b;
	}

	public bool IsInsideBounds(Vector2 p, float offset)
	{
		return p.x >= this.RoomBounds.xMin - offset && p.x <= this.RoomBounds.xMax + offset && p.y >= this.RoomBounds.yMin - offset && p.y <= this.RoomBounds.yMax + offset;
	}

	public int LinkBack(WallEdge s, WallEdge go, Room r2, bool tryAgain)
	{
		int num = 0;
		while (go.Links.ContainsKey(r2))
		{
			num++;
			go = go.Links[r2];
			if (go == s)
			{
				if (tryAgain && num > 1)
				{
					num = this.LinkBack(s, s, r2, false) - 1;
					if (num > 1)
					{
						num = this.LinkBack(s, s.Links[r2], r2, false) - 1;
					}
				}
				return num;
			}
		}
		return 0;
	}

	public bool CanMerge(Room other)
	{
		if (this.Floor != other.Floor)
		{
			return false;
		}
		int num = 0;
		int num2 = 0;
		bool flag = this.Edges.First<WallEdge>().Links.ContainsKey(other);
		foreach (WallEdge wallEdge in this.Edges)
		{
			if (num2 % 2 == 1)
			{
				if (!wallEdge.Links.ContainsKey(other))
				{
					num2++;
				}
			}
			else if (wallEdge.Links.ContainsKey(other))
			{
				num2++;
			}
			if (wallEdge.Links.ContainsKey(other) && wallEdge.Links[other].Links.ContainsKey(this) && wallEdge.Links[other].Links[this] == wallEdge)
			{
				num++;
			}
		}
		if (flag)
		{
			num2--;
		}
		if (num2 > 2)
		{
			return false;
		}
		num2 = 0;
		flag = other.Edges.First<WallEdge>().Links.ContainsKey(this);
		foreach (WallEdge wallEdge2 in other.Edges)
		{
			if (num2 % 2 == 1)
			{
				if (!wallEdge2.Links.ContainsKey(this))
				{
					num2++;
				}
			}
			else if (wallEdge2.Links.ContainsKey(this))
			{
				num2++;
			}
		}
		if (flag)
		{
			num2--;
		}
		return num2 <= 2 && num != 0;
	}

	public List<Vector2> MergeWith(Room other, Dictionary<WallSnap, UndoObject.UndoAction> snaps, List<UndoObject.UndoAction> undos)
	{
		List<WallEdge> list = this.Edges.Concat(other.Edges).Distinct<WallEdge>().ToList<WallEdge>();
		Dictionary<WallEdge, WallEdge> dictionary = this.Edges.ToDictionary((WallEdge x) => x.Links[this], (WallEdge x) => x);
		int num = 0;
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[i];
			if (!wallEdge.Links.ContainsKey(other) || !wallEdge.Links[other].Links.ContainsKey(this))
			{
				num = i;
				break;
			}
		}
		List<Vector2> list2 = new List<Vector2>();
		bool flag = true;
		for (int j = 0; j < this.Edges.Count; j++)
		{
			WallEdge wallEdge2 = this.Edges[(j + num) % this.Edges.Count];
			if (wallEdge2.Links.ContainsKey(other) && wallEdge2.Links[other].Links.ContainsKey(this))
			{
				WallEdge wallEdge3 = dictionary[wallEdge2];
				while (wallEdge3.Links.ContainsKey(other))
				{
					if (wallEdge3.Links[other].Links.ContainsKey(this) && wallEdge3 == wallEdge3.Links[other].Links[this])
					{
						break;
					}
					if (flag)
					{
						WallEdge wallEdge4 = wallEdge3;
						do
						{
							list2.Add(wallEdge4.Pos);
							wallEdge4 = wallEdge4.Links[this];
						}
						while (wallEdge4.Links.ContainsKey(other) && wallEdge4.Links[other].Links.ContainsKey(this));
						flag = false;
					}
					HashSet<WallSnap> hashSet = null;
					if (wallEdge3.Links.ContainsKey(this) && wallEdge3.Children.TryGetValue(wallEdge3.Links[this], out hashSet))
					{
						foreach (WallSnap wallSnap in hashSet)
						{
							if (undos != null && !wallSnap.BeenDestroyed)
							{
								undos.Add(snaps[wallSnap]);
							}
							wallSnap.DestroyMe();
						}
						hashSet.Clear();
						wallEdge3.Links[this].Children[wallEdge3].Clear();
					}
					wallEdge3.Links[this] = wallEdge3.Links[other];
					wallEdge3.Links.Remove(other);
					wallEdge3 = wallEdge3.Links[this];
					if (wallEdge3.Links.ContainsKey(this) || !wallEdge3.Links.ContainsKey(other))
					{
						break;
					}
				}
			}
		}
		int num2 = 0;
		List<WallEdge> list3 = new List<WallEdge>();
		WallEdge wallEdge5 = this.Edges[0].Links[this];
		WallEdge wallEdge6 = wallEdge5;
		while (!list3.Contains(wallEdge5))
		{
			list3.Add(wallEdge5);
			wallEdge5 = wallEdge5.Links[this];
			num2++;
			if (num2 > this.Edges.Count * 2 + other.Edges.Count * 2)
			{
				Debug.LogException(new Exception("Broken while loop"), this);
			}
			else if (wallEdge5 != wallEdge6)
			{
				continue;
			}
			IL_419:
			this.Edges = list3;
			this.Edges.ForEach(delegate(WallEdge x)
			{
				x.Links.Remove(other);
			});
			this.UpdateBounds();
			List<Furniture> list4 = other._furnitures.ToList<Furniture>();
			list4.ForEach(delegate(Furniture x)
			{
				x.Parent = null;
			});
			other._furnitures.Clear();
			other.FurnitureTypes.Clear();
			GameSettings.Instance.sRoomManager.Rooms.RemoveAll((Room x) => x == other);
			foreach (WallEdge wallEdge7 in this.Edges)
			{
				HashSet<WallSnap> hashSet2 = null;
				if (wallEdge7.Children.TryGetValue(wallEdge7.Links[this], out hashSet2))
				{
					foreach (WallSnap wallSnap2 in hashSet2)
					{
						bool beenDestroyed = wallSnap2.BeenDestroyed;
						if (!wallSnap2.EdgeChanged(wallSnap2.WallPosition.Keys.ToArray<WallEdge>(), false) && undos != null && !beenDestroyed && snaps.ContainsKey(wallSnap2))
						{
							undos.Add(snaps[wallSnap2]);
						}
					}
				}
			}
			this.OptimizeSegments();
			this.DirtyOuterMesh = true;
			this.DirtyInnerMesh = true;
			List<Actor> list5 = other.Occupants.ToList<Actor>();
			for (int k = 0; k < list5.Count; k++)
			{
				Actor actor = list5[k];
				actor.currentRoom = this;
			}
			UnityEngine.Object.Destroy(other.gameObject);
			foreach (WallEdge wallEdge8 in list)
			{
				if (!this.Edges.Contains(wallEdge8))
				{
					GameSettings.Instance.sRoomManager.AllSegments.Remove(wallEdge8);
					foreach (WallSnap wallSnap3 in wallEdge8.Children.SelectMany((KeyValuePair<WallEdge, HashSet<WallSnap>> x) => x.Value))
					{
						UnityEngine.Object.Destroy(wallSnap3.gameObject);
					}
				}
			}
			this.UpdateFloor();
			for (int l = 0; l < list4.Count; l++)
			{
				Furniture furniture = list4[l];
				furniture.UpdateParent(false, false);
			}
			this.RecalculateTableGroups();
			for (int m = 0; m < this._furnitures.Count; m++)
			{
				this._furnitures[m].UpdateBoundsMesh();
			}
			other.Edges.Clear();
			this.DirtyNavMesh = true;
			this.DirtyPathNodes = true;
			this.FurnitureValidCheck(undos);
			this.Area = Utilities.PolygonArea((from x in this.Edges
			select x.Pos).ToList<Vector2>());
			return list2;
		}
		int num3 = list3.IndexOf(wallEdge5);
		for (int n = 0; n < num3; n++)
		{
			list3.RemoveAt(0);
		}
		goto IL_419;
	}

	public Dictionary<WallSnap, UndoObject.UndoAction> PrepareSplit(bool undo)
	{
		Dictionary<WallSnap, UndoObject.UndoAction> dictionary = new Dictionary<WallSnap, UndoObject.UndoAction>();
		foreach (WallSnap wallSnap in this.Edges.SelectMany((WallEdge x) => x.Children.SelectMany((KeyValuePair<WallEdge, HashSet<WallSnap>> z) => z.Value)).Distinct<WallSnap>())
		{
			if (undo && !wallSnap.BeenDestroyed)
			{
				dictionary[wallSnap] = new UndoObject.UndoAction(wallSnap, false);
			}
			else
			{
				dictionary[wallSnap] = null;
			}
		}
		return dictionary;
	}

	public Room Split(List<WallEdge> segments, List<UndoObject.UndoAction> destroyed, Dictionary<WallSnap, UndoObject.UndoAction> snaps, string debug = null, bool keepGroup = true)
	{
		if (debug == null)
		{
			debug = string.Join("\n", (from x in segments
			select x.Pos.x + ";" + x.Pos.y).ToArray<string>()) + "\n\n" + string.Join("\n", (from x in this.Edges
			select x.Pos.x + ";" + x.Pos.y).ToArray<string>());
		}
		Room result;
		try
		{
			WallEdge wallEdge = segments[0].Links[this];
			for (int i = 0; i < segments.Count - 1; i++)
			{
				segments[i].Links[this] = segments[i + 1];
			}
			int num = 0;
			WallEdge wallEdge2 = segments[segments.Count - 1];
			WallEdge wallEdge3 = wallEdge2;
			List<WallEdge> list = new List<WallEdge>();
			for (;;)
			{
				list.Add(wallEdge3);
				wallEdge3 = wallEdge3.Links[this];
				num++;
				if (num > this.Edges.Count * 2 + segments.Count * 2)
				{
					break;
				}
				if (wallEdge3 == wallEdge2)
				{
					goto Block_7;
				}
			}
			Debug.LogException(new Exception("Broken while loop"), this);
			return null;
			Block_7:
			int num2 = this.Edges.Count;
			num2 = Mathf.Max(list.Count, num2);
			List<WallEdge> list2 = new List<WallEdge>();
			wallEdge3 = wallEdge;
			num = 0;
			while (wallEdge3 != wallEdge2)
			{
				list2.Add(wallEdge3);
				wallEdge3 = wallEdge3.Links[this];
				num++;
				if (num > num2 * 2 + segments.Count * 2)
				{
					Debug.LogException(new Exception("Broken while loop"), this);
					return null;
				}
			}
			List<WallEdge> list3 = new List<WallEdge>(list2);
			list3.AddRange(segments.Reverse<WallEdge>());
			if (GameSettings.Instance.sRoomManager.IsSupported(from x in list
			select x.Pos, this.Floor, this, true, null))
			{
				if (GameSettings.Instance.sRoomManager.IsSupported(from x in list3
				select x.Pos, this.Floor, this, true, null))
				{
					this.Edges = list;
					list2.ForEach(delegate(WallEdge x)
					{
						x.Links.Remove(this);
					});
					list2.AddRange(segments.Reverse<WallEdge>());
					Room room = BuildController.Instance.MakeRoom(list2, this.Floor, null, false, false, true, this.Outdoors);
					this.CopyOverSettings(room);
					foreach (KeyValuePair<WallSnap, UndoObject.UndoAction> keyValuePair in snaps)
					{
						if (!keyValuePair.Key.EdgeChanged(new WallEdge[0], false) && keyValuePair.Value != null && destroyed != null)
						{
							Furniture furniture = keyValuePair.Key as Furniture;
							destroyed.Add(keyValuePair.Value);
							if (furniture != null)
							{
								destroyed.AddRange(from x in furniture.IterateSnap(null)
								select new UndoObject.UndoAction(x, false));
							}
						}
					}
					this.DirtyInnerMesh = true;
					this.DirtyNavMesh = true;
					this.DirtyPathNodes = true;
					this.SetSizes();
					for (int j = 0; j < this.Dirts.Count; j++)
					{
						if (!this.IsInside(this.Dirts[j].Pos))
						{
							Vector2 pos = this.Dirts[j].Pos;
							float amount = this.Dirts[j].Amount;
							this.RemoveDirt(j);
							if (room.IsInside(pos))
							{
								room.AddNewDirt(pos, amount);
							}
							j--;
						}
					}
					this.UpdateDirtScore(true);
					room.InitWritable();
					room.UpdateDirtScore(true);
					room.UpdateRoom(true, true, true, true, true, true, false);
					if (keepGroup && this.RoomGroup != null)
					{
						RoomGroup roomGroup = GameSettings.Instance.GetRoomGroup(this.RoomGroup);
						if (roomGroup != null)
						{
							roomGroup.AddRoom(room);
						}
					}
					foreach (Actor actor in this.Occupants.ToList<Actor>())
					{
						actor.UpdateCurrentRoom(true);
					}
					this.RefreshNoise();
					room.RefreshNoise();
					return room;
				}
			}
			for (int k = 0; k < this.Edges.Count; k++)
			{
				this.Edges[k].Links[this] = this.Edges[(k + 1) % this.Edges.Count];
			}
			WindowManager.SpawnDialog("SplitUnsupported".Loc(), true, DialogWindow.DialogType.Error);
			result = null;
		}
		catch (Exception exception)
		{
			MiscMsg.SendMsg("SplitRoom", Versioning.VersionString + "\n" + debug);
			Debug.LogException(exception);
			result = null;
		}
		return result;
	}

	private List<WallEdge> Destroy()
	{
		if (this.Edges == null)
		{
			return new List<WallEdge>();
		}
		List<WallEdge> list = new List<WallEdge>();
		List<Room> list2 = (from x in this.Edges.SelectMany((WallEdge x) => x.Links.Keys)
		where x != this
		select x).Distinct<Room>().ToList<Room>();
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[i];
			WallEdge wallEdge2 = this.Edges[(i + 1) % this.Edges.Count];
			wallEdge.Links.Remove(this);
			HashSet<WallSnap> hashSet;
			if (wallEdge.Children.TryGetValue(wallEdge2, out hashSet))
			{
				foreach (WallSnap wallSnap in hashSet)
				{
					if (wallEdge2.Links.ContainsValue(wallEdge))
					{
						wallSnap.EdgeChanged(wallSnap.WallPosition.Keys.ToArray<WallEdge>(), false);
					}
					else
					{
						wallSnap.DestroyMe();
					}
				}
			}
			if (wallEdge.Links.Count == 0)
			{
				list.Add(wallEdge);
			}
		}
		foreach (Room room in list2)
		{
			room.OptimizeSegments();
		}
		list2.ForEach(delegate(Room x)
		{
			x.DirtyOuterMesh = true;
		});
		return list;
	}

	public void UpdateSurrounded()
	{
		this.IsSurrounded = true;
		if (this.Outdoors)
		{
			this.IsSurrounded = false;
			return;
		}
		if (this._furnitures.Any((Furniture x) => x.TwoFloors && x.Parent == this))
		{
			this.IsSurrounded = false;
			return;
		}
		if (Options.OpaqueGlass)
		{
			this.IsSurrounded = true;
			return;
		}
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[i];
			WallEdge wallEdge2 = this.Edges[(i + 1) % this.Edges.Count];
			Room room = wallEdge2.GetRoom(wallEdge);
			HashSet<WallSnap> hashSet;
			if ((room == null || room.Outdoors) && wallEdge.Children.TryGetValue(wallEdge2, out hashSet))
			{
				if (hashSet.Count != 0)
				{
					foreach (RoomSegment roomSegment in hashSet.OfType<RoomSegment>())
					{
						if (roomSegment.LightAddition > 0f || roomSegment.IsConnector)
						{
							this.IsSurrounded = false;
							return;
						}
					}
				}
			}
		}
	}

	private void UpdateOuteredges()
	{
		if (GameSettings.Instance.sRoomManager.DisableMeshRebuild)
		{
			return;
		}
		this.DirtyOuterMesh = false;
		this.CalculateInsulation();
		if (this.Outdoors)
		{
			if (this.OuterWalls != null)
			{
				UnityEngine.Object.Destroy(this.OuterWalls);
			}
			this.GenerateFence();
			this.GenerateRoof();
			this.GenerateUpperPolygon();
			this.SetSizes();
			this.RecalculateStateVariables(false);
			this.UpdateVisibility();
			GameSettings.Instance.sRoomManager.UpdateSupport(this.Floor);
			return;
		}
		this.GenerateRoof();
		Vector2 vector = new Vector2((float)this._outsideColorID, (float)RoomMaterialController.GetMaterialID(this.OutsideMat));
		List<Vector2> list = new List<Vector2>();
		List<Vector3> list2 = new List<Vector3>();
		List<Vector4> list3 = new List<Vector4>();
		WallEdge wallEdge = this.Edges[0];
		WallEdge wallEdge2 = wallEdge.Links[this];
		WallEdge wallEdge3 = wallEdge2;
		bool flag = true;
		List<Vector2> list4 = new List<Vector2>();
		float num = 0f;
		List<CombineInstance> list5 = new List<CombineInstance>();
		int num2 = 0;
		for (;;)
		{
			WallEdge wallEdge4 = wallEdge2.Links[this];
			WallEdge localF = wallEdge2;
			WallEdge localS = wallEdge4;
			int num3 = wallEdge2.Links.Keys.Count((Room x) => localS.Links.Any((KeyValuePair<Room, WallEdge> y) => y.Key == x && y.Value == localF));
			if (num3 > 0 && !wallEdge2.IsAgainstOutdoors(wallEdge4))
			{
				if (wallEdge2 == wallEdge3 && !flag)
				{
					goto IL_1A6;
				}
				flag = false;
				wallEdge = wallEdge2;
				wallEdge2 = wallEdge4;
				num2++;
				if (num2 > this.Edges.Count * 2)
				{
					break;
				}
			}
			else
			{
				flag = false;
				WallEdge first = this.FindSubstitute(wallEdge, wallEdge2, wallEdge4, true, false);
				WallEdge third = this.FindSubstitute(wallEdge, wallEdge2, wallEdge4, false, false);
				Vector2 offset = Room.GetOffset(first, wallEdge2, wallEdge4, -Room.WallOffset / 2f, true);
				Vector2 offset2 = Room.GetOffset(wallEdge2, wallEdge4, third, -Room.WallOffset / 2f, true);
				Vector2 vector2 = offset2 - offset;
				float magnitude = vector2.magnitude;
				vector2 = vector2.normalized;
				Vector3 item = new Vector3(vector2.y, 0f, -vector2.x);
				Vector4 item2 = new Vector4(vector2.x, 0f, vector2.y, -1f);
				float num4 = num;
				float num5 = magnitude / 2f;
				list.Add(offset);
				list2.Add(item);
				list3.Add(item2);
				list4.Add(new Vector2(num4, 0f));
				bool flag2 = true;
				Vector2[] split = wallEdge2.GetSplit(wallEdge4);
				if (split != null)
				{
					for (int i = 0; i < split.Length; i++)
					{
						Vector2? vector3 = Utilities.ProjectToLine(split[i], offset, offset2);
						if (vector3 == null)
						{
							if (i == 0)
							{
								list.RemoveAt(list.Count - 1);
								list2.RemoveAt(list2.Count - 1);
								list3.RemoveAt(list3.Count - 1);
								list4.RemoveAt(list4.Count - 1);
							}
							else if (i == split.Length - 1)
							{
								flag2 = false;
							}
						}
						else
						{
							list.Add(vector3.Value);
							float num6 = offset.Dist(vector3.Value) / magnitude;
							list2.Add(item);
							list3.Add(item2);
							list4.Add(new Vector2(num4 + num5 * num6, 0f));
						}
					}
				}
				if (flag2)
				{
					list.Add(offset2);
					list2.Add(item);
					list3.Add(item2);
					list4.Add(new Vector2(num4 + num5, 0f));
				}
				foreach (Mesh mesh in wallEdge2.GetAllMeshes(wallEdge4, offset, num, false, this.Floor, vector))
				{
					list5.Add(new CombineInstance
					{
						mesh = mesh,
						transform = Matrix4x4.identity
					});
				}
				if (list4.Count > 0)
				{
					num = list4.Last<Vector2>().x % 1f;
				}
				wallEdge = wallEdge2;
				wallEdge2 = wallEdge4;
				num2++;
				if (num2 > this.Edges.Count * 2)
				{
					goto Block_15;
				}
			}
			if (wallEdge2 == wallEdge3)
			{
				goto IL_484;
			}
		}
		this.BrokenWhileLoop(delegate
		{
			<UpdateOuteredges>c__AnonStoreyB.$this.UpdateOuteredges();
		});
		return;
		IL_1A6:
		goto IL_484;
		Block_15:
		this.BrokenWhileLoop(delegate
		{
			<UpdateOuteredges>c__AnonStoreyB.$this.UpdateOuteredges();
		});
		return;
		IL_484:
		Mesh mesh2 = new Mesh();
		mesh2.vertices = list.AppendSelfArray((Vector2 x) => x.ToVector3(0f), (Vector2 x) => x.ToVector3(2f));
		mesh2.uv = list4.AppendSelfArray((Vector2 x) => new Vector2(x.x, 1f));
		mesh2.uv2 = Utilities.RepeatValue<Vector2>(vector, list4.Count * 2);
		mesh2.normals = list2.AppendSelfArray<Vector3>();
		mesh2.tangents = list3.AppendSelfArray<Vector4>();
		List<int> list6 = new List<int>();
		for (int k = 0; k < list.Count - 1; k += 2)
		{
			list6.Add(k);
			list6.Add(list.Count + k);
			list6.Add(list.Count + k + 1);
			list6.Add(list.Count + k + 1);
			list6.Add(k + 1);
			list6.Add(k);
		}
		mesh2.triangles = list6.ToArray();
		list5.Add(new CombineInstance
		{
			mesh = mesh2,
			transform = Matrix4x4.identity
		});
		if (Cheats.CeilingMeshes)
		{
			list5.Add(new CombineInstance
			{
				mesh = this.Roof.GetComponent<MeshFilter>().mesh,
				transform = Matrix4x4.TRS(new Vector3(0f, 2f, 0f), Quaternion.identity, new Vector3(1f, -1f, 1f))
			});
		}
		Material highlight = null;
		if (this.OuterWalls != null)
		{
			Renderer component = this.OuterWalls.GetComponent<Renderer>();
			if (component.sharedMaterials.Length > 1)
			{
				highlight = component.sharedMaterials[1];
			}
			UnityEngine.Object.Destroy(this.OuterWalls);
		}
		this.OuterWalls = new GameObject("OuterWalls");
		this.OuterWalls.tag = "Highlight";
		this.OuterWalls.transform.position = Vector3.up * (float)(this.Floor * 2);
		this.OuterWalls.transform.SetParent(base.transform);
		MeshRenderer meshRenderer = this.OuterWalls.AddComponent<MeshRenderer>();
		meshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
		this.SetMaterial(meshRenderer, highlight, Room.MatType.Outer);
		MeshFilter meshFilter = this.OuterWalls.AddComponent<MeshFilter>();
		meshFilter.mesh.CombineMeshes(list5.ToArray());
		this.GenerateUpperPolygon();
		this.SetSizes();
		this.RecalculateStateVariables(false);
		GameSettings.Instance.sRoomManager.UpdateSupport(this.Floor);
		this.UpdateVisibility();
	}

	public void UpdateAllMaterials()
	{
		if (!this.Outdoors)
		{
			if (this.InnerWalls != null)
			{
				this.SetMaterial(this.InnerWalls.GetComponent<MeshRenderer>(), null, Room.MatType.Inner);
			}
			if (this.OuterWalls != null)
			{
				this.SetMaterial(this.OuterWalls.GetComponent<MeshRenderer>(), null, Room.MatType.Outer);
			}
		}
	}

	private void SetMaterial(Renderer rend, Material highlight, Room.MatType type)
	{
		Material material = RoomMaterialController.Instance.MainMat;
		if (GameSettings.Instance != null && this.Floor == GameSettings.Instance.ActiveFloor)
		{
			GameSettings.WallState wallsDown = GameSettings.WallsDown;
			if (type == Room.MatType.Inner && (wallsDown == GameSettings.WallState.Low || wallsDown == GameSettings.WallState.LowNoSeg))
			{
				material = RoomMaterialController.Instance.MainCutMat;
			}
			else if (type == Room.MatType.Outer && (wallsDown == GameSettings.WallState.Low || wallsDown == GameSettings.WallState.LowNoSeg || wallsDown == GameSettings.WallState.Back))
			{
				material = RoomMaterialController.Instance.MainCutMat;
			}
		}
		highlight = (highlight ?? ((rend.sharedMaterials.Length <= 1) ? null : rend.sharedMaterials[1]));
		Material[] sharedMaterials;
		if (highlight != null)
		{
			Material[] array = new Material[2];
			array[0] = material;
			sharedMaterials = array;
			array[1] = highlight;
		}
		else
		{
			(sharedMaterials = new Material[1])[0] = material;
		}
		rend.sharedMaterials = sharedMaterials;
	}

	private WallEdge FindSubstitute(WallEdge s1, WallEdge s2, WallEdge s3, bool first, bool reverse = false)
	{
		Vector2 vector = (!reverse) ? s2.Pos : s3.Pos;
		Vector2 vector2 = (!reverse) ? s3.Pos : s2.Pos;
		if (first)
		{
			float num = float.MaxValue;
			WallEdge result = s1;
			foreach (Room room in s2.Links.Keys)
			{
				if (!room.Outdoors)
				{
					WallEdge wallEdge = s2.FindConnectionIn(room);
					if (wallEdge != null)
					{
						float num2 = Room.LeftVal(vector2, vector, wallEdge.Pos);
						if (num2 < num)
						{
							result = wallEdge;
							num = num2;
						}
					}
				}
			}
			return result;
		}
		float num3 = float.MinValue;
		WallEdge result2 = s3;
		foreach (KeyValuePair<Room, WallEdge> keyValuePair in s3.Links)
		{
			if (!keyValuePair.Key.Outdoors)
			{
				float num4 = Room.LeftVal(vector, vector2, keyValuePair.Value.Pos);
				if (num4 > num3)
				{
					result2 = keyValuePair.Value;
					num3 = num4;
				}
			}
		}
		return result2;
	}

	public static Vector2 GetOffset(WallEdge first, WallEdge second, WallEdge third, float offset, bool angleOffset = true)
	{
		return Room.GetOffset(first.Pos, second.Pos, third.Pos, offset, angleOffset);
	}

	public static Vector2 GetOffset(Vector2 first, Vector2 second, Vector2 third, float offset, bool angleOffset = true)
	{
		if (Utilities.Alike(first, second, third, 0.0001f))
		{
			Vector2 vector = (third - second).normalized * offset;
			return second + new Vector2(-vector.y, vector.x);
		}
		Vector3 b = new Vector3(second.x, 0f, second.y);
		Vector3 a = new Vector3(first.x, 0f, first.y);
		Vector3 a2 = new Vector3(third.x, 0f, third.y);
		Quaternion a3 = Quaternion.LookRotation(a - b);
		Quaternion b2 = Quaternion.LookRotation(a2 - b);
		Quaternion rotation = Quaternion.Lerp(a3, b2, 0.5f);
		float num = 1f;
		if (angleOffset)
		{
			num = Mathf.DeltaAngle(a3.eulerAngles.y, b2.eulerAngles.y);
			num = Mathf.Abs(Mathf.Sin(num * 0.0174532924f / 2f));
		}
		Vector3 a4 = (rotation * Vector3.forward).normalized * (offset / num);
		float y = Vector3.Cross(a - b, a2 - b).y;
		if (!Mathf.Approximately(y, 0f) && y < 0f)
		{
			a4 = -a4;
		}
		return second + new Vector2(a4.x, a4.z);
	}

	private void GenerateInnerPolygon()
	{
		if (GameSettings.Instance.sRoomManager.DisableMeshRebuild || this.Edges.Count < 3)
		{
			return;
		}
		this.DirtyInnerMesh = false;
		if (this.Outdoors)
		{
			if (this.InnerWalls != null)
			{
				UnityEngine.Object.Destroy(this.InnerWalls);
			}
			this.GenerateUpperPolygon();
			this.UpdateFloor();
			this.UpdateDarkness();
			this.Area = Utilities.PolygonArea((from x in this.Edges
			select x.Pos).ToList<Vector2>());
			this.RecalculateStateVariables(false);
			this.UpdateVisibility();
			return;
		}
		this.UpdateFloor();
		bool flag = this.MakeBlack();
		Vector2 vector = new Vector2((float)((!flag) ? this._insideColorID : RoomMaterialController.Instance.BlackColorID), (float)RoomMaterialController.GetMaterialID((!flag) ? this.InsideMat : "CannotRent"));
		int num = 0;
		List<Vector2> list = new List<Vector2>();
		List<Vector3> list2 = new List<Vector3>();
		List<Vector4> list3 = new List<Vector4>();
		List<Vector2> list4 = new List<Vector2>();
		float num2 = 0f;
		WallEdge wallEdge = this.Edges[0];
		WallEdge wallEdge2 = wallEdge.Links[this];
		WallEdge wallEdge3 = wallEdge;
		List<CombineInstance> list5 = new List<CombineInstance>();
		float num3 = 150f;
		bool flag2 = this.Edges[1].Pos.FullAngleBetween(this.Edges[0].Pos, this.Edges[2].Pos) < num3;
		bool flag3 = this.Edges[2].Pos.FullAngleBetween(this.Edges[1].Pos, this.Edges[3 % this.Edges.Count].Pos) < num3;
		for (;;)
		{
			WallEdge wallEdge4 = wallEdge2.Links[this];
			WallEdge third = wallEdge4.Links[this];
			Vector2 offset = Room.GetOffset(wallEdge, wallEdge2, wallEdge4, Room.WallOffset / 2f, true);
			Vector2 offset2 = Room.GetOffset(wallEdge2, wallEdge4, third, Room.WallOffset / 2f, true);
			Vector2 a = offset2 - offset;
			float magnitude = a.magnitude;
			a = a.normalized;
			Vector4 item = new Vector4(a.x, 0f, a.y, 1f);
			Vector3 item2 = new Vector3(-a.y, 0f, a.x);
			float num4 = num2;
			float num5 = magnitude / 2f;
			Vector2 b = -a * Room.WallOffset;
			Vector2 vector2 = offset + b;
			Vector2 vector3 = offset2 - b;
			list.Add((!flag2) ? offset : vector2);
			list2.Add(item2);
			list3.Add(item);
			list4.Add(new Vector2(num4 - ((!flag2) ? 0f : (Room.WallOffset / 2f)), 0f));
			Vector2[] split = wallEdge2.GetSplit(wallEdge4);
			bool flag4 = true;
			bool flag5 = false;
			if (split != null)
			{
				for (int i = 0; i < split.Length; i++)
				{
					Vector2? vector4 = Utilities.ProjectToLine(split[i], offset, offset2);
					if (vector4 == null)
					{
						bool flag6 = (flag2 && i == 0) || (flag3 && i == split.Length - 1);
						if (flag6)
						{
							vector4 = Utilities.ProjectToLine(split[i], vector2, vector3);
						}
						if (flag6 && vector4 != null)
						{
							if (i == 0)
							{
								list.Add(vector4.Value);
								float num6 = offset.Dist(vector4.Value) / magnitude;
								list2.Add(item2);
								list3.Add(item);
								list4.Add(new Vector2(num4 + num5 * num6, 0f));
							}
							else if (i == split.Length - 1)
							{
								flag5 = true;
							}
						}
						else if (i == 0)
						{
							list.RemoveAt(list.Count - 1);
							list2.RemoveAt(list2.Count - 1);
							list3.RemoveAt(list3.Count - 1);
							list4.RemoveAt(list4.Count - 1);
						}
						else if (i == split.Length - 1)
						{
							flag4 = false;
						}
					}
					else
					{
						list.Add(vector4.Value);
						float num7 = offset.Dist(vector4.Value) / magnitude;
						list2.Add(item2);
						list3.Add(item);
						list4.Add(new Vector2(num4 + num5 * num7, 0f));
					}
				}
			}
			if (flag4)
			{
				if (flag5)
				{
					list.Add(offset2);
					list2.Add(item2);
					list3.Add(item);
					list4.Add(new Vector2(num4 + num5, 0f));
				}
				list.Add((!flag3) ? offset2 : vector3);
				list2.Add(item2);
				list3.Add(item);
				list4.Add(new Vector2(num4 + num5 + ((!flag3) ? 0f : (Room.WallOffset / 2f)), 0f));
			}
			foreach (Mesh mesh in wallEdge2.GetAllMeshes(wallEdge4, offset, num2, true, this.Floor, vector))
			{
				list5.Add(new CombineInstance
				{
					mesh = mesh,
					transform = Matrix4x4.identity
				});
			}
			if (list4.Count > 0)
			{
				num2 = (num4 + num5) % 1f;
			}
			wallEdge = wallEdge2;
			wallEdge2 = wallEdge4;
			wallEdge4 = wallEdge2.Links[this];
			flag2 = flag3;
			flag3 = (wallEdge4.Pos.FullAngleBetween(wallEdge2.Pos, wallEdge4.Links[this].Pos) < num3);
			num++;
			if (num > this.Edges.Count * 2)
			{
				break;
			}
			if (wallEdge == wallEdge3)
			{
				goto Block_27;
			}
		}
		this.BrokenWhileLoop(delegate
		{
			this.GenerateInnerPolygon();
		});
		return;
		Block_27:
		Mesh mesh2 = new Mesh();
		Vector3[] vertices = list.AppendSelfArray((Vector2 x) => x.ToVector3(0f), (Vector2 x) => x.ToVector3(2f));
		mesh2.vertices = vertices;
		mesh2.uv = list4.AppendSelfArray((Vector2 x) => new Vector2(x.x, 1f));
		mesh2.uv2 = Utilities.RepeatValue<Vector2>(vector, list4.Count * 2);
		mesh2.normals = list2.AppendSelfArray<Vector3>();
		mesh2.tangents = list3.AppendSelfArray<Vector4>();
		List<int> list6 = new List<int>();
		for (int k = 0; k < list.Count - 1; k += 2)
		{
			list6.Add(list.Count + k + 1);
			list6.Add(list.Count + k);
			list6.Add(k);
			list6.Add(k);
			list6.Add(k + 1);
			list6.Add(list.Count + k + 1);
		}
		mesh2.triangles = list6.ToArray();
		list5.Add(new CombineInstance
		{
			mesh = mesh2,
			transform = Matrix4x4.identity
		});
		if (Cheats.CeilingMeshes)
		{
			CombineInstance item3 = default(CombineInstance);
			Mesh mesh3 = this.FloorMesh.GetComponent<MeshFilter>().mesh.Duplicate();
			Vector2 v = Vector2.one * 0.5f;
			mesh3.uv = mesh3.vertices.SelectInPlace((Vector3 x) => v);
			item3.mesh = mesh3;
			item3.transform = Matrix4x4.TRS(new Vector3(0f, 1.99f, 0f), Quaternion.identity, new Vector3(1f, -1f, 1f));
			list5.Add(item3);
		}
		if (this.InnerWalls != null)
		{
			UnityEngine.Object.Destroy(this.InnerWalls);
		}
		this.InnerWalls = new GameObject("InnerWalls");
		this.InnerWalls.transform.position = Vector3.up * (float)(this.Floor * 2);
		this.InnerWalls.transform.SetParent(base.transform);
		MeshRenderer meshRenderer = this.InnerWalls.AddComponent<MeshRenderer>();
		meshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
		this.SetMaterial(meshRenderer, null, Room.MatType.Inner);
		MeshFilter meshFilter = this.InnerWalls.AddComponent<MeshFilter>();
		meshFilter.mesh.CombineMeshes(list5.ToArray());
		this.GenerateUpperPolygon();
		this.UpdateDarkness();
		this.Area = Utilities.PolygonArea((from x in this.Edges
		select x.Pos).ToList<Vector2>());
		this.RecalculateStateVariables(false);
		this.UpdateVisibility();
		this.RefreshDirtQuad();
	}

	private bool CanOptimize(WallEdge s)
	{
		return s.Links.Count == 1 || (s.Links.Count == 2 && s.Links.Values.All((WallEdge x) => x.Links.ContainsValue(s)));
	}

	private bool CanOptimizeOutdoor(WallEdge s)
	{
		bool result;
		if (s.Links.Count((KeyValuePair<Room, WallEdge> x) => !x.Key.Outdoors) != 1)
		{
			if (s.Links.Count((KeyValuePair<Room, WallEdge> x) => !x.Key.Outdoors) == 2)
			{
				result = (from x in s.Links
				where !x.Key.Outdoors
				select x).All((KeyValuePair<Room, WallEdge> x) => x.Value.Links.ContainsValue(s));
			}
			else
			{
				result = false;
			}
		}
		else
		{
			result = true;
		}
		return result;
	}

	public void OptimizeSegments()
	{
		HashSet<Room> hashSet = new HashSet<Room>();
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[(i != 0) ? (i - 1) : (this.Edges.Count - 1)];
			WallEdge wallEdge2 = this.Edges[i];
			WallEdge wallEdge3 = this.Edges[(i + 1) % this.Edges.Count];
			if (this.CanOptimize(wallEdge2))
			{
				Vector2 normalized = (wallEdge2.Pos - wallEdge.Pos).normalized;
				Vector2 normalized2 = (wallEdge3.Pos - wallEdge2.Pos).normalized;
				if (wallEdge2.Pos == wallEdge.Pos || normalized == normalized2 || Mathf.Abs(Mathf.Acos(Vector2.Dot(normalized, normalized2))) < 0.05235988f)
				{
					GameSettings.Instance.sRoomManager.AllSegments.Remove(wallEdge2);
					hashSet.AddRange(wallEdge2.Links.Keys);
					foreach (Room room in wallEdge2.Links.Keys)
					{
						room.Edges.Remove(wallEdge2);
					}
					foreach (KeyValuePair<Room, WallEdge> keyValuePair in wallEdge.Links.ToList<KeyValuePair<Room, WallEdge>>())
					{
						if (keyValuePair.Value == wallEdge2)
						{
							wallEdge.Links[keyValuePair.Key] = wallEdge3;
						}
					}
					foreach (KeyValuePair<Room, WallEdge> keyValuePair2 in wallEdge3.Links.ToList<KeyValuePair<Room, WallEdge>>())
					{
						if (keyValuePair2.Value == wallEdge2)
						{
							wallEdge3.Links[keyValuePair2.Key] = wallEdge;
						}
					}
					float num = wallEdge.Pos.Dist(wallEdge3.Pos);
					float num2 = wallEdge.Pos.Dist(wallEdge2.Pos);
					HashSet<WallSnap> source = null;
					if (wallEdge.Children.TryGetValue(wallEdge2, out source))
					{
						foreach (WallSnap wallSnap in source.ToList<WallSnap>())
						{
							if (wallSnap.FirstEdge == wallEdge)
							{
								wallSnap.Init(wallEdge, wallEdge3, wallSnap.WallPosition[wallEdge] / num, true);
							}
							else
							{
								wallSnap.Init(wallEdge3, wallEdge, 1f - wallSnap.WallPosition[wallEdge] / num, true);
							}
						}
					}
					if (wallEdge2.Children.TryGetValue(wallEdge3, out source))
					{
						foreach (WallSnap wallSnap2 in source.ToList<WallSnap>())
						{
							if (wallSnap2.FirstEdge == wallEdge2)
							{
								wallSnap2.Init(wallEdge, wallEdge3, (num2 + wallSnap2.WallPosition[wallEdge2]) / num, true);
							}
							else
							{
								wallSnap2.Init(wallEdge3, wallEdge, 1f - (num2 + wallSnap2.WallPosition[wallEdge2]) / num, true);
							}
						}
					}
					i--;
				}
			}
		}
		bool flag = true;
		if (this.Edges.Count >= 3 && this != null)
		{
			flag = this.TryFixEdges();
		}
		foreach (Room room2 in hashSet)
		{
			room2.DirtyInnerMesh = true;
			room2.TryFixEdges();
		}
		if (flag && this.Edges.Count < 3 && this != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void GenerateUpperPolygon()
	{
		if (this.Outdoors)
		{
			if (this.UpperWalls != null)
			{
				UnityEngine.Object.Destroy(this.UpperWalls);
			}
			return;
		}
		int num = 0;
		List<Vector2> list = new List<Vector2>();
		WallEdge wallEdge = this.Edges[0];
		WallEdge wallEdge2 = wallEdge.Links[this];
		WallEdge wallEdge3 = wallEdge;
		for (;;)
		{
			WallEdge locSec = wallEdge2;
			WallEdge wallEdge4 = wallEdge2.Links[this];
			if (this.CanOptimizeOutdoor(wallEdge2))
			{
				list.Add(Room.GetOffset(wallEdge, wallEdge2, wallEdge4, Room.WallOffset / 2f, true));
				list.Add(Room.GetOffset(wallEdge, wallEdge2, wallEdge4, -Room.WallOffset / 2f, true));
			}
			else if (wallEdge2.Links.Count == 2 && wallEdge2.Links.Values.Any((WallEdge x) => x.Links.ContainsValue(locSec)))
			{
				list.Add(Room.GetOffset(wallEdge, wallEdge2, wallEdge4, Room.WallOffset / 2f, true));
				WallEdge wallEdge5 = this.FindSubstitute(wallEdge, wallEdge2, wallEdge4, true, false);
				WallEdge wallEdge6 = this.FindSubstitute(null, wallEdge5, wallEdge2, false, false);
				wallEdge5 = this.FindSubstitute(wallEdge5, wallEdge2, wallEdge6, true, false);
				wallEdge5 = ((wallEdge5 != wallEdge6) ? wallEdge5 : wallEdge);
				wallEdge6 = ((wallEdge6 != wallEdge5) ? wallEdge6 : wallEdge4);
				list.Add(Room.GetOffset(wallEdge5, wallEdge2, wallEdge6, -Room.WallOffset / 2f, true));
			}
			else
			{
				float y = wallEdge4.Pos.x - wallEdge2.Pos.x;
				float num2 = wallEdge4.Pos.y - wallEdge2.Pos.y;
				Vector2 vector = new Vector2(-num2, y);
				Vector2 normalized = vector.normalized;
				list.Add(wallEdge2.Pos + normalized * (Room.WallOffset / 2f));
				list.Add(wallEdge2.Pos - normalized * (Room.WallOffset / 2f));
			}
			wallEdge = wallEdge2;
			wallEdge2 = (locSec = wallEdge4);
			wallEdge4 = wallEdge2.Links[this];
			if (this.CanOptimizeOutdoor(wallEdge2))
			{
				list.Add(Room.GetOffset(wallEdge, wallEdge2, wallEdge4, Room.WallOffset / 2f, true));
				list.Add(Room.GetOffset(wallEdge, wallEdge2, wallEdge4, -Room.WallOffset / 2f, true));
			}
			else if (wallEdge2.Links.Count == 2 && wallEdge2.Links.Values.Any((WallEdge x) => x.Links.ContainsValue(locSec)))
			{
				list.Add(Room.GetOffset(wallEdge, wallEdge2, wallEdge4, Room.WallOffset / 2f, true));
				WallEdge wallEdge7 = this.FindSubstitute(wallEdge, wallEdge2, wallEdge4, true, false);
				WallEdge wallEdge8 = this.FindSubstitute(null, wallEdge7, wallEdge2, false, false);
				wallEdge7 = this.FindSubstitute(wallEdge7, wallEdge2, wallEdge8, true, false);
				wallEdge7 = ((wallEdge7 != wallEdge8) ? wallEdge7 : wallEdge);
				wallEdge8 = ((wallEdge8 != wallEdge7) ? wallEdge8 : wallEdge4);
				list.Add(Room.GetOffset(wallEdge7, wallEdge2, wallEdge8, -Room.WallOffset / 2f, true));
			}
			else
			{
				float y2 = wallEdge2.Pos.x - wallEdge.Pos.x;
				float num3 = wallEdge2.Pos.y - wallEdge.Pos.y;
				Vector2 vector2 = new Vector2(-num3, y2);
				Vector2 normalized2 = vector2.normalized;
				list.Add(wallEdge2.Pos + normalized2 * (Room.WallOffset / 2f));
				list.Add(wallEdge2.Pos - normalized2 * (Room.WallOffset / 2f));
			}
			num++;
			if (num > this.Edges.Count * 2)
			{
				break;
			}
			if (wallEdge == wallEdge3)
			{
				goto Block_14;
			}
		}
		this.BrokenWhileLoop(delegate
		{
			<GenerateUpperPolygon>c__AnonStorey.$this.GenerateUpperPolygon();
		});
		return;
		Block_14:
		Mesh mesh = new Mesh();
		Vector3[] array = list.SelectInPlace((Vector2 x) => new Vector3(x.x, 2f, x.y));
		mesh.vertices = array;
		mesh.uv = Utilities.RepeatValue<Vector2>(Vector2.zero, array.Length);
		mesh.normals = Utilities.RepeatValue<Vector3>(Vector3.up, array.Length);
		List<int> list2 = new List<int>();
		for (int i = 0; i < list.Count; i += 4)
		{
			list2.Add(i + 2);
			list2.Add(i + 1);
			list2.Add(i);
			list2.Add(i + 3);
			list2.Add(i + 1);
			list2.Add(i + 2);
		}
		mesh.triangles = list2.ToArray();
		if (this.UpperWalls != null)
		{
			UnityEngine.Object.Destroy(this.UpperWalls);
		}
		this.UpperWalls = new GameObject("TopWall");
		this.UpperWalls.transform.position = Vector3.up * (float)(this.Floor * 2);
		this.UpperWalls.transform.SetParent(base.transform);
		this.TopRend = this.UpperWalls.AddComponent<MeshRenderer>();
		this.TopRend.reflectionProbeUsage = ReflectionProbeUsage.Off;
		this.TopRend.receiveShadows = false;
		this.TopRend.shadowCastingMode = ShadowCastingMode.Off;
		this.TopRend.sharedMaterial = MaterialBank.Instance.Blackness;
		MeshFilter meshFilter = this.UpperWalls.AddComponent<MeshFilter>();
		meshFilter.mesh = mesh;
	}

	private void UpdateFloor()
	{
		this.DirtyFloorMesh = false;
		int num = 0;
		List<Vector2> list = new List<Vector2>();
		WallEdge wallEdge = this.Edges[0];
		WallEdge wallEdge2 = wallEdge.Links[this];
		WallEdge wallEdge3 = wallEdge;
		for (;;)
		{
			WallEdge wallEdge4 = wallEdge2.Links[this];
			list.Add(wallEdge2.Pos);
			wallEdge = wallEdge2;
			wallEdge2 = wallEdge4;
			num++;
			if (num > this.Edges.Count * 2)
			{
				break;
			}
			if (wallEdge == wallEdge3)
			{
				goto Block_2;
			}
		}
		this.BrokenWhileLoop(delegate
		{
			this.UpdateFloor();
		});
		return;
		Block_2:
		Mesh mesh = new Mesh();
		if (this._furnitures.Any((Furniture x) => x.ExtraParent == this))
		{
			if (this.Floor == 0)
			{
				TimeOfDay.Instance.GroundTopDirty = true;
			}
			Triangulator triangulator = new Triangulator(list.ToArray());
			int[] array = triangulator.Triangulate((from x in this._furnitures
			where x.ExtraParent == this
			select x).Select(delegate(Furniture x)
			{
				if (x.FinalNav == null)
				{
					x.UpdateBoundaryPoints();
				}
				return x.FinalNav;
			}).ToList<Vector2[]>(), new DTSweepContext());
			mesh.vertices = (from x in triangulator.Points
			select new Vector3(x.x, 0f, x.y)).ToArray<Vector3>();
			array.ReverseListPart(0, array.Length);
			mesh.triangles = array;
		}
		else
		{
			mesh.vertices = (from x in list
			select new Vector3(x.x, 0f, x.y)).ToArray<Vector3>();
			Triangulator triangulator2 = new Triangulator(list.ToArray());
			mesh.triangles = triangulator2.Triangulate();
		}
		Vector3[] vertices = mesh.vertices;
		bool flag = this.MakeBlack();
		mesh.normals = Utilities.RepeatValue<Vector3>(Vector3.up, vertices.Length);
		mesh.tangents = Utilities.RepeatValue<Vector4>(new Vector4(1f, 0f, 0f, -1f), vertices.Length);
		mesh.uv = vertices.SelectInPlace((Vector3 x) => new Vector2(x.x, x.z));
		mesh.uv2 = Utilities.RepeatValue<Vector2>(new Vector2((float)((!this.FloorMat.Equals("None")) ? ((!flag) ? this._floorColorID : RoomMaterialController.Instance.BlackColorID) : RoomMaterialController.Instance.GroundColorID), (float)RoomMaterialController.GetMaterialID((!flag) ? this.FloorMat : "CannotRent")), vertices.Length);
		Material highlight = null;
		if (this.FloorMesh != null)
		{
			Renderer component = this.OuterWalls.GetComponent<Renderer>();
			if (component.sharedMaterials.Length > 1)
			{
				highlight = component.sharedMaterials[1];
			}
			UnityEngine.Object.Destroy(this.FloorMesh);
		}
		this.FloorMesh = new GameObject("Floor");
		this.FloorMesh.tag = "Highlight";
		this.FloorMesh.transform.position = new Vector3(0f, (float)(this.Floor * 2) + 0.01f, 0f);
		this.FloorMesh.transform.SetParent(base.transform);
		MeshRenderer meshRenderer = this.FloorMesh.AddComponent<MeshRenderer>();
		this.SetMaterial(meshRenderer, highlight, Room.MatType.Floor);
		meshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
		this.FloorMesh.GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.TwoSided;
		MeshFilter meshFilter = this.FloorMesh.AddComponent<MeshFilter>();
		meshFilter.mesh = mesh;
	}

	public List<Vector2> GetFenceSegments()
	{
		List<Vector2> list = new List<Vector2>();
		List<Vector2> list2 = new List<Vector2>();
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge n1 = this.Edges[i];
			WallEdge wallEdge = this.Edges[(i + 1) % this.Edges.Count];
			int num = n1.FenceCount(wallEdge, false);
			if (num == 1 || (num == 2 && this.DID > wallEdge.Links.First((KeyValuePair<Room, WallEdge> x) => x.Value == n1).Key.DID))
			{
				list2.Clear();
				list2.Add(n1.Pos);
				Vector2[] split = n1.GetSplit(wallEdge);
				if (split != null)
				{
					list2.AddRange(split);
				}
				list2.Add(wallEdge.Pos);
				for (int j = 0; j < list2.Count; j += 2)
				{
					Vector2 item = list2[j];
					Vector2 item2 = list2[j + 1];
					list.Add(item);
					list.Add(item2);
				}
			}
		}
		return list;
	}

	private void GenerateFence()
	{
		List<ObjectDatabase.FenceStyle> fenceStyles = ObjectDatabase.Instance.FenceStyles;
		ObjectDatabase.FenceStyle fenceStyle = (!fenceStyles.Any((ObjectDatabase.FenceStyle x) => x.Name.Equals(this.FenceStyle))) ? fenceStyles.First<ObjectDatabase.FenceStyle>() : fenceStyles.First((ObjectDatabase.FenceStyle x) => x.Name.Equals(this.FenceStyle));
		List<CombineInstance> list = new List<CombineInstance>();
		List<Vector3> list2 = new List<Vector3>();
		int num = this.Edges[this.Edges.Count - 1].FenceCount(this.Edges[0], false);
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge n1 = this.Edges[i];
			WallEdge wallEdge = this.Edges[(i + 1) % this.Edges.Count];
			int num2 = n1.FenceCount(wallEdge, false);
			if (num2 == 1 || (num2 == 2 && this.DID > wallEdge.Links.First((KeyValuePair<Room, WallEdge> x) => x.Value == n1).Key.DID))
			{
				list2.Clear();
				list2.Add(n1.Pos.ToVector3(0f));
				Vector2[] split = n1.GetSplit(wallEdge);
				if (split != null)
				{
					list2.AddRange(from x in split
					select x.ToVector3(0f));
				}
				list2.Add(wallEdge.Pos.ToVector3(0f));
				for (int j = 0; j < list2.Count; j += 2)
				{
					Vector3 vector = list2[j];
					Vector3 vector2 = list2[j + 1];
					Vector3 vector3 = vector2 - vector;
					if (!(vector3 == Vector3.zero))
					{
						float magnitude = vector3.magnitude;
						Quaternion q = Quaternion.LookRotation(vector3);
						list.Add(new CombineInstance
						{
							mesh = this.StretchUV(fenceStyle.Fence, magnitude),
							transform = Matrix4x4.TRS((vector + vector2) * 0.5f, q, new Vector3(1f, 1f, magnitude))
						});
						if (j > 0 || num > 0)
						{
							list.Add(new CombineInstance
							{
								mesh = fenceStyle.Pole,
								transform = Matrix4x4.TRS(vector, q, Vector3.one)
							});
						}
						if (list2.Count > 2 && j < list2.Count - 2)
						{
							list.Add(new CombineInstance
							{
								mesh = fenceStyle.Pole,
								transform = Matrix4x4.TRS(vector2, q, Vector3.one)
							});
						}
					}
				}
			}
			num = num2;
		}
		this.OuterWalls = new GameObject("OuterWalls");
		this.OuterWalls.tag = "Highlight";
		this.OuterWalls.transform.position = Vector3.up * (float)(this.Floor * 2);
		this.OuterWalls.transform.SetParent(base.transform);
		MeshRenderer meshRenderer = this.OuterWalls.AddComponent<MeshRenderer>();
		meshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
		meshRenderer.sharedMaterial = new Material(fenceStyle.Mat);
		meshRenderer.sharedMaterial.color = this.OutsideColor;
		MeshFilter meshFilter = this.OuterWalls.AddComponent<MeshFilter>();
		meshFilter.mesh.CombineMeshes(list.ToArray());
		base.Highlight(this.IsHighlight, this.IsSecondary);
	}

	private Mesh StretchUV(Mesh m, float length)
	{
		Mesh mesh = new Mesh();
		mesh.vertices = m.vertices;
		mesh.triangles = m.triangles;
		mesh.tangents = m.tangents;
		mesh.normals = m.normals;
		Vector2[] array = new Vector2[m.uv.Length];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = new Vector2(m.uv[i].x * length, m.uv[i].y);
		}
		mesh.uv = array;
		return mesh;
	}

	private void UpdateDarkness()
	{
		if (this.Outdoors)
		{
			if (this.Darkness != null)
			{
				UnityEngine.Object.Destroy(this.Darkness);
			}
			return;
		}
		int num = 0;
		List<Vector2> floorPolygon = new List<Vector2>();
		WallEdge wallEdge = this.Edges[0];
		WallEdge wallEdge2 = wallEdge.Links[this];
		WallEdge wallEdge3 = wallEdge;
		for (;;)
		{
			WallEdge wallEdge4 = wallEdge2.Links[this];
			floorPolygon.Add(Room.GetOffset(wallEdge, wallEdge2, wallEdge4, Room.WallOffset / 2f + 0.01f, true));
			wallEdge = wallEdge2;
			wallEdge2 = wallEdge4;
			num++;
			if (num > this.Edges.Count * 2)
			{
				break;
			}
			if (wallEdge == wallEdge3)
			{
				goto Block_4;
			}
		}
		this.BrokenWhileLoop(delegate
		{
			this.UpdateDarkness();
		});
		return;
		Block_4:
		Mesh mesh = new Mesh();
		Vector3[] array = floorPolygon.AppendSelfArray((Vector2 x) => x.ToVector3(0f), (Vector2 x) => x.ToVector3(1.99f));
		Vector2[] array2 = new Vector2[array.Length];
		for (int i = 0; i < floorPolygon.Count; i++)
		{
			array2[i] = Vector2.zero;
			array2[i + floorPolygon.Count] = Vector2.one;
		}
		mesh.vertices = array;
		Triangulator triangulator = new Triangulator(floorPolygon.ToArray());
		List<int> list = new List<int>();
		list.AddRange(from x in triangulator.Triangulate()
		select x + floorPolygon.Count);
		for (int j = 0; j < floorPolygon.Count; j++)
		{
			list.Add(j);
			list.Add(floorPolygon.Count + j);
			if (j == floorPolygon.Count - 1)
			{
				list.Add(floorPolygon.Count);
				list.Add(floorPolygon.Count);
				list.Add(0);
			}
			else
			{
				list.Add(floorPolygon.Count + j + 1);
				list.Add(floorPolygon.Count + j + 1);
				list.Add(j + 1);
			}
			list.Add(j);
		}
		mesh.SetTriangles(list, 0);
		mesh.normals = Utilities.RepeatValue<Vector3>(Vector3.up, array.Length);
		mesh.uv = array2;
		if (this.Darkness != null)
		{
			UnityEngine.Object.Destroy(this.Darkness);
		}
		this.Darkness = new GameObject("Darkness");
		this.Darkness.transform.position = new Vector3(0f, (float)(this.Floor * 2), 0f);
		this.Darkness.transform.SetParent(base.transform);
		this.DarknessRend = this.Darkness.AddComponent<MeshRenderer>();
		this.DarknessRend.sharedMaterial = MaterialBank.Instance.GetDarkness(1f);
		this.DarknessRend.reflectionProbeUsage = ReflectionProbeUsage.Off;
		this.DarknessRend.shadowCastingMode = ShadowCastingMode.Off;
		this.DarknessRend.receiveShadows = false;
		MeshFilter meshFilter = this.Darkness.AddComponent<MeshFilter>();
		meshFilter.mesh = mesh;
	}

	public static bool Clockwise(List<WallEdge> s)
	{
		float num = 0f;
		for (int i = 0; i < s.Count; i++)
		{
			WallEdge wallEdge = s[i];
			WallEdge wallEdge2 = s[(i + 1) % s.Count];
			num += (wallEdge2.Pos.x - wallEdge.Pos.x) * (wallEdge2.Pos.y + wallEdge.Pos.y);
		}
		return num > 0f;
	}

	private void OnDrawGizmosSelected()
	{
		if (!this.Dummy)
		{
			float grayscale = this.OutsideColor.grayscale;
			Gizmos.color = this.OutsideColor;
			foreach (WallEdge wallEdge in this.Edges)
			{
				WallEdge wallEdge2 = wallEdge.Links[this];
				Gizmos.DrawLine(new Vector3(wallEdge.Pos.x, (float)(this.Floor * 2), wallEdge.Pos.y), new Vector3(wallEdge2.Pos.x, (float)(this.Floor * 2) + grayscale, wallEdge2.Pos.y));
			}
			if (Example.CachedPaths)
			{
				Gizmos.color = Color.green;
				foreach (PathNode<Vector3> pathNode in this.PathNodes)
				{
					List<PathNode<Vector3>> list = pathNode.GetConnections().ToList<PathNode<Vector3>>();
					for (int i = 0; i < list.Count; i++)
					{
						PathNode<Vector3> pathNode2 = list[i];
						IRoomConnector roomConnector = pathNode2.Tag as IRoomConnector;
						for (int j = i + 1; j < list.Count; j++)
						{
							PathNode<Vector3> pathNode3 = list[j];
							IRoomConnector roomConnector2 = pathNode3.Tag as IRoomConnector;
							List<Vector3> list2;
							if (this.CachedPaths.TryGetValue(new KeyValuePair<IRoomConnector, IRoomConnector>(roomConnector, roomConnector2), out list2) || this.CachedPaths.TryGetValue(new KeyValuePair<IRoomConnector, IRoomConnector>(roomConnector2, roomConnector), out list2))
							{
								Gizmos.DrawSphere(list2[0], 0.1f);
								for (int k = 0; k < list2.Count - 1; k++)
								{
									Gizmos.DrawLine(list2[k], list2[k + 1]);
									Gizmos.DrawSphere(list2[k + 1], 0.1f);
								}
							}
						}
					}
				}
			}
		}
		if (this.NavMap != null && Example.EnableNavMesh)
		{
			foreach (TriangleNode triangleNode in this.NavMap)
			{
				for (int m = 0; m < triangleNode.PathNode.Point.Points.Length; m++)
				{
					Vector2 vector = (!Example.NavMeshPortal) ? triangleNode.PathNode.Point.Points[m] : triangleNode.PathNode.Point.PortalPoints[m];
					Vector2 vector2 = (!Example.NavMeshPortal) ? triangleNode.PathNode.Point.Points[(m + 1) % triangleNode.PathNode.Point.Points.Length] : triangleNode.PathNode.Point.PortalPoints[(m + 1) % triangleNode.PathNode.Point.Points.Length];
					TriangleNode triangleNode2 = triangleNode.PathNode.Point.Connections[m];
					Gizmos.color = ((!Example.UseNavWeight || triangleNode2 == null) ? Color.cyan : Color.Lerp(Color.green, Color.red, triangleNode.PathNode.Point.Weight.GetOrDefault(triangleNode2, 1f).MapRange(0.5f, 2f, 0f, 1f, false)));
					Gizmos.DrawLine(new Vector3(vector.x, (float)(this.Floor * 2) + 0.05f, vector.y), new Vector3(vector2.x, (float)(this.Floor * 2) + 0.05f, vector2.y));
				}
				Gizmos.color = Color.black;
				foreach (TriangleNode triangleNode3 in from x in triangleNode.PathNode.GetConnections()
				select x.Point)
				{
					Gizmos.DrawLine(new Vector3(triangleNode.Center.x, (float)(this.Floor * 2) + 0.05f, triangleNode.Center.y), new Vector3(triangleNode3.Center.x, (float)(this.Floor * 2) + 0.05f, triangleNode3.Center.y));
				}
			}
		}
		if (this.DirtTree != null && Example.EnableDirtTree)
		{
			this.DirtTree.ForEach(delegate(QuadTreeNode<Room.Dirt> x)
			{
				float xMin = this.RoomBounds.xMin;
				Rect bounds = x.Bounds;
				float num = (xMin - bounds.center.x) / this.RoomBounds.width;
				float yMin = this.RoomBounds.yMin;
				Rect bounds2 = x.Bounds;
				Vector3 vector3 = Utilities.HSVToRGB((num + (yMin - bounds2.center.y) / this.RoomBounds.height) * 360f % 360f, 1f, 1f);
				Gizmos.color = new Color(vector3.x, vector3.y, vector3.z, 1f);
				Rect bounds3 = x.Bounds;
				Vector3 center = bounds3.center.ToVector3((float)(this.Floor * 2) + 0.05f);
				Rect bounds4 = x.Bounds;
				Gizmos.DrawWireCube(center, bounds4.size.ToVector3(0.1f));
				foreach (Room.Dirt dirt in x.Contents)
				{
					Gizmos.DrawSphere(dirt.Pos.ToVector3((float)(this.Floor * 2) + 0.025f), 0.05f);
				}
			});
		}
		Gizmos.color = Color.white;
	}

	private void GenerateRoof()
	{
		if (this.Outdoors)
		{
			if (this.Roof != null)
			{
				UnityEngine.Object.Destroy(this.Roof);
			}
			return;
		}
		this.UpdateSurrounded();
		this.DirtyRoofMesh = false;
		if (this.Floor == -1)
		{
			this.HasTwoFloor = this._furnitures.Any((Furniture x) => x.TwoFloors && x.Parent == this);
			return;
		}
		List<Vector2> list = new List<Vector2>();
		int num = 0;
		WallEdge wallEdge = this.Edges[0];
		WallEdge wallEdge2 = wallEdge;
		WallEdge wallEdge3 = wallEdge.Links[this];
		for (;;)
		{
			WallEdge wallEdge4 = wallEdge3.Links[this];
			WallEdge wallEdge5 = this.FindSubstitute(wallEdge, wallEdge3, wallEdge4, true, false);
			WallEdge wallEdge6 = this.FindSubstitute(null, wallEdge, wallEdge3, false, false);
			wallEdge5 = ((wallEdge5 != wallEdge4) ? wallEdge5 : wallEdge);
			wallEdge6 = ((wallEdge6 != wallEdge) ? wallEdge6 : wallEdge4);
			Vector2 offset = Room.GetOffset(wallEdge, wallEdge3, wallEdge6, -Room.WallOffset / 2f, true);
			Vector2 offset2 = Room.GetOffset(wallEdge5, wallEdge3, wallEdge4, -Room.WallOffset / 2f, true);
			bool flag = !wallEdge3.IsAgainstOutdoors(wallEdge) && wallEdge3.Links.ContainsValue(wallEdge);
			bool flag2 = !wallEdge4.IsAgainstOutdoors(wallEdge3) && wallEdge4.Links.ContainsValue(wallEdge3);
			if (flag2 && flag)
			{
				list.Add(wallEdge3.Pos);
			}
			else if (flag2)
			{
				list.Add(offset);
				list.Add(wallEdge3.Pos);
			}
			else if (flag)
			{
				list.Add(wallEdge3.Pos);
				list.Add(offset2);
			}
			else if (offset == offset2)
			{
				list.Add(offset);
			}
			else
			{
				list.Add(offset);
				list.Add(wallEdge3.Pos);
				list.Add(offset2);
			}
			wallEdge = wallEdge3;
			wallEdge3 = wallEdge4;
			num++;
			if (num > this.Edges.Count * 2)
			{
				break;
			}
			if (wallEdge == wallEdge2)
			{
				goto Block_14;
			}
		}
		this.BrokenWhileLoop(delegate
		{
			this.GenerateRoof();
		});
		return;
		Block_14:
		for (int i = 0; i < list.Count; i++)
		{
			int index = (i + 1) % list.Count;
			if (list[i] == list[index])
			{
				list.RemoveAt(index);
				i--;
			}
		}
		Mesh mesh = new Mesh();
		List<Furniture> list2 = (from x in this._furnitures
		where x.TwoFloors && x.Parent == this && x.CheckTwoFloorValid()
		select x).ToList<Furniture>();
		if (list2.Count > 0)
		{
			this.HasTwoFloor = true;
			Triangulator triangulator = new Triangulator(list.ToArray());
			int[] array = triangulator.Triangulate(list2.Select(delegate(Furniture x)
			{
				if (x.FinalNav == null)
				{
					x.UpdateBoundaryPoints();
				}
				return x.FinalNav;
			}).ToList<Vector2[]>(), new DTSweepContext());
			mesh.vertices = (from x in triangulator.Points
			select new Vector3(x.x, 2f, x.y)).ToArray<Vector3>();
			array.ReverseListPart(0, array.Length);
			mesh.triangles = array;
		}
		else
		{
			this.HasTwoFloor = false;
			mesh.vertices = (from x in list
			select new Vector3(x.x, 2f, x.y)).ToArray<Vector3>();
			Triangulator triangulator2 = new Triangulator(list.ToArray());
			mesh.triangles = triangulator2.Triangulate();
		}
		Vector3[] vertices = mesh.vertices;
		mesh.normals = Utilities.RepeatValue<Vector3>(Vector3.up, vertices.Length);
		mesh.uv = Utilities.RepeatValue<Vector2>(RoomMaterialController.Instance.ColorController.GetColorUV(this._outsideColorID), vertices.Length);
		if (this.Roof != null)
		{
			UnityEngine.Object.Destroy(this.Roof);
		}
		this.Roof = new GameObject("Roof");
		this.Roof.transform.position = new Vector3(0f, (float)(this.Floor * 2), 0f);
		this.Roof.transform.SetParent(base.transform);
		MeshRenderer meshRenderer = this.Roof.AddComponent<MeshRenderer>();
		meshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
		meshRenderer.sharedMaterial = MaterialBank.Instance.StandardRoof;
		MeshFilter meshFilter = this.Roof.AddComponent<MeshFilter>();
		meshFilter.mesh = mesh;
	}

	private void BuildNavMesh()
	{
		this.DirtyNavMesh = false;
		KeyValuePair<Vector2[], int[]> keyValuePair = new KeyValuePair<Vector2[], int[]>(null, null);
		if (!this.Dummy)
		{
			List<Furniture> list = (from x in this._furnitures
			where x.Height1 < Actor.HumanHeight && x.FinalNav != null && x.FinalNav.Length > 0
			select x).ToList<Furniture>();
			List<Vector3> holeTr = (from x in list
			select x.OriginalPosition).ToList<Vector3>();
			List<Vector2[]> list2 = (from x in list
			select x.FinalNav.ToArray<Vector2>()).ToList<Vector2[]>();
			Vector2[] expanded = this.GetExpanded(Room.WallOffset / 2f + 0.01f);
			try
			{
				keyValuePair = Utilities.SubtractAndTriangulate(expanded, list2, true, true, holeTr);
			}
			catch (Exception ex)
			{
				try
				{
					if (list2.Count > list.Count)
					{
						list2.RemoveRange(list.Count, list2.Count - list.Count);
					}
					keyValuePair = Utilities.SubtractAndTriangulate(expanded, list2, false, true, holeTr);
				}
				catch (Exception ex2)
				{
					ErrorLogging.AddException(new Exception("Error in room nav:" + Environment.NewLine + ex2.Message));
					keyValuePair = Utilities.SubtractAndTriangulate(this.GetExpanded(Room.WallOffset / 2f + 0.01f), new List<Vector2[]>(), false, true, null);
					MiscMsg.SendMsg("RoomNav", Versioning.VersionString + "\n" + this.ConvertToString());
				}
			}
		}
		else
		{
			List<Vector2[]> list3 = new List<Vector2[]>();
			for (int i = 0; i < RoadManager.Instance.Landmarks.Count; i++)
			{
				Landmark landmark = RoadManager.Instance.Landmarks[i];
				Vector2[] navMesh = landmark.GetNavMesh();
				if (navMesh != null)
				{
					list3.Add(navMesh);
				}
			}
			Dictionary<RoadNode, bool> parkingMesh = RoadManager.Instance.GetParkingMesh();
			foreach (KeyValuePair<RoadNode, bool> keyValuePair2 in parkingMesh)
			{
				list3.Add(keyValuePair2.Key.NavMesh);
			}
			List<Room> rooms = GameSettings.Instance.sRoomManager.GetRooms();
			for (int j = 0; j < rooms.Count; j++)
			{
				Room room = rooms[j];
				if (room.Floor == 0 && room.Edges != null)
				{
					list3.Add(room.GetExpanded(-Room.WallOffset / 2f));
				}
			}
			int count = list3.Count;
			try
			{
				keyValuePair = Utilities.SubtractAndTriangulate(new Vector2[]
				{
					new Vector2(-1f, -1f),
					new Vector2(257f, -1f),
					new Vector2(257f, 257f),
					new Vector2(-1f, 257f)
				}, list3, true, false, null);
			}
			catch (Exception ex3)
			{
				try
				{
					if (list3.Count > count)
					{
						list3.RemoveRange(count, list3.Count - count);
					}
					keyValuePair = Utilities.SubtractAndTriangulate(new Vector2[]
					{
						new Vector2(-1f, -1f),
						new Vector2(257f, -1f),
						new Vector2(257f, 257f),
						new Vector2(-1f, 257f)
					}, list3, false, false, null);
				}
				catch (Exception ex4)
				{
					ErrorLogging.AddException(new Exception("Error in ground nav:" + Environment.NewLine + ex4.Message));
					keyValuePair = Utilities.SubtractAndTriangulate(new Vector2[]
					{
						new Vector2(-1f, -1f),
						new Vector2(257f, -1f),
						new Vector2(257f, 257f),
						new Vector2(-1f, 257f)
					}, (from x in RoadManager.Instance.Landmarks
					select x.GetNavMesh()).ToList<Vector2[]>(), false, false, null);
					MiscMsg.SendMsg("GroundNav", Versioning.VersionString + "\n" + this.ConvertToString());
				}
			}
		}
		this.BSPNavMap = null;
		try
		{
			this.NavMap = TriangleNode.GenerateMap(keyValuePair.Key, keyValuePair.Value, 0.3f);
		}
		catch (Exception ex5)
		{
			ErrorLogging.AddException(ex5);
			keyValuePair = Utilities.SubtractAndTriangulate(this.GetExpanded(Room.WallOffset / 2f + 0.01f), new List<Vector2[]>(), false, true, null);
			this.NavMap = TriangleNode.GenerateMap(keyValuePair.Key, keyValuePair.Value, 0.3f);
			MiscMsg.SendMsg("Room triangulate", Versioning.VersionString + "\n" + this.ConvertToString());
		}
		this.NavmeshRebuilding = false;
	}

	private void PostNavMesh()
	{
		if (this.NavMap == null)
		{
			return;
		}
		this.RemoveUnreachableNavNodes();
		if (this.NavMap.Length >= 16)
		{
			this.BSPNavMap = new BSPTree<TriangleNode>(this.NavMap.Length / 8, 8, delegate(TriangleNode n, bool v, float x)
			{
				float num = (!v) ? n.rect.yMin : n.rect.xMin;
				float num2 = (!v) ? n.rect.yMax : n.rect.xMax;
				if (x < num)
				{
					return 1;
				}
				if (x > num2)
				{
					return -1;
				}
				return 0;
			}, delegate(List<TriangleNode> x, bool y)
			{
				float num = 0f;
				for (int l = 0; l < x.Count; l++)
				{
					num += ((!y) ? x[l].Center.y : x[l].Center.x);
				}
				return num / (float)Mathf.Max(1, x.Count);
			});
			float x2 = this.NavMap.Average((TriangleNode x) => x.Center.x);
			float y2 = this.NavMap.Average((TriangleNode x) => x.Center.y);
			Vector2 cv = new Vector2(x2, y2);
			foreach (TriangleNode node in from x in this.NavMap
			orderby (x.Center - cv).sqrMagnitude descending
			select x)
			{
				this.BSPNavMap.AddNode(node);
			}
		}
		List<Furniture> f = (from x in this._furnitures
		where x.SnapPoints.Length > 0 || x.InteractionPoints.Length > 0
		select x).ToList<Furniture>();
		if (f.Count > 16)
		{
			int c = Mathf.RoundToInt(Mathf.Sqrt((float)f.Count));
			for (int i = 0; i < f.Count; i++)
			{
				f[i].ImprintPosition();
			}
			Thread[] array = new Thread[Mathf.CeilToInt((float)f.Count / (float)c)];
			for (int m = 0; m < array.Length; m++)
			{
				int j = m;
				array[m] = new Thread(delegate
				{
					try
					{
						int num = Mathf.Min(j * c + c, f.Count);
						for (int l = j * c; l < num; l++)
						{
							f[l].UpdateFreeNavs(true);
						}
					}
					catch (Exception ex)
					{
						ErrorLogging.AddException(ex);
						throw;
					}
				});
				array[m].Start();
			}
			array.JoinThreads();
		}
		else
		{
			for (int k = 0; k < f.Count; k++)
			{
				f[k].UpdateFreeNavs(false);
			}
		}
		this.FixActorPositions();
	}

	private void RemoveUnreachableNavNodes()
	{
		if (this.Dummy)
		{
			return;
		}
		HashSet<TriangleNode> hashSet = new HashSet<TriangleNode>();
		foreach (IRoomConnector roomConnector in this.GetConnectors())
		{
			TriangleNode nodeAt = this.GetNodeAt(roomConnector.GetOffsetPos(this, false).FlattenVector3());
			if (nodeAt != null)
			{
				this.AddToReachable(nodeAt, hashSet);
			}
		}
		if (hashSet.Count > 0 && hashSet.Count < this.NavMap.Length)
		{
			this.NavMap = hashSet.ToArray<TriangleNode>();
		}
	}

	private void AddToReachable(TriangleNode node, HashSet<TriangleNode> map)
	{
		Stack<TriangleNode> stack = new Stack<TriangleNode>();
		if (!map.Contains(node))
		{
			stack.Push(node);
		}
		while (stack.Count > 0)
		{
			TriangleNode triangleNode = stack.Pop();
			map.Add(triangleNode);
			foreach (PathNode<TriangleNode> pathNode in triangleNode.PathNode.GetConnections())
			{
				if (!map.Contains(pathNode.Point))
				{
					stack.Push(pathNode.Point);
				}
			}
		}
	}

	public override int GetFloor()
	{
		return this.Floor;
	}

	public override bool IsSelectableInView()
	{
		return true;
	}

	public override string ToString()
	{
		return this.DID.ToString();
	}

	public string ConvertToString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (this.Dummy)
		{
			foreach (Vector2[] array in from x in GameSettings.Instance.sRoomManager.GetRooms()
			where x.Floor == 0 && x.Edges != null
			select x.GetExpanded(-Room.WallOffset / 2f))
			{
				for (int i = 0; i < array.Length; i++)
				{
					stringBuilder.AppendLine(array[i].x + "\t" + array[i].y);
				}
				stringBuilder.AppendLine();
			}
		}
		else
		{
			foreach (WallEdge wallEdge in this.Edges)
			{
				stringBuilder.Append(string.Concat(new object[]
				{
					wallEdge.Pos.x,
					";",
					wallEdge.Pos.y,
					";"
				}));
			}
			stringBuilder.AppendLine();
			int num = 0;
			Dictionary<Furniture, int> dictionary = new Dictionary<Furniture, int>();
			using (IEnumerator<Furniture> enumerator3 = (from x in this._furnitures
			orderby (!x.IsSnapping) ? 0 : 1
			select x).GetEnumerator())
			{
				while (enumerator3.MoveNext())
				{
					Furniture item = enumerator3.Current;
					stringBuilder.Append(item.name + ";");
					stringBuilder.Append(string.Concat(new object[]
					{
						item.OriginalPosition.x,
						";",
						item.OriginalPosition.z,
						";"
					}));
					stringBuilder.Append(item.transform.rotation.eulerAngles.y + ";");
					if (item.WallFurn)
					{
						WallEdge item2 = item.WallPosition.Keys.First((WallEdge x) => x != item.FirstEdge);
						stringBuilder.Append(string.Concat(new object[]
						{
							this.Edges.IndexOf(item.FirstEdge),
							";",
							this.Edges.IndexOf(item2),
							";",
							item.WallPosition[item.FirstEdge],
							";"
						}));
					}
					else
					{
						stringBuilder.Append("-1;-1;-1;");
					}
					if (item.IsSnapping)
					{
						stringBuilder.Append(dictionary[item.SnappedTo.Parent] + ";" + item.SnappedTo.Id);
					}
					else
					{
						stringBuilder.Append("-1;-1");
					}
					stringBuilder.AppendLine();
					dictionary[item] = num;
					num++;
				}
			}
		}
		return stringBuilder.ToString();
	}

	public void UpdateFurnitureWallNearness()
	{
		for (int i = 0; i < this._furnitures.Count; i++)
		{
			this._furnitures[i].CalcEdge();
		}
	}

	public Vector2? FindRandomSpot()
	{
		TriangleNode random = (from x in this.NavMap
		where x.Area > 0.5f
		select x).GetRandom<TriangleNode>();
		return (random != null) ? new Vector2?(random.Points.GetRandomTrianglePoint()) : null;
	}

	public bool IsNeutral()
	{
		return this.Dummy || (this.ForceRole != -4 && this.Teams.Count <= 0 && this.GetFurniture("Computer").Count <= 0 && this.GetFurniture("Toilet").Count <= 0);
	}

	public void UpdateIsPrivate()
	{
		for (int i = 0; i < this.Edges.Count; i++)
		{
			WallEdge wallEdge = this.Edges[i];
			WallEdge key = this.Edges[(i + 1) % this.Edges.Count];
			HashSet<WallSnap> orNull = wallEdge.Children.GetOrNull(key);
			if (orNull != null)
			{
				foreach (WallSnap wallSnap in orNull)
				{
					RoomSegment roomSegment = wallSnap as RoomSegment;
					if (roomSegment != null && !roomSegment.IsPrivate)
					{
						this.IsPrivate = false;
						return;
					}
				}
			}
		}
		this.IsPrivate = true;
	}

	public bool IsPlayerControlled()
	{
		return GameSettings.Instance == null || GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode || this.PlayerOwned;
	}

	public bool IsSupported(HashSet<Room> with = null)
	{
		return GameSettings.Instance.sRoomManager.IsSupported(from x in this.Edges
		select x.Pos, this.Floor, null, true, with);
	}

	public void UpdatePillars()
	{
		int num = Mathf.CeilToInt(this.RoomBounds.width);
		int num2 = Mathf.CeilToInt(this.RoomBounds.height);
		List<Vector2> list = new List<Vector2>();
		for (int i = 0; i < num; i++)
		{
			for (int j = 0; j < num2; j++)
			{
				bool flag = false;
				float x = this.RoomBounds.xMin + (float)i + 0.5f;
				float y = this.RoomBounds.yMin + (float)j + 0.5f;
				Vector2 vector = new Vector2(x, y);
				if (this.IsInside(vector))
				{
					for (int k = 0; k < list.Count; k++)
					{
						if ((list[k] - vector).sqrMagnitude < 16f)
						{
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						for (int l = 0; l < this.Edges.Count; l++)
						{
							WallEdge wallEdge = this.Edges[l];
							if ((vector - wallEdge.Pos).sqrMagnitude < 16f)
							{
								flag = true;
								break;
							}
							WallEdge wallEdge2 = this.Edges[(l + 1) % this.Edges.Count];
							Vector2? vector2 = Utilities.ProjectToLine(vector, wallEdge.Pos, wallEdge2.Pos);
							if (vector2 != null && (vector - vector2.Value).sqrMagnitude < 16f)
							{
								flag = true;
								break;
							}
						}
					}
					if (!flag)
					{
						GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(BuildController.Instance.PillarPrefab);
						gameObject.transform.position = vector.ToVector3((float)this.Floor * 2f);
						list.Add(vector);
					}
				}
			}
		}
	}

	public static float WallOffset = 0.2f;

	[NonSerialized]
	public List<WallEdge> Edges;

	public float AccRefreshTime;

	[NonSerialized]
	public Dictionary<KeyValuePair<IRoomConnector, IRoomConnector>, List<Vector3>> CachedPaths = new Dictionary<KeyValuePair<IRoomConnector, IRoomConnector>, List<Vector3>>();

	public static HashSet<Room> UpdatePCNoisiness = new HashSet<Room>();

	public Rect RoomBounds;

	public int Floor;

	public float Insulation = 1f;

	[NonSerialized]
	public string RoomGroup;

	[NonSerialized]
	private HashList<Furniture> _furnitures = new HashList<Furniture>();

	[NonSerialized]
	public Dictionary<string, HashList<Furniture>> FurnitureTypes = new Dictionary<string, HashList<Furniture>>();

	[NonSerialized]
	public List<Actor> Occupants = new List<Actor>();

	public GameObject Darkness;

	[NonSerialized]
	public List<PathNode<Vector3>> PathNodes = new List<PathNode<Vector3>>();

	public List<PathNode<Vector3>> SubNodes = new List<PathNode<Vector3>>();

	public bool Dummy;

	public bool Outdoors;

	public bool HasTwoFloor;

	public bool CanClean = true;

	public bool Accessible = true;

	public bool IsPrivate = true;

	[NonSerialized]
	private bool _playerOwned;

	[NonSerialized]
	private bool _rentable = true;

	public int Reservers;

	public float WindowDarkLevel = 0.5f;

	public float IndirectLighting;

	public float WindowDarkLevelNoCap = 0.5f;

	public float Temperature;

	public float FenceHeight = 1f;

	public GameObject InnerWalls;

	public GameObject OuterWalls;

	public GameObject UpperWalls;

	public GameObject FloorMesh;

	public GameObject Roof;

	public GameObject DirtObject;

	public TeamTextScript TeamText;

	public TeamTextScript RoleText;

	private Renderer TopRend;

	private Renderer DarknessRend;

	public string[] TempTeam;

	public int ForceRole = -1;

	public Material shadowMat;

	public float DirtScore = 1f;

	private bool DisableDirt;

	public float FurnEnvironment = 1f;

	public Texture2D ErrorIcon;

	public GUIStyle ErrorBox;

	public List<string> Problems = new List<string>();

	[NonSerialized]
	public List<Room.Dirt> Dirts = new List<Room.Dirt>();

	[NonSerialized]
	public QuadTree<Room.Dirt> DirtTree;

	public float Dust;

	public ParticleSystem DustParticles;

	private float[] AuraValues;

	public int[] AuraCount;

	public float Area;

	public bool DirtyNavMesh = true;

	public bool DirtyOuterMesh = true;

	public bool DirtyInnerMesh = true;

	public bool DirtyPathNodes = true;

	public bool DirtyStateVariables = true;

	public bool DirtyFloorMesh;

	public bool DirtyRoofMesh;

	public bool stateRefreshNeighbours;

	public bool NavmeshRebuilding;

	public bool NavmeshRebuildStarted;

	public Room ParentRoom;

	[NonSerialized]
	public List<Room> ChildrenRooms = new List<Room>();

	[NonSerialized]
	public Dictionary<Room, float> NoisePropagation = new Dictionary<Room, float>();

	[NonSerialized]
	public HashSet<Furniture> TempControllers = new HashSet<Furniture>();

	public static readonly HashList<Furniture> EmptyFurn = new HashList<Furniture>();

	public string _insideMat;

	public string _outsideMat;

	public string _floorMat;

	public string _fenceStyle = "Concrete";

	private Color _insideColor = Color.white;

	private Color _floorColor = Color.white;

	private Color _outsideColor = Color.gray;

	[NonSerialized]
	private int _insideColorID = -1;

	[NonSerialized]
	private int _floorColorID = -1;

	[NonSerialized]
	private int _outsideColorID = -1;

	private bool FirstOutsideMat = true;

	private bool FirstInsideMat = true;

	private bool FirstFloorMat = true;

	private bool FirstRoofMat = true;

	private float _darknessLevel;

	public Furniture[] Lamps = new Furniture[0];

	[NonSerialized]
	public HashSet<Team> Teams = new HashSet<Team>();

	private static string[] actions = new string[]
	{
		"Destroy",
		"Change Room Team",
		"Limit Use",
		"Select Staff",
		"SelectBuildingFloor",
		"Select Building",
		"Room Color",
		"Material",
		"Types in Room",
		"MergeRooms",
		"Save Style",
		"Apply Style",
		"GroupRooms"
	};

	private static string[] EditActions = new string[]
	{
		"Destroy",
		"SelectBuildingFloor",
		"Select Building",
		"Room Color",
		"Material",
		"Types in Room",
		"MergeRooms",
		"Save Style",
		"Apply Style",
		"ToggleRentable",
		"TogglePlayerOwned",
		"GroupRentRooms",
		"AutoGroupRentRooms"
	};

	private static string[] RentableActions = new string[]
	{
		"Change Room Team",
		"Limit Use",
		"Select Staff",
		"SelectBuildingFloor",
		"Select Building",
		"Room Color",
		"Types in Room",
		"GroupRooms"
	};

	private static string[] NotOwnedRentableActions = new string[]
	{
		"Select Staff",
		"SelectBuildingFloor",
		"Select Building"
	};

	private static string[] NonRentable = new string[]
	{
		"Select Staff",
		"SelectBuildingFloor",
		"Select Building"
	};

	public List<TableScript> TableParents = new List<TableScript>();

	[NonSerialized]
	public float WallArea = 1f;

	[NonSerialized]
	private float _maxCoolingTemp;

	[NonSerialized]
	private float _coolingControlArea;

	[NonSerialized]
	private float _maxHeatingTemp;

	[NonSerialized]
	private float _heatingControlArea;

	[NonSerialized]
	private float _serverTemp;

	[NonSerialized]
	private float _noThermo;

	[NonSerialized]
	private float _noThermoArea;

	[NonSerialized]
	private float _noThermoMax;

	[NonSerialized]
	private float _noThermoAgg;

	public float TheoMaxCoolingTemp;

	public float TheoCoolingControlArea;

	public float TheoMaxHeatingTemp;

	public float TheoHeatingControlArea;

	[NonSerialized]
	private float _lastTempValueUpdate = -1f;

	[NonSerialized]
	public float TempHeatUsageAgg;

	[NonSerialized]
	public float TempCoolUsageAgg;

	[NonSerialized]
	public float TempHeatUsageMax;

	[NonSerialized]
	public float TempCoolUsageMax;

	private bool DataOverlayMode;

	private static readonly List<TriangleNode.Portal> PortalCache = new List<TriangleNode.Portal>();

	public Vector2 Center;

	[NonSerialized]
	private uint SerializedParentRoom;

	[NonSerialized]
	private uint[] SerializedChildrenRooms;

	private bool HasTriedFix;

	[NonSerialized]
	private bool IsSurrounded;

	private TriangleNode[] NavMap;

	private BSPTree<TriangleNode> BSPNavMap;

	public class Dirt : IHasVector
	{
		public Dirt(Vector2 pos, float amount, int idx)
		{
			this.Pos = pos;
			this.Amount = amount;
			this.Index = idx;
		}

		public Vector2 GetPos()
		{
			return this.Pos;
		}

		public Vector2 Pos;

		public float Amount;

		public int Index;
	}

	public enum RoomLimits
	{
		Meeting = -4,
		Canteen,
		Lounge,
		Anyone,
		Leaders,
		Programmers,
		Designers,
		Artists,
		Marketing
	}

	public enum MatType
	{
		Floor,
		Inner,
		Outer
	}
}
