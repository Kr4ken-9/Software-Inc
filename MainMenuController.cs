﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using DevConsole;
using DG.Tweening;
using SSAA;
using Steamworks;
using Twitter;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CinematicEffects;
using UnityStandardAssets.ImageEffects;

public class MainMenuController : MonoBehaviour
{
	public SaveGame CurrentSaveGame
	{
		get
		{
			return (this.MainMenuSaves.Length <= 0) ? null : this.MainMenuSaves[this.CurrentSave];
		}
	}

	private void OnDestroy()
	{
		if (MainMenuController.Instance == this)
		{
			MainMenuController.Instance = null;
		}
	}

	public void UpdateSunSlider()
	{
		this.Sun.intensity = this.SunSlider.value;
	}

	public void TogglePictureMode()
	{
		this.PictureMode = !this.PictureMode;
		this.Extra.enabled = !this.PictureMode;
		this.LanguageCombo.SetActive(!this.PictureMode);
		this.SnapshotCamera.enabled = this.PictureMode;
		if (this.PictureMode)
		{
			this.MapObj.transform.rotation = Quaternion.identity;
			this.PicturePanel.SetActive(true);
			this.MainPanel.SetActive(false);
		}
		else
		{
			this.PicturePanel.SetActive(false);
			this.MainPanel.SetActive(true);
			this.SunSlider.value = 1.4f;
		}
	}

	public void Tweet()
	{
		if (this.PinField.gameObject.activeSelf)
		{
			this.WaitPanel.gameObject.SetActive(true);
			base.StartCoroutine(API.GetAccessToken(MainMenuController.ConsumerKey, MainMenuController.ConsumerSecret, MainMenuController.RequestToken.Token, this.PinField.text, new AccessTokenCallback(this.AccessTokenCallback)));
			this.PinField.gameObject.SetActive(false);
		}
		else if (MainMenuController.AccessToken == null)
		{
			this.WaitPanel.gameObject.SetActive(true);
			base.StartCoroutine(API.GetRequestToken(MainMenuController.ConsumerKey, MainMenuController.ConsumerSecret, new RequestTokenCallback(this.RequestTokenCallback)));
		}
		else
		{
			this.PostPic();
		}
	}

	private void RequestTokenCallback(bool success, RequestTokenResponse response)
	{
		if (success)
		{
			API.OpenAuthorizationPage(response.Token);
			this.PinField.gameObject.SetActive(true);
			MainMenuController.RequestToken = response;
		}
		else
		{
			this.TwitterError();
		}
		this.WaitPanel.gameObject.SetActive(false);
	}

	private void PostPic()
	{
		this.TweetField.text = "DefaultTweet".Loc();
		this.SavePanel.gameObject.SetActive(false);
		this.TweetPanel.gameObject.SetActive(true);
	}

	public void CancelTweet()
	{
		this.SavePanel.gameObject.SetActive(true);
		this.TweetPanel.gameObject.SetActive(false);
	}

	public void TweetNow()
	{
		this.RealPostPic();
		this.CancelTweet();
	}

	private void RealPostPic()
	{
		this.WaitPanel.gameObject.SetActive(true);
		base.StartCoroutine(API.PostMedia(this.Tweetable, MainMenuController.ConsumerKey, MainMenuController.ConsumerSecret, MainMenuController.AccessToken, new PostMediaCallback(this.PostMediaCallback)));
	}

	private void AccessTokenCallback(bool success, AccessTokenResponse response)
	{
		if (success)
		{
			MainMenuController.AccessToken = response;
			this.PostPic();
		}
		else
		{
			this.TwitterError();
		}
		this.WaitPanel.gameObject.SetActive(false);
	}

	private void TwitterError()
	{
		DialogWindow d = WindowManager.SpawnDialog();
		d.Show("TweetError".Loc(), false, DialogWindow.DialogType.Error, new KeyValuePair<string, Action>[]
		{
			new KeyValuePair<string, Action>("OK", delegate
			{
				d.Window.Close();
			})
		});
	}

	private void PostMediaCallback(bool success, string mediaId)
	{
		if (success)
		{
			base.StartCoroutine(API.PostTweet(this.TweetField.text, mediaId, MainMenuController.ConsumerKey, MainMenuController.ConsumerSecret, MainMenuController.AccessToken, new PostTweetCallback(this.PostTweetCallback)));
		}
		else
		{
			this.TwitterError();
		}
	}

	private void PostTweetCallback(bool success)
	{
		if (!success)
		{
			this.TwitterError();
		}
		this.WaitPanel.gameObject.SetActive(false);
	}

	public void Spin(float degrees)
	{
		this.MapObj.transform.rotation = Quaternion.Euler(0f, this.MapObj.transform.rotation.eulerAngles.y + degrees, 0f);
	}

	public void ApplyOptions()
	{
		this.Bloom.enabled = Options.Bloom;
		this.TiltScript.enabled = Options.TiltShift;
		this.TiltScript.blurArea = 5f * CameraScript.ScreenUpscale;
		this.SSAAObj.enabled = (Options.SSAA > 10);
		internal_SSAA.ChangeScale((float)Options.SSAA / 10f);
		this.SSAO.enabled = Options.AmbientOcclusion;
		this.AntiAlias.enabled = Options.FXAA;
		this.SMAA.enabled = Options.SMAA;
		this.SSR.enabled = Options.SSR;
		this.ColorCorrection.redChannel.MoveKey(1, new Keyframe(0.5f, Options.Gamma, 1f, 1f));
		this.ColorCorrection.greenChannel.MoveKey(1, new Keyframe(0.5f, Options.Gamma, 1f, 1f));
		this.ColorCorrection.blueChannel.MoveKey(1, new Keyframe(0.5f, Options.Gamma, 1f, 1f));
		this.ColorCorrection.UpdateParameters();
	}

	private void Start()
	{
		GameSettings.ResetForcePause();
		GameSettings.IsQuitting = false;
		if (MainMenuController._firstRun)
		{
			this.ErrorHandling();
			MainMenuController._firstRun = false;
		}
		this.defaultColor = this.SnapshotCamera.backgroundColor;
		this.MainMenuSaves = (from x in SaveGameManager.SaveGames
		where !x.Legacy && !x.Broken && !x.BuildingOnly
		orderby x.RealTime descending
		select x).ToArray<SaveGame>();
		this.CamEffects = (from x in this.SnapshotCamera.transform.GetComponentsInChildren<MonoBehaviour>()
		where !x.enabled
		select x).ToArray<MonoBehaviour>();
		this.ApplyOptions();
		if (MainMenuController.Instance != null)
		{
			UnityEngine.Object.Destroy(MainMenuController.Instance.gameObject);
		}
		MainMenuController.Instance = this;
		if (this.MainMenuSaves.Length > 0)
		{
			this.MapObj = MinimapThumbnailMaker.Instance.MinimapMaker.CreateMap(this.MainMenuSaves[0].Map, true);
			this.MapObj.transform.position = new Vector3(0f, 0f, 30f);
			TweenSettingsExtensions.SetEase<Tweener>(ShortcutExtensions.DOMoveZ(this.MapObj.transform, 0f, 0.4f, false), 9);
			this.SaveName.text = string.Concat(new object[]
			{
				this.MainMenuSaves[0].Name,
				"(1/",
				this.MainMenuSaves.Length,
				")"
			});
		}
		else
		{
			this.LoadController.SetActive(false);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			TextAsset textAsset = Resources.Load("StartMap") as TextAsset;
			using (MemoryStream memoryStream = new MemoryStream(textAsset.bytes))
			{
				MiniMapMaker.MapDescriptor map = (MiniMapMaker.MapDescriptor)binaryFormatter.Deserialize(memoryStream);
				this.MapObj = MinimapThumbnailMaker.Instance.MinimapMaker.CreateMap(map, true);
			}
		}
		this.MapObj.transform.localScale = this.MapObj.transform.localScale * 10f;
	}

	private void ErrorHandling()
	{
		if (Directory.Exists("BugReports"))
		{
			string[] directories = Directory.GetDirectories("BugReports");
			for (int i = 0; i < directories.Length; i++)
			{
				string dir = directories[i];
				try
				{
					string path = Path.Combine(dir, "Report.xml");
					if (File.Exists(path))
					{
						XMLParser.XMLNode xmlnode = XMLParser.ParseXML(File.ReadAllText(path));
						MainMenuController.IsUploading = true;
						FeedbackWindow.Instance.Window.Show();
						base.StartCoroutine(FeedbackWindow.Instance.UploadSpecial(xmlnode.GetNode("Mail", true).Value, xmlnode.GetNode("Message", true).Value, xmlnode.GetNode("Type", true).Value, xmlnode.GetNode("Version", true).Value, (from x in xmlnode.GetNode("Files", true).Children
						select Path.GetFullPath(Path.Combine(dir, x.Value))).ToArray<string>(), dir));
					}
					break;
				}
				catch (Exception ex)
				{
					DevConsole.Console.LogError(ex.ToString());
				}
			}
		}
		if (Options.AskReporting)
		{
			IEnumerable<string> directories2 = Directory.GetDirectories("./");
			if (MainMenuController.<>f__mg$cache0 == null)
			{
				MainMenuController.<>f__mg$cache0 = new Func<string, string>(Path.GetFileName);
			}
			foreach (string text in directories2.OrderByDescending(MainMenuController.<>f__mg$cache0))
			{
				string dir1 = text;
				string fileName = Path.GetFileName(text);
				if (Regex.IsMatch(fileName, "\\d\\d\\d\\d\\-\\d\\d\\-\\d\\d_\\d\\d\\d\\d\\d\\d") && !File.Exists(Path.Combine(text, "Checked.txt")))
				{
					int num = Convert.ToInt32(fileName.Substring(0, 4));
					int num2 = Convert.ToInt32(fileName.Substring(5, 2));
					int num3 = Convert.ToInt32(fileName.Substring(8, 2));
					if (num >= 2016 || num2 >= 6 || num3 >= 13)
					{
						DialogWindow d = WindowManager.SpawnDialog();
						d.Show("CrashMessage".Loc(), false, DialogWindow.DialogType.Information, new KeyValuePair<string, Action>[]
						{
							new KeyValuePair<string, Action>("Yes", delegate
							{
								FeedbackWindow instance = FeedbackWindow.Instance;
								FeedbackWindow.ReportTypes type = FeedbackWindow.ReportTypes.Crash;
								bool save = false;
								bool log = false;
								IList<string> files = Directory.GetFiles(dir1);
								if (MainMenuController.<>f__mg$cache1 == null)
								{
									MainMenuController.<>f__mg$cache1 = new Func<string, string>(Path.GetFullPath);
								}
								instance.Show(type, save, log, files.SelectInPlace(MainMenuController.<>f__mg$cache1));
								File.Create(Path.Combine(dir1, "Checked.txt"));
								d.Window.Close();
							}),
							new KeyValuePair<string, Action>("No", delegate
							{
								File.Create(Path.Combine(dir1, "Checked.txt"));
								d.Window.Close();
							}),
							new KeyValuePair<string, Action>("Never", delegate
							{
								Options.AskReporting = false;
								d.Window.Close();
							})
						});
						break;
					}
				}
			}
		}
	}

	public void OffsetSave(int offset)
	{
		if (this.MainMenuSaves.Length == 0)
		{
			this.CurrentSave = 0;
			return;
		}
		this.CurrentSave += offset;
		while (this.CurrentSave < 0)
		{
			this.CurrentSave = this.MainMenuSaves.Length + this.CurrentSave;
		}
		if (this.CurrentSave >= this.MainMenuSaves.Length)
		{
			this.CurrentSave %= this.MainMenuSaves.Length;
		}
		SaveGame saveGame = this.MainMenuSaves[this.CurrentSave];
		GameObject oldMap = this.MapObj;
		TweenSettingsExtensions.OnComplete<Tweener>(TweenSettingsExtensions.SetEase<Tweener>(ShortcutExtensions.DOMoveZ(oldMap.transform, (float)(-30 * offset), 0.2f, false), 8), delegate
		{
			UnityEngine.Object.Destroy(oldMap);
		});
		this.MapObj = MinimapThumbnailMaker.Instance.MinimapMaker.CreateMap(saveGame.Map, true);
		this.MapObj.transform.localScale = this.MapObj.transform.localScale * 10f;
		this.MapObj.transform.position = new Vector3(0f, 0f, (float)(30 * offset));
		TweenSettingsExtensions.SetEase<Tweener>(ShortcutExtensions.DOMoveZ(this.MapObj.transform, 0f, 0.4f, false), 9);
		this.SaveName.text = string.Concat(new object[]
		{
			saveGame.Name,
			"(",
			this.CurrentSave + 1,
			"/",
			this.MainMenuSaves.Length,
			")"
		});
	}

	public void TakePic()
	{
		base.StartCoroutine(this.SnapRoutine());
	}

	private IEnumerator SnapRoutine()
	{
		this.LoadingWait.SetActive(true);
		Canvas.ForceUpdateCanvases();
		this.SnapshotCamera.Render();
		yield return null;
		if (this.PictureTex != null)
		{
			UnityEngine.Object.Destroy(this.PictureTex);
		}
		int w = 1920;
		int h = 1080;
		try
		{
			w = Convert.ToInt32(this.ResWidth.text);
			h = Convert.ToInt32(this.ResHeight.text);
		}
		catch (Exception)
		{
		}
		if (!this.CheckSize(w, h))
		{
			this.LoadingWait.SetActive(false);
			WindowManager.SpawnDialog("PicTakingError".Loc(), true, DialogWindow.DialogType.Error);
			yield break;
		}
		this.PictureTex = new Texture2D(w, h, TextureFormat.RGB24, false);
		if (!this.SnapNow(this.PictureTex))
		{
			this.LoadingWait.SetActive(false);
			WindowManager.SpawnDialog("PicTakingError".Loc(), true, DialogWindow.DialogType.Error);
			yield break;
		}
		yield return null;
		if (this.Tweetable != null)
		{
			UnityEngine.Object.Destroy(this.Tweetable);
		}
		this.Tweetable = new Texture2D(1024, 512, TextureFormat.RGB24, false);
		if (!this.SnapNow(this.Tweetable))
		{
			this.LoadingWait.SetActive(false);
			WindowManager.SpawnDialog("PicTakingError".Loc(), true, DialogWindow.DialogType.Error);
			yield break;
		}
		this.LoadingWait.SetActive(false);
		this.ToggleImageSave();
		yield break;
	}

	public void ChangeColor()
	{
		WindowManager.SpawnColorDialog(delegate(Color c)
		{
			this.SnapshotCamera.backgroundColor = c;
		}, this.SnapshotCamera.backgroundColor, new Color[]
		{
			this.SnapshotCamera.backgroundColor,
			this.defaultColor
		});
	}

	public void EndEditRes(bool main)
	{
		string value = (!main) ? this.ResHeight.text : this.ResWidth.text;
		int num = (!main) ? 1080 : 1920;
		try
		{
			num = Convert.ToInt32(value);
		}
		catch (Exception)
		{
		}
		if (main)
		{
			this.ResWidth.text = num.ToString();
		}
		else
		{
			this.ResHeight.text = num.ToString();
		}
	}

	private bool CheckSize(int width, int height)
	{
		int num = Mathf.Max(width, height);
		int num2 = width * height / 1024 * 32 / 1024;
		return num2 >= 0 && num <= SystemInfo.maxTextureSize && num2 <= SystemInfo.graphicsMemorySize - 256;
	}

	private bool SnapNow(Texture2D outputTex)
	{
		for (int i = 0; i < this.CamEffects.Length; i++)
		{
			this.CamEffects[i].enabled = true;
		}
		int j;
		for (j = 1; j <= 8; j *= 2)
		{
			int num = Mathf.Max(outputTex.width, outputTex.height) * j;
			int num2 = (outputTex.width * outputTex.height / 1024 * 32 * j * j * 2 + outputTex.width * outputTex.height / 1024 * 32) / 1024;
			if (num2 < 0 || num > SystemInfo.maxTextureSize || (float)num2 > (float)SystemInfo.graphicsMemorySize * 0.75f - 256f)
			{
				j /= 2;
				break;
			}
		}
		if (j < 1)
		{
			return false;
		}
		j = Mathf.Min(8, j);
		Debug.Log("Screenshot upscaling by " + j);
		this.ssr.enabled = this.SSRToggle.isOn;
		RenderTexture renderTexture = new RenderTexture(outputTex.width * j, outputTex.height * j, 24);
		renderTexture.Create();
		this.SnapshotCamera.targetTexture = renderTexture;
		this.SnapshotCamera.Render();
		this.SnapshotCamera.targetTexture = null;
		RenderTexture active = RenderTexture.active;
		RenderTexture.active = renderTexture;
		Texture2D texture2D = new Texture2D(outputTex.width * j, outputTex.height * j, TextureFormat.RGB24, true);
		texture2D.ReadPixels(new Rect(0f, 0f, (float)(outputTex.width * j), (float)(outputTex.height * j)), 0, 0);
		texture2D.Apply();
		RenderTexture.active = active;
		outputTex.SetPixels(texture2D.GetPixels(Mathf.RoundToInt(Mathf.Log((float)j, 2f))));
		outputTex.Apply();
		renderTexture.Release();
		UnityEngine.Object.Destroy(texture2D);
		for (int k = 0; k < this.CamEffects.Length; k++)
		{
			this.CamEffects[k].enabled = false;
		}
		return true;
	}

	public void SaveImage()
	{
		int num = 1;
		string text = Path.Combine(Utilities.GetRoot(), "Snaps");
		if (!Directory.Exists(text))
		{
			Directory.CreateDirectory(text);
		}
		else
		{
			string[] files = Directory.GetFiles(text, "*.png");
			if (files.Length > 0)
			{
				num = files.Select(delegate(string x)
				{
					int result;
					try
					{
						int num2 = Convert.ToInt32(Path.GetFileNameWithoutExtension(x).Replace("Pic", string.Empty));
						result = num2;
					}
					catch (Exception)
					{
						result = 0;
					}
					return result;
				}).Max() + 1;
			}
		}
		string file = Path.GetFullPath(Path.Combine(text, "Pic" + num + ".png"));
		File.WriteAllBytes(file, this.PictureTex.EncodeToPNG());
		if (SteamManager.Initialized)
		{
			DialogWindow diag = WindowManager.SpawnDialog();
			diag.Show("SteamImgQ".Loc(), false, DialogWindow.DialogType.Question, new KeyValuePair<string, Action>[]
			{
				new KeyValuePair<string, Action>("Yes", delegate
				{
					SteamScreenshots.AddScreenshotToLibrary(file, null, this.PictureTex.width, this.PictureTex.height);
					diag.Window.Close();
				}),
				new KeyValuePair<string, Action>("No", delegate
				{
					diag.Window.Close();
				})
			});
		}
		this.ToggleImageSave();
	}

	public void ToggleImageSave()
	{
		this.SavePanel.SetActive(!this.SavePanel.activeSelf);
		if (this.SavePanel.activeSelf)
		{
			this.PictureShow.texture = this.PictureTex;
		}
	}

	private IEnumerator ChangeDisp()
	{
		int screenWidth = Screen.width;
		int screenHeight = Screen.height;
		int refresh = Screen.currentResolution.refreshRate;
		int curDisp = (!PlayerPrefs.HasKey("UnitySelectMonitor")) ? 0 : PlayerPrefs.GetInt("UnitySelectMonitor");
		curDisp = (curDisp + 1) % Display.displays.Length;
		PlayerPrefs.SetInt("UnitySelectMonitor", curDisp);
		Screen.SetResolution(800, 600, Screen.fullScreen, refresh);
		yield return null;
		Screen.SetResolution(screenWidth, screenHeight, Screen.fullScreen, refresh);
		yield break;
	}

	private void Update()
	{
		if (!this.PictureMode)
		{
			this.MapObj.transform.Rotate(0f, Time.deltaTime * 10f, 0f);
			this.Extra.shadows = ((!Options.MoreShadow) ? LightShadows.None : LightShadows.Hard);
		}
		else
		{
			if (Input.GetMouseButtonDown(0))
			{
				this.lastX = Input.mousePosition.x;
				this.lastRot = this.MapObj.transform.rotation.eulerAngles.y;
			}
			if (!GUICheck.OverGUI && Input.GetMouseButton(0))
			{
				this.MapObj.transform.rotation = Quaternion.Euler(0f, this.lastRot - (Input.mousePosition.x - this.lastX), 0f);
			}
		}
	}

	public static MainMenuController Instance;

	public GameObject MapObj;

	public SSAOPro SSAO;

	public TiltShift TiltScript;

	public Antialiasing AntiAlias;

	public BloomOptimized Bloom;

	public SuperSampling_SSAA SSAAObj;

	public AntiAliasing SMAA;

	public ScreenSpaceReflection SSR;

	public Camera SnapshotCamera;

	public MonoBehaviour[] CamEffects;

	private bool PictureMode;

	public GameObject MainPanel;

	public GameObject PicturePanel;

	public GameObject LoadController;

	public GameObject SavePanel;

	public GameObject TweetPanel;

	public GameObject WaitPanel;

	public GameObject LanguageCombo;

	public RawImage PictureShow;

	private Texture2D PictureTex;

	private Texture2D Tweetable;

	private int CurrentSave;

	private SaveGame[] MainMenuSaves;

	public Text SaveName;

	private Color defaultColor;

	private float lastX;

	private float lastRot;

	public InputField PinField;

	public InputField TweetField;

	public InputField ResWidth;

	public InputField ResHeight;

	public ScreenSpaceReflection ssr;

	public Toggle SSRToggle;

	public Slider SunSlider;

	public Light Sun;

	public Light Extra;

	public GameObject LoadingWait;

	public GUIProgressBar pBar;

	public GameObject pBarGO;

	public Text pBarText;

	public ColorCorrectionCurves ColorCorrection;

	private static string ConsumerKey = "UQb6TaTAXNyy5FomBebwxqqqz";

	private static string ConsumerSecret = "uBHWlowMe2bxTmli0Qctj1RSP8bRG27m4xkmthSCe7sgxstQDy";

	private static AccessTokenResponse AccessToken;

	private static RequestTokenResponse RequestToken;

	private static bool _firstRun = true;

	[NonSerialized]
	public static bool IsUploading;

	[CompilerGenerated]
	private static Func<string, string> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<string, string> <>f__mg$cache1;
}
