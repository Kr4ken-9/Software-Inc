﻿using System;
using UnityEngine;

public class CameraFloat : MonoBehaviour
{
	private void Start()
	{
		this.StartPos = base.transform.position;
		this.StartRot = base.transform.rotation;
	}

	private void Update()
	{
		this.t += Time.deltaTime;
		if (this.t >= this.max)
		{
			this.CurPos = base.transform.position;
			this.CurRot = base.transform.rotation;
			this.EndPos = this.StartPos + UnityEngine.Random.insideUnitSphere * UnityEngine.Random.Range(0.1f, 0.5f);
			this.EndRot = this.StartRot * Quaternion.Euler(UnityEngine.Random.Range(-2f, 0f), UnityEngine.Random.Range(-2f, 2f), 0f);
			this.t = 0f;
			this.max = UnityEngine.Random.Range(5f, 10f);
		}
		base.transform.position = Vector3.Lerp(this.CurPos, this.EndPos, this.AnimCurve.Evaluate(this.t / this.max));
		base.transform.rotation = Quaternion.Lerp(this.CurRot, this.EndRot, this.AnimCurve.Evaluate(this.t / this.max));
	}

	public AnimationCurve AnimCurve;

	private Vector3 StartPos;

	private Vector3 CurPos;

	private Vector3 EndPos;

	private Quaternion StartRot;

	private Quaternion CurRot;

	private Quaternion EndRot;

	private float t;

	private float max;
}
