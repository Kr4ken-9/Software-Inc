﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class HRManagement
{
	public bool HandleComplaint(float demand, Team parent)
	{
		parent.Leader.employee.ChangeSkill(Employee.EmployeeRole.Lead, 0.01f, false);
		return true;
	}

	public void HandleWage(Actor employee, Team parent)
	{
		employee.NegotiateSalary = false;
		employee.employee.ChangeSalary(employee.employee.Worth(-1, true), employee.employee.Worth(-1, true), employee, true);
	}

	private void CheckEducation(Team parent)
	{
		if (this.EducationAmount != 0 && Utilities.GetDaysFlat(this.LastEducation, SDateTime.Now()) >= this.EducationCooldown)
		{
			int num = this.EducationAmount;
			foreach (Actor actor in from x in parent.GetEmployeesDirect()
			where x.isActiveAndEnabled && x.CoursePoints <= 0f && x.employee.IsRole(Employee.RoleBit.Educateable)
			orderby x.LastCourse
			select x)
			{
				if (num == 0)
				{
					break;
				}
				Employee.EmployeeRole role = EducationWindow.Roles[0];
				string text = null;
				float num2 = 0f;
				for (int i = 0; i < EducationWindow.Roles.Length; i++)
				{
					Employee.EmployeeRole employeeRole = EducationWindow.Roles[i];
					if (actor.employee.IsRole(employeeRole))
					{
						foreach (string text2 in GameSettings.Instance.GetUnlockedSpecializations(employeeRole))
						{
							float specialization = actor.employee.GetSpecialization(employeeRole, text2, false);
							if (specialization > num2 && specialization < this.EducationLevel && specialization >= parent.SpecializationMinCap)
							{
								num2 = specialization;
								text = text2;
								role = employeeRole;
							}
						}
					}
				}
				if (text != null)
				{
					float num3 = (float)this.EducationDuration * EducationWindow.EducationCost;
					this.Spent += num3;
					GameSettings.Instance.MyCompany.MakeTransaction(-num3, Company.TransactionCategory.Education, null);
					HUD.Instance.educationWindow.SendEmployee(actor, this.EducationDuration, role, text, true);
					this.LastEducation = SDateTime.Now();
					parent.Leader.employee.ChangeSkill(Employee.EmployeeRole.Lead, 0.01f, false);
					num--;
				}
			}
		}
	}

	public void CheckRoles(Team parent)
	{
		if (this.ForceRole != HRManagement.RoleMode.Custom)
		{
			List<Actor> employeesDirect = parent.GetEmployeesDirect();
			for (int i = 0; i < employeesDirect.Count; i++)
			{
				Actor actor = employeesDirect[i];
				if (!actor.employee.IsRole(Employee.RoleBit.Lead))
				{
					HRManagement.RoleMode forceRole = this.ForceRole;
					if (forceRole != HRManagement.RoleMode.HiredFor)
					{
						if (forceRole == HRManagement.RoleMode.Best)
						{
							Employee.RoleBit bestRoles = actor.employee.GetBestRoles(false, 0.8f);
							if (bestRoles != Employee.RoleBit.None && bestRoles != actor.employee.CurrentRoleBit)
							{
								actor.ChangeRole(bestRoles);
							}
						}
					}
					else if (actor.employee.HiredFor != Employee.EmployeeRole.Lead)
					{
						Employee.RoleBit roleBit = Employee.RoleToMask[(int)actor.employee.HiredFor];
						if (actor.employee.CurrentRoleBit != roleBit)
						{
							actor.ChangeRole(roleBit);
						}
					}
				}
			}
		}
	}

	private void LookForEmployees(Team parent)
	{
		int[] array = parent.CountRoles(null);
		for (int i = 0; i < this.MaxEmployees.Length; i++)
		{
			Employee.EmployeeRole role = i + Employee.EmployeeRole.Programmer;
			int num = this.MaxEmployees[i] - array[i + 1];
			if (num > 0)
			{
				float skill = parent.Leader.employee.GetSkill(Employee.EmployeeRole.Lead);
				float num2 = HireWindow.GetFinalCost(1, (int)this.HireBracket, true, true, true) * skill.MapRange(0f, 1f, 1f, 0.5f, false) / (float)num;
				for (int j = 0; j < num; j++)
				{
					if (!GameSettings.Instance.MyCompany.CanMakeTransaction(-num2))
					{
						return;
					}
					Employee employee = new Employee(SDateTime.Now(), role, UnityEngine.Random.value > 0.5f, this.HireBracket, GameSettings.Instance.Personalities, false, this.GetPreferredSpec(role, parent), new int?(this.PreferredAge), parent, skill, skill.MapRange(0f, 1f, 0.01f, 0.1f, false));
					Actor actor = GameSettings.Instance.SpawnActor(employee.Female);
					actor.employee = employee;
					actor.Team = parent.Name;
					employee.SetRoles(Employee.RoleBit.AnyRole);
					parent.Leader.employee.ChangeSkill(Employee.EmployeeRole.Lead, 0.01f, false);
					this.Spent += num2;
					GameSettings.Instance.MyCompany.MakeTransaction(-num2, Company.TransactionCategory.Hire, null);
				}
			}
		}
	}

	public string GetPreferredSpec(Employee.EmployeeRole role, Team parent)
	{
		if (this.Specs.Count == 0 || !Employee.IsSpecialized(role))
		{
			return null;
		}
		List<Actor> employeesDirect = parent.GetEmployeesDirect();
		Dictionary<string, float> dictionary = new Dictionary<string, float>();
		string[] unlockedSpecializations = GameSettings.Instance.GetUnlockedSpecializations(role);
		for (int i = 0; i < employeesDirect.Count; i++)
		{
			Actor actor = employeesDirect[i];
			foreach (string text in unlockedSpecializations)
			{
				if (!this.Specs.Contains(text))
				{
					dictionary.AddUp(text, actor.employee.GetSpecialization(role, text, false));
				}
			}
		}
		string result;
		if (dictionary.Count == 0)
		{
			result = null;
		}
		else
		{
			result = dictionary.MinInstance((KeyValuePair<string, float> x) => x.Value).Key;
		}
		return result;
	}

	public void Update(Team parent)
	{
		this.LookForEmployees(parent);
		if (this.EducationLevel > 0f)
		{
			this.CheckEducation(parent);
		}
		this.CheckRoles(parent);
	}

	private bool _enabled;

	public float SpentLast;

	public float Spent;

	public int[] MaxEmployees = new int[4];

	public Employee.WageBracket HireBracket;

	public float EducationLevel;

	public float EducationBudget = 4000f;

	public int EducationCooldown = 1;

	public int EducationAmount;

	public int EducationDuration = 1;

	public int PreferredAge = 35;

	public HRManagement.RoleMode ForceRole = HRManagement.RoleMode.HiredFor;

	public SDateTime LastEducation = new SDateTime(1970);

	public bool HandleWages;

	public bool HandleComplaints;

	public SHashSet<string> Specs = new SHashSet<string>();

	public enum RoleMode
	{
		Custom,
		HiredFor,
		Best
	}
}
