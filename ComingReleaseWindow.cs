﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ComingReleaseWindow : MonoBehaviour
{
	private void Start()
	{
		GUIColumn guicolumn = this.List["ProducProtoRelease"];
		this.List.LastSort = guicolumn;
		guicolumn.Sort(true, true);
	}

	public void Toggle()
	{
		this.Window.Toggle(false);
		if (this.Window.Shown)
		{
			this.UpdateList();
		}
	}

	public void UpdateList()
	{
		this.List.Items.Clear();
		List<object> list = new List<object>();
		foreach (SimulatedCompany simulatedCompany in GameSettings.Instance.simulation.Companies.Values)
		{
			list.AddRange(simulatedCompany.Releases.Cast<object>());
		}
		this.List.Items.AddRange(list);
	}

	public GUIWindow Window;

	public GUIListView List;
}
