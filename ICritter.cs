﻿using System;
using UnityEngine;

public interface ICritter
{
	string GetTypeName();

	void ResetPlace();

	float OptimalMinWeather();

	float OptimalMaxWeather();

	float OptimalMinLight();

	float OptimalMaxLight();

	void Spawn();

	GameObject GetGameObject();

	bool ShouldUpdate();

	bool UpdateMe();
}
