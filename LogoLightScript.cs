﻿using System;
using UnityEngine;

public class LogoLightScript : MonoBehaviour
{
	private void Start()
	{
		this.time = Time.timeSinceLevelLoad;
		this.time2 = Time.timeSinceLevelLoad;
		base.GetComponent<Renderer>().material.color = this.c1;
		this.next = 2f;
	}

	private void Update()
	{
		if (Time.timeSinceLevelLoad - this.time2 < 2f)
		{
			base.GetComponent<Renderer>().material.color = this.c1;
			return;
		}
		if (Time.timeSinceLevelLoad - this.time > this.next)
		{
			this.isc1 = !this.isc1;
			this.time = Time.timeSinceLevelLoad;
			this.next = UnityEngine.Random.Range(1f, 3f);
		}
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Plane plane = new Plane(base.transform.parent.rotation * Vector3.left, base.transform.parent.position);
		float distance = 0f;
		plane.Raycast(ray, out distance);
		Vector3 point = ray.GetPoint(distance);
		if ((base.transform.position - point).magnitude < 0.1f)
		{
			base.GetComponent<Renderer>().material.color = this.c3;
			this.isc1 = false;
		}
		else
		{
			base.GetComponent<Renderer>().material.color = ((!this.isc1) ? this.c2 : this.c1);
		}
	}

	private float time;

	private float time2;

	private float next;

	public Color c1;

	public Color c2;

	public Color c3;

	public bool isc1 = true;
}
