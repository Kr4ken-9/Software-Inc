﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AutomationWindow : MonoBehaviour
{
	private void Awake()
	{
		this.WageBracket.UpdateContent<string>(Enum.GetNames(typeof(Employee.WageBracket)));
		this.ForceRole.UpdateContent<string>(from x in Enum.GetNames(typeof(HRManagement.RoleMode))
		select "RoleMode" + x);
	}

	public void Show(params Team[] ts)
	{
		this.Initializing = true;
		TutorialSystem.Instance.StartTutorial("HR", false);
		this.Window.Show();
		this.HREdCooldown.TooltipDescription = "EducationCooldownTip".Loc(new object[]
		{
			(GameSettings.DaysPerMonth <= 1) ? "Months".Loc() : "Days".Loc()
		});
		this.Teams = ts;
		this.TeamName.text = Utilities.GetTeamDescription((from x in ts
		select x.Name).ToList<string>());
		this.HandleWage.isOn = this.Teams.Mode((Team x) => x.HR.HandleWages, false);
		this.HandleComplaint.isOn = this.Teams.Mode((Team x) => x.HR.HandleComplaints, false);
		this.HREducationCooldown.text = this.Teams.Mode((Team x) => x.HR.EducationCooldown, 0).ToString();
		this.HREducationAmount.text = this.Teams.Mode((Team x) => x.HR.EducationAmount, 0).ToString();
		this.HREducation.value = this.Teams.Average((Team x) => x.HR.EducationLevel);
		this.HREdDuration.value = (float)this.Teams.Mode((Team x) => x.HR.EducationDuration, 0);
		for (int i = 0; i < 4; i++)
		{
			int i1 = i;
			this.HREmployeeCount[i].text = this.Teams.Mode((Team x) => x.HR.MaxEmployees[i1], 0).ToString();
		}
		this.HRAge.value = (float)this.Teams.Mode((Team x) => x.HR.PreferredAge, 0);
		this.WageBracket.Selected = (int)this.Teams.Mode((Team x) => x.HR.HireBracket, Employee.WageBracket.Low);
		this.ForceRole.Selected = (int)this.Teams.Mode((Team x) => x.HR.ForceRole, HRManagement.RoleMode.Custom);
		this.Initializing = false;
		this.UpdateSpecLabel();
	}

	public void UpdateWageBracket()
	{
		if (!this.Initializing && this.Teams != null)
		{
			foreach (Team team in this.Teams)
			{
				team.HR.HireBracket = (Employee.WageBracket)this.WageBracket.Selected;
			}
		}
	}

	public void UpdateRoleMode()
	{
		if (!this.Initializing && this.Teams != null)
		{
			foreach (Team team in this.Teams)
			{
				team.HR.ForceRole = (HRManagement.RoleMode)this.ForceRole.Selected;
			}
		}
	}

	public void UpdateAge()
	{
		if (!this.Initializing)
		{
			foreach (Team team in this.Teams)
			{
				team.HR.PreferredAge = (int)this.HRAge.value;
			}
		}
	}

	public void UpdateHRCount(int i)
	{
		if (!this.Initializing)
		{
			try
			{
				int num = Convert.ToInt32(this.HREmployeeCount[i].text);
				foreach (Team team in this.Teams)
				{
					team.HR.MaxEmployees[i] = num;
				}
			}
			catch (Exception)
			{
			}
		}
	}

	public void UpdateHREducation()
	{
		if (!this.Initializing)
		{
			try
			{
				foreach (Team team in this.Teams)
				{
					team.HR.EducationLevel = this.HREducation.value;
				}
			}
			catch (Exception)
			{
			}
		}
	}

	public void UpdateHREducationDuration()
	{
		if (!this.Initializing)
		{
			try
			{
				foreach (Team team in this.Teams)
				{
					team.HR.EducationDuration = (int)this.HREdDuration.value;
				}
			}
			catch (Exception)
			{
			}
		}
		this.HRDurationText.text = this.HREdDuration.value.ToString("N0");
	}

	public void UpdateHREducationCoolDown()
	{
		if (!this.Initializing)
		{
			try
			{
				int b = Convert.ToInt32(this.HREducationCooldown.text);
				foreach (Team team in this.Teams)
				{
					team.HR.EducationCooldown = Mathf.Max(1, b);
				}
			}
			catch (Exception)
			{
			}
		}
	}

	public void UpdateHREducationAmount()
	{
		if (!this.Initializing)
		{
			try
			{
				int educationAmount = Convert.ToInt32(this.HREducationAmount.text);
				foreach (Team team in this.Teams)
				{
					team.HR.EducationAmount = educationAmount;
				}
			}
			catch (Exception)
			{
			}
		}
	}

	public void ToggleComplaint()
	{
		if (!this.Initializing)
		{
			foreach (Team team in this.Teams)
			{
				team.HR.HandleComplaints = this.HandleComplaint.isOn;
			}
		}
	}

	public void ToggleWage()
	{
		if (!this.Initializing)
		{
			foreach (Team team in this.Teams)
			{
				team.HR.HandleWages = this.HandleWage.isOn;
			}
		}
	}

	private void Update()
	{
		int[] array = new int[Employee.RoleCount];
		foreach (Team team in this.Teams)
		{
			team.CountRoles(array);
		}
		this.HREmployeeText[0].text = string.Concat(new object[]
		{
			"Programmers".Loc(),
			"(",
			array[1],
			"):"
		});
		this.HREmployeeText[1].text = string.Concat(new object[]
		{
			"Designers".Loc(),
			"(",
			array[2],
			"):"
		});
		this.HREmployeeText[2].text = string.Concat(new object[]
		{
			"Artists".Loc(),
			"(",
			array[3],
			"):"
		});
		this.HREmployeeText[3].text = string.Concat(new object[]
		{
			"Marketing".Loc(),
			"(",
			array[4],
			"):"
		});
		this.HRSalariesText.text = this.Teams.Sum((Team y) => y.GetEmployeesDirect().SumSafe((Actor x) => x.GetRealSalary())).Currency(true);
		this.Spent.text = this.Teams.Sum((Team y) => y.HR.SpentLast).Currency(true);
		this.HRToggle.SetActive(this.Teams.Any((Team y) => y.HREnabled));
		this.HRAgeText.text = this.HRAge.value.ToString();
	}

	public void PickSpecs()
	{
		string[] specs = GameSettings.Instance.GetUnlockedSpecializations(Employee.EmployeeRole.Lead);
		bool[] selected = (from x in specs
		select this.Teams.Any((Team y) => !y.HR.Specs.Contains(x))).ToArray<bool>();
		SelectorController.Instance.selectWindow.ShowMulti("Specializations".Loc(), specs, selected, delegate(int[] sel)
		{
			HashSet<string> hashSet = (from x in sel
			select specs[x]).ToHashSet<string>();
			if (hashSet.Count == specs.Length)
			{
				hashSet.Clear();
			}
			for (int i = 0; i < this.Teams.Length; i++)
			{
				Team team = this.Teams[i];
				team.HR.Specs.Clear();
				team.HR.Specs.AddRange(hashSet);
			}
			this.UpdateSpecLabel();
		}, true, true, true);
	}

	public void UpdateSpecLabel()
	{
		string[] unlockedSpecializations = GameSettings.Instance.GetUnlockedSpecializations(Employee.EmployeeRole.Lead);
		string[] array = (from x in unlockedSpecializations
		where this.Teams.Any((Team y) => !y.HR.Specs.Contains(x))
		select x).ToArray<string>();
		if (array.Length == 0 || array.Length == unlockedSpecializations.Length)
		{
			this.SpecLabel.text = "Any".Loc();
		}
		else if (array.Length == 1)
		{
			this.SpecLabel.text = array[0].Loc();
		}
		else
		{
			this.SpecLabel.text = array.Length.ToString() + " " + "Specializations".Loc().ToLower();
		}
	}

	public GUIWindow Window;

	public GameObject HRToggle;

	[NonSerialized]
	public Team[] Teams;

	public bool Initializing;

	public GUICombobox WageBracket;

	public GUICombobox ForceRole;

	public Toggle HandleWage;

	public Toggle HandleComplaint;

	public InputField HREducationCooldown;

	public InputField HREducationAmount;

	public GUIToolTipper HREdCooldown;

	public Slider HREducation;

	public Slider HRAge;

	public Slider HREdDuration;

	public Text HRAgeText;

	public Text HRDurationText;

	public Text HRSalariesText;

	public Text TeamName;

	public Text Spent;

	public InputField[] HREmployeeCount;

	public Text[] HREmployeeText;

	public Text SpecLabel;
}
