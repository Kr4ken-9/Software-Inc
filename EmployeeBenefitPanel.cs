﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EmployeeBenefitPanel : MonoBehaviour
{
	private void Start()
	{
		this.Init();
	}

	private void Init()
	{
		if (this._values.Count == 0)
		{
			foreach (KeyValuePair<string, EmployeeBenefit> benefit in EmployeeBenefit.Benefits)
			{
				Toggle toggle = this.MakeElement<Toggle>(this.CheckBoxPrefab);
				this.MakeElement<Text>(this.LabelPrefab).text = benefit.Key.Loc() + ":";
				Slider slider = this.MakeElement<Slider>(this.SliderPrefab);
				slider.wholeNumbers = true;
				slider.maxValue = (float)Mathf.FloorToInt((benefit.Value.Max - benefit.Value.Min) / benefit.Value.Increment);
				Text l = this.MakeElement<Text>(this.LabelPrefab);
				KeyValuePair<string, EmployeeBenefit> benefit1 = benefit;
				slider.onValueChanged.AddListener(delegate(float x)
				{
					float num = benefit1.Value.Min + x * benefit1.Value.Increment;
					float valueFromTarget = this.GetValueFromTarget(benefit1.Key);
					if (!Mathf.Approximately(num, valueFromTarget))
					{
						l.text = benefit1.Value.AddPost(benefit1.Value.ValueToText(num) + "*") + " (" + benefit1.Value.ValueToText(valueFromTarget) + ")";
					}
					else
					{
						l.text = benefit1.Value.AddPost(benefit1.Value.ValueToText(num));
					}
				});
				slider.value = (float)Mathf.FloorToInt((this.GetValueFromTarget(benefit1.Key) - benefit.Value.Min) / benefit.Value.Increment);
				slider.onValueChanged.Invoke(slider.value);
				toggle.onValueChanged.AddListener(delegate(bool x)
				{
					this.ToggleChange(benefit1.Key, x);
				});
				this._values[benefit1.Key] = new KeyValuePair<Slider, Toggle>(slider, toggle);
			}
		}
	}

	private void ToggleChange(string key, bool on)
	{
		this._values[key].Key.interactable = (this.BenefitStyle == EmployeeBenefitPanel.Style.Reset || on);
	}

	private float GetValueFromTarget(string key)
	{
		return (this.Targets != null) ? this.Targets.Mode((Dictionary<string, float> x) => EmployeeBenefit.GetBenefitValue(x, key), 0f) : EmployeeBenefit.Benefits[key].Default;
	}

	private T MakeElement<T>(GameObject o)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(o);
		gameObject.transform.SetParent(this.Panel, false);
		return gameObject.GetComponent<T>();
	}

	public void SetTargets(EmployeeBenefitPanel.Style style, params Dictionary<string, float>[] targets)
	{
		this.Targets = targets;
		this.BenefitStyle = style;
		this.Init();
		foreach (KeyValuePair<string, KeyValuePair<Slider, Toggle>> keyValuePair in this._values)
		{
			EmployeeBenefit benefit = EmployeeBenefit.Benefits[keyValuePair.Key];
			keyValuePair.Value.Key.value = (float)Mathf.FloorToInt((this.GetValueFromTarget(keyValuePair.Key) - benefit.Min) / benefit.Increment);
			bool flag = this.BenefitStyle == EmployeeBenefitPanel.Style.Override && this.Targets != null && this.Targets.Any((Dictionary<string, float> x) => x.ContainsKey(benefit.Name));
			this.ToggleChange(keyValuePair.Key, flag);
			keyValuePair.Value.Value.isOn = flag;
			keyValuePair.Value.Value.GetComponent<GUIToolTipper>().ToolTipValue = this.BenefitStyle.ToString() + "Benefits";
		}
	}

	public void Apply()
	{
		if (this.Targets != null)
		{
			foreach (KeyValuePair<string, KeyValuePair<Slider, Toggle>> keyValuePair in this._values)
			{
				if (this.BenefitStyle == EmployeeBenefitPanel.Style.Reset || keyValuePair.Value.Value.isOn)
				{
					EmployeeBenefit employeeBenefit = EmployeeBenefit.Benefits[keyValuePair.Key];
					foreach (Dictionary<string, float> dictionary in this.Targets)
					{
						dictionary[keyValuePair.Key] = employeeBenefit.Min + keyValuePair.Value.Key.value * employeeBenefit.Increment;
					}
					keyValuePair.Value.Key.onValueChanged.Invoke(keyValuePair.Value.Key.value);
				}
				if (this.BenefitStyle == EmployeeBenefitPanel.Style.Override && !keyValuePair.Value.Value.isOn)
				{
					foreach (Dictionary<string, float> dictionary2 in this.Targets)
					{
						dictionary2.Remove(keyValuePair.Key);
					}
				}
				if (this.BenefitStyle == EmployeeBenefitPanel.Style.Reset && keyValuePair.Value.Value.isOn)
				{
					foreach (Actor actor in GameSettings.Instance.sActorManager.Actors)
					{
						actor.employee.CustomBenefits.Remove(keyValuePair.Key);
					}
				}
			}
			if (this.BenefitStyle == EmployeeBenefitPanel.Style.Reset)
			{
				foreach (KeyValuePair<string, KeyValuePair<Slider, Toggle>> keyValuePair2 in this._values)
				{
					keyValuePair2.Value.Value.isOn = false;
				}
			}
			this.Revert();
			this.OnChange.Invoke();
		}
	}

	public void Revert()
	{
		this.SetTargets(this.BenefitStyle, this.Targets);
	}

	public Transform Panel;

	public GameObject SliderPrefab;

	public GameObject LabelPrefab;

	public GameObject CheckBoxPrefab;

	[NonSerialized]
	private Dictionary<string, KeyValuePair<Slider, Toggle>> _values = new Dictionary<string, KeyValuePair<Slider, Toggle>>();

	[NonSerialized]
	public Dictionary<string, float>[] Targets;

	public EmployeeBenefitPanel.Style BenefitStyle;

	public UnityEvent OnChange = new UnityEvent();

	public enum Style
	{
		Reset,
		Override
	}
}
