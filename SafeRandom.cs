﻿using System;

public static class SafeRandom
{
	public static Random Rnd
	{
		get
		{
			if (SafeRandom._rnd == null)
			{
				SafeRandom._rnd = new Random();
			}
			return SafeRandom._rnd;
		}
	}

	[ThreadStatic]
	private static Random _rnd;
}
