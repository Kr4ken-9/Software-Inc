﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NormalCar : MonoBehaviour
{
	public void Reset()
	{
		this.GoHome = false;
		this.Debug = false;
		this.TrafficWait = -1f;
		this.UltimateWait = -1f;
		this.currentNode = 0;
		this.Goal = null;
		this.Path = null;
	}

	public RoadNode GetGoal()
	{
		return this.Goal;
	}

	public void DestroyEvent()
	{
		if (this.Goal != null && !this.GoHome)
		{
			this.Goal.Taken = false;
		}
	}

	public void Init()
	{
		this.Car.CurrentSpeed = this.Car.Speed;
		this.Goal = this.Car.Target;
		if (this.Debug)
		{
			this.Goal = RoadManager.Instance.FindRandomParking();
			if (this.Goal == null)
			{
				RoadManager.Instance.DestroyCar(this.Car);
				return;
			}
		}
		this.Path = RoadManager.Instance.MakeRoadPlan(ref this.Goal);
		if (this.Path == null)
		{
			if (this.Car.AnyOccupants())
			{
				this.Car.ForEachOccupant(delegate(Actor x)
				{
					GameSettings.Instance.sActorManager.ReadyForBus.Add(x);
				});
			}
			if (this.Goal.Parking)
			{
				this.Goal.Taken = false;
			}
			RoadManager.Instance.DestroyCar(this.Car);
			return;
		}
		if (this.Goal.Parking)
		{
			this.Goal.Taken = true;
		}
		if (this.Car.AnyOccupants())
		{
			this.Car.CanDestroy = false;
		}
		Vector2 vector = this.Path[1] - this.Path[0];
		base.transform.rotation = Quaternion.LookRotation(new Vector3(vector.x, 0f, vector.y));
		base.transform.position = new Vector3(this.Path[0].x, base.transform.position.y, this.Path[0].y);
		Actor actor = this.Car.FirstActor();
		if (actor == null)
		{
			this.MyColor = this.ColorPick.Evaluate(UnityEngine.Random.value);
		}
		else
		{
			this.MyColor = ((actor.AItype != AI<Actor>.AIType.Employee || actor.GetBenefitValue("Company car") <= 0f) ? actor.CarColor3 : RoadManager.Instance.CompanyCarColor);
		}
		this.Car.UpdateColor(this.MyColor);
	}

	public bool CheckTrafficCycle(List<IHasSpeed> Visit, List<IHasSpeed> Visited, CarScript caller)
	{
		for (int i = 0; i < Visit.Count; i++)
		{
			IHasSpeed hasSpeed = Visit[i];
			CarScript cc = hasSpeed as CarScript;
			if (cc != null)
			{
				if (Visited.Contains(hasSpeed))
				{
					if ((from x in cc.WaitingFor
					where x != null
					select x).OfType<CarScript>().Any((CarScript x) => Mathf.DeltaAngle(x.transform.rotation.eulerAngles.y, cc.transform.rotation.eulerAngles.y) > 45f))
					{
						cc.WaitingFor.Clear();
						return false;
					}
					return true;
				}
				else
				{
					Visited.Add(hasSpeed);
					if (this.CheckTrafficCycle(cc.WaitingFor, Visited, cc))
					{
						if ((from x in caller.WaitingFor
						where x != null
						select x).OfType<CarScript>().Any((CarScript x) => Mathf.DeltaAngle(x.transform.rotation.eulerAngles.y, caller.transform.rotation.eulerAngles.y) > 45f))
						{
							caller.WaitingFor.Clear();
							return false;
						}
						return true;
					}
				}
			}
		}
		return false;
	}

	private void Update()
	{
		if (GameSettings.GameSpeed == 0f)
		{
			return;
		}
		float num = Time.deltaTime * GameSettings.GameSpeed;
		bool flag = false;
		if (this.Car.WaitingFor.Count > 0)
		{
			flag = true;
			float maxSpeed = this.Car.GetMaxSpeed(this.Car.GetAngle());
			this.Car.CurrentSpeed = ((!this.Car.Parked) ? Mathf.Lerp(this.Car.CurrentSpeed, maxSpeed, num * ((this.Car.CurrentSpeed <= maxSpeed) ? (this.Car.Speed / 8f) : this.Car.Speed)) : 0f);
			if (this.Car.CurrentSpeed <= 0f)
			{
				if (this.UltimateWait == -1f)
				{
					this.UltimateWait = 60f;
				}
				else
				{
					this.UltimateWait -= num;
					if (this.UltimateWait <= 0f)
					{
						this.Car.WaitingFor.Clear();
						this.UltimateWait = -1f;
					}
				}
				if (this.TrafficWait == -1f)
				{
					this.TrafficWait = (float)UnityEngine.Random.Range(4, 6);
				}
				else
				{
					this.TrafficWait -= num;
					if (this.TrafficWait <= 0f)
					{
						this.CheckTrafficCycle(this.Car.WaitingFor, new List<IHasSpeed>
						{
							this.Car
						}, this.Car);
						this.TrafficWait = -1f;
					}
				}
			}
			else
			{
				this.UltimateWait = -1f;
				this.TrafficWait = -1f;
			}
		}
		else
		{
			this.TrafficWait = -1f;
		}
		if (this.currentNode < this.Path.Count)
		{
			Vector2 b = new Vector2(base.transform.position.x, base.transform.position.z);
			float num2 = this.Car.CurrentSpeed * num;
			Vector2 vector = this.Path[this.currentNode] - b;
			float magnitude = vector.magnitude;
			Vector2 vector2 = vector;
			bool flag2 = vector2 == Vector2.zero;
			if (!flag2 && magnitude >= num2)
			{
				vector2 *= num2 / magnitude;
				flag2 = (vector2 == Vector2.zero);
				num2 = 0f;
			}
			else
			{
				num2 -= magnitude;
			}
			if (this.Car.WaitingFor.Count == 0 && !flag)
			{
				Vector2 normalized = (this.GetPosAt(this.currentNode, magnitude + 2f) - base.transform.position.FlattenVector3()).normalized;
				float num3 = (180f - Mathf.Abs(Vector2.Angle(base.transform.forward.FlattenVector3(), normalized))) / 180f;
				num3 = Mathf.Max(0.2f, num3 * num3);
				if (this.Goal != null && this.Goal.Parking)
				{
					float num4 = 5f;
					if (this.currentNode < this.Path.Count - 2)
					{
						num4 = this.Car.Speed * num3;
					}
					float num5 = (this.Car.CurrentSpeed >= num4) ? this.Car.Speed : (this.Car.Speed / 8f);
					this.Car.CurrentSpeed = Mathf.Lerp(this.Car.CurrentSpeed, num4, num * num5);
				}
				else
				{
					float num6 = num3 * this.Car.Speed;
					float num7 = (this.Car.CurrentSpeed >= num6) ? this.Car.Speed : (this.Car.Speed / 8f);
					this.Car.CurrentSpeed = Mathf.Lerp(this.Car.CurrentSpeed, num6, num * num7);
				}
			}
			base.transform.position = new Vector3(base.transform.position.x + vector2.x, base.transform.position.y, base.transform.position.z + vector2.y);
			if (!flag2)
			{
				base.transform.rotation = Quaternion.Lerp(base.transform.rotation, Quaternion.LookRotation(new Vector3(vector2.x, 0f, vector2.y)), num * this.Car.CurrentSpeed);
			}
			if (num2 > 0f)
			{
				bool flag3 = num2 > 1f;
				int num8 = 0;
				float magnitude2 = (this.Path[this.currentNode] - base.transform.position.FlattenVector3()).magnitude;
				bool flag4 = false;
				while (magnitude2 <= num2)
				{
					if (num8 > 25)
					{
						flag4 = false;
						break;
					}
					base.transform.position = new Vector3(this.Path[this.currentNode].x, base.transform.position.y, this.Path[this.currentNode].y);
					this.currentNode++;
					if (this.currentNode == this.Path.Count)
					{
						if (this.Goal != null && this.Goal.Parking)
						{
							if (!this.Debug && !this.Car.AnyLiveOccupants(false))
							{
								this.GoHome = true;
								return;
							}
							if (GameSettings.GameSpeed > 1f)
							{
								Vector2 normalized2 = (this.Path[this.currentNode - 1] - this.Path[this.currentNode - 2]).normalized;
								base.transform.rotation = Quaternion.Lerp(base.transform.rotation, Quaternion.LookRotation(new Vector3(normalized2.x, 0f, normalized2.y)), UnityEngine.Random.Range(0.98f, 1f));
							}
							this.Car.BindOccupants();
							this.Car.BeginSpawn();
							this.Car.Parked = true;
							this.Car.AudioComp.volume = 0f;
							this.Car.CurrentSpeed = 0f;
							this.Car.LightsE = false;
							this.Car.AudioE = false;
						}
						else
						{
							RoadManager.Instance.DestroyCar(this.Car);
						}
						flag4 = false;
						break;
					}
					if (this.currentNode > 0)
					{
						flag4 = true;
						if (flag3)
						{
							Vector2 normalized3 = (this.Path[this.currentNode] - this.Path[this.currentNode - 1]).normalized;
							base.transform.rotation = Quaternion.LookRotation(new Vector3(normalized3.x, 0f, normalized3.y));
						}
					}
					num8++;
					num2 -= magnitude2;
					magnitude2 = (this.Path[this.currentNode] - base.transform.position.FlattenVector3()).magnitude;
				}
				if (flag4 && this.currentNode > 0 && num2 > 0f)
				{
					base.transform.position = base.transform.position + (this.Path[this.currentNode] - this.Path[this.currentNode - 1]).normalized.ToVector3(0f) * num2;
				}
			}
			return;
		}
		if (this.Goal == null)
		{
			RoadManager.Instance.DestroyCar(this.Car);
			return;
		}
		if (this.GoHome || (this.Debug && Utilities.ChancePerInGameMinute(0.03f, Time.deltaTime) > 0))
		{
			this.Car.Parked = false;
			this.Goal.Taken = false;
			this.Car.CanDestroy = true;
			this.Path = RoadManager.Instance.GetHome(this.Goal);
			this.Goal = null;
			this.currentNode = 0;
			this.Car.LightsE = true;
			this.Car.AudioE = true;
			if (this.Path == null)
			{
				RoadManager.Instance.DestroyCar(this.Car);
			}
		}
		else if (!this.Debug && !this.Car.AnyLiveOccupants(true))
		{
			if (!this.Car.AllDoorsClosed())
			{
				for (int i = 0; i < this.Car.SpawnPoints.Length; i++)
				{
					CarSpawn carSpawn = this.Car.SpawnPoints[i];
					carSpawn.CloseDoor();
				}
			}
			else
			{
				this.GoHome = true;
			}
		}
		else if (!this.Car.IsSpawning() && this.Car.AnyDeadOccupants())
		{
			this.Car.BeginSpawn();
		}
	}

	private Vector2 GetPosAt(int offset, float distance)
	{
		if (offset >= this.Path.Count - 1)
		{
			return this.Path[this.Path.Count - 1];
		}
		for (int i = offset; i < this.Path.Count - 1; i++)
		{
			Vector2 vector = this.Path[i];
			Vector2 a = this.Path[i + 1];
			Vector2 a2 = a - vector;
			float magnitude = a2.magnitude;
			if (magnitude > distance)
			{
				return vector + a2 * (distance / magnitude);
			}
			distance -= magnitude;
		}
		return this.Path[this.Path.Count - 1];
	}

	private void OnDrawGizmosSelected()
	{
		if (this.Car != null && this.Car.AnyOccupants())
		{
			this.Car.ForEachOccupant(delegate(Actor item)
			{
				if (item != null)
				{
					if (item.MyCar == this.Car)
					{
						Gizmos.color = ((!this.Car.SpawnPoints[item.CarSpawnID].Occupants.Contains(item)) ? Color.yellow : Color.green);
						Gizmos.DrawLine(base.transform.position, item.transform.position);
					}
					else
					{
						Gizmos.color = Color.red;
						Gizmos.DrawLine(base.transform.position, item.transform.position);
						if (item.MyCar != null)
						{
							Gizmos.DrawLine(item.MyCar.transform.position, item.transform.position);
						}
					}
				}
			});
			Gizmos.color = Color.white;
		}
	}

	public void Serialize(WriteDictionary dict)
	{
		string key = "Path";
		object value;
		if (this.Path == null)
		{
			value = new List<SVector3>();
		}
		else
		{
			value = (from x in this.Path
			select x).ToList<SVector3>();
		}
		dict[key] = value;
		dict["GoHome"] = this.GoHome;
		dict["DeColor3"] = this.MyColor;
		dict["currentNode"] = this.currentNode;
		dict["Debug"] = this.Debug;
		dict["TrafficWait"] = this.TrafficWait;
		if (this.Goal != null)
		{
			dict["Goal"] = true;
			dict["GoalId"] = this.Goal.ID;
			Vector2 pos = this.Goal.GetPos();
			dict["GoalX"] = (int)pos.x;
			dict["GoalY"] = (int)pos.y;
		}
	}

	public bool Deserialize(WriteDictionary dict)
	{
		this.Path = (from x in dict.Get<List<SVector3>>("Path", new List<SVector3>())
		select x).ToList<Vector2>();
		if (this.Path.Count == 0)
		{
			this.Path = null;
		}
		this.GoHome = dict.Get<bool>("GoHome", false);
		this.currentNode = dict.Get<int>("currentNode", 0);
		this.Debug = dict.Get<bool>("Debug", true);
		this.TrafficWait = dict.Get<float>("TrafficWait", -1f);
		this.MyColor = dict.Get<SVector3>("DeColor3", this.ColorPick.Evaluate(UnityEngine.Random.value));
		this.Car.UpdateColor(this.MyColor);
		this.Car.BindOccupants();
		bool flag = dict.Get<bool>("Goal", false);
		if (flag)
		{
			int x2 = dict.Get<int>("GoalX", 0);
			int y = dict.Get<int>("GoalY", 0);
			int id = dict.Get<int>("GoalId", -1);
			RoadSegment segment = RoadManager.Instance.GetSegment(x2, y);
			if (segment != null)
			{
				RoadNode roadNode = segment.Parking.FirstOrDefault((RoadNode z) => z.ID == id);
				if (!(roadNode != null))
				{
					return false;
				}
				this.Goal = roadNode;
				if (this.Car.Parked || (this.Path != null && this.currentNode < this.Path.Count))
				{
					this.Goal.Taken = true;
				}
			}
		}
		return true;
	}

	public Gradient ColorPick;

	public List<Vector2> Path;

	public bool GoHome;

	public bool Debug;

	private float TrafficWait = -1f;

	private float UltimateWait = -1f;

	public int DeltaTest = 1;

	private int DeltaCountdown;

	private Color DeColor;

	private int currentNode;

	private RoadNode Goal;

	public CarScript Car;

	public Color MyColor;
}
