﻿using System;
using UnityEngine;

public class CubeSpin : MonoBehaviour
{
	private void Start()
	{
		this.target = Quaternion.LookRotation(UnityEngine.Random.onUnitSphere);
		this.countDown = UnityEngine.Random.Range(1f, 4f);
	}

	private void Update()
	{
		if (GameSettings.GameSpeed > 0f)
		{
			this.countDown -= Time.deltaTime * GameSettings.GameSpeed;
			if (this.countDown <= 0f)
			{
				this.target = Quaternion.LookRotation(UnityEngine.Random.onUnitSphere);
				this.countDown = UnityEngine.Random.Range(1f, 4f);
			}
			base.transform.rotation = Quaternion.Lerp(base.transform.rotation, this.target, Time.deltaTime * GameSettings.GameSpeed);
		}
	}

	private Quaternion target;

	private float countDown;
}
