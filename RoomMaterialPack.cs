﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DevConsole;
using Steamworks;
using UnityEngine;

public class RoomMaterialPack : IWorkshopItem
{
	private RoomMaterialPack(string root, XMLParser.XMLNode rootNode, bool fromLocal)
	{
		this.ItemTitle = rootNode.Name;
		this.Root = root;
		this.Materials = rootNode.Children.Select(new Func<XMLParser.XMLNode, RoomMaterialController.WallMaterial>(this.ParseXML)).ToArray<RoomMaterialController.WallMaterial>();
		if (!fromLocal)
		{
			for (int i = 0; i < this.Materials.Length; i++)
			{
				this.Materials[i].FromSteam = true;
			}
		}
	}

	public string ItemTitle { get; set; }

	public PublishedFileId_t? SteamID { get; set; }

	public bool SetTitle
	{
		get
		{
			return this._setTitle;
		}
		set
		{
			this._setTitle = value;
		}
	}

	public bool CanUpload
	{
		get
		{
			return this._canUpload;
		}
		set
		{
			this._canUpload = value;
		}
	}

	private RoomMaterialController.WallMaterial ParseXML(XMLParser.XMLNode node)
	{
		string value = node.GetNode("Category", true).Value;
		if (!RoomMaterialPack.Categories.Contains(value))
		{
			throw new Exception("Got non existent category: " + value);
		}
		string name = node.Name;
		string category = value;
		string texturePath = this.GetTexturePath(node, "Base");
		string texturePath2 = this.GetTexturePath(node, "Bump");
		string texturePath3 = this.GetTexturePath(node, "Extra");
		bool? nodeValueOptional = node.GetNodeValueOptional<bool>("Skirting");
		return new RoomMaterialController.WallMaterial(name, category, texturePath, texturePath2, texturePath3, (nodeValueOptional == null) ? value.Equals("Interior") : nodeValueOptional.Value);
	}

	private string GetTexturePath(XMLParser.XMLNode node, string name)
	{
		string nodeValue = node.GetNodeValue(name, null);
		return (nodeValue != null) ? Path.Combine(this.Root, nodeValue) : null;
	}

	public static RoomMaterialPack LoadPack(string root, bool fromLocal, ref bool errors)
	{
		string text = Path.Combine(root, "materials.xml");
		string text2 = Path.GetFileNameWithoutExtension(root);
		if (File.Exists(text))
		{
			try
			{
				XMLParser.XMLNode xmlnode = XMLParser.ParseXML(Utilities.ReadOnlyReadAllText(text));
				text2 = xmlnode.TryGetAttribute("Name", text2);
				return new RoomMaterialPack(root, xmlnode, fromLocal);
			}
			catch (Exception ex)
			{
				Debug.LogException(new Exception("Failed loading material pack " + text2 + ":\n" + ex.ToString()));
			}
		}
		else
		{
			Debug.LogError("Failed loading material pack " + text2 + " due to missing materials.xml");
		}
		errors = true;
		if (Options.ConsoleOnError && !DevConsole.Console.isOpen)
		{
			DevConsole.Console.Open();
		}
		return null;
	}

	public string GetWorkshopType()
	{
		return "Material";
	}

	public string FolderPath()
	{
		return Path.GetFullPath(this.Root);
	}

	public string[] GetValidExts()
	{
		return new string[]
		{
			"png",
			"xml",
			"txt"
		};
	}

	public string[] ExtraTags()
	{
		return (from x in this.Materials
		select x.Category).Distinct<string>().ToArray<string>();
	}

	public string GetThumbnail()
	{
		string text = Path.Combine(this.FolderPath(), "Thumbnail.png");
		return (!File.Exists(text)) ? null : text;
	}

	public string Root;

	public RoomMaterialController.WallMaterial[] Materials;

	private bool _canUpload = true;

	private bool _setTitle = true;

	public static HashSet<string> Categories = new HashSet<string>
	{
		"Floor",
		"Interior",
		"Exterior"
	};
}
