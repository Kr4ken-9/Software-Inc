﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentEditor : MonoBehaviour
{
	public void Show(EnvironmentEditor.EditorType type)
	{
		this.CurrentType = type;
		switch (this.CurrentType)
		{
		case EnvironmentEditor.EditorType.Skyscraper:
			this.InitSkyscraper(true);
			this.InitHouse(false);
			this.InitTrees(false);
			break;
		case EnvironmentEditor.EditorType.House:
			this.InitSkyscraper(false);
			this.InitHouse(true);
			this.InitTrees(false);
			break;
		case EnvironmentEditor.EditorType.Trees:
			this.InitSkyscraper(false);
			this.InitHouse(false);
			this.InitTrees(true);
			break;
		default:
			return;
		}
		BuildController.Instance.ClearBuild(false, false, false, true);
		base.gameObject.SetActive(true);
		if (GameSettings.Instance.ActiveFloor != 0)
		{
			GameSettings.Instance.ActiveFloor = 0;
			Furniture.UpdateEdgeDetection();
			GameSettings.Instance.sRoomManager.ChangeFloor();
		}
	}

	private void InitSkyscraper(bool active)
	{
		this.SkyscraperViz.gameObject.SetActive(active);
		this._skyscraperState = 0;
		this._skyHeight = 4f;
	}

	private void InitHouse(bool active)
	{
		this.HouseMesh.gameObject.SetActive(active);
		if (active)
		{
			this.SetHouseMesh(-1, 1f);
		}
	}

	private void SetHouseMesh(int idx = -1, float scale = 1f)
	{
		if (idx == -1)
		{
			idx = this._currentHouse;
		}
		BurbHouse burbHouse = RoadManager.Instance.BurbHousePrefabs[idx];
		this.HouseMesh.sharedMesh = burbHouse.Rend.GetComponent<MeshFilter>().sharedMesh;
		this.HouseMesh.transform.localPosition = burbHouse.Rend.transform.localPosition;
		this.HouseMesh.transform.localRotation = burbHouse.Rend.transform.localRotation;
		this.HouseMesh.transform.localScale = burbHouse.Rend.transform.localScale * scale;
	}

	private void InitTrees(bool active)
	{
		this.TreeViz.gameObject.SetActive(active);
	}

	public static bool DisableScroll()
	{
		return EnvironmentEditor.Instance != null && EnvironmentEditor.Instance.gameObject.activeSelf && EnvironmentEditor.Instance.CurrentType != EnvironmentEditor.EditorType.Skyscraper;
	}

	private void Awake()
	{
		if (EnvironmentEditor.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		EnvironmentEditor.Instance = this;
		base.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		if (EnvironmentEditor.Instance == this)
		{
			EnvironmentEditor.Instance = null;
		}
	}

	private void Update()
	{
		if (Input.GetMouseButtonUp(1))
		{
			if (this._addedTrees)
			{
				this._addedTrees = false;
				GameSettings.Instance.BatchTempTrees();
			}
			this.CleanUpTreeUndos();
			base.gameObject.SetActive(false);
			return;
		}
		if (!GUICheck.OverGUI)
		{
			switch (this.CurrentType)
			{
			case EnvironmentEditor.EditorType.Skyscraper:
				this.SkyscraperEditor();
				break;
			case EnvironmentEditor.EditorType.House:
				this.HouseEditor();
				break;
			case EnvironmentEditor.EditorType.Trees:
				this.TreeEditor();
				break;
			default:
				base.gameObject.SetActive(false);
				break;
			}
		}
	}

	private void SkyscraperEditor()
	{
		if (this._skyscraperState == 0)
		{
			Vector2 mouseProj = HUD.Instance.GetMouseProj(0f, false);
			SkraperGen skraperGen = null;
			for (int i = 0; i < RoadManager.Instance.Landmarks.Count; i++)
			{
				SkraperGen skraperGen2 = RoadManager.Instance.Landmarks[i] as SkraperGen;
				if (skraperGen2 != null && skraperGen2.Blob.Contains(mouseProj))
				{
					skraperGen = skraperGen2;
					this._skyP1 = skraperGen2.Blob.min - Vector2.one * 0.1f;
					this._skyP2 = skraperGen2.Blob.max + Vector2.one * 0.1f;
					this._skyHeight = skraperGen2.Height + 0.1f;
					break;
				}
			}
			if (skraperGen != null)
			{
				ErrorOverlay.Instance.ShowError("EnvDestroyHint", false, false, 0f, true);
				if (Input.GetMouseButtonUp(0) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
				{
					GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
					{
						new UndoObject.UndoAction(skraperGen, true)
					});
					UnityEngine.Object.Destroy(skraperGen.gameObject);
				}
				this.UpdateSkyscraperViz();
				return;
			}
			this._skyHeight = 4f;
		}
		if (Input.GetMouseButtonUp(0))
		{
			this._skyscraperState++;
			if (this._skyscraperState == 3)
			{
				float xmin = Mathf.Floor(Mathf.Min(this._skyP1.x, this._skyP2.x)) - 1f;
				float ymin = Mathf.Floor(Mathf.Min(this._skyP1.y, this._skyP2.y)) - 1f;
				float xmax = Mathf.Ceil(Mathf.Max(this._skyP1.x, this._skyP2.x)) + 1f;
				float ymax = Mathf.Ceil(Mathf.Max(this._skyP1.y, this._skyP2.y)) + 1f;
				SkraperGen mark = RoadManager.Instance.PlaceBuilding(Rect.MinMaxRect(xmin, ymin, xmax, ymax), UnityEngine.Random.value > 0.5f, UnityEngine.Random.value > 0.5f, this._skyHeight);
				GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
				{
					new UndoObject.UndoAction(mark, false)
				});
				this._skyscraperState = 0;
				this._skyHeight = 4f;
			}
		}
		else if (this._skyscraperState == 2)
		{
			Vector3 inPoint = ((this._skyP1 + this._skyP2) * 0.5f).ToVector3(0f);
			Vector3 inNormal = CameraScript.Instance.mainCam.transform.forward;
			Vector3 vector = new Vector3(inNormal.x, 0f, inNormal.z);
			inNormal = vector.normalized;
			Plane plane = new Plane(inNormal, inPoint);
			Ray ray = CameraScript.Instance.mainCam.ScreenPointToRay(Input.mousePosition);
			float distance = 0f;
			plane.Raycast(ray, out distance);
			this._skyHeight = Mathf.Clamp(Mathf.Round(ray.GetPoint(distance).y / 2f) * 2f, 4f, 30f);
		}
		else
		{
			Vector2 mouseProj2 = HUD.Instance.GetMouseProj(0f, false);
			mouseProj2 = new Vector2(Mathf.Floor(mouseProj2.x) + 0.5f, Mathf.Floor(mouseProj2.y) + 0.5f);
			this._skyP2 = this.Clamp(mouseProj2, this.Bounds.Expand(-3f, -3f));
			if (this._skyscraperState == 0)
			{
				this._skyP1 = this._skyP2;
			}
		}
		this.UpdateSkyscraperViz();
	}

	private Vector2 Clamp(Vector2 v, Rect r)
	{
		return new Vector2(Mathf.Clamp(v.x, r.xMin, r.xMax), Mathf.Clamp(v.y, r.yMin, r.yMax));
	}

	private void UpdateSkyscraperViz()
	{
		float num = Mathf.Floor(Mathf.Min(this._skyP1.x, this._skyP2.x)) - 1f;
		float num2 = Mathf.Floor(Mathf.Min(this._skyP1.y, this._skyP2.y)) - 1f;
		float num3 = Mathf.Ceil(Mathf.Max(this._skyP1.x, this._skyP2.x)) + 1f;
		float num4 = Mathf.Ceil(Mathf.Max(this._skyP1.y, this._skyP2.y)) + 1f;
		this.SkyscraperViz.position = new Vector3((num + num3) / 2f, this._skyHeight / 2f, (num2 + num4) / 2f);
		this.SkyscraperViz.localScale = new Vector3(num3 - num, this._skyHeight, num4 - num2);
	}

	private void HouseEditor()
	{
		if (Input.mouseScrollDelta.y > 0f)
		{
			this._currentHouse = (this._currentHouse + 1) % RoadManager.Instance.BurbHousePrefabs.Length;
		}
		if (Input.mouseScrollDelta.y < 0f)
		{
			this._currentHouse--;
			if (this._currentHouse < 0)
			{
				this._currentHouse = RoadManager.Instance.BurbHousePrefabs.Length - 1;
			}
		}
		Vector2 mouseProj = HUD.Instance.GetMouseProj(0f, false);
		for (int i = 0; i < RoadManager.Instance.Landmarks.Count; i++)
		{
			BurbHouse burbHouse = RoadManager.Instance.Landmarks[i] as BurbHouse;
			if (burbHouse != null && burbHouse.NavMesh.GetBounds().Contains(mouseProj))
			{
				if (Input.GetMouseButtonUp(0) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
				{
					GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
					{
						new UndoObject.UndoAction(burbHouse, true)
					});
					UnityEngine.Object.Destroy(burbHouse.gameObject);
				}
				else
				{
					this.SetHouseMesh(burbHouse.Idx, 1.1f);
					this.HouseViz.position = burbHouse.transform.position;
					this.HouseViz.rotation = burbHouse.transform.rotation;
				}
				ErrorOverlay.Instance.ShowError("EnvDestroyHint", false, false, 0f, true);
				return;
			}
		}
		float roadSize = RoadManager.Instance.RoadSize;
		int num = Mathf.FloorToInt(mouseProj.x / roadSize);
		int num2 = Mathf.FloorToInt(mouseProj.y / roadSize);
		Vector3 forward = Vector3.left;
		float num3 = (this.GetRoad(num - 1, num2) <= 0) ? 9f : (mouseProj.x % roadSize);
		int num4 = 0;
		float num5 = (this.GetRoad(num, num2 - 1) <= 0) ? 9f : (mouseProj.y % roadSize);
		if (num5 < num3)
		{
			num4 = 1;
			num3 = num5;
		}
		num5 = ((this.GetRoad(num + 1, num2) <= 0) ? 9f : (roadSize - mouseProj.x % roadSize));
		if (num5 < num3)
		{
			num4 = 2;
			num3 = num5;
		}
		num5 = ((this.GetRoad(num, num2 + 1) <= 0) ? 9f : (roadSize - mouseProj.y % roadSize));
		if (num5 < num3)
		{
			num4 = 3;
		}
		switch (num4)
		{
		case 0:
			mouseProj = new Vector2(Mathf.Floor(mouseProj.x / roadSize) * roadSize, mouseProj.y);
			forward = Vector3.left;
			break;
		case 1:
			mouseProj = new Vector2(mouseProj.x, Mathf.Floor(mouseProj.y / roadSize) * roadSize);
			forward = Vector3.back;
			break;
		case 2:
			mouseProj = new Vector2(Mathf.Ceil(mouseProj.x / roadSize) * roadSize, mouseProj.y);
			forward = Vector3.right;
			break;
		case 3:
			mouseProj = new Vector2(mouseProj.x, Mathf.Ceil(mouseProj.y / roadSize) * roadSize);
			forward = Vector3.forward;
			break;
		}
		this.HouseViz.position = this.Clamp(mouseProj, this.Bounds).ToVector3(0f);
		this.HouseViz.rotation = Quaternion.LookRotation(forward);
		if (Input.GetMouseButtonUp(0))
		{
			BurbHouse mark = RoadManager.Instance.PlaceHouse(this._currentHouse, this.HouseViz.position, this.HouseViz.rotation);
			GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
			{
				new UndoObject.UndoAction(mark, false)
			});
		}
		this.SetHouseMesh(-1, 1f);
	}

	public byte GetRoad(int x, int y)
	{
		return RoadManager.Instance.GetRoad(x, y);
	}

	private void CleanUpTreeUndos()
	{
		if (this._treeAddUndos.Count == 0 && this._treeRemoveUndos.Count == 0)
		{
			return;
		}
		List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
		if (this._treeAddUndos.Count > 0)
		{
			list.Add(new UndoObject.UndoAction(this._treeAddUndos.ToArray(), true));
			this._treeAddUndos.Clear();
		}
		if (this._treeRemoveUndos.Count > 0)
		{
			list.Add(new UndoObject.UndoAction(this._treeRemoveUndos.ToArray(), false));
			this._treeRemoveUndos.Clear();
		}
		if (list.Count > 0)
		{
			GameSettings.Instance.AddUndo(list.ToArray());
		}
	}

	private void TreeEditor()
	{
		this._TreeDrawSize = Mathf.Clamp(this._TreeDrawSize + Input.mouseScrollDelta.y, 1f, 15f);
		Vector2 mouseProj = HUD.Instance.GetMouseProj(0f, false);
		this.TreeViz.position = mouseProj.ToVector3(0f);
		this.TreeViz.localScale = new Vector3(this._TreeDrawSize, 1f, this._TreeDrawSize);
		if (Input.GetMouseButton(0))
		{
			if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
			{
				this._lastPlace = Vector2.one * -10f;
				Vector2 a = Vector2.one * this._TreeDrawSize;
				this._treeRes.Clear();
				GameSettings.Instance.TreeTree.Query(new Rect(mouseProj - a * 0.5f, Vector2.one * this._TreeDrawSize), this._treeRes);
				float num = Mathf.Pow(this._TreeDrawSize / 2f, 2f);
				for (int i = 0; i < this._treeRes.Count; i++)
				{
					if ((mouseProj - this._treeRes[i].GetPos()).sqrMagnitude > num)
					{
						this._treeRes.RemoveAt(i);
						i--;
					}
				}
				if (this._treeRes.Count > 0)
				{
					for (int j = 0; j < this._treeRes.Count; j++)
					{
						global::TreeInstance treeInstance = this._treeRes[j];
						this._treeAddUndos.Add(treeInstance);
						treeInstance.BelongsTo.RemoveTree(treeInstance);
						GameSettings.Instance.Trees.Remove(treeInstance);
						GameSettings.Instance.TreeTree.removeItem(treeInstance);
					}
				}
			}
			else
			{
				float num2 = Mathf.Max(2f, this._TreeDrawSize);
				if ((mouseProj - this._lastPlace).magnitude > num2)
				{
					this._addedTrees = true;
					int num3 = Mathf.Max(1, Mathf.FloorToInt(num2 / 4f));
					Vector2 a2 = mouseProj - Vector2.one * num2 / 2f;
					float num4 = num2 / (float)num3;
					for (int k = 0; k < num3; k++)
					{
						for (int l = 0; l < num3; l++)
						{
							Vector2 b = new Vector2((UnityEngine.Random.value - 0.5f) * (num4 / 2f - 1f), (UnityEngine.Random.value - 0.5f) * (num4 / 2f - 1f));
							Vector2 vector = a2 + new Vector2((float)k * num4 + num4 / 2f, (float)l * num4 + num4 / 2f) + b;
							if (this.Bounds.Contains(vector))
							{
								this._treeRemoveUndos.Add(GameSettings.Instance.AddTree(vector.ToVector3(0f), true));
							}
						}
					}
					this._lastPlace = mouseProj;
				}
			}
		}
		else
		{
			ErrorOverlay.Instance.ShowError("EnvDestroyHint", false, false, 0f, true);
			this._lastPlace = Vector2.one * -10f;
			if (this._addedTrees)
			{
				this._addedTrees = false;
				GameSettings.Instance.BatchTempTrees();
			}
			this.CleanUpTreeUndos();
		}
	}

	public static EnvironmentEditor Instance;

	public Transform SkyscraperViz;

	public Transform HouseViz;

	public Transform TreeViz;

	private Vector2 _skyP1;

	private Vector2 _skyP2;

	private Vector2 _lastPlace;

	private float _skyHeight = 4f;

	private float _TreeDrawSize = 1f;

	private int _skyscraperState;

	private int _currentHouse;

	private bool _addedTrees;

	public MeshFilter HouseMesh;

	public Rect Bounds;

	public EnvironmentEditor.EditorType CurrentType;

	[NonSerialized]
	private List<global::TreeInstance> _treeRes = new List<global::TreeInstance>();

	[NonSerialized]
	private List<global::TreeInstance> _treeAddUndos = new List<global::TreeInstance>();

	[NonSerialized]
	private List<global::TreeInstance> _treeRemoveUndos = new List<global::TreeInstance>();

	public enum EditorType
	{
		Skyscraper,
		House,
		Trees
	}
}
