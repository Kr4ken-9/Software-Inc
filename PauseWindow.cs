﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseWindow : MonoBehaviour
{
	public void ToggleShow()
	{
		this.Panel.SetActive(!this.Panel.activeSelf);
		if (this.Panel.activeSelf)
		{
			GameSettings.ForcePause = true;
			GameSettings.FreezeGame = true;
			this.MoveButton.SetActive(!GameSettings.Instance.EditMode);
			WindowManager.DisableAll(true, null);
		}
		else
		{
			OptionsWindow.Instance.Window.Close();
			SaveGameManager.Instance.SaveGameWindow.Close();
			GameSettings.ForcePause = false;
			WindowManager.EnableAll();
		}
	}

	public void CaptureScreenshot()
	{
		GameSettings.Instance.StartCoroutine(this.ReportScreen());
		this.ToggleShow();
	}

	public IEnumerator ReportScreen()
	{
		HUD.Instance.BlurScript.blurSize = 0f;
		yield return null;
		string path = Path.Combine(Path.GetFullPath("./"), "ScreenCap.png");
		Application.CaptureScreenshot(path);
		yield return null;
		FeedbackWindow.Instance.Show(FeedbackWindow.ReportTypes.Feedback, false, false, new string[]
		{
			path
		});
		yield break;
	}

	public void DoAction(int i)
	{
		switch (i)
		{
		case 0:
			this.ToggleShow();
			break;
		case 1:
			if (GameSettings.Instance.AssociatedSave != null)
			{
				GameSettings.Instance.AssociatedSave.SaveNow(false, true, SaveGameManager.CurrentMode);
			}
			else if (this.CheckRentModeError())
			{
				SaveGameManager.Instance.Show(true, false, GameSettings.Instance.EditMode, false, null);
			}
			break;
		case 2:
			if (this.CheckRentModeError())
			{
				SaveGameManager.Instance.Show(true, false, GameSettings.Instance.EditMode, false, null);
			}
			break;
		case 3:
			SaveGameManager.Instance.Show(false, false, false, false, null);
			break;
		case 4:
			OptionsWindow.Instance.Show();
			break;
		case 5:
			GameSettings.UnloadNow();
			ErrorLogging.FirstOfScene = true;
			ErrorLogging.SceneChanging = true;
			SceneManager.LoadScene("MainMenu");
			break;
		case 6:
			GameSettings.UnloadNow();
			Process.GetCurrentProcess().Kill();
			break;
		case 7:
			SaveGameManager.Instance.Show(false, false, true, true, delegate(SaveGame x)
			{
				try
				{
					SaveGameManager.Instance.ShowWaitPanel();
					float num;
					if (GameSettings.Instance.RentMode)
					{
						num = (from y in GameSettings.Instance.sRoomManager.AllFurniture
						where !y.PlacedInEditMode
						select y).SumSafe((Furniture y) => y.GetSellPrice());
					}
					else
					{
						num = GameSettings.Instance.GetMapCost(true);
					}
					float num2 = num;
					bool flag = false;
					SaveGame saveGame = null;
					if (!GameSettings.Instance.RentMode && GameSettings.Instance.sRoomManager.Rooms.Count > 0)
					{
						saveGame = SaveGameManager.Instance.BuildingSave();
						flag = true;
					}
					if (flag && saveGame == null)
					{
						SaveGameManager.Instance.HideWaitPanel();
					}
					else if (x == null)
					{
						num2 -= PlotArea.StartPlotPrice;
						if (!GameSettings.Instance.MyCompany.CanMakeTransaction(num2))
						{
							WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), true, DialogWindow.DialogType.Error);
							SaveGameManager.Instance.DeleteSave(saveGame, false);
							SaveGameManager.Instance.HideWaitPanel();
						}
						else
						{
							SaveGameManager.Instance.AutoSave();
							GameSettings.Instance.MyCompany.MakeTransaction(num2, Company.TransactionCategory.Construction, "Relocation");
							byte[] companyData = GameReader.CreateDictionaryData(GameReader.LoadMode.Company);
							GameData.DaysPerMonth = GameSettings.DaysPerMonth;
							SaveGameManager.LoadGame(null, companyData, false, true, false, false);
						}
					}
					else
					{
						GameData.LoadYear = SDateTime.Now().RealYear;
						float[] buildMeta = x.GetBuildMeta();
						bool flag2 = false;
						if (buildMeta[0] == 1f)
						{
							num2 -= buildMeta[2];
							if (!GameSettings.Instance.MyCompany.CanMakeTransaction(num2))
							{
								flag2 = true;
								WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), true, DialogWindow.DialogType.Error);
							}
						}
						byte[] companyData2 = null;
						if (!flag2)
						{
							SaveGameManager.Instance.AutoSave();
							GameSettings.Instance.MyCompany.MakeTransaction(num2, Company.TransactionCategory.Construction, "Relocation");
							companyData2 = GameReader.CreateDictionaryData(GameReader.LoadMode.Company);
							GameData.DaysPerMonth = GameSettings.DaysPerMonth;
						}
						if (flag2 || !SaveGameManager.LoadGame(x, companyData2, false, true, true, false))
						{
							if (!flag2)
							{
								GameSettings.Instance.MyCompany.MakeTransaction(-num2, Company.TransactionCategory.Construction, "Relocation");
							}
							SaveGameManager.Instance.DeleteSave(saveGame, false);
							SaveGameManager.Instance.HideWaitPanel();
							GameData.LoadYear = 0;
						}
					}
				}
				catch (Exception exception)
				{
					SaveGameManager.Instance.HideWaitPanel();
					UnityEngine.Debug.LogException(exception);
				}
			});
			break;
		}
	}

	private bool CheckRentModeError()
	{
		if (GameSettings.Instance.EditMode && GameSettings.Instance.RentMode)
		{
			if (!GameSettings.Instance.sRoomManager.Rooms.Any((Room x) => x.PlayerOwned && x.Rentable))
			{
				WindowManager.Instance.ShowMessageBox("RentModeEditorError".Loc(), true, DialogWindow.DialogType.Error);
				return false;
			}
		}
		return true;
	}

	public GameObject Panel;

	public GameObject MoveButton;
}
