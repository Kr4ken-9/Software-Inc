﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using DevConsole;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackWindow : MonoBehaviour
{
	private void Awake()
	{
		if (FeedbackWindow.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		FeedbackWindow.Instance = this;
	}

	private void OnDestroy()
	{
		if (FeedbackWindow.Instance == this)
		{
			FeedbackWindow.Instance = null;
		}
	}

	public void Show()
	{
		this.Show(FeedbackWindow.ReportTypes.Feedback, false, false, new string[0]);
	}

	public void AddSave(bool doIt)
	{
		if (MainMenuController.Instance != null && MainMenuController.Instance.CurrentSaveGame != null)
		{
			string fullPath = Path.GetFullPath(MainMenuController.Instance.CurrentSaveGame.FilePath);
			if (doIt)
			{
				this._files.Add(fullPath);
			}
			else
			{
				this._files.Remove(fullPath);
			}
		}
		else if (GameSettings.Instance != null)
		{
			SaveGame saveGame = GameSettings.Instance.AssociatedSave;
			if (saveGame == null)
			{
				saveGame = GameSettings.Instance.AssociatedAutoSave;
			}
			else if (GameSettings.Instance.AssociatedAutoSave != null)
			{
				saveGame = ((!(saveGame.RealTime > GameSettings.Instance.AssociatedAutoSave.RealTime)) ? GameSettings.Instance.AssociatedAutoSave : saveGame);
			}
			if (saveGame != null)
			{
				string fullPath2 = Path.GetFullPath(saveGame.FilePath);
				if (doIt)
				{
					this._files.Add(fullPath2);
				}
				else
				{
					this._files.Remove(fullPath2);
				}
			}
		}
		this.FileUploads.text = string.Join("\n", this._files.ToArray<string>());
	}

	public static string GetLogFile()
	{
		return "~/.config/unity3d/Coredumping/Software Inc/Player.log";
	}

	public void AddLog(bool doIt)
	{
		if (doIt)
		{
			this._files.Add(FeedbackWindow.GetLogFile());
		}
		else
		{
			this._files.Remove(FeedbackWindow.GetLogFile());
		}
		this.FileUploads.text = string.Join("\n", this._files.ToArray<string>());
	}

	public void Show(FeedbackWindow.ReportTypes type, bool save, bool log, params string[] extraFiles)
	{
		if (File.Exists("ReportID.txt"))
		{
			try
			{
				this.PreviousIDs.text = File.ReadAllText("ReportID.txt");
				LayoutRebuilder.ForceRebuildLayoutImmediate(this.PreviousPanel.GetComponent<RectTransform>());
			}
			catch (Exception)
			{
			}
		}
		this.PreviousPanel.SetActive(!string.IsNullOrEmpty(this.PreviousIDs.text));
		this.Exception = null;
		this._files.Clear();
		this._files.AddRange(extraFiles);
		bool flag = File.Exists(FeedbackWindow.GetLogFile());
		this.IncludeLog.gameObject.SetActive(flag && type == FeedbackWindow.ReportTypes.Feedback);
		this.IncludeLog.isOn = (flag && log);
		if (flag && log)
		{
			this.AddLog(true);
		}
		this.SpecToggle.isOn = false;
		if (type == FeedbackWindow.ReportTypes.Feedback)
		{
			this.IncludeSave.gameObject.SetActive((type != FeedbackWindow.ReportTypes.Crash && MainMenuController.Instance != null && MainMenuController.Instance.CurrentSaveGame != null) || (GameSettings.Instance != null && (GameSettings.Instance.AssociatedSave != null || GameSettings.Instance.AssociatedAutoSave != null)));
			this.IncludeSave.isOn = (this.IncludeSave.gameObject.activeSelf && save);
		}
		this.Type = type;
		this.Mail.text = string.Empty;
		this.Message.text = string.Empty;
		this.FileUploads.text = string.Join("\n", this._files.ToArray<string>());
		this.Window.Show();
	}

	private void Update()
	{
		if (this.CurrentJob != null)
		{
			if (this.CurrentJob.isDone || !string.IsNullOrEmpty(this.CurrentJob.error))
			{
				if (this._isRetry)
				{
					this._isRetry = false;
					this.CurrentJob = null;
					this.MainPanel.SetActive(true);
					this.ProgressPanel.SetActive(false);
					this.Window.Close();
					return;
				}
				string text = null;
				int num = 0;
				if (!string.IsNullOrEmpty(this.CurrentJob.error))
				{
					text = this.CurrentJob.error;
				}
				else if (this.CurrentJob.text == null || !this.CurrentJob.text.StartsWith("Success"))
				{
					text = this.CurrentJob.text;
				}
				else
				{
					try
					{
						num = Convert.ToInt32(this.CurrentJob.text.Substring(8));
					}
					catch (Exception)
					{
					}
				}
				this.CurrentJob = null;
				this.MainPanel.SetActive(true);
				this.ProgressPanel.SetActive(false);
				if (text == null)
				{
					FeedbackWindow._lastPost = Time.realtimeSinceStartup;
					WindowManager.SpawnDialog("ReportSuccess2".Loc(new object[]
					{
						num
					}), true, DialogWindow.DialogType.Information);
					this.PreviousPanel.SetActive(true);
					Text previousIDs = this.PreviousIDs;
					previousIDs.text = previousIDs.text + num.ToString() + "\n";
					LayoutRebuilder.ForceRebuildLayoutImmediate(this.PreviousPanel.GetComponent<RectTransform>());
					try
					{
						if (File.Exists("ReportID.txt"))
						{
							File.AppendAllText("ReportID.txt", "\n" + num.ToString());
						}
						else
						{
							File.WriteAllText("ReportID.txt", num.ToString());
						}
					}
					catch (Exception)
					{
					}
					this.Window.Close();
				}
				else
				{
					DialogWindow d = WindowManager.SpawnDialog();
					this.Window.Close();
					d.Show("ReportFailure".Loc() + "\n" + text, false, DialogWindow.DialogType.Error, new KeyValuePair<string, Action>[]
					{
						new KeyValuePair<string, Action>("Yes", delegate
						{
							this.SaveLastReport();
							WindowManager.SpawnDialog("TryLaterConfirm".Loc(), true, DialogWindow.DialogType.Question);
							d.Window.Close();
						}),
						new KeyValuePair<string, Action>("No", delegate
						{
							this.Window.Show();
							d.Window.Close();
						})
					});
				}
			}
			else
			{
				this.bar.Value = this.CurrentJob.uploadProgress;
			}
		}
	}

	public void StartUpload()
	{
		if (FeedbackWindow._lastPost > 0f && Time.realtimeSinceStartup - FeedbackWindow._lastPost < 300f)
		{
			WindowManager.SpawnDialog("ReportDelayError".Loc(), true, DialogWindow.DialogType.Error);
			return;
		}
		if (this.Mail.text.Trim().Length > 0 && !Regex.IsMatch(this.Mail.text, "\\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\\Z", RegexOptions.IgnoreCase))
		{
			WindowManager.SpawnDialog("MailInvalid".Loc(), true, DialogWindow.DialogType.Error);
			return;
		}
		if ((this.Type == FeedbackWindow.ReportTypes.Feedback || this.Type == FeedbackWindow.ReportTypes.Exception) && this.Message.text.Trim().Length < 50)
		{
			WindowManager.SpawnDialog("NoFeedbackError".Loc(), true, DialogWindow.DialogType.Error);
			return;
		}
		base.StartCoroutine(this.Upload());
	}

	public IEnumerator UploadSpecial(string mail, string message, string type, string version, string[] files, string deleteDir)
	{
		this._isRetry = true;
		WWWForm form = new WWWForm();
		form.AddField("Mail", mail);
		form.AddField("Message", message);
		form.AddField("Type", type);
		form.AddField("Version", version);
		foreach (string text in files)
		{
			if (File.Exists(text) && new FileInfo(text).Length < 15728640L)
			{
				using (FileStream fileStream = File.Open(text, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
				{
					BinaryReader reader = new BinaryReader(fileStream);
					form.AddBinaryData("Files[]", reader.ReadAllBytes(), Path.GetFileName(text));
				}
			}
		}
		this.MainPanel.SetActive(false);
		this.ProgressPanel.SetActive(true);
		this.CurrentJob = new WWW("https://www.coredumping.com/BugReporting/Report.php", form);
		WWW i = this.CurrentJob;
		yield return this.CurrentJob;
		MainMenuController.IsUploading = false;
		if (string.IsNullOrEmpty(i.error) && "Success".Equals(i.text))
		{
			try
			{
				Directory.Delete(deleteDir, true);
			}
			catch (Exception ex)
			{
				DevConsole.Console.LogError(ex.ToString());
			}
		}
		else
		{
			string str = (!string.IsNullOrEmpty(i.error)) ? i.error : i.text;
			DevConsole.Console.LogError("Failed sending report with error:\n" + str);
		}
		yield break;
	}

	private string GetSpecs()
	{
		return string.Format("OS: {0}\nMemory {1} MB\nProcessor:{2} cores: {3}\nGraphics: {4} {5}, {6} MB, v. {7} - {8}", new object[]
		{
			SystemInfo.operatingSystem,
			SystemInfo.systemMemorySize,
			SystemInfo.processorType,
			SystemInfo.processorCount,
			SystemInfo.graphicsDeviceVendor,
			SystemInfo.graphicsDeviceName,
			SystemInfo.graphicsMemorySize,
			SystemInfo.graphicsDeviceVersion,
			SystemInfo.graphicsDeviceType.ToString()
		});
	}

	private void SaveLastReport()
	{
		string text = Path.Combine(Utilities.GetRoot(), "BugReports");
		if (!Directory.Exists(text))
		{
			Directory.CreateDirectory(text);
		}
		DateTime now = DateTime.Now;
		string path = string.Format("{0}-{1}-{2}_{3}{4}{5}", new object[]
		{
			now.Year,
			now.Month.ToString("00"),
			now.Day.ToString("00"),
			now.Hour.ToString("00"),
			now.Minute.ToString("00"),
			now.Second.ToString("00")
		});
		string text2 = Path.Combine(text, path);
		Directory.CreateDirectory(text2);
		List<string> list = new List<string>();
		foreach (string text3 in this._lastFiles)
		{
			try
			{
				File.Copy(text3, Path.Combine(text2, Path.GetFileName(text3)));
				list.Add(Path.GetFileName(text3));
			}
			catch (Exception ex)
			{
				DevConsole.Console.LogError(ex.ToString());
			}
		}
		string text4 = "Root";
		XMLParser.XMLNode[] array = new XMLParser.XMLNode[5];
		array[0] = new XMLParser.XMLNode("Mail", this._lastMail, null);
		array[1] = new XMLParser.XMLNode("Message", this._lastMessage.Replace("<", "&#60;").Replace(">", "&#62;"), null);
		array[2] = new XMLParser.XMLNode("Version", Versioning.VersionString, null);
		array[3] = new XMLParser.XMLNode("Type", this._lastType, null);
		array[4] = new XMLParser.XMLNode("Files", (from x in list
		select new XMLParser.XMLNode("File", x, null)).ToArray<XMLParser.XMLNode>());
		XMLParser.XMLNode xmlnode = new XMLParser.XMLNode(text4, array);
		File.WriteAllText(Path.Combine(text2, "Report.xml"), XMLParser.ExportXML(xmlnode));
	}

	public IEnumerator Upload()
	{
		this._isRetry = false;
		WWWForm form = new WWWForm();
		form.AddField("Mail", this.Mail.text);
		string msg = this.Message.text;
		if (this.Exception != null)
		{
			msg = msg + "\n\n" + this.Exception;
		}
		if (this.SpecToggle.isOn)
		{
			msg = msg + "\n\n" + this.GetSpecs();
		}
		this._lastMessage = msg;
		this._lastMail = this.Mail.text;
		this._lastFiles = this._files.ToArray<string>();
		this._lastType = this.Type.ToString();
		form.AddField("Message", msg);
		form.AddField("Type", this.Type.ToString());
		form.AddField("Version", Versioning.VersionString);
		form.AddField("NewReport", "True");
		foreach (string text in this._files)
		{
			if (File.Exists(text) && new FileInfo(text).Length < 15728640L)
			{
				using (FileStream fileStream = File.Open(text, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
				{
					BinaryReader reader = new BinaryReader(fileStream);
					form.AddBinaryData("Files[]", reader.ReadAllBytes(), Path.GetFileName(text));
				}
			}
		}
		this.MainPanel.SetActive(false);
		this.ProgressPanel.SetActive(true);
		this.CurrentJob = new WWW("https://www.coredumping.com/BugReporting/Report.php", form);
		yield return this.CurrentJob;
		yield break;
	}

	public GUIWindow Window;

	public FeedbackWindow.ReportTypes Type;

	public InputField Mail;

	public InputField Message;

	public GUIProgressBar bar;

	public Text FileUploads;

	public Text PreviousIDs;

	public WWW CurrentJob;

	public GameObject MainPanel;

	public GameObject ProgressPanel;

	public GameObject PreviousPanel;

	public Toggle IncludeSave;

	public Toggle IncludeLog;

	public Toggle SpecToggle;

	[NonSerialized]
	public string Exception;

	[NonSerialized]
	private readonly HashSet<string> _files = new HashSet<string>();

	private string _lastMail;

	private string _lastMessage;

	private string _lastType;

	private string[] _lastFiles;

	private static float _lastPost = -1f;

	public static FeedbackWindow Instance;

	private bool _isRetry;

	public enum ReportTypes
	{
		Feedback,
		Exception,
		Crash
	}
}
