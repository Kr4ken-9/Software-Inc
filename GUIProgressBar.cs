﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GUIProgressBar : MaskableGraphic
{
	public float Value
	{
		get
		{
			return this._value;
		}
		set
		{
			this._value = Mathf.Clamp(value, (float)((!this.FromCenter) ? 0 : -1), 1f);
			this.SetVerticesDirty();
		}
	}

	public float? AltValue
	{
		get
		{
			return this._altValue;
		}
		set
		{
			this._altValue = value;
			this.SetVerticesDirty();
		}
	}

	protected override void OnPopulateMesh(VertexHelper vh)
	{
		vh.Clear();
		Vector2 a = new Vector2(-base.rectTransform.pivot.x * base.rectTransform.rect.width, -base.rectTransform.pivot.y * base.rectTransform.rect.height);
		Vector2 b = new Vector2((1f - base.rectTransform.pivot.x) * base.rectTransform.rect.width, (1f - base.rectTransform.pivot.y) * base.rectTransform.rect.height);
		this.DrawRect(a, b, this.color, vh);
		float num = (!this.Animated) ? this.Value : this._currentValue;
		float num2 = b.x - a.x;
		bool flag = Mathf.Approximately(num, 0f);
		if (flag && (this.FromCenter || this.AltValue == null || Mathf.Approximately(this.AltValue.Value, 0f)))
		{
			return;
		}
		if (this.FromCenter)
		{
			if (num < 0f)
			{
				float num3 = 1f + num;
				this.DrawRect(new Vector2(a.x + num2 * num3 * 0.5f, a.y), new Vector2(a.x + num2 / 2f, b.y), (!this.NoCenterColor) ? Color.Lerp(this.EndExt, this.StartColor, num3) : this.StartColor, this.StartColor, vh);
			}
			else
			{
				this.DrawRect(new Vector2(a.x + num2 / 2f, a.y), new Vector2(a.x + num2 / 2f + num2 * num * 0.5f, b.y), (!this.NoCenterColor) ? this.StartColor : this.EndColor, (!this.NoCenterColor) ? Color.Lerp(this.StartColor, this.EndColor, num) : this.EndColor, vh);
			}
		}
		else
		{
			if (!flag)
			{
				if (this.OnlyUseStart)
				{
					this.DrawRect(a, new Vector2(a.x + num2 * num, b.y), this.StartColor, vh);
				}
				else
				{
					this.DrawRect(a, new Vector2(a.x + num2 * num, b.y), this.StartColor, Color.Lerp(this.StartColor, this.EndColor, num), vh);
				}
			}
			if (this.AltValue != null && !Mathf.Approximately(this.AltValue.Value, 0f))
			{
				if (this.AltValue.Value > num)
				{
					this.DrawRect(new Vector2(a.x + num2 * num, a.y + 4f), new Vector2(a.x + num2 * this.AltValue.Value, b.y - 4f), new Color(0.8f, 0.8f, 0.8f, 0.8f), vh);
				}
				else
				{
					this.DrawRect(new Vector2(a.x, a.y + 4f), new Vector2(a.x + num2 * this.AltValue.Value, b.y - 4f), new Color(1f, 1f, 1f, 0.8f), vh);
				}
			}
		}
	}

	private void DrawRect(Vector2 a, Vector2 b, Color c, VertexHelper vh)
	{
		vh.AddUIVertexQuad(new UIVertex[]
		{
			new UIVertex
			{
				color = c,
				position = new Vector3(a.x, a.y, 0f)
			},
			new UIVertex
			{
				color = c,
				position = new Vector3(a.x, b.y, 0f)
			},
			new UIVertex
			{
				color = c,
				position = new Vector3(b.x, b.y, 0f)
			},
			new UIVertex
			{
				color = c,
				position = new Vector3(b.x, a.y, 0f)
			}
		});
	}

	private void DrawRect(Vector2 a, Vector2 b, Color c, Color c2, VertexHelper vh)
	{
		vh.AddUIVertexQuad(new UIVertex[]
		{
			new UIVertex
			{
				color = c,
				position = new Vector3(a.x, a.y, 0f)
			},
			new UIVertex
			{
				color = c,
				position = new Vector3(a.x, b.y, 0f)
			},
			new UIVertex
			{
				color = c2,
				position = new Vector3(b.x, b.y, 0f)
			},
			new UIVertex
			{
				color = c2,
				position = new Vector3(b.x, a.y, 0f)
			}
		});
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		this._currentValue = 0f;
	}

	private void FixedUpdate()
	{
		if (this.Animated && this._currentValue != this.Value)
		{
			this._currentValue = Mathf.Lerp(this._currentValue, this.Value, Time.deltaTime * 8f);
			if (Mathf.Approximately(this._currentValue, this.Value))
			{
				this._currentValue = this.Value;
			}
			this.SetVerticesDirty();
		}
	}

	[SerializeField]
	private float _value = 0.5f;

	[NonSerialized]
	private float? _altValue;

	public Color StartColor;

	public Color EndColor;

	public Color EndExt;

	public bool FromCenter;

	public bool NoCenterColor;

	public bool StartMidColor;

	public bool OnlyUseStart;

	public bool Animated;

	[NonSerialized]
	private float _currentValue;
}
