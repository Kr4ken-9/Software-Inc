﻿using System;

public struct Currency
{
	public Currency(string name, string pre, string post, float rate)
	{
		this.Name = name;
		this.Prefix = pre;
		this.Postfix = post;
		this.Rate = rate;
	}

	public Currency(XMLParser.XMLNode node)
	{
		this.Name = node.GetNode("Name", true).Value;
		this.Prefix = node.GetNode("Prefix", true).Value;
		this.Postfix = node.GetNode("Postfix", true).Value;
		this.Rate = (float)Convert.ToDouble(node.GetNode("Rate", true).Value);
	}

	public readonly string Name;

	public readonly string Prefix;

	public readonly string Postfix;

	public readonly float Rate;
}
