﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ButtonCounter : MonoBehaviour
{
	public void SetNumber(int n)
	{
		base.gameObject.SetActive(n > 0);
		if (n > 0)
		{
			this.CounterLabel.text = Mathf.Min(9, n) + ((n <= 9) ? string.Empty : "+");
		}
	}

	public Text CounterLabel;
}
