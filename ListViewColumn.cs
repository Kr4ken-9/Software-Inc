﻿using System;
using System.Collections.Generic;
using System.Linq;

public class ListViewColumn<T>
{
	public ListViewColumn(string name, string actionName, Action<T> action, float w = 1f)
	{
		this.Name = name;
		this.ActionName = actionName;
		this.Action = action;
		this.IsAction = true;
		this.Width = w;
	}

	public ListViewColumn(string name, Func<T, string> output, Comparison<T> compare, float w = 1f)
	{
		this.Name = name;
		this.Output = output;
		this.IsAction = false;
		this.Width = w;
		this.Compare = compare;
	}

	public void SortSpecific(List<T> input, List<int> selected, bool ascending)
	{
		this.SortAsc = ascending;
		this.Sort(input, selected);
	}

	public void Sort(List<T> input, List<int> selected)
	{
		IEnumerable<T> source = from i in selected
		select input[i];
		input.Sort(this.Compare);
		if (!this.SortAsc)
		{
			input.Reverse();
		}
		this.SortAsc = !this.SortAsc;
		selected.Clear();
		selected.AddRange(from x in source
		select input.IndexOf(x));
	}

	public readonly bool IsAction;

	public string Name;

	public readonly string ActionName;

	public readonly Action<T> Action;

	public readonly Func<T, string> Output;

	private readonly Comparison<T> Compare;

	public float Width;

	public bool SortAsc = true;

	public bool Visible = true;
}
