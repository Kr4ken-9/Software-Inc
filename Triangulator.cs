﻿using System;
using System.Collections.Generic;
using System.Linq;
using Poly2Tri;
using UnityEngine;

public class Triangulator
{
	public Triangulator(IEnumerable<Vector2> points)
	{
		this.Points = new List<Vector2>(points);
	}

	private List<Vector2[]> JoinHoles(List<Vector2[]> holes)
	{
		List<Vector2[]> list = holes.ToList<Vector2[]>();
		List<Vector2[]> list2 = new List<Vector2[]>();
		list2.Add(list[0]);
		list.Remove(list2[0]);
		for (int i = 0; i < list2.Count; i++)
		{
			for (int j = 0; j < list.Count; j++)
			{
				Vector2[] array = this.JoinPolygons(list2[i], list[j]);
				if (array != null)
				{
					list.RemoveAt(j);
					list2.RemoveAt(i);
					list2.Insert(i, array);
					j = -1;
				}
			}
			if (list.Count > 0)
			{
				Vector2[] item = list[0];
				list2.Add(item);
				list.Remove(item);
			}
		}
		return list2;
	}

	private Vector2[] JoinPolygons(Vector2[] a, Vector2[] b)
	{
		int[] array = new int[a.Length];
		for (int i = 0; i < a.Length; i++)
		{
			bool flag = false;
			for (int j = 0; j < b.Length; j++)
			{
				if (a[i] == b[j])
				{
					array[i] = j;
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				array[i] = -1;
			}
		}
		int num = -1;
		int num2 = -1;
		for (int k = 0; k < array.Length; k++)
		{
			int num3 = (k != 0) ? (k - 1) : (array.Length - 1);
			int num4 = (k != array.Length - 1) ? (k + 1) : 0;
			if (array[num3] == -1 && array[k] > -1 && array[num4] == -1)
			{
				num = k;
				num2 = array[k];
				break;
			}
		}
		if (num > -1)
		{
			Vector2 vector = a[num] - b[num2];
			Vector2 vector2 = new Vector2(-vector.y, vector.x);
			vector = vector2.normalized;
			List<Vector2> list = a.ToList<Vector2>();
			List<Vector2> list2 = b.Skip(num2 + 1).Take(b.Length - num2 - 1).Concat(b.Take(num2)).ToList<Vector2>();
			list2.Add(b[num2]);
			list.InsertRange(num + 1, list2);
			int num5 = num + b.Length;
			Vector2 a2 = (num != 0) ? list[num - 1] : list.Last<Vector2>();
			Vector2 b2 = list[num + 1];
			Vector2 a3 = (a2 + b2) * 0.5f;
			list[num] += (a3 - list[num]).normalized;
			a2 = list[num5 - 1];
			b2 = ((num5 != list.Count - 1) ? list[num5 + 1] : list[0]);
			a3 = (a2 + b2) * 0.5f;
			list[num5] += (a3 - list[num5]).normalized;
			return list.ToArray();
		}
		return null;
	}

	private Dictionary<int, int> fixPolygons(List<Vector2[]> holes)
	{
		Dictionary<int, int> dictionary = new Dictionary<int, int>();
		List<Vector2[]> list = holes.ToList<Vector2[]>();
		while (list.Count > 0)
		{
			float num = float.PositiveInfinity;
			Vector2[] p = null;
			int num2 = 0;
			int p2 = 0;
			for (int i = 0; i < list.Count; i++)
			{
				for (int j = 0; j < list[i].Length; j++)
				{
					for (int k = 0; k < this.Points.Count; k++)
					{
						if (!dictionary.ContainsKey(k) && !dictionary.ContainsValue(k))
						{
							float sqrMagnitude = (list[i][j] - this.Points[k]).sqrMagnitude;
							if (sqrMagnitude < num)
							{
								p = list[i];
								num = sqrMagnitude;
								num2 = j;
								p2 = k;
							}
						}
					}
				}
			}
			if (p[num2] == this.Points[p2])
			{
				List<Vector2> list2 = p.Reverse<Vector2>().ToList<Vector2>();
				int num3 = num2;
				num2 = p.Length - 1 - num2;
				List<Vector2> list3 = list2.Skip(num2 + 1).Take(list2.Count - num2 - 1).Concat(list2.Take(num2)).ToList<Vector2>();
				list3.Add(p[num3]);
				num2 = p.Length - 1;
				this.Points.InsertRange(p2 + 1, list3);
				num2 += p2 + 1;
				Vector2 a = (num2 != this.Points.Count - 1) ? this.Points[num2 + 1] : this.Points[0];
				this.Points[num2] = this.Points[num2] + (a - this.Points[num2]).normalized;
				dictionary = dictionary.ToDictionary((KeyValuePair<int, int> x) => (x.Key < p2) ? x.Key : (x.Key + p.Length), (KeyValuePair<int, int> x) => (x.Value < p2) ? x.Value : (x.Value + p.Length));
				dictionary[p2] = num2;
			}
			else
			{
				Vector2 b = p[num2] - this.Points[p2];
				Vector2 vector = new Vector2(-b.y, b.x);
				b = vector.normalized;
				Vector2 a2 = this.Points[p2];
				num2 = p.Length - 1 - num2;
				List<Vector2> list4 = p.Reverse<Vector2>().ToList<Vector2>();
				Vector2[] shift = list4.Skip(num2).Take(list4.Count - num2).Concat(list4.Take(num2)).ToArray<Vector2>();
				this.Points.InsertRange(p2 + 1, shift);
				dictionary = dictionary.ToDictionary((KeyValuePair<int, int> x) => (x.Key < p2) ? x.Key : (x.Key + shift.Length + 2), (KeyValuePair<int, int> x) => (x.Value < p2) ? x.Value : (x.Value + shift.Length + 2));
				dictionary[p2 + 1] = p2 + shift.Length + 1;
				dictionary[p2] = p2 + shift.Length + 2;
				this.Points.Insert(p2 + shift.Length + 1, shift.First<Vector2>() - b);
				this.Points.Insert(p2 + shift.Length + 2, a2 - b);
			}
			list.Remove(p);
		}
		return dictionary;
	}

	public int[] Triangulate(IEnumerable<Vector2[]> holes, DTSweepContext context)
	{
		List<PolygonPoint> list = new List<PolygonPoint>();
		PolygonPoint[] array = this.Points.Select((Vector2 x, int i) => new PolygonPoint((double)x.x, (double)x.y, i)).ToArray<PolygonPoint>();
		int t = this.Points.Count;
		Polygon polygon = new Polygon(array);
		list.AddRange(array);
		foreach (Vector2[] source in holes)
		{
			array = source.Select((Vector2 x, int i) => new PolygonPoint((double)x.x, (double)x.y, t + i)).ToArray<PolygonPoint>();
			list.AddRange(array);
			polygon.AddHole(new Polygon(array));
			t += array.Length;
		}
		context.PrepareTriangulation(polygon);
		DTSweep.Triangulate(context);
		context.Clear();
		this.Points = this.Points.Concat(holes.SelectMany((Vector2[] x) => x)).ToList<Vector2>();
		return polygon.Triangles.SelectMany((DelaunayTriangle x) => from z in x.Points
		select z.I).ToArray<int>();
	}

	private List<PolygonPoint> MakeHole(Vector2[] hole, List<PolygonPoint> existingPoints, ref int t)
	{
		List<PolygonPoint> list = new List<PolygonPoint>();
		for (int i = 0; i < hole.Length; i++)
		{
			PolygonPoint polygonPoint = null;
			foreach (PolygonPoint polygonPoint2 in existingPoints)
			{
				if (Mathf.Abs(polygonPoint2.Xf - hole[i].x) < 0.1f && Mathf.Abs(polygonPoint2.Yf - hole[i].y) < 0.1f)
				{
					polygonPoint = polygonPoint2;
					break;
				}
			}
			if (polygonPoint != null)
			{
				list.Add(polygonPoint);
			}
			else
			{
				polygonPoint = new PolygonPoint((double)hole[i].x, (double)hole[i].y, t);
				list.Add(polygonPoint);
				existingPoints.Add(polygonPoint);
				t++;
			}
		}
		return list;
	}

	public int[] Triangulate(IEnumerable<Vector2[]> holes)
	{
		List<Vector2[]> list = holes.ToList<Vector2[]>();
		if (list.Count == 0)
		{
			return this.Triangulate();
		}
		Dictionary<int, int> dictionary = this.fixPolygons(list);
		int[] array = this.Triangulate();
		foreach (KeyValuePair<int, int> keyValuePair in dictionary)
		{
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] == keyValuePair.Value)
				{
					array[i] = keyValuePair.Key;
				}
			}
		}
		foreach (int num in from x in dictionary.Values
		orderby x descending
		select x)
		{
			for (int j = 0; j < array.Length; j++)
			{
				if (array[j] > num)
				{
					array[j]--;
				}
			}
			this.Points.RemoveAt(num);
		}
		return array;
	}

	public int[] Triangulate()
	{
		List<int> list = new List<int>();
		int count = this.Points.Count;
		if (count < 3)
		{
			return list.ToArray();
		}
		int[] array = new int[count];
		if (this.Area() > 0f)
		{
			for (int i = 0; i < count; i++)
			{
				array[i] = i;
			}
		}
		else
		{
			for (int j = 0; j < count; j++)
			{
				array[j] = count - 1 - j;
			}
		}
		int k = count;
		int num = 2 * k;
		int num2 = 0;
		int num3 = k - 1;
		while (k > 2)
		{
			if (num-- <= 0)
			{
				return list.ToArray();
			}
			int num4 = num3;
			if (k <= num4)
			{
				num4 = 0;
			}
			num3 = num4 + 1;
			if (k <= num3)
			{
				num3 = 0;
			}
			int num5 = num3 + 1;
			if (k <= num5)
			{
				num5 = 0;
			}
			if (this.Snip(num4, num3, num5, k, array))
			{
				int item = array[num4];
				int item2 = array[num3];
				int item3 = array[num5];
				list.Add(item);
				list.Add(item2);
				list.Add(item3);
				num2++;
				int num6 = num3;
				for (int l = num3 + 1; l < k; l++)
				{
					array[num6] = array[l];
					num6++;
				}
				k--;
				num = 2 * k;
			}
		}
		list.Reverse();
		return list.ToArray();
	}

	private float Area()
	{
		int count = this.Points.Count;
		float num = 0f;
		int index = count - 1;
		int i = 0;
		while (i < count)
		{
			Vector2 vector = this.Points[index];
			Vector2 vector2 = this.Points[i];
			num += vector.x * vector2.y - vector2.x * vector.y;
			index = i++;
		}
		return num * 0.5f;
	}

	private bool Snip(int u, int v, int w, int n, int[] V)
	{
		Vector2 a = this.Points[V[u]];
		Vector2 b = this.Points[V[v]];
		Vector2 c = this.Points[V[w]];
		if (Mathf.Epsilon > (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x))
		{
			return false;
		}
		for (int i = 0; i < n; i++)
		{
			if (i != u && i != v && i != w)
			{
				Vector2 p = this.Points[V[i]];
				if (this.InsideTriangle(a, b, c, p))
				{
					return false;
				}
			}
		}
		return true;
	}

	private bool InsideTriangle(Vector2 A, Vector2 B, Vector2 C, Vector2 P)
	{
		float num = C.x - B.x;
		float num2 = C.y - B.y;
		float num3 = A.x - C.x;
		float num4 = A.y - C.y;
		float num5 = B.x - A.x;
		float num6 = B.y - A.y;
		float num7 = P.x - A.x;
		float num8 = P.y - A.y;
		float num9 = P.x - B.x;
		float num10 = P.y - B.y;
		float num11 = P.x - C.x;
		float num12 = P.y - C.y;
		float num13 = num * num10 - num2 * num9;
		float num14 = num5 * num8 - num6 * num7;
		float num15 = num3 * num12 - num4 * num11;
		return num13 >= 0f && num15 >= 0f && num14 >= 0f;
	}

	public List<Vector2> Points = new List<Vector2>();
}
