﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class GuestAI : AI<Actor>
{
	public override void InitTree()
	{
		string name = "Spawn";
		if (GuestAI.<>f__mg$cache0 == null)
		{
			GuestAI.<>f__mg$cache0 = new Func<Actor, int>(GuestAI.Spawn);
		}
		BehaviorNode<Actor> behaviorNode = new BehaviorNode<Actor>(name, GuestAI.<>f__mg$cache0, true, -1);
		string name2 = "GoHome";
		if (GuestAI.<>f__mg$cache1 == null)
		{
			GuestAI.<>f__mg$cache1 = new Func<Actor, int>(Actor.GoHomeState);
		}
		BehaviorNode<Actor> behaviorNode2 = new BehaviorNode<Actor>(name2, GuestAI.<>f__mg$cache1, true, -1);
		string name3 = "Despawn";
		if (GuestAI.<>f__mg$cache2 == null)
		{
			GuestAI.<>f__mg$cache2 = new Func<Actor, int>(GuestAI.Despawn);
		}
		BehaviorNode<Actor> behaviorNode3 = new BehaviorNode<Actor>(name3, GuestAI.<>f__mg$cache2, true, -1);
		string name4 = "IsOff";
		if (GuestAI.<>f__mg$cache3 == null)
		{
			GuestAI.<>f__mg$cache3 = new Func<Actor, int>(GuestAI.IsOff);
		}
		BehaviorNode<Actor> behaviorNode4 = new BehaviorNode<Actor>(name4, GuestAI.<>f__mg$cache3, false, -1);
		string name5 = "LookForReceptionDesk";
		if (GuestAI.<>f__mg$cache4 == null)
		{
			GuestAI.<>f__mg$cache4 = new Func<Actor, int>(GuestAI.LookForReceptionDesk);
		}
		BehaviorNode<Actor> behaviorNode5 = new BehaviorNode<Actor>(name5, GuestAI.<>f__mg$cache4, false, -1);
		string name6 = "IsUp";
		if (GuestAI.<>f__mg$cache5 == null)
		{
			GuestAI.<>f__mg$cache5 = new Func<Actor, int>(GuestAI.IsUp);
		}
		BehaviorNode<Actor> behaviorNode6 = new BehaviorNode<Actor>(name6, GuestAI.<>f__mg$cache5, false, -1);
		string name7 = "GoToReceptionDesk";
		if (GuestAI.<>f__mg$cache6 == null)
		{
			GuestAI.<>f__mg$cache6 = new Func<Actor, int>(GuestAI.GoToReceptionDesk);
		}
		BehaviorNode<Actor> behaviorNode7 = new BehaviorNode<Actor>(name7, GuestAI.<>f__mg$cache6, true, -1);
		string name8 = "WaitAtDesk";
		if (GuestAI.<>f__mg$cache7 == null)
		{
			GuestAI.<>f__mg$cache7 = new Func<Actor, int>(GuestAI.WaitAtDesk);
		}
		BehaviorNode<Actor> behaviorNode8 = new BehaviorNode<Actor>(name8, GuestAI.<>f__mg$cache7, true, -1);
		string name9 = "Loiter";
		if (GuestAI.<>f__mg$cache8 == null)
		{
			GuestAI.<>f__mg$cache8 = new Func<Actor, int>(GuestAI.Loiter);
		}
		BehaviorNode<Actor> behaviorNode9 = new BehaviorNode<Actor>(name9, GuestAI.<>f__mg$cache8, true, -1);
		string name10 = "GoHomeBusStop";
		if (GuestAI.<>f__mg$cache9 == null)
		{
			GuestAI.<>f__mg$cache9 = new Func<Actor, int>(Actor.GoHomeBusStop);
		}
		BehaviorNode<Actor> behaviorNode10 = new BehaviorNode<Actor>(name10, GuestAI.<>f__mg$cache9, true, -1);
		string name11 = "ShouldUseBus";
		if (GuestAI.<>f__mg$cacheA == null)
		{
			GuestAI.<>f__mg$cacheA = new Func<Actor, int>(Actor.ShouldUseBus);
		}
		BehaviorNode<Actor> behaviorNode11 = new BehaviorNode<Actor>(name11, GuestAI.<>f__mg$cacheA, true, -1);
		this.BehaviorNodes.Add("Spawn", behaviorNode);
		this.BehaviorNodes.Add("GoHome", behaviorNode2);
		this.BehaviorNodes.Add("Despawn", behaviorNode3);
		this.BehaviorNodes.Add("IsOff", behaviorNode4);
		this.BehaviorNodes.Add("LookForReceptionDesk", behaviorNode5);
		this.BehaviorNodes.Add("IsUp", behaviorNode6);
		this.BehaviorNodes.Add("GoToReceptionDesk", behaviorNode7);
		this.BehaviorNodes.Add("WaitAtDesk", behaviorNode8);
		this.BehaviorNodes.Add("Loiter", behaviorNode9);
		this.BehaviorNodes.Add("GoHomeBusStop", behaviorNode10);
		this.BehaviorNodes.Add("ShouldUseBus", behaviorNode11);
		behaviorNode.Success = behaviorNode5;
		behaviorNode5.Success = behaviorNode4;
		behaviorNode6.Success = behaviorNode7;
		behaviorNode6.Failure = behaviorNode5;
		behaviorNode7.Success = behaviorNode8;
		behaviorNode7.Failure = behaviorNode5;
		behaviorNode8.Success = behaviorNode11;
		behaviorNode4.Success = behaviorNode11;
		behaviorNode4.Failure = behaviorNode9;
		behaviorNode2.Success = behaviorNode3;
		behaviorNode2.Failure = behaviorNode10;
		behaviorNode3.Success = AI<Actor>.DummyNode;
		behaviorNode9.Success = behaviorNode6;
		behaviorNode9.Failure = behaviorNode6;
		behaviorNode11.Success = behaviorNode10;
		behaviorNode11.Failure = behaviorNode2;
		behaviorNode10.Success = behaviorNode3;
		behaviorNode10.Failure = behaviorNode9;
	}

	private static int Spawn(Actor self)
	{
		if (self.CurrentPath == null)
		{
			return 2;
		}
		return (!self.WalkPath()) ? 1 : 2;
	}

	private static int GoToReceptionDesk(Actor self)
	{
		if (self.UsingPoint == null)
		{
			self.CleaningRoom = null;
			return 0;
		}
		if (self.CurrentPath == null)
		{
			if (!self.PathToFurniture(self.UsingPoint, true))
			{
				self.CleaningRoom = null;
				return 0;
			}
			return 1;
		}
		else
		{
			bool flag = self.WalkPath();
			if (flag)
			{
				self.anim.SetEnum("AnimControl", self.UsingPoint.Animation);
				self.TurnToFurniture();
				return 2;
			}
			return 1;
		}
	}

	private static int WaitAtDesk(Actor self)
	{
		int num = self.WaitForTimer(5f);
		if (num == 2)
		{
			if (self.deal != null)
			{
				HUD.Instance.dealWindow.InsertDeal(self.deal);
			}
			self.UsingPoint = null;
		}
		return num;
	}

	private static int LookForReceptionDesk(Actor self)
	{
		if (self.CleaningRoom == null)
		{
			ReceptionDesk receptionDesk = (from x in GameSettings.Instance.sRoomManager.AllFurniture
			where x != null && x.Type.Equals("Desk")
			select x.GetComponent<ReceptionDesk>() into x
			where x.Active
			select x).MinInstance((ReceptionDesk x) => (float)x.Queue.Count);
			if (receptionDesk != null)
			{
				receptionDesk.Queue.Add(self);
				self.CleaningRoom = receptionDesk.GetComponent<Furniture>().Parent;
			}
		}
		return 2;
	}

	private static int Despawn(Actor self)
	{
		foreach (InteractionPoint interactionPoint in self.InQueue.Values)
		{
			interactionPoint.RemoveFromQueue(self);
		}
		self.InQueue.Clear();
		if (self.UsingPoint != null)
		{
			self.UsingPoint.UsedBy = null;
			self.UsingPoint = null;
		}
		UnityEngine.Object.Destroy(self.gameObject);
		return 2;
	}

	private static int IsOff(Actor self)
	{
		if ((SDateTime.Now() - self.MeetingTime).ToInt() > 180)
		{
			if (self.UsingPoint != null)
			{
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
			return 2;
		}
		return 0;
	}

	private static int IsUp(Actor self)
	{
		return (!(self.UsingPoint != null) || !self.UsingPoint.Parent.Type.Equals("Desk")) ? 0 : 2;
	}

	private static int Loiter(Actor self)
	{
		if (self.CurrentPath != null)
		{
			bool flag = self.WalkPath();
			if (self.UsingPoint == null)
			{
				self.CurrentPath = null;
				return 0;
			}
			if (flag)
			{
				self.anim.SetEnum("AnimControl", self.UsingPoint.Animation);
				self.TurnToFurniture();
				return 2;
			}
			return 1;
		}
		else if (self.UsingPoint != null)
		{
			if (self.UsingPoint.Parent == null)
			{
				return 0;
			}
			if (self.UsingPoint.Parent.Type.Equals("Desk"))
			{
				return 2;
			}
			return self.WaitForTimer(5f);
		}
		else
		{
			self.anim.SetInteger("AnimControl", 0);
			if (self.CleaningRoom != null)
			{
				return self.GoToFurniture("Couch", "Use", 1, false, self.CleaningRoom, false, false, null);
			}
			if (GameSettings.Instance != null)
			{
				Furniture random = (from x in GameSettings.Instance.sRoomManager.AllFurniture
				where x != null && x.Parent != null && x.Type.Equals("Desk")
				select x).GetRandom<Furniture>();
				return self.GoToFurniture("Couch", "Use", 1, false, (!(random == null)) ? random.Parent : null, false, false, null);
			}
			return 0;
		}
	}

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache5;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache6;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache7;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache8;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache9;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheA;
}
