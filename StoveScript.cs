﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StoveScript : MonoBehaviour
{
	public Holdable TakeReady()
	{
		foreach (KeyValuePair<Transform, Holdable> keyValuePair in this.Parent.Holdables.ToList<KeyValuePair<Transform, Holdable>>().ReverseEnum<KeyValuePair<Transform, Holdable>>())
		{
			if (keyValuePair.Value != null)
			{
				FoodItem component = keyValuePair.Value.GetComponent<FoodItem>();
				if (component.Done >= 1f)
				{
					this.Parent.Holdables[keyValuePair.Key] = null;
					return keyValuePair.Value;
				}
			}
		}
		this.Parent.UpdateHoldableStatus();
		return null;
	}

	public bool HasReady()
	{
		foreach (KeyValuePair<Transform, Holdable> keyValuePair in this.Parent.Holdables)
		{
			if (keyValuePair.Value != null)
			{
				FoodItem component = keyValuePair.Value.GetComponent<FoodItem>();
				if (component.Done >= 1f)
				{
					return true;
				}
			}
		}
		return false;
	}

	private void FixedUpdate()
	{
		bool flag = false;
		int num = 0;
		foreach (KeyValuePair<Transform, Holdable> keyValuePair in this.Parent.Holdables.ToList<KeyValuePair<Transform, Holdable>>())
		{
			ParticleSystem particleSystem = this.Particles[num];
			if (keyValuePair.Value != null)
			{
				FoodItem component = keyValuePair.Value.GetComponent<FoodItem>();
				if (component.Done < 1f)
				{
					if (this.Parent != null && this.Parent.Parent != null && GameSettings.Instance != null && this.Parent.Parent.Floor == GameSettings.Instance.ActiveFloor)
					{
						particleSystem.gameObject.SetActive(true);
						particleSystem.main.simulationSpeed = (float)HUD.Instance.GameSpeed;
					}
					else
					{
						particleSystem.gameObject.SetActive(false);
					}
					flag = true;
					component.Done += Time.deltaTime * GameSettings.GameSpeed / 15f;
				}
				else
				{
					particleSystem.gameObject.SetActive(false);
				}
			}
			else
			{
				particleSystem.gameObject.SetActive(false);
			}
			num++;
		}
		if (this.Parent.IsOn != flag)
		{
			this.Parent.IsOn = flag;
		}
	}

	public Furniture Parent;

	public ParticleSystem[] Particles;
}
