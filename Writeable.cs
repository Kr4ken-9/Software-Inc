﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Writeable : MonoBehaviour
{
	public object GetDeserializedObject(uint id)
	{
		if (id == 0u)
		{
			return null;
		}
		object result = null;
		if (Writeable.DeserializedObjects.TryGetValue(id, out result))
		{
			return result;
		}
		return null;
	}

	public static object STGetDeserializedObject(uint id)
	{
		if (id == 0u)
		{
			return null;
		}
		object result = null;
		if (Writeable.DeserializedObjects.TryGetValue(id, out result))
		{
			return result;
		}
		return null;
	}

	public static uint GetNextID()
	{
		uint idcount = Writeable.IDCount;
		Writeable.IDCount += 1u;
		return idcount;
	}

	public void InitWritable()
	{
		if (this.DID == 0u)
		{
			this.DID = Writeable.GetNextID();
		}
	}

	public WriteDictionary SerializeThis(GameReader.LoadMode mode)
	{
		WriteDictionary writeDictionary = new WriteDictionary(this.WriteName());
		writeDictionary["WriteID"] = this.DID;
		this.SerializeMe(writeDictionary, mode);
		return writeDictionary;
	}

	public void SerializeThis(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		this.SerializeMe(dictionary, mode);
	}

	public void DeserializeThis(WriteDictionary dictionary)
	{
		this.DID = (uint)dictionary["WriteID"];
		if (this.DID >= Writeable.IDCount)
		{
			Writeable.IDCount = this.DID + 1u;
		}
		this.Deserialized = true;
		try
		{
			Writeable.DeserializedObjects[this.DID] = this.DeserializeMe(dictionary);
		}
		catch (Exception exception)
		{
			Debug.LogException(exception);
		}
	}

	public virtual string WriteName()
	{
		return "None";
	}

	protected virtual void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
	}

	public virtual void PostDeserialize()
	{
	}

	protected virtual object DeserializeMe(WriteDictionary dictionary)
	{
		return null;
	}

	public static uint IDCount = 1u;

	public static Dictionary<uint, object> DeserializedObjects = new Dictionary<uint, object>();

	[NonSerialized]
	public uint DID;

	public bool Deserialized;
}
