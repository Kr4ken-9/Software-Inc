﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EmployeeWindow : MonoBehaviour
{
	private void Start()
	{
		this.ChangeType();
		this.EmployeeList.OnSelectChange = delegate(bool direct)
		{
			Actor[] selected = this.EmployeeList.GetSelected<Actor>();
			this.chart.SetContent((from x in selected
			select x.employee).ToArray<Employee>());
			List<Team> list = (from x in selected
			select x.GetTeam()).Distinct<Team>().ToList<Team>();
			this.chart.MinSkillTeam = ((list.Count != 1) ? null : list[0]);
			if (direct)
			{
				SelectorController instance = SelectorController.Instance;
				instance.Highligt(false);
				instance.Selected.Clear();
				foreach (Actor item in selected)
				{
					if (!instance.Selected.Contains(item))
					{
						UISoundFX.PlaySFX("ObjectHighlight", -1f, 0f);
						instance.Selected.Add(item);
					}
				}
				instance.DoPostSelectChecks();
				HintController.Instance.Show(HintController.Hints.HintEmployeeSelection);
			}
		};
	}

	public void Show(HashSet<Team> t = null)
	{
		this._customEmps = null;
		if (this.Window.Shown && t == null)
		{
			this.Window.Close();
		}
		else
		{
			TutorialSystem.Instance.StartTutorial("Employees", false);
			this.teams = t;
			this.Window.Show();
			this.EmployeeList.ClearSelected();
			this.UpdateEmployeeList();
		}
	}

	public void Show(IEnumerable<Actor> employees)
	{
		TutorialSystem.Instance.StartTutorial("Employees", false);
		this.Window.Show();
		this.EmployeeList.ClearSelected();
		this._customEmps = employees.ToList<Actor>();
		this.UpdateEmployeeList();
	}

	public void UpdateEmployeeList()
	{
		if (this._customEmps != null)
		{
			this.EmployeeList.Items = (from x in this._customEmps
			where x != null && x.gameObject != null
			select x).Cast<object>().ToList<object>();
		}
		else if (this.teams != null)
		{
			this.EmployeeList.Items = (from x in GameSettings.Instance.sActorManager.Actors
			where this.teams.Contains(x.GetTeam())
			select x).Cast<object>().ToList<object>();
		}
		else
		{
			this.EmployeeList.Items = GameSettings.Instance.sActorManager.Actors.Cast<object>().ToList<object>();
		}
	}

	public void ChangeType()
	{
		this.EmployeeList["EmployeeName"].gameObject.SetActive(true);
		this.EmployeeList["EmployeeRole"].gameObject.SetActive(this.Information.isOn);
		this.EmployeeList["EmployeeTeam"].gameObject.SetActive(this.Information.isOn);
		this.EmployeeList["EmployeeSalary"].gameObject.SetActive(this.Information.isOn);
		this.EmployeeList["EmployeeVacation"].gameObject.SetActive(this.Information.isOn);
		this.EmployeeList["EmployeeAge"].gameObject.SetActive(this.Information.isOn);
		this.EmployeeList["EmployeeYears"].gameObject.SetActive(this.Information.isOn);
		this.EmployeeList["EmployeeState"].gameObject.SetActive(this.State.isOn);
		this.EmployeeList["EmployeeEffectiveness"].gameObject.SetActive(this.State.isOn);
		this.EmployeeList["EmployeeCompatibility"].gameObject.SetActive(this.State.isOn);
		this.EmployeeList["EmployeeSatisfaction"].gameObject.SetActive(this.State.isOn);
		this.EmployeeList["EmployeeSkillLead"].gameObject.SetActive(this.Skill.isOn);
		this.EmployeeList["EmployeeSkillCode"].gameObject.SetActive(this.Skill.isOn);
		this.EmployeeList["EmployeeSkillDesign"].gameObject.SetActive(this.Skill.isOn);
		this.EmployeeList["EmployeeSkillArt"].gameObject.SetActive(this.Skill.isOn);
		this.EmployeeList["EmployeeSkillMarketing"].gameObject.SetActive(this.Skill.isOn);
	}

	public void FireSelected()
	{
		Actor[] selected = (from x in this.EmployeeList.GetSelected<Actor>()
		where !x.employee.Founder
		select x).ToArray<Actor>();
		if (selected.Length > 0)
		{
			WindowManager.Instance.ShowMessageBox(string.Format("DismissMsg".Loc(), selected.Length), true, DialogWindow.DialogType.Warning, delegate
			{
				Actor[] selected;
				foreach (Actor actor in selected)
				{
					actor.Fire();
				}
				this.EmployeeList.ClearSelected();
				this.EmployeeList.UpdateElements();
			}, "Fire employees", null);
		}
	}

	public void SelectAll()
	{
		this.EmployeeList.ClearSelected();
		int[] array = new int[this.EmployeeList.Items.Count];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = i;
		}
		this.EmployeeList.LastSelectDirect = true;
		this.EmployeeList.Selected.AddRange(array);
		this.EmployeeList.UpdateSelected();
	}

	public void Educate()
	{
		if (this.EmployeeList.Selected.Count > 0)
		{
			HUD.Instance.educationWindow.Show(this.EmployeeList.GetSelected<Actor>());
		}
	}

	public void ShowDetails()
	{
		if (this.EmployeeList.Selected.Count > 0)
		{
			Actor emp = this.EmployeeList.GetSelected<Actor>()[0];
			HUD.Instance.DetailWindow.Show(emp, false, true);
		}
	}

	public void ChangeBenefits()
	{
		if (this.EmployeeList.Selected.Count > 0)
		{
			HUD.Instance.benefitWindow.Show(this.EmployeeList.GetSelected<Actor>());
		}
	}

	public static void ChangeRolesNow(IList<Actor> acts)
	{
		if (acts.Count > 0)
		{
			HUD.Instance.roleSelect.Show(acts);
		}
	}

	public void ChangeRoles()
	{
		EmployeeWindow.ChangeRolesNow(this.EmployeeList.GetSelected<Actor>());
	}

	public void ChangeTeams()
	{
		Actor[] selected = this.EmployeeList.GetSelected<Actor>().ToArray<Actor>();
		Actor actor = selected.FirstOrDefault((Actor x) => x.GetTeam() != null);
		string selected2 = (!(actor != null)) ? null : actor.Team;
		HUD.Instance.TeamSelectWindow.Show(true, selected2, delegate(string[] x)
		{
			foreach (Actor actor2 in from y in selected
			where y != null
			select y)
			{
				actor2.Team = ((x.Length != 0) ? x[0] : null);
			}
			this.EmployeeList.UpdateElements();
		}, null);
	}

	public GUIListView EmployeeList;

	public GUIWindow Window;

	[NonSerialized]
	private HashSet<Team> teams;

	public Toggle Information;

	public Toggle State;

	public Toggle Skill;

	public SpecializationChart chart;

	[NonSerialized]
	private List<Actor> _customEmps;
}
