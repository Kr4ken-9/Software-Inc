﻿using System;

public class Cheats
{
	public void SuperSpeed()
	{
		HUD.Instance.disableSpeedPanel = true;
		GameSettings.GameSpeed = 1000f;
		HUD.Instance.disableSpeedPanel = false;
	}

	public static bool DisableDarkness;

	public static bool UnlockFurn;

	public static bool ForceLights;

	public static bool CeilingMeshes;
}
