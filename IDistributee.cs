﻿using System;

public interface IDistributee
{
	float RefreshTime { get; set; }

	bool IsValid { get; }

	void UpdateNow(float delta);

	void UpdateNow2(float delta);

	bool NeedUpdate(bool firstFunction);
}
