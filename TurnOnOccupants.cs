﻿using System;
using UnityEngine;

public class TurnOnOccupants : MonoBehaviour
{
	private void Start()
	{
		this.furn = base.GetComponent<Furniture>();
		if (this.Glow != null)
		{
			this.Glow.material.SetColor("_EmissionColor", Color.black);
		}
	}

	private void Update()
	{
		if (this.furn == null || this.furn.Parent == null)
		{
			return;
		}
		if (this.furn.IsOn)
		{
			if (this.spin != null)
			{
				this.spinSpeed = Mathf.Lerp(this.spinSpeed, 7f, Time.deltaTime * 0.3f * GameSettings.GameSpeed);
				this.spin.transform.rotation = this.spin.transform.rotation * Quaternion.Euler(0f, this.spinSpeed * GameSettings.GameSpeed, 0f);
			}
			if (this.Glow != null && this.glowFactor < 1f)
			{
				if (Mathf.Approximately(this.glowFactor, 1f))
				{
					this.glowFactor = 1f;
				}
				else
				{
					this.glowFactor = Mathf.Lerp(this.glowFactor, 1f, Time.deltaTime * 0.3f * GameSettings.GameSpeed);
					this.Glow.material.SetColor("_EmissionColor", new Color(this.glowFactor, this.glowFactor, this.glowFactor));
				}
			}
			if (this.furn.Parent.Occupants.Count == 0)
			{
				this.furn.IsOn = false;
			}
		}
		else
		{
			if (this.spin != null && this.spinSpeed > 0f)
			{
				if (Mathf.Approximately(this.spinSpeed, 0f))
				{
					this.spinSpeed = 0f;
				}
				else
				{
					this.spinSpeed = Mathf.Lerp(this.spinSpeed, 0f, Time.deltaTime * 0.3f * GameSettings.GameSpeed);
					this.spin.transform.rotation = this.spin.transform.rotation * Quaternion.Euler(0f, this.spinSpeed * GameSettings.GameSpeed, 0f);
				}
			}
			if (this.Glow != null && this.glowFactor > 0f)
			{
				if (Mathf.Approximately(this.glowFactor, 0f))
				{
					this.glowFactor = 0f;
				}
				else
				{
					this.glowFactor = Mathf.Lerp(this.glowFactor, 0f, Time.deltaTime * 0.3f * GameSettings.GameSpeed);
					this.Glow.material.SetColor("_EmissionColor", new Color(this.glowFactor, this.glowFactor, this.glowFactor));
				}
			}
			if (this.furn.Parent.Occupants.Count > 0)
			{
				this.furn.IsOn = true;
			}
		}
	}

	private Furniture furn;

	public GameObject spin;

	public Renderer Glow;

	private float spinSpeed;

	private float glowFactor;
}
