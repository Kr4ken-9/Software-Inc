﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoadManager : Writeable
{
	private IEnumerable<RoadNode> FreePark
	{
		get
		{
			IEnumerable<RoadNode> result;
			if (GameSettings.Instance.RentMode)
			{
				result = this.Parking.Keys;
			}
			else
			{
				result = from x in this.Parking
				where x.Value
				select x.Key;
			}
			return result;
		}
	}

	public void RegisterParking(RoadNode node)
	{
		if (node.Parking)
		{
			this.AddedSpots.Add(node);
			this.Parking[node] = false;
			this.UpdateParkingAvailibility();
		}
	}

	public Dictionary<RoadNode, bool> GetParkingMesh()
	{
		return this.Parking;
	}

	public CarScript CreateCar(int idx)
	{
		HashSet<CarScript> hashSet;
		if (this.CachedCars.TryGetValue(idx, out hashSet) && hashSet.Count > 0)
		{
			CarScript carScript = hashSet.First<CarScript>();
			carScript.Reset();
			hashSet.Remove(carScript);
			this.Cars.Add(carScript);
			carScript.gameObject.SetActive(true);
			return carScript;
		}
		CarScript component = UnityEngine.Object.Instantiate<GameObject>(this.CarPrefabs[idx]).GetComponent<CarScript>();
		this.Cars.Add(component);
		return component;
	}

	public void DestroyCar(CarScript car)
	{
		car.DestroyEvent();
		car.gameObject.SetActive(false);
		this.Cars.Remove(car);
		HashSet<CarScript> hashSet;
		if (!this.CachedCars.TryGetValue(car.CarIdx, out hashSet))
		{
			hashSet = new HashSet<CarScript>();
			this.CachedCars[car.CarIdx] = hashSet;
		}
		hashSet.Add(car);
		TimeOfDay.Instance.canSkip = TimeOfDay.Instance.CanSkip();
	}

	public CarScript SendCar(Actor employee, Func<Room, float> priority, RoadNode.ParkingAssign type)
	{
		RoadNode roadNode = (employee.AItype != AI<Actor>.AIType.Employee) ? this.FindParkingSpotStaff(employee, type, priority) : this.FindParkingSpot(employee, type);
		if (roadNode != null)
		{
			int num;
			if (employee.AItype == AI<Actor>.AIType.Courier)
			{
				num = 3;
			}
			else
			{
				float benefitValue = employee.GetBenefitValue("Company car");
				if (benefitValue > 0f)
				{
					num = (int)benefitValue;
				}
				else
				{
					num = ((employee.WorksForFree() || employee.employee.Salary <= 7000f) ? 1 : 2);
					employee.SetCar(num);
				}
			}
			CarScript carScript = this.CreateCar(num);
			carScript.AddOccupant(employee, true);
			carScript.Target = roadNode;
			carScript.Init();
			return carScript;
		}
		return null;
	}

	public void UpdateParkingAvailibility()
	{
		foreach (RoadNode roadNode in (from x in this.Parking
		where !x.Value
		select x.Key).ToList<RoadNode>())
		{
			if (GameSettings.Instance.PlayerOwnedPoint(new Vector2(roadNode.transform.position.x, roadNode.transform.position.z)))
			{
				this.Parking[roadNode] = true;
			}
		}
	}

	public RoadNode FindParkingSpotStaff(Actor emp, RoadNode.ParkingAssign type, Func<Room, float> priority)
	{
		List<RoadNode> list = (emp.AItype != AI<Actor>.AIType.Guest) ? (from x in this.FreePark
		where !x.Taken && (x.Assign == RoadNode.ParkingAssign.Anyone || x.Assign == type) && !HUD.Instance.UnreachableParking.Contains(x)
		select x).ToList<RoadNode>() : (from x in this.Parking.Keys
		where !x.Taken && (x.Assign == RoadNode.ParkingAssign.Anyone || x.Assign == type) && !HUD.Instance.UnreachableParking.Contains(x)
		select x).ToList<RoadNode>();
		if (list.Count == 0)
		{
			return null;
		}
		Room room = (!emp.HasAssignedRooms) ? null : emp.GetAssignedRooms().MinInstance(priority);
		Vector2 vector;
		if (room != null)
		{
			vector = new Vector2(room.Center.x, room.Center.y);
		}
		else
		{
			Room room2 = GameSettings.Instance.sRoomManager.Rooms.MinInstance(priority);
			if (!(room2 != null))
			{
				return (emp.AItype != AI<Actor>.AIType.Courier) ? null : list.FirstOrDefault<RoadNode>();
			}
			vector = new Vector2(room2.Center.x, room2.Center.y);
		}
		Vector3 p3D = new Vector3(vector.x, 0f, vector.y);
		return list.MinInstance((RoadNode x) => (x.transform.position - p3D).sqrMagnitude * ((x.Assign != type) ? 1f : 0.25f));
	}

	public RoadNode FindParkingSpot(Actor emp, RoadNode.ParkingAssign type)
	{
		List<RoadNode> list = (from x in this.FreePark
		where !x.Taken && (x.Assign == RoadNode.ParkingAssign.Anyone || x.Assign == type) && !HUD.Instance.UnreachableParking.Contains(x)
		select x).ToList<RoadNode>();
		if (list.Count == 0)
		{
			return null;
		}
		Vector2 zero = Vector2.zero;
		bool flag = false;
		Furniture furniture = emp.Owns.FirstOrDefault((Furniture x) => x.Type.Equals("Computer"));
		if (furniture != null)
		{
			zero = new Vector2(furniture.transform.position.x, furniture.transform.position.z);
			flag = true;
		}
		if (!flag && emp.Team != null)
		{
			Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Teams.Contains(emp.GetTeam()));
			if (room != null)
			{
				zero = new Vector2(room.Center.x, room.Center.y);
				flag = true;
			}
		}
		if (!flag)
		{
			furniture = GameSettings.Instance.sRoomManager.AllFurniture.FirstOrDefault((Furniture x) => x.Type.Equals("Computer") && x.GetInteractionPoint(emp, "Use") != null);
			if (furniture != null)
			{
				furniture.Reserved = emp;
				zero = new Vector2(furniture.transform.position.x, furniture.transform.position.z);
				flag = true;
			}
		}
		if (!flag)
		{
			return null;
		}
		Vector3 p3d = new Vector3(zero.x, 0f, zero.y);
		return list.MinInstance((RoadNode x) => (x.transform.position - p3d).sqrMagnitude * ((x.Assign != type) ? 1f : 0.25f));
	}

	public void DeregisterParking(RoadNode node)
	{
		if (node.Parking)
		{
			this.Parking.Remove(node);
		}
	}

	public void PlaceRoad(int x, int y, byte type)
	{
		this.CachedPaths.Clear();
		if (x >= 0 && x < this.RoadMap.GetLength(0) && y >= 0 && y < this.RoadMap.GetLength(1))
		{
			byte b = this.RoadMap[x, y];
			this.RoadMap[x, y] = type;
			for (int i = -1; i <= 1; i++)
			{
				if (i == 0)
				{
					this.UpdateRoad(x, y, this.RoadMap[x, y] < 2 || b != this.RoadMap[x, y], true);
				}
				else
				{
					if (x + i >= 0 && x + i < this.RoadMap.GetLength(0) && y >= 0 && y < this.RoadMap.GetLength(1))
					{
						this.UpdateRoad(x + i, y, this.RoadMap[x + i, y] < 2, true);
					}
					if (x >= 0 && x < this.RoadMap.GetLength(0) && y + i >= 0 && y + i < this.RoadMap.GetLength(1))
					{
						this.UpdateRoad(x, y + i, this.RoadMap[x, y + i] < 2, true);
					}
				}
			}
			for (int j = -1; j <= 1; j++)
			{
				this.UpdateConnections(x + j, y);
				if (j != 0)
				{
					this.UpdateConnections(x, y + j);
				}
			}
			this.PlaceRoadLamps();
		}
		this.UpdateUnreachable();
	}

	private void UpdateUnreachable()
	{
		foreach (RoadNode roadNode in HUD.Instance.UnreachableParking.ToList<RoadNode>())
		{
			bool flag = true;
			for (int i = 0; i < this.InputList.Count; i++)
			{
				List<Vector2> list = this.FindPath(this.InputList[i], roadNode);
				if (list != null)
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				for (int j = 0; j < this.OutputList.Count; j++)
				{
					List<Vector2> list2 = this.FindPath(roadNode, this.OutputList[j]);
					if (list2 != null)
					{
						flag = false;
						break;
					}
				}
			}
			if (!flag)
			{
				HUD.Instance.UnreachableParking.Remove(roadNode);
			}
		}
		foreach (RoadNode roadNode2 in this.AddedSpots)
		{
			bool flag2 = true;
			for (int k = 0; k < this.InputList.Count; k++)
			{
				List<Vector2> list3 = this.FindPath(this.InputList[k], roadNode2);
				if (list3 != null)
				{
					flag2 = false;
					break;
				}
			}
			if (flag2)
			{
				for (int l = 0; l < this.OutputList.Count; l++)
				{
					List<Vector2> list4 = this.FindPath(roadNode2, this.OutputList[l]);
					if (list4 != null)
					{
						flag2 = false;
						break;
					}
				}
			}
			if (flag2)
			{
				HUD.Instance.UnreachableParking.Add(roadNode2);
			}
		}
		this.AddedSpots.Clear();
	}

	public void PlaceRoad(Rect r, byte type)
	{
		this.CachedPaths.Clear();
		byte[,] array = new byte[this.RoadMap.GetLength(0), this.RoadMap.GetLength(1)];
		Array.Copy(this.RoadMap, array, this.RoadMap.GetLength(0) * this.RoadMap.GetLength(1));
		int num = (int)r.xMin;
		while ((float)num < r.xMax)
		{
			int num2 = (int)r.yMin;
			while ((float)num2 < r.yMax)
			{
				if (this.CheckFree(num, num2))
				{
					this.RoadMap[num, num2] = type;
				}
				num2++;
			}
			num++;
		}
		int num3 = (int)r.xMin - 1;
		while ((float)num3 < r.xMax + 1f)
		{
			int num4 = (int)r.yMin - 1;
			while ((float)num4 < r.yMax + 1f)
			{
				if (num3 >= 0 && num3 < this.RoadMap.GetLength(0) && num4 >= 0 && num4 < this.RoadMap.GetLength(1))
				{
					this.UpdateRoad(num3, num4, this.RoadMap[num3, num4] < 2 || array[num3, num4] != this.RoadMap[num3, num4], true);
				}
				num4++;
			}
			num3++;
		}
		int num5 = (int)r.xMin - 1;
		while ((float)num5 < r.xMax + 1f)
		{
			int num6 = (int)r.yMin - 1;
			while ((float)num6 < r.yMax + 1f)
			{
				if (num5 >= 0 && num5 < this.RoadMap.GetLength(0) && num6 >= 0 && num6 < this.RoadMap.GetLength(1))
				{
					this.UpdateConnections(num5, num6);
				}
				num6++;
			}
			num5++;
		}
		this.UpdateUnreachable();
		this.PlaceRoadLamps();
	}

	private void ForcePlaceRoad(Rect r, byte type)
	{
		int num = (int)r.xMin;
		while ((float)num < r.xMax)
		{
			int num2 = (int)r.yMin;
			while ((float)num2 < r.yMax)
			{
				this.RoadMap[num, num2] = type;
				num2++;
			}
			num++;
		}
		int num3 = (int)r.xMin - 1;
		while ((float)num3 < r.xMax + 1f)
		{
			int num4 = (int)r.yMin - 1;
			while ((float)num4 < r.yMax + 1f)
			{
				if (num3 >= 0 && num3 < this.RoadMap.GetLength(0) && num4 >= 0 && num4 < this.RoadMap.GetLength(1))
				{
					this.UpdateRoad(num3, num4, true, true);
				}
				num4++;
			}
			num3++;
		}
		int num5 = (int)r.xMin - 1;
		while ((float)num5 < r.xMax + 1f)
		{
			int num6 = (int)r.yMin - 1;
			while ((float)num6 < r.yMax + 1f)
			{
				if (num5 >= 0 && num5 < this.RoadMap.GetLength(0) && num6 >= 0 && num6 < this.RoadMap.GetLength(1))
				{
					this.UpdateConnections(num5, num6);
				}
				num6++;
			}
			num5++;
		}
		this.PlaceRoadLamps();
	}

	public bool CheckFree(int x, int y)
	{
		int num = Mathf.FloorToInt(GameSettings.Instance.BusStopSign.transform.position.x / this.RoadSize) + 1;
		int num2 = Mathf.FloorToInt(GameSettings.Instance.BusStopSign.transform.position.z / this.RoadSize);
		if (x == num && y == num2)
		{
			ErrorOverlay.Instance.ShowError("RoadBusSignError", false, false, 0f, true);
			return false;
		}
		int num3 = (int)this.RoadSize;
		for (int i = 0; i < num3; i++)
		{
			for (int j = 0; j < num3; j++)
			{
				int num4 = x * num3 + i;
				int num5 = y * num3 + j;
				if (num4 != 8 && num5 != 8 && num4 != 247 && num5 != 247 && !GameSettings.Instance.PlayerOwnedPoint(new Vector2((float)num4, (float)num5)))
				{
					return false;
				}
			}
		}
		Rect rect = new Rect((float)(x * num3), (float)(y * num3), (float)num3, (float)num3);
		if (!(GameSettings.Instance.sRoomManager.GetRoomFromPoint(0, rect.center, true) != GameSettings.Instance.sRoomManager.Outside))
		{
			if (!(from r in GameSettings.Instance.sRoomManager.GetRooms()
			where r.Floor >= 0 && r.Floor <= 1
			select r).Any((Room r) => r.Edges.Any((WallEdge z) => rect.CompletelyWithin(z.Pos))))
			{
				Vector2[] array = new Vector2[]
				{
					new Vector2(rect.xMin, rect.yMin),
					new Vector2(rect.xMax, rect.yMin),
					new Vector2(rect.xMax, rect.yMax),
					new Vector2(rect.xMin, rect.yMax)
				};
				foreach (WallEdge wallEdge in GameSettings.Instance.sRoomManager.AllSegments)
				{
					if (wallEdge.Floor == 0 || wallEdge.Floor == 1)
					{
						foreach (WallEdge wallEdge2 in wallEdge.Links.Values)
						{
							for (int k = 0; k < array.Length; k++)
							{
								Vector2 pos = wallEdge.Pos;
								Vector2 pos2 = wallEdge2.Pos;
								Vector2 vector = (pos + pos2) * 0.5f;
								if (Utilities.LinesIntersect(pos, pos2, array[k], array[(k + 1) % array.Length], true, false))
								{
									return false;
								}
								if (vector.x > array[0].x && vector.x < array[2].x && vector.y > array[0].y && vector.y < array[2].y)
								{
									return false;
								}
							}
						}
					}
				}
				return true;
			}
		}
		return false;
	}

	private void UpdateRoad(int x, int y, bool change, bool treeRemoval = true)
	{
		if (x >= 0 && x < this.RoadMap.GetLength(0) && y >= 0 && y < this.RoadMap.GetLength(1))
		{
			if (this.ObjectMap[x, y] != null && change)
			{
				RoadSegment roadSegment = this.ObjectMap[x, y];
				if (x == 0 && y == this.RoadMap.GetLength(1) - 1)
				{
					this.InputList.Remove(roadSegment.WestIn);
					this.OutputList.Remove(roadSegment.WestOut);
				}
				if (x == 0 && y == 0)
				{
					this.InputList.Remove(roadSegment.EastIn);
					this.OutputList.Remove(roadSegment.EastOut);
				}
				if (x == this.RoadMap.GetLength(0) - 1 && y == this.RoadMap.GetLength(1) - 1)
				{
					this.InputList.Remove(roadSegment.NorthIn);
					this.OutputList.Remove(roadSegment.NorthOut);
				}
				if (x == this.RoadMap.GetLength(0) - 1 && y == 0)
				{
					this.InputList.Remove(roadSegment.NorthIn);
					this.OutputList.Remove(roadSegment.NorthOut);
				}
				roadSegment.RemoveConnections();
				UnityEngine.Object.Destroy(roadSegment.gameObject);
				this.ObjectMap[x, y] = null;
			}
			if (this.RoadMap[x, y] == 1)
			{
				byte roadType = this.GetRoadType(x, y);
				RoadManager.RoadPiece roadPiece = this.RoadPieces[(int)roadType];
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(roadPiece.Piece);
				gameObject.transform.rotation = Quaternion.Euler(0f, (float)roadPiece.Rotation, 0f) * gameObject.transform.rotation;
				gameObject.transform.position = new Vector3((float)x * this.RoadSize + this.RoadSize / 2f, 0.01f, (float)y * this.RoadSize + this.RoadSize / 2f);
				gameObject.transform.SetParent(base.transform);
				RoadSegment component = gameObject.GetComponent<RoadSegment>();
				component.Init(roadPiece.Rotation);
				component.x = x;
				component.y = y;
				this.ObjectMap[x, y] = component;
				if (treeRemoval)
				{
					this.RemoveTrees(x, y);
				}
				if (x == 0 && y == this.RoadMap.GetLength(1) - 1)
				{
					this.InputList.Add(component.WestIn);
					this.OutputList.Add(component.WestOut);
				}
				if (x == 0 && y == 0)
				{
					this.InputList.Add(component.EastIn);
					this.OutputList.Add(component.EastOut);
				}
				if (x == this.RoadMap.GetLength(0) - 1 && y == this.RoadMap.GetLength(1) - 1)
				{
					this.InputList.Add(component.NorthIn);
					this.OutputList.Add(component.NorthOut);
				}
				if (x == this.RoadMap.GetLength(0) - 1 && y == 0)
				{
					this.InputList.Add(component.NorthIn);
					this.OutputList.Add(component.NorthOut);
				}
			}
			if (change && this.RoadMap[x, y] == 2)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.ParkingHors);
				gameObject2.transform.position = new Vector3((float)x * this.RoadSize + this.RoadSize / 2f, 0.01f, (float)y * this.RoadSize + this.RoadSize / 2f);
				gameObject2.transform.SetParent(base.transform);
				RoadSegment component2 = gameObject2.GetComponent<RoadSegment>();
				component2.Init(0);
				component2.x = x;
				component2.y = y;
				this.ObjectMap[x, y] = component2;
				if (treeRemoval)
				{
					this.RemoveTrees(x, y);
				}
			}
			if (change && this.RoadMap[x, y] == 3)
			{
				GameObject gameObject3 = UnityEngine.Object.Instantiate<GameObject>(this.ParkingHors);
				gameObject3.transform.position = new Vector3((float)x * this.RoadSize + this.RoadSize / 2f, 0.01f, (float)y * this.RoadSize + this.RoadSize / 2f);
				gameObject3.transform.rotation = Quaternion.Euler(0f, 90f, 0f) * gameObject3.transform.rotation;
				gameObject3.transform.SetParent(base.transform);
				RoadSegment component3 = gameObject3.GetComponent<RoadSegment>();
				component3.Init(90);
				component3.x = x;
				component3.y = y;
				this.ObjectMap[x, y] = component3;
				if (treeRemoval)
				{
					this.RemoveTrees(x, y);
				}
			}
		}
	}

	private void PlaceRoadLamps()
	{
		this.Lamps.ForEach(delegate(GameObject x)
		{
			UnityEngine.Object.Destroy(x);
		});
		this.Lamps.Clear();
		this.Visited = new bool[this.RoadMap.GetLength(0), this.RoadMap.GetLength(1)];
		this.PlaceSubLamps(0, 0, false, 1);
	}

	private void PlaceSubLamps(int x, int y, bool side, int last)
	{
		if (x < 0 || x >= this.RoadMap.GetLength(0) || y < 0 || y >= this.RoadMap.GetLength(1))
		{
			return;
		}
		if (this.GetRoad(x, y) == 0 || this.Visited[x, y])
		{
			return;
		}
		this.Visited[x, y] = true;
		bool flag = side;
		int num = last - 1;
		int num2 = 0;
		if (last <= 0)
		{
			int num3 = 1;
			int num4 = 0;
			if (side)
			{
				num3 *= -1;
				num4 += 180;
			}
			if (this.GetRoad(x + num3, y) == 0)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.LampPrefab);
				gameObject.transform.position = new Vector3((float)x * this.RoadSize + this.RoadSize / 2f, 0f, (float)y * this.RoadSize + this.RoadSize / 2f);
				gameObject.transform.rotation = Quaternion.Euler(0f, (float)num4, 0f);
				this.Lamps.Add(gameObject);
				num2 = 1;
			}
			else if (this.GetRoad(x, y + num3) == 0)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.LampPrefab);
				gameObject2.transform.position = new Vector3((float)x * this.RoadSize + this.RoadSize / 2f, 0f, (float)y * this.RoadSize + this.RoadSize / 2f);
				gameObject2.transform.rotation = Quaternion.Euler(0f, (float)(num4 - 90), 0f);
				this.Lamps.Add(gameObject2);
				num2 = 2;
			}
			else if (this.GetRoad(x - num3, y) == 0)
			{
				GameObject gameObject3 = UnityEngine.Object.Instantiate<GameObject>(this.LampPrefab);
				gameObject3.transform.position = new Vector3((float)x * this.RoadSize + this.RoadSize / 2f, 0f, (float)y * this.RoadSize + this.RoadSize / 2f);
				gameObject3.transform.rotation = Quaternion.Euler(0f, (float)(num4 - 180), 0f);
				this.Lamps.Add(gameObject3);
				num2 = 1;
			}
			else if (this.GetRoad(x, y - num3) == 0)
			{
				GameObject gameObject4 = UnityEngine.Object.Instantiate<GameObject>(this.LampPrefab);
				gameObject4.transform.position = new Vector3((float)x * this.RoadSize + this.RoadSize / 2f, 0f, (float)y * this.RoadSize + this.RoadSize / 2f);
				gameObject4.transform.rotation = Quaternion.Euler(0f, (float)(num4 + 90), 0f);
				this.Lamps.Add(gameObject4);
				num2 = 2;
			}
			flag = !flag;
			num = 3;
		}
		this.PlaceSubLamps(x + 1, y, flag, num - ((num2 != 1) ? 0 : 1));
		this.PlaceSubLamps(x, y + 1, flag, num - ((num2 != 2) ? 0 : 1));
		this.PlaceSubLamps(x - 1, y, flag, num - ((num2 != 1) ? 0 : 1));
		this.PlaceSubLamps(x, y - 1, flag, num - ((num2 != 2) ? 0 : 1));
	}

	private void UpdateConnections(int x, int y)
	{
		RoadSegment segment = this.GetSegment(x, y);
		if (segment == null)
		{
			return;
		}
		RoadSegment segment2 = this.GetSegment(x + 1, y);
		if (segment2 != null)
		{
			if (segment2.SouthIn != null && segment.NorthOut != null)
			{
				segment.NorthOut.self.AddConnection(segment2.SouthIn.self);
			}
			if (segment2.SouthOut != null && segment.NorthIn != null)
			{
				segment2.SouthOut.self.AddConnection(segment.NorthIn.self);
			}
		}
		RoadSegment segment3 = this.GetSegment(x - 1, y);
		if (segment3 != null)
		{
			if (segment3.NorthOut != null && segment.SouthIn != null)
			{
				segment3.NorthOut.self.AddConnection(segment.SouthIn.self);
			}
			if (segment3.NorthIn != null && segment.SouthOut != null)
			{
				segment.SouthOut.self.AddConnection(segment3.NorthIn.self);
			}
		}
		RoadSegment segment4 = this.GetSegment(x, y - 1);
		if (segment4 != null)
		{
			if (segment4.WestOut != null && segment.EastIn != null)
			{
				segment4.WestOut.self.AddConnection(segment.EastIn.self);
			}
			if (segment4.WestIn != null && segment.EastOut != null)
			{
				segment.EastOut.self.AddConnection(segment4.WestIn.self);
			}
		}
		RoadSegment segment5 = this.GetSegment(x, y + 1);
		if (segment5 != null)
		{
			if (segment5.EastIn != null && segment.WestOut != null)
			{
				segment.WestOut.self.AddConnection(segment5.EastIn.self);
			}
			if (segment5.EastOut != null && segment.WestIn != null)
			{
				segment5.EastOut.self.AddConnection(segment.WestIn.self);
			}
		}
	}

	public RoadSegment GetSegment(int x, int y)
	{
		if (x >= 0 && x < this.RoadMap.GetLength(0) && y >= 0 && y < this.RoadMap.GetLength(1))
		{
			RoadSegment roadSegment = this.ObjectMap[x, y];
			return (!(roadSegment == null)) ? roadSegment.GetComponent<RoadSegment>() : null;
		}
		return null;
	}

	private void RemoveTrees(int x, int y)
	{
		if (GameSettings.Instance.TreeTree != null && x >= 0 && x < this.RoadMap.GetLength(0) && y >= 0 && y < this.RoadMap.GetLength(1))
		{
			HashSet<global::TreeInstance> hashSet = new HashSet<global::TreeInstance>();
			bool flag = false;
			Bounds bounds = new Bounds(new Vector3((float)x * this.RoadSize + this.RoadSize / 2f, 1f, (float)y * this.RoadSize + this.RoadSize / 2f), new Vector3(this.RoadSize, 2f, this.RoadSize));
			foreach (global::TreeInstance treeInstance in GameSettings.Instance.TreeTree.Query(bounds.Flatten().Expand(4f, 4f)))
			{
				if (treeInstance.Bounds.Intersects(bounds))
				{
					hashSet.Add(treeInstance);
					flag = true;
				}
			}
			if (flag)
			{
				foreach (global::TreeInstance treeInstance2 in hashSet)
				{
					treeInstance2.BelongsTo.RemoveTree(treeInstance2);
					GameSettings.Instance.Trees.Remove(treeInstance2);
					GameSettings.Instance.TreeTree.removeItem(treeInstance2);
				}
			}
		}
	}

	public void UpdateRoadVisibility()
	{
		for (int i = 0; i < this.ObjectMap.GetLength(0); i++)
		{
			for (int j = 0; j < this.ObjectMap.GetLength(1); j++)
			{
				if (this.ObjectMap[i, j] != null)
				{
					this.ObjectMap[i, j].render.enabled = (GameSettings.Instance.ActiveFloor >= 0);
				}
			}
		}
	}

	private byte GetRoadType(int x, int y)
	{
		byte b = 0;
		if (this.GetRoad(x, y - 1) > 0)
		{
			b |= 1;
		}
		if (this.GetRoad(x + 1, y) > 0)
		{
			b |= 2;
		}
		if (this.GetRoad(x, y + 1) > 0)
		{
			b |= 4;
		}
		if (this.GetRoad(x - 1, y) > 0)
		{
			b |= 8;
		}
		return b;
	}

	public byte GetRoad(Vector2 p)
	{
		return this.GetRoad(Mathf.FloorToInt(p.x / this.RoadSize), Mathf.FloorToInt(p.y / this.RoadSize));
	}

	public byte GetRoad(int x, int y)
	{
		if (x == 0 && y == -1)
		{
			return 1;
		}
		if (x == 0 && y == this.RoadMap.GetLength(1))
		{
			return 1;
		}
		if (x == this.RoadMap.GetLength(0) && y == 0)
		{
			return 1;
		}
		if (x == this.RoadMap.GetLength(0) && y == this.RoadMap.GetLength(1) - 1)
		{
			return 1;
		}
		if (x >= 0 && x < this.RoadMap.GetLength(0) && y >= 0 && y < this.RoadMap.GetLength(1))
		{
			return this.RoadMap[x, y];
		}
		return 0;
	}

	private void Awake()
	{
		if (RoadManager.Instance != null)
		{
			UnityEngine.Object.Destroy(RoadManager.Instance.gameObject);
		}
		RoadManager.Instance = this;
	}

	private void Start()
	{
		base.InitWritable();
		if (!GameData.LoadBuildingOnLoad)
		{
			this.RoadMap = new byte[(int)this.GridSize.x, (int)this.GridSize.y];
			this.ObjectMap = new RoadSegment[(int)this.GridSize.x, (int)this.GridSize.y];
			for (int i = 0; i < this.RoadMap.GetLength(1); i++)
			{
				this.PlaceRoad(0, i, 1);
			}
			for (int j = 0; j < this.RoadMap.GetLength(1); j++)
			{
				this.PlaceRoad(this.RoadMap.GetLength(0) - 1, j, 1);
			}
			for (int k = 1; k < this.RoadMap.GetLength(0) - 1; k++)
			{
				this.PlaceRoad(k, 0, 1);
				this.PlaceRoad(k, this.RoadMap.GetLength(1) - 1, 1);
			}
			if (GameData.Environment == GameData.EnvironmentType.City)
			{
				int num = (GameData.RNDValue <= 0.5f) ? 1 : -2;
				List<Rect> list = new List<Rect>();
				this.PlaceLine(false, 1, this.RoadMap.GetLength(0) - 1, 1, this.RoadMap.GetLength(1) - 1, list, this.RoadMap.GetLength(0) / 2 + num, 5);
				int num2 = list.Count / 4;
				List<KeyValuePair<Rect, float>> list2 = new List<KeyValuePair<Rect, float>>();
				Rect item = list.FirstOrDefault(delegate(Rect x)
				{
					Rect rect = new Rect(x.x * this.RoadSize, x.y * this.RoadSize, x.width * this.RoadSize, x.height * this.RoadSize);
					return rect.Contains(new Vector2(16f, 128f));
				});
				list.Remove(item);
				for (int l = 0; l < num2; l++)
				{
					Rect random = list.GetRandom(GameData.RND);
					list.Remove(random);
					Rect key = new Rect(random.x * this.RoadSize, random.y * this.RoadSize, random.width * this.RoadSize, random.height * this.RoadSize);
					list2.Add(new KeyValuePair<Rect, float>(key, random.width * random.height * RoadBuildCube.RoadCost * 0.5f));
					this.ForcePlaceRoad(random, (GameData.RNDValue <= 0.5f) ? 3 : 2);
				}
				this.AddedSpots.Clear();
				list = (from x in list
				select new Rect(x.x * this.RoadSize, x.y * this.RoadSize, x.width * this.RoadSize, x.height * this.RoadSize)).ToList<Rect>();
				List<KeyValuePair<Rect, float>> collection = this.PlaceBuildings(list);
				GameSettings.Instance.SpawnTreeAreas(list);
				if (!GameSettings.Instance.EditMode)
				{
					List<KeyValuePair<Rect, float>> list3 = new List<KeyValuePair<Rect, float>>();
					list3.AddRange(from x in list
					select new KeyValuePair<Rect, float>(x, 0f));
					list3.AddRange(collection);
					list3.AddRange(list2);
					list3.Add(new KeyValuePair<Rect, float>(new Rect(9f, 120f, 15f, 16f), 0f));
					GameSettings.Instance.Plots = PlotArea.PlotsFromRects(list3);
					List<Vector2> list4 = this.CreatePlotDiff(new Rect(item.x * this.RoadSize, item.y * this.RoadSize, item.width * this.RoadSize, item.height * this.RoadSize));
					if (list4 != null)
					{
						GameSettings.Instance.Plots.Insert(GameSettings.Instance.Plots.Count - 2, new PlotArea((from x in list4
						select new PlotArea.PlotPoint(x.x, x.y)).ToArray<PlotArea.PlotPoint>()));
					}
					GameSettings.Instance.InitPlots(false);
					PlotArea plotArea = GameSettings.Instance.Plots[GameSettings.Instance.Plots.Count - 1];
					plotArea.Price = PlotArea.StartPlotPrice;
					GameSettings.Instance.BuyPlot(plotArea);
				}
			}
			if (GameSettings.Instance.EnvType == GameData.EnvironmentType.Town)
			{
				int num3 = (GameData.RNDValue <= 0.5f) ? 2 : -3;
				List<Rect> list5 = new List<Rect>();
				this.PlaceLine(false, 1, this.RoadMap.GetLength(0) - 1, 1, this.RoadMap.GetLength(1) - 1, list5, this.RoadMap.GetLength(0) / 2 + num3, 10);
				Rect item2 = list5.FirstOrDefault(delegate(Rect x)
				{
					Rect rect = new Rect(x.x * this.RoadSize, x.y * this.RoadSize, x.width * this.RoadSize, x.height * this.RoadSize);
					return rect.Contains(new Vector2(16f, 128f));
				});
				list5.Remove(item2);
				list5 = (from x in list5
				select new Rect(x.x * this.RoadSize, x.y * this.RoadSize, x.width * this.RoadSize, x.height * this.RoadSize)).ToList<Rect>();
				List<Rect> list6 = new List<Rect>();
				for (int m = 0; m < 2; m++)
				{
					int index = GameData.RNDRange(0, list5.Count);
					list6.Add(list5[index]);
					list5.RemoveAt(index);
				}
				GameSettings.Instance.SpawnTreeAreas(list6);
				List<PlotArea> list7 = PlotArea.PlotsFromRects(list5);
				List<Rect> list8 = this.CreatePlotDiffs(new Rect(item2.x * this.RoadSize, item2.y * this.RoadSize, item2.width * this.RoadSize, item2.height * this.RoadSize));
				if (list8.Count > 0)
				{
					list7.AddRange(PlotArea.PlotsFromRects(list8));
				}
				list7 = PlotArea.DividePlots(list7, 16f);
				this.SpawnHouses(list7);
				if (!GameSettings.Instance.EditMode)
				{
					list7.AddRange(PlotArea.DividePlots(PlotArea.PlotsFromRects(list6), 32f));
					list7.Add(new PlotArea(new Rect(9f, 120f, 15f, 16f).ToPolygon().SelectInPlace((Vector2 x) => new PlotArea.PlotPoint(x))));
					GameSettings.Instance.Plots = list7;
					GameSettings.Instance.InitPlots(true);
					PlotArea plotArea2 = GameSettings.Instance.Plots[GameSettings.Instance.Plots.Count - 1];
					plotArea2.Price = PlotArea.StartPlotPrice;
					GameSettings.Instance.BuyPlot(plotArea2);
				}
			}
			GameSettings.Instance.sRoomManager.Outside.DirtyNavMesh = true;
		}
	}

	private void SpawnHouses(List<PlotArea> plots)
	{
		for (int i = 0; i < plots.Count; i++)
		{
			PlotArea plotArea = plots[i];
			if (plotArea.Polygon.Length == 4 && plotArea.Area > 100f)
			{
				int num = GameData.RNDRange(0, 4);
				bool flag = false;
				for (int j = 0; j < 4; j++)
				{
					int num2 = (j + num) % 4;
					Vector2 vector = plotArea.Polygon[num2];
					Vector2 a = plotArea.Polygon[(num2 + 1) % 4];
					Vector2 a2 = a - vector;
					Vector2 vector2 = vector + a2 * 0.5f;
					Vector2 vector3 = vector2 - a2.normalized.Turn90();
					byte road = this.GetRoad(vector3);
					if (road > 0)
					{
						BurbHouse burbHouse = this.PlaceHouse(vector2.ToVector3(0f), Quaternion.LookRotation((vector3 - vector2).ToVector3(0f)), plotArea.Polygon, plotArea.Area);
						if (burbHouse != null)
						{
							plotArea.AddonCost = burbHouse.Cost;
							flag = true;
							break;
						}
					}
				}
				if (!flag)
				{
					GameSettings.Instance.SpawnTreePolygon(plotArea.Polygon);
				}
			}
			else
			{
				GameSettings.Instance.SpawnTreePolygon(plotArea.Polygon);
			}
		}
	}

	public BurbHouse PlaceHouse(int house, Vector3 p, Quaternion rot)
	{
		BurbHouse burbHouse = UnityEngine.Object.Instantiate<BurbHouse>(this.BurbHousePrefabs[house]);
		burbHouse.transform.position = p;
		burbHouse.transform.rotation = rot;
		burbHouse.Init(null);
		this.Landmarks.Add(burbHouse);
		return burbHouse;
	}

	private BurbHouse PlaceHouse(Vector3 p, Quaternion rot, Vector2[] polygon, float area)
	{
		Matrix4x4 matrix4x = Matrix4x4.TRS(p, rot, Vector3.one);
		for (int i = 0; i < this.BurbHousePrefabs.Length; i++)
		{
			BurbHouse burbHouse = this.BurbHousePrefabs[i];
			if (burbHouse.LowerAreaReq <= area)
			{
				bool flag = true;
				for (int j = 0; j < burbHouse.Bounds.Length; j++)
				{
					Vector2 p2 = matrix4x.MultiplyPoint(burbHouse.Bounds[j].ToVector3(0f)).FlattenVector3();
					if (!Utilities.IsInside(p2, polygon))
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					BurbHouse burbHouse2 = UnityEngine.Object.Instantiate<BurbHouse>(burbHouse);
					burbHouse2.transform.position = p;
					burbHouse2.transform.rotation = rot;
					burbHouse2.Init(polygon);
					this.Landmarks.Add(burbHouse2);
					return burbHouse2;
				}
			}
		}
		return null;
	}

	private List<Rect> CreatePlotDiffs(Rect surPlot)
	{
		List<Rect> list = new List<Rect>();
		if (surPlot.xMax == 24f)
		{
			if (surPlot.yMin < 120f)
			{
				list.Add(Rect.MinMaxRect(8f, surPlot.yMin, 24f, 120f));
			}
			if (surPlot.yMax > 136f)
			{
				list.Add(Rect.MinMaxRect(8f, 136f, 24f, surPlot.yMax));
			}
			return list;
		}
		list.Add(Rect.MinMaxRect(24f, surPlot.yMin, surPlot.xMax, surPlot.yMax));
		if (surPlot.yMin < 120f)
		{
			list.Add(Rect.MinMaxRect(8f, surPlot.yMin, 24f, 120f));
		}
		if (surPlot.yMax > 136f)
		{
			list.Add(Rect.MinMaxRect(8f, 136f, 24f, surPlot.yMax));
		}
		return list;
	}

	private List<Vector2> CreatePlotDiff(Rect surPlot)
	{
		if (surPlot.xMax != 24f)
		{
			List<Vector2> list = new List<Vector2>();
			if (surPlot.yMin < 120f)
			{
				list.Add(new Vector2(24f, 120f));
				list.Add(new Vector2(8f, 120f));
				list.Add(new Vector2(8f, surPlot.yMin));
			}
			else
			{
				list.Add(new Vector2(24f, surPlot.yMin));
			}
			list.Add(new Vector2(surPlot.xMax, surPlot.yMin));
			list.Add(new Vector2(surPlot.xMax, surPlot.yMax));
			if (surPlot.yMax > 136f)
			{
				list.Add(new Vector2(8f, surPlot.yMax));
				list.Add(new Vector2(8f, 136f));
				list.Add(new Vector2(24f, 136f));
			}
			else
			{
				list.Add(new Vector2(24f, surPlot.yMax));
			}
			return list;
		}
		if (surPlot.yMin < 120f)
		{
			return new List<Vector2>
			{
				new Vector2(8f, surPlot.yMin),
				new Vector2(24f, surPlot.yMin),
				new Vector2(24f, 120f),
				new Vector2(8f, 120f)
			};
		}
		if (surPlot.yMax > 136f)
		{
			return new List<Vector2>
			{
				new Vector2(8f, 136f),
				new Vector2(24f, 136f),
				new Vector2(24f, surPlot.yMax),
				new Vector2(8f, surPlot.yMax)
			};
		}
		return null;
	}

	public void UpdateTreeRemoval()
	{
		for (int i = 0; i < this.RoadMap.GetLength(0); i++)
		{
			for (int j = 0; j < this.RoadMap.GetLength(1); j++)
			{
				if (this.RoadMap[i, j] > 0)
				{
					this.RemoveTrees(i, j);
				}
			}
		}
	}

	private List<KeyValuePair<Rect, float>> PlaceBuildings(List<Rect> blobs)
	{
		int num = blobs.Count / 2;
		List<KeyValuePair<Rect, float>> list = new List<KeyValuePair<Rect, float>>();
		for (int i = 0; i < num; i++)
		{
			Rect random = blobs.GetRandom(GameData.RND);
			blobs.Remove(random);
			SkraperGen skraperGen = this.PlaceBuilding(random, GameData.RNDValue > 0.5f, GameData.RNDValue > 0.5f, -1f);
			if (GameData.RNDValue > 0.5f)
			{
				list.Add(new KeyValuePair<Rect, float>(random, random.width * random.height * skraperGen.Height * 100f));
			}
		}
		return list;
	}

	public SkraperGen PlaceBuilding(Rect blob, bool thn, bool thw, float h = -1f)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.BuildingPrefab);
		SkraperGen component = gameObject.GetComponent<SkraperGen>();
		component.Init(blob, thn, thw, 0, 0, 0, true, h);
		this.Landmarks.Add(component);
		return component;
	}

	public List<UndoObject.UndoAction> UpdateScrapers()
	{
		List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
		for (int i = 0; i < this.Landmarks.Count; i++)
		{
			Landmark landmark = this.Landmarks[i];
			if (GameSettings.Instance.PlayerOwnedPoint(landmark.Center()))
			{
				list.Add(new UndoObject.UndoAction(landmark, true));
				UnityEngine.Object.Destroy(landmark.gameObject);
				GameSettings.Instance.sRoomManager.Outside.DirtyNavMesh = true;
			}
		}
		return list;
	}

	private void PlaceLine(bool horz, int rangeMin, int rangeMax, int borderMin, int borderMax, List<Rect> areas, int mid = -1, int stop = 5)
	{
		if (mid == -1)
		{
			int num = (rangeMax - rangeMin) / 4;
			num = GameData.RNDRange(-num, num);
			mid = (rangeMin + rangeMax) / 2 + num;
		}
		Rect r;
		if (horz)
		{
			r = new Rect((float)mid, (float)borderMin, 1f, (float)(borderMax - borderMin));
		}
		else
		{
			r = new Rect((float)borderMin, (float)mid, (float)(borderMax - borderMin), 1f);
		}
		this.ForcePlaceRoad(r, 1);
		if (borderMax - borderMin > stop)
		{
			this.PlaceLine(!horz, borderMin, borderMax, rangeMin, mid, areas, -1, stop);
			this.PlaceLine(!horz, borderMin, borderMax, mid + 1, rangeMax, areas, -1, stop);
		}
		else if (horz)
		{
			areas.Add(new Rect((float)rangeMin, (float)borderMin, (float)(mid - rangeMin), (float)(borderMax - borderMin)));
			areas.Add(new Rect((float)(mid + 1), (float)borderMin, (float)(rangeMax - mid - 1), (float)(borderMax - borderMin)));
		}
		else
		{
			areas.Add(new Rect((float)borderMin, (float)rangeMin, (float)(borderMax - borderMin), (float)(mid - rangeMin)));
			areas.Add(new Rect((float)borderMin, (float)(mid + 1), (float)(borderMax - borderMin), (float)(rangeMax - mid - 1)));
		}
	}

	private void OnDestory()
	{
		if (RoadManager.Instance == this)
		{
			RoadManager.Instance = null;
		}
	}

	private RoadNode FindFirst(RoadSegment seg, bool getOut)
	{
		if (getOut)
		{
			if (seg.EastOut != null)
			{
				return seg.EastOut;
			}
			if (seg.NorthOut != null)
			{
				return seg.NorthOut;
			}
			if (seg.WestOut != null)
			{
				return seg.WestOut;
			}
			if (seg.SouthOut != null)
			{
				return seg.SouthOut;
			}
		}
		else
		{
			if (seg.WestIn != null)
			{
				return seg.WestIn;
			}
			if (seg.SouthIn != null)
			{
				return seg.SouthIn;
			}
			if (seg.EastIn != null)
			{
				return seg.EastIn;
			}
			if (seg.NorthIn != null)
			{
				return seg.NorthIn;
			}
		}
		return null;
	}

	public List<Vector2> FindPath(RoadNode p1, RoadNode p2)
	{
		KeyValuePair<RoadNode, RoadNode> key = new KeyValuePair<RoadNode, RoadNode>(p1, p2);
		List<Vector2> result;
		if (this.CachedPaths.TryGetValue(key, out result))
		{
			return result;
		}
		Func<Vector2, Vector2, float> func = (Vector2 xx, Vector2 yy) => (xx - yy).magnitude;
		object target = p2.self.Tag;
		List<PathNode<Vector2>> list = NodePathFinding<Vector2>.FindPath(p1.self, p2.self, func, func, (object tt) => !((RoadNode)tt).Parking || tt == target, Vector2.zero);
		if (list != null)
		{
			List<Vector2> list2 = (from pp in list
			select pp.Point).ToList<Vector2>();
			for (int i = 1; i < list2.Count - 1; i++)
			{
				if ((list2[i] - list2[i + 1]).magnitude < 1f)
				{
					list2.RemoveAt(i);
					i--;
				}
			}
			if (this.CachedPaths.Count < 256)
			{
				this.CachedPaths[key] = list2;
			}
			return list2;
		}
		return null;
	}

	public RoadNode FindRandomParking()
	{
		return (from x in this.Parking.Keys
		where !x.Taken
		select x).GetRandom<RoadNode>();
	}

	public List<Vector2> MakeRoadPlan(ref RoadNode goal)
	{
		if (goal != null)
		{
			RoadNode g = goal;
			foreach (RoadNode roadNode in from x in this.InputList
			orderby x.GetPos().ManhattanDist(g.GetPos())
			select x)
			{
				List<Vector2> list = this.FindPath(roadNode, goal);
				if (list != null)
				{
					this.FixPathEnds(list, !roadNode.Parking, !goal.Parking);
					return list;
				}
			}
			HUD.Instance.UnreachableParking.Add(goal);
			return null;
		}
		List<int> list2 = this.InputList.Select((RoadNode x, int i) => i).ToList<int>();
		int random = list2.GetRandom<int>();
		list2.Remove(random);
		int random2 = list2.GetRandom<int>();
		goal = this.OutputList[random2];
		List<Vector2> list3 = this.FindPath(this.InputList[random], goal);
		if (list3 != null)
		{
			this.FixPathEnds(list3, !this.InputList[random].Parking, !goal.Parking);
		}
		return list3;
	}

	private void FixPathEnds(List<Vector2> path, bool start, bool end)
	{
		if (path.Count >= 3)
		{
			if (start)
			{
				path[0] = path[0] + (path[0] - path[1]).normalized * 2f;
			}
			if (end)
			{
				path[path.Count - 1] = path[path.Count - 1] + (path[path.Count - 1] - path[path.Count - 2]).normalized * 2f;
			}
		}
	}

	public List<Vector2> GetHome(RoadNode start)
	{
		List<int> list = (from x in this.OutputList.Select((RoadNode x, int i) => i)
		orderby UnityEngine.Random.value
		select x).ToList<int>();
		for (int j = 0; j < list.Count; j++)
		{
			RoadNode p = this.OutputList[list[j]];
			List<Vector2> list2 = this.FindPath(start, p);
			if (list2 != null)
			{
				return list2;
			}
		}
		HUD.Instance.UnreachableParking.Add(start);
		return null;
	}

	private void OnDrawGizmos()
	{
		if (this.Points != null)
		{
			Gizmos.color = Color.red;
			for (int i = 0; i < this.Points.Count - 1; i++)
			{
				Vector2 vector = this.Points[i];
				Vector2 vector2 = this.Points[i + 1];
				Gizmos.DrawLine(new Vector3(vector.x, 1f, vector.y), new Vector3(vector2.x, 1f, vector2.y));
			}
			Gizmos.color = Color.white;
		}
	}

	public override string WriteName()
	{
		return "RoadManager";
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		this.RoadMap = dictionary.Get<byte[,]>("Roads", new byte[(int)this.GridSize.x, (int)this.GridSize.y]);
		this.ObjectMap = new RoadSegment[this.RoadMap.GetLength(0), this.RoadMap.GetLength(1)];
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		for (int i = 0; i < this.RoadMap.GetLength(0); i++)
		{
			for (int j = 0; j < this.RoadMap.GetLength(1); j++)
			{
				if (this.RoadMap[i, j] > 0)
				{
					this.UpdateRoad(i, j, true, false);
				}
			}
		}
		for (int k = 0; k < this.RoadMap.GetLength(0); k++)
		{
			for (int l = 0; l < this.RoadMap.GetLength(1); l++)
			{
				if (this.RoadMap[k, l] > 0)
				{
					this.UpdateConnections(k, l);
				}
			}
		}
		this.PlaceRoadLamps();
		this.AddedSpots.Clear();
		Debug.Log("Loaded road map in " + (Time.realtimeSinceStartup - realtimeSinceStartup));
		WriteDictionary[] array = dictionary.Get<WriteDictionary[]>("Landmarks", new WriteDictionary[0]);
		foreach (WriteDictionary dict in array)
		{
			this.DeserializeLandmark(dict);
		}
		Dictionary<Vector3I, RoadNode.ParkingAssign> dictionary2 = dictionary.Get<Dictionary<Vector3I, RoadNode.ParkingAssign>>("Parking", new Dictionary<Vector3I, RoadNode.ParkingAssign>());
		using (Dictionary<Vector3I, RoadNode.ParkingAssign>.Enumerator enumerator = dictionary2.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				KeyValuePair<Vector3I, RoadNode.ParkingAssign> p = enumerator.Current;
				RoadSegment segment = this.GetSegment(p.Key.x, p.Key.y);
				if (segment != null)
				{
					RoadNode roadNode = segment.Parking.FirstOrDefault((RoadNode x) => x.ID == p.Key.z);
					if (roadNode != null)
					{
						roadNode.Assign = p.Value;
					}
				}
			}
		}
		return this;
	}

	public void DeserializeLandmark(WriteDictionary dict)
	{
		if (dict.Name.Equals("SkyScraper"))
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.BuildingPrefab);
			SkraperGen component = gameObject.GetComponent<SkraperGen>();
			component.DeserializeThis(dict);
			this.Landmarks.Add(component);
		}
		else if (dict.Name.Equals("BurbHouse"))
		{
			BurbHouse burbHouse = UnityEngine.Object.Instantiate<BurbHouse>(this.BurbHousePrefabs[dict.Get<int>("Idx", 0)]);
			this.Landmarks.Add(burbHouse);
			burbHouse.DeserializeThis(dict);
		}
	}

	public GameObject FindLandmark(uint did)
	{
		for (int i = 0; i < this.Landmarks.Count; i++)
		{
			Landmark landmark = this.Landmarks[i];
			if (landmark.DID == did)
			{
				return landmark.gameObject;
			}
		}
		return null;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["Roads"] = this.RoadMap;
		dictionary["Landmarks"] = (from x in this.Landmarks
		select x.SerializeThis(GameReader.LoadMode.Full)).ToArray<WriteDictionary>();
		dictionary["Parking"] = (from x in this.Parking.Keys
		where x.Assign != RoadNode.ParkingAssign.Anyone
		select x).ToDictionary((RoadNode x) => new Vector3I(Mathf.FloorToInt(x.transform.position.x / this.RoadSize), Mathf.FloorToInt(x.transform.position.z / this.RoadSize), x.ID), (RoadNode x) => x.Assign);
	}

	public static RoadManager Instance;

	public RoadManager.RoadPiece[] RoadPieces;

	public GameObject ParkingHors;

	public Vector2 GridSize;

	public Color CompanyCarColor;

	public float RoadSize;

	public byte[,] RoadMap;

	public bool[,] Visited;

	public List<GameObject> Lamps = new List<GameObject>();

	public GameObject LampPrefab;

	[Header("In order of size starting with biggest")]
	public BurbHouse[] BurbHousePrefabs;

	public RoadSegment[,] ObjectMap;

	public Vector2 PCur;

	public bool PClick;

	private List<Vector2> Points;

	private Dictionary<RoadNode, bool> Parking = new Dictionary<RoadNode, bool>();

	private List<RoadNode> InputList = new List<RoadNode>();

	private List<RoadNode> OutputList = new List<RoadNode>();

	public List<CarScript> Cars = new List<CarScript>();

	public Dictionary<int, HashSet<CarScript>> CachedCars = new Dictionary<int, HashSet<CarScript>>();

	public GameObject[] CarPrefabs;

	public GameObject BuildingPrefab;

	private List<RoadNode> AddedSpots = new List<RoadNode>();

	public List<Landmark> Landmarks = new List<Landmark>();

	[NonSerialized]
	private Dictionary<KeyValuePair<RoadNode, RoadNode>, List<Vector2>> CachedPaths = new Dictionary<KeyValuePair<RoadNode, RoadNode>, List<Vector2>>();

	[Serializable]
	public struct RoadPiece
	{
		[SerializeField]
		public GameObject Piece;

		[SerializeField]
		public int Rotation;
	}
}
