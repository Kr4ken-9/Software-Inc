﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ProductPrinter : Writeable
{
	public Furniture Furn
	{
		get
		{
			if (this._furn == null)
			{
				this._furn = base.GetComponent<Furniture>();
			}
			return this._furn;
		}
	}

	private void Start()
	{
		if (GameSettings.Instance != null)
		{
			GameSettings.Instance.ProductPrinters.Add(this);
			TutorialSystem.Instance.StartTutorial("Distribution", false);
		}
	}

	private void OnDestroy()
	{
		if (GameSettings.Instance != null)
		{
			GameSettings.Instance.ProductPrinters.Remove(this);
			if (this.LocalOrder != null)
			{
				this.LocalOrder.RemoveFromStorage();
			}
		}
	}

	private bool AnyOrders()
	{
		foreach (KeyValuePair<uint, PrintJob> keyValuePair in GameSettings.Instance.PrintOrders)
		{
			if (keyValuePair.Value.Priority > 0f && keyValuePair.Value.IsActive())
			{
				return true;
			}
		}
		return false;
	}

	private ProductPrintOrder GenerateOrder()
	{
		Dictionary<uint, uint> dictionary = new Dictionary<uint, uint>();
		int num = this.PrintAmount;
		List<PrintJob> list = (from x in GameSettings.Instance.PrintOrders.Values
		where x.IsActive()
		select x).ToList<PrintJob>();
		float num2 = 0f;
		for (int i = 0; i < list.Count; i++)
		{
			num2 += list[i].Priority;
		}
		int j = 0;
		while (j < list.Count)
		{
			PrintJob printJob = list[j];
			IStockable stockable = null;
			if (printJob.DealID == null)
			{
				goto IL_104;
			}
			Deal deal = null;
			if (!HUD.Instance.dealWindow.AllDeals.TryGetValue(printJob.DealID.Value, out deal))
			{
				goto IL_104;
			}
			PrintDeal printDeal = deal as PrintDeal;
			if (printDeal == null)
			{
				goto IL_104;
			}
			uint num3 = printDeal.CurrentAmount + GameSettings.Instance.GetPrintsInStorage(printJob.ID, false);
			if (num3 <= printDeal.Goal)
			{
				goto IL_104;
			}
			IL_279:
			j++;
			continue;
			IL_104:
			int num4 = Mathf.Min(num, Mathf.RoundToInt(printJob.Priority / num2 * (float)this.PrintAmount));
			if (printJob.Limit != null)
			{
				if ((long)num4 >= (long)((ulong)printJob.Limit.Value))
				{
					stockable = printJob.GetStockable();
					if (stockable != null)
					{
						HUD.Instance.AddPopupMessage("FinishedPrintingJob".Loc(new object[]
						{
							stockable.GetName()
						}), "Info", PopupManager.PopUpAction.OpenProductDetails, printJob.ID, PopupManager.NotificationSound.Good, 0.25f, PopupManager.PopupIDs.None, 4);
					}
					num4 = (int)printJob.Limit.Value;
					GameSettings.Instance.CancelPrintOrder(printJob.ID, false);
					HUD.Instance.distributionWindow.RefreshOrders();
				}
				else
				{
					PrintJob printJob2 = printJob;
					uint? limit = printJob2.Limit;
					printJob2.Limit = ((limit == null) ? null : new uint?(limit.GetValueOrDefault() - (uint)num4));
				}
			}
			num -= num4;
			if (num4 > 0)
			{
				float num5 = (float)num4 * this.PrintPrice;
				if (printJob.DealID == null)
				{
					stockable = (stockable ?? printJob.GetStockable());
					if (stockable != null)
					{
						stockable.AddLoss(num5);
					}
				}
				GameSettings.Instance.MyCompany.MakeTransaction(-num5, Company.TransactionCategory.Bills, "Printing");
				dictionary[printJob.ID] = (uint)num4;
			}
			if (num == 0)
			{
				break;
			}
			goto IL_279;
		}
		if (num == this.PrintAmount)
		{
			return null;
		}
		ProductPrintOrder productPrintOrder = new ProductPrintOrder(dictionary);
		productPrintOrder.AddToStorage();
		return productPrintOrder;
	}

	public float ActalPrintSpeed()
	{
		float num = this.Furn.upg.Quality;
		if (num < 0.5f)
		{
			num *= 2f;
		}
		else
		{
			num = 1f;
		}
		return this.PrintSpeed * num / (float)GameSettings.DaysPerMonth;
	}

	public void Tick(float delta)
	{
		if (this.Furn.upg.Broken)
		{
			this.Furn.IsOn = false;
			return;
		}
		if (!this.Furn.IsOn && !this.AnyOrders())
		{
			return;
		}
		bool isOn = this.Furn.IsOn;
		this.NextPrint -= Utilities.PerHour(this.ActalPrintSpeed(), delta, false);
		ProductPallet productPallet = null;
		while (this.NextPrint <= 0f)
		{
			if (!this.AnyOrders())
			{
				this.NextPrint = 0f;
				this.Furn.IsOn = false;
				return;
			}
			if (productPallet != null && productPallet.CurrentAmount >= productPallet.MaxOrders)
			{
				productPallet = null;
			}
			if (productPallet == null)
			{
				HashList<Furniture> furniture = this.Furn.Parent.GetFurniture("Pallet");
				for (int i = 0; i < furniture.Count; i++)
				{
					Furniture furniture2 = furniture[i];
					if (furniture2 != null)
					{
						ProductPallet component = furniture2.GetComponent<ProductPallet>();
						if (component != null && component.CurrentAmount < component.MaxOrders)
						{
							productPallet = component;
							break;
						}
					}
				}
			}
			if (this.LocalOrder != null)
			{
				if (!(productPallet != null))
				{
					isOn = false;
					this.NextPrint = 1f;
					break;
				}
				isOn = true;
				productPallet.AddOrder(this.LocalOrder);
				this.LocalOrder = null;
				this.LocalBox.SetActive(false);
			}
			else
			{
				ProductPrintOrder productPrintOrder = this.GenerateOrder();
				if (productPrintOrder != null)
				{
					if (!(productPallet != null))
					{
						isOn = false;
						this.LocalOrder = productPrintOrder;
						this.LocalBox.SetActive(true);
						this.NextPrint = 1f;
						HUD.Instance.AddPopupMessage("NotEnoughPallets".Loc(), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.ProductPrinterBackedUp, 1);
						break;
					}
					productPallet.AddOrder(productPrintOrder);
					isOn = true;
				}
			}
			this.NextPrint += 1f;
		}
		this.Furn.IsOn = isOn;
	}

	public ProductPrintOrder Take()
	{
		ProductPrintOrder localOrder = this.LocalOrder;
		this.LocalOrder = null;
		this.LocalBox.SetActive(false);
		return localOrder;
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		this.LocalOrder = dictionary.Get<ProductPrintOrder>("PrinterOrder", null);
		this.LocalBox.SetActive(this.LocalOrder != null);
		return this;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["PrinterOrder"] = this.LocalOrder;
	}

	public float PrintPrice = 0.1f;

	public float PrintSpeed;

	public int PrintAmount;

	[NonSerialized]
	public float NextPrint = 1f;

	[NonSerialized]
	private Furniture _furn;

	[NonSerialized]
	public ProductPrintOrder LocalOrder;

	public GameObject LocalBox;
}
