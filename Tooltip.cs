﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
	public static bool IsShowing
	{
		get
		{
			return Tooltip.Instance != null && Tooltip.Instance.gameObject.activeSelf;
		}
	}

	public static RectTransform CurrentRect
	{
		get
		{
			return (!(Tooltip.Instance == null)) ? Tooltip.Instance.Rect : null;
		}
		set
		{
			if (Tooltip.Instance != null)
			{
				Tooltip.Instance.Rect = value;
			}
		}
	}

	public static void SetToolTip(string title, string description, RectTransform rect)
	{
		bool flag = string.IsNullOrEmpty(title);
		bool flag2 = string.IsNullOrEmpty(description);
		if (Tooltip.Instance != null && (!flag || !flag2))
		{
			Tooltip.Instance.ForcePosition = false;
			Tooltip.Instance.TitlePanel.SetActive(!flag);
			Tooltip.Instance.Title.text = (title ?? string.Empty);
			Tooltip.Instance.Description.gameObject.SetActive(!flag2);
			Tooltip.Instance.Description.text = (description ?? string.Empty);
			Tooltip.Instance.Rect = rect;
			LayoutElement component = Tooltip.Instance.Description.GetComponent<LayoutElement>();
			float preferredWidth = Mathf.Max((float)((!flag2) ? 256 : 128), Tooltip.Instance.Title.preferredWidth + 8f);
			Tooltip.Instance.TitlePanel.GetComponent<LayoutElement>().preferredWidth = preferredWidth;
			component.preferredWidth = preferredWidth;
			Tooltip.Instance.gameObject.SetActive(true);
			Tooltip.Instance.transform.SetAsLastSibling();
			Tooltip.Instance.UpdatePosition();
		}
	}

	public static void UpdateToolTip(string title, string description, Vector2 pos)
	{
		bool flag = string.IsNullOrEmpty(title);
		bool flag2 = string.IsNullOrEmpty(description);
		if (Tooltip.Instance != null && (!flag || !flag2))
		{
			Tooltip.Instance.ForcePosition = true;
			Tooltip.Instance.TRect.anchoredPosition = new Vector2(pos.x, pos.y);
			Tooltip.Instance.TitlePanel.SetActive(!flag);
			Tooltip.Instance.Title.text = (title ?? string.Empty);
			Tooltip.Instance.Description.gameObject.SetActive(!flag2);
			Tooltip.Instance.Description.text = (description ?? string.Empty);
			LayoutElement component = Tooltip.Instance.Description.GetComponent<LayoutElement>();
			float preferredWidth = Mathf.Max((float)((!flag2) ? 256 : 128), Tooltip.Instance.Title.preferredWidth + 8f);
			Tooltip.Instance.TitlePanel.GetComponent<LayoutElement>().preferredWidth = preferredWidth;
			component.preferredWidth = preferredWidth;
		}
	}

	public static void UpdateToolTip(string title, string description)
	{
		bool flag = string.IsNullOrEmpty(title);
		bool flag2 = string.IsNullOrEmpty(description);
		if (Tooltip.Instance != null && (!flag || !flag2))
		{
			Tooltip.Instance.TitlePanel.SetActive(!flag);
			Tooltip.Instance.Title.text = (title ?? string.Empty);
			Tooltip.Instance.Description.gameObject.SetActive(!flag2);
			Tooltip.Instance.Description.text = (description ?? string.Empty);
			LayoutElement component = Tooltip.Instance.Description.GetComponent<LayoutElement>();
			float preferredWidth = Mathf.Max((float)((!flag2) ? 256 : 128), Tooltip.Instance.Title.preferredWidth + 8f);
			Tooltip.Instance.TitlePanel.GetComponent<LayoutElement>().preferredWidth = preferredWidth;
			component.preferredWidth = preferredWidth;
		}
	}

	private void UpdatePosition()
	{
		float num = Input.mousePosition.x;
		float num2 = Input.mousePosition.y;
		num2 -= (float)Screen.height;
		num = Mathf.Clamp(num / Options.UISize + 16f, 0f, (float)Screen.width / Options.UISize - this.TRect.rect.width);
		num2 = Mathf.Clamp(num2 / Options.UISize - 16f, (float)(-(float)Screen.height) / Options.UISize + this.TRect.rect.height, 0f);
		this.TRect.anchoredPosition = new Vector2(num, num2);
	}

	private void Update()
	{
		if (!this.ForcePosition)
		{
			this.UpdatePosition();
		}
		if (this.Rect == null || !this.Rect.gameObject.activeInHierarchy || !RectTransformUtility.RectangleContainsScreenPoint(this.Rect, Input.mousePosition, null))
		{
			this.Rect = null;
			base.gameObject.SetActive(false);
		}
	}

	private void OnDestroy()
	{
		if (Tooltip.Instance == this)
		{
			Tooltip.Instance = null;
		}
	}

	private void Start()
	{
		Tooltip.Instance = this;
		this.TRect = base.GetComponent<RectTransform>();
		base.gameObject.SetActive(false);
	}

	public static void Hide()
	{
		if (Tooltip.Instance != null)
		{
			Tooltip.Instance.gameObject.SetActive(false);
		}
	}

	public Text Title;

	public Text Description;

	public GameObject TitlePanel;

	private RectTransform Rect;

	private RectTransform TRect;

	private static Tooltip Instance;

	public bool ForcePosition;
}
