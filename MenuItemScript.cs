﻿using System;
using System.Diagnostics;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuItemScript : MonoBehaviour
{
	private void Start()
	{
		for (int i = 0; i < this.Tweens.Length; i++)
		{
			MenuItemScript.TweenerRect tweenerRect = this.Tweens[i];
			if (tweenerRect.TweenPos)
			{
				tweenerRect.MainTransform.anchoredPosition = tweenerRect.PosStart;
				TweenSettingsExtensions.SetDelay<Tweener>(ShortcutExtensions46.DOAnchorPos(tweenerRect.MainTransform, tweenerRect.PosEnd, tweenerRect.Time, false), tweenerRect.Delay);
			}
			if (tweenerRect.TweenSize)
			{
				tweenerRect.MainTransform.sizeDelta = tweenerRect.SizeStart;
				TweenSettingsExtensions.SetDelay<Tweener>(ShortcutExtensions46.DOSizeDelta(tweenerRect.MainTransform, tweenerRect.SizeEnd, tweenerRect.Time, false), tweenerRect.Delay);
			}
		}
	}

	public void Action(int action)
	{
		if (this.disable)
		{
			return;
		}
		switch (action)
		{
		case 0:
			this.disable = true;
			SiteNewsFeeder.AbortIfActive();
			ErrorLogging.FirstOfScene = true;
			ErrorLogging.SceneChanging = true;
			SceneManager.LoadScene("Customization");
			break;
		case 1:
		{
			SaveGame currentSaveGame = this.mainMenu.CurrentSaveGame;
			if (currentSaveGame != null)
			{
				this.mainMenu.WaitPanel.SetActive(true);
				SiteNewsFeeder.AbortIfActive();
				this.disable = SaveGameManager.LoadGame(currentSaveGame, null, false, true, true, false);
				this.mainMenu.WaitPanel.SetActive(this.disable);
			}
			else
			{
				WindowManager.SpawnDialog().Show("MissingSaveContinue".Loc(), true, DialogWindow.DialogType.Error, null);
			}
			break;
		}
		case 2:
			SaveGameManager.Instance.Show(false, false, false, false, null);
			break;
		case 3:
			OptionsWindow.Instance.Show();
			break;
		case 4:
			GameSettings.IsQuitting = true;
			Process.GetCurrentProcess().Kill();
			break;
		case 5:
			SaveGameManager.Instance.Show(false, true, true, true, null);
			break;
		}
	}

	private bool disable;

	public MainMenuController mainMenu;

	public MenuItemScript.TweenerRect[] Tweens;

	[Serializable]
	public struct TweenerRect
	{
		public RectTransform MainTransform;

		public Vector2 PosStart;

		public Vector2 PosEnd;

		public Vector2 SizeStart;

		public Vector2 SizeEnd;

		public float Time;

		public float Delay;

		public bool TweenPos;

		public bool TweenSize;
	}
}
