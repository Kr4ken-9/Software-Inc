﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Feature
{
	public Feature(Feature feat)
	{
		this.Name = feat.Name;
		this.From = feat.From;
		this.Description = feat.Description;
		this.Category = feat.Category;
		this.ArtCategory = feat.ArtCategory;
		this.Forced = feat.Forced;
		this.Base = feat.Base;
		this.Vital = feat.Vital;
		this.Innovation = feat.Innovation;
		this.Usability = feat.Usability;
		this.Stability = feat.Stability;
		this.CodeArtRatio = feat.CodeArtRatio;
		this.ServerRequirement = feat.ServerRequirement;
		this.DevTime = feat.DevTime;
		this.Unlock = feat.Unlock;
		this.Dependencies = feat.Dependencies;
		this.Research = feat.Research;
		this.SoftwareCategories = feat.SoftwareCategories;
		this.Software = feat.Software;
	}

	public Feature(XMLParser.XMLNode node, string software, bool isBase)
	{
		try
		{
			this.Name = node.GetNode("Name", true).Value;
			this.Description = node.GetNode("Description", true).Value;
			this.From = ((!node.Attributes.ContainsKey("From")) ? null : node.Attributes["From"]);
			this.Forced = (node.Attributes.ContainsKey("Forced") && node.Attributes["Forced"].ConvertToBool("Forced"));
			this.Category = node.GetNode("Category", true).Value;
			XMLParser.XMLNode node2 = node.GetNode("ArtCategory", false);
			if (node2 != null)
			{
				this.ArtCategory = node2.Value;
			}
			if (string.IsNullOrEmpty(this.Category))
			{
				throw new Exception("Missing category definition");
			}
			this.Unlock = null;
			this.Software = software;
			XMLParser.XMLNode[] nodes = node.GetNodes("SoftwareCategory", false);
			if (nodes.Length > 0)
			{
				this.SoftwareCategories = nodes.ToDictionary((XMLParser.XMLNode x) => x.GetAttribute("Category"), (XMLParser.XMLNode x) => x.Value.ConvertToInt("Category"));
				this.Unlock = new int?(this.SoftwareCategories.Min((KeyValuePair<string, int> x) => x.Value));
			}
			else if (!this.Forced)
			{
				XMLParser.XMLNode node3 = node.GetNode("Unlock", false);
				if (node3 != null)
				{
					this.Unlock = new int?(node3.Value.ConvertToInt("Unlock"));
				}
			}
			this.Vital = (node.Attributes.ContainsKey("Vital") && node.Attributes["Vital"].ConvertToBool("Vital"));
			this.Innovation = Mathf.Abs(node.GetNode("Innovation", true).Value.ConvertToFloat("Innovation"));
			this.Usability = Mathf.Abs(node.GetNode("Usability", true).Value.ConvertToFloat("Usability"));
			this.Stability = Mathf.Abs(node.GetNode("Stability", true).Value.ConvertToFloat("Stability"));
			this.CodeArtRatio = Mathf.Clamp01(node.GetNode("CodeArt", true).Value.ConvertToFloat("CodeArt"));
			float num = this.Innovation + this.Usability + this.Stability;
			if (num > 0f)
			{
				this.Innovation /= num;
				this.Usability /= num;
				this.Stability /= num;
			}
			this.DevTime = Mathf.Abs(node.GetNode("DevTime", true).Value.ConvertToFloat("DevTime"));
			this.Dependencies = (from x in node.GetNodes("Dependency", false)
			select new KeyValuePair<string, string>(x.Attributes["Software"], x.Value)).ToArray<KeyValuePair<string, string>>();
			this.ServerRequirement = ((!node.Contains("Server")) ? 0f : Mathf.Clamp01(node.GetNode("Server", true).Value.ConvertToFloat("Server")));
			this.Base = isBase;
			this.Research = ((!node.Attributes.ContainsKey("Research")) ? null : node.Attributes["Research"]);
			if (this.Research != null && !this.Dependencies.Contains(new KeyValuePair<string, string>(software, this.Research)))
			{
				this.Dependencies = this.Dependencies.Concat(new KeyValuePair<string, string>[]
				{
					new KeyValuePair<string, string>(software, this.Research)
				}).ToArray<KeyValuePair<string, string>>();
			}
		}
		catch (Exception ex)
		{
			throw new Exception("Error loading feature " + this.Name + ": " + ex.Message);
		}
	}

	public Feature()
	{
	}

	public uint PatentOwner
	{
		get
		{
			if (this._patentOwner > 0u)
			{
				this.CheckPatentValid();
			}
			return this._patentOwner;
		}
	}

	public bool OneCategory()
	{
		return this.ArtCategory == null;
	}

	public string GetCategory(bool code)
	{
		return (!code && this.ArtCategory != null) ? this.ArtCategory : this.Category;
	}

	public Feature InverseFromLookup(SoftwareType t, string category)
	{
		return t.Features.Values.FirstOrDefault((Feature x) => x.From != null && x.From.Equals(this.Name) && x.IsCompatible(category));
	}

	public bool CheckPatentValid()
	{
		if (this._patentOwner > 0u && GameSettings.Instance.simulation.GetCompany(this._patentOwner) == null)
		{
			this._patentOwner = 0u;
		}
		if (Utilities.GetMonths(this.ResearchDate, SDateTime.Now()) > (float)(Feature.PatentExpiration * 12))
		{
			if (this._patentOwner > 0u)
			{
				Company company = GameSettings.Instance.simulation.GetCompany(this._patentOwner);
				if (company != null)
				{
					if (company.Player)
					{
						HUD.Instance.AddPopupMessage(string.Format("PatentExpiration".Loc(), this.Software.LocSW(), this.Name.SWFeat(this.Software)), "Info", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Neutral, 1f, PopupManager.PopupIDs.None, 6);
					}
					company.Patents.Remove(new KeyValuePair<string, string>(this.Software, this.Name));
				}
				this._patentOwner = 0u;
			}
			return false;
		}
		return true;
	}

	public void TransferPatent(Company newOwner)
	{
		if (!this.Researched)
		{
			this.ResearchDate = SDateTime.Now();
			this.Researched = true;
		}
		if (!this.CheckPatentValid())
		{
			return;
		}
		if (this.PatentOwner != 0u)
		{
			Company company = GameSettings.Instance.simulation.GetCompany(this.PatentOwner);
			if (company != null)
			{
				company.Patents.Remove(new KeyValuePair<string, string>(this.Software, this.Name));
			}
		}
		if (newOwner == null)
		{
			this._patentOwner = 0u;
		}
		else
		{
			this._patentOwner = newOwner.ID;
			newOwner.Patents.Add(new KeyValuePair<string, string>(this.Software, this.Name));
		}
	}

	public bool IsCompatible(string softwareCategory)
	{
		return this.SoftwareCategories == null || softwareCategory == null || this.SoftwareCategories.ContainsKey(softwareCategory);
	}

	public void FindDependencies(string swType, HashSet<string> result, Dictionary<string, SoftwareType> types, SoftwareType parent)
	{
		foreach (KeyValuePair<string, string> keyValuePair in this.Dependencies)
		{
			if (keyValuePair.Key.Equals(swType))
			{
				result.Add(keyValuePair.Value);
			}
			if (swType.Equals("Operating System") && keyValuePair.Key.Equals(parent.OSNeed))
			{
				types[keyValuePair.Key].Features[keyValuePair.Value].FindDependencies(swType, result, types, parent);
			}
		}
	}

	public bool IsUnlocked(int year, string cat)
	{
		if (cat != null && this.SoftwareCategories != null)
		{
			int num = 0;
			return this.SoftwareCategories.TryGetValue(cat, out num) && year >= num - SDateTime.BaseYear;
		}
		return this.Unlock == null || year >= this.Unlock.Value - SDateTime.BaseYear;
	}

	public int GetUnlock(string cat)
	{
		if (cat == null || this.SoftwareCategories == null)
		{
			int? unlock = this.Unlock;
			return (unlock == null) ? 0 : unlock.Value;
		}
		int result = 0;
		if (this.SoftwareCategories.TryGetValue(cat, out result))
		{
			return result;
		}
		return 0;
	}

	public bool IsUnlocked(string cat, int year, Dictionary<string, SoftwareType> sw)
	{
		if (this.Research != null && !this.Researched)
		{
			return false;
		}
		if (!this.IsUnlocked(year, cat))
		{
			return false;
		}
		if (this.Dependencies.Length > 0)
		{
			foreach (KeyValuePair<string, string> keyValuePair in this.Dependencies)
			{
				if (!sw[keyValuePair.Key].Features[keyValuePair.Value].IsUnlocked(null, year, sw))
				{
					return false;
				}
			}
			return true;
		}
		return true;
	}

	public Feature[] GetDependencies(SoftwareType type)
	{
		return (from x in this.Dependencies
		where x.Key.Equals(type.Name)
		select type.Features[x.Value]).ToArray<Feature>();
	}

	public XMLParser.XMLNode ToNode()
	{
		XMLParser.XMLNode xmlnode = new XMLParser.XMLNode("Name", this.Name, null);
		XMLParser.XMLNode xmlnode2 = new XMLParser.XMLNode("Description", this.Description, null);
		XMLParser.XMLNode xmlnode3 = new XMLParser.XMLNode("DevTime", this.DevTime.ToString(), null);
		XMLParser.XMLNode xmlnode4 = new XMLParser.XMLNode("Innovation", this.Innovation.ToString(), null);
		XMLParser.XMLNode xmlnode5 = new XMLParser.XMLNode("Usability", this.Usability.ToString(), null);
		XMLParser.XMLNode xmlnode6 = new XMLParser.XMLNode("Stability", this.Stability.ToString(), null);
		XMLParser.XMLNode xmlnode7 = new XMLParser.XMLNode("CodeArt", this.CodeArtRatio.ToString(), null);
		XMLParser.XMLNode[] array = (from x in this.Dependencies
		select new XMLParser.XMLNode("Dependency", x.Value, new Dictionary<string, string>
		{
			{
				"Software",
				x.Key
			}
		})).ToArray<XMLParser.XMLNode>();
		XMLParser.XMLNode xmlnode8 = new XMLParser.XMLNode("Dependencies", array);
		XMLParser.XMLNode xmlnode9 = new XMLParser.XMLNode("Feature", new XMLParser.XMLNode[]
		{
			xmlnode,
			xmlnode2,
			xmlnode3,
			xmlnode4,
			xmlnode5,
			xmlnode6,
			xmlnode7,
			xmlnode8
		});
		if (this.Forced)
		{
			xmlnode9.Attributes["Forced"] = "TRUE";
		}
		if (this.From != null)
		{
			xmlnode9.Attributes["From"] = this.From;
		}
		return xmlnode9;
	}

	public override string ToString()
	{
		return this.Name;
	}

	public static int PatentExpiration = 15;

	public readonly string Name;

	public readonly string From;

	public readonly string Description;

	public readonly string Software;

	private readonly string Category;

	private readonly string ArtCategory;

	public readonly bool Forced;

	public readonly bool Base;

	public readonly bool Vital;

	public readonly float Innovation;

	public readonly float Usability;

	public readonly float Stability;

	public readonly float CodeArtRatio;

	public readonly float ServerRequirement;

	public readonly float DevTime;

	public readonly int? Unlock;

	public readonly KeyValuePair<string, string>[] Dependencies;

	public readonly string Research;

	public readonly Dictionary<string, int> SoftwareCategories;

	public float Royalty;

	public float Income;

	public bool Researched;

	private uint _patentOwner;

	public SDateTime ResearchDate;
}
