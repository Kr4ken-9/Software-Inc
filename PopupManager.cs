﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupManager : MonoBehaviour
{
	private GUIPopUp HasID(int id)
	{
		if (id == -1)
		{
			return null;
		}
		GUIPopUp result = null;
		if (this.PopupIDDict.TryGetValue(id, out result))
		{
			return result;
		}
		return null;
	}

	private void PlaySFX(PopupManager.NotificationSound sfx)
	{
		UISoundFX.PlaySFX("Notification" + sfx.ToString(), -1f, 0f);
	}

	public void AddPopup(string message, string icon, PopupManager.PopUpAction action, uint target, Color textColor, float importance, PopupManager.NotificationSound sfx, int cooldown, PopupManager.PopupIDs idd = PopupManager.PopupIDs.None)
	{
		this.AddPopup(message, icon, action, target, textColor, importance, sfx, cooldown, (int)idd);
	}

	public void UpdateCooldowns()
	{
		for (int i = 0; i < this.PopupButtons.Count - 1; i++)
		{
			if (Utilities.GetDays(this.PopupButtons[i].popup.Time, SDateTime.Now()) <= (float)this.PopupButtons[i].popup.CooldownDays)
			{
				break;
			}
			UnityEngine.Object.Destroy(this.PopupButtons[i].gameObject);
		}
		if (this.PopupButtons.Count > 0)
		{
			GUIPopUp guipopUp = this.PopupButtons[this.PopupButtons.Count - 1];
			if (Utilities.GetDays(guipopUp.popup.Time, SDateTime.Now()) > (float)guipopUp.popup.CooldownDays)
			{
				UnityEngine.Object.Destroy(guipopUp.gameObject);
			}
		}
	}

	public void AddPopup(string message, string icon, PopupManager.PopUpAction action, uint target, Color textColor, float importance, PopupManager.NotificationSound sfx, int cooldown, int id = -1)
	{
		GUIPopUp guipopUp = this.HasID(id);
		if (guipopUp == null)
		{
			this.PlaySFX(sfx);
			this.UpdateCooldowns();
			if (this.PopupButtons.Count > this.MaxPops)
			{
				UnityEngine.Object.Destroy(this.PopupButtons[0].gameObject);
			}
			PopupManager.PopUp popUp = new PopupManager.PopUp(message, icon, action, target, SDateTime.Now(), id, textColor, cooldown);
			popUp.Importance = importance;
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
			gameObject.transform.SetParent(this.ButtonPanel, false);
			GUIPopUp component = gameObject.GetComponent<GUIPopUp>();
			component.popup = popUp;
			component.MainImage.color = this.ImportanceGradient.Evaluate(Mathf.Clamp01(importance));
			this.PopupButtons.Add(component);
			this.PopupIDDict[id] = component;
			component.Made = Time.realtimeSinceStartup;
		}
		else if (importance > 1f && this.PopupButtons[this.PopupButtons.Count - 1] != guipopUp)
		{
			this.PlaySFX(sfx);
			guipopUp.transform.SetAsLastSibling();
			this.PopupButtons.Remove(guipopUp);
			this.PopupButtons.Add(guipopUp);
		}
	}

	public void AddPopup(PopupManager.PopUp pop)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
		gameObject.transform.SetParent(this.ButtonPanel, false);
		GUIPopUp component = gameObject.GetComponent<GUIPopUp>();
		component.popup = pop;
		component.IsNew = false;
		component.MainImage.color = this.ImportanceGradient.Evaluate(pop.Importance);
		this.PopupButtons.Add(component);
		this.PopupIDDict[pop.Identifier] = component;
	}

	private void Update()
	{
		if (this.MainPanel.sizeDelta.y == 16f)
		{
			this.Scroll.value = 0f;
		}
		if (this.isDragging)
		{
			this.MainPanel.sizeDelta = new Vector2(this.MainPanel.sizeDelta.x, Mathf.Clamp(this.firstH - (Input.mousePosition.y - this.firstY), 16f, (float)(Screen.height - 262)));
			return;
		}
		float num = 16f;
		for (int i = this.PopupButtons.Count - 1; i > -1; i--)
		{
			if (this.PopupButtons[i].popup.Importance <= 1f && (this.PopupButtons[i].Made <= -1f || Time.realtimeSinceStartup - this.PopupButtons[i].Made >= 10f * (1f + this.PopupButtons[i].popup.Importance)))
			{
				break;
			}
			num += this.PopupButtons[i].panel.preferredHeight;
		}
		if (num < this.MainPanel.sizeDelta.y && RectTransformUtility.RectangleContainsScreenPoint(this.MainPanel, new Vector2(Input.mousePosition.x, Input.mousePosition.y + 8f)))
		{
			return;
		}
		float num2 = Mathf.Round(Mathf.Lerp(this.MainPanel.sizeDelta.y, num, Time.deltaTime * 16f));
		if (Mathf.Abs(num2 - num) < 2f)
		{
			num2 = num;
		}
		if (Mathf.Approximately(this.MainPanel.sizeDelta.y, 16f) && num > 16f)
		{
			UISoundFX.PlaySFX("PopupRolldown", -1f, 0f);
		}
		this.MainPanel.sizeDelta = new Vector2(this.MainPanel.sizeDelta.x, Mathf.Clamp(num2, 16f, (float)(Screen.height - 262)));
	}

	public void BeginDrag()
	{
		this.firstY = Mathf.Max((float)Screen.height - this.MainPanel.sizeDelta.y, Input.mousePosition.y);
		this.firstH = this.MainPanel.sizeDelta.y;
		this.PopupButtons.ForEach(delegate(GUIPopUp x)
		{
			x.Made = -1f;
		});
		this.isDragging = true;
	}

	public void EndDrag()
	{
		this.isDragging = false;
	}

	public GameObject ButtonPrefab;

	public Gradient ImportanceGradient;

	public RectTransform ButtonPanel;

	public RectTransform MainPanel;

	public Scrollbar Scroll;

	[NonSerialized]
	public List<GUIPopUp> PopupButtons = new List<GUIPopUp>();

	[NonSerialized]
	public Dictionary<int, GUIPopUp> PopupIDDict = new Dictionary<int, GUIPopUp>();

	public int MaxPops = 50;

	private float firstY;

	private float firstH;

	private bool isDragging;

	public enum PopUpAction
	{
		None = -1,
		GotoEmp,
		GotoRoom,
		GotoFurn,
		GotoHR,
		OpenInsurance,
		OpenProductDetails,
		OpenCompanyDetails
	}

	[Serializable]
	public class PopUp
	{
		public PopUp(string t, string c, PopupManager.PopUpAction action, uint actionTarget, SDateTime time, int id, Color color, int cooldown)
		{
			this.Icon = c;
			this.Text = t;
			this.Time = time;
			this.Identifier = id;
			this.textColor = color;
			this.Action = action;
			this.ActionTarget = actionTarget;
			this.CooldownDays = cooldown;
		}

		public PopUp()
		{
		}

		public readonly string Icon;

		public readonly string Text;

		public readonly PopupManager.PopUpAction Action = PopupManager.PopUpAction.None;

		public readonly SDateTime Time;

		public readonly int Identifier;

		public readonly uint ActionTarget;

		public readonly SVector3 textColor;

		public float Importance;

		public int CooldownDays = 6;
	}

	public enum PopupIDs
	{
		None = -1,
		Teamcompat,
		Dirt,
		Furniture,
		Computer,
		JobSatisfcation,
		EmployeeCountProblem,
		ServerProblems,
		Marketing,
		Bus,
		ElectronicHeat,
		BlockedOff,
		Support,
		TemperatureOverburdenend,
		EmployeeWorkAssignment,
		PCWarning,
		FanWarning,
		HRHireBudget,
		CookStove,
		LateProduct,
		CourierParking,
		ProductPrinterBackedUp,
		ProjectManagement,
		EmptyRoomGroup,
		SubsidiaryOverwork
	}

	public enum NotificationSound
	{
		Issue,
		Warning,
		Good,
		Neutral
	}
}
