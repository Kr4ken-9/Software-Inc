﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class ContractWork
{
	private ContractWork(string c, int m, int i, int d, int p, string s, float q, float pb, SDateTime a, string[] f, float art, float artUnits, float codeUnits, float diff)
	{
		this.Company = c;
		this.Months = m;
		this.Initial = i;
		this.Done = d;
		this.Penalty = p;
		this.SoftwareType = s;
		this.Quality = q;
		this.PerBug = pb;
		this.Added = a;
		this.Features = f;
		this.Art = art;
		this.ArtUnits = Mathf.Ceil(artUnits * 100f) / 100f;
		this.CodeUnits = Mathf.Ceil(codeUnits * 100f) / 100f;
		this.Difficulty = diff;
	}

	public ContractWork()
	{
	}

	public float GetDevTime()
	{
		return GameSettings.Instance.SoftwareTypes[this.SoftwareType].DevTime(this.Features, null, null);
	}

	public static ContractWork GenerateWork(float difficulty)
	{
		string c = GameSettings.Instance.RNG["ContractCompany"].GenerateName();
		string[] arr = (from x in GameSettings.Instance.SoftwareTypes.Values
		where x.OneClient && x.IsUnlocked(TimeOfDay.Instance.Year)
		select x.Name).ToArray<string>();
		string random = arr.GetRandom<string>();
		SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[random];
		string[] array = softwareType.GenerateFeatures(difficulty, "Default", new Dictionary<string, uint>(), null, SDateTime.Now(), false);
		float[] balance = softwareType.GetBalance(array);
		float num = Mathf.Min(0.95f, Utilities.RandomGaussClamped(difficulty, 0.1f));
		float pb = Utilities.RandomGaussClamped(balance[1], 0.2f) * 100f;
		float num2 = softwareType.DevTime(array, null, null);
		float num3 = Mathf.Max(1f, num2 * num);
		float num4 = 1f + Utilities.RandomGaussClamped(difficulty, 0.05f);
		int m = (int)num3;
		int i = (int)(1000f * Utilities.RandomGauss(1f, 0.1f) * num4);
		int d = (int)(num3 * 5000f * Utilities.RandomGauss(2f - balance[0], 0.1f) * num4);
		int p = (int)(num3 * 2000f * Utilities.RandomGauss(2f - balance[0], 0.1f) * num4);
		SDateTime a = SDateTime.Now();
		float num5 = 1f - softwareType.CodeArtRatio(array);
		int num6 = 0;
		float num7 = 0f;
		softwareType.CalculateLOCArt(array, out num6, out num7);
		float num8 = 0.65f + 0.3f * UnityEngine.Random.Range(difficulty / 2f, difficulty);
		return new ContractWork(c, m, i, d, p, random, num, pb, a, array, num5, num2 * num8 * num5, num2 * num8 * (1f - num5), difficulty);
	}

	public WorkItem GenerateWorkItem(string SCM)
	{
		return DesignDocument.CreateWork(this.Company, this.SoftwareType, "Default", new Dictionary<string, uint>(), null, 0f, SDateTime.Now(), GameSettings.Instance.MyCompany, null, false, 0f, this.Features, this, null, SCM, false);
	}

	public string GetStatus(float quality, SDateTime start, bool IncludeQuality = true)
	{
		string text = Utilities.Countdown(SDateTime.Now(), start + new SDateTime(this.Months, 0));
		if (IncludeQuality)
		{
			return text + "\n" + "ContractGoal".Loc(new object[]
			{
				Mathf.Min(100f, (this.Quality != 0f) ? (quality / this.Quality * 100f) : 100f).ToString("F")
			});
		}
		return text;
	}

	public override string ToString()
	{
		return this.Company;
	}

	public readonly string Company;

	public readonly int Months;

	public readonly int Initial;

	public readonly int Done;

	public readonly int Penalty;

	public readonly string SoftwareType;

	public readonly string[] Features;

	public readonly float Quality;

	public readonly float PerBug;

	public readonly float Art;

	public readonly float Difficulty;

	public readonly float ArtUnits;

	public readonly float CodeUnits;

	public readonly SDateTime Added;
}
