﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIRotationScript : MonoBehaviour
{
	private void Start()
	{
		this._scale = base.transform.localScale;
	}

	public void Stop()
	{
		this._stop = true;
		this.stopped = Time.realtimeSinceStartup;
	}

	private void Update()
	{
		if (this._stop)
		{
			this.Overlay.fillAmount = Mathf.Min(1f, (Time.realtimeSinceStartup - this.stopped) / 2.5f);
			base.transform.localScale = this._scale * this.ScaleCurve.Evaluate(Time.realtimeSinceStartup);
		}
		else
		{
			base.transform.rotation = base.transform.rotation * Quaternion.Euler(0f, 0f, Time.deltaTime * this.Speed);
		}
	}

	private bool _stop;

	public float Speed = -50f;

	public AnimationCurve ScaleCurve;

	public Image Overlay;

	private Vector3 _scale;

	private float stopped;
}
