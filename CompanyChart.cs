﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CompanyChart : MonoBehaviour
{
	private int Granularity
	{
		get
		{
			return CompanyChart.Granularities[Mathf.Max(0, this.GranularityCombo.Selected)];
		}
	}

	private void Start()
	{
		this.Window.OnSizeChanged = new Action(this.LineChart.UpdateCachedLines);
		this.Legend.OnToggle = new Action(this.UpdateChart);
		this.Legend.Colors = (this.LineChart.Colors = HUD.ThemeColors.ToList<Color>());
		this.Legend.Sheet = true;
		this.GranularityCombo.UpdateContent<string>(new string[]
		{
			"Monthly",
			"Quarterly",
			"Yearly"
		});
	}

	public void OnScroll(PointerEventData d)
	{
		this.Range[3] = this.Range[3] - (int)d.scrollDelta.y;
		this.UpdateScroll();
	}

	public void UpdateScroll()
	{
		if (!this.UpdatingScroll)
		{
			this.UpdatingScroll = true;
			this.Range[3] = Mathf.Min(this.Range[1], Mathf.Max(2, this.Range[3]));
			this.scrollbar.size = ((this.Range[1] != 0) ? ((float)this.Range[3] / (float)this.Range[1]) : 1f);
			float f = Mathf.Lerp((float)(this.Range[3] / 2), (float)(this.Range[1] - this.Range[3] / 2), this.scrollbar.value) - (float)(this.Range[3] / 2);
			this.Range[2] = Mathf.Min(this.Range[1] - this.Range[3], Mathf.RoundToInt(f));
			this.UpdateChart();
			this.UpdatingScroll = false;
		}
	}

	private void Update()
	{
		if (this.LastDate.Year != TimeOfDay.Instance.Year || this.LastDate.Month != TimeOfDay.Instance.Month)
		{
			this.LastDate = SDateTime.Now();
			this.UpdateChart();
		}
	}

	public void UpdateLabels(List<List<float>> values)
	{
		bool flag = values.Count > 0;
		float num;
		if (flag)
		{
			num = (from x in values
			select (x.Count <= 0) ? 0f : x.Min()).Min();
		}
		else
		{
			num = 0f;
		}
		float num2 = num;
		float num3;
		if (flag)
		{
			num3 = (from x in values
			select (x.Count <= 0) ? 0f : x.Max()).Max();
		}
		else
		{
			num3 = 0f;
		}
		float num4 = num3;
		float num5 = Mathf.Max(Mathf.Abs(num2), Mathf.Abs(num4));
		num5 = Mathf.Pow(10f, Mathf.Floor(Mathf.Log10(num5)));
		this.VertLabel.Label1.text = ((num5 != 0f) ? num4.CurrencyRoundUpToNearest(num5).Currency(true) : 0f.Currency(true));
		this.VertLabel.Label2.text = ((num5 != 0f) ? num2.CurrencyRoundDownToNearest(num5).Currency(true) : 0f.Currency(true));
		int granularity = this.Granularity;
		if (granularity == 12)
		{
			this.HorizLabel.Label1.text = (this.TCompany.Founded.RealYear + this.Range[2]).ToString();
			this.HorizLabel.Label2.text = (this.TCompany.Founded.RealYear + Mathf.Min(this.Range[1] - 1, this.Range[2] + this.Range[3] - 1)).ToString();
		}
		else if (granularity == 3)
		{
			SDateTime sdateTime = this.TCompany.Founded + new SDateTime(this.Range[2] * granularity, 0);
			SDateTime sdateTime2 = this.TCompany.Founded + new SDateTime(Mathf.Min(this.Range[1] - 1, this.Range[2] + this.Range[3] - 1) * granularity, 0);
			this.HorizLabel.Label1.text = sdateTime.ToQuarterString();
			this.HorizLabel.Label2.text = sdateTime2.ToQuarterString();
		}
		else
		{
			SDateTime sdateTime3 = this.TCompany.Founded + new SDateTime(this.Range[2], 0);
			SDateTime sdateTime4 = this.TCompany.Founded + new SDateTime(Mathf.Min(this.Range[1] - 1, this.Range[2] + this.Range[3] - 1), 0);
			this.HorizLabel.Label1.text = sdateTime3.ToCompactString();
			this.HorizLabel.Label2.text = sdateTime4.ToCompactString();
		}
	}

	public void UpdateChart()
	{
		if (this.TCompany == null)
		{
			return;
		}
		this.LineChart.Colors = (from x in this.Legend.GUIItems.Select((KeyValuePair<string, GameObject> x, int i) => new KeyValuePair<Toggle, int>(x.Value.GetComponent<Toggle>(), i))
		where x.Key.isOn
		select this.Legend.Colors[x.Value % this.Legend.Colors.Count]).ToList<Color>();
		List<KeyValuePair<string, List<float>>> list = (from x in this.TCompany.Cashflow
		where x.Value.Sum() != 0f
		select x).ToList<KeyValuePair<string, List<float>>>();
		if (list.Count > 0)
		{
			int granularity = this.Granularity;
			if (granularity != this._lastGranularity)
			{
				int num = this.TCompany.Founded.Month % granularity / this._lastGranularity;
				int num2 = this.TCompany.Founded.Month % this._lastGranularity / granularity;
				this.Range[1] = this.ConvertCount(list.Max((KeyValuePair<string, List<float>> x) => x.Value.Count));
				this.Range[3] = Mathf.Clamp(Mathf.CeilToInt((float)this.Range[3] * (float)this._lastGranularity / (float)granularity), Mathf.Min(2, this.Range[1]), this.Range[1]);
				this.Range[2] = Mathf.Clamp((this.Range[2] + num) * this._lastGranularity / granularity - num2, 0, this.Range[1] - this.Range[3]);
				this._lastGranularity = granularity;
				this.UpdatingScroll = true;
				this.scrollbar.size = ((this.Range[1] != 0) ? ((float)this.Range[3] / (float)this.Range[1]) : 1f);
				this.UpdatingScroll = false;
			}
			else if (this.ConvertCount(list.Max((KeyValuePair<string, List<float>> x) => x.Value.Count)) != this.Range[1])
			{
				if (this.Range[1] == this.Range[2] + this.Range[3])
				{
					this.Range[1] = this.ConvertCount(list.Max((KeyValuePair<string, List<float>> x) => x.Value.Count));
					if (this.Range[2] == 0)
					{
						this.Range[3] = this.Range[1];
					}
					else
					{
						this.Range[2] = this.Range[1] - this.Range[3];
					}
					this.UpdatingScroll = true;
					this.scrollbar.size = ((this.Range[1] != 0) ? ((float)this.Range[3] / (float)this.Range[1]) : 1f);
					this.UpdatingScroll = false;
				}
				else
				{
					this.Range[1] = this.ConvertCount(list.Max((KeyValuePair<string, List<float>> x) => x.Value.Count));
				}
				this.UpdateScroll();
			}
		}
		List<string> range = (from x in list
		select x.Key into x
		where !this.Legend.Items.Contains(x)
		select x).ToList<string>();
		this.Legend.Items.AddRange(range);
		List<List<float>> values = (from x in (from x in list
		orderby this.Legend.Items.IndexOf(x.Key)
		select x).Where((KeyValuePair<string, List<float>> x, int i) => this.Legend.IsOn(i))
		select this.NormalizeValues(x.Value, !x.Key.Equals("Balance"))).ToList<List<float>>();
		this.UpdateLabels(values);
		this.UpdateLineChart(values);
	}

	private int ConvertCount(int count)
	{
		return Mathf.CeilToInt((float)count / (float)this.Granularity);
	}

	private List<float> NormalizeValues(List<float> input, bool aggregate)
	{
		int num = this.Range[2];
		int num2 = this.Range[3];
		List<float> list = new List<float>(num2);
		int granularity = this.Granularity;
		if (granularity > 1)
		{
			int num3 = (num != 0) ? 0 : (this.TCompany.Founded.Month % granularity);
			for (int i = 0; i < num2; i++)
			{
				if (aggregate)
				{
					list.Add(0f);
					for (int j = 0; j < granularity - num3; j++)
					{
						int num4 = num + i * granularity + j;
						if (num4 >= input.Count)
						{
							break;
						}
						List<float> list2;
						int index;
						(list2 = list)[index = i] = list2[index] + input[num4];
					}
				}
				else
				{
					list.Add(input[Mathf.Min(input.Count - 1, num + i * granularity + granularity - 1 - num3)]);
				}
				num3 = 0;
			}
		}
		else
		{
			for (int k = 0; k < num2; k++)
			{
				list.Add(input[num + k]);
			}
		}
		return list;
	}

	private void UpdateLineChart(List<List<float>> values)
	{
		this.LineChart.Values.Clear();
		this.LineChart.Values.AddRange(values);
		this.LineChart.UpdateCachedLines();
	}

	public void SetCompany(Company company)
	{
		this.TCompany = company;
		this.LineChart.ToolTipFunc = delegate(int i, float x)
		{
			int granularity = this.Granularity;
			if (granularity == 12)
			{
				return (this.TCompany.Founded.RealYear + this.Range[2] + i).ToString() + ": " + x.Currency(true);
			}
			if (granularity == 3)
			{
				return (this.TCompany.Founded + new SDateTime((this.Range[2] + i) * granularity, 0)).ToQuarterString() + ": " + x.Currency(true);
			}
			return (this.TCompany.Founded + new SDateTime(this.Range[2] + i, 0)).ToVeryCompactString() + ": " + x.Currency(true);
		};
	}

	public void Show(Company company)
	{
		this.SetCompany(company);
		this._lastGranularity = 1;
		this.GranularityCombo.Selected = 0;
		this.Range[0] = 0;
		this.Range[1] = this.TCompany.Cashflow.Max((KeyValuePair<string, List<float>> x) => x.Value.Count);
		this.Range[3] = Mathf.Min(this.Range[1], 12);
		this.Range[2] = Mathf.Max(0, this.Range[1] - 12);
		this.scrollbar.value = 1f;
		this.UpdatingScroll = true;
		this.scrollbar.size = ((this.Range[1] != 0) ? ((float)this.Range[3] / (float)this.Range[1]) : 1f);
		this.UpdatingScroll = false;
		this.Legend.Items.Clear();
		this.UpdateChart();
		this.Window.NonLocTitle = company.Name + " " + "cashflow".Loc();
		this.Window.Show();
	}

	public GUIWindow Window;

	public GUILineChart LineChart;

	public GUILegend Legend;

	public SDateTime LastDate = new SDateTime(-1, -1);

	[NonSerialized]
	public Company TCompany;

	public Scrollbar scrollbar;

	public ChartLabel HorizLabel;

	public ChartLabel VertLabel;

	public GUICombobox GranularityCombo;

	private bool UpdatingScroll;

	private int[] Range = new int[4];

	private int _lastGranularity = 1;

	public static readonly int[] Granularities = new int[]
	{
		1,
		3,
		12
	};
}
