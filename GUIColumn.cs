﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GUIColumn : MonoBehaviour
{
	public void UpdateElements()
	{
		if (this.Parent == null)
		{
			return;
		}
		int num = Mathf.Min(this.Parent.ActualItems.Count, Mathf.FloorToInt(base.transform.parent.GetComponent<RectTransform>().rect.height / 24f));
		if (this.Items.Count < num)
		{
			int num2 = num - this.Items.Count;
			for (int i = 0; i < num2; i++)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ItemPrefab[(int)this.Type]);
				GUIListItem component = gameObject.GetComponent<GUIListItem>();
				component.Parent = this;
				component.Idx = this.Items.Count;
				gameObject.transform.SetParent(this.ContentPanel.transform, false);
				this.Items.Add(component);
				if (this.Type == GUIColumn.ColumnType.Action)
				{
					component.SetValue((!this.Parent.IgnoreTranslation) ? this.HeaderValue.Loc() : this.HeaderValue.LocDef(this.HeaderValue));
				}
			}
		}
		for (int j = 0; j < this.Items.Count; j++)
		{
			int num3 = j + this.Parent.Scroll;
			if (num3 > -1 && num3 < this.Parent.ActualItems.Count)
			{
				if (this.Type != GUIColumn.ColumnType.Action)
				{
					this.Items[j].SetValue(this.Label(this.Parent.ActualItems[num3]));
				}
				if (!this.Items[j].gameObject.activeSelf)
				{
					this.Items[j].gameObject.SetActive(true);
				}
			}
			else if (this.Items[j].gameObject.activeSelf)
			{
				this.Items[j].gameObject.SetActive(false);
			}
		}
	}

	public void UpdateSelected()
	{
		GUIColumn.ColumnType type = this.Type;
		if (type == GUIColumn.ColumnType.Stars || type == GUIColumn.ColumnType.Role || type == GUIColumn.ColumnType.Label)
		{
			for (int i = 0; i < this.Items.Count; i++)
			{
				this.Items[i].Highlight(this.Parent.Selected.Contains(i + this.Parent.Scroll));
			}
		}
	}

	public void HeaderPressed()
	{
		switch (this.Type)
		{
		case GUIColumn.ColumnType.Label:
		case GUIColumn.ColumnType.Slider:
		case GUIColumn.ColumnType.Stars:
		case GUIColumn.ColumnType.Role:
			this.Parent.LastSort = this;
			this.Sort(this.SortAsc, true);
			break;
		}
	}

	public void Sort(bool asc, bool update = true)
	{
		List<object> source = (from x in this.Parent.Selected
		where x < this.Parent.ActualItems.Count
		select x into i
		select this.Parent.ActualItems[i]).ToList<object>();
		this.Parent.ResetHeaders();
		this.Header.text = this.HeaderValue.Loc() + ((!asc) ? "▼" : "▲");
		List<KeyValuePair<int, object>> list = this.Parent.ActualItems.Select((object x, int i) => new KeyValuePair<int, object>(i, x)).ToList<KeyValuePair<int, object>>();
		list.Sort(delegate(KeyValuePair<int, object> x, KeyValuePair<int, object> y)
		{
			int num = (!asc) ? this.Comparison(y.Value, x.Value) : this.Comparison(x.Value, y.Value);
			if (num == 0)
			{
				return x.Key.CompareTo(y.Key);
			}
			return num;
		});
		for (int j = 0; j < list.Count; j++)
		{
			this.Parent.ActualItems[j] = list[j].Value;
		}
		this.SortAsc = !asc;
		this.Parent.Selected.Clear();
		this.Parent.Selected.AddRange(from x in source
		select this.Parent.ActualItems.IndexOf(x) into x
		where x > -1
		select x);
		if (update)
		{
			this.Parent.UpdateElements();
		}
	}

	public void OnClickAction(int i)
	{
		switch (this.Type)
		{
		case GUIColumn.ColumnType.Label:
		case GUIColumn.ColumnType.Stars:
		case GUIColumn.ColumnType.Role:
			this.Parent.LastSelectDirect = true;
			this.Parent.Select(this.Parent.Scroll + i);
			break;
		case GUIColumn.ColumnType.Action:
		{
			int num = this.Parent.Scroll + i;
			if (num < this.Parent.ActualItems.Count)
			{
				this.Action(this.Parent.ActualItems[num]);
			}
			break;
		}
		}
	}

	public void BeginDrag()
	{
		if (Time.realtimeSinceStartup - this.lastHeaderClick < 0.5f)
		{
			if (this.Items.Any<GUIListItem>())
			{
				float preferredWidth = this.Items.Max((GUIListItem x) => x.PreferredWidth) + 12f;
				this.layoutElement.preferredWidth = preferredWidth;
			}
		}
		else
		{
			this.initWidth = this.rectTransform.rect.width;
			this.mouseX = Input.mousePosition.x / Options.UISize;
			this.isDragging = true;
		}
		this.lastHeaderClick = Time.realtimeSinceStartup;
	}

	private void Awake()
	{
		this.rectTransform = base.GetComponent<RectTransform>();
		this.layoutElement = base.GetComponent<LayoutElement>();
	}

	private void OnEnable()
	{
		this.UpdateElements();
	}

	private void Update()
	{
		if (this.Type != GUIColumn.ColumnType.Action && this.ContinuallyUpdate)
		{
			int num = Mathf.Min(this.Items.Count, this.Parent.ActualItems.Count - this.Parent.Scroll);
			for (int i = 0; i < num; i++)
			{
				this.Items[i].SetValue(this.Label(this.Parent.ActualItems[i + this.Parent.Scroll]));
			}
		}
		if (this.isDragging)
		{
			this.layoutElement.preferredWidth = this.initWidth + Input.mousePosition.x / Options.UISize - this.mouseX;
			if (Input.GetMouseButtonUp(0))
			{
				this.isDragging = false;
				Options.AddColumnWidth(base.name, this.layoutElement.preferredWidth);
			}
		}
	}

	private void ActivateFilter()
	{
		this.FilterActive = true;
		this.FilterIcon.color = new Color32(62, 212, 82, byte.MaxValue);
		this.Parent.UpdateActiveList(true);
		this.PanelImage.color = this.FilteredPanelColor;
	}

	public void ToggleFilter()
	{
		this.FilterIcon.color = Color.black;
		this.PanelImage.color = this.DefaultPanelColor;
		if (!this.FilterActive)
		{
			switch (this.Filter)
			{
			case GUIListView.FilterType.Number:
			{
				List<float> list = (from x in this.Parent.ActualItems
				select (float)((this.GetFilterValue != null) ? this.GetFilterValue(x) : x)).ToList<float>();
				float from = list.MinSafe((float x) => x, float.MaxValue, 0f);
				float to = list.MaxSafe((float x) => x, float.MinValue, 0f);
				HUD.Instance.numberRangeWindow.Show(from, to, delegate(float f, float t)
				{
					this.FilterNumber[0] = f;
					this.FilterNumber[1] = t;
					this.ActivateFilter();
				});
				break;
			}
			case GUIListView.FilterType.Name:
			{
				Dictionary<string, uint> dictionary = new Dictionary<string, uint>();
				foreach (object arg in this.Parent.ActualItems)
				{
					string key = (string)((this.GetFilterValue != null) ? this.GetFilterValue(arg) : this.Label(arg));
					dictionary.AddUp(key, 1u);
				}
				List<KeyValuePair<string, uint>> names = (from x in dictionary
				orderby x.Key
				select x).ToList<KeyValuePair<string, uint>>();
				if (names.Count > 1)
				{
					SelectorController.Instance.selectWindow.ShowMulti(string.Empty, from x in names
					select string.Concat(new object[]
					{
						x.Key,
						"(",
						x.Value,
						")"
					}), null, delegate(int[] sel)
					{
						if (sel.Length > 0)
						{
							this.FilterName = sel.Select((int x) => names[x].Key).ToHashSet<string>();
							this.ActivateFilter();
						}
					}, true, false, false);
				}
				break;
			}
			case GUIListView.FilterType.Date:
			{
				List<int> list2 = (from x in this.Parent.ActualItems
				select ((SDateTime)((this.GetFilterValue != null) ? this.GetFilterValue(x) : x)).ToInt()).ToList<int>();
				int d = list2.MinSafeInt((int x) => x, int.MaxValue, 0);
				int d2 = list2.MaxSafeInt((int x) => x, int.MinValue, 0);
				HUD.Instance.dateRangeWindow.Show(SDateTime.FromInt(d), SDateTime.FromInt(d2), delegate(SDateTime f, SDateTime t)
				{
					this.FilterNumber[0] = (float)f.ToInt();
					this.FilterNumber[1] = (float)t.ToInt() + 1439f;
					this.ActivateFilter();
				});
				break;
			}
			case GUIListView.FilterType.Bool:
				SelectorController.Instance.selectWindow.Show(string.Empty, new string[]
				{
					"Yes".Loc(),
					"No".Loc()
				}, delegate(int sel)
				{
					this.FilterBool = (sel == 0);
					this.ActivateFilter();
				}, false, true, true, false, null);
				break;
			case GUIListView.FilterType.Bitmask:
			{
				object obj = this.Parent.ActualItems.FirstOrDefault<object>();
				if (obj != null)
				{
					Type type = this.GetFilterValue(obj).GetType();
					string[] values = (from object x in Enum.GetValues(type)
					where (int)x > 0 && Mathf.IsPowerOfTwo((int)x)
					orderby (int)x
					select x.ToString().Loc()).ToArray<string>();
					SelectorController.Instance.selectWindow.ShowMulti(string.Empty, values, null, delegate(int[] sel)
					{
						if (sel.Length > 0)
						{
							int num = 0;
							foreach (int num2 in sel)
							{
								num |= Mathf.RoundToInt(Mathf.Pow(2f, (float)num2));
							}
							this.FilterMask = num;
							this.ActivateFilter();
						}
					}, true, false, false);
				}
				break;
			}
			}
		}
		else
		{
			this.FilterActive = false;
			this.Parent.UpdateActiveList(true);
		}
	}

	public void SetValue(int i, object value)
	{
		this.SetVariable(this.Parent.ActualItems[this.Parent.Scroll + i], value);
	}

	public GUIColumn.ColumnType Type;

	public GameObject[] ItemPrefab;

	public GUIListView Parent;

	public GameObject ContentPanel;

	public Action<object> Action;

	public Action<object, object> SetVariable;

	public Func<object, object> Label;

	public Comparison<object> Comparison;

	public Func<object, object> GetFilterValue;

	public GUIListView.FilterType Filter;

	public bool FilterActive;

	public float[] FilterNumber = new float[2];

	public HashSet<string> FilterName = new HashSet<string>();

	public int FilterMask = int.MaxValue;

	public bool FilterBool;

	public List<GUIListItem> Items = new List<GUIListItem>();

	public Text Header;

	public string HeaderValue;

	public bool SortAsc;

	public bool ContinuallyUpdate;

	public float mouseX;

	public float initWidth;

	public RectTransform rectTransform;

	public LayoutElement layoutElement;

	private bool isDragging;

	private float lastHeaderClick;

	public Image FilterIcon;

	public Image PanelImage;

	public Color DefaultPanelColor;

	public Color FilteredPanelColor;

	public enum ColumnType
	{
		Label,
		Action,
		Slider,
		Stars,
		Role
	}
}
