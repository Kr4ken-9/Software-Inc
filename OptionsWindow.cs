﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsWindow : MonoBehaviour
{
	private void Awake()
	{
		OptionsWindow.Instance = this;
	}

	private void Start()
	{
		this.InitKeys();
		this.InitTutorials();
		this.InitMods();
		this.InitAudio();
	}

	public void ResetKeys()
	{
		InputController.Reset();
		this.InputButtons.ForEach(delegate(InputButton x)
		{
			x.UpdateText();
		});
		Options.SaveToFile();
	}

	private void InitDynamic()
	{
		this.InitGameplay();
		this.InitGFX();
	}

	public void ResetHints()
	{
		Options.ResetHints();
	}

	public void ResetWindowPos()
	{
		Options.ResetSizes();
	}

	public void ResetColumns()
	{
		Options.ResetWidths();
	}

	private void InitGameplay()
	{
		this.AutoSkip.onValueChanged.RemoveAllListeners();
		this.AutoSkip.isOn = Options.AutoSkip;
		this.AutoSkip.onValueChanged.AddListener(delegate(bool x)
		{
			Options.AutoSkip = x;
		});
		this.HintToggle.onValueChanged.RemoveAllListeners();
		this.HintToggle.isOn = Options.HintsEnabled;
		this.HintToggle.onValueChanged.AddListener(delegate(bool x)
		{
			Options.HintsEnabled = x;
		});
		this.CurrencyShortToggle.onValueChanged.RemoveAllListeners();
		this.CurrencyShortToggle.isOn = Options.CurrencyShortForm;
		this.CurrencyShortToggle.onValueChanged.AddListener(delegate(bool x)
		{
			Options.CurrencyShortForm = x;
		});
		this.AskMarketing.onValueChanged.RemoveAllListeners();
		this.AskMarketing.isOn = Options.AskMarketing;
		this.AskMarketing.onValueChanged.AddListener(delegate(bool x)
		{
			Options.AskMarketing = x;
		});
		this.AskPrinting.onValueChanged.RemoveAllListeners();
		this.AskPrinting.isOn = Options.AskPrinting;
		this.AskPrinting.onValueChanged.AddListener(delegate(bool x)
		{
			Options.AskPrinting = x;
		});
		this.Currency.OnSelectedChangedRemoveAllListeners();
		this.Currency.UpdateContent<object>((from x in GameData.CurrencyRates
		select x.Key).ToList<object>());
		this.Currency.UpdateSelection(this.Currency.Items.IndexOf(Options.Currency));
		this.Currency.OnSelectedChanged.AddListener(delegate
		{
			Options.Currency = this.Currency.SelectedItem.ToString();
		});
		this.Background.onValueChanged.RemoveAllListeners();
		this.Background.isOn = Options.RunInBackground;
		this.Background.onValueChanged.AddListener(delegate(bool x)
		{
			Options.RunInBackground = x;
		});
		this.AutoSave.onValueChanged.RemoveAllListeners();
		this.AutoSave.isOn = Options.AutoSave;
		this.AutoSave.onValueChanged.AddListener(delegate(bool x)
		{
			Options.AutoSave = x;
		});
		this.Backup.onValueChanged.RemoveAllListeners();
		this.Backup.isOn = Options.Backup;
		this.Backup.onValueChanged.AddListener(delegate(bool x)
		{
			Options.Backup = x;
		});
		this.Tutorials.onValueChanged.RemoveAllListeners();
		this.Tutorials.isOn = Options.Tutorial;
		this.Tutorials.onValueChanged.AddListener(delegate(bool x)
		{
			Options.Tutorial = x;
		});
		this.EdgeScrolling.onValueChanged.RemoveAllListeners();
		this.EdgeScrolling.isOn = Options.EdgeScroll;
		this.EdgeScrolling.onValueChanged.AddListener(delegate(bool x)
		{
			Options.EdgeScroll = x;
		});
		this.AMPM.onValueChanged.RemoveAllListeners();
		this.AMPM.isOn = Options.AMPM;
		this.AMPM.onValueChanged.AddListener(delegate(bool x)
		{
			Options.AMPM = x;
		});
		this.Celsius.onValueChanged.RemoveAllListeners();
		this.Celsius.isOn = Options.Celsius;
		this.Celsius.onValueChanged.AddListener(delegate(bool x)
		{
			Options.Celsius = x;
		});
		this.ScrollSlider.onValueChanged.RemoveAllListeners();
		this.ScrollSlider.value = Options.ScrollSpeed;
		this.ScrollSlider.onValueChanged.AddListener(delegate(float x)
		{
			Options.ScrollSpeed = x;
		});
		this.ZoomSlider.onValueChanged.RemoveAllListeners();
		this.ZoomSlider.value = Options.ZoomSpeed;
		this.ZoomSlider.onValueChanged.AddListener(delegate(float x)
		{
			Options.ZoomSpeed = x;
		});
		this.RotationSlider.onValueChanged.RemoveAllListeners();
		this.RotationSlider.value = Options.RotationSpeed;
		this.RotationSlider.onValueChanged.AddListener(delegate(float x)
		{
			Options.RotationSpeed = x;
		});
		if (GameSettings.Instance != null)
		{
			this.Difficulty.OnSelectedChangedRemoveAllListeners();
			this.Difficulty.UpdateContent<string>(GameData.DifficultySettings);
			this.Difficulty.Selected = GameSettings.Instance.Difficulty;
			this.Difficulty.OnSelectedChanged.AddListener(delegate
			{
				GameSettings.Instance.Difficulty = this.Difficulty.Selected;
			});
		}
		else
		{
			this.Difficulty.gameObject.SetActive(false);
			this.DifficultyText.SetActive(false);
		}
	}

	private void RefreshGfxCombo()
	{
		this.DefaultGFXCombo.OnSelectedChangedRemoveAllListeners();
		this.DefaultGFXCombo.Selected = AutoGfxSettings.GetSetting() + 1;
		this.DefaultGFXCombo.OnSelectedChanged.AddListener(delegate
		{
			if (this.DefaultGFXCombo.Selected > 0)
			{
				AutoGfxSettings.SetLevel(AutoGfxSettings.DefaultSettings[this.DefaultGFXCombo.Selected - 1]);
				this.InitGFX();
				this.RefreshGfxCombo();
			}
		});
	}

	private void InitGFX()
	{
		this.DefaultGFXCombo.OnSelectedChanged = null;
		this.DefaultGFXCombo.UpdateContent<string>(new string[]
		{
			"GFXCustom",
			"GFXNothing",
			"GFXLow",
			"GFXMedium",
			"GFXHigh",
			"GFXUltra"
		});
		List<Resolution> res = (from x in Screen.resolutions
		where x.height >= 720 && x.width >= 1024
		select x).ToList<Resolution>();
		this.Resolution.OnSelectedChangedRemoveAllListeners();
		this.Resolution.UpdateContent<object>((from x in res
		select string.Concat(new object[]
		{
			x.width,
			"x",
			x.height,
			" ",
			x.refreshRate,
			"Hz"
		})).ToList<object>());
		Resolution item = Options.FindRes(Options.TResolution.width, Options.TResolution.height, Options.TResolution.refreshRate);
		int num = res.IndexOf(item);
		if (num >= 0)
		{
			this.Resolution.UpdateSelection(num);
		}
		this.Resolution.OnSelectedChanged.AddListener(delegate
		{
			Options.TResolution = res[this.Resolution.Selected];
		});
		this.Fullscreen.onValueChanged.RemoveAllListeners();
		this.Fullscreen.isOn = Options.Fullscreen;
		this.Fullscreen.onValueChanged.AddListener(delegate(bool x)
		{
			Options.Fullscreen = x;
		});
		this.FXAA.onValueChanged.RemoveAllListeners();
		this.FXAA.isOn = Options.FXAA;
		this.FXAA.onValueChanged.AddListener(delegate(bool x)
		{
			Options.FXAA = x;
		});
		this.TargetFramerate.onEndEdit.RemoveAllListeners();
		this.TargetFramerate.text = ((Options.VSync != 0) ? "VSync".Loc() : Options.TargetFrameRate.ToString());
		this.TargetFramerate.interactable = (Options.VSync == 0);
		this.TargetFramerate.onEndEdit.AddListener(delegate(string x)
		{
			if (Options.VSync > 0)
			{
				return;
			}
			bool flag = false;
			try
			{
				int num2 = Convert.ToInt32(x);
				if (num2 == -1 || num2 > 0)
				{
					Options.TargetFrameRate = num2;
				}
				else
				{
					flag = true;
				}
			}
			catch (Exception)
			{
				flag = true;
			}
			if (flag)
			{
				WindowManager.Instance.ShowMessageBox("InvalidAmount".Loc(), false, DialogWindow.DialogType.Error);
				this.TargetFramerate.text = Options.TargetFrameRate.ToString();
			}
		});
		if (!Options.CheckSMAASupport())
		{
			this.SMAA.interactable = false;
			this.SMAATipper.TooltipDescription = "SMAADetailsError";
			this.SMAA.isOn = false;
		}
		else
		{
			this.SMAA.onValueChanged.RemoveAllListeners();
			this.SMAA.isOn = Options.SMAA;
			this.SMAA.onValueChanged.AddListener(delegate(bool x)
			{
				Options.SMAA = x;
			});
		}
		this.SSR.onValueChanged.RemoveAllListeners();
		this.SSR.isOn = Options.SSR;
		this.SSR.onValueChanged.AddListener(delegate(bool x)
		{
			Options.SSR = x;
		});
		this.VSync.OnSelectedChangedRemoveAllListeners();
		this.VSync.UpdateContent<object>(new List<object>
		{
			"Off",
			"1 frame",
			"2 frames"
		});
		this.VSync.UpdateSelection(Options.VSync);
		this.VSync.OnSelectedChanged.AddListener(delegate
		{
			if (this.VSync.Selected == 0)
			{
				this.TargetFramerate.interactable = true;
				this.TargetFramerate.text = Options.TargetFrameRate.ToString();
				Options.VSync = this.VSync.Selected;
			}
			else
			{
				Options.VSync = this.VSync.Selected;
				this.TargetFramerate.text = "VSync".Loc();
				this.TargetFramerate.interactable = false;
			}
		});
		this.SSAA.onValueChanged.RemoveAllListeners();
		this.SSAA.value = (float)Options.SSAA;
		this.SSAA.onValueChanged.AddListener(delegate(float x)
		{
			Options.SSAA = (int)x;
			if (Options.SSAA == 10)
			{
				this.SSAAText.text = "Off".Loc();
			}
			else
			{
				string text2 = Options.SSAA.ToString();
				this.SSAAText.text = string.Concat(new object[]
				{
					text2[0],
					".",
					text2[1],
					"x"
				});
			}
		});
		if (Options.SSAA == 10)
		{
			this.SSAAText.text = "Off".Loc();
		}
		else
		{
			string text = Options.SSAA.ToString();
			this.SSAAText.text = string.Concat(new object[]
			{
				text[0],
				".",
				text[1],
				"x"
			});
		}
		this.Shadows.onValueChanged.RemoveAllListeners();
		this.Shadows.isOn = Options.Shadows;
		this.Shadows.onValueChanged.AddListener(delegate(bool x)
		{
			Options.Shadows = x;
		});
		this.Frosted.onValueChanged.RemoveAllListeners();
		this.Frosted.isOn = Options.Frosted;
		this.Frosted.onValueChanged.AddListener(delegate(bool x)
		{
			Options.Frosted = x;
		});
		this.ShadowQuality.onValueChanged.RemoveAllListeners();
		this.ShadowQuality.value = (float)Options.ShadowQuality;
		this.ShadowQuality.onValueChanged.AddListener(delegate(float x)
		{
			Options.ShadowQuality = (int)x;
		});
		this.GammaSlider.onValueChanged.RemoveAllListeners();
		this.GammaSlider.value = Options.Gamma;
		this.GammaSlider.onValueChanged.AddListener(delegate(float x)
		{
			if (!Mathf.Approximately(x, 0.5f) && Mathf.Abs(x - 0.5f) < 0.02f)
			{
				this.GammaSlider.value = 0.5f;
				Options.Gamma = 0.5f;
			}
			else
			{
				Options.Gamma = x;
			}
		});
		this.MoreShadows.onValueChanged.RemoveAllListeners();
		this.MoreShadows.isOn = Options.MoreShadow;
		this.MoreShadows.onValueChanged.AddListener(delegate(bool x)
		{
			Options.MoreShadow = x;
		});
		this.OpaqueGlass.onValueChanged.RemoveAllListeners();
		this.OpaqueGlass.isOn = Options.OpaqueGlass;
		this.OpaqueGlass.onValueChanged.AddListener(delegate(bool x)
		{
			Options.OpaqueGlass = x;
		});
		this.AmbientOcclusion.onValueChanged.RemoveAllListeners();
		this.AmbientOcclusion.isOn = Options.AmbientOcclusion;
		this.AmbientOcclusion.onValueChanged.AddListener(delegate(bool x)
		{
			Options.AmbientOcclusion = x;
		});
		this.TiltShift.onValueChanged.RemoveAllListeners();
		this.TiltShift.isOn = Options.TiltShift;
		this.TiltShift.onValueChanged.AddListener(delegate(bool x)
		{
			Options.TiltShift = x;
		});
		this.Bloom.onValueChanged.RemoveAllListeners();
		this.Bloom.isOn = Options.Bloom;
		this.Bloom.onValueChanged.AddListener(delegate(bool x)
		{
			Options.Bloom = x;
		});
	}

	public void IncreaseUISize()
	{
		if (Options.UISize < 2f)
		{
			Options.UISize += 0.1f;
		}
	}

	public void DecreaseUISize()
	{
		if (Options.UISize > 1f)
		{
			Options.UISize -= 0.1f;
		}
	}

	private void InitAudio()
	{
		using (Dictionary<string, AudioMixerGroup>.Enumerator enumerator = AudioManager.MixerMap.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				KeyValuePair<string, AudioMixerGroup> map = enumerator.Current;
				OptionsWindow $this = this;
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.LabelPrefab);
				Text component = gameObject.GetComponent<Text>();
				component.text = map.Key.Loc();
				component.alignment = TextAnchor.MiddleLeft;
				gameObject.transform.SetParent(this.AudioPanel.transform, false);
				gameObject = UnityEngine.Object.Instantiate<GameObject>(this.SliderPrefab);
				Slider component2 = gameObject.GetComponent<Slider>();
				component2.maxValue = 0f;
				component2.minValue = -1f;
				component2.onValueChanged.RemoveAllListeners();
				component2.value = this.MapVolume(AudioManager.GetVolume(map.Key), true);
				component2.onValueChanged.AddListener(delegate(float x)
				{
					AudioManager.SetVolume(map.Key, $this.MapVolume(x, false));
				});
				gameObject.transform.SetParent(this.AudioPanel.transform, false);
			}
		}
	}

	private float MapVolume(float input, bool reverse)
	{
		if (reverse)
		{
			return -Mathf.Sqrt(-input / 80f);
		}
		return -Mathf.Pow(input, 2f) * 80f;
	}

	private void InitKeys()
	{
		foreach (InputController.Keys keys in (from x in typeof(InputController.Keys).GetFields(BindingFlags.Static | BindingFlags.Public)
		select Enum.Parse(typeof(InputController.Keys), x.Name)).OfType<InputController.Keys>().ToArray<InputController.Keys>())
		{
			int num = (int)keys;
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.LabelPrefab);
			gameObject.GetComponent<Text>().text = InputController.KeyNames[num].Loc();
			gameObject.transform.SetParent(this.KeyPanel.transform, false);
			GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.InputButtonPrefab);
			InputButton component = gameObject2.GetComponent<InputButton>();
			component.Key = keys;
			this.InputButtons.Add(component);
			gameObject2.transform.SetParent(this.KeyPanel.transform, false);
			gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.InputButtonPrefab);
			component = gameObject2.GetComponent<InputButton>();
			component.Key = keys;
			component.Alt = true;
			this.InputButtons.Add(component);
			gameObject2.transform.SetParent(this.KeyPanel.transform, false);
		}
	}

	private void InitTutorials()
	{
		if (this.EnableTutorials)
		{
			foreach (string text in TutorialSystem.Tutorials.Keys)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
				gameObject.GetComponentInChildren<Text>().text = text.Loc();
				string ttut = text;
				gameObject.GetComponent<Button>().onClick.AddListener(delegate
				{
					this.Window.Close();
					HUD.Instance.pauseWindow.ToggleShow();
					TutorialSystem.Instance.StartTutorial(ttut, true);
				});
				gameObject.transform.SetParent(this.TutorialPanel.transform, false);
			}
		}
		else
		{
			this.TutorialButton.SetActive(false);
		}
	}

	private void InitMods()
	{
		foreach (ModController.DLLMod dllmod in ModController.Instance.Mods)
		{
			GameObject content = UnityEngine.Object.Instantiate<GameObject>(this.ModContentPrefab);
			RectTransform component = content.GetComponentInChildren<ContentSizeFitter>().GetComponent<RectTransform>();
			try
			{
				dllmod.Meta.ConstructOptionsScreen(component, dllmod.Behaviors);
			}
			catch (Exception exception)
			{
				bool modded = ErrorLogging.Modded;
				ErrorLogging.Modded = true;
				Debug.LogException(exception);
				ErrorLogging.Modded = modded;
				UnityEngine.Object.Destroy(content);
				continue;
			}
			float num = component.sizeDelta.x;
			float num2 = component.sizeDelta.y;
			for (int i = 0; i < component.childCount; i++)
			{
				Bounds bounds = RectTransformUtility.CalculateRelativeRectTransformBounds(component, component.GetChild(i));
				num = Mathf.Max(num, bounds.max.x);
				num2 = Mathf.Max(num2, -bounds.min.y);
			}
			component.sizeDelta = new Vector2(num + 20f, num2 + 20f);
			content.SetActive(false);
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ModHeaderPrefab);
			gameObject.GetComponentInChildren<Text>().text = dllmod.Meta.Name;
			Toggle componentInChildren = gameObject.GetComponentInChildren<Toggle>();
			componentInChildren.isOn = dllmod.Active;
			ModController.DLLMod mod1 = dllmod;
			componentInChildren.onValueChanged.AddListener(delegate(bool x)
			{
				try
				{
					mod1.Activate(x);
				}
				catch (Exception ex)
				{
					Debug.Log("Error activating/deactivating dll mod " + mod1.FileName + ":\n" + ex.ToString());
					WindowManager.SpawnDialog("ModActivateError".Loc(), true, DialogWindow.DialogType.Error);
				}
			});
			gameObject.GetComponentInChildren<Button>().onClick.AddListener(delegate
			{
				content.SetActive(!content.activeSelf);
			});
			gameObject.transform.SetParent(this.ModPanel, false);
			content.transform.SetParent(this.ModPanel, false);
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("<size=16>" + "Software".Loc() + ":</size>");
		if (GameData.ModPackages.Count == 0)
		{
			stringBuilder.AppendLine(" •" + "None".Loc());
		}
		else
		{
			foreach (ModPackage modPackage in GameData.ModPackages)
			{
				stringBuilder.AppendLine(" •" + modPackage.ItemTitle);
			}
		}
		stringBuilder.AppendLine();
		stringBuilder.AppendLine("<size=16>" + "Furniture".Loc() + ":</size>");
		if (FurnitureLoader.LoadedFurniture.Count == 0)
		{
			stringBuilder.AppendLine(" •" + "None".Loc());
		}
		else
		{
			foreach (FurnitureMod furnitureMod in FurnitureLoader.LoadedFurniture)
			{
				stringBuilder.AppendLine(" •" + furnitureMod.ItemTitle);
			}
		}
		stringBuilder.AppendLine();
		stringBuilder.AppendLine("<size=16>" + "Blueprints".Loc() + ":</size>");
		if (GameData.Prefabs.Count == 0)
		{
			stringBuilder.AppendLine(" •" + "None".Loc());
		}
		else
		{
			foreach (BuildingPrefab buildingPrefab in GameData.Prefabs)
			{
				stringBuilder.AppendLine(" •" + buildingPrefab.ItemTitle);
			}
		}
		stringBuilder.AppendLine();
		stringBuilder.AppendLine("<size=16>" + "Error".Loc() + ":</size>");
		if (GameData.FailedModList.Count == 0)
		{
			stringBuilder.AppendLine(" •" + "None".Loc());
		}
		else
		{
			foreach (string str in GameData.FailedModList)
			{
				stringBuilder.AppendLine(" •" + str);
			}
		}
		this.ModContentText.text = stringBuilder.ToString();
	}

	public void ActivatePanel(int j)
	{
		for (int i = 0; i < this.Panels.Length; i++)
		{
			this.Panels[i].SetActive(i == j);
		}
	}

	public void Show()
	{
		if (GameSettings.Instance != null)
		{
			this.Difficulty.Selected = GameSettings.Instance.Difficulty;
		}
		this.InitDynamic();
		this.RefreshGfxCombo();
		this.Window.Show();
	}

	public void AutoGraphics()
	{
		if (GameSettings.Instance != null)
		{
			WindowManager.Instance.ShowMessageBox("GfxTestWarning".Loc(), false, DialogWindow.DialogType.Warning, delegate
			{
				GameSettings.UnloadNow();
				ErrorLogging.FirstOfScene = true;
				ErrorLogging.SceneChanging = true;
				SceneManager.LoadScene("AutoGraphicsSettings");
			}, null, null);
		}
		else
		{
			DialogWindow diag = WindowManager.SpawnDialog();
			DialogWindow diag2 = diag;
			string msg = "GfxTestResWarning".Loc();
			bool draw = false;
			DialogWindow.DialogType type = DialogWindow.DialogType.Information;
			KeyValuePair<string, Action>[] array = new KeyValuePair<string, Action>[2];
			array[0] = new KeyValuePair<string, Action>("Yes", delegate
			{
				ErrorLogging.SceneChanging = true;
				SceneManager.LoadScene("AutoGraphicsSettings");
			});
			array[1] = new KeyValuePair<string, Action>("No", delegate
			{
				diag.Window.Close();
			});
			diag2.Show(msg, draw, type, array);
		}
	}

	public void ResetConfirmations()
	{
		Options.IgnoreQuestions.Clear();
		Options.SaveToFile();
	}

	public GUIWindow Window;

	public GameObject LabelPrefab;

	public GameObject InputButtonPrefab;

	public GameObject ButtonPrefab;

	public GameObject SliderPrefab;

	public GameObject[] Panels;

	public List<InputButton> InputButtons;

	public Toggle AutoSkip;

	public Toggle Background;

	public Toggle AutoSave;

	public Toggle Backup;

	public Toggle Tutorials;

	public Toggle EdgeScrolling;

	public Toggle AMPM;

	public Toggle Celsius;

	public Toggle Fullscreen;

	public Toggle Shadows;

	public Toggle AmbientOcclusion;

	public Toggle TiltShift;

	public Toggle Bloom;

	public Toggle MoreShadows;

	public Toggle AskMarketing;

	public Toggle AskPrinting;

	public Toggle SSR;

	public Toggle FXAA;

	public Toggle SMAA;

	public Toggle HintToggle;

	public Toggle CurrencyShortToggle;

	public Toggle Frosted;

	public Toggle OpaqueGlass;

	public Text AutoSaveT;

	public Text SSAAText;

	public Text ModContentText;

	public InputField TargetFramerate;

	public GUICombobox Currency;

	public GUICombobox Resolution;

	public GUICombobox VSync;

	public GUICombobox Difficulty;

	public GUICombobox DefaultGFXCombo;

	public Slider aMusic;

	public Slider aSFX;

	public Slider aUI;

	public Slider ScrollSlider;

	public Slider ZoomSlider;

	public Slider RotationSlider;

	public Slider SSAA;

	public Slider ShadowQuality;

	public Slider GammaSlider;

	public GameObject KeyPanel;

	public GameObject TutorialPanel;

	public GameObject TutorialButton;

	public GameObject DifficultyText;

	public GameObject AudioPanel;

	public GUIToolTipper SMAATipper;

	public bool EnableTutorials = true;

	public Transform ModPanel;

	public GameObject ModContentPrefab;

	public GameObject ModHeaderPrefab;

	public static OptionsWindow Instance;
}
