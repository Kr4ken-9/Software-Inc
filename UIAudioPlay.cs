﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIAudioPlay : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerUpHandler, IEventSystemHandler
{
	public void OnPointerDown(PointerEventData eventData)
	{
		if (!string.IsNullOrEmpty(this.OnMouseDown))
		{
			UISoundFX.PlaySFX(this.OnMouseDown, -1f, 0f);
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (!string.IsNullOrEmpty(this.OnMouseOver))
		{
			UISoundFX.PlaySFX(this.OnMouseOver, -1f, 0f);
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (!string.IsNullOrEmpty(this.OnMouseUp))
		{
			UISoundFX.PlaySFX(this.OnMouseUp, -1f, 0f);
		}
	}

	public string OnMouseOver;

	public string OnMouseUp;

	public string OnMouseDown;
}
