﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CostDisplay : MonoBehaviour
{
	public void Show(float price, Vector2 pos, Color color)
	{
		this.Text = price.Currency(true);
		this.show = true;
		this.col = color;
		this.position = pos;
	}

	public void Show(float price, Vector2 pos)
	{
		this.Show(price, pos, Color.white);
	}

	public void Show(float price, Vector3 pos)
	{
		this.Show(price, pos, Color.white);
	}

	public void Show(float price, Vector3 pos, Color color)
	{
		Vector3 vector = Camera.main.WorldToScreenPoint(pos);
		if (pos.z >= 0f)
		{
			this.Show(price, new Vector2(vector.x, (float)Screen.height - vector.y), color);
		}
		else
		{
			this.Hide();
		}
	}

	public void Hide()
	{
		this.show = false;
	}

	public void FloatAway(float price)
	{
		this.Floaters.Add(new CostDisplay.Floater(price.Currency(true), new Vector3(this.position.x, this.position.y, 1f)));
		this.Hide();
	}

	public void FloatAway()
	{
		this.Floaters.Add(new CostDisplay.Floater(this.Text, new Vector3(this.position.x, this.position.y, 1f)));
		this.Hide();
	}

	private void Start()
	{
		CostDisplay.Instance = this;
	}

	private void Update()
	{
		for (int i = 0; i < this.Floaters.Count; i++)
		{
			this.Floaters[i].Pos = this.Floaters[i].Pos - new Vector3(Mathf.Sin(this.Floaters[i].Pos.z * 3.14159274f * 4f), Time.deltaTime * 128f, Time.deltaTime);
			if (this.Floaters[i].Pos.z <= 0f)
			{
				this.Floaters.RemoveAt(i);
				i--;
			}
		}
	}

	private void OnGUI()
	{
		if (this.show)
		{
			GUI.color = this.col;
			GUI.Label(new Rect(this.position.x - 64f, this.position.y - 12f, 128f, 24f), this.Text, this.Shadow);
			GUI.Label(new Rect(this.position.x - 64f, this.position.y - 12f, 128f, 24f), this.Text, this.TextStyle);
		}
		foreach (CostDisplay.Floater floater in this.Floaters)
		{
			GUI.color = new Color(1f, 1f, 1f, floater.Pos.z);
			GUI.Label(new Rect(floater.Pos.x - 64f, floater.Pos.y - 12f, 128f, 24f), floater.Text, this.Shadow);
			GUI.Label(new Rect(floater.Pos.x - 64f, floater.Pos.y - 12f, 128f, 24f), floater.Text, this.TextStyle);
		}
		GUI.color = Color.white;
	}

	private string Text = "0";

	private bool show;

	private Vector2 position;

	private List<CostDisplay.Floater> Floaters = new List<CostDisplay.Floater>();

	public GUIStyle TextStyle;

	public GUIStyle Shadow;

	public Color col;

	public static CostDisplay Instance;

	private class Floater
	{
		public Floater(string t, Vector3 p)
		{
			this.Text = t;
			this.Pos = p;
		}

		public string Text;

		public Vector3 Pos;
	}
}
