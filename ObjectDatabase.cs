﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectDatabase : ScriptableObject
{
	public static ObjectDatabase Instance
	{
		get
		{
			if (!ObjectDatabase._bound)
			{
				ObjectDatabase._instance = UnityEngine.Object.Instantiate<ObjectDatabase>(Resources.Load<ObjectDatabase>("ObjectDatabaseInstance"));
				ObjectDatabase._bound = true;
			}
			return ObjectDatabase._instance;
		}
	}

	public GameObject GetFurniture(string name)
	{
		GameObject result;
		if (this._furns.TryGetValue(name, out result))
		{
			return result;
		}
		return null;
	}

	public List<Furniture> GetUpgrades(string group)
	{
		return (from x in this._furnCs.Values
		where x.UpgradeCompatible(@group)
		select x).ToList<Furniture>();
	}

	public Furniture GetFurnitureComponent(string name)
	{
		return this._furnCs.GetOrNull(name);
	}

	public RoomSegment GetSegmentComponent(string name)
	{
		return this._segments.GetOrNull(name);
	}

	public List<GameObject> GetAllFurniture()
	{
		return this.Furniture;
	}

	public void AddFurniture(GameObject obj)
	{
		this.Furniture.Add(obj);
		this._furns.Add(obj.name, obj);
		this._furnCs.Add(obj.name, obj.GetComponent<Furniture>());
	}

	public void AddFencestyle(ObjectDatabase.FenceStyle style)
	{
		this.FenceStyles.Add(style);
	}

	public void AddSegment(GameObject obj)
	{
		this.RoomSegments.Add(obj);
		this._segments.Add(obj.name, obj.GetComponent<RoomSegment>());
	}

	public void RemoveFurniture(GameObject furn)
	{
		this.Furniture.Remove(furn);
		this._furnCs.Remove(furn.name);
		this._furns.Remove(furn.name);
	}

	private void OnEnable()
	{
		this._furns = this.Furniture.ToDictionary((GameObject x) => x.name, (GameObject x) => x);
		this._furnCs = this.Furniture.ToDictionary((GameObject x) => x.name, (GameObject x) => x.GetComponent<Furniture>());
		this._segments = this.RoomSegments.ToDictionary((GameObject x) => x.name, (GameObject x) => x.GetComponent<RoomSegment>());
	}

	[SerializeField]
	private List<GameObject> Furniture;

	public List<GameObject> RoomSegments;

	public List<ObjectDatabase.FenceStyle> FenceStyles;

	public Material DefaultFurnitureMaterial;

	public Material CombineFurnitureMaterial;

	public Material DefaultBaseMaterial;

	public Material GlassMaterial;

	[NonSerialized]
	private Dictionary<string, GameObject> _furns;

	[NonSerialized]
	private Dictionary<string, Furniture> _furnCs;

	[NonSerialized]
	private Dictionary<string, RoomSegment> _segments;

	private static bool _bound;

	private static ObjectDatabase _instance;

	[Serializable]
	public struct FenceStyle
	{
		public string Name;

		public Mesh Pole;

		public Mesh Fence;

		public Material Mat;

		public Texture Thumbnail;

		public Texture ThumbnailBump;

		public float Height;
	}
}
