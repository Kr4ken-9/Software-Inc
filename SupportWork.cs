﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SupportWork : WorkItem
{
	public SupportWork(string name) : base(name, null, -1)
	{
	}

	public SupportWork()
	{
	}

	public SupportWork(uint targetProduct, string productName, int siblingIndex) : base("Support for " + productName, null, siblingIndex)
	{
		this._targetProduct = targetProduct;
		SoftwareProduct targetProduct2 = this.TargetProduct;
		this.StartBugs = Mathf.Max(targetProduct2.Bugs, targetProduct2.StartBugs);
		TutorialSystem.Instance.StartTutorial("Support work", false);
	}

	public SoftwareProduct TargetProduct
	{
		get
		{
			return GameSettings.Instance.simulation.GetProduct(this._targetProduct, false);
		}
	}

	public override string GetIcon()
	{
		return "Info";
	}

	public override WorkItem.HasWorkReturn HasWork(Actor actor)
	{
		if (!actor.employee.IsRole(Employee.RoleBit.Programmer | Employee.RoleBit.Marketer))
		{
			return WorkItem.HasWorkReturn.NotApplicable;
		}
		if (this.Tickets.Count == 0 && this.Verified <= this.StartBugs - this.TargetProduct.Bugs)
		{
			return WorkItem.HasWorkReturn.Waiting;
		}
		return WorkItem.HasWorkReturn.True;
	}

	public override Color BackColor
	{
		get
		{
			return new Color(0.75f, 0.75f, 0f);
		}
	}

	public void Simulate(SDateTime time)
	{
		int count = this.Tickets.Count;
		for (int i = 0; i < this.Tickets.Count; i++)
		{
			SDateTime d = this.Tickets[i];
			if ((float)(time - d).ToInt() <= 2880f * (float)GameSettings.DaysPerMonth)
			{
				break;
			}
			this.Missed++;
			this.TicketDeal--;
			this.Tickets.RemoveAt(i);
			i--;
			if (!this.TargetProduct.OpenSource)
			{
				GameSettings.Instance.MyCompany.AddFans(-Mathf.RoundToInt(this.FixChance(0.001f, false) * Utilities.RandomRange(0.5f, 1f) * (float)count), this.TargetProduct._type, this.TargetProduct._category);
				HUD.Instance.AddPopupMessage("SupportWorkSlowWarning".Loc(new object[]
				{
					this.TargetProduct.Name
				}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.Support, 1);
			}
		}
		this._ticketsLast = this.Tickets.Count;
		if (this.TargetProduct.Userbase == 0)
		{
			return;
		}
		int num = Mathf.RoundToInt(this.FixChance(0.25f, true) * Utilities.RandomRange(0f, Mathf.Sqrt((float)this.TargetProduct.Userbase) * 0.1f));
		if (num == 0 && this.TargetProduct.Userbase > 0)
		{
			num = Utilities.RandomRange(0, 3);
		}
		for (int j = 0; j < num; j++)
		{
			this.Tickets.Add(time);
		}
	}

	private float FixChance(float offset, bool dpm)
	{
		float num = (this.StartBugs != 0) ? Mathf.Clamp01((float)this.TargetProduct.Bugs / (float)this.StartBugs) : 1f;
		float num2 = offset + num * (1f - offset);
		return (!dpm) ? num2 : (num2 / (float)GameSettings.DaysPerMonth);
	}

	public float GetAverageSkill(Team t)
	{
		float num = 0f;
		float num2 = 0f;
		List<Actor> employeesDirect = t.GetEmployeesDirect();
		for (int i = 0; i < employeesDirect.Count; i++)
		{
			if (employeesDirect[i].employee.IsRole(Employee.RoleBit.Programmer))
			{
				float skill = employeesDirect[i].employee.GetSkill(Employee.EmployeeRole.Programmer);
				num += skill * skill;
				num2 += skill;
			}
		}
		return (num2 <= 0f) ? 0f : (num / num2);
	}

	public override void DoWork(Actor employee, float effectiveness, float delta)
	{
		effectiveness *= ((!employee.employee.IsRole(Employee.RoleBit.Lead)) ? ((employee.GetPCAddonBonus(Employee.EmployeeRole.Programmer) + employee.GetPCAddonBonus(Employee.EmployeeRole.Marketer)) / 2f) : employee.GetPCAddonBonus(Employee.EmployeeRole.Lead));
		Team team = employee.GetTeam();
		this.DoWorkSub(effectiveness, delta, employee.employee.GetSkill(Employee.EmployeeRole.Programmer), employee.employee.GetSkill(Employee.EmployeeRole.Marketer), employee.employee.IsRole(Employee.RoleBit.Lead), (team == null) ? employee.employee.GetSkill(Employee.EmployeeRole.Programmer) : this.GetAverageSkill(team), true, true);
	}

	public override bool CanUseCompany()
	{
		return true;
	}

	public override bool HasCompanyWork()
	{
		return this.Tickets.Count != 0 || this.Verified > this.StartBugs - this.TargetProduct.Bugs;
	}

	private void DoWorkSub(float effectiveness, float delta, float progSkill, float markSkill, bool leader, float teamAverage, bool useGameSpeed, bool recordSkill)
	{
		SoftwareProduct targetProduct = this.TargetProduct;
		if (this.Tickets.Count > 0)
		{
			this.NextVerify += Utilities.PerDay((progSkill + markSkill).MapRange(0f, 2f, 1f, 2f, false) * 200f * effectiveness, delta, useGameSpeed);
			if (recordSkill)
			{
				base.RecordSkill(Employee.EmployeeRole.Marketer, markSkill, delta);
			}
			while (this.NextVerify >= 1f && this.Tickets.Count > 0)
			{
				this.TicketDeal++;
				this.Tickets.RemoveAt(0);
				this._ticketsLast = Mathf.Min(this.Tickets.Count, this._ticketsLast);
				if (this.Verified < this.StartBugs)
				{
					float num = this.FixChance(0.1f, false);
					if (UnityEngine.Random.value * 1.25f + 0.1f < teamAverage * num)
					{
						this.Verified = Mathf.Min(this.Verified + 1, this.StartBugs);
					}
				}
				this.NextVerify -= 1f;
			}
		}
		if (this.Verified > this.StartBugs - targetProduct.Bugs)
		{
			float num2 = (!leader) ? 1f : 0.25f;
			this.NextFix += Utilities.PerHour(effectiveness, delta, useGameSpeed) * progSkill * num2 * SoftwareAlpha.GetBugSpeedDamp(1f - (float)targetProduct.Bugs / (float)this.StartBugs) / (float)GameSettings.DaysPerMonth;
			if (recordSkill)
			{
				base.RecordSkill(Employee.EmployeeRole.Programmer, progSkill, delta);
			}
			while (this.NextFix >= 1f && this.Verified > this.StartBugs - targetProduct.Bugs)
			{
				targetProduct.Bugs = Mathf.Max(0, targetProduct.Bugs - 1);
				this.NextFix -= 1f;
			}
		}
	}

	public override float CompanyWork(float delta)
	{
		SoftwareProduct targetProduct = this.TargetProduct;
		int num = Mathf.Clamp(targetProduct.Userbase / 10000, 5, 100);
		float averageQuality = base.CompanyWorker.AverageQuality;
		float effectiveness = averageQuality * (float)num;
		this.DoWorkSub(effectiveness, delta, averageQuality, averageQuality, false, averageQuality, false, false);
		return Utilities.PerDay(base.CompanyWorker.BusinessSavy.MapRange(0f, 1f, 4f, 2f, false) * (float)num * Employee.AverageWage * Employee.RoleSalary[1], delta, false);
	}

	public override Employee.EmployeeRole? GetBoostRole(Actor act)
	{
		bool flag = act.employee.IsRole(Employee.RoleBit.Programmer);
		bool flag2 = act.employee.IsRole(Employee.RoleBit.Marketer);
		if (flag2 && flag)
		{
			return new Employee.EmployeeRole?((Utilities.RandomValue <= 0.25f) ? Employee.EmployeeRole.Marketer : Employee.EmployeeRole.Programmer);
		}
		if (flag2)
		{
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Marketer);
		}
		if (flag)
		{
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Programmer);
		}
		return null;
	}

	public override int EmitType(Actor actor)
	{
		return 0;
	}

	public override float GetWorkScore()
	{
		return 1f;
	}

	public override string CollapseLabel()
	{
		return "TicketsQueued".Loc(new object[]
		{
			this.GetTicketCount()
		});
	}

	public override string Category()
	{
		return this.TargetProduct.Userbase.ToString("N0") + " " + "Active users".Loc().ToLower();
	}

	public override string CurrentStage()
	{
		return "BugsFixed".Loc(new object[]
		{
			this.StartBugs - this.TargetProduct.Bugs
		}) + "\n" + "BugsVerified".Loc(new object[]
		{
			this.Verified
		});
	}

	public override string GetProgressLabel()
	{
		return "TicketsQueued".Loc(new object[]
		{
			this.GetTicketCount()
		}) + "\n" + "TicketsMissed".Loc(new object[]
		{
			this.Missed
		});
	}

	public override int GUIWorkItemType()
	{
		return 1;
	}

	private int GetTicketCount()
	{
		float t = TimeOfDay.Instance.Minute / 60f;
		return Mathf.RoundToInt(Mathf.Lerp((float)this._ticketsLast, (float)this.Tickets.Count, t));
	}

	public void CancelSupport()
	{
		if (this.AutoDev)
		{
			GameSettings.Instance.MyCompany.AddFans(-Mathf.RoundToInt((float)this.TargetProduct.Bugs / (this.TargetProduct.DevTime * SoftwareAlpha.BugLimitFactor) * (float)this.TargetProduct.Userbase), this.TargetProduct._type, this.TargetProduct._category);
			this.Kill(false);
			return;
		}
		if (base.ActiveDeal != null && this.TargetProduct.Bugs == this.StartBugs && this.Tickets.Count == 0)
		{
			HUD.Instance.dealWindow.CancelDeal(base.ActiveDeal, false);
			base.ActiveDeal = null;
			this.Kill(false);
		}
		else if (!this.TargetProduct.OpenSource && (base.ActiveDeal == null || !base.ActiveDeal.Incoming) && this.TargetProduct.Userbase > 0)
		{
			WindowManager.Instance.ShowMessageBox("SupportWorkCancelWarning2".Loc(), false, DialogWindow.DialogType.Warning, delegate
			{
				GameSettings.Instance.MyCompany.AddFans(-Mathf.RoundToInt((float)this.TargetProduct.Bugs / (this.TargetProduct.DevTime * SoftwareAlpha.BugLimitFactor) * (float)this.TargetProduct.Userbase), this.TargetProduct._type, this.TargetProduct._category);
				this.Kill(false);
			}, "Cancel support", null);
		}
		else
		{
			this.Kill(false);
		}
	}

	public override float StressMultiplier()
	{
		return 0.25f;
	}

	public override string GetWorkTypeName()
	{
		return "Support";
	}

	public override void AddCost(float cost)
	{
		this.TargetProduct.AddLoss(cost);
	}

	private readonly uint _targetProduct;

	public readonly int StartBugs;

	public List<SDateTime> Tickets = new List<SDateTime>();

	private int _ticketsLast;

	public float NextFix;

	public float NextVerify;

	public int Verified;

	public int Missed;

	public int TicketDeal;
}
