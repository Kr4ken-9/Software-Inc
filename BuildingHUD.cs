﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class BuildingHUD : MonoBehaviour
{
	private void Start()
	{
		BuildingHUD.Instance = this;
	}

	public void Enable(bool enable1, bool enable2, bool enable3)
	{
		this.XImg.gameObject.SetActive(enable1);
		this.YImg.gameObject.SetActive(enable2);
		this.RotImg.gameObject.SetActive(enable3);
	}

	public void SetDimension(Vector3 p1, Vector3 p2, bool first = true)
	{
		Vector3 a = Camera.main.WorldToScreenPoint(p1);
		Vector3 b = Camera.main.WorldToScreenPoint(p2);
		Image image = (!first) ? this.YImg : this.XImg;
		Text text = (!first) ? this.YText : this.XText;
		Vector2 vector = new Vector2((float)Screen.width, (float)Screen.height);
		float a2 = vector.magnitude + 128f;
		Vector3 vector2 = (a + b) * 0.5f;
		image.rectTransform.anchoredPosition = new Vector2(vector2.x - (float)(Screen.width / 2), vector2.y - (float)(Screen.height / 2));
		image.rectTransform.sizeDelta = new Vector2(Mathf.Min(a2, (a - b).magnitude), image.rectTransform.sizeDelta.y);
		float num = Mathf.Atan2(b.y - a.y, b.x - a.x) * 57.29578f;
		num += (float)((num >= -90f) ? 0 : 180);
		num -= (float)((num <= 90f) ? 0 : 180);
		image.rectTransform.rotation = Quaternion.Euler(0f, 0f, num);
		text.text = (p1 - p2).magnitude.ToString("F1");
	}

	public void SetRot(Vector3 p1, Vector3 p2, Vector3 p3)
	{
		Vector2 a = new Vector2(p1.x, p1.z);
		Vector2 b = new Vector2(p2.x, p2.z);
		Vector2 c = new Vector2(p3.x, p3.z);
		Vector3 vector = Camera.main.WorldToScreenPoint(p1);
		Vector3 vector2 = Camera.main.WorldToScreenPoint(p2);
		Vector3 vector3 = Camera.main.WorldToScreenPoint(p3);
		float num = b.AngleBetween(a, c);
		this.RotImg.rectTransform.anchoredPosition = new Vector2(vector2.x, -((float)Screen.height - vector2.y));
		float num2 = Mathf.Atan2(vector.y - vector2.y, vector.x - vector2.x) * 57.29578f + 90f;
		float num3 = Mathf.Atan2(vector3.y - vector2.y, vector3.x - vector2.x) * 57.29578f + 90f;
		float num4 = Mathf.DeltaAngle(num2, num3) / 360f;
		if (num4 < 0f)
		{
			num4 = Mathf.Abs(num4);
			this.RotImg.rectTransform.rotation = Quaternion.Euler(0f, 0f, num2);
			this.RotImg.fillAmount = num4;
			this.RotText.rectTransform.localRotation = Quaternion.Euler(0f, 0f, -num2);
		}
		else
		{
			this.RotImg.rectTransform.rotation = Quaternion.Euler(0f, 0f, num3);
			this.RotImg.fillAmount = num4;
			this.RotText.rectTransform.localRotation = Quaternion.Euler(0f, 0f, -num3);
		}
		this.RotText.text = num.ToString("F1") + "°";
	}

	public void SetDimension(Rect rect)
	{
		rect = new Rect(rect.x, rect.y, rect.width + 1f, rect.height + 1f);
		Vector3 a = Camera.main.WorldToScreenPoint(new Vector3(rect.xMin, (float)(GameSettings.Instance.ActiveFloor * 2), rect.yMin));
		Vector3 b = Camera.main.WorldToScreenPoint(new Vector3(rect.xMin, (float)(GameSettings.Instance.ActiveFloor * 2), rect.yMax));
		Vector3 b2 = Camera.main.WorldToScreenPoint(new Vector3(rect.xMax, (float)(GameSettings.Instance.ActiveFloor * 2), rect.yMin));
		Vector3 vector = (a + b) * 0.5f;
		this.XImg.rectTransform.anchoredPosition = new Vector2(vector.x - (float)(Screen.width / 2), vector.y - (float)(Screen.height / 2));
		this.XImg.rectTransform.sizeDelta = new Vector2((a - b).magnitude, this.XImg.rectTransform.sizeDelta.y);
		float num = Mathf.Atan2(b.y - a.y, b.x - a.x) * 57.29578f;
		num += (float)((num >= -90f) ? 0 : 180);
		num -= (float)((num <= 90f) ? 0 : 180);
		this.XImg.rectTransform.rotation = Quaternion.Euler(0f, 0f, num);
		this.XText.text = rect.height.ToString();
		vector = (a + b2) * 0.5f;
		this.YImg.rectTransform.anchoredPosition = new Vector2(vector.x - (float)(Screen.width / 2), vector.y - (float)(Screen.height / 2));
		this.YImg.rectTransform.sizeDelta = new Vector2((a - b2).magnitude, this.YImg.rectTransform.sizeDelta.y);
		num = Mathf.Atan2(b2.y - a.y, b2.x - a.x) * 57.29578f;
		num += (float)((num >= -90f) ? 0 : 180);
		num -= (float)((num <= 90f) ? 0 : 180);
		this.YImg.rectTransform.rotation = Quaternion.Euler(0f, 0f, num);
		this.YText.text = rect.width.ToString();
	}

	public static BuildingHUD Instance;

	public Image XImg;

	public Image YImg;

	public Image RotImg;

	public Text XText;

	public Text YText;

	public Text RotText;
}
