﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GUIListItem : MonoBehaviour
{
	public void SetValue(object value)
	{
		this._inputEnabled = false;
		switch (this.Parent.Type)
		{
		case GUIColumn.ColumnType.Label:
		case GUIColumn.ColumnType.Action:
			this._label.text = ((value != null) ? value.ToString() : string.Empty);
			break;
		case GUIColumn.ColumnType.Slider:
			this._slider.value = (float)value;
			break;
		case GUIColumn.ColumnType.Stars:
			this._iconBar.Values[1] = (float)value;
			this._iconBar.SetVerticesDirty();
			break;
		case GUIColumn.ColumnType.Role:
		{
			Employee.RoleBit roleBit = (Employee.RoleBit)value;
			this._roleBar.Sprites[0] = (((roleBit & Employee.RoleBit.Lead) <= Employee.RoleBit.None) ? -1 : 0);
			this._roleBar.Sprites[1] = (((roleBit & Employee.RoleBit.Designer) <= Employee.RoleBit.None) ? -1 : 2);
			this._roleBar.Sprites[2] = (((roleBit & Employee.RoleBit.Programmer) <= Employee.RoleBit.None) ? -1 : 1);
			this._roleBar.Sprites[3] = (((roleBit & Employee.RoleBit.Artist) <= Employee.RoleBit.None) ? -1 : 3);
			this._roleBar.Sprites[4] = (((roleBit & Employee.RoleBit.Marketer) <= Employee.RoleBit.None) ? -1 : 4);
			this._roleBar.SetVerticesDirty();
			break;
		}
		}
		this._inputEnabled = true;
	}

	public void Highlight(bool highlight)
	{
		GUIColumn.ColumnType type = this.Parent.Type;
		if (type != GUIColumn.ColumnType.Label)
		{
			if (type != GUIColumn.ColumnType.Stars)
			{
				if (type == GUIColumn.ColumnType.Role)
				{
					this._roleBar.Highlight = highlight;
					this._roleBar.SetVerticesDirty();
				}
			}
			else
			{
				this._iconBar.Highlight = highlight;
				this._iconBar.SetVerticesDirty();
			}
		}
		else
		{
			this._backgroundImage.color = ((!highlight) ? Color.clear : Color.white);
		}
	}

	public float PreferredWidth
	{
		get
		{
			if (this.Parent == null)
			{
				return 0f;
			}
			switch (this.Parent.Type)
			{
			case GUIColumn.ColumnType.Label:
			case GUIColumn.ColumnType.Action:
				return this._label.preferredWidth;
			case GUIColumn.ColumnType.Stars:
				return 68f;
			case GUIColumn.ColumnType.Role:
				return 120f;
			}
			return 64f;
		}
	}

	public void OnClick()
	{
		this.Parent.OnClickAction(this.Idx);
	}

	public void SetCallBack()
	{
		if (this._inputEnabled)
		{
			GUIColumn.ColumnType type = this.Parent.Type;
			if (type == GUIColumn.ColumnType.Slider)
			{
				this.Parent.SetValue(this.Idx, this._slider.value);
			}
		}
	}

	public int Idx;

	[SerializeField]
	private Text _label;

	[SerializeField]
	private Image _backgroundImage;

	[SerializeField]
	private Slider _slider;

	[SerializeField]
	private IconFillBar _iconBar;

	[SerializeField]
	private SheetTipGraphic _roleBar;

	public GUIColumn Parent;

	private bool _inputEnabled = true;
}
