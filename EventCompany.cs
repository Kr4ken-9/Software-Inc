﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class EventCompany : Company
{
	private EventCompany(string name, int money, SDateTime time) : base(name, money, time, true)
	{
	}

	public EventCompany(EventCompany Original) : base(Original.Name, (int)Original.Money, Original.Founded, true)
	{
		this.ReleaseSchedule = Original.ReleaseSchedule;
		this._businessReputation = Original.BusinessReputation;
		this._fans = Original._fans;
	}

	public SoftwareProduct[] Simulate(SDateTime time, MarketSimulation sim)
	{
		List<SoftwareProduct> list = new List<SoftwareProduct>();
		foreach (EventCompany.ProductPrototype item in (from x in this.ReleaseSchedule
		where Utilities.GetMonthsFlat(x.Release, time) >= 0
		select x).ToList<EventCompany.ProductPrototype>())
		{
			this.ReleaseSchedule.Remove(item);
			list.Add(item.ToProduct(this, sim));
		}
		this.Products.AddRange(list);
		return list.ToArray();
	}

	public static EventCompany Load(XMLParser.XMLNode node)
	{
		string value = node.GetNode("Name", true).Value;
		int money = Convert.ToInt32(node.GetNode("Money", true).Value);
		SDateTime time = SDateTime.FromString(node.GetNode("Founded", true).Value);
		return new EventCompany(value, money, time)
		{
			ReleaseSchedule = EventCompany.LoadProducts(node.GetNode("Products", true).GetNodes("Product", false)),
			_businessReputation = (float)Convert.ToDouble(node.GetNode("BusinessReputation", true).Value) / 100f,
			_fans = Convert.ToUInt32(node.GetNode("Fans", true).Value)
		};
	}

	private static List<EventCompany.ProductPrototype> LoadProducts(XMLParser.XMLNode[] nodes)
	{
		List<EventCompany.ProductPrototype> list = new List<EventCompany.ProductPrototype>();
		foreach (XMLParser.XMLNode xmlnode in nodes)
		{
			SDateTime release = SDateTime.FromString(xmlnode.GetNode("Release", true).Value);
			string value = xmlnode.GetNode("Name", true).Value;
			string value2 = xmlnode.GetNode("Type", true).Value;
			string value3 = xmlnode.GetNode("Category", true).Value;
			Dictionary<string, string> needs = xmlnode.GetNode("Needs", true).GetNodes("Need", false).ToDictionary((XMLParser.XMLNode x) => x.Attributes["Type"], (XMLParser.XMLNode x) => x.Value);
			string[] os = (from x in xmlnode.GetNode("OS", true).GetNodes("Name", false)
			select x.Value).ToArray<string>();
			float codeProgress = Mathf.Clamp01((float)Convert.ToDouble(xmlnode.GetNode("CodeProgress", true).Value));
			float artProgress = Mathf.Clamp01((float)Convert.ToDouble(xmlnode.GetNode("ArtProgress", true).Value));
			float codeQuality = Mathf.Clamp01((float)Convert.ToDouble(xmlnode.GetNode("CodeQuality", true).Value));
			float artQuality = Mathf.Clamp01((float)Convert.ToDouble(xmlnode.GetNode("ArtQuality", true).Value));
			float price = (float)Convert.ToDouble(xmlnode.GetNode("Price", true).Value);
			bool inHouse = Convert.ToBoolean(xmlnode.GetNode("InHouse", true).Value);
			float reception = (float)Convert.ToDouble(xmlnode.GetNode("Reception", true).Value);
			float popularity = (float)Convert.ToDouble(xmlnode.GetNode("Popularity", true).Value);
			string text = xmlnode.GetNode("SequelTo", true).Value;
			string[] feats = (from x in xmlnode.GetNode("Features", true).GetNodes("Feature", false)
			select x.Value).ToArray<string>();
			text = ((text != null && !string.IsNullOrEmpty(text.Trim())) ? text : null);
			list.Add(new EventCompany.ProductPrototype(value, value2, value3, needs, os, codeProgress, artProgress, codeQuality, artQuality, price, release, inHouse, reception, popularity, text, feats));
		}
		return list;
	}

	public List<EventCompany.ProductPrototype> ReleaseSchedule = new List<EventCompany.ProductPrototype>();

	[Serializable]
	public struct ProductPrototype
	{
		public ProductPrototype(string name, string type, string category, Dictionary<string, string> needs, string[] os, float codeProgress, float artProgress, float codeQuality, float artQuality, float price, SDateTime release, bool inHouse, float reception, float popularity, string sequelTo, string[] feats)
		{
			this.Name = name;
			this.Type = type;
			this.Category = category;
			this.SequelTo = sequelTo;
			this.Needs = needs;
			this.OSs = os;
			this.CodeProgress = codeProgress;
			this.ArtProgress = artProgress;
			this.CodeQuality = codeQuality;
			this.ArtQuality = artQuality;
			this.Price = price;
			this.Release = release;
			this.InHouse = inHouse;
			this.Reception = reception;
			this.Popularity = popularity;
			this.Features = feats;
			this._devTime = 0f;
		}

		public float GetDevtime()
		{
			if (this._devTime == 0f)
			{
				this._devTime = GameSettings.Instance.SoftwareTypes[this.Type].DevTime(this.Features, this.Category, null);
			}
			return this._devTime;
		}

		public SoftwareProduct ToProduct(Company company, MarketSimulation simulation)
		{
			bool osspecific = GameSettings.Instance.SoftwareTypes[this.Type].OSSpecific;
			Dictionary<string, uint> needs = this.Needs.ToDictionary((KeyValuePair<string, string> x) => x.Key, (KeyValuePair<string, string> x) => simulation.GetProductIDFromName(x.Value));
			string[] features = GameSettings.Instance.SoftwareTypes[this.Type].FixUp(this.Features, this.Category);
			uint? sequelto = (this.SequelTo != null) ? new uint?(GameSettings.Instance.simulation.GetProductIDFromName(this.SequelTo)) : null;
			uint[] array;
			if (osspecific)
			{
				array = (from x in this.OSs
				select GameSettings.Instance.simulation.GetProductIDFromName(x)).ToArray<uint>();
			}
			else
			{
				array = null;
			}
			uint[] array2 = array;
			uint followers = (uint)(GameSettings.Instance.SoftwareTypes[this.Type].GetReach(this.Category, this.Features, array2) * this.Reception * company.GetReputation(this.Type, this.Category));
			return new SoftwareProduct(this.Name, this.Type, this.Category, needs, array2, this.CodeProgress, this.ArtProgress, this.CodeQuality, this.ArtQuality, this.Price, this.Release, this.Release, 0, this.InHouse, company, sequelto, GameSettings.Instance.simulation.GetID(), 0f, features, null, followers, null);
		}

		public readonly string Name;

		public readonly string Type;

		public readonly string Category;

		public readonly string SequelTo;

		public readonly Dictionary<string, string> Needs;

		public readonly string[] OSs;

		public readonly string[] Features;

		public readonly float CodeProgress;

		public readonly float ArtProgress;

		public readonly float CodeQuality;

		public readonly float ArtQuality;

		public readonly float Price;

		public readonly SDateTime Release;

		public readonly bool InHouse;

		public readonly float Reception;

		public readonly float Popularity;

		[NonSerialized]
		private float _devTime;
	}
}
