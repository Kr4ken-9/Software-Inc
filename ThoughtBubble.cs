﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ThoughtBubble : MonoBehaviour
{
	private void Start()
	{
		if (ThoughtBubble.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		ThoughtBubble.Instance = this;
		base.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		if (ThoughtBubble.Instance == this)
		{
			ThoughtBubble.Instance = null;
		}
	}

	public void SetThought(string thought, Transform obj)
	{
		this.text.text = thought;
		if (!string.IsNullOrEmpty(thought))
		{
			Vector3 vector = CameraScript.Instance.mainCam.WorldToScreenPoint(obj.position + Vector3.up * 0.3f);
			if (vector.z < 0f)
			{
				return;
			}
			this.myself.anchoredPosition = new Vector2(vector.x / Options.UISize, -((float)Screen.height - vector.y) / Options.UISize);
			this.myself.sizeDelta = new Vector2(this.text.preferredWidth / 2f + 68f, this.text.preferredHeight + 83f);
			this.Show = 0.1f;
			this.trans = obj;
			base.gameObject.SetActive(true);
		}
	}

	private void Update()
	{
		if (this.Show <= 0f || this.trans == null)
		{
			base.gameObject.SetActive(false);
			return;
		}
		this.Show -= Time.deltaTime;
		Vector3 vector = CameraScript.Instance.mainCam.WorldToScreenPoint(this.trans.position + Vector3.up * 0.3f);
		if (vector.z < 0f)
		{
			base.gameObject.SetActive(false);
			return;
		}
		this.myself.anchoredPosition = new Vector2(vector.x / Options.UISize, -((float)Screen.height - vector.y) / Options.UISize);
	}

	public Text text;

	public float Show;

	public Transform trans;

	public RectTransform myself;

	public static ThoughtBubble Instance;
}
