﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class CompanyType
{
	public CompanyType(string name, Dictionary<KeyValuePair<string, string>, float> types, int min, int max, float perYear, string force, string nameGen)
	{
		this.Name = name;
		this.Types = types;
		this.Min = min;
		this.Max = max;
		this.PerYear = perYear;
		this.Force = force;
		this.NameGen = nameGen;
	}

	public CompanyType(XMLParser.XMLNode node)
	{
		this.Name = node.GetNodes("Specialization", true)[0].Value;
		this.Force = null;
		XMLParser.XMLNode node2 = node.GetNode("Force", false);
		if (node2 != null)
		{
			this.Force = node2.Value;
		}
		if (this.Force != null && this.Force.Split(new char[]
		{
			','
		}).Length != 2)
		{
			throw new Exception("Force value formatted incorrectly, please input as: SOFTWARETYPE,SOFTWARECATEGORY");
		}
		this.Min = Convert.ToInt32(node.GetNode("Min", true).Value);
		this.Max = Convert.ToInt32(node.GetNode("Max", true).Value);
		this.PerYear = (float)Convert.ToDouble(node.GetNode("PerYear", true).Value);
		this.Types = node.GetNode("Types", true).GetNodes("Type", false).ToDictionary((XMLParser.XMLNode x) => new KeyValuePair<string, string>(x.GetAttribute("Software"), x.TryGetAttribute("Category", null)), (XMLParser.XMLNode x) => (float)Convert.ToDouble(x.Value));
		this.NameGen = node.GetNodeValue("NameGen", null);
	}

	public CompanyType()
	{
	}

	public float GetEffort(string software, string category)
	{
		float result = 0f;
		if (this.Types.TryGetValue(new KeyValuePair<string, string>(software, category), out result))
		{
			return result;
		}
		if (this.Types.TryGetValue(new KeyValuePair<string, string>(software, null), out result))
		{
			return result;
		}
		return 1f;
	}

	public Dictionary<string, string[]> GetTypes()
	{
		Dictionary<string, HashSet<string>> dictionary = new Dictionary<string, HashSet<string>>();
		foreach (KeyValuePair<string, string> keyValuePair in this.Types.Keys)
		{
			SoftwareType softwareType;
			if (GameSettings.Instance.SoftwareTypes.TryGetValue(keyValuePair.Key, out softwareType) && (keyValuePair.Value == null || softwareType.Categories.ContainsKey(keyValuePair.Value)))
			{
				dictionary.Append(keyValuePair.Key, keyValuePair.Value);
			}
		}
		foreach (KeyValuePair<string, HashSet<string>> keyValuePair2 in dictionary.ToList<KeyValuePair<string, HashSet<string>>>())
		{
			if (keyValuePair2.Value.Contains(null))
			{
				dictionary[keyValuePair2.Key] = new SHashSet<string>
				{
					null
				};
			}
		}
		return dictionary.ToDictionary((KeyValuePair<string, HashSet<string>> x) => x.Key, (KeyValuePair<string, HashSet<string>> x) => x.Value.ToArray<string>());
	}

	public bool IsValid(int year)
	{
		foreach (KeyValuePair<KeyValuePair<string, string>, float> keyValuePair in this.Types)
		{
			SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[keyValuePair.Key.Key];
			if (softwareType.IsUnlocked(year))
			{
				if (keyValuePair.Key.Value == null)
				{
					if (softwareType.Categories.Values.Any((SoftwareCategory x) => x.IsUnlocked(year)))
					{
						return true;
					}
				}
				else if (softwareType.Categories[keyValuePair.Key.Value].IsUnlocked(year))
				{
					return true;
				}
			}
		}
		return false;
	}

	public override string ToString()
	{
		return this.Name;
	}

	public readonly string Name;

	public readonly Dictionary<KeyValuePair<string, string>, float> Types;

	public readonly float PerYear;

	public readonly int Min;

	public readonly int Max;

	public readonly string Force;

	public readonly string NameGen;
}
