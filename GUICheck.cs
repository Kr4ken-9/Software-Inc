﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUICheck : MonoBehaviour
{
	private void Update()
	{
		EventSystem current = EventSystem.current;
		if (current != null)
		{
			GUICheck.OverGUI = current.IsPointerOverGameObject();
			if (!WindowManager.HasModal && GUICombobox.OpenCombo == null && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2)))
			{
				Vector2 point = new Vector2(Input.mousePosition.x / Options.UISize, ((float)Screen.height - Input.mousePosition.y) / Options.UISize);
				for (int i = WindowManager.ActiveWindows.Count - 1; i >= 0; i--)
				{
					GUIWindow guiwindow = WindowManager.ActiveWindows[i];
					if (!(guiwindow == null) && !(guiwindow.gameObject == null))
					{
						Rect rect = new Rect(guiwindow.rectTransform.anchoredPosition.x, -guiwindow.rectTransform.anchoredPosition.y, guiwindow.rectTransform.rect.width, guiwindow.rectTransform.rect.height);
						if (rect.Contains(point))
						{
							guiwindow.Focus();
							break;
						}
					}
				}
			}
		}
	}

	public static bool OverGUI = true;
}
