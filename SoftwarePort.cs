﻿using System;
using System.Linq;
using UnityEngine;

[Serializable]
public class SoftwarePort : WorkItem
{
	public SoftwarePort(string name) : base(name, null, -1)
	{
	}

	public SoftwarePort()
	{
	}

	public SoftwarePort(SoftwareProduct product, SoftwareProduct[] os) : base("Porting".Loc() + " " + product.Name, null, -1)
	{
		this._product = product.ID;
		this.OSs = (from x in os
		select x.ID).ToArray<uint>();
		int count = this.Product.OSs.Count;
		float modif = Mathf.Pow(1.01f, (float)(count + os.Length - 1)) * product.DevTime * product.RealQuality;
		this.Goal = (from x in os
		select modif / 32f).ToArray<float>();
		this.GoalSum = this.Goal.Sum();
	}

	public SoftwareProduct Product
	{
		get
		{
			return GameSettings.Instance.simulation.GetProduct(this._product, false);
		}
	}

	public SoftwareProduct[] OSP
	{
		get
		{
			if (this._osp == null)
			{
				this._osp = (from x in this.OSs
				select GameSettings.Instance.simulation.GetProduct(x, true)).ToArray<SoftwareProduct>();
			}
			return this._osp;
		}
	}

	public override Color BackColor
	{
		get
		{
			return new Color32(121, 42, 191, byte.MaxValue);
		}
	}

	public override string GetWorkTypeName()
	{
		return "Port";
	}

	public override string GetIcon()
	{
		return "ArrowRight";
	}

	public override float GetProgress()
	{
		return this.Prog / this.GoalSum;
	}

	public float GetOpBound()
	{
		float num = 0f;
		for (int i = 0; i < this.Goal.Length; i++)
		{
			num += this.Goal[i];
			if (this.Prog < num)
			{
				return num;
			}
		}
		return this.GoalSum;
	}

	public uint GetLastOp()
	{
		float num = 0f;
		for (int i = 0; i < this.Goal.Length; i++)
		{
			num += this.Goal[i];
			if (this.Prog < num)
			{
				return this.OSs[Mathf.Max(0, i - 1)];
			}
		}
		return this.OSs[this.OSs.Length - 1];
	}

	public uint GetCurrentOp()
	{
		float num = 0f;
		for (int i = 0; i < this.Goal.Length; i++)
		{
			num += this.Goal[i];
			if (this.Prog < num)
			{
				return this.OSs[i];
			}
		}
		return this.OSs[this.OSs.Length - 1];
	}

	public int GetCurrentIdx()
	{
		float num = 0f;
		for (int i = 0; i < this.Goal.Length; i++)
		{
			num += this.Goal[i];
			if (this.Prog < num)
			{
				return i;
			}
		}
		return this.OSs.Length - 1;
	}

	public override string CurrentStage()
	{
		int num = this.GetCurrentIdx();
		if (this.WaitingForMock)
		{
			num = Mathf.Max(0, num - 1);
		}
		SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(this.OSs[num], true);
		return "PortTo".Loc(new object[]
		{
			product.Name
		});
	}

	public override float StressMultiplier()
	{
		return 0.5f;
	}

	public override void DoWork(Actor actor, float effectiveness, float delta)
	{
		if (base.ActiveDeal == null && this.Product.DevCompany != GameSettings.Instance.MyCompany)
		{
			this.Kill(false);
			return;
		}
		effectiveness = Utilities.PerDay(effectiveness * actor.GetPCAddonBonus(Employee.EmployeeRole.Programmer), delta, true);
		float skill = actor.employee.GetSkill(Employee.EmployeeRole.Programmer);
		base.RecordSkill(Employee.EmployeeRole.Programmer, skill, delta);
		effectiveness *= skill;
		if (actor.employee.IsRole(Employee.RoleBit.Lead))
		{
			effectiveness *= 0.25f;
		}
		float opBound = this.GetOpBound();
		this.Prog = Mathf.Min(this.GoalSum, this.Prog + effectiveness);
		if (this.Prog >= opBound)
		{
			uint lastOp = this.GetLastOp();
			SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(lastOp, true);
			if (product.IsMock && !product.MockSucceeded)
			{
				this.WaitingForMock = true;
				return;
			}
			if (!this.Product.OSs.Contains(lastOp))
			{
				this.Product.OSs.Add(lastOp);
			}
		}
		if (this.GoalSum == this.Prog)
		{
			this.Kill(false);
		}
	}

	public override bool CanUseCompany()
	{
		return true;
	}

	public override bool HasCompanyWork()
	{
		if (this.WaitingForMock)
		{
			SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(this.GetLastOp(), true);
			if (product.IsMock && !product.MockSucceeded)
			{
				return false;
			}
			this.WaitingForMock = false;
			if (!this.Product.OSs.Contains(product.ID))
			{
				this.Product.OSs.Add(product.ID);
			}
		}
		return true;
	}

	public override float CompanyWork(float delta)
	{
		if (base.ActiveDeal == null && this.Product.DevCompany != GameSettings.Instance.MyCompany)
		{
			this.Kill(false);
			return 0f;
		}
		float num = 2f;
		float num2 = Utilities.PerDay(num, delta, false);
		float num3 = base.CompanyWorker.AverageQuality * num2;
		float opBound = this.GetOpBound();
		this.Prog = Mathf.Min(this.GoalSum, this.Prog + num3);
		if (this.Prog >= opBound)
		{
			uint lastOp = this.GetLastOp();
			SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(lastOp, true);
			if (product.IsMock && !product.MockSucceeded)
			{
				this.WaitingForMock = true;
				return 0f;
			}
			if (!this.Product.OSs.Contains(lastOp))
			{
				this.Product.OSs.Add(lastOp);
			}
		}
		if (this.GoalSum == this.Prog)
		{
			this.Kill(false);
		}
		return num2 * base.CompanyWorker.BusinessSavy.MapRange(0f, 1f, 4f, 2f, false) * num * Employee.AverageWage * Employee.RoleSalary[1];
	}

	public override Employee.EmployeeRole? GetBoostRole(Actor act)
	{
		if (act.employee.IsRole(Employee.RoleBit.Programmer))
		{
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Programmer);
		}
		return null;
	}

	public override int EmitType(Actor actor)
	{
		return 0;
	}

	public override WorkItem.HasWorkReturn HasWork(Actor actor)
	{
		if (this.WaitingForMock)
		{
			SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(this.GetLastOp(), true);
			if (product.IsMock && !product.MockSucceeded)
			{
				return WorkItem.HasWorkReturn.Ignore;
			}
			this.WaitingForMock = false;
			if (!this.Product.OSs.Contains(product.ID))
			{
				this.Product.OSs.Add(product.ID);
			}
		}
		if (actor.employee.IsRole(Employee.RoleBit.Programmer))
		{
			return WorkItem.HasWorkReturn.True;
		}
		return WorkItem.HasWorkReturn.NotApplicable;
	}

	public void Skip()
	{
		if (this.WaitingForMock)
		{
			this.WaitingForMock = false;
		}
		else
		{
			this.Prog = this.GetOpBound();
		}
		if (this.GoalSum == this.Prog)
		{
			this.Kill(false);
		}
	}

	public override string Category()
	{
		return this.GetCurrentIdx() + 1 + "/" + this.OSs.Length;
	}

	public override string GetProgressLabel()
	{
		if (this.WaitingForMock)
		{
			return "MockOSPortWait".Loc();
		}
		return base.GetProgressLabel();
	}

	public override void AddCost(float cost)
	{
		this.Product.Loss += cost;
	}

	public readonly uint _product;

	public readonly uint[] OSs;

	public readonly float[] Goal;

	public readonly float GoalSum;

	public float Prog;

	[NonSerialized]
	public SoftwareProduct[] _osp;

	private bool WaitingForMock;
}
