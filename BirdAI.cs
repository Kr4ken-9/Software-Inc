﻿using System;
using UnityEngine;

public class BirdAI : MonoBehaviour, ICritter
{
	public string GetTypeName()
	{
		return "Bird";
	}

	public float OptimalMaxWeather()
	{
		return 1000f;
	}

	public float OptimalMinWeather()
	{
		return 5f;
	}

	public GameObject GetGameObject()
	{
		return base.gameObject;
	}

	public void ResetPlace()
	{
		float x = UnityEngine.Random.value * 256f;
		float z = UnityEngine.Random.value * 256f;
		if (UnityEngine.Random.value > 0.5f)
		{
			if (UnityEngine.Random.value > 0.5f)
			{
				x = 0f;
				base.transform.rotation = Quaternion.LookRotation(new Vector3(1f, 0f, 0f));
			}
			else
			{
				x = 256f;
				base.transform.rotation = Quaternion.LookRotation(new Vector3(-1f, 0f, 0f));
			}
		}
		else if (UnityEngine.Random.value > 0.5f)
		{
			z = 0f;
			base.transform.rotation = Quaternion.LookRotation(new Vector3(0f, 0f, 1f));
		}
		else
		{
			z = 256f;
			base.transform.rotation = Quaternion.LookRotation(new Vector3(0f, 0f, -1f));
		}
		base.transform.position = new Vector3(x, (float)UnityEngine.Random.Range(10, 20), z);
	}

	public void Spawn()
	{
		this.CountDown = 5f;
		this.HomeMode = false;
		this.NewTarget();
	}

	private void NewTarget()
	{
		Quaternion rotation = Quaternion.Euler(UnityEngine.Random.Range(-40f, 40f), UnityEngine.Random.Range(-40f, 40f), 0f) * base.transform.rotation;
		float num = Mathf.Repeat(rotation.eulerAngles.x, 360f);
		rotation = Quaternion.Euler((num >= 30f) ? Mathf.Clamp(num, 330f, 360f) : Mathf.Clamp(num, 0f, 30f), rotation.eulerAngles.y, rotation.eulerAngles.z);
		this.Target = base.transform.position + rotation * Vector3.forward * 10f;
		this.Target = new Vector3(this.Target.x, Mathf.Clamp(this.Target.y, 10f, 40f), this.Target.z);
		this.turnRate = 0f;
	}

	public bool UpdateMe()
	{
		if (GameSettings.GameSpeed == 0f)
		{
			this.anim.speed = 0f;
			return false;
		}
		this.anim.speed = 1f;
		if (!this.HomeMode && CritterController.ShouldGoHome(this))
		{
			this.Target = base.transform.forward * 512f;
			this.Target = new Vector3(this.Target.x, Mathf.Clamp(this.Target.y, 10f, 40f), this.Target.z);
			this.HomeMode = true;
		}
		if (this.turnRate < 1f)
		{
			this.turnRate = Mathf.Clamp01(this.turnRate + Time.deltaTime * GameSettings.GameSpeed * 2f);
		}
		if (!this.HomeMode)
		{
			this.CountDown -= Time.deltaTime * GameSettings.GameSpeed;
			if (this.CountDown < 0f)
			{
				this.CountDown = UnityEngine.Random.Range(2f, 5f);
				this.NewTarget();
			}
			else if ((base.transform.position - this.Target).sqrMagnitude < 1f)
			{
				this.CountDown = UnityEngine.Random.Range(2f, 5f);
				this.NewTarget();
			}
		}
		Quaternion rotation = Quaternion.Lerp(base.transform.rotation, Quaternion.LookRotation(this.Target - base.transform.position), this.turnRate * Time.deltaTime * GameSettings.GameSpeed);
		this.zRot = Mathf.Lerp(this.zRot, Room.LeftVal(this.Target.FlattenVector3(), base.transform.position.FlattenVector3(), (base.transform.position - base.transform.forward).FlattenVector3()) * 57.29578f / 2f, Time.deltaTime * GameSettings.GameSpeed);
		rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y, this.zRot);
		base.transform.rotation = rotation;
		float y = base.transform.forward.normalized.y;
		float num = this.Speed;
		if (y > 0f)
		{
			float num2 = y;
			this.anim.SetFloat("Flap", 1f - num2);
			num *= 0.25f + (1f - num2) * 0.75f;
		}
		else
		{
			this.anim.SetFloat("Flap", 0f);
		}
		base.transform.position = base.transform.position + base.transform.forward.normalized * num * Time.deltaTime * GameSettings.GameSpeed;
		return base.transform.position.x < -5f || base.transform.position.x > 261f || base.transform.position.z < -5f || base.transform.position.z > 261f;
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawLine(base.transform.position, this.Target);
	}

	public float OptimalMinLight()
	{
		return 0.9f;
	}

	public float OptimalMaxLight()
	{
		return 2f;
	}

	public bool ShouldUpdate()
	{
		return base.transform.position.y < CameraScript.Instance.mainCam.transform.position.y;
	}

	public Animator anim;

	private Vector3 Target;

	private float CountDown = 5f;

	public float Speed = 5f;

	private float turnRate;

	private float zRot;

	public bool HomeMode;
}
