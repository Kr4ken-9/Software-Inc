﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class ArticleGenerator
{
	public static string GenerateSoftwareReview(SoftwareProduct p)
	{
		return ArticleGenerator.GenerateArticle<ArticleGenerator.SoftwareReviewData>(ArticleGenerator.SoftwareReview, new string[]
		{
			"Intro",
			"Body",
			"Conclusion"
		}, new ArticleGenerator.SoftwareReviewData(p));
	}

	public static string GeneratePressBuildReview(ArticleGenerator.PressBuildReviewData p)
	{
		return ArticleGenerator.GenerateArticle<ArticleGenerator.PressBuildReviewData>(ArticleGenerator.PressBuildReview, new string[]
		{
			"Intro",
			"Body"
		}, p);
	}

	public static string GeneratePressReleaseReview(ArticleGenerator.PressReleaseData p)
	{
		return ArticleGenerator.GenerateArticle<ArticleGenerator.PressReleaseData>(ArticleGenerator.PressReleaseReview, new string[]
		{
			"Intro",
			"Body"
		}, p);
	}

	private static string GenerateArticle<T>(Dictionary<string, ArticleGenerator.Sentence<T>[]> sentences, string[] order, T subject)
	{
		Dictionary<string, StringBuilder> sections = new Dictionary<string, StringBuilder>();
		foreach (string key in order)
		{
			sections[key] = new StringBuilder();
		}
		foreach (KeyValuePair<string, ArticleGenerator.Sentence<T>[]> keyValuePair in sentences)
		{
			Dictionary<string, IGrouping<string, ArticleGenerator.Sentence<T>>> dictionary = (from x in keyValuePair.Value
			where x.Check(subject)
			group x by x.Group).ToDictionary((IGrouping<string, ArticleGenerator.Sentence<T>> x) => x.Key, (IGrouping<string, ArticleGenerator.Sentence<T>> x) => x);
			string random = (from x in dictionary
			where x.Value.Any((ArticleGenerator.Sentence<T> z) => z.AppendTo == null)
			select x.Key).GetRandom<string>();
			ArticleGenerator.SentenceTree<T> tree = new ArticleGenerator.SentenceTree<T>();
			if (random != null)
			{
				ArticleGenerator.Sentence<T> sentence = (from x in dictionary[random]
				where x.AppendTo == null
				orderby x.Priority, Utilities.RandomValue
				select x).FirstOrDefault<ArticleGenerator.Sentence<T>>();
				dictionary.Remove(random);
				tree.AddSentence(random, sentence, null);
				random = (from x in dictionary
				where x.Value.Any((ArticleGenerator.Sentence<T> z) => z.AppendTo == null || tree.Sentence.ContainsKey(z.AppendTo))
				select x.Key).GetRandom<string>();
				if (random != null)
				{
					do
					{
						sentence = (from x in dictionary[random]
						where x.AppendTo == null || tree.Sentence.ContainsKey(x.AppendTo)
						orderby x.Priority, Utilities.RandomValue
						select x).FirstOrDefault<ArticleGenerator.Sentence<T>>();
						dictionary.Remove(random);
						tree.AddSentence(random, sentence, sentence.AppendTo);
						random = (from x in dictionary
						where x.Value.Any((ArticleGenerator.Sentence<T> z) => z.AppendTo == null || tree.Sentence.ContainsKey(z.AppendTo))
						select x.Key).GetRandom<string>();
					}
					while (random != null);
				}
			}
			string value = tree.Execute(subject);
			sections[keyValuePair.Key].Append(value);
		}
		StringBuilder stringBuilder = new StringBuilder();
		bool flag = true;
		foreach (StringBuilder stringBuilder2 in from x in order
		select sections[x])
		{
			if (stringBuilder2.Length > 0)
			{
				if (!flag)
				{
					stringBuilder.AppendLine();
					stringBuilder.AppendLine();
				}
				stringBuilder.Append(stringBuilder2.ToString());
				flag = false;
			}
		}
		return stringBuilder.ToString();
	}

	static ArticleGenerator()
	{
		// Note: this type is marked as 'beforefieldinit'.
		Dictionary<string, ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>[]> dictionary = new Dictionary<string, ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>[]>();
		dictionary.Add("Intro", new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>[]
		{
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Start", null, 1, (ArticleGenerator.SoftwareReviewData x) => true, (ArticleGenerator.SoftwareReviewData x) => new string[]
			{
				x.C.Name,
				x.P.Name
			}, new string[]
			{
				"SoftwareIntroStart0",
				"SoftwareIntroStart1",
				"SoftwareIntroStart2"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Start", null, 0, (ArticleGenerator.SoftwareReviewData x) => !x.C.Products.Any((SoftwareProduct y) => !y.Traded && y._type.Equals(x.P._type)), (ArticleGenerator.SoftwareReviewData x) => new string[]
			{
				x.C.Name,
				x.P.Type.Category.Loc(),
				x.P.Name
			}, new string[]
			{
				"SoftwareIntroStart3",
				"SoftwareIntroStart4",
				"SoftwareIntroStart5"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("StartAdd", "Start", 0, (ArticleGenerator.SoftwareReviewData x) => x.FanBase > 0.5f, null, new string[]
			{
				"SoftwareIntroStartAdd0",
				"SoftwareIntroStartAdd1"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("StartAdd2", "StartAdd", 1, (ArticleGenerator.SoftwareReviewData x) => x.P.Quality > 0.25f, null, new string[]
			{
				"SoftwareIntroStartAdd20",
				"SoftwareIntroStartAdd21",
				"SoftwareIntroStartAdd22"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("StartAdd2", "StartAdd", 1, (ArticleGenerator.SoftwareReviewData x) => x.P.Quality <= 0.25f, null, new string[]
			{
				"SoftwareIntroStartAdd23",
				"SoftwareIntroStartAdd24"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Sequel", null, 1, (ArticleGenerator.SoftwareReviewData x) => x.P.SequelTo != null, (ArticleGenerator.SoftwareReviewData x) => new string[]
			{
				x.P.Name,
				x.P.SequelTo.Name
			}, new string[]
			{
				"SoftwareIntroSequel0",
				"SoftwareIntroSequel1",
				"SoftwareIntroSequel1",
				"SoftwareIntroSequel2"
			}, (ArticleGenerator.SoftwareReviewData x) => x.P.SequelTo.Quality, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("SequelAdd", "Sequel", 1, (ArticleGenerator.SoftwareReviewData x) => x.P.SequelTo != null && x.P.SequelBonus < 1f && x.P.SequelTo.Quality >= 0.75f, null, new string[]
			{
				"SoftwareIntroSequelAdd0"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("SequelAdd", "Sequel", 1, (ArticleGenerator.SoftwareReviewData x) => x.P.SequelTo != null && x.P.SequelBonus >= 1f && x.P.SequelTo.Quality >= 0.75f, null, new string[]
			{
				"SoftwareIntroSequelAdd1"
			}, null, 1)
		});
		Dictionary<string, ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>[]> dictionary2 = dictionary;
		string key = "Body";
		ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>[] array = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>[15];
		array[0] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Company", null, 0, (ArticleGenerator.SoftwareReviewData x) => true, (ArticleGenerator.SoftwareReviewData x) => new string[]
		{
			x.C.Name
		}, new string[]
		{
			"SoftwareBodyCompany0",
			"SoftwareBodyCompany1",
			"SoftwareBodyCompany2",
			"SoftwareBodyCompany3",
			"SoftwareBodyCompany4",
			"SoftwareBodyCompany5",
			"SoftwareBodyCompany6",
			"SoftwareBodyCompany7",
			"SoftwareBodyCompany8"
		}, (ArticleGenerator.SoftwareReviewData x) => x.FanBase, 3);
		array[1] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("CompanySWB", "Company", 0, (ArticleGenerator.SoftwareReviewData x) => true, null, new string[]
		{
			"SoftwareBodyCompanySWB0",
			"SoftwareBodyCompanySWB1"
		}, (ArticleGenerator.SoftwareReviewData x) => Mathf.Abs(x.Rep - x.FanBase), 1);
		array[2] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("CompanySW", "CompanySWB", 0, (ArticleGenerator.SoftwareReviewData x) => true, (ArticleGenerator.SoftwareReviewData x) => new string[]
		{
			x.P.Type.Category.Loc()
		}, new string[]
		{
			"SoftwareBodyCompanySW0",
			"SoftwareBodyCompanySW1",
			"SoftwareBodyCompanySW2",
			"SoftwareBodyCompanySW3",
			"SoftwareBodyCompanySW4",
			"SoftwareBodyCompanySW5"
		}, (ArticleGenerator.SoftwareReviewData x) => x.Rep, 2);
		array[3] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Bugs", null, 3, (ArticleGenerator.SoftwareReviewData x) => x.BugScore > 0.5f, null, new string[]
		{
			"SoftwareBodyBugs0",
			"SoftwareBodyBugs1",
			"SoftwareBodyBugs2",
			"SoftwareBodyBugs3"
		}, null, 1);
		array[4] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("BugAddon", "Bugs", 1, (ArticleGenerator.SoftwareReviewData x) => x.BugScore > 0.8f, null, new string[]
		{
			"SoftwareBodyBugAddon0",
			"SoftwareBodyBugAddon1",
			"SoftwareBodyBugAddon2"
		}, null, 1);
		array[5] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Overall", null, 1, (ArticleGenerator.SoftwareReviewData x) => true, (ArticleGenerator.SoftwareReviewData x) => new string[]
		{
			x.P.Name
		}, new string[]
		{
			"SoftwareBodyOverall0",
			"SoftwareBodyOverall1",
			"SoftwareBodyOverall2",
			"SoftwareBodyOverall3",
			"SoftwareBodyOverall4",
			"SoftwareBodyOverall5",
			"SoftwareBodyOverall6",
			"SoftwareBodyOverall7"
		}, (ArticleGenerator.SoftwareReviewData x) => x.P.Quality, 2);
		array[6] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("CodeProg", "Overall", 1, (ArticleGenerator.SoftwareReviewData x) => x.Ratio > 0f, null, new string[]
		{
			"SoftwareBodyCodeProg0",
			"SoftwareBodyCodeProg1",
			"SoftwareBodyCodeProg2",
			"SoftwareBodyCodeProg3",
			"SoftwareBodyCodeProg4",
			"SoftwareBodyCodeProg5"
		}, (ArticleGenerator.SoftwareReviewData x) => x.P.CodeProgress, 2);
		array[7] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("CodeQualB", "CodeProg", 1, (ArticleGenerator.SoftwareReviewData x) => true, null, new string[]
		{
			"SoftwareBodyCompanySWB0",
			"SoftwareBodyCompanySWB1"
		}, (ArticleGenerator.SoftwareReviewData x) => Mathf.Abs(x.P.CodeProgress - x.P.CodeQuality), 1);
		array[8] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("CodeQual", "CodeQualB", 1, (ArticleGenerator.SoftwareReviewData x) => true, null, new string[]
		{
			"SoftwareBodyCodeQual0",
			"SoftwareBodyCodeQual1",
			"SoftwareBodyCodeQual2",
			"SoftwareBodyCodeQual3",
			"SoftwareBodyCodeQual4",
			"SoftwareBodyCodeQual5"
		}, (ArticleGenerator.SoftwareReviewData x) => x.P.CodeQuality, 2);
		array[9] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("ArtProg", null, 2, (ArticleGenerator.SoftwareReviewData x) => x.Ratio < 1f, null, new string[]
		{
			"SoftwareBodyArtProg0",
			"SoftwareBodyArtProg1",
			"SoftwareBodyArtProg2",
			"SoftwareBodyArtProg3",
			"SoftwareBodyArtProg4",
			"SoftwareBodyArtProg5"
		}, (ArticleGenerator.SoftwareReviewData x) => x.P.ArtProgress, 2);
		array[10] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("ArtQualB", "ArtProg", 1, (ArticleGenerator.SoftwareReviewData x) => true, null, new string[]
		{
			"SoftwareBodyCompanySWB0",
			"SoftwareBodyCompanySWB1"
		}, (ArticleGenerator.SoftwareReviewData x) => Mathf.Abs(x.P.ArtProgress - x.P.ArtQuality), 1);
		array[11] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("ArtQual", "ArtQualB", 1, (ArticleGenerator.SoftwareReviewData x) => true, null, new string[]
		{
			"SoftwareBodyArtQual0",
			"SoftwareBodyArtQual1",
			"SoftwareBodyArtQual2",
			"SoftwareBodyArtQual3",
			"SoftwareBodyArtQual4",
			"SoftwareBodyArtQual5"
		}, (ArticleGenerator.SoftwareReviewData x) => x.P.ArtQuality, 2);
		array[12] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Copmetitor", null, 3, (ArticleGenerator.SoftwareReviewData x) => x.Comp != null, (ArticleGenerator.SoftwareReviewData x) => new string[]
		{
			x.Comp.Name,
			x.Comp.DevCompany.Name
		}, new string[]
		{
			"SoftwareBodyCopmetitor0",
			"SoftwareBodyCopmetitor1",
			"SoftwareBodyCopmetitor2"
		}, null, 1);
		array[13] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("ReachProb", null, 3, (ArticleGenerator.SoftwareReviewData x) => x.ReachScore < 0.1f, null, new string[]
		{
			"SoftwareBodyReachProb0",
			"SoftwareBodyReachProb1",
			"SoftwareBodyReachProb2"
		}, null, 1);
		int num = 14;
		string group = "FeatScore";
		string appendTo = null;
		int priority = 3;
		Func<ArticleGenerator.SoftwareReviewData, bool> check = (ArticleGenerator.SoftwareReviewData x) => x.FeatScore < 0.75f;
		Func<ArticleGenerator.SoftwareReviewData, string[]> extractor = null;
		string[] array2 = new string[4];
		array2[0] = "SoftwareBodyFeatScore0";
		array2[1] = "SoftwareBodyFeatScore1";
		array2[2] = "SoftwareBodyFeatScore2";
		array[num] = new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>(group, appendTo, priority, check, extractor, array2, (ArticleGenerator.SoftwareReviewData x) => x.FeatScore, 1);
		dictionary2.Add(key, array);
		dictionary.Add("Conclusion", new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>[]
		{
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Begin", null, 0, (ArticleGenerator.SoftwareReviewData x) => true, null, new string[]
			{
				"SoftwareConclusionBegin0",
				"SoftwareConclusionBegin1",
				"SoftwareConclusionBegin2"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("BeginResult", "Begin", 0, (ArticleGenerator.SoftwareReviewData x) => x.PriceScore > 0.75f, (ArticleGenerator.SoftwareReviewData x) => new string[]
			{
				x.P.Name
			}, new string[]
			{
				"SoftwareConclusionBeginResult0",
				"SoftwareConclusionBeginResult1",
				"SoftwareConclusionBeginResult2"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("BeginResult", "Begin", 0, (ArticleGenerator.SoftwareReviewData x) => x.PriceScore > 0.25f && x.PriceScore <= 0.75f, (ArticleGenerator.SoftwareReviewData x) => new string[]
			{
				x.P.Name
			}, new string[]
			{
				"SoftwareConclusionBeginResult3",
				"SoftwareConclusionBeginResult4"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("BeginResult", "Begin", 0, (ArticleGenerator.SoftwareReviewData x) => x.PriceScore <= 0.25f, (ArticleGenerator.SoftwareReviewData x) => new string[]
			{
				x.P.Name
			}, new string[]
			{
				"SoftwareConclusionBeginResult5",
				"SoftwareConclusionBeginResult6"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Sales", null, 1, (ArticleGenerator.SoftwareReviewData x) => x.SellingPotential > 0.75f, null, new string[]
			{
				"SoftwareConclusionSales0",
				"SoftwareConclusionSales1"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Sales", null, 1, (ArticleGenerator.SoftwareReviewData x) => x.SellingPotential < 0.25f, null, new string[]
			{
				"SoftwareConclusionSales2",
				"SoftwareConclusionSales3"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("SaleRep", "Sales", 0, (ArticleGenerator.SoftwareReviewData x) => x.SellingPotential < 0.25f && x.Rep < 0.25f, null, new string[]
			{
				"SoftwareConclusionSaleRep0",
				"SoftwareConclusionSaleRep1"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("SaleRepHope", "SaleRep", 0, (ArticleGenerator.SoftwareReviewData x) => x.P.Quality > 0.75f, null, new string[]
			{
				"SoftwareConclusionSaleRepHope0",
				"SoftwareConclusionSaleRepHope1"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("Marketing", "Sales", 0, (ArticleGenerator.SoftwareReviewData x) => x.SellingPotential < 0.25f && x.Hype < 0.25f, null, new string[]
			{
				"SoftwareConclusionMarketing0",
				"SoftwareConclusionMarketing1",
				"SoftwareConclusionMarketing2"
			}, null, 1),
			new ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>("SalesReach", "Sales", 0, (ArticleGenerator.SoftwareReviewData x) => x.SellingPotential < 0.25f && x.ReachScore < 0.25f, null, new string[]
			{
				"SoftwareConclusionSalesReach0",
				"SoftwareConclusionSalesReach1"
			}, null, 1)
		});
		ArticleGenerator.SoftwareReview = dictionary;
		ArticleGenerator.PressBuildReview = new Dictionary<string, ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>[]>
		{
			{
				"Intro",
				new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>[]
				{
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("Intro", null, 0, (ArticleGenerator.PressBuildReviewData x) => true, (ArticleGenerator.PressBuildReviewData x) => new string[]
					{
						x.Company,
						x.Product
					}, new string[]
					{
						"PressBuildIntroIntro0",
						"PressBuildIntroIntro1",
						"PressBuildIntroIntro2"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("IntroAdd", "Intro", 0, (ArticleGenerator.PressBuildReviewData x) => x.PressBuildEffect < 0.25f, null, new string[]
					{
						"PressBuildIntroIntroAdd0",
						"PressBuildIntroIntroAdd1",
						"PressBuildIntroIntroAdd2",
						"PressBuildIntroIntroAdd3"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("IntroAdd", "Intro", 1, (ArticleGenerator.PressBuildReviewData x) => x.EstQual < 0.25f, null, new string[]
					{
						"PressBuildIntroIntroAdd4",
						"SoftwareIntroStartAdd24",
						"PressBuildIntroIntroAdd5"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("IntroAdd", "Intro", 2, (ArticleGenerator.PressBuildReviewData x) => true, null, new string[]
					{
						"PressBuildIntroIntroAdd6",
						"PressBuildIntroIntroAdd7",
						"PressBuildIntroIntroAdd8"
					}, null, 1)
				}
			},
			{
				"Body",
				new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>[]
				{
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("Rep", null, 0, (ArticleGenerator.PressBuildReviewData x) => x.Rep < 0.1f, (ArticleGenerator.PressBuildReviewData x) => new string[]
					{
						x.Company
					}, new string[]
					{
						"PressBuildBodyRep0",
						"PressBuildBodyRep1"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("Rep", null, 0, (ArticleGenerator.PressBuildReviewData x) => x.Rep > 0.5f, (ArticleGenerator.PressBuildReviewData x) => new string[]
					{
						x.Company
					}, new string[]
					{
						"PressBuildBodyRep2",
						"PressBuildBodyRep3"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("Quality", null, 2, (ArticleGenerator.PressBuildReviewData x) => true, (ArticleGenerator.PressBuildReviewData x) => null, new string[]
					{
						"PressBuildBodyQuality0",
						"PressBuildBodyQuality1",
						"PressBuildBodyQuality2",
						"PressBuildBodyQuality3",
						"PressBuildBodyQuality4",
						"PressBuildBodyQuality5",
						"PressBuildBodyQuality6",
						"PressBuildBodyQuality7"
					}, (ArticleGenerator.PressBuildReviewData x) => x.EstQual, 2),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("QToFeatScore", "Quality", 0, (ArticleGenerator.PressBuildReviewData x) => true, (ArticleGenerator.PressBuildReviewData x) => null, new string[]
					{
						"SoftwareBodyCompanySWB0",
						"SoftwareBodyCompanySWB1"
					}, (ArticleGenerator.PressBuildReviewData x) => Mathf.Abs(x.EstQual - x.FeatScore), 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("FeatScore", "QToFeatScore", 0, (ArticleGenerator.PressBuildReviewData x) => true, (ArticleGenerator.PressBuildReviewData x) => null, new string[]
					{
						"PressBuildBodyFeatScore0",
						"PressBuildBodyFeatScore1",
						"PressBuildBodyFeatScore2",
						"PressBuildBodyFeatScore3"
					}, (ArticleGenerator.PressBuildReviewData x) => x.FeatScore, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("Followers", null, 1, (ArticleGenerator.PressBuildReviewData x) => true, (ArticleGenerator.PressBuildReviewData x) => new string[]
					{
						x.Product
					}, new string[]
					{
						"PressBuildBodyFollowers0",
						"PressBuildBodyFollowers1",
						"PressBuildBodyFollowers2",
						"PressBuildBodyFollowers3",
						"PressBuildBodyFollowers4"
					}, (ArticleGenerator.PressBuildReviewData x) => x.Followers, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("ReleaseDate", null, 3, (ArticleGenerator.PressBuildReviewData x) => x.HasRelease == null, (ArticleGenerator.PressBuildReviewData x) => null, new string[]
					{
						"PressBuildBodyReleaseDate0",
						"PressBuildBodyReleaseDate1"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("ReleaseDate", null, 4, (ArticleGenerator.PressBuildReviewData x) => x.Months < 0, (ArticleGenerator.PressBuildReviewData x) => null, new string[]
					{
						"PressBuildBodyReleaseDate2"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("ReleaseDate", null, 5, (ArticleGenerator.PressBuildReviewData x) => x.Months < 6, (ArticleGenerator.PressBuildReviewData x) => null, new string[]
					{
						"PressBuildBodyReleaseDate3",
						"PressBuildBodyReleaseDate4",
						"PressBuildBodyReleaseDate5"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("ReleaseDate", null, 6, (ArticleGenerator.PressBuildReviewData x) => x.Months > 12, (ArticleGenerator.PressBuildReviewData x) => null, new string[]
					{
						"PressBuildBodyReleaseDate6",
						"PressBuildBodyReleaseDate7",
						"PressBuildBodyReleaseDate8"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>("ReleaseDate", null, 7, (ArticleGenerator.PressBuildReviewData x) => true, (ArticleGenerator.PressBuildReviewData x) => new string[]
					{
						x.HasRelease.Value.ToCompactString()
					}, new string[]
					{
						"PressBuildBodyReleaseDate9",
						"PressBuildBodyReleaseDate10",
						"PressBuildBodyReleaseDate11"
					}, null, 1)
				}
			}
		};
		ArticleGenerator.PressReleaseReview = new Dictionary<string, ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>[]>
		{
			{
				"Intro",
				new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>[]
				{
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("Intro", null, 0, (ArticleGenerator.PressReleaseData x) => x.PressEffect < 0.25f, (ArticleGenerator.PressReleaseData x) => new string[]
					{
						x.Company,
						x.Product
					}, new string[]
					{
						"PressReleaseIntroIntro0",
						"PressReleaseIntroIntro1",
						"PressReleaseIntroIntro2",
						"PressReleaseIntroIntro3"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("Intro", null, 1, (ArticleGenerator.PressReleaseData x) => true, (ArticleGenerator.PressReleaseData x) => new string[]
					{
						x.Company,
						x.Product
					}, new string[]
					{
						"PressReleaseIntroIntro4",
						"PressReleaseIntroIntro5",
						"PressReleaseIntroIntro6",
						"PressReleaseIntroIntro7"
					}, null, 1)
				}
			},
			{
				"Body",
				new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>[]
				{
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("Rep", null, 0, (ArticleGenerator.PressReleaseData x) => x.Rep < 0.1f, (ArticleGenerator.PressReleaseData x) => new string[]
					{
						x.Company
					}, new string[]
					{
						"PressBuildBodyRep0",
						"PressBuildBodyRep1"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("Rep", null, 0, (ArticleGenerator.PressReleaseData x) => x.Rep > 0.5f, (ArticleGenerator.PressReleaseData x) => new string[]
					{
						x.Company
					}, new string[]
					{
						"PressBuildBodyRep2",
						"PressBuildBodyRep3"
					}, (ArticleGenerator.PressReleaseData x) => x.Rep, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("Phase", null, 1, (ArticleGenerator.PressReleaseData x) => x.Phase == 0, (ArticleGenerator.PressReleaseData x) => new string[]
					{
						x.Product
					}, new string[]
					{
						"PressReleaseBodyPhase0",
						"PressReleaseBodyPhase1",
						"PressReleaseBodyPhase2"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("Phase", null, 2, (ArticleGenerator.PressReleaseData x) => true, (ArticleGenerator.PressReleaseData x) => new string[]
					{
						x.Product
					}, new string[]
					{
						"PressReleaseBodyPhase3",
						"PressReleaseBodyPhase4",
						"PressReleaseBodyPhase5"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("PhaseAdd", "Phase", 0, (ArticleGenerator.PressReleaseData x) => true, null, new string[]
					{
						"PressReleaseBodyPhaseAdd0",
						"PressReleaseBodyPhaseAdd1",
						"PressReleaseBodyPhaseAdd2",
						"PressReleaseBodyPhaseAdd3"
					}, (ArticleGenerator.PressReleaseData x) => x.FeatScore, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("PressOptions", null, 3, (ArticleGenerator.PressReleaseData x) => x.PressOptions < 0.15f, null, new string[]
					{
						"PressReleaseBodyPressOptions0",
						"PressReleaseBodyPressOptions1",
						"PressReleaseBodyPressOptions2"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("PressOptions", null, 4, (ArticleGenerator.PressReleaseData x) => x.PressOptions < 0.7f, null, new string[]
					{
						"PressReleaseBodyPressOptions3",
						"PressReleaseBodyPressOptions4"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("PressOptions", null, 5, (ArticleGenerator.PressReleaseData x) => true, null, new string[]
					{
						"PressReleaseBodyPressOptions5",
						"PressReleaseBodyPressOptions6",
						"PressReleaseBodyPressOptions7"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("PressOptionsAndBut", "PressOptions", 0, (ArticleGenerator.PressReleaseData x) => true, null, new string[]
					{
						"SoftwareBodyCompanySWB0",
						"SoftwareBodyCompanySWB1"
					}, (ArticleGenerator.PressReleaseData x) => Mathf.Abs(x.PressQuality - x.PressOptions * 1.43f), 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("PressOptionsAdd", "PressOptionsAndBut", 0, (ArticleGenerator.PressReleaseData x) => true, null, new string[]
					{
						"PressReleaseBodyPressOptionsAdd0",
						"PressReleaseBodyPressOptionsAdd1",
						"PressReleaseBodyPressOptionsAdd2",
						"PressReleaseBodyPressOptionsAdd3",
						"PressReleaseBodyPressOptionsAdd4",
						"PressReleaseBodyPressOptionsAdd5",
						"PressReleaseBodyPressOptionsAdd6",
						"PressReleaseBodyPressOptionsAdd7"
					}, (ArticleGenerator.PressReleaseData x) => x.PressQuality, 2),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("ReleaseDate", null, 6, (ArticleGenerator.PressReleaseData x) => x.ReleaseDate == null, null, new string[]
					{
						"PressReleaseBodyReleaseDate0",
						"PressReleaseBodyReleaseDate1"
					}, null, 1),
					new ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>("ReleaseDate", null, 7, (ArticleGenerator.PressReleaseData x) => true, (ArticleGenerator.PressReleaseData x) => new string[]
					{
						x.ReleaseDate.Value.ToCompactString()
					}, new string[]
					{
						"PressReleaseBodyReleaseDate2",
						"PressReleaseBodyReleaseDate3"
					}, null, 1)
				}
			}
		};
	}

	public static Dictionary<string, ArticleGenerator.Sentence<ArticleGenerator.SoftwareReviewData>[]> SoftwareReview;

	public static Dictionary<string, ArticleGenerator.Sentence<ArticleGenerator.PressBuildReviewData>[]> PressBuildReview;

	public static Dictionary<string, ArticleGenerator.Sentence<ArticleGenerator.PressReleaseData>[]> PressReleaseReview;

	public class Sentence<T>
	{
		public Sentence(string group, string appendTo, int priority, Func<T, bool> check, Func<T, string[]> extractor, string[] sentences, Func<T, float> evaluator = null, int range = 1)
		{
			this.Group = group;
			this.AppendTo = appendTo;
			this.Priority = priority;
			this.Check = check;
			this.Extractor = extractor;
			this.Sentences = sentences;
			this.Evaluator = evaluator;
			this.EvalRange = range;
		}

		public string Execute(T input)
		{
			string input2 = (this.Evaluator != null) ? this.Sentences[this.Evaluator(input).Quantize(this.Sentences.Length / this.EvalRange) * this.EvalRange + Utilities.RandomRange(0, this.EvalRange)] : this.Sentences[Utilities.RandomRange(0, this.Sentences.Length)];
			string[] array = (this.Extractor != null) ? this.Extractor(input) : null;
			if (array == null)
			{
				return input2.Loc();
			}
			string result;
			try
			{
				result = string.Format(input2.Loc(), array);
			}
			catch (Exception)
			{
				result = input2.Loc();
			}
			return result;
		}

		public int Priority;

		public string Group;

		public string AppendTo;

		public Func<T, bool> Check;

		public Func<T, string[]> Extractor;

		public Func<T, float> Evaluator;

		public string[] Sentences;

		public int EvalRange;
	}

	public class SoftwareReviewData
	{
		public SoftwareReviewData(SoftwareProduct p)
		{
			this.P = p;
			this.C = p.DevCompany;
			this.Rep = p.DevCompany.GetReputation(p._type, p._category);
			this.FanBase = 1f - Mathf.Pow(1f - Mathf.Clamp01(this.C.Fans / 2000000f), 2f);
			SoftwareType type = p.Type;
			List<SoftwareProduct> list = (from x in GameSettings.Instance.simulation.GetAllProducts()
			where x != this.P && x._type.Equals(this.P._type) && x._category.Equals(this.P._category) && Utilities.GetMonths(x.Release, this.P.Release) < 60f
			select x).ToList<SoftwareProduct>();
			float priceScore;
			if (list.Count > 0)
			{
				float? num = list.AverageOutlier((SoftwareProduct x) => x.Price / x.DevTime, 0.5f);
				priceScore = ((num == null) ? Utilities.GetBasePrice(this.P._type, this.P._category, 1f, 1f) : num.Value);
			}
			else
			{
				priceScore = Utilities.GetBasePrice(this.P._type, this.P._category, 1f, 1f);
			}
			this.PriceScore = priceScore;
			float num2 = p.Price / p.DevTime;
			this.PriceScore = ((num2 > this.PriceScore) ? Mathf.Clamp01(num2 / (this.PriceScore * 2f)).MapRange(0.5f, 1f, 1f, 0f, false) : 1f);
			this.PriceScore *= p.Quality;
			this.SellingPotential = p.GetQuality(SDateTime.Now(), true) * this.PriceScore * this.Rep;
			this.Hype = Mathf.Sqrt(p.GetAwareness());
			this.BugScore = (float)p.Bugs / (p.DevTime * SoftwareAlpha.BugLimitFactor);
			this.Ratio = type.CodeArtRatio(p.Features);
			this.Comp = null;
			float num3 = 0f;
			foreach (SoftwareProduct softwareProduct in GameSettings.Instance.simulation.GetAllProducts())
			{
				if (softwareProduct._type.Equals(p._type) && softwareProduct._category.Equals(p._category) && (!type.OSSpecific || p.OSOverlap(softwareProduct)))
				{
					float quality = softwareProduct.GetQuality(SDateTime.Now(), true);
					if (quality > num3)
					{
						num3 = quality;
						this.Comp = softwareProduct;
					}
				}
			}
			if (type.OSSpecific)
			{
				uint[] oss = (from x in (from x in GameSettings.Instance.simulation.GetAllProducts()
				where "Operating System".Equals(x._type)
				orderby x.Userbase descending
				select x).Take(3)
				select x.ID).ToArray<uint>();
				this.ReachScore = type.GetReach(p._category, p.Features, p.OSs) / type.GetReach(p._category, p.Features, oss);
			}
			else
			{
				this.ReachScore = 1f;
			}
			this.FeatScore = p.RelativeFeatureScore(GameSettings.Instance.simulation, SDateTime.Now());
			this.SellingPotential *= this.ReachScore;
			this.SellingPotential = Mathf.Sqrt(this.SellingPotential);
			if (this.SellingPotential < 0.25f && p.Followers > 1000u && p.Followers > this.C.Fans)
			{
				this.SellingPotential = 0.5f;
			}
		}

		public SoftwareProduct P;

		public Company C;

		public float PriceScore;

		public float SellingPotential;

		public float Hype;

		public float BugScore;

		public float Ratio;

		public float ReachScore;

		public float FeatScore;

		public float Rep;

		public float FanBase;

		public SoftwareProduct Comp;
	}

	public class PressBuildReviewData
	{
		public PressBuildReviewData(string company, string product, float estQual, float featScore, float pressBuildEffect, float followers, float rep, int months, SDateTime? hasRelease)
		{
			this.Company = company;
			this.Product = product;
			this.EstQual = estQual;
			this.FeatScore = featScore;
			this.PressBuildEffect = pressBuildEffect;
			this.Followers = followers;
			this.Rep = rep;
			this.Months = months;
			this.HasRelease = hasRelease;
		}

		public string Company;

		public string Product;

		public float EstQual;

		public float FeatScore;

		public float PressBuildEffect;

		public float Followers;

		public float Rep;

		public int Months;

		public SDateTime? HasRelease;
	}

	public class PressReleaseData
	{
		public PressReleaseData(string company, string product, int phase, SDateTime? releaseDate, float rep, float innovation, float featScore, float pressQuality, float pressOptions, float pressEffect)
		{
			this.Company = company;
			this.Product = product;
			this.Phase = phase;
			this.ReleaseDate = releaseDate;
			this.Rep = rep;
			this.Innovation = innovation;
			this.FeatScore = featScore;
			this.PressQuality = pressQuality;
			this.PressEffect = pressEffect;
			this.PressOptions = pressOptions;
		}

		public string Company;

		public string Product;

		public int Phase;

		public SDateTime? ReleaseDate;

		public float Rep;

		public float Innovation;

		public float FeatScore;

		public float PressQuality;

		public float PressOptions;

		public float PressEffect;
	}

	public class SentenceTree<T>
	{
		public void AddSentence(string group, ArticleGenerator.Sentence<T> sentence, string appendTo)
		{
			this.Sentence[group] = sentence;
			this.Tree[group] = new List<string>();
			if (appendTo != null)
			{
				this.Tree[appendTo].Add(group);
			}
		}

		public string Execute(T input)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (ArticleGenerator.Sentence<T> sentence in from x in this.Tree
			select this.Sentence[x.Key] into x
			where x.AppendTo == null
			orderby x.Priority
			select x)
			{
				stringBuilder.Append(sentence.Execute(input));
				this.ExecuteSub(sentence.Group, input, stringBuilder);
				if (stringBuilder[stringBuilder.Length - 1] != '!' && stringBuilder[stringBuilder.Length - 1] != '?')
				{
					stringBuilder.Append(".");
				}
				stringBuilder.Append(" ");
			}
			return stringBuilder.ToString();
		}

		private void ExecuteSub(string group, T input, StringBuilder result)
		{
			foreach (ArticleGenerator.Sentence<T> sentence in from x in this.Tree[@group]
			select this.Sentence[x] into x
			orderby x.Priority
			select x)
			{
				result.Append(sentence.Execute(input));
				this.ExecuteSub(sentence.Group, input, result);
			}
		}

		public Dictionary<string, ArticleGenerator.Sentence<T>> Sentence = new Dictionary<string, ArticleGenerator.Sentence<T>>();

		public Dictionary<string, List<string>> Tree = new Dictionary<string, List<string>>();
	}
}
