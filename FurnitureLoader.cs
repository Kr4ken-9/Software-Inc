﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using DevConsole;
using UnityEngine;

public static class FurnitureLoader
{
	public static void Init()
	{
		if (!FurnitureLoader.Initialized)
		{
			Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
			bool flag = false;
			float realtimeSinceStartup = Time.realtimeSinceStartup;
			FurnitureLoader.LoadFurniture(out flag, true);
			Debug.Log("Modded furniture load time: " + (Time.realtimeSinceStartup - realtimeSinceStartup));
			if (FurnitureLoader.LoadedFurniture.Count > 0)
			{
				LoadDebugger.AddInfo("Finished loading furniture mods");
			}
			if (flag && Options.ConsoleOnError && !DevConsole.Console.isOpen)
			{
				DevConsole.Console.Open();
			}
			FurnitureLoader.Initialized = true;
			if (ModWindow.Instance != null)
			{
				SteamWorkshop.RecheckItems(FurnitureLoader.LoadedFurniture.OfType<IWorkshopItem>());
				ModWindow.Instance.Refresh();
			}
		}
	}

	public static void ReLoadFurniture()
	{
		foreach (GameObject gameObject in FurnitureLoader.LoadedFurniture.SelectMany((FurnitureMod x) => x.Furniture))
		{
			ObjectDatabase.Instance.RemoveFurniture(gameObject);
			Furniture component = gameObject.GetComponent<Furniture>();
			if (component != null)
			{
				component.isTemporary = true;
			}
			UnityEngine.Object.Destroy(gameObject);
			if (HUD.Instance != null)
			{
				HUD.Instance.RemoveFurnitureButton(gameObject);
			}
		}
		FurnitureLoader.LoadedFurniture.Clear();
		bool flag = false;
		FurnitureLoader.LoadFurniture(out flag, false);
		if (flag && Options.ConsoleOnError && !DevConsole.Console.isOpen)
		{
			DevConsole.Console.Open();
		}
		if (HUD.Instance != null)
		{
			foreach (GameObject furniture in FurnitureLoader.LoadedFurniture.SelectMany((FurnitureMod x) => x.Furniture))
			{
				HUD.Instance.AddFurnitureButton(furniture);
			}
		}
		SteamWorkshop.RecheckItems(FurnitureLoader.LoadedFurniture.OfType<IWorkshopItem>());
		if (ModWindow.Instance != null)
		{
			ModWindow.Instance.Refresh();
		}
	}

	public static FurnitureMod LoadFurnitureMod(string dir, ref bool errors, string name, bool logFail = false)
	{
		try
		{
			List<GameObject> list = new List<GameObject>();
			StringBuilder stringBuilder = new StringBuilder();
			Dictionary<string, Mesh> meshes = new Dictionary<string, Mesh>();
			Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
			string path = Path.Combine(dir, "Materials.xml");
			Dictionary<string, Material> dictionary = new Dictionary<string, Material>();
			if (File.Exists(path))
			{
				try
				{
					dictionary = FurnitureLoader.LoadMaterials(XMLParser.ParseXML(File.ReadAllText(path)), dir);
				}
				catch (Exception ex)
				{
					DevConsole.Console.LogError("Failed loading materials for " + name + " with error:\n" + ex.ToString());
				}
			}
			dictionary["Glass"] = ObjectDatabase.Instance.GlassMaterial;
			foreach (string text in Directory.GetFiles(dir, "*.xml"))
			{
				if (!Path.GetFileName(text).ToLower().Equals("materials.xml"))
				{
					stringBuilder.AppendLine("Loading furniture: " + name + "/" + Path.GetFileName(text));
					bool flag = false;
					string text2 = string.Empty;
					XMLParser.XMLNode xmlnode = null;
					try
					{
						text2 = File.ReadAllText(text);
					}
					catch (Exception arg)
					{
						stringBuilder.AppendLine("\tFailed reading file");
						stringBuilder.AppendLine("\t" + arg);
						flag = true;
					}
					if (!flag)
					{
						try
						{
							xmlnode = XMLParser.ParseXML(text2);
						}
						catch (Exception arg2)
						{
							stringBuilder.AppendLine("\tFailed parsing xml");
							stringBuilder.AppendLine("\t" + arg2);
							flag = true;
						}
					}
					GameObject gameObject = null;
					bool flag2;
					bool flag3;
					if (!flag)
					{
						try
						{
							gameObject = FurnitureLoader.CreateFurnitureObject(Path.GetFileNameWithoutExtension(text), xmlnode, dir, stringBuilder, out flag2, out flag3, meshes, sprites, dictionary);
							AudioSource[] componentsInChildren = gameObject.GetComponentsInChildren<AudioSource>();
							for (int j = 0; j < componentsInChildren.Length; j++)
							{
								componentsInChildren[j].outputAudioMixerGroup = AudioManager.InGameNormal;
							}
						}
						catch (Exception ex2)
						{
							stringBuilder.AppendLine("\tFailed loading with error:");
							stringBuilder.AppendLine("\t" + ex2.ToString());
							flag2 = false;
							flag3 = false;
						}
					}
					else
					{
						flag2 = false;
						flag3 = false;
					}
					if (flag2)
					{
						Furniture component = gameObject.GetComponent<Furniture>();
						component.FileName = text;
						gameObject.SetActive(false);
						UnityEngine.Object.DontDestroyOnLoad(gameObject);
						ObjectDatabase.Instance.AddFurniture(gameObject);
						list.Add(gameObject);
						component.UpgradeTo = xmlnode.TryGetAttribute("UpgradeTo", null);
						component.UpgradeFrom = xmlnode.TryGetAttribute("UpgradeFrom", null);
					}
					else if (gameObject != null)
					{
						gameObject.SetActive(false);
						Furniture component2 = gameObject.GetComponent<Furniture>();
						if (component2 != null)
						{
							component2.isTemporary = true;
						}
						UnityEngine.Object.Destroy(gameObject);
					}
					if (!flag2 || flag3)
					{
						errors = true;
						if (!flag2)
						{
							DevConsole.Console.LogError(stringBuilder.ToString());
						}
						else
						{
							DevConsole.Console.LogWarning(stringBuilder.ToString());
						}
						LoadDebugger.AddError("Failed loading furniture mod: " + name);
					}
				}
			}
			if (list.Count > 0)
			{
				FurnitureMod furnitureMod = new FurnitureMod(dir, list, Path.GetFileNameWithoutExtension(dir));
				FurnitureLoader.LoadedFurniture.Add(furnitureMod);
				return furnitureMod;
			}
		}
		catch (Exception ex3)
		{
			if (logFail)
			{
				GameData.FailedModList.Add(name);
			}
			string text3 = "Error loading furniture pack " + name + ":\n" + ex3.ToString();
			LoadDebugger.AddError("Failed loading furniture mod: " + name);
			Debug.Log(text3);
			DevConsole.Console.LogError(text3);
		}
		return null;
	}

	public static void BakeBounds(Furniture furn)
	{
		if (furn == null)
		{
			DevConsole.Console.LogError("Furniture does not exist");
		}
		if (furn.FileName != null && File.Exists(furn.FileName))
		{
			try
			{
				XMLParser.XMLNode xmlnode = XMLParser.ParseXML(File.ReadAllText(furn.FileName));
				xmlnode.Attributes.Remove("AutoBounds");
				XMLParser.XMLNode xmlnode2 = xmlnode.GetNode("Furniture", false);
				if (xmlnode2 == null)
				{
					xmlnode2 = new XMLParser.XMLNode("Furniture");
					xmlnode.Children.Add(xmlnode2);
				}
				xmlnode2.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("BuildBoundary"));
				if (furn.BuildBoundary != null && furn.BuildBoundary.Length > 0)
				{
					xmlnode2.Children.Add(new XMLParser.XMLNode("BuildBoundary", string.Join("\n", furn.BuildBoundary.SelectInPlace((Vector2 x) => x.Serialize())), null));
				}
				else
				{
					xmlnode2.Children.Add(new XMLParser.XMLNode("BuildBoundary", string.Empty, null));
				}
				xmlnode2.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("NavBoundary"));
				if (furn.NavBoundary != null && furn.NavBoundary.Length > 0)
				{
					xmlnode2.Children.Add(new XMLParser.XMLNode("NavBoundary", string.Join("\n", furn.NavBoundary.SelectInPlace((Vector2 x) => x.Serialize())), null));
				}
				else
				{
					xmlnode2.Children.Add(new XMLParser.XMLNode("NavBoundary", string.Empty, null));
				}
				xmlnode2.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("MeshBoundary"));
				if (furn.MeshBoundary != null && furn.MeshBoundary.Length > 0)
				{
					xmlnode2.Children.Add(new XMLParser.XMLNode("MeshBoundary", string.Join("\n", furn.MeshBoundary.SelectInPlace((Vector2 x) => x.Serialize())), null));
				}
				else
				{
					xmlnode2.Children.Add(new XMLParser.XMLNode("MeshBoundary", string.Empty, null));
				}
				xmlnode2.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("WallWidth"));
				xmlnode2.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("YOffset"));
				if (furn.WallFurn)
				{
					xmlnode2.Children.Add(new XMLParser.XMLNode("WallWidth", furn.WallWidth.ToString(), null));
					xmlnode2.Children.Add(new XMLParser.XMLNode("YOffset", furn.YOffset.ToString(), null));
				}
				xmlnode2.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("Height1"));
				xmlnode2.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("Height2"));
				xmlnode2.Children.Add(new XMLParser.XMLNode("Height1", furn.Height1.ToString(), null));
				xmlnode2.Children.Add(new XMLParser.XMLNode("Height2", furn.Height2.ToString(), null));
				xmlnode2.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("OnXEdge"));
				xmlnode2.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("OnYEdge"));
				xmlnode2.Children.Add(new XMLParser.XMLNode("OnXEdge", furn.OnXEdge.ToString(), null));
				xmlnode2.Children.Add(new XMLParser.XMLNode("OnYEdge", furn.OnYEdge.ToString(), null));
				BoxCollider component = furn.GetComponent<BoxCollider>();
				if (component != null)
				{
					xmlnode.Children.RemoveAll((XMLParser.XMLNode x) => x.Name.Equals("BoxCollider"));
					XMLParser.XMLNode xmlnode3 = new XMLParser.XMLNode("BoxCollider");
					xmlnode3.Children.Add(new XMLParser.XMLNode("center", component.center.Serialize(), null));
					xmlnode3.Children.Add(new XMLParser.XMLNode("size", component.size.Serialize(), null));
					xmlnode.Children.Add(xmlnode3);
				}
				File.WriteAllText(furn.FileName, XMLParser.ExportXML(xmlnode));
				DevConsole.Console.Log(furn.FileName + " has been updated");
			}
			catch (Exception ex)
			{
				DevConsole.Console.LogError(ex.ToString());
			}
			return;
		}
		DevConsole.Console.LogError("Failed finding XML file");
	}

	private static void LoadFurniture(out bool errors, bool logErrors)
	{
		errors = false;
		string path = Path.Combine(Utilities.GetRoot(), "Furniture");
		if (!Directory.Exists(path))
		{
			Directory.CreateDirectory(path);
		}
		else
		{
			foreach (string text in Directory.GetDirectories(path))
			{
				FurnitureLoader.LoadFurnitureMod(text, ref errors, Path.GetFileName(text), logErrors);
			}
		}
	}

	private static string FindUniqueFurnitureName(string input)
	{
		int num = 1;
		string text = input;
		while (ObjectDatabase.Instance.GetFurniture(text) != null)
		{
			text = input + " " + num;
			num++;
		}
		return text;
	}

	private static Dictionary<string, Material> LoadMaterials(XMLParser.XMLNode root, string dir)
	{
		Dictionary<string, Material> dictionary = new Dictionary<string, Material>();
		foreach (XMLParser.XMLNode xmlnode in root.Children)
		{
			if (xmlnode.TryGetAttribute("Standard", null) != null)
			{
				Material material = new Material(ObjectDatabase.Instance.CombineFurnitureMaterial);
				Texture2D texture2D = new Texture2D(256, 256);
				texture2D.LoadImage(File.ReadAllBytes(Path.Combine(dir, xmlnode.GetNodeValue("Texture", null))));
				material.mainTexture = texture2D;
				dictionary[xmlnode.Name] = material;
			}
			else
			{
				Material material2 = new Material(ObjectDatabase.Instance.DefaultBaseMaterial);
				material2.SetTexture("_MainTex", null);
				material2.SetTexture("_BumpMap", null);
				material2.SetTexture("_OcclusionMap", null);
				material2.SetFloat("_Metallic", 0f);
				material2.SetFloat("_Glossiness", 0f);
				material2.SetFloat("_BumpScale", 1f);
				material2.SetColor("_Emission", Color.black);
				dictionary[xmlnode.Name] = material2;
				foreach (XMLParser.XMLNode xmlnode2 in xmlnode.Children)
				{
					if (xmlnode2.Name.Equals("Textures"))
					{
						foreach (XMLParser.XMLNode xmlnode3 in xmlnode2.Children)
						{
							Texture2D texture2D2 = new Texture2D(256, 256);
							texture2D2.LoadImage(File.ReadAllBytes(Path.Combine(dir, xmlnode3.Value)));
							material2.SetTexture(xmlnode3.Name, texture2D2);
						}
					}
					else if (xmlnode2.Name.Equals("Floats"))
					{
						foreach (XMLParser.XMLNode xmlnode4 in xmlnode2.Children)
						{
							material2.SetFloat(xmlnode4.Name, (float)Convert.ToDouble(xmlnode4.Value));
						}
					}
					else if (xmlnode2.Name.Equals("Colors"))
					{
						foreach (XMLParser.XMLNode xmlnode5 in xmlnode2.Children)
						{
							material2.SetColor(xmlnode5.Name, SVector3.Deserialize(xmlnode5.Value, false));
						}
					}
				}
			}
		}
		return dictionary;
	}

	private static void StripFurnitureGameObject(GameObject go)
	{
		foreach (Renderer renderer in go.GetComponentsInChildren<Renderer>())
		{
			renderer.transform.SetParent(null);
			UnityEngine.Object.Destroy(renderer.gameObject);
		}
		Furniture component = go.GetComponent<Furniture>();
		component.Colorable.Clear();
		Upgradable component2 = go.GetComponent<Upgradable>();
		bool flag = component2 != null;
		BoxCollider[] components = go.GetComponents<BoxCollider>();
		for (int j = 1; j < components.Length; j++)
		{
			UnityEngine.Object.Destroy(components[j]);
		}
		foreach (Transform transform in go.GetComponentsInChildren<Transform>())
		{
			if (transform.gameObject != go && transform.GetComponent<SnapPoint>() == null && transform.GetComponent<InteractionPoint>() == null && (!flag || transform.gameObject != component2.SmokePosition))
			{
				transform.transform.SetParent(null);
				UnityEngine.Object.Destroy(transform.gameObject);
			}
		}
		component.HoldablePoints = new Transform[0];
		component.ColorableLights = new List<Light>();
		component.UpgradeFrom = null;
		component.UpgradeTo = null;
	}

	private static void InitFurniture(Furniture furn)
	{
		if (furn.Colorable == null)
		{
			furn.Colorable = new List<Renderer>();
		}
		if (furn.ColorableLights == null)
		{
			furn.ColorableLights = new List<Light>();
		}
		if (furn.Type == null)
		{
			furn.Type = "None";
		}
		if (furn.AuraValues == null)
		{
			furn.AuraValues = new float[0];
		}
	}

	private static GameObject CreateFurnitureObject(string furnName, XMLParser.XMLNode root, string rootFolder, StringBuilder output, out bool success, out bool error, Dictionary<string, Mesh> meshes, Dictionary<string, Sprite> sprites, Dictionary<string, Material> materials)
	{
		error = false;
		string text = root.TryGetAttribute("Base", null);
		Furniture furn;
		GameObject gameObject;
		if (text != null)
		{
			GameObject furniture = ObjectDatabase.Instance.GetFurniture(text);
			if (furniture != null)
			{
				gameObject = UnityEngine.Object.Instantiate<GameObject>(furniture);
				FurnitureLoader.StripFurnitureGameObject(gameObject);
				furn = gameObject.GetComponent<Furniture>();
			}
			else
			{
				output.AppendLine("\tFailed loading furniture " + text + " using default");
				gameObject = new GameObject();
				furn = gameObject.AddComponent<Furniture>();
				error = true;
			}
		}
		else
		{
			gameObject = new GameObject();
			furn = gameObject.AddComponent<Furniture>();
		}
		FurnitureLoader.InitFurniture(furn);
		gameObject.name = FurnitureLoader.FindUniqueFurnitureName(furnName);
		string attribute = root.GetAttribute("Thumbnail");
		Sprite sprite;
		if (!sprites.TryGetValue(attribute, out sprite))
		{
			Texture2D texture2D = new Texture2D(128, 128);
			texture2D.LoadImage(File.ReadAllBytes(Path.Combine(rootFolder, attribute)));
			sprite = Sprite.Create(texture2D, new Rect(0f, 0f, 128f, 128f), Vector2.zero);
			sprites[attribute] = sprite;
		}
		furn.Thumbnail = sprite;
		GameObject[] array = null;
		bool flag = false;
		try
		{
			flag = Convert.ToBoolean(root.TryGetAttribute("AutoBounds", null) ?? "False");
		}
		catch (Exception)
		{
		}
		foreach (XMLParser.XMLNode xmlnode in root.Children)
		{
			if (xmlnode.Name.Equals("Models"))
			{
				array = new GameObject[xmlnode.Children.Count];
				int num = 0;
				foreach (XMLParser.XMLNode xmlnode2 in xmlnode.Children)
				{
					try
					{
						string value = xmlnode2.GetNode("File", true).Value;
						Mesh mesh;
						if (!meshes.TryGetValue(value, out mesh))
						{
							List<Mesh> list = ObjImporter.ImportMeshes(File.ReadAllText(Path.Combine(rootFolder, value)), false, true);
							if (list.Count == 1)
							{
								mesh = list[0];
							}
							else
							{
								if (list.Count <= 1)
								{
									throw new Exception("No mesh data");
								}
								mesh = new Mesh();
								mesh.CombineMeshes((from x in list
								select new CombineInstance
								{
									mesh = x,
									subMeshIndex = 0,
									transform = Matrix4x4.identity
								}).ToArray<CombineInstance>());
							}
							meshes[value] = mesh;
						}
						GameObject gameObject2 = new GameObject(FurnitureLoader.GetNodeValue<string>(xmlnode2, "ComponentName", "SubMesh"));
						gameObject2.AddComponent<MeshFilter>().sharedMesh = mesh;
						array[num] = gameObject2;
						MeshRenderer meshRenderer = gameObject2.AddComponent<MeshRenderer>();
						int num2 = Convert.ToInt32(FurnitureLoader.GetNodeValue<string>(xmlnode2, "Parent", "-1"));
						gameObject2.transform.SetParent((num2 < 0) ? gameObject.transform : array[num2].transform);
						gameObject2.transform.localPosition = SVector3.Deserialize(xmlnode2.GetNode("Position", true).Value, false);
						gameObject2.transform.localRotation = Quaternion.Euler(SVector3.Deserialize(xmlnode2.GetNode("Rotation", true).Value, false));
						gameObject2.transform.localScale = SVector3.Deserialize(xmlnode2.GetNode("Scale", true).Value, false);
						if (xmlnode2.Contains("Material"))
						{
							string nodeValue = xmlnode2.GetNodeValue("Material", null);
							Material material;
							if (materials.TryGetValue(nodeValue, out material))
							{
								meshRenderer.sharedMaterial = material;
								if (material.shader == ObjectDatabase.Instance.CombineFurnitureMaterial.shader)
								{
									gameObject2.tag = "Highlight";
								}
							}
							else
							{
								output.AppendLine("\tFailed finding material" + nodeValue + " for mesh " + value);
								meshRenderer.material = ObjectDatabase.Instance.CombineFurnitureMaterial;
								error = true;
							}
						}
						else
						{
							gameObject2.tag = "Highlight";
							meshRenderer.material = ObjectDatabase.Instance.CombineFurnitureMaterial;
							furn.Colorable.Add(meshRenderer);
						}
					}
					catch (Exception ex)
					{
						string nodeValue2 = FurnitureLoader.GetNodeValue<string>(xmlnode2, "File", "Undefined");
						output.AppendLine("\tFailed loading mesh " + nodeValue2 + " with error:");
						output.AppendLine("\t" + ex.Message);
						success = false;
						return gameObject;
					}
					num++;
				}
			}
			else if (xmlnode.Name.Equals("Transforms"))
			{
				foreach (XMLParser.XMLNode xmlnode3 in xmlnode.Children)
				{
					GameObject gameObject3 = new GameObject(xmlnode3.GetNode("Name", true).Value);
					XMLParser.XMLNode parent = xmlnode3.GetNode("Parent", false);
					if (parent != null)
					{
						Transform transform = gameObject.GetComponentsInChildren<Transform>().FirstOrDefault((Transform x) => x.name.Equals(parent.Value));
						gameObject3.transform.SetParent(transform ?? gameObject.transform);
					}
					else
					{
						gameObject3.transform.SetParent(gameObject.transform);
					}
					gameObject3.transform.localPosition = SVector3.Deserialize(xmlnode3.GetNode("Position", true).Value, false);
					gameObject3.transform.localRotation = Quaternion.Euler(SVector3.Deserialize(xmlnode3.GetNode("Rotation", true).Value, false));
				}
			}
			else if (xmlnode.Name.Equals("InteractionPoints"))
			{
				try
				{
					for (int i = 0; i < furn.InteractionPoints.Length; i++)
					{
						UnityEngine.Object.Destroy(furn.InteractionPoints[i].gameObject);
					}
					List<InteractionPoint> list2 = new List<InteractionPoint>();
					int[] array2 = new int[xmlnode.Children.Count];
					int num3 = 0;
					foreach (XMLParser.XMLNode xmlnode4 in xmlnode.Children)
					{
						GameObject gameObject4 = new GameObject(FurnitureLoader.GetNodeValue<string>(xmlnode4, "ComponentName", "InteractionPoint"));
						gameObject4.transform.SetParent(gameObject.transform);
						InteractionPoint interactionPoint = gameObject4.AddComponent<InteractionPoint>();
						interactionPoint.transform.localPosition = SVector3.Deserialize(xmlnode4.GetNode("Position", true).Value, false);
						interactionPoint.transform.localRotation = Quaternion.Euler(SVector3.Deserialize(xmlnode4.GetNode("Rotation", true).Value, false));
						interactionPoint.Name = xmlnode4.GetNode("Name", true).Value;
						interactionPoint.Animation = FurnitureLoader.GetNodeValue<Actor.AnimationStates>(xmlnode4, "Animation", Actor.AnimationStates.Idle);
						interactionPoint.subAnimation = FurnitureLoader.GetNodeValue<int>(xmlnode4, "SubAnimation", 0);
						interactionPoint.MinimumNeeded = FurnitureLoader.GetNodeValue<int>(xmlnode4, "MinimumNeeded", 1);
						interactionPoint.NeedsReachCheck = FurnitureLoader.GetNodeValue<bool>(xmlnode4, "ReachCheck", true);
						interactionPoint.Parent = furn;
						array2[num3] = FurnitureLoader.GetNodeValue<int>(xmlnode4, "Child", -1);
						list2.Add(interactionPoint);
						num3++;
					}
					furn.InteractionPoints = list2.ToArray();
					for (int j = 0; j < furn.InteractionPoints.Length; j++)
					{
						if (array2[j] > -1)
						{
							furn.InteractionPoints[j].Child = furn.InteractionPoints[array2[j]];
						}
						furn.InteractionPoints[j].Id = j;
					}
				}
				catch (Exception ex2)
				{
					output.AppendLine("\tFailed loading interaction points with error:");
					output.AppendLine("\t" + ex2.Message);
					success = false;
					return gameObject;
				}
			}
			else if (xmlnode.Name.Equals("SnapPoints"))
			{
				try
				{
					for (int k = 0; k < furn.SnapPoints.Length; k++)
					{
						UnityEngine.Object.Destroy(furn.SnapPoints[k].gameObject);
					}
					List<SnapPoint> list3 = new List<SnapPoint>();
					int[][] array3 = new int[xmlnode.Children.Count][];
					int num4 = 0;
					foreach (XMLParser.XMLNode xmlnode5 in xmlnode.Children)
					{
						GameObject gameObject5 = new GameObject(FurnitureLoader.GetNodeValue<string>(xmlnode5, "ComponentName", "SnapPoint"));
						if (array != null)
						{
							int num5 = Convert.ToInt32(FurnitureLoader.GetNodeValue<string>(xmlnode5, "MeshParent", "-1"));
							gameObject5.transform.SetParent((num5 < 0) ? gameObject.transform : array[num5].transform);
						}
						else
						{
							gameObject5.transform.SetParent(gameObject.transform);
						}
						SnapPoint snapPoint = gameObject5.AddComponent<SnapPoint>();
						snapPoint.transform.localPosition = SVector3.Deserialize(xmlnode5.GetNode("Position", true).Value, false);
						snapPoint.transform.localRotation = Quaternion.Euler(SVector3.Deserialize(xmlnode5.GetNode("Rotation", true).Value, false));
						snapPoint.Name = xmlnode5.GetNode("Name", true).Value;
						snapPoint.CheckValid = FurnitureLoader.GetNodeValue<bool>(xmlnode5, "CheckValid", true);
						snapPoint.Parent = furn;
						XMLParser.XMLNode node = xmlnode5.GetNode("Links", false);
						int[][] array4 = array3;
						int num6 = num4;
						int[] array5;
						if (node != null)
						{
							IList<string> arr = node.Value.Split(new char[]
							{
								','
							});
							if (FurnitureLoader.<>f__mg$cache0 == null)
							{
								FurnitureLoader.<>f__mg$cache0 = new Func<string, int>(Convert.ToInt32);
							}
							array5 = arr.SelectInPlace(FurnitureLoader.<>f__mg$cache0);
						}
						else
						{
							array5 = new int[0];
						}
						array4[num6] = array5;
						list3.Add(snapPoint);
						num4++;
					}
					furn.SnapPoints = list3.ToArray();
					for (int l = 0; l < furn.SnapPoints.Length; l++)
					{
						furn.SnapPoints[l].InitLinks = array3[l].SelectInPlace((int x) => furn.SnapPoints[x]);
						furn.SnapPoints[l].Id = l;
					}
				}
				catch (Exception ex3)
				{
					output.AppendLine("\tFailed loading snap points with error:");
					output.AppendLine("\t" + ex3.Message);
					success = false;
					return gameObject;
				}
			}
			else
			{
				string text2 = xmlnode.TryGetAttribute("Namespace", null);
				string text3 = xmlnode.TryGetAttribute("Assembly", null);
				Type type;
				if (text2 != null && text3 != null)
				{
					type = Type.GetType(text2 + xmlnode.Name + ", " + text3);
				}
				else
				{
					Type type2;
					if ((type2 = Type.GetType(xmlnode.Name)) == null)
					{
						type2 = (Type.GetType(xmlnode.Name + ", Assembly-CSharp") ?? Type.GetType("UnityEngine." + xmlnode.Name + ", UnityEngine"));
					}
					type = type2;
				}
				if (type != null)
				{
					if (xmlnode.TryGetAttribute("Destroy", null) != null)
					{
						UnityEngine.Component component = gameObject.GetComponent(type);
						if (component != null && component != furn)
						{
							UnityEngine.Object.DestroyImmediate(component);
						}
					}
					else
					{
						string pRef = xmlnode.TryGetAttribute("Parent", null);
						GameObject gameObject6;
						if (pRef != null)
						{
							Transform transform2 = gameObject.GetComponentsInChildren<Transform>().FirstOrDefault((Transform x) => x.name.Equals(pRef));
							if (!(transform2 != null))
							{
								output.AppendLine("\tFailed finding parent gameobject " + pRef + ":");
								error = true;
								continue;
							}
							gameObject6 = transform2.gameObject;
						}
						else
						{
							gameObject6 = gameObject;
						}
						UnityEngine.Component component2 = gameObject6.GetComponent(type);
						if (component2 == null)
						{
							component2 = gameObject6.AddComponent(type);
						}
						if (component2 != null)
						{
							foreach (XMLParser.XMLNode xmlnode6 in xmlnode.Children)
							{
								FieldInfo field = type.GetField(xmlnode6.Name);
								if (field != null)
								{
									try
									{
										object value2 = FurnitureLoader.ConvertValue(field.FieldType, xmlnode6.Value, gameObject, materials);
										field.SetValue(component2, value2);
									}
									catch (Exception ex4)
									{
										output.AppendLine("\tFailed setting field " + xmlnode6.Name + ":");
										output.AppendLine("\t" + ex4.Message);
										error = true;
									}
								}
								else
								{
									PropertyInfo property = type.GetProperty(xmlnode6.Name);
									if (property != null)
									{
										try
										{
											object value3 = FurnitureLoader.ConvertValue(property.PropertyType, xmlnode6.Value, gameObject, materials);
											property.SetValue(component2, value3, null);
										}
										catch (Exception ex5)
										{
											output.AppendLine("\tFailed setting property " + xmlnode6.Name + ":");
											output.AppendLine("\t" + ex5.Message);
											error = true;
										}
									}
									else
									{
										output.AppendLine("\tUndefined variable " + xmlnode6.Name);
										error = true;
									}
								}
							}
						}
						else
						{
							output.AppendLine("\tCouldn't create type " + xmlnode.Name);
							error = true;
						}
					}
				}
				else
				{
					output.AppendLine("\tUndefined type " + xmlnode.Name);
					error = true;
				}
			}
		}
		if (array.Length == 0)
		{
			output.AppendLine("\tFurniture needs at least one mesh");
			success = false;
			return gameObject;
		}
		furn.UseStandardMat = (furn.Colorable.Count > 0);
		Vector2[] array6 = null;
		if (flag)
		{
			array6 = furn.CalculateBoundary().ToArray();
			Vector3 vector = new Vector3(10f, 2f, 10f);
			Vector3 vector2 = new Vector3(-10f, 0f, -10f);
			foreach (Vector3 v in gameObject.GetComponentsInChildren<MeshFilter>().SelectMany((MeshFilter x) => from y in x.mesh.vertices
			select x.transform.localToWorldMatrix.MultiplyPoint(y)))
			{
				vector = Utilities.MinVector(vector, v);
				vector2 = Utilities.MaxVector(vector2, v);
			}
			furn.Height1 = Mathf.Clamp(vector.y, 0f, 2f);
			furn.Height2 = Mathf.Clamp(vector2.y, 0f, 2f);
			BoxCollider component3 = gameObject.GetComponent<BoxCollider>();
			if (component3 != null)
			{
				component3.center = (vector2 + vector) * 0.5f;
				component3.size = vector2 - vector;
			}
			furn.NavBoundary = new Vector2[0];
			furn.BuildBoundary = new Vector2[0];
			if (furn.WallFurn)
			{
				furn.YOffset = (furn.Height1 + furn.Height2) * 0.5f;
				furn.WallWidth = vector2.x - vector.x;
			}
			int num7 = Mathf.RoundToInt(vector2.x - vector.x);
			int num8 = Mathf.RoundToInt(vector2.z - vector.z);
			furn.OnXEdge = (num7 != 0 && num7 % 2 == 0);
			furn.OnYEdge = (num8 != 0 && num8 % 2 == 0);
			if (Utilities.PolygonArea(array6.ToList<Vector2>()) > 0.05f)
			{
				if (furn.Height1 < 0.2f)
				{
					furn.NavBoundary = array6;
				}
				furn.BuildBoundary = new Vector2[array6.Length];
				for (int m = 0; m < array6.Length; m++)
				{
					Vector2 first = array6[(m != 0) ? (m - 1) : (array6.Length - 1)];
					Vector2 second = array6[m];
					Vector2 third = array6[(m + 1) % array6.Length];
					furn.BuildBoundary[m] = Utilities.GetOffset(first, second, third, 0.01f, false);
				}
			}
		}
		if (!furn.WallFurn && furn.BuildBoundary != null && furn.BuildBoundary.Length > 0)
		{
			if (array6 == null)
			{
				array6 = furn.CalculateBoundary().ToArray();
			}
			furn.MeshBoundary = array6;
		}
		furn.upg = furn.GetComponent<Upgradable>();
		furn.HasUpg = (furn.upg != null);
		success = true;
		return gameObject;
	}

	private static T GetNodeValue<T>(XMLParser.XMLNode node, string subName, T def)
	{
		XMLParser.XMLNode node2 = node.GetNode(subName, false);
		if (node2 != null)
		{
			return (T)((object)TypeDescriptor.GetConverter(typeof(T)).ConvertFrom(node2.Value));
		}
		return def;
	}

	private static object ConvertValue(Type type, string value, GameObject o, Dictionary<string, Material> materials)
	{
		if (type.IsClass && string.IsNullOrEmpty(value))
		{
			return null;
		}
		if (type.IsSubclassOf(typeof(UnityEngine.Component)))
		{
			if (value.ToLower().Equals("self"))
			{
				return o.GetComponent(type);
			}
			Transform transform = o.GetComponentsInChildren<Transform>().FirstOrDefault((Transform x) => x.name.Equals(value));
			return (!(transform != null)) ? null : transform.GetComponent(type);
		}
		else if (type == typeof(GameObject))
		{
			if (value.ToLower().Equals("self"))
			{
				return o;
			}
			Transform transform2 = o.GetComponentsInChildren<Transform>().FirstOrDefault((Transform x) => x.name.Equals(value));
			return (!(transform2 != null)) ? null : transform2.gameObject;
		}
		else
		{
			if (type == typeof(Material))
			{
				return materials[value];
			}
			if (type.IsArray)
			{
				string[] array = value.Split(new string[]
				{
					Environment.NewLine,
					"\r\n",
					"\n"
				}, StringSplitOptions.RemoveEmptyEntries);
				Type elementType = type.GetElementType();
				Array array2 = Array.CreateInstance(elementType, array.Length);
				for (int i = 0; i < array.Length; i++)
				{
					array2.SetValue(FurnitureLoader.ConvertValue(elementType, array[i], o, materials), i);
				}
				return array2;
			}
			if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
			{
				string[] array3 = value.Split(new string[]
				{
					Environment.NewLine,
					"\r\n",
					"\n"
				}, StringSplitOptions.RemoveEmptyEntries);
				Type type2 = type.GetGenericArguments()[0];
				IList list = (IList)Activator.CreateInstance(type);
				for (int j = 0; j < array3.Length; j++)
				{
					list.Add(FurnitureLoader.ConvertValue(type2, array3[j], o, materials));
				}
				return list;
			}
			if (type == typeof(Color))
			{
				Color color;
				if (value[0] == '#' && ColorUtility.TryParseHtmlString(value, out color))
				{
					return color;
				}
				return SVector3.Deserialize(value, false).ToColor();
			}
			else
			{
				if (type == typeof(Vector3))
				{
					return SVector3.Deserialize(value, false).ToVector3();
				}
				if (type == typeof(Vector2))
				{
					return SVector3.Deserialize(value, false).ToVector2();
				}
				if (type == typeof(Quaternion))
				{
					return Quaternion.Euler(SVector3.Deserialize(value, false));
				}
				return TypeDescriptor.GetConverter(type).ConvertFrom(value);
			}
		}
	}

	private static bool Initialized = false;

	public static List<FurnitureMod> LoadedFurniture = new List<FurnitureMod>();

	[CompilerGenerated]
	private static Func<string, int> <>f__mg$cache0;
}
