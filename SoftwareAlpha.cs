﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class SoftwareAlpha : SoftwareWorkItem, IServerItem, IStockable
{
	public SoftwareAlpha(string name) : base(name)
	{
	}

	public SoftwareAlpha()
	{
	}

	public SoftwareAlpha(string name, string type, string category, Dictionary<string, uint> needs, string[] feat, uint[] os, float price, SDateTime start, Dictionary<string, float> designResult, float delay, float bugs, Company company, uint? sequelTo, bool inHouse, float loss, ContractWork contract, string server, string server2, int siblingIndex, SHashSet<uint> everWorked, float followers, uint maxFollowers, float followerChange, SDateTime? releaseDate, int maxBugs) : base(name, type, category, needs, os, price, start, company, sequelTo, feat, inHouse, loss, contract, server, server2, designResult, siblingIndex, followers, maxFollowers, followerChange, releaseDate, maxBugs)
	{
		this.Delay = delay;
		this.Bugs = bugs;
		if (this.CodeArtRatio == 1f)
		{
			this.ArtProgress = 1f;
		}
		else if (this.CodeArtRatio == 0f)
		{
			this.CodeProgress = 1f;
		}
		if (contract == null)
		{
			TutorialSystem.Instance.StartTutorial("Alpha work", false);
		}
		this.RegisterServer();
		this.SpecCodeArt = new Dictionary<string, float>();
		for (int i = 0; i < this.Specs.Length; i++)
		{
			float num = this.SpecDevTime[i, 0] + this.SpecDevTime[i, 1];
			float value = this.SpecDevTime[i, 0] / num;
			this.SpecCodeArt[this.Specs[i]] = value;
		}
		this.CodeCounterSpots = base.Type.GetOptimalEmployeeCount(this.DevTime, this.CodeArtRatio)[1];
		this.ArtCounterSpots = Mathf.CeilToInt((float)this.CodeCounterSpots * (1f - this.CodeArtRatio));
		this.CodeCounterSpots = Mathf.CeilToInt((float)this.CodeCounterSpots * this.CodeArtRatio);
		this.SpecTempQuality = new float[this.Specs.Length, 2, Mathf.Max(this.CodeCounterSpots, this.ArtCounterSpots)];
		this.SpecWeight = new float[this.Specs.Length, 2, Mathf.Max(this.CodeCounterSpots, this.ArtCounterSpots)];
		this.FinalSpecQuality = new float[this.Specs.Length, 2];
		for (int j = 0; j < this.Specs.Length; j++)
		{
			this.FinalSpecQuality[j, 0] = this.SpecQuality[j] * 0.5f;
			this.MaxDevDt = Mathf.Max(this.MaxDevDt, this.SpecDevTime[j, 0]);
			this.MaxArtDt = Mathf.Max(this.MaxArtDt, this.SpecDevTime[j, 1]);
		}
		SoftwareType type2 = base.Type;
		type2.CalculateLOCArt(this.Features, out this.LinesOfCode, out this.MBArt);
		if (contract == null)
		{
			this.ArtDTMult = Utilities.RandomRange(1f, 1.1f);
			this.CodeDTMult = Utilities.RandomRange(1f, 1.1f);
		}
		this.UpdateCodeArtTimes();
		this.EverWorked = everWorked;
	}

	public float CodeProgressLim
	{
		get
		{
			return Mathf.Min(1f, this.CodeProgress);
		}
	}

	public float ArtProgressLim
	{
		get
		{
			return Mathf.Min(1f, this.ArtProgress);
		}
	}

	public uint SoftwareID
	{
		get
		{
			uint? swid = this.SWID;
			return (swid == null) ? 0u : swid.Value;
		}
	}

	public uint PhysicalCopies { get; set; }

	public void AddReviewScore(float accuracy, float reviewsCompleted)
	{
		this.ReviewsDone++;
		this.ReviewScore += accuracy * reviewsCompleted * this.GetCodeArtProgress() * ((float)this.ReviewsDone / 8f);
	}

	private float GetReviewFactor()
	{
		return 1f - Mathf.Pow(0.25f, this.ReviewScore);
	}

	public float GetBugReviewFactor()
	{
		return this.GetReviewFactor() * 0.1f;
	}

	public float GetQualityFactor()
	{
		return Mathf.Pow(this.GetReviewFactor(), 2f) * 0.05f;
	}

	private void UpdateCodeArtTimes()
	{
		this.CodeDevTime = 0f;
		this.ArtDevTime = 0f;
		for (int i = 0; i < this.Specs.Length; i++)
		{
			this.CodeDevTime += this.SpecDevTime[i, 0];
			this.ArtDevTime += this.SpecDevTime[i, 1];
		}
		this.CodeDevTime *= this.CodeDTMult;
		this.ArtDevTime *= this.ArtDTMult;
	}

	public uint ForceID()
	{
		if (this.Mock != null)
		{
			this.SWID = new uint?(this.Mock.ID);
			return this.Mock.ID;
		}
		if (this.SWID == null)
		{
			this.SWID = new uint?(GameSettings.Instance.simulation.GetID());
		}
		return this.SWID.Value;
	}

	public SoftwareProduct CreateMock()
	{
		this.Mock = new SoftwareProduct(this.SoftwareName, this._type, this._category, this.Needs, this.OSs, 1f, 1f, 1f, 1f, this.Price, this.DevStart, this.DevStart, 0, this.InHouse, GameSettings.Instance.MyCompany, this.SequelTo, GameSettings.Instance.simulation.GetID(), 0f, this.Features, null, 0u, this);
		return this.Mock;
	}

	public void RegisterServer()
	{
		if (this.Server2 != null)
		{
			GameSettings.Instance.RegisterWithServer(this.Server2, this, true);
		}
	}

	public override string CurrentStage()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine((!this.InBeta) ? ((!this.InDelay) ? "Alpha" : "Delay").Loc() : "Beta");
		if (this.contract != null)
		{
			stringBuilder.AppendLine((this.ArtProgress != 0f || this.CodeArtRatio != 0f) ? this.contract.GetStatus(0f, this.DevStart, false) : "DesignWaitLabel".Loc());
		}
		else if (this.deal > 0u)
		{
			stringBuilder.AppendLine(base.ActiveDeal.GetStatus());
		}
		else
		{
			stringBuilder.AppendLine("FollowerAmount".Loc(new object[]
			{
				Mathf.RoundToInt(base.Followers).ToString("N0")
			}));
		}
		return stringBuilder.ToString();
	}

	public override string GetProgressLabel()
	{
		if (this.InBeta)
		{
			return (Mathf.FloorToInt(this.Bugs) != 0) ? "StillBugs".Loc(new object[]
			{
				(int)this.FixedBugs
			}) : "NoBugs".Loc();
		}
		if (this.InDelay)
		{
			return string.Empty;
		}
		StringBuilder stringBuilder = new StringBuilder();
		if (this.CodeArtRatio > 0f)
		{
			float num = this.CodeProgress * this.CodeDevTime;
			if (this.contract != null)
			{
				num = Mathf.Floor(num * 100f) / 100f;
				stringBuilder.AppendLine(string.Concat(new string[]
				{
					"Code".Loc(),
					": ",
					num.ToString("N2"),
					"/",
					this.contract.CodeUnits.ToString("N2")
				}));
			}
			else
			{
				stringBuilder.AppendLine("Code".Loc() + ": " + num.ToString("N2"));
			}
		}
		if (this.CodeArtRatio < 1f)
		{
			float num2 = this.ArtProgress * this.ArtDevTime;
			if (this.contract != null)
			{
				num2 = Mathf.Floor(num2 * 100f) / 100f;
				stringBuilder.AppendLine(string.Concat(new string[]
				{
					"Art".Loc(),
					": ",
					num2.ToString("N2"),
					"/",
					this.contract.ArtUnits.ToString("N2")
				}));
			}
			else
			{
				stringBuilder.AppendLine("Art".Loc() + ": " + num2.ToString("N2"));
			}
		}
		return stringBuilder.ToString();
	}

	public override Color BackColor
	{
		get
		{
			return new Color(0f, 0.75f, 0f);
		}
	}

	public override string GetIcon()
	{
		return "Software";
	}

	public float MaxQualityPercent(Team team, bool both = true, bool onlyArt = true, bool ratio = true)
	{
		if (team == null)
		{
			return 0f;
		}
		float num = 0f;
		float num2 = 0f;
		List<Actor> employeesDirect = team.GetEmployeesDirect();
		for (int i = 0; i < employeesDirect.Count; i++)
		{
			Employee employee = employeesDirect[i].employee;
			if ((both || onlyArt) && employee.IsRole(Employee.RoleBit.Artist))
			{
				num = Mathf.Max(num, employee.GetSkill(Employee.EmployeeRole.Artist));
			}
			if ((both || !onlyArt) && employee.IsRole(Employee.RoleBit.Programmer))
			{
				num2 = Mathf.Max(num2, employee.GetSkill(Employee.EmployeeRole.Programmer));
			}
		}
		float result;
		if (both)
		{
			result = num * (1f - this.CodeArtRatio) + num2 * this.CodeArtRatio;
		}
		else if (onlyArt)
		{
			result = num * ((!ratio) ? 1f : (1f - this.CodeArtRatio));
		}
		else
		{
			result = num2 * ((!ratio) ? 1f : this.CodeArtRatio);
		}
		return result;
	}

	public override float GetMax()
	{
		return 0f;
	}

	public override float GetProgress()
	{
		return 0f;
	}

	public float GetCodeArtProgress()
	{
		return this.CodeArtRatio * this.CodeProgressLim + (1f - this.CodeArtRatio) * this.ArtProgressLim;
	}

	public float GetQuality()
	{
		float num = 0f;
		float num2 = 0f;
		for (int i = 0; i < this.Specs.Length; i++)
		{
			num += Mathf.Min(this.FinalSpecQuality[i, 0], SoftwareAlpha.GetMaxCodeQuality(this.CodeProgress)) * this.SpecDevTime[i, 0] + this.FinalSpecQuality[i, 1] * this.SpecDevTime[i, 1];
			num2 += this.SpecDevTime[i, 0] + this.SpecDevTime[i, 1];
		}
		float quality = (num2 != 0f) ? (num / num2) : 0f;
		float progress = this.CodeArtRatio * this.CodeProgressLim + (1f - this.CodeArtRatio) * this.ArtProgressLim;
		return SoftwareAlpha.FinalQualityCalc(progress, quality);
	}

	public static float GetMaxCodeQuality(float progress)
	{
		if (progress <= 1f)
		{
			return 1f;
		}
		if (progress <= 2f)
		{
			return (2f - progress) * 0.75f + 0.25f;
		}
		return 1f / progress * 0.5f;
	}

	public float[] GetQualities()
	{
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		for (int i = 0; i < this.Specs.Length; i++)
		{
			num += Mathf.Min(this.FinalSpecQuality[i, 0], SoftwareAlpha.GetMaxCodeQuality(this.CodeProgress)) * this.SpecDevTime[i, 0];
			num2 += this.SpecDevTime[i, 0];
			num3 += this.FinalSpecQuality[i, 1] * this.SpecDevTime[i, 1];
			num4 += this.SpecDevTime[i, 1];
		}
		float num5 = 1f + this.GetQualityFactor();
		return new float[]
		{
			this.CodeProgressLim,
			this.ArtProgressLim,
			(num2 != 0f) ? Mathf.Min(1f, num5 * (num / num2)) : 0f,
			(num4 != 0f) ? Mathf.Min(1f, num5 * (num3 / num4)) : 0f
		};
	}

	public float GetLimitedQuality()
	{
		return this.GetQuality();
	}

	public float GetLimitedProgress(Team team)
	{
		return (!this.InBeta) ? this.GetCodeArtProgress() : ((Mathf.FloorToInt(this.Bugs) != 0) ? (this.FixedBugs / Mathf.Min(Mathf.Floor(this.Bugs), Mathf.Floor(this.MaxBugFix(team)))) : 1f);
	}

	public override WorkItem.HasWorkReturn HasWork(Actor actor)
	{
		if (this.AutoDev && !this.Enabled)
		{
			this.Working.Remove(actor.DID);
			return WorkItem.HasWorkReturn.Ignore;
		}
		if (!actor.employee.IsRole(Employee.RoleBit.Programmer | Employee.RoleBit.Artist))
		{
			this.Working.Remove(actor.DID);
			return WorkItem.HasWorkReturn.NotApplicable;
		}
		if (this.InBeta)
		{
			bool flag = actor.employee.IsRole(Employee.RoleBit.Programmer) && this.FixedBugs < Mathf.Floor(this.Bugs) && this.FixedBugs < Mathf.Floor(this.MaxBugFix(actor.GetTeam()));
			if (flag)
			{
				this.Working.Add(actor.DID);
			}
			else
			{
				this.Working.Remove(actor.DID);
			}
			return (!flag) ? WorkItem.HasWorkReturn.Finished : WorkItem.HasWorkReturn.True;
		}
		if (this.InDelay)
		{
			this.Working.Add(actor.DID);
			return WorkItem.HasWorkReturn.True;
		}
		bool flag2 = false;
		bool flag3 = actor.employee.IsRole(Employee.RoleBit.Programmer);
		bool flag4 = actor.employee.IsRole(Employee.RoleBit.Artist);
		if (flag3 && this.CodeArtRatio > 0f)
		{
			flag2 = true;
		}
		if (flag4 && this.CodeArtRatio < 1f)
		{
			flag2 = true;
		}
		if (flag2)
		{
			this.Working.Add(actor.DID);
		}
		else
		{
			this.Working.Remove(actor.DID);
		}
		if (flag2)
		{
			return (!this.CheckAdequateSpecLevel(actor)) ? WorkItem.HasWorkReturn.NotApplicable : WorkItem.HasWorkReturn.True;
		}
		return WorkItem.HasWorkReturn.Finished;
	}

	private float MaxBugFix(Team team)
	{
		float num = (this.contract != null || this.Bugs <= SoftwareAlpha.BugLimitFactor) ? 1f : 0.9f;
		if (team == null)
		{
			float input = 1f;
			if (this.DevTeams.Count > 0)
			{
				input = this.DevTeams.Max((string x) => this.MaxQualityPercent(GameSettings.Instance.sActorManager.Teams[x], false, false, false));
			}
			return this.Bugs * this.ContractBugFactor(input) * num;
		}
		return this.Bugs * this.ContractBugFactor(this.MaxQualityPercent(team, false, false, false)) * num;
	}

	public float ContractBugFactor(float input)
	{
		if (this.contract == null)
		{
			return input;
		}
		return Mathf.Lerp(input, 1f, 0.5f);
	}

	public void AddQuality(float effectiveness, float companySkill, bool realTime)
	{
		throw new NotImplementedException();
	}

	private float GetSpeedDamp(float prog)
	{
		return (prog >= 1f) ? 0.1f : (-1.35f * (prog * prog) + 1.45f);
	}

	public static float GetBugSpeedDamp(float prog)
	{
		return 1f - prog * prog + 0.01f;
	}

	public static float FinalQualityCalc(float codeProgress, float artProgress, float codeQuality, float artQuality, string type, IList<string> features)
	{
		return SoftwareAlpha.FinalQualityCalc(codeProgress, artProgress, codeQuality, artQuality, GameSettings.Instance.SoftwareTypes[type], features);
	}

	public static float FinalQualityCalc(float codeProgress, float artProgress, float codeQuality, float artQuality, SoftwareType type, IList<string> features)
	{
		return SoftwareAlpha.FinalQualityCalc(codeProgress, artProgress, codeQuality, artQuality, type.CodeArtRatio(features));
	}

	public static float FinalQualityCalc(float codeProgress, float artProgress, float codeQuality, float artQuality, float ratio)
	{
		float progress = ratio * Mathf.Clamp01(codeProgress) + (1f - ratio) * Mathf.Clamp01(artProgress);
		float quality = ratio * codeQuality + (1f - ratio) * artQuality;
		return SoftwareAlpha.FinalQualityCalc(progress, quality);
	}

	public static float FinalQualityCalc(float progress, float quality)
	{
		return progress * quality * (2f - progress + quality) / 2f;
	}

	private float ModEffectiveness(Actor ac, float eff)
	{
		float num = 1f;
		if (ac.employee.IsRole(Employee.RoleBit.Lead))
		{
			num = ac.GetPCAddonBonus(Employee.EmployeeRole.Lead);
		}
		else
		{
			bool flag = ac.employee.IsRole(Employee.RoleBit.Artist);
			bool flag2 = ac.employee.IsRole(Employee.RoleBit.Programmer);
			if (flag && flag2)
			{
				num = (ac.GetPCAddonBonus(Employee.EmployeeRole.Programmer) + ac.GetPCAddonBonus(Employee.EmployeeRole.Artist)) / 2f;
			}
			else if (flag)
			{
				num = ac.GetPCAddonBonus(Employee.EmployeeRole.Artist);
			}
			else if (flag2)
			{
				num = ac.GetPCAddonBonus(Employee.EmployeeRole.Programmer);
			}
		}
		return eff * num * SoftwareAlpha.DifficultySpeed[GameSettings.Instance.Difficulty];
	}

	private bool CheckAdequateSpecLevel(Actor act)
	{
		Team team = act.GetTeam();
		if (team == null)
		{
			return true;
		}
		float specializationMinCap = team.SpecializationMinCap;
		if (specializationMinCap == 0f)
		{
			return true;
		}
		for (int i = 0; i < 2; i++)
		{
			Employee.EmployeeRole role = (i != 0) ? Employee.EmployeeRole.Artist : Employee.EmployeeRole.Programmer;
			if (act.employee.IsRole(role))
			{
				for (int j = 0; j < this.Specs.Length; j++)
				{
					if (this.SpecDevTime[j, i] > 0f && act.employee.GetSpecialization(role, this.Specs[j], false) >= specializationMinCap)
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public void UpdateSkill(Actor ac, float delta, float minimumSpec)
	{
		if (ac.employee.IsRole(Employee.RoleBit.Programmer))
		{
			ac.employee.AddToSpecializations(Employee.EmployeeRole.Programmer, this.SpecDevTime, this.Specs, delta, minimumSpec);
		}
		if (ac.employee.IsRole(Employee.RoleBit.Artist))
		{
			ac.employee.AddToSpecializations(Employee.EmployeeRole.Artist, this.SpecDevTime, this.Specs, delta, minimumSpec);
		}
	}

	public override void DoWork(Actor actor, float effectiveness, float delta)
	{
		effectiveness = this.ModEffectiveness(actor, effectiveness);
		this.EverWorked.Add(actor.DID);
		Team team = actor.GetTeam();
		if (team == null || float.IsNaN(effectiveness) || float.IsInfinity(effectiveness) || effectiveness < 0f)
		{
			return;
		}
		if (this.ArtProgress == 0f && this.CodeArtRatio == 0f)
		{
			this.DevStart = new SDateTime(0, 0, TimeOfDay.Instance.Day, TimeOfDay.Instance.Month, TimeOfDay.Instance.Year);
		}
		float employeeCountEffect = SoftwareType.GetEmployeeCountEffect(Mathf.Max(1, this.Working.Count), this.DevTime, false);
		effectiveness *= employeeCountEffect;
		if (this.contract != null)
		{
			effectiveness *= 2f;
		}
		effectiveness *= 1f + this.SourceControlBoost * 0.1f;
		float num = (!actor.employee.IsRole(Employee.RoleBit.Lead)) ? 1f : 0.25f;
		if (this.InBeta)
		{
			float num2 = Mathf.Min(this.MaxBugFix(team), this.Bugs);
			if (num2 > 0f)
			{
				this.FixedBugs = Mathf.Clamp(this.FixedBugs + Utilities.PerHour(effectiveness, delta, true) * num * (4f / (float)GameSettings.DaysPerMonth) * SoftwareAlpha.GetBugSpeedDamp(this.FixedBugs / num2), 0f, num2);
			}
		}
		else if (this.InDelay)
		{
			this.Delay -= Utilities.PerHour(effectiveness * num, delta, true) / (float)GameSettings.DaysPerMonth;
			if (this.Delay <= 0f)
			{
				this.StartBeta(false);
			}
		}
		else
		{
			Team team2 = actor.GetTeam();
			float num3 = (team2 != null) ? team2.SpecializationMinCap : 0f;
			float num4 = 1f;
			float num5 = 1f;
			float skill = actor.employee.GetSkill(Employee.EmployeeRole.Programmer);
			float skill2 = actor.employee.GetSkill(Employee.EmployeeRole.Artist);
			if (actor.employee.IsRole(Employee.RoleBit.Artist) && actor.employee.IsRole(Employee.RoleBit.Programmer))
			{
				float num6 = skill + skill2;
				num4 = skill / num6;
				num5 = skill2 / num6;
			}
			float num7 = Utilities.PerHour(0.0714285746f, delta, true) * 2.2f;
			num7 /= (float)GameSettings.DaysPerMonth;
			float num8 = 1f - this.SourceControlBoost * 0.75f;
			float num9 = 1f / Mathf.Max(0.5f, effectiveness);
			for (int i = 0; i < 2; i++)
			{
				bool flag = i == 0;
				if (!flag || this.CodeArtRatio != 0f)
				{
					if (flag || this.CodeArtRatio != 1f)
					{
						Employee.EmployeeRole role = (!flag) ? Employee.EmployeeRole.Artist : Employee.EmployeeRole.Programmer;
						if (actor.employee.IsRole(role))
						{
							float num10 = (!flag) ? skill2 : skill;
							base.RecordSkill(role, num10, delta);
							float num11 = (!flag) ? num5 : num4;
							float num12 = num10.MapRange(0f, 1f, 0.9f, 1.1f, false);
							if (flag && this.CodeArtRatio > 0f)
							{
								float num13 = this.CodeProgress;
								this.CodeProgress = Mathf.Max(this.CodeProgress + 1f / (this.CodeArtRatio * this.DevTime) * effectiveness * num7 * num12 * this.GetSpeedDamp(this.CodeProgress) * num11, 0f);
								num13 = Mathf.Max(0f, this.CodeProgress - num13);
								float num14 = num13 * ((float)this.MaxBugs / 2f) * num10.MapRange(0f, 1f, 1f, 0.1f, false) * num8 * actor.employee.Diligence.MapRange(-1f, 1f, 2f, 1f, false);
								this.Bugs += num14;
							}
							if (!flag && this.CodeArtRatio < 1f)
							{
								this.ArtProgress = Mathf.Max(this.ArtProgress + 1f / ((1f - this.CodeArtRatio) * this.DevTime) * effectiveness * num7 * num12 * this.GetSpeedDamp(this.ArtProgress) * num11 * 0.8f, 0f);
								num12 = num10 * 0.8f;
								this.Delay += Mathf.Max(0f, num9 * num8 * num11 * Utilities.PerHour(1f - num12, delta, true) * 2f) / (float)GameSettings.DaysPerMonth;
							}
							if ((this.CodeArtRatio == 0f || this.CodeProgress >= 1.5f) && (this.CodeArtRatio == 1f || this.ArtProgress >= 1f))
							{
								this.HasFinished = true;
							}
							if (!flag || this.CodeProgress <= 1f)
							{
								float num15 = delta * num * effectiveness * num11;
								float num16 = 0f;
								for (int j = 0; j < this.Specs.Length; j++)
								{
									float num17 = this.SpecDevTime[j, i];
									if (num17 > 0f)
									{
										float specialization = actor.employee.GetSpecialization(role, this.Specs[j], true);
										if (specialization >= num3)
										{
											num16 = ((specialization <= num16) ? num16 : specialization);
											this.SpecTempQuality[j, i, this.CurrentCounter[i]] = specialization;
											this.SpecWeight[j, i, this.CurrentCounter[i]] = num15;
										}
										else
										{
											this.SpecTempQuality[j, i, this.CurrentCounter[i]] = 0f;
											this.SpecWeight[j, i, this.CurrentCounter[i]] = 0f;
										}
									}
								}
								if (num16 > 0f)
								{
									for (int k = 0; k < this.Specs.Length; k++)
									{
										float num18 = this.SpecDevTime[k, i];
										if (num18 > 0f)
										{
											float num19 = this.SpecTempQuality[k, i, this.CurrentCounter[i]];
											if (num19 >= num3)
											{
												num19 = Mathf.Max(0.01f, num19);
												this.SpecWeight[k, i, this.CurrentCounter[i]] *= num19 / num16;
											}
										}
									}
								}
								this.CurrentCounter[i]++;
								if (this.CurrentCounter[i] >= ((!flag) ? this.ArtCounterSpots : this.CodeCounterSpots))
								{
									this.CollectAverage(flag);
								}
							}
						}
					}
				}
			}
			this.UpdateSkill(actor, delta, num3);
		}
	}

	public override float GetWorkBoost(Employee.EmployeeRole role, float currentSkill)
	{
		float workBoost = base.GetWorkBoost(role, currentSkill);
		if (!this.InBeta && !this.InDelay)
		{
			return workBoost + this.Stability;
		}
		return workBoost;
	}

	public override Employee.EmployeeRole? GetBoostRole(Actor act)
	{
		bool flag = act.employee.IsRole(Employee.RoleBit.Artist);
		bool flag2 = act.employee.IsRole(Employee.RoleBit.Programmer);
		if (flag && flag2)
		{
			if (!this.InBeta)
			{
				return new Employee.EmployeeRole?((Utilities.RandomValue <= 0.5f) ? Employee.EmployeeRole.Artist : Employee.EmployeeRole.Programmer);
			}
			return new Employee.EmployeeRole?(Employee.EmployeeRole.Programmer);
		}
		else
		{
			if (flag)
			{
				return new Employee.EmployeeRole?(Employee.EmployeeRole.Artist);
			}
			if (flag2)
			{
				return new Employee.EmployeeRole?(Employee.EmployeeRole.Programmer);
			}
			return null;
		}
	}

	private void CollectAverage(bool code)
	{
		int num = (!code) ? 1 : 0;
		int num2 = (!code) ? this.ArtCounterSpots : this.CodeCounterSpots;
		float num3 = (!code) ? this.MaxArtDt : this.MaxDevDt;
		float num4 = (!code) ? (this.ArtProgress - this.LastArtProgress) : ((this.CodeProgress - this.LastCodeProgress) * 0.5f);
		if (code)
		{
			this.LastCodeProgress = this.CodeProgress;
		}
		else
		{
			this.LastArtProgress = this.ArtProgress;
		}
		for (int i = 0; i < this.Specs.Length; i++)
		{
			float num5 = this.SpecDevTime[i, num];
			if (num5 > 0f)
			{
				num5 /= num3;
				float num6 = 0f;
				float num7 = 0f;
				for (int j = 0; j < num2; j++)
				{
					float num8 = Mathf.Min(this.SpecTempQuality[i, num, j], this.MaxQual);
					float num9 = this.SpecWeight[i, num, j];
					num7 += num8 * num9;
					num6 += num8 * num9 * num8;
				}
				if (num7 > 0f)
				{
					float b = num6 / num7;
					this.FinalSpecQuality[i, num] = this.LerpTowards(this.FinalSpecQuality[i, num], b, num4 / num5);
				}
			}
		}
		this.CurrentCounter[num] = 0;
	}

	private void DoWarningCycle(bool code)
	{
		if (this.WarningQuality == null)
		{
			this.WarningQuality = new float[this.FinalSpecQuality.GetLength(0), this.FinalSpecQuality.GetLength(1)];
			for (int i = 0; i < this.FinalSpecQuality.GetLength(0); i++)
			{
				for (int j = 0; j < this.FinalSpecQuality.GetLength(1); j++)
				{
					this.WarningQuality[i, j] = this.FinalSpecQuality[i, j];
				}
			}
			return;
		}
		int num = (!code) ? 1 : 0;
		bool flag = false;
		if (code)
		{
			this.CodeWarningCycle++;
			if (this.CodeWarningCycle > SoftwareAlpha.WarningCycles)
			{
				this.CodeWarningCycle = 0;
				flag = true;
			}
		}
		else
		{
			this.ArtWarningCycle++;
			if (this.ArtWarningCycle > SoftwareAlpha.WarningCycles)
			{
				this.ArtWarningCycle = 0;
				flag = true;
			}
		}
		if (flag)
		{
			for (int k = 0; k < this.FinalSpecQuality.GetLength(0); k++)
			{
				if (Mathf.Abs(this.WarningQuality[k, num] - this.FinalSpecQuality[k, num]) > 0.01f)
				{
					if (this.FinalSpecQuality[k, num] < 0.95f && this.WarningQuality[k, num] > this.FinalSpecQuality[k, num] && !HUD.Instance.popupManager.PopupIDDict.ContainsKey(13))
					{
						Actor actor = this.FindLowestSkilled(code, this.Specs[k], this.FinalSpecQuality[k, num]);
						if (actor != null && actor.employee.GetSpecialization((!code) ? Employee.EmployeeRole.Artist : Employee.EmployeeRole.Programmer, this.Specs[k], false) < 0.75f)
						{
							string text = this.Specs[k].Loc() + " " + ((!code) ? "Art".Loc() : "Code".Loc()).ToLower();
							HUD.Instance.AddPopupMessage("ProjectQualityWarning".Loc(new object[]
							{
								this.SoftwareName,
								actor.employee.FullName,
								text
							}), "Exclamation", PopupManager.PopUpAction.GotoEmp, actor.DID, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.EmployeeWorkAssignment, 1);
						}
					}
					this.WarningQuality[k, num] = this.FinalSpecQuality[k, num];
				}
			}
		}
	}

	private Actor FindLowestSkilled(bool code, string spec, float val)
	{
		Actor result = null;
		float num = 1f;
		foreach (string key in this.DevTeams)
		{
			Team orNull = GameSettings.Instance.sActorManager.Teams.GetOrNull(key);
			if (orNull != null)
			{
				List<Actor> employeesDirect = orNull.GetEmployeesDirect();
				for (int i = 0; i < employeesDirect.Count; i++)
				{
					Actor actor = employeesDirect[i];
					if (this.Applicable(code, actor) && actor.isActiveAndEnabled && this.HasWork(actor) == WorkItem.HasWorkReturn.True)
					{
						float specialization = actor.employee.GetSpecialization((!code) ? Employee.EmployeeRole.Artist : Employee.EmployeeRole.Programmer, spec, false);
						if (specialization < num && specialization <= val + 0.05f)
						{
							result = actor;
							num = specialization;
						}
					}
				}
			}
		}
		return result;
	}

	private bool Applicable(bool code, Actor actor)
	{
		return actor.employee.IsRole((!code) ? Employee.RoleBit.Artist : Employee.RoleBit.Programmer);
	}

	private float LerpTowards(float a, float b, float t)
	{
		if (a == b)
		{
			return a;
		}
		if (b < a)
		{
			return Mathf.Max(b, a - t);
		}
		return Mathf.Min(b, a + t);
	}

	public override float GetWorkScore()
	{
		return (!this.InBeta) ? ((!this.InDelay) ? (this.GetCodeArtProgress() * 0.5f) : 0.5f) : (this.GetCodeArtProgress() * 0.5f + 0.5f);
	}

	public void UserPromote()
	{
		if (this.InBeta)
		{
			if (GameSettings.Instance.PressBuildQueue.Contains(this))
			{
				WindowManager.Instance.ShowMessageBox("PressBuildNotReleased".Loc(), true, DialogWindow.DialogType.Question, delegate
				{
					this.PressCheck();
				}, null, null);
			}
			else
			{
				this.PressCheck();
			}
		}
		else
		{
			this.PromoteAction();
		}
	}

	public void PressCheck()
	{
		if (GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>().Any((MarketingPlan x) => x.Type == MarketingPlan.TaskType.PressRelease && x.TargetItem == this))
		{
			WindowManager.Instance.ShowMessageBox("PressReleaseNotReleased".Loc(), true, DialogWindow.DialogType.Question, delegate
			{
				this.PromoteAction();
			}, null, null);
		}
		else
		{
			WindowManager.Instance.ShowMessageBox("ProductReleaseConfirmation".Loc(new object[]
			{
				this.SoftwareName
			}), true, DialogWindow.DialogType.Question, delegate
			{
				this.PromoteAction();
			}, "Release product", null);
		}
	}

	public override object PromoteAction()
	{
		if (this.InBeta)
		{
			if (base.Type.OSSpecific && this.OSs != null)
			{
				if ((from x in this.OSs
				select GameSettings.Instance.simulation.GetProduct(x, true)).Any((SoftwareProduct x) => x.IsMock && !x.MockSucceeded))
				{
					if (!this.AutoDev)
					{
						WindowManager.Instance.ShowMessageBox("MockOSError".Loc(), false, DialogWindow.DialogType.Error);
					}
					return null;
				}
			}
			GameSettings.Instance.DeregisterServerItem(this);
			if (base.ActiveDeal != null && base.ActiveDeal.Incoming)
			{
				this.Kill(false);
				return null;
			}
			this.Features = this.Features.ToArray<string>();
			if (this.contract == null)
			{
				float[] qualities = this.GetQualities();
				string softwareName = this.SoftwareName;
				string type = this._type;
				string category = this._category;
				Dictionary<string, uint> needs = this.Needs;
				uint[] oss = this.OSs;
				float codeProgress = qualities[0];
				float artProgress = qualities[1];
				float codeQuality = qualities[2];
				float artQuality = qualities[3];
				float price = this.Price;
				SDateTime devStart = this.DevStart;
				SDateTime release = SDateTime.Now();
				int bugs = (int)Mathf.Max(0f, this.Bugs - this.FixedBugs);
				bool inHouse = this.InHouse;
				Company myCompany = this.MyCompany;
				uint? sequelTo = this.SequelTo;
				uint id;
				if (this.Mock != null)
				{
					id = this.Mock.ID;
				}
				else
				{
					uint? swid = this.SWID;
					id = ((swid == null) ? GameSettings.Instance.simulation.GetID() : swid.Value);
				}
				SoftwareProduct softwareProduct = new SoftwareProduct(softwareName, type, category, needs, oss, codeProgress, artProgress, codeQuality, artQuality, price, devStart, release, bugs, inHouse, myCompany, sequelTo, id, this.Loss, this.Features, this.Server, (uint)Math.Floor((double)base.Followers), null);
				softwareProduct.PhysicalCopies += this.PhysicalCopies;
				GameSettings.Instance.MyCompany.Products.Add(softwareProduct);
				if (this.Mock != null)
				{
					GameSettings.Instance.simulation.UpgradeFromMock(softwareProduct);
				}
				else
				{
					GameSettings.Instance.simulation.AddProduct(softwareProduct, false);
				}
				GameSettings.Instance.simulation.ClearStockable(this.SoftwareID);
				HUD.Instance.distributionWindow.RefreshOrders();
				HUD.Instance.ApplyProductWindowFilters();
				SupportWork supportWork = new SupportWork(softwareProduct.ID, softwareProduct.Name, (!(this.guiItem == null)) ? this.guiItem.transform.GetSiblingIndex() : -1);
				if (!supportWork.Done)
				{
					if (!this.AutoDev)
					{
						foreach (string key in GameSettings.Instance.GetDefaultTeams("Support", this.DevTeams))
						{
							Team orNull = GameSettings.Instance.sActorManager.Teams.GetOrNull(key);
							if (orNull != null)
							{
								supportWork.AddDevTeam(orNull, false);
							}
						}
					}
					supportWork.Collapsed = this.Collapsed;
					GameSettings.Instance.MyCompany.WorkItems.Add(supportWork);
				}
				if (!this.AutoDev && !softwareProduct.InHouse)
				{
					if (Options.AskMarketing)
					{
						this.AskMarketing(softwareProduct);
					}
					else if (Options.AskPrinting)
					{
						this.AskPrinting(softwareProduct);
					}
				}
				this.Kill(false);
				return new object[]
				{
					supportWork,
					softwareProduct
				};
			}
			float[] qualities2 = this.GetQualities();
			float quality = qualities2[2] * this.CodeArtRatio + qualities2[3] * (1f - this.CodeArtRatio);
			float num = 0f;
			if (this.contract.CodeUnits > 0f)
			{
				num += this.CodeProgress * this.CodeDevTime / this.contract.CodeUnits * this.CodeArtRatio;
			}
			if (this.contract.ArtUnits > 0f)
			{
				num += this.ArtProgress * this.ArtDevTime / this.contract.ArtUnits * (1f - this.CodeArtRatio);
			}
			int daysFlat = Utilities.GetDaysFlat(this.DevStart, SDateTime.Now());
			int bugs2 = (int)Mathf.Clamp(this.Bugs - this.FixedBugs, 0f, this.DevTime * SoftwareAlpha.BugLimitFactor);
			ContractResult item = new ContractResult(this.contract, false, bugs2, quality, daysFlat, num);
			HUD.Instance.contractWindow.ContractResults.Items.Add(item);
			this.Kill(false);
		}
		else if (this.InDelay)
		{
			if (!this.AutoDev && this.Delay > 0f)
			{
				GameSettings.ForcePause = true;
				DialogWindow dialog = WindowManager.SpawnDialog();
				dialog.Show("PrematureDelayWarning".Loc(new object[]
				{
					base.Name
				}), false, DialogWindow.DialogType.Warning, new KeyValuePair<string, Action>[]
				{
					new KeyValuePair<string, Action>("Yes", delegate
					{
						this.StartBeta(true);
						GameSettings.ForcePause = false;
						dialog.Window.Close();
					}),
					new KeyValuePair<string, Action>("No", delegate
					{
						GameSettings.ForcePause = false;
						dialog.Window.Close();
					})
				});
			}
			else
			{
				this.StartBeta(true);
			}
			if (!this.AutoDev && this.contract == null && base.ActiveDeal == null)
			{
				HintController.Instance.Show(HintController.Hints.HintProductRelease);
			}
		}
		else if (this.GetCodeArtProgress() > 0f)
		{
			if (!this.AutoDev)
			{
				WindowManager.Instance.ShowMessageBox("AlphaPromoteConfirmation".Loc(), true, DialogWindow.DialogType.Question, delegate
				{
					this.Delay = Mathf.Clamp(this.Delay, 0f, 1f);
					this.InDelay = true;
				}, "Promote from alpha", null);
			}
			else
			{
				this.Delay = Mathf.Clamp(this.Delay, 0f, 1f);
				this.InDelay = true;
			}
		}
		else if (!this.AutoDev)
		{
			WindowManager.Instance.ShowMessageBox("AlphaNoProgress".Loc(), false, DialogWindow.DialogType.Error);
		}
		return null;
	}

	private void StartBeta(bool withPenalty)
	{
		if (withPenalty)
		{
			this.Bugs *= 10f;
			this.Bugs += this.Delay * this.DevTime * 5f;
			this.Bugs = Mathf.Clamp(this.Bugs, 0f, this.DevTime * SoftwareAlpha.BugLimitFactor);
		}
		this.Bugs *= 1f - this.GetBugReviewFactor();
		this.InBeta = true;
	}

	private void AskMarketing(SoftwareProduct result)
	{
		GameSettings.ForcePause = true;
		DialogWindow dialog = WindowManager.SpawnDialog();
		dialog.Show("ReleaseMarketingHint".Loc(new object[]
		{
			base.Name
		}), false, DialogWindow.DialogType.Question, new KeyValuePair<string, Action>[]
		{
			new KeyValuePair<string, Action>("Yes", delegate
			{
				HUD.Instance.marketingWindow.Window.OnClose = delegate
				{
					if (Options.AskPrinting)
					{
						this.AskPrinting(result);
					}
					HUD.Instance.marketingWindow.Window.OnClose = null;
				};
				HUD.Instance.marketingWindow.Show(result);
				GameSettings.ForcePause = false;
				dialog.Window.Close();
			}),
			new KeyValuePair<string, Action>("No", delegate
			{
				GameSettings.ForcePause = false;
				dialog.Window.Close();
				if (Options.AskPrinting)
				{
					this.AskPrinting(result);
				}
			}),
			new KeyValuePair<string, Action>("Don't ask again", delegate
			{
				GameSettings.ForcePause = false;
				Options.AskMarketing = false;
				dialog.Window.Close();
				if (Options.AskPrinting)
				{
					this.AskPrinting(result);
				}
			})
		});
	}

	private void AskPrinting(SoftwareProduct result)
	{
		GameSettings.ForcePause = true;
		DialogWindow dialog = WindowManager.SpawnDialog();
		dialog.Show("ReleasePrintingPrompt".Loc(new object[]
		{
			base.Name
		}), false, DialogWindow.DialogType.Question, new KeyValuePair<string, Action>[]
		{
			new KeyValuePair<string, Action>("Yes", delegate
			{
				HUD.Instance.copyOrderWindow.Show(result);
				GameSettings.ForcePause = false;
				dialog.Window.Close();
			}),
			new KeyValuePair<string, Action>("No", delegate
			{
				GameSettings.ForcePause = false;
				dialog.Window.Close();
			}),
			new KeyValuePair<string, Action>("Don't ask again", delegate
			{
				GameSettings.ForcePause = false;
				Options.AskPrinting = false;
				dialog.Window.Close();
			})
		});
	}

	public override float StressMultiplier()
	{
		return 1f;
	}

	public override void Kill(bool wasCancelled = false)
	{
		if (this.Mock != null && !this.Mock.MockSucceeded)
		{
			GameSettings.Instance.simulation.RemoveMock(this.Mock);
			foreach (SoftwareWorkItem softwareWorkItem in GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareWorkItem>().ToList<SoftwareWorkItem>())
			{
				if (softwareWorkItem.Type.OSSpecific && softwareWorkItem.OSs != null && softwareWorkItem.OSs.Contains(this.Mock.ID))
				{
					HashSet<uint> hashSet = softwareWorkItem.OSs.ToHashSet<uint>();
					hashSet.Remove(this.Mock.ID);
					if (hashSet.Count > 0)
					{
						softwareWorkItem.OSs = hashSet.ToArray<uint>();
					}
					else
					{
						softwareWorkItem.Kill(true);
					}
				}
			}
			foreach (SoftwarePort softwarePort in GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwarePort>().ToList<SoftwarePort>())
			{
				if (softwarePort.OSs.Contains(this.Mock.ID))
				{
					softwarePort.Kill(false);
				}
			}
		}
		List<MarketingPlan> list = (from x in GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>()
		where x.TargetItem == this
		select x).ToList<MarketingPlan>();
		for (int i = 0; i < list.Count; i++)
		{
			list[i].Kill(false);
		}
		if (HUD.Instance.marketingWindow.TargetWork == this)
		{
			HUD.Instance.marketingWindow.Window.Close();
		}
		GameSettings.Instance.PressBuildQueue.Remove(this);
		GameSettings.Instance.FollowerSimulation.Remove(this);
		base.FixAutoDev();
		GameSettings.Instance.DeregisterServerItem(this);
		base.Kill(wasCancelled);
	}

	protected override void Cancelled()
	{
		base.Cancelled();
		if (base.Followers > 0f && this.ReleaseDate != null)
		{
			GameSettings.Instance.MyCompany.AddFans(-Mathf.CeilToInt(base.Followers * 0.75f), this._type, this._category);
		}
		GameSettings.Instance.CancelPrintOrder(this.SoftwareID, false);
		GameSettings.Instance.simulation.ClearStockable(this.SoftwareID);
		HUD.Instance.distributionWindow.RefreshOrders();
		if (this.contract != null)
		{
			ContractResult item = new ContractResult(this.contract, true, 0, 0f, Utilities.GetDaysFlat(this.DevStart, SDateTime.Now()), 0f);
			HUD.Instance.contractWindow.ContractResults.Items.Add(item);
		}
		float codeArtProgress = this.GetCodeArtProgress();
		if (base.ActiveDeal != null && codeArtProgress == 0f)
		{
			HUD.Instance.dealWindow.CancelDeal(base.ActiveDeal, false);
			base.ActiveDeal = null;
		}
	}

	public float GetLoadRequirement()
	{
		float num = 0f;
		List<Team> devTeams = base.GetDevTeams();
		for (int i = 0; i < devTeams.Count; i++)
		{
			num += (float)devTeams[i].Count * (this.DevTime / 24f);
		}
		return num;
	}

	public void HandleLoad(float load)
	{
		this.SourceControlBoost = load;
	}

	public string GetDescription()
	{
		return base.Name;
	}

	public void SerializeServer(string name)
	{
		if (name == null)
		{
			this.SourceControlBoost = 0f;
		}
		this.Server2 = name;
	}

	public override string GetWorkTypeName()
	{
		return "Development";
	}

	public bool CancelOnUnload()
	{
		return true;
	}

	public override string HightlightButton()
	{
		if (this.InHouse || base.Followers != 0f || this.contract != null || this.deal != 0u)
		{
			return null;
		}
		if (GameSettings.Instance.PressBuildQueue.Contains(this))
		{
			return null;
		}
		foreach (WorkItem workItem in GameSettings.Instance.MyCompany.WorkItems)
		{
			MarketingPlan marketingPlan = workItem as MarketingPlan;
			if (marketingPlan != null && marketingPlan.Type == MarketingPlan.TaskType.PressRelease && marketingPlan.TargetItemID == this.ID)
			{
				return null;
			}
		}
		return "Market";
	}

	public override int EmitType(Actor actor)
	{
		bool flag = !this.InBeta && this.CodeArtRatio < 1f && actor.employee.IsRole(Employee.RoleBit.Artist);
		bool flag2 = this.CodeArtRatio > 0f && actor.employee.IsRole(Employee.RoleBit.Programmer);
		if (flag && flag2)
		{
			return UnityEngine.Random.Range(0, 2);
		}
		return (!flag) ? 0 : 1;
	}

	public string GetName()
	{
		return this.SoftwareName;
	}

	public void AddLoss(float cost)
	{
		this.Loss += cost;
	}

	public Company GetCompany()
	{
		return GameSettings.Instance.MyCompany;
	}

	public void CheckCompetency()
	{
		if (!this.InDelay && !this.InBeta && this.DevTeams.Count > 0)
		{
			List<Team> devTeams = base.GetDevTeams();
			for (int i = 0; i < 2; i++)
			{
				Employee.EmployeeRole role = (i != 0) ? Employee.EmployeeRole.Artist : Employee.EmployeeRole.Programmer;
				for (int j = 0; j < this.Specs.Length; j++)
				{
					if (this.SpecDevTime[j, i] > 0f)
					{
						bool flag = false;
						bool flag2 = false;
						for (int k = 0; k < devTeams.Count; k++)
						{
							List<Actor> employeesDirect = devTeams[k].GetEmployeesDirect();
							for (int l = 0; l < employeesDirect.Count; l++)
							{
								flag = true;
								Actor actor = employeesDirect[l];
								if (this.HasWork(actor) == WorkItem.HasWorkReturn.True && actor.employee.GetSpecialization(role, this.Specs[j], true) > 0.25f)
								{
									flag2 = true;
									break;
								}
							}
							if (flag2)
							{
								break;
							}
						}
						if (!flag)
						{
							return;
						}
						if (!flag2)
						{
							string text = this.Specs[j].Loc() + " " + ((i != 0) ? "Art".Loc() : "Code".Loc()).ToLower();
							HUD.Instance.AddPopupMessage("NoCompetencyAlpha".Loc(new object[]
							{
								this.SoftwareName,
								text
							}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.EmployeeWorkAssignment, 1);
							return;
						}
					}
				}
			}
		}
	}

	public static float BugLimitFactor = 100f;

	public static float[] DifficultySpeed = new float[]
	{
		1.1f,
		1.05f,
		1f
	};

	public float CodeProgress;

	public float ArtProgress;

	public float LastCodeProgress;

	public float LastArtProgress;

	public bool InBeta;

	public bool InDelay;

	public bool Released;

	public bool HasFinished;

	public float Bugs;

	public float FixedBugs;

	public SoftwareProduct Mock;

	private float SourceControlBoost;

	public float[,,] SpecTempQuality;

	public float[,,] SpecWeight;

	public float[,] FinalSpecQuality;

	public float MaxArtDt;

	public float MaxDevDt;

	public int[] CurrentCounter = new int[2];

	public int ArtCounterSpots;

	public int CodeCounterSpots;

	public Dictionary<string, float> SpecCodeArt = new Dictionary<string, float>();

	public int LinesOfCode;

	public float MBArt;

	public SHashSet<uint> EverWorked;

	public uint? SWID;

	public List<ReviewWindow.ReviewData> PastReviews = new List<ReviewWindow.ReviewData>();

	public float CodeDevTime;

	public float ArtDevTime;

	public float CodeDTMult = 1f;

	public float ArtDTMult = 1f;

	public int ReviewsDone;

	public float ReviewScore;

	private static int WarningCycles = 20;

	private int CodeWarningCycle;

	private int ArtWarningCycle;

	private float[,] WarningQuality;
}
