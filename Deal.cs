﻿using System;

[Serializable]
public abstract class Deal
{
	public Deal(Company company, bool request = false)
	{
		this.Request = request;
		if (this.Request)
		{
			this._bidder = company.ID;
		}
		else
		{
			this._client = company.ID;
		}
		this.ID = GameSettings.Instance.simulation.GetDealID();
	}

	public Deal()
	{
	}

	public bool Incoming
	{
		get
		{
			return this.Request || this.Client != GameSettings.Instance.MyCompany;
		}
	}

	public Company Client
	{
		get
		{
			return GameSettings.Instance.simulation.GetCompany(this._client);
		}
	}

	public Company Bidder
	{
		get
		{
			return GameSettings.Instance.simulation.GetCompany(this._bidder);
		}
	}

	public Company Company
	{
		get
		{
			return GameSettings.Instance.simulation.GetCompany(this._company);
		}
	}

	public string CompanyName
	{
		get
		{
			Company company = (!this.Request) ? this.Client : this.Bidder;
			return (company != null) ? company.Name : string.Empty;
		}
	}

	public abstract float Worth();

	public virtual bool Cancel()
	{
		if (!this.Active)
		{
			return false;
		}
		this.Active = false;
		return true;
	}

	public abstract float Payout();

	public abstract float ReputationEffect(bool ending);

	public abstract string ReputationCategory();

	public virtual void Accept(Company company)
	{
		this._company = company.ID;
		if (!this.CancelOnAccept())
		{
			this.Active = true;
		}
	}

	public abstract string Description();

	public abstract string Title();

	public abstract void HandleUpdate();

	public abstract string[] GetDetailedDescriptionVars();

	public abstract string[] GetDetailedDescriptionValues();

	public virtual bool StillValid(bool active)
	{
		return this.Client != null;
	}

	public virtual bool CancelOnAccept()
	{
		return false;
	}

	public bool Request;

	public bool Active;

	private readonly uint _client;

	private readonly uint _bidder;

	private uint _company;

	public readonly uint ID;
}
