﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActorStyler : MonoBehaviour
{
	private void Start()
	{
		if (ActorStyler.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		ActorStyler.Instance = this;
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		for (int i = 0; i < this.Items.Length; i++)
		{
			GameObject gameObject = this.Items[i];
			ActorBodyItem component = gameObject.GetComponent<ActorBodyItem>();
			this._itemDict.Add(component.Type.ToString() + component.Name, gameObject);
		}
		this._itemDict["Shadow"] = this.Shadow;
	}

	public ActorBodyItem InitShadow(IStylable act)
	{
		ActorBodyItem actorBodyItem = this.SetItem(act, "Shadow");
		act.BodyItems.Remove(actorBodyItem);
		return actorBodyItem;
	}

	public ActorBodyItem SetItem(IStylable act, string item)
	{
		GameObject gameObject = this._itemDict[item];
		ActorBodyItem component = gameObject.GetComponent<ActorBodyItem>();
		GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(gameObject);
		ActorBodyItem component2 = gameObject2.GetComponent<ActorBodyItem>();
		foreach (ActorBodyItem actorBodyItem in act.BodyItems.ToList<ActorBodyItem>())
		{
			if (component.Type == ActorBodyItem.BodyType.Accessory)
			{
				if (actorBodyItem.Type == ActorBodyItem.BodyType.Accessory && component.Category.Equals(actorBodyItem.Category))
				{
					component2.Load(actorBodyItem.Save());
					act.BodyItems.Remove(actorBodyItem);
					UnityEngine.Object.Destroy(actorBodyItem.gameObject);
				}
			}
			else if (actorBodyItem.Type == component.Type)
			{
				component2.Load(actorBodyItem.Save());
				act.BodyItems.Remove(actorBodyItem);
				UnityEngine.Object.Destroy(actorBodyItem.gameObject);
			}
		}
		this.RigItem(component, gameObject2, act);
		if (component.Type == ActorBodyItem.BodyType.Head)
		{
			act.UpdateEyes();
		}
		return component2;
	}

	public void RigItem(ActorBodyItem itb, GameObject bodyItem, IStylable actor)
	{
		Transform transform = actor.GetTransform();
		if (itb.IsFaceTexture)
		{
			ActorBodyItem actorBodyItem = actor.BodyItems.FirstOrDefault((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head);
			if (actorBodyItem != null)
			{
				actorBodyItem.rend.material.SetTexture("_OverlayTex", itb.FaceTexture);
			}
			bodyItem.transform.SetParent(transform);
		}
		else if (itb.IsRigged)
		{
			this.AttatchToBones(actor.GetTransform().gameObject, bodyItem);
		}
		else
		{
			Transform parent = transform.GetComponentsInChildren<Transform>(true).FirstOrDefault((Transform x) => x.name.Equals(itb.RootBone));
			bodyItem.transform.localScale = bodyItem.transform.localScale * transform.localScale.x;
			Vector3 position = bodyItem.transform.position;
			bodyItem.transform.SetParent(parent, true);
			bodyItem.transform.localPosition = ((!itb.KeepLocalPosition) ? Vector3.zero : position);
			bodyItem.transform.localRotation = Quaternion.Euler(0f, 270f, 180f);
		}
		actor.BodyItems.Add(bodyItem.GetComponent<ActorBodyItem>());
	}

	public ActorBodyItem.BodyItemObject[] GenerateStyle(bool female, string style)
	{
		ActorStyler.StyleGeneration styleGeneration = this.Styles.FirstOrDefault((ActorStyler.StyleGeneration x) => x.Key.Equals(style));
		styleGeneration = (styleGeneration ?? this.Styles[0]);
		List<ActorBodyItem.BodyItemObject> list = new List<ActorBodyItem.BodyItemObject>();
		ActorStyler.RandomGradient[] mgradients = styleGeneration.MGradients;
		ActorStyler.TreeNode[] first = (!female) ? styleGeneration.MItemTree : styleGeneration.FItemTree;
		Dictionary<string, float> cols = new Dictionary<string, float>();
		foreach (ActorStyler.RandomGradient randomGradient in mgradients)
		{
			if (!string.IsNullOrEmpty(randomGradient.DependOn))
			{
				float num = cols[randomGradient.DependOn] + (UnityEngine.Random.value - 0.5f) * randomGradient.DependScale;
				cols[randomGradient.Key] = ((!randomGradient.DependClamp) ? ((num >= 0f) ? (num % 1f) : (1f + num)) : Mathf.Clamp01(num));
			}
			else
			{
				cols[randomGradient.Key] = UnityEngine.Random.value;
			}
		}
		Dictionary<string, Color> colors = mgradients.ToDictionary((ActorStyler.RandomGradient x) => x.Key, (ActorStyler.RandomGradient x) => x.Gradient.Evaluate(cols[x.Key]));
		foreach (ActorStyler.TreeNode treeNode in first.Concat(styleGeneration.AItemTree))
		{
			ActorStyler.LeafNode leafNode = null;
			float num2;
			if (treeNode.MustPickOne)
			{
				num2 = treeNode.Leaves.SumSafe((ActorStyler.LeafNode x) => x.Chance);
			}
			else
			{
				num2 = 1f;
			}
			float num3 = num2;
			float num4 = UnityEngine.Random.value * num3;
			float num5 = 0f;
			for (int j = 0; j < treeNode.Leaves.Length; j++)
			{
				num5 += treeNode.Leaves[j].Chance;
				if (num4 < num5)
				{
					leafNode = treeNode.Leaves[j];
					break;
				}
			}
			if (leafNode != null)
			{
				ActorBodyItem.BodyItemObject item = leafNode.ActorItem.Save(leafNode.Colors.ToDictionary((ActorStyler.ColorMapping x) => x.Value, (ActorStyler.ColorMapping x) => colors[x.Key]));
				list.Add(item);
			}
		}
		return list.ToArray();
	}

	public void ApplySavedStyle(ActorBodyItem.BodyItemObject[] items, IStylable actor)
	{
		foreach (ActorBodyItem actorBodyItem in actor.BodyItems)
		{
			UnityEngine.Object.Destroy(actorBodyItem.gameObject);
		}
		actor.BodyItems.Clear();
		foreach (ActorBodyItem.BodyItemObject bodyItemObject in items)
		{
			ActorBodyItem actorBodyItem2 = this.SetItem(actor, bodyItemObject.Key);
			if (actorBodyItem2.LOD2Part != null && actorBodyItem2.LOD2Part.Length > 0)
			{
				for (int j = 0; j < actorBodyItem2.LOD2Part.Length; j++)
				{
					actor.SetLOD2Color(actorBodyItem2.LOD2Part[j], bodyItemObject.Colors[actorBodyItem2.LOD2ColorKey[j]]);
				}
			}
			if (actorBodyItem2.Type == ActorBodyItem.BodyType.Hair)
			{
				actor.UpdateHairColor(bodyItemObject.Colors["Color"]);
			}
			if (actorBodyItem2.Type == ActorBodyItem.BodyType.Head)
			{
				actor.UpdateSkinColor(bodyItemObject.Colors["Skin"]);
			}
			actorBodyItem2.Load(bodyItemObject);
		}
		actor.PostUpdate();
	}

	public void CopyStyle(IStylable src, IStylable actor)
	{
		foreach (ActorBodyItem actorBodyItem in actor.BodyItems.ToList<ActorBodyItem>())
		{
			UnityEngine.Object.Destroy(actorBodyItem.gameObject);
		}
		foreach (ActorBodyItem actorBodyItem2 in src.BodyItems)
		{
			ActorBodyItem.BodyItemObject bodyItemObject = actorBodyItem2.Save();
			ActorBodyItem actorBodyItem3 = this.SetItem(actor, bodyItemObject.Key);
			if (actorBodyItem3.LOD2Part != null && actorBodyItem3.LOD2Part.Length > 0)
			{
				for (int i = 0; i < actorBodyItem3.LOD2Part.Length; i++)
				{
					actor.SetLOD2Color(actorBodyItem3.LOD2Part[i], bodyItemObject.Colors[actorBodyItem3.LOD2ColorKey[i]]);
				}
			}
			if (actorBodyItem3.Type == ActorBodyItem.BodyType.Hair)
			{
				actor.UpdateHairColor(bodyItemObject.Colors["Color"]);
			}
			if (actorBodyItem3.Type == ActorBodyItem.BodyType.Head)
			{
				actor.UpdateSkinColor(bodyItemObject.Colors["Skin"]);
			}
			actorBodyItem3.Load(bodyItemObject);
		}
		actor.PostUpdate();
	}

	private void AttatchToBones(GameObject parent, GameObject child)
	{
		Vector3 localScale = child.transform.localScale;
		child.transform.localScale = child.transform.localScale * parent.transform.localScale.x;
		Transform[] array = (from x in child.GetComponentsInChildren<Transform>()
		where x != child.transform && x.GetComponent<SkinnedMeshRenderer>() == null
		select x).ToArray<Transform>();
		Transform[] componentsInChildren = parent.GetComponentsInChildren<Transform>();
		child.transform.parent = parent.transform;
		child.transform.localPosition = Vector3.zero;
		child.transform.localRotation = Quaternion.identity;
		child.transform.localScale = localScale;
		Transform[] array2 = array;
		for (int i = 0; i < array2.Length; i++)
		{
			Transform bone = array2[i];
			Transform transform = componentsInChildren.FirstOrDefault((Transform x) => x.name.Equals(bone.name));
			if (transform != null)
			{
				bone.parent = transform;
			}
			bone.localPosition = Vector3.zero;
			bone.localRotation = Quaternion.identity;
			bone.name = "Rigged" + bone.name;
		}
	}

	public ActorStyler.StyleGeneration[] Styles;

	public static ActorStyler Instance;

	public GameObject[] Items;

	public GameObject Shadow;

	private readonly Dictionary<string, GameObject> _itemDict = new Dictionary<string, GameObject>();

	[Serializable]
	public class StyleGeneration
	{
		public string Key;

		public ActorStyler.RandomGradient[] MGradients;

		public ActorStyler.TreeNode[] MItemTree;

		public ActorStyler.TreeNode[] FItemTree;

		public ActorStyler.TreeNode[] AItemTree;
	}

	[Serializable]
	public class RandomGradient
	{
		public string Key;

		public string DependOn;

		public float DependScale = 0.1f;

		public bool DependClamp = true;

		public Gradient Gradient;
	}

	[Serializable]
	public class TreeNode
	{
		public string Key;

		public bool MustPickOne;

		public ActorStyler.LeafNode[] Leaves;
	}

	[Serializable]
	public class LeafNode
	{
		public override string ToString()
		{
			return (!(this.ActorItem != null)) ? "MISSING OBJECT!" : this.ActorItem.Name;
		}

		public ActorBodyItem ActorItem;

		public float Chance;

		public ActorStyler.ColorMapping[] Colors;
	}

	[Serializable]
	public class ColorMapping
	{
		public override string ToString()
		{
			return this.Key + " -> " + this.Value;
		}

		public string Key;

		public string Value;
	}
}
