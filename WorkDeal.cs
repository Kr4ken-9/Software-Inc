﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class WorkDeal : Deal
{
	public WorkDeal(WorkItem work, float bid) : base(GameSettings.Instance.MyCompany, false)
	{
		this.WorkItem = work;
		this.worth = bid;
		this.StartDate = SDateTime.Now();
		this.Longevity = 0;
	}

	public WorkDeal(SimulatedCompany.ProductPrototype product, WorkDeal.WorkType type, Company company, SDateTime start, int longevity) : base(company, false)
	{
		this.Prototype = product;
		this.workType = type;
		this.worth = Utilities.RandomGauss(0.7f, 0.1f) * GameSettings.Instance.SoftwareTypes[product.Type].DevTime(product.Features, product.Category, null) * 1000f;
		this.StartDate = start;
		this.Longevity = Mathf.Max(1, longevity);
	}

	public WorkDeal(SoftwareProduct product, WorkDeal.WorkType type, SDateTime start, int longevity) : base(product.DevCompany, false)
	{
		this.Product = product;
		this.workType = type;
		if (type == WorkDeal.WorkType.Support)
		{
			this.worth = product.DevTime * 500f * Utilities.RandomGauss(0.7f, 0.1f);
		}
		else
		{
			this.worth = product.Quality * Utilities.GetMarketingEffort(product.DevTime) * Utilities.RandomRange(MarketingPlan.PostMarketingPrice + 10000f, MarketingPlan.PostMarketingPrice * 2f) * 0.05f;
		}
		this.StartDate = start;
		this.Longevity = Mathf.Max(1, longevity);
	}

	public WorkDeal()
	{
	}

	public override void Accept(Company company)
	{
		base.Accept(company);
		if (base.Incoming)
		{
			WorkItem workItem = this.GenerateWork();
			if (workItem != null)
			{
				GameSettings.Instance.MyCompany.WorkItems.Add(workItem);
				if (GameSettings.Instance.sActorManager.Teams.Count == 1)
				{
					workItem.AddDevTeam(GameSettings.Instance.sActorManager.Teams.Values.First<Team>(), false);
				}
				workItem.ActiveDeal = this;
			}
			this.WorkItem = workItem;
		}
	}

	public WorkItem GenerateWork()
	{
		switch (this.workType)
		{
		case WorkDeal.WorkType.Design:
		{
			SimulatedCompany.ProductPrototype prototype = this.Prototype;
			return new DesignDocument(prototype.Name, prototype.Type, prototype.Category, prototype.Needs, prototype.OSs, prototype.Price, prototype.DevStart, base.Client, prototype.SequelTo, prototype.InHouse, prototype.Loss, prototype.Features, null, null, null, false);
		}
		case WorkDeal.WorkType.Development:
		{
			SimulatedCompany.ProductPrototype prototype = this.Prototype;
			SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[prototype.Type];
			float stability = softwareType.GetBalance(prototype.Features)[1];
			Dictionary<string, float> specializationMonths = softwareType.GetSpecializationMonths(prototype.Features, prototype.Category, prototype.OSs, null);
			return new SoftwareAlpha(prototype.Name, prototype.Type, prototype.Category, prototype.Needs, prototype.Features, prototype.OSs, prototype.Price, prototype.DevStart, specializationMonths.ToDictionary((KeyValuePair<string, float> x) => x.Key, (KeyValuePair<string, float> x) => 1f), 0f, 0f, base.Client, prototype.SequelTo, prototype.InHouse, prototype.Loss, null, null, null, -1, new SHashSet<uint>(), 0f, 1u, 0f, null, SoftwareWorkItem.GetMaximumBugs(prototype.DevTime, stability));
		}
		case WorkDeal.WorkType.Support:
		{
			SoftwareProduct product = this.Product;
			if (product.Bugs == 0)
			{
				product.Bugs = Mathf.FloorToInt(Utilities.RandomGaussClamped(0.7f, 0.1f) * product.DevTime * 20f);
			}
			return new SupportWork(product.ID, product.Name, -1);
		}
		case WorkDeal.WorkType.Marketing:
		{
			SoftwareProduct product = this.Product;
			return new MarketingPlan(0f, product.ID, product.Name);
		}
		default:
			return null;
		}
	}

	public override bool Cancel()
	{
		if (!base.Cancel())
		{
			return false;
		}
		if (base.Incoming)
		{
			WorkItem workItem = this.WorkItem;
			if (workItem != null)
			{
				SimulatedCompany.ProductPrototype prototype = this.Prototype;
				if (prototype != null)
				{
					float t = Mathf.Clamp01(Utilities.GetMonths(prototype.DevStart, SDateTime.Now()) / prototype.DevTime);
					WorkDeal.WorkType workType = this.workType;
					if (workType != WorkDeal.WorkType.Design)
					{
						if (workType == WorkDeal.WorkType.Development)
						{
							SoftwareAlpha softwareAlpha = workItem as SoftwareAlpha;
							float quality = softwareAlpha.GetQuality();
							prototype.Quality = Mathf.Lerp(Mathf.Max(quality, prototype.Quality), quality, t);
						}
					}
					else
					{
						DesignDocument designDocument = workItem as DesignDocument;
						float workScore = designDocument.GetWorkScore();
						prototype.Quality = Mathf.Lerp(Mathf.Max(workScore, prototype.Quality), workScore, t);
					}
				}
				workItem.Kill(true);
			}
		}
		return true;
	}

	public override float Worth()
	{
		return this.worth / (float)GameSettings.DaysPerMonth;
	}

	private string GetName()
	{
		if (!base.Incoming)
		{
			return this.WorkItem.Name;
		}
		if (this.Product == null)
		{
			return (this.Prototype != null) ? this.Prototype.Name : string.Empty;
		}
		return this.Product.Name;
	}

	public string GetEndDate()
	{
		return (!base.Incoming) ? "Not applicable".Loc() : (this.StartDate + new SDateTime(this.Longevity, 0)).ToCompactString();
	}

	public string GetStatus()
	{
		return Utilities.Countdown(SDateTime.Now(), this.StartDate + new SDateTime(this.Longevity, 0));
	}

	private string GetSWType()
	{
		if (base.Incoming)
		{
			return (this.Product != null) ? this.Product._type : this.Prototype.Type;
		}
		WorkItem workItem = this.WorkItem;
		if (workItem is SoftwareWorkItem)
		{
			return (workItem as SoftwareWorkItem)._type;
		}
		if (workItem is MarketingPlan)
		{
			MarketingPlan marketingPlan = workItem as MarketingPlan;
			return (!marketingPlan.PreMarketing) ? marketingPlan.TargetProd._type : marketingPlan.TargetItem._type;
		}
		if (workItem is SupportWork)
		{
			return (workItem as SupportWork).TargetProduct._type;
		}
		return string.Empty;
	}

	private WorkDeal.WorkType GetWorkType()
	{
		WorkItem workItem = this.WorkItem;
		if (workItem != null)
		{
			if (workItem is DesignDocument)
			{
				return WorkDeal.WorkType.Design;
			}
			if (workItem is SoftwareAlpha)
			{
				return WorkDeal.WorkType.Development;
			}
			if (workItem is MarketingPlan)
			{
				return WorkDeal.WorkType.Marketing;
			}
			if (workItem is SupportWork)
			{
				return WorkDeal.WorkType.Support;
			}
		}
		return this.workType;
	}

	public override string Description()
	{
		return string.Format("WorkDeal".Loc(), this.GetName(), this.GetWorkType().ToString().Loc());
	}

	public override string Title()
	{
		return this.workType.ToString();
	}

	public override float Payout()
	{
		return this.Worth();
	}

	public override void HandleUpdate()
	{
		if (!base.Incoming)
		{
		}
	}

	private string SoftwareType()
	{
		if (base.Incoming)
		{
			if (this.Product != null)
			{
				return this.Product._type.LocSW();
			}
			if (this.Prototype != null)
			{
				return (this.Prototype != null) ? this.Prototype.Type.LocSW() : string.Empty.Loc();
			}
		}
		return string.Empty;
	}

	public override string[] GetDetailedDescriptionVars()
	{
		if (this.workType == WorkDeal.WorkType.Marketing)
		{
			return new string[]
			{
				"Type".Loc(),
				"Software".Loc(),
				(GameSettings.DaysPerMonth != 1) ? "Per day".Loc() : "Per month".Loc(),
				"Budget".Loc() + " " + ((GameSettings.DaysPerMonth != 1) ? "Per day".Loc() : "Per month".Loc()).ToLower(),
				"Expires".Loc()
			};
		}
		return new string[]
		{
			"Type".Loc(),
			"Software".Loc(),
			(GameSettings.DaysPerMonth != 1) ? "Per day".Loc() : "Per month".Loc(),
			"Expires".Loc()
		};
	}

	public override string[] GetDetailedDescriptionValues()
	{
		if (this.workType == WorkDeal.WorkType.Marketing)
		{
			return new string[]
			{
				this.GetWorkType().ToString().Loc(),
				this.SoftwareType(),
				this.Worth().Currency(true),
				(Utilities.GetMarketingEffort(this.Product.DevTime) * this.Product.Quality * 0.05f * MarketingPlan.PostMarketingPrice / (float)GameSettings.DaysPerMonth).Currency(true),
				this.GetEndDate()
			};
		}
		return new string[]
		{
			this.GetWorkType().ToString().Loc(),
			this.SoftwareType(),
			this.Worth().Currency(true),
			this.GetEndDate()
		};
	}

	public override bool StillValid(bool active)
	{
		return base.StillValid(active) && (!base.Incoming || ((this.Prototype == null || !this.Prototype.Released) && Utilities.GetMonths(this.StartDate, SDateTime.Now()) <= (float)this.Longevity));
	}

	public override float ReputationEffect(bool ending)
	{
		if (base.Incoming)
		{
			switch (this.workType)
			{
			case WorkDeal.WorkType.Design:
				if (ending)
				{
					DesignDocument designDocument = this.WorkItem as DesignDocument;
					if (designDocument == null)
					{
						return 0f;
					}
					float progress = designDocument.GetProgress();
					if (progress == 0f)
					{
						return 0f;
					}
					return (0.25f - Mathf.Abs(progress - 0.5f)) * 2f * (designDocument.DevTime / 48f);
				}
				break;
			case WorkDeal.WorkType.Development:
				if (ending)
				{
					SoftwareAlpha softwareAlpha = this.WorkItem as SoftwareAlpha;
					if (softwareAlpha == null)
					{
						return 0f;
					}
					return (softwareAlpha.GetLimitedQuality() * 2f - 1f) * 2f * (softwareAlpha.DevTime / 48f);
				}
				break;
			case WorkDeal.WorkType.Support:
				if (!ending)
				{
					SupportWork supportWork = this.WorkItem as SupportWork;
					if (supportWork == null)
					{
						return 0f;
					}
					int ticketDeal = supportWork.TicketDeal;
					supportWork.TicketDeal = 0;
					int num = Mathf.RoundToInt((float)supportWork.Tickets.Count * 0.05f);
					return Mathf.Min((float)(ticketDeal - num) / ((ticketDeal <= num) ? (500f / (float)num) : (100f * (float)num)) + ((ticketDeal <= num) ? 0f : 0.01f), 0.1f);
				}
				break;
			case WorkDeal.WorkType.Marketing:
				if (!ending)
				{
					MarketingPlan marketingPlan = this.WorkItem as MarketingPlan;
					if (marketingPlan == null)
					{
						return 0f;
					}
					SoftwareProduct product = this.Product;
					float num2 = Utilities.GetMarketingEffort(product.DevTime) * product.Quality * 0.05f / (float)GameSettings.DaysPerMonth;
					return Mathf.Min((marketingPlan.LastProgress - num2) * 0.25f, 0.1f);
				}
				break;
			}
		}
		return 0f;
	}

	public override string ReputationCategory()
	{
		return "Deal";
	}

	public WorkDeal.WorkType workType;

	public readonly float worth;

	public readonly SDateTime StartDate;

	public readonly int Longevity;

	public WorkItem WorkItem;

	public SimulatedCompany.ProductPrototype Prototype;

	public SoftwareProduct Product;

	public enum WorkType
	{
		Design,
		Development,
		Support,
		Marketing
	}
}
