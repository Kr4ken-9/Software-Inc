﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;

public class ModController : MonoBehaviour
{
	public static string ModFolder
	{
		get
		{
			return Path.Combine(Utilities.GetRoot(), "DLLMods");
		}
	}

	public bool HasMods()
	{
		return Directory.Exists(ModController.ModFolder) && Directory.GetFiles(ModController.ModFolder, "*.dll").Length > 0;
	}

	private void LoadMods()
	{
		foreach (string path in Directory.GetFiles(ModController.ModFolder, "*.dll"))
		{
			string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(path);
			try
			{
				Assembly assembly = Assembly.LoadFile(path);
				List<Type> list = new List<Type>();
				ModMeta modMeta = null;
				foreach (Type type in assembly.GetTypes())
				{
					if (modMeta == null && typeof(ModMeta).IsAssignableFrom(type))
					{
						ConstructorInfo constructor = type.GetConstructor(new Type[0]);
						if (constructor != null)
						{
							modMeta = (ModMeta)constructor.Invoke(null);
						}
					}
					if (type.IsSubclassOf(typeof(ModBehaviour)))
					{
						list.Add(type);
					}
				}
				if (modMeta != null)
				{
					if (list.Count > 0)
					{
						List<ModBehaviour> list2 = new List<ModBehaviour>();
						foreach (Type componentType in list)
						{
							ModBehaviour modBehaviour = base.gameObject.AddComponent(componentType) as ModBehaviour;
							modBehaviour.enabled = false;
							list2.Add(modBehaviour);
						}
						ModController.DLLMod dllmod = new ModController.DLLMod(modMeta, fileNameWithoutExtension, list2.ToArray());
						foreach (ModBehaviour modBehaviour2 in list2)
						{
							modBehaviour2.ParentMod = dllmod;
						}
						this.Mods.Add(dllmod);
					}
					else
					{
						Debug.Log("Error loading dll mod " + fileNameWithoutExtension + ": didn't have any behaviours and was not loaded");
					}
				}
				else
				{
					Debug.Log("Error loading dll mod " + fileNameWithoutExtension + ": didn't have any meta and was not loaded");
				}
			}
			catch (Exception ex)
			{
				Debug.Log("Error loading dll mod " + fileNameWithoutExtension + ":\n" + ex.ToString());
			}
		}
	}

	private void Start()
	{
		if (ModController.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		ModController.Instance = this;
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		if (this.HasMods())
		{
			if (!PlayerPrefs.HasKey("MODEULA"))
			{
				DialogWindow dialog = WindowManager.SpawnDialog();
				dialog.Show(ModController.ModEula, false, DialogWindow.DialogType.Information, new KeyValuePair<string, Action>[]
				{
					new KeyValuePair<string, Action>("I Agree", delegate
					{
						PlayerPrefs.SetInt("MODEULA", 1);
						PlayerPrefs.Save();
						this.LoadMods();
						dialog.Window.Close();
					}),
					new KeyValuePair<string, Action>("Decline", delegate
					{
						dialog.Window.Close();
					})
				});
			}
			else
			{
				this.LoadMods();
			}
		}
		FurnitureLoader.Init();
	}

	public static ModController Instance;

	[NonSerialized]
	public List<ModController.DLLMod> Mods = new List<ModController.DLLMod>();

	public static string ModEula = "The game has detected dll-based mods. You must agree to the following before any mods will be loaded:\r\n\r\nThe following agreement governs all download, installation or use of any assembly file (hereinafter \"mods\") that modifies the runtime behaviour of Software Inc. If you do not agree to this agreement, you may not install or use mods.\r\n\r\nYou acknowledge that downloading, installing or using mods may damage your computer and any data stored on it. Your installation or use of mods is at your own discretion and risk, you are solely responsible and COREDUMPING IVS is not liable for any loss of data or damage to your computer.";

	public class DLLMod
	{
		public DLLMod(ModMeta meta, string filename, ModBehaviour[] behaviours)
		{
			this.Meta = meta;
			this.FileName = filename;
			this.Behaviors = behaviours;
			this.Settings = new Dictionary<string, string>();
			this.Settings["Active"] = "False";
			try
			{
				string path = Path.Combine(ModController.ModFolder, this.FileName + "Setting.txt");
				if (File.Exists(path))
				{
					ConfigFile configFile = ConfigFile.Load(File.ReadAllLines(path));
					foreach (KeyValuePair<string, List<string>> keyValuePair in configFile.Values)
					{
						this.Settings[keyValuePair.Key] = keyValuePair.Value[0];
					}
				}
			}
			catch (Exception)
			{
			}
			if (this.Active)
			{
				this.Activate(true);
			}
		}

		public bool Active
		{
			get
			{
				bool result = false;
				string input;
				if (this.Settings.TryGetValue("Active", out input))
				{
					result = input.ConvertToBoolDef(false);
				}
				return result;
			}
		}

		public void SaveSetting(string name, string value)
		{
			if (!"Active".Equals(name))
			{
				this.Settings[name] = value;
				this.SaveSettings();
			}
		}

		public void Activate(bool active)
		{
			foreach (ModBehaviour modBehaviour in this.Behaviors)
			{
				if (active)
				{
					modBehaviour.OnActivate();
				}
				else
				{
					modBehaviour.OnDeactivate();
				}
				modBehaviour.enabled = active;
			}
			this.Settings["Active"] = active.ToString();
			this.SaveSettings();
			bool modded;
			if (!active)
			{
				modded = ModController.Instance.Mods.Any((ModController.DLLMod x) => x.Active);
			}
			else
			{
				modded = true;
			}
			ErrorLogging.Modded = modded;
		}

		private void SaveSettings()
		{
			try
			{
				ConfigFile configFile = ConfigFile.FromDictionary(this.Settings);
				string path = Path.Combine(ModController.ModFolder, this.FileName + "Setting.txt");
				File.WriteAllText(path, configFile.Serialize());
			}
			catch (Exception)
			{
			}
		}

		public ModMeta Meta;

		public string FileName;

		public Dictionary<string, string> Settings;

		public ModBehaviour[] Behaviors;
	}
}
