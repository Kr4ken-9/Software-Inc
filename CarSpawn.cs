﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CarSpawn : MonoBehaviour
{
	public void Reset()
	{
		this.Occupants.Clear();
		this.OpenAmount = 0f;
		this.DoorOpen = false;
		for (int i = 0; i < this.Doors.Length; i++)
		{
			this.Doors[i].DoorHinge.localRotation = Quaternion.Euler(0f, this.Doors[i].ClosedDegree, 0f);
		}
	}

	private void Update()
	{
		if (this.DoorOpen && this.OpenAmount < 1f)
		{
			this.DoorStart -= Time.deltaTime * GameSettings.GameSpeed * 2f;
			if (this.DoorStart < 0f)
			{
				this.OpenAmount = 1f;
			}
			else
			{
				this.OpenAmount = 1f - this.DoorStart;
			}
		}
		if (!this.DoorOpen && this.OpenAmount > 0f)
		{
			this.DoorStart -= Time.deltaTime * GameSettings.GameSpeed * 2f;
			if (this.DoorStart < 0f)
			{
				if (this.DoorCloseSfx != null)
				{
					this.Parent.PlaySFX(this.DoorCloseSfx);
				}
				this.OpenAmount = 0f;
			}
			else
			{
				this.OpenAmount = this.DoorStart;
			}
		}
		for (int i = 0; i < this.Doors.Length; i++)
		{
			this.Doors[i].DoorHinge.localRotation = Quaternion.Euler(0f, Mathf.Lerp(this.Doors[i].ClosedDegree, this.Doors[i].OpenDegree, this.OpenAmount), 0f);
		}
	}

	public void OpenDoor()
	{
		if (this.OpenAmount == 0f)
		{
			this.DoorStart = 1f;
			if (this.DoorOpenSfx != null)
			{
				this.Parent.PlaySFX(this.DoorOpenSfx);
			}
		}
		this.DoorOpen = true;
	}

	public void CloseDoor()
	{
		if (this.OpenAmount == 1f)
		{
			this.DoorStart = 1f;
		}
		this.DoorOpen = false;
	}

	public void BeginSpawn()
	{
		if (this.Occupants.Count > 0)
		{
			base.StartCoroutine(this.SpawnOccupants());
		}
	}

	public bool AnyActive()
	{
		foreach (Actor actor in this.Occupants)
		{
			if (actor != null && actor.isActiveAndEnabled)
			{
				return true;
			}
		}
		return false;
	}

	private IEnumerator SpawnOccupants()
	{
		this.isSpawning = true;
		foreach (Actor x in this.Occupants.ToList<Actor>())
		{
			if (x != null && !x.isActiveAndEnabled)
			{
				this.OpenDoor();
				if (this.PositionOffset)
				{
					x.transform.position = base.transform.position + base.transform.rotation * new Vector3(UnityEngine.Random.Range(this.MinOffset.x, this.MaxOffset.x), 0f, UnityEngine.Random.Range(this.MinOffset.y, this.MaxOffset.x));
					x.transform.rotation = base.transform.rotation * Quaternion.Euler(0f, UnityEngine.Random.Range(this.MinAngle, this.MaxAngle), 0f);
				}
				else
				{
					x.transform.position = base.transform.position;
					x.transform.rotation = base.transform.rotation;
				}
				x.enabled = true;
				x.SetVisible(true);
				x.anim.Play(CarSpawn.AnimationStates[this.SubAnimation], 0, 0f);
				x.MeetNow();
				if (this.WalkOut)
				{
					x.PathProg = 0f;
					x.CurrentPathNode = 0;
					x.CurrentPath = new List<Vector3>
					{
						x.transform.position,
						x.transform.position + base.transform.rotation * new Vector3(UnityEngine.Random.Range(-1f, 1f), 0f, UnityEngine.Random.Range(0f, 0.5f))
					};
				}
				while (GameSettings.GameSpeed == 0f)
				{
					yield return new WaitForSeconds(0.1f);
				}
				yield return new WaitForSeconds(UnityEngine.Random.Range(this.MinSpawnDelay, this.MaxSpawnDelay) / GameSettings.GameSpeed);
			}
		}
		if (this.AutoCloseDoor)
		{
			this.CloseDoor();
		}
		this.isSpawning = false;
		yield break;
	}

	public int ID;

	public CarSpawn.DoorController[] Doors;

	[NonSerialized]
	public HashSet<Actor> Occupants = new HashSet<Actor>();

	public int Capacity;

	public bool CanGoIn = true;

	public bool CanGoOut = true;

	private bool DoorOpen;

	public float OpenAmount;

	private float DoorStart;

	public int SubAnimation;

	public static string[] AnimationStates = new string[]
	{
		"CarOutRight",
		"CarOutLeft",
		"BusOut"
	};

	public static string[] AnimationInStates = new string[]
	{
		"CarInRight",
		"CarInLeft",
		"BusIn"
	};

	public bool AutoCloseDoor;

	public bool WalkOut;

	public bool isSpawning;

	public float MinSpawnDelay = 1f;

	public float MaxSpawnDelay = 2f;

	public bool PositionOffset;

	public Vector2 MinOffset;

	public Vector2 MaxOffset;

	public float MinAngle;

	public float MaxAngle;

	public AudioClip DoorOpenSfx;

	public AudioClip DoorCloseSfx;

	public CarScript Parent;

	[Serializable]
	public struct DoorController
	{
		public Transform DoorHinge;

		public float OpenDegree;

		public float ClosedDegree;
	}
}
