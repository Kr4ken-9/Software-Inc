﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public static class AudioManager
{
	static AudioManager()
	{
		AudioManager.MaxChannels = AudioSettings.GetConfiguration().numVirtualVoices;
		AudioManager.MasterMixer = Resources.Load<AudioMixer>("MasterMixer");
		AudioManager.Master = AudioManager.MasterMixer.FindMatchingGroups("Master")[0];
		AudioManager.UI = AudioManager.MasterMixer.FindMatchingGroups("Master/UI")[0];
		AudioManager.World = AudioManager.MasterMixer.FindMatchingGroups("Master/World")[0];
		AudioManager.InGame = AudioManager.MasterMixer.FindMatchingGroups("Master/World/In-game")[0];
		AudioManager.InGameNormal = AudioManager.MasterMixer.FindMatchingGroups("Master/World/In-game/Normal")[0];
		AudioManager.InGameHighPass = AudioManager.MasterMixer.FindMatchingGroups("Master/World/In-game/HighPass")[0];
		AudioManager.Environment = AudioManager.MasterMixer.FindMatchingGroups("Master/World/Environment")[0];
		AudioManager.Music = AudioManager.MasterMixer.FindMatchingGroups("Master/World/Music")[0];
		AudioManager.MixerMap = new Dictionary<string, AudioMixerGroup>
		{
			{
				"Master",
				AudioManager.Master
			},
			{
				"Music",
				AudioManager.Music
			},
			{
				"SFX",
				AudioManager.InGame
			},
			{
				"UI",
				AudioManager.UI
			},
			{
				"Environment",
				AudioManager.Environment
			}
		};
	}

	public static List<string> ToConfig()
	{
		List<string> list = new List<string>();
		foreach (KeyValuePair<string, AudioMixerGroup> keyValuePair in AudioManager.MixerMap)
		{
			list.Add(keyValuePair.Key + "=" + AudioManager.GetVolume(keyValuePair.Key));
		}
		return list;
	}

	public static void LoadConfig(List<string> values)
	{
		if (values == null)
		{
			return;
		}
		foreach (string text in values)
		{
			try
			{
				string[] array = text.Split(new char[]
				{
					'='
				});
				string key = array[0];
				float value = (float)Convert.ToDouble(array[1]);
				AudioManager.InitVolumes[key] = value;
			}
			catch (Exception exception)
			{
				Debug.LogException(exception);
			}
		}
	}

	public static float GetVolume(string map)
	{
		float result;
		AudioManager.MasterMixer.GetFloat(map + "Volume", out result);
		return result;
	}

	public static void SetVolume(string map, float volume)
	{
		AudioManager.MasterMixer.SetFloat(map + "Volume", volume);
		Options.SaveToFile();
	}

	public static int MaxChannels;

	public static int FurniturePlaying = 0;

	public static AudioMixer MasterMixer;

	public static AudioMixerGroup Master;

	public static AudioMixerGroup UI;

	public static AudioMixerGroup World;

	public static AudioMixerGroup InGame;

	public static AudioMixerGroup InGameNormal;

	public static AudioMixerGroup InGameHighPass;

	public static AudioMixerGroup Environment;

	public static AudioMixerGroup Music;

	public static Dictionary<string, AudioMixerGroup> MixerMap = new Dictionary<string, AudioMixerGroup>();

	public static Dictionary<string, float> InitVolumes = new Dictionary<string, float>();

	public static bool VolumeLoaded = false;
}
