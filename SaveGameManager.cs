﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveGameManager : MonoBehaviour
{
	public static string SaveFolder
	{
		get
		{
			return Path.Combine(Utilities.GetRoot(), "Saves");
		}
	}

	public static string BuildingFolder
	{
		get
		{
			return Path.Combine(Utilities.GetRoot(), "Buildings");
		}
	}

	public bool Visible
	{
		get
		{
			return this.SaveGameWindow.Shown;
		}
	}

	public static GameReader.LoadMode CurrentMode
	{
		get
		{
			return (!(GameSettings.Instance == null)) ? ((!GameSettings.Instance.EditMode) ? GameReader.LoadMode.Full : GameReader.LoadMode.Building) : GameReader.LoadMode.Full;
		}
	}

	public void AutoSave()
	{
		if (GameSettings.Instance != null)
		{
			if (GameSettings.Instance.AssociatedAutoSave != null)
			{
				GameSettings.Instance.AssociatedAutoSave.SaveNow(false, false, SaveGameManager.CurrentMode);
			}
			else
			{
				this.SaveMyGame(this.AutosaveName((!GameSettings.Instance.EditMode) ? "Autosave" : "Building", true, GameSettings.Instance.EditMode), true, false, null);
			}
		}
	}

	private string AutosaveName(string baseName, bool alwayIncludeNumber, bool building)
	{
		baseName = Utilities.CleanFileName(baseName);
		if (!alwayIncludeNumber && !SaveGameManager.CheckExists(baseName + ".sav", building))
		{
			return baseName;
		}
		baseName += " ";
		int num = 1;
		while (SaveGameManager.CheckExists(baseName + num, building))
		{
			num++;
		}
		return baseName + num;
	}

	public static bool CheckExists(string name, bool building)
	{
		if (!building)
		{
			return File.Exists(Path.Combine(SaveGameManager.SaveFolder, name + ".sav"));
		}
		return Directory.Exists(Path.Combine(SaveGameManager.BuildingFolder, name));
	}

	public SaveGame BuildingSave()
	{
		string text = "Building";
		if (GameSettings.Instance != null && GameSettings.Instance.AssociatedSave != null)
		{
			text = GameSettings.Instance.AssociatedSave.Name + " building";
		}
		if (SaveGameManager.CheckExists(text, true))
		{
			int num = 1;
			while (SaveGameManager.CheckExists(text + " " + num, true))
			{
				num++;
			}
			text = text + " " + num;
		}
		SaveGame saveGame = SaveGame.CreateSave(text, GameReader.LoadMode.Building, false);
		if (saveGame.SaveNow(false, true, GameReader.LoadMode.Building))
		{
			this.AddSaveItem(saveGame);
			return saveGame;
		}
		return null;
	}

	public void DeleteSave(SaveGame game, bool message)
	{
		if (game != null && !game.Readonly && File.Exists(game.FilePath))
		{
			try
			{
				if (game.BuildingOnly)
				{
					Directory.Delete(game.Root, true);
				}
				else
				{
					File.Delete(game.FilePath);
				}
				if (GameSettings.Instance != null)
				{
					if (GameSettings.Instance.AssociatedAutoSave == game)
					{
						GameSettings.Instance.AssociatedAutoSave = null;
					}
					if (GameSettings.Instance.AssociatedSave == game)
					{
						GameSettings.Instance.AssociatedSave = null;
					}
				}
				this.RemoveSaveItem(this.FindSave(game));
			}
			catch (Exception exception)
			{
				Debug.LogException(exception);
				if (message)
				{
					WindowManager.SpawnDialog("DeleteFail".Loc(), true, DialogWindow.DialogType.Error);
				}
			}
		}
	}

	public void InstantiateSave(SaveGame save)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.SaveItemPrefab);
		SaveFileItem item = gameObject.GetComponent<SaveFileItem>();
		item.Init(save);
		item.Toggle.onValueChanged.AddListener(delegate(bool x)
		{
			this.SelectChange(item);
		});
		item.Toggle.group = this.TGroup;
		this.SaveItems.Add(item);
		item.transform.SetParent(this.SaveListPanel, false);
	}

	public void ReorderList()
	{
		this.SaveItems.Sort();
		for (int i = 0; i < this.SaveItems.Count; i++)
		{
			this.SaveItems[i].transform.SetSiblingIndex(i);
		}
		this.UpdateThumbs();
	}

	public void UpdateThumbs()
	{
		LayoutRebuilder.ForceRebuildLayoutImmediate(this.SaveListContainer);
		for (int i = 0; i < this.SaveItems.Count; i++)
		{
			SaveFileItem saveFileItem = this.SaveItems[i];
			if (-saveFileItem.rect.offsetMax.y < this.SaveListPanel.anchoredPosition.y + this.SaveListContainer.rect.height && -saveFileItem.rect.offsetMin.y > this.SaveListPanel.anchoredPosition.y)
			{
				if (!saveFileItem.TextureInit)
				{
					saveFileItem.InitTexture(SaveGameManager.GetTexture());
				}
			}
			else if (saveFileItem.TextureInit)
			{
				Texture2D texture2D = saveFileItem.DeInitTex();
				if (texture2D != null)
				{
					SaveGameManager.ThumbPics.Add(texture2D);
				}
			}
		}
	}

	private static Texture2D GetTexture()
	{
		if (SaveGameManager.ThumbPics.Count == 0)
		{
			return new Texture2D(128, 128, TextureFormat.ARGB32, false);
		}
		Texture2D result = SaveGameManager.ThumbPics[SaveGameManager.ThumbPics.Count - 1];
		SaveGameManager.ThumbPics.RemoveAt(SaveGameManager.ThumbPics.Count - 1);
		return result;
	}

	public void AddSaveItem(SaveGame save)
	{
		if (save == null)
		{
			return;
		}
		SaveFileItem saveFileItem = this.FindSave(save);
		if (saveFileItem == null)
		{
			SaveGameManager.AddSave(save);
			this.InstantiateSave(save);
		}
		else
		{
			saveFileItem.Init(save);
		}
		this.ReorderList();
	}

	public void RemoveSaveItem(SaveFileItem save)
	{
		if (save == null)
		{
			return;
		}
		SaveGameManager.RemoveSave(save.Save);
		this.SaveItems.Remove(save);
		UnityEngine.Object.Destroy(save.gameObject);
		this.UpdateThumbs();
	}

	private void OnDestroy()
	{
		if (SaveGameManager.Instance == this)
		{
			SaveGameManager.Instance = null;
		}
		if (this.MapTex != null)
		{
			UnityEngine.Object.Destroy(this.MapTex);
		}
	}

	public static void InitializeSaves()
	{
		if (SaveGameManager.SaveGames == null)
		{
			SaveGameManager.SaveGames = new EventList<SaveGame>();
			if (!Directory.Exists(SaveGameManager.SaveFolder))
			{
				Directory.CreateDirectory(SaveGameManager.SaveFolder);
			}
			if (!Directory.Exists(SaveGameManager.BuildingFolder))
			{
				Directory.CreateDirectory(SaveGameManager.BuildingFolder);
				try
				{
					string text = Path.Combine(SaveGameManager.BuildingFolder, "Apartment Inc");
					Directory.CreateDirectory(text);
					File.WriteAllText(Path.Combine(text, "def.bin"), "NA");
					File.WriteAllBytes(Path.Combine(text, "Apartment Inc.build"), (Resources.Load("Apartment Inc") as TextAsset).bytes);
				}
				catch (Exception ex)
				{
					Debug.LogException(new Exception("Failed creating start building:\n" + ex.ToString()));
				}
			}
			DateTime now = DateTime.Now;
			foreach (string path in Directory.GetFiles(SaveGameManager.SaveFolder, "*.sav"))
			{
				SaveGame saveGame = SaveGameManager.LoadGameMeta(path, false, true);
				if (saveGame != null)
				{
					SaveGameManager.AddSave(saveGame);
				}
			}
			foreach (string path2 in Directory.GetDirectories(SaveGameManager.BuildingFolder))
			{
				SaveGame saveGame2 = SaveGameManager.LoadGameMeta(path2, true, true);
				if (saveGame2 != null)
				{
					SaveGameManager.AddSave(saveGame2);
				}
			}
			Debug.Log("Initializing save games: " + (DateTime.Now - now).TotalSeconds);
		}
	}

	public static void RemoveSave(SaveGame game)
	{
		SaveGameManager.SaveGames.Remove(game);
		if (game.BuildingOnly)
		{
			SaveGameManager.WorkshoppableGames.Remove(game);
		}
	}

	public static void AddSave(SaveGame game)
	{
		SaveGameManager.SaveGames.Add(game);
		if (game.BuildingOnly)
		{
			SaveGameManager.WorkshoppableGames.Add(game);
		}
	}

	public static SaveGame LoadGameMeta(string path, bool build, bool canWrite)
	{
		SaveGame result;
		try
		{
			result = SaveGame.LoadGame(path, build, canWrite);
		}
		catch (Exception ex)
		{
			Debug.LogError(string.Concat(new object[]
			{
				"Error loading save: ",
				Path.GetFileName(path),
				"\n",
				ex
			}));
			result = null;
		}
		return result;
	}

	private void Start()
	{
		Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
		if (SaveGameManager.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		SaveGameManager.Instance = this;
		SaveGameManager.InitializeSaves();
		this.MapTex = new Texture2D(256, 256, TextureFormat.ARGB32, false);
		this.CustomMapThumb.texture = this.MapTex;
		GUICombobox environmentDrop = this.EnvironmentDrop;
		IEnumerable<string> names = Enum.GetNames(typeof(GameData.EnvironmentType));
		if (SaveGameManager.<>f__mg$cache0 == null)
		{
			SaveGameManager.<>f__mg$cache0 = new Func<string, string>(Localization.Loc);
		}
		environmentDrop.UpdateContent<string>(names.Select(SaveGameManager.<>f__mg$cache0));
		GUICombobox climateDrop = this.ClimateDrop;
		IEnumerable<string> names2 = Enum.GetNames(typeof(GameData.ClimateType));
		if (SaveGameManager.<>f__mg$cache1 == null)
		{
			SaveGameManager.<>f__mg$cache1 = new Func<string, string>(Localization.Loc);
		}
		climateDrop.UpdateContent<string>(names2.Select(SaveGameManager.<>f__mg$cache1));
		this.EnvironmentDrop.OnSelectedChanged.AddListener(new UnityAction(this.RenderMiniMap));
		this.ClimateDrop.OnSelectedChanged.AddListener(new UnityAction(this.RenderMiniMap));
		for (int i = 0; i < SaveGameManager.SaveGames.Count; i++)
		{
			this.InstantiateSave(SaveGameManager.SaveGames[i]);
		}
		this.ReorderList();
		if (this.SaveItems.Count > 0)
		{
			this.SaveItems[0].Toggle.isOn = true;
		}
	}

	public void RenderMiniMap()
	{
		GameData.EnvironmentType selected = (GameData.EnvironmentType)this.EnvironmentDrop.Selected;
		MinimapThumbnailMaker.Instance.RenderMap((GameData.ClimateType)this.ClimateDrop.Selected, selected, this.MapTex);
		this.BiggerPlotToggle.gameObject.SetActive(selected == GameData.EnvironmentType.Rural && !this.EditMode);
	}

	private void SelectChange(SaveFileItem save)
	{
		if (save.Toggle.isOn)
		{
			if (this.isSaving)
			{
				this.CurrentName.text = save.Save.Name;
			}
			this.BackupButton.SetActive(this.CustomAction == null && !this.isSaving && File.Exists(save.Save.FilePath + ".bak"));
		}
	}

	public SaveFileItem FindSave(SaveGame game)
	{
		return this.SaveItems.FirstOrDefault((SaveFileItem x) => x.Save == game);
	}

	public SaveFileItem GetSelected()
	{
		if (this.SaveItems.Count((SaveFileItem x) => x.gameObject.activeSelf) == 1)
		{
			return this.SaveItems.FirstOrDefault((SaveFileItem x) => x.gameObject.activeSelf);
		}
		return this.SaveItems.FirstOrDefault((SaveFileItem x) => x.gameObject.activeSelf && x.Toggle.isOn);
	}

	public void UpdateInput()
	{
		if (!this.isSaving)
		{
			this.UpdateActive();
		}
	}

	public void TogglePlot(bool on)
	{
		if (on)
		{
			if (this.BuildingToggle.isOn)
			{
				this.SaveListContainer.gameObject.SetActive(true);
				this.PlotGenPanel.SetActive(false);
				this.UpdateThumbs();
			}
			else if (this.PlotToggle.isOn)
			{
				this.SaveListContainer.gameObject.SetActive(false);
				this.PlotGenPanel.SetActive(true);
			}
		}
	}

	private void UpdateActive()
	{
		bool flag = true;
		bool flag2 = !this.isSaving && !string.IsNullOrEmpty(this.CurrentName.text);
		string value = this.CurrentName.text.ToLower();
		for (int i = 0; i < this.SaveItems.Count; i++)
		{
			SaveFileItem saveFileItem = this.SaveItems[i];
			bool flag3 = ((!this.isSaving && !this.EditMode) || !saveFileItem.Save.Readonly) && saveFileItem.Save.BuildingOnly == this.BuildingMode && (!this.EditMode || (saveFileItem.Save.BuildingOnly && saveFileItem.Save.GetBuildMeta()[0] == 0f));
			if (flag2)
			{
				flag3 &= (saveFileItem.Save.Name + saveFileItem.Save.CompanyName).ToLower().Contains(value);
			}
			saveFileItem.gameObject.SetActive(flag3);
			if (flag3 && flag)
			{
				flag = false;
				saveFileItem.Toggle.isOn = true;
			}
			else
			{
				saveFileItem.Toggle.isOn = false;
			}
		}
	}

	public void ChangeBiggerPlotMode()
	{
		GameData.RuralBigPlots = this.BiggerPlotToggle.isOn;
	}

	public void Show(bool save, bool editMode, bool buildingMode = false, bool plotMode = false, Action<SaveGame> customAction = null)
	{
		this.SaveGameWindow.Title = ((!buildingMode || save) ? "Save games" : "Pick map");
		this.CustomAction = customAction;
		this.isSaving = save;
		this.TGroup.allowSwitchOff = this.isSaving;
		this.EditMode = editMode;
		this.BuildingMode = buildingMode;
		this.NewPlotCost.text = "Cost".Loc() + ": " + PlotArea.StartPlotPrice.Currency(true);
		this.NewPlotCost.gameObject.SetActive(!editMode);
		this.BiggerPlotToggle.isOn = false;
		this.BiggerPlotToggle.gameObject.SetActive(false);
		this.UpdateActive();
		this.PlotTogglePanel.SetActive(plotMode);
		if (plotMode)
		{
			this.PlotToggle.isOn = true;
			this.BuildingToggle.isOn = false;
			this.GenerationField.text = SaveGameManager.GenerateGenString();
			this.EnvironmentDrop.Selected = 2;
			this.ClimateDrop.Selected = 1;
		}
		else
		{
			this.BuildingToggle.isOn = true;
			this.PlotToggle.isOn = false;
		}
		this.TogglePlot(true);
		this.UpdateThumbs();
		bool flag = save || !plotMode;
		this.NameLabel.SetActive(flag);
		this.CurrentName.gameObject.SetActive(flag);
		this.ButtonText.text = ((!this.isSaving) ? "Load".Loc() : "Save".Loc());
		if (this.CustomAction != null || plotMode)
		{
			this.ButtonText.text = "OK".Loc();
		}
		this.SaveGameWindow.Show();
		if (this.isSaving)
		{
			this.CurrentName.text = this.AutosaveName((!buildingMode) ? GameSettings.Instance.MyCompany.Name : "Building", false, buildingMode);
			this.CurrentName.Select();
			if (GameSettings.Instance.AssociatedSave != null)
			{
				this.CurrentName.text = GameSettings.Instance.AssociatedSave.Name;
				SaveFileItem saveFileItem = this.FindSave(GameSettings.Instance.AssociatedSave);
				if (saveFileItem != null && saveFileItem.gameObject.activeSelf)
				{
					this.SelectSave(saveFileItem);
				}
				else
				{
					this.SelectSave(null);
				}
			}
			else
			{
				this.SelectSave(null);
			}
		}
		else
		{
			this.CurrentName.text = string.Empty;
			if (flag)
			{
				this.CurrentName.Select();
			}
		}
		this.UpdateThumbs();
	}

	private void SelectSave(SaveFileItem item)
	{
		for (int i = 0; i < this.SaveItems.Count; i++)
		{
			SaveFileItem saveFileItem = this.SaveItems[i];
			saveFileItem.Toggle.isOn = (saveFileItem == item);
		}
	}

	private static string GenerateGenString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < 10; i++)
		{
			if (Utilities.RandomValue > 0.5f)
			{
				stringBuilder.Append((char)Utilities.RandomRange(65, 91));
			}
			else
			{
				stringBuilder.Append(Utilities.RandomRange(0, 10).ToString());
			}
		}
		return stringBuilder.ToString();
	}

	public bool SaveMyGame(string name, bool auto = false, bool associate = true, GameReader.LoadMode? mode = null)
	{
		GameReader.LoadMode finalMode = (mode == null) ? SaveGameManager.CurrentMode : mode.Value;
		SaveGame exists = SaveGameManager.SaveGames.FirstOrDefault((SaveGame x) => x.BuildingOnly == (finalMode == GameReader.LoadMode.Building) && x.Name.Equals(name));
		if (exists != null)
		{
			if (exists.Readonly)
			{
				WindowManager.Instance.ShowMessageBox("OverwriteError".Loc(), false, DialogWindow.DialogType.Error);
				return false;
			}
			if (!auto)
			{
				WindowManager.Instance.ShowMessageBox("OverwriteConfirmMsg".Loc(), false, DialogWindow.DialogType.Warning, delegate
				{
					if (exists.SaveNow(true, true, finalMode) && associate)
					{
						GameSettings.Instance.AssociatedSave = exists;
					}
				}, "Overwrite save file", null);
				return true;
			}
			if (exists.SaveNow(false, false, finalMode))
			{
				if (associate)
				{
					GameSettings.Instance.AssociatedSave = exists;
				}
				GameSettings.Instance.AssociatedAutoSave = exists;
				return true;
			}
			return false;
		}
		else
		{
			SaveGame saveGame = SaveGame.SaveCurrentGame(name, auto, finalMode);
			if (saveGame != null)
			{
				this.AddSaveItem(saveGame);
				if (associate)
				{
					GameSettings.Instance.AssociatedSave = saveGame;
				}
				if (auto)
				{
					GameSettings.Instance.AssociatedAutoSave = saveGame;
				}
				return true;
			}
			return false;
		}
	}

	public static bool LoadGame(SaveGame game, byte[] companyData, bool backup, bool company, bool building, bool editMode)
	{
		bool flag = game == null && !building;
		string path = (!flag) ? ((!backup || building) ? game.FilePath : (game.FilePath + ".bak")) : null;
		if (!flag && backup && !File.Exists(path))
		{
			return false;
		}
		if (!flag && game.Broken && !backup)
		{
			WindowManager.SpawnDialog("CorruptSaveFile".Loc(), true, DialogWindow.DialogType.Error);
			return false;
		}
		if (game != null && !game.BuildingOnly)
		{
			Versioning.Version version = Versioning.DisectVersionString(game.GameVersion);
			if (version.Major < SaveGameManager.MinimumSupportedSaveAlpha)
			{
				WindowManager.SpawnDialog(string.Format("OldSaveGameError".Loc(), version.SimpleVersion(), Versioning.SimpleVersionString), true, DialogWindow.DialogType.Error);
				return false;
			}
		}
		if (flag || File.Exists(path))
		{
			GameSettings.UnloadNow();
			GameData.LoadGame(game, companyData, backup, company, building, editMode);
			ErrorLogging.FirstOfScene = true;
			SiteNewsFeeder.AbortIfActive();
			ErrorLogging.SceneChanging = true;
			SceneManager.LoadSceneAsync("MainScene");
			return true;
		}
		WindowManager.SpawnDialog("SaveDeletedError".Loc(), true, DialogWindow.DialogType.Error);
		SaveGameManager.RemoveSave(game);
		if (SaveGameManager.Instance != null)
		{
			SaveGameManager.Instance.RemoveSaveItem(SaveGameManager.Instance.FindSave(game));
		}
		return false;
	}

	public void ButtonClick(bool text)
	{
		if (!text || Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
		{
			if (this.CustomAction != null)
			{
				if (this.PlotGenPanel.activeSelf)
				{
					GameData.Climate = (GameData.ClimateType)this.ClimateDrop.Selected;
					GameData.Environment = (GameData.EnvironmentType)this.EnvironmentDrop.Selected;
					GameData.RandomString = this.GenerationField.text;
					this.CustomAction(null);
					this.CustomAction = null;
				}
				else
				{
					SaveFileItem selected = this.GetSelected();
					if (selected != null)
					{
						this.CustomAction(selected.Save);
						this.CustomAction = null;
					}
				}
				this.SaveGameWindow.Close();
			}
			else if (this.isSaving)
			{
				string text2 = Utilities.CleanFileName(this.CurrentName.text);
				this.CurrentName.text = text2;
				this.SaveMyGame(text2, false, true, null);
				this.SaveGameWindow.Close();
			}
			else if (this.PlotGenPanel.activeSelf)
			{
				this.ShowWaitPanel();
				GameData.EditMode = this.EditMode;
				GameData.Climate = (GameData.ClimateType)this.ClimateDrop.Selected;
				GameData.Environment = (GameData.EnvironmentType)this.EnvironmentDrop.Selected;
				GameData.RandomString = this.GenerationField.text;
				GameData.LoadAnyOnLoad = false;
				GameSettings.UnloadNow();
				ErrorLogging.FirstOfScene = true;
				SiteNewsFeeder.AbortIfActive();
				ErrorLogging.SceneChanging = true;
				SceneManager.LoadSceneAsync("MainScene");
			}
			else
			{
				SaveFileItem selected2 = this.GetSelected();
				if (selected2 != null && !this.isLoading)
				{
					this.isLoading = SaveGameManager.LoadGame(selected2.Save, null, false, !this.EditMode, true, this.EditMode);
					if (this.isLoading)
					{
						this.ShowWaitPanel();
					}
				}
			}
			if (text)
			{
				InputController.InputEnabled = true;
			}
		}
	}

	public void ShowWaitPanel()
	{
		if (this.WaitPanel != null)
		{
			this.WaitPanel.gameObject.SetActive(true);
		}
	}

	public void HideWaitPanel()
	{
		if (this.WaitPanel != null)
		{
			this.WaitPanel.gameObject.SetActive(true);
		}
	}

	public void LoadBackup()
	{
		DialogWindow diag = WindowManager.SpawnDialog();
		diag.Show("LoadBackupMsg".Loc(), false, DialogWindow.DialogType.Question, new KeyValuePair<string, Action>[]
		{
			new KeyValuePair<string, Action>("Yes", delegate
			{
				SaveFileItem selected = this.GetSelected();
				if (selected != null && !this.isLoading)
				{
					this.isLoading = SaveGameManager.LoadGame(selected.Save, null, true, !selected.Save.BuildingOnly, true, this.EditMode);
					if (this.isLoading)
					{
						this.ShowWaitPanel();
					}
				}
				diag.Window.Close();
			}),
			new KeyValuePair<string, Action>("No", delegate
			{
				diag.Window.Close();
			})
		});
	}

	public static int MinimumSupportedSaveAlpha = 10;

	public static List<SaveGame> SaveGames;

	public static HashSet<SaveGame> WorkshoppableGames = new HashSet<SaveGame>();

	public static SaveGameManager Instance;

	public GameObject SaveItemPrefab;

	public List<SaveFileItem> SaveItems = new List<SaveFileItem>();

	public static List<Texture2D> ThumbPics = new List<Texture2D>();

	public Text ButtonText;

	public Text NewPlotCost;

	public GameObject NameLabel;

	public GameObject WaitPanel;

	public GameObject PlotTogglePanel;

	public GameObject PlotGenPanel;

	public GUICombobox EnvironmentDrop;

	public GUICombobox ClimateDrop;

	public GUIWindow SaveGameWindow;

	public InputField CurrentName;

	public InputField GenerationField;

	public GameObject BackupButton;

	public RectTransform SaveListPanel;

	public RectTransform SaveListContainer;

	public ToggleGroup TGroup;

	public Toggle PlotToggle;

	public Toggle BuildingToggle;

	public RawImage CustomMapThumb;

	private bool isSaving;

	public bool isLoading;

	public Texture2D MapTex;

	public Toggle BiggerPlotToggle;

	[NonSerialized]
	private Action<SaveGame> CustomAction;

	private bool EditMode;

	private bool BuildingMode;

	[CompilerGenerated]
	private static Func<string, string> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<string, string> <>f__mg$cache1;
}
