﻿using System;
using UnityEngine;

public class WebEnable : MonoBehaviour
{
	private void Start()
	{
		if (base.GetComponent<Renderer>() != null)
		{
			base.GetComponent<Renderer>().enabled = true;
		}
		base.enabled = true;
	}

	private void Update()
	{
	}
}
