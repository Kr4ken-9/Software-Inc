﻿using System;
using UnityEngine;

public class BookScript : MonoBehaviour
{
	private void Start()
	{
		base.GetComponent<Renderer>().material.mainTextureOffset = new Vector2((float)UnityEngine.Random.Range(0, 2) * 0.5f, (float)UnityEngine.Random.Range(0, 2) * 0.5f);
	}
}
