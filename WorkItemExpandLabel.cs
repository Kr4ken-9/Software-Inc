﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class WorkItemExpandLabel : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public void OnPointerClick(PointerEventData eventData)
	{
		this.Parent.Expand();
	}

	public GUIWorkItem Parent;
}
