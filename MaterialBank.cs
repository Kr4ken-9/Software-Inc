﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MaterialBank : MonoBehaviour
{
	public void RemoveFurnitureMat(Furniture furn, Color c1, Color c2, Color c3)
	{
		List<MaterialBank.MaterialHolder> list;
		if (!this.FurnitureBatch.TryGetValue(furn.name, out list))
		{
			return;
		}
		int i = 0;
		while (i < list.Count)
		{
			MaterialBank.MaterialHolder materialHolder = list[i];
			if (!furn.ColorPrimaryEnabled)
			{
				goto IL_6F;
			}
			if (furn.FullColorMaterial)
			{
				if (!(materialHolder.Color != c1))
				{
					goto IL_6F;
				}
			}
			else if (!(materialHolder.Color1 != c1))
			{
				goto IL_6F;
			}
			IL_119:
			i++;
			continue;
			IL_6F:
			if (furn.ColorSecondaryEnabled && materialHolder.Color2 != c2)
			{
				goto IL_119;
			}
			if ((furn.ColorTertiaryEnabled || (furn.ColorableLights.Count > 0 && !furn.LightPrimary)) && materialHolder.Color3 != c3)
			{
				goto IL_119;
			}
			List<int> list2 = this.FurnitureBatchCount[furn.name];
			List<int> list3;
			int index;
			(list3 = list2)[index = i] = list3[index] - 1;
			if (i != 0 && list2[i] == 0)
			{
				list2.RemoveAt(i);
				list.RemoveAt(i);
			}
			return;
		}
	}

	private MaterialBank.MaterialHolder QueryFurnitureMats(Furniture furn, Color c1, Color c2, Color c3)
	{
		List<MaterialBank.MaterialHolder> list;
		if (!this.FurnitureBatch.TryGetValue(furn.name, out list))
		{
			List<MaterialBank.MaterialHolder> list2 = new List<MaterialBank.MaterialHolder>();
			list2.Add(new MaterialBank.MaterialHolder((from x in furn.Colorable
			select new Material(x.GetComponent<Renderer>().sharedMaterial)).ToArray<Material>(), furn.ColorPrimaryDefault, furn.ColorSecondaryDefault, furn.ColorTertiaryDefault, furn.FullColorMaterial));
			list = list2;
			this.FurnitureBatch[furn.name] = list;
			this.FurnitureBatchCount[furn.name] = new List<int>
			{
				0
			};
		}
		int i = 0;
		while (i < list.Count)
		{
			MaterialBank.MaterialHolder materialHolder = list[i];
			if (!furn.ColorPrimaryEnabled)
			{
				goto IL_FA;
			}
			if (furn.FullColorMaterial)
			{
				if (!(materialHolder.Color != c1))
				{
					goto IL_FA;
				}
			}
			else if (!(materialHolder.Color1 != c1))
			{
				goto IL_FA;
			}
			IL_19A:
			i++;
			continue;
			IL_FA:
			if ((furn.ColorSecondaryEnabled || furn.ForceColorSecondary) && materialHolder.Color2 != c2)
			{
				goto IL_19A;
			}
			if ((furn.ColorTertiaryEnabled || furn.ForceColorTertiary || (furn.ColorableLights.Count > 0 && !furn.LightPrimary)) && materialHolder.Color3 != c3)
			{
				goto IL_19A;
			}
			List<int> list3;
			int index;
			(list3 = this.FurnitureBatchCount[furn.name])[index = i] = list3[index] + 1;
			return materialHolder;
		}
		MaterialBank.MaterialHolder materialHolder2 = new MaterialBank.MaterialHolder(list[0]);
		if (furn.ColorPrimaryEnabled)
		{
			if (furn.FullColorMaterial)
			{
				materialHolder2.Color = c1;
			}
			else
			{
				materialHolder2.Color1 = c1;
			}
		}
		if (furn.ColorSecondaryEnabled || furn.TurnOffSecondaryColor)
		{
			materialHolder2.Color2 = c2;
		}
		else if (furn.ForceColorSecondary)
		{
			materialHolder2.Color2 = furn.ColorSecondaryDefault;
		}
		if (furn.ColorTertiaryEnabled || furn.TurnOffTertiaryColor)
		{
			materialHolder2.Color3 = c3;
		}
		else if (furn.ForceColorTertiary)
		{
			materialHolder2.Color3 = furn.ColorTertiaryDefault;
		}
		list.Add(materialHolder2);
		this.FurnitureBatchCount[furn.name].Add(1);
		return materialHolder2;
	}

	public Material[] FurnitureMat(Furniture furn, Color c1, Color c2, Color c3)
	{
		MaterialBank.MaterialHolder m = this.QueryFurnitureMats(furn, c1, c2, c3);
		return m.MapTo.SelectInPlace((int x) => m.Materials[x]);
	}

	public MaterialPropertyBlock FurnitureInstanceMat(Furniture furn, Color c1, Color c2, Color c3)
	{
		MaterialBank.MaterialHolder materialHolder = this.QueryFurnitureMats(furn, c1, c2, c3);
		return materialHolder.MatBlock;
	}

	private void Start()
	{
		MaterialBank.Instance = this;
		if (RoomMaterialController.Instance != null)
		{
			this.StandardRoof.mainTexture = RoomMaterialController.Instance.ColorController.MainTex;
		}
		this.DefaultRed = new MaterialPropertyBlock();
		Color32 c = new Color32(242, 79, 79, byte.MaxValue);
		this.DefaultRed.SetColor("_Color1", c);
		this.DefaultRed.SetColor("_Color2", c);
		this.DefaultRed.SetColor("_Color3", c);
		this.DefaultWhite = new MaterialPropertyBlock();
		Color32 c2 = new Color32(239, 228, 215, byte.MaxValue);
		this.DefaultWhite.SetColor("_Color1", c2);
		this.DefaultWhite.SetColor("_Color2", c2);
		this.DefaultWhite.SetColor("_Color3", c2);
		foreach (Furniture furniture in from x in ObjectDatabase.Instance.GetAllFurniture()
		select x.GetComponent<Furniture>())
		{
			if (furniture.Colorable.Count > 0)
			{
				if (furniture.UseStandardMat)
				{
					this.FurnitureBatch[furniture.name] = new List<MaterialBank.MaterialHolder>
					{
						new MaterialBank.MaterialHolder(furniture.ColorPrimaryDefault, furniture.ColorSecondaryDefault, furniture.ColorTertiaryDefault)
					};
				}
				else
				{
					Dictionary<string, List<MaterialBank.MaterialHolder>> furnitureBatch = this.FurnitureBatch;
					string name = furniture.name;
					List<MaterialBank.MaterialHolder> list = new List<MaterialBank.MaterialHolder>();
					list.Add(new MaterialBank.MaterialHolder(furniture.Colorable.Select((Renderer x) => new Material(x.GetComponent<Renderer>().sharedMaterial)).ToArray<Material>(), furniture.ColorPrimaryDefault, furniture.ColorSecondaryDefault, furniture.ColorTertiaryDefault, furniture.FullColorMaterial));
					furnitureBatch[name] = list;
				}
				this.FurnitureBatchCount[furniture.name] = new List<int>
				{
					0
				};
			}
		}
		this.DarknessMats = new Material[this.DarknessCount];
		Material darkness = this.Darkness;
		float num = (float)(this.DarknessMats.Length - 1);
		for (int i = 0; i < this.DarknessMats.Length; i++)
		{
			float num2 = (float)i / num;
			this.DarknessMats[i] = new Material(darkness);
			this.DarknessMats[i].SetFloat("_Transparency", num2 * this.MaxDarkness);
		}
	}

	public Material GetDarkness(float darkness)
	{
		if (float.IsPositiveInfinity(darkness) || float.IsNaN(darkness))
		{
			return this.DarknessMats[this.DarknessMats.Length - 1];
		}
		if (float.IsNegativeInfinity(darkness))
		{
			return this.DarknessMats[0];
		}
		return this.DarknessMats[Mathf.RoundToInt(Mathf.Clamp01(darkness) * (float)(this.DarknessMats.Length - 1))];
	}

	public Material BaseMat;

	public Material StandardRoof;

	public Material Darkness;

	public Material Blackness;

	public string[] Category;

	public GameObject SmokeParticleSystem;

	public static MaterialBank Instance;

	[NonSerialized]
	public Dictionary<string, List<MaterialBank.MaterialHolder>> FurnitureBatch = new Dictionary<string, List<MaterialBank.MaterialHolder>>();

	[NonSerialized]
	public Dictionary<string, List<int>> FurnitureBatchCount = new Dictionary<string, List<int>>();

	public int DarknessCount = 25;

	public float MaxDarkness = 0.8f;

	private Material[] DarknessMats;

	public MaterialPropertyBlock DefaultWhite;

	public MaterialPropertyBlock DefaultRed;

	public class MaterialHolder
	{
		public MaterialHolder(MaterialBank.MaterialHolder source)
		{
			this.Instanced = source.Instanced;
			if (this.Instanced)
			{
				this.MatBlock = new MaterialPropertyBlock();
				this.Color1 = source.color1;
				this.Color2 = source.color2;
				this.Color3 = source.color3;
			}
			else
			{
				this.color1 = source.color1;
				this.color2 = source.color2;
				this.color3 = source.color3;
				this.Materials = source.Materials.SelectInPlace((Material x) => new Material(x));
				this.MapTo = source.MapTo.ToArray<int>();
				this.FullColor = source.FullColor;
			}
		}

		public MaterialHolder(Color c1, Color c2, Color c3)
		{
			this.Instanced = true;
			this.MatBlock = new MaterialPropertyBlock();
			this.Color1 = c1;
			this.Color2 = c2;
			this.Color3 = c3;
		}

		public MaterialHolder(Material[] mat, Color c1, Color c2, Color c3, bool fullColor)
		{
			List<Material> list = new List<Material>();
			List<int> list2 = new List<int>();
			for (int i = 0; i < mat.Length; i++)
			{
				for (int j = 0; j < list.Count; j++)
				{
					if (mat[i] == list[j])
					{
						list2.Add(j);
						break;
					}
				}
				if (list2.Count <= i)
				{
					list.Add(new Material(mat[i]));
					list2.Add(i);
				}
			}
			this.Materials = list.ToArray();
			this.MapTo = list2.ToArray();
			this.FullColor = fullColor;
			if (fullColor)
			{
				this.Color = c1;
			}
			else
			{
				this.Color1 = c1;
				this.Color2 = c2;
				this.Color3 = c3;
			}
		}

		public Color Color
		{
			get
			{
				return this.color1;
			}
			set
			{
				this.color1 = value;
				if (this.Instanced)
				{
					this.MatBlock.SetColor("_Color1", value);
				}
				else
				{
					for (int i = 0; i < this.Materials.Length; i++)
					{
						this.Materials[i].color = value;
					}
				}
			}
		}

		public Color Color1
		{
			get
			{
				return this.color1;
			}
			set
			{
				this.color1 = value;
				if (this.Instanced)
				{
					this.MatBlock.SetColor("_Color1", value);
				}
				else
				{
					for (int i = 0; i < this.Materials.Length; i++)
					{
						this.Materials[i].SetColor("_Color1", value);
					}
				}
			}
		}

		public Color Color2
		{
			get
			{
				return this.color2;
			}
			set
			{
				this.color2 = value;
				if (this.Instanced)
				{
					this.MatBlock.SetColor("_Color2", value);
				}
				else
				{
					for (int i = 0; i < this.Materials.Length; i++)
					{
						this.Materials[i].SetColor("_Color2", value);
					}
				}
			}
		}

		public Color Color3
		{
			get
			{
				return this.color3;
			}
			set
			{
				this.color3 = value;
				if (this.Instanced)
				{
					this.MatBlock.SetColor("_Color3", value);
				}
				else if (!this.FullColor)
				{
					for (int i = 0; i < this.Materials.Length; i++)
					{
						this.Materials[i].SetColor("_Color3", value);
					}
				}
			}
		}

		public Material[] Materials;

		public MaterialPropertyBlock MatBlock;

		public int[] MapTo;

		public bool FullColor;

		public bool Instanced;

		private Color color1;

		private Color color2;

		private Color color3;
	}

	public class BuildMaterial
	{
		public BuildMaterial(Material mat)
		{
			this.Mat = mat;
		}

		public Material Mat;

		public int Count;
	}

	[Serializable]
	public class WallMaterial
	{
		public Material GenerateMaterial(Material baseMat)
		{
			Material material = new Material(baseMat);
			material.SetTexture("_MainTex", this.Base);
			material.SetTexture("_BumpMap", this.Bump);
			material.SetTexture("_OcclusionMap", this.Occlusion);
			material.SetTexture("_Overlay", this.Overlay);
			material.SetFloat("_Metallic", this.Metallic);
			material.SetFloat("_Glossiness", this.Smoothness);
			material.SetFloat("_BumpScale", this.BumpScale);
			return material;
		}

		public string Name;

		public string Category;

		public Texture2D Base;

		public Texture2D Bump;

		public Texture2D Occlusion;

		public Texture2D Overlay;

		public float Metallic;

		public float Smoothness;

		public float BumpScale;
	}
}
