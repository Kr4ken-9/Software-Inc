﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class TreeBatch : MonoBehaviour
{
	public bool CanAdd(global::TreeInstance tree)
	{
		return this._trees.Contains(tree) || this._trees.Count < this.MaxTrees;
	}

	public void AddTree(global::TreeInstance tree)
	{
		if (this._trees.Count == 0)
		{
			this.Center = tree.GetPos();
		}
		this._trees.Add(tree);
		tree.BelongsTo = this;
		this.Dirty = true;
	}

	public void RemoveTree(global::TreeInstance tree)
	{
		if (this._trees.Contains(tree))
		{
			this._trees.Remove(tree);
			tree.BelongsTo = null;
			this.Dirty = true;
		}
	}

	private void Update()
	{
		bool flag = GameSettings.Instance != null && GameSettings.Instance.ActiveFloor > -1;
		if (flag != this.IsVisible)
		{
			if (this.HasTrunk)
			{
				this.Trunks.gameObject.SetActive(flag);
			}
			if (this.HasLeaf)
			{
				this.Leaves.gameObject.SetActive(flag);
			}
			this.IsVisible = flag;
		}
	}

	public bool GenerateMesh()
	{
		if (!this.Dirty)
		{
			return true;
		}
		if (this._trees.Count == 0)
		{
			return false;
		}
		this.Dirty = false;
		CombineInstance[] array = new CombineInstance[this._trees.Count];
		List<CombineInstance> list = new List<CombineInstance>();
		int num = 0;
		foreach (global::TreeInstance treeInstance in this._trees)
		{
			StaticTree treeMesh = treeInstance.TreeMesh;
			array[num] = new CombineInstance
			{
				mesh = treeMesh.Trunk.sharedMesh,
				transform = treeInstance.Transform
			};
			num++;
			if (!(treeMesh.Leaves == null))
			{
				list.Add(new CombineInstance
				{
					mesh = TreeBatch.RandomUVs(treeMesh, treeInstance.LeaveOffset),
					transform = treeInstance.Transform
				});
			}
		}
		if (!this.HasTrunk)
		{
			this.Trunks = this.CreateMesh("MergedTrunks", this.TrunkMat);
			this.HasTrunk = true;
		}
		this.Trunks.mesh.CombineMeshes(array);
		if (list.Count > 0)
		{
			if (!this.HasLeaf)
			{
				this.Leaves = this.CreateMesh("MergedLeaves", this.LeafMat);
				this.HasLeaf = true;
			}
			this.Leaves.mesh.CombineMeshes(list.ToArray());
		}
		else if (this.HasLeaf)
		{
			UnityEngine.Object.Destroy(this.Leaves.gameObject);
			this.HasLeaf = false;
		}
		return true;
	}

	private MeshFilter CreateMesh(string batchName, Material mat)
	{
		GameObject gameObject = new GameObject(batchName);
		gameObject.isStatic = true;
		MeshFilter result = gameObject.AddComponent<MeshFilter>();
		Renderer renderer = gameObject.AddComponent<MeshRenderer>();
		renderer.sharedMaterial = mat;
		renderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
		gameObject.transform.parent = base.transform;
		return result;
	}

	private static Mesh RandomUVs(StaticTree mesh, float off)
	{
		Mesh mesh2 = new Mesh();
		Vector2[] uv = mesh.Leaves.sharedMesh.uv;
		for (int i = 0; i < uv.Length; i++)
		{
			uv[i] = new Vector2((uv[i].x + off) % 1f, uv[i].y);
		}
		mesh2.SetVertices(mesh.Verts);
		mesh2.SetNormals(mesh.Norms);
		mesh2.SetTangents(mesh.Tans);
		mesh2.uv = uv;
		mesh2.SetTriangles(mesh.Tris, 0);
		return mesh2;
	}

	[NonSerialized]
	private HashSet<global::TreeInstance> _trees = new HashSet<global::TreeInstance>();

	public Vector2 Center;

	public bool HasTrunk;

	public bool HasLeaf;

	public bool IsVisible = true;

	public MeshFilter Leaves;

	public MeshFilter Trunks;

	public Material TrunkMat;

	public Material LeafMat;

	public int MaxTrees;

	public bool Dirty;
}
