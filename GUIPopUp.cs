﻿using System;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUIPopUp : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	private void Start()
	{
		if (this.popup != null)
		{
			this.TextBox.text = this.popup.Text;
			if (this.popup.Action != PopupManager.PopUpAction.None)
			{
				Text textBox = this.TextBox;
				textBox.text = textBox.text + " (" + "NotificationGotoHint".Loc() + ")";
			}
			this.TextBox.color = this.popup.textColor;
			this.Date.text = this.popup.Time.ToExtraCompactString();
			this.Icon.sprite = IconManager.GetIcon(this.popup.Icon);
			LayoutRebuilder.ForceRebuildLayoutImmediate(base.transform.parent.GetComponent<RectTransform>());
			this.panel.preferredHeight = Mathf.Max(45f, this.TextBox.preferredHeight + 4f);
			if (this.IsNew)
			{
				Color color = this.MainImage.color;
				this.MainImage.color = new Color32(149, 180, 253, 191);
				ShortcutExtensions46.DOColor(this.MainImage, color, 3f);
			}
		}
		else
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void OnDestroy()
	{
		if (HUD.Instance != null)
		{
			HUD.Instance.popupManager.PopupButtons.Remove(this);
			HUD.Instance.popupManager.PopupIDDict.Remove(this.popup.Identifier);
		}
	}

	private void Goto(Vector3 pos, global::Selectable sel, bool moveIfInactive)
	{
		if (!BuildController.Instance.CanChangeFloor())
		{
			return;
		}
		if (moveIfInactive || sel.isActiveAndEnabled)
		{
			GameSettings.Instance.ActiveFloor = Mathf.FloorToInt((pos.y + 1f) / 2f);
			Furniture.UpdateEdgeDetection();
			GameSettings.Instance.sRoomManager.ChangeFloor();
			CameraScript.GotoPos(new Vector2(pos.x, pos.z));
		}
		if (sel != null)
		{
			SelectorController.Instance.Highligt(false);
			SelectorController.Instance.Selected.Clear();
			SelectorController.Instance.Selected.Add(sel);
			SelectorController.Instance.DoPostSelectChecks();
		}
	}

	public void OnPointerClick(PointerEventData d)
	{
		if (d.button == PointerEventData.InputButton.Left)
		{
			if (this.popup.Action != PopupManager.PopUpAction.None)
			{
				UISoundFX.PlaySFX("ButtonClick", -1f, 0f);
				switch (this.popup.Action)
				{
				case PopupManager.PopUpAction.GotoEmp:
				{
					Actor actor = GameSettings.Instance.sActorManager.Actors.FirstOrDefault((Actor x) => x.DID == this.popup.ActionTarget);
					if (actor != null)
					{
						this.Goto(actor.transform.position, actor, false);
					}
					else
					{
						actor = GameSettings.Instance.sActorManager.Staff.FirstOrDefault((Actor x) => x.DID == this.popup.ActionTarget);
						if (actor != null)
						{
							this.Goto(actor.transform.position, actor, false);
						}
					}
					break;
				}
				case PopupManager.PopUpAction.GotoRoom:
				{
					Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == this.popup.ActionTarget);
					if (room != null)
					{
						this.Goto(new Vector3(room.Center.x, (float)(room.Floor * 2), room.Center.y), room, true);
					}
					break;
				}
				case PopupManager.PopUpAction.GotoFurn:
				{
					Furniture furniture = GameSettings.Instance.sRoomManager.AllFurniture.FirstOrDefault((Furniture x) => x.DID == this.popup.ActionTarget);
					if (furniture != null)
					{
						this.Goto(furniture.transform.position, furniture, true);
					}
					break;
				}
				case PopupManager.PopUpAction.GotoHR:
				{
					Actor actor2 = GameSettings.Instance.sActorManager.Actors.FirstOrDefault((Actor x) => x.DID == this.popup.ActionTarget);
					if (actor2 != null && actor2.employee.IsRole(Employee.RoleBit.Lead))
					{
						HUD.Instance.TeamWindow.autoWindow.Show(new Team[]
						{
							actor2.GetTeam()
						});
					}
					break;
				}
				case PopupManager.PopUpAction.OpenInsurance:
					HUD.Instance.insuranceWindow.Show(false);
					break;
				case PopupManager.PopUpAction.OpenProductDetails:
				{
					SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(this.popup.ActionTarget, false);
					if (product != null)
					{
						HUD.Instance.GetProductWindow(null).ShowProductDetails(product);
					}
					break;
				}
				case PopupManager.PopUpAction.OpenCompanyDetails:
				{
					Company company = GameSettings.Instance.simulation.GetCompany(this.popup.ActionTarget);
					if (company != null)
					{
						HUD.Instance.companyWindow.ShowCompanyDetails(company);
					}
					break;
				}
				}
			}
		}
		else if (d.button == PointerEventData.InputButton.Right)
		{
			SelectorController.CanClick = false;
			UISoundFX.PlaySFX("ButtonSwoop", -1f, 0f);
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public LayoutElement panel;

	public RawImage MainImage;

	public Image Icon;

	public Text TextBox;

	public Text Date;

	[NonSerialized]
	public PopupManager.PopUp popup;

	[NonSerialized]
	public float Made = -1f;

	[NonSerialized]
	public bool IsNew = true;
}
