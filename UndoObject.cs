﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class UndoObject
{
	public UndoObject(params UndoObject.UndoAction[] actions)
	{
		this.Actions = actions;
		this.MakeDescription();
	}

	public UndoObject(params UndoObject.UndoAction[][] actions)
	{
		this.Actions = actions.SelectMany((UndoObject.UndoAction[] x) => x).ToArray<UndoObject.UndoAction>();
		this.MakeDescription();
	}

	private void MakeDescription()
	{
		this.Description = string.Join("\n", (from x in this.Actions
		group x by x.Type).Select(delegate(IGrouping<UndoObject.UndoAction.ActionType, UndoObject.UndoAction> x)
		{
			string input = "Undo" + x.Key.ToString();
			object[] array = new object[1];
			array[0] = x.Sum((UndoObject.UndoAction z) => z.Count());
			return input.Loc(array);
		}).ToArray<string>());
	}

	private static bool DestroyFurniture(UndoObject.UndoAction action)
	{
		Furniture furniture = GameSettings.Instance.sRoomManager.AllFurniture.FirstOrDefault((Furniture x) => x.DID == action.Get<uint>("ID"));
		if (furniture != null)
		{
			furniture.Undo = true;
			UnityEngine.Object.Destroy(furniture.gameObject);
			return true;
		}
		return false;
	}

	private static bool CreateFurniture(UndoObject.UndoAction action)
	{
		WriteDictionary writeDictionary = action.Get<WriteDictionary>("Furn");
		string name = writeDictionary["Type"].ToString();
		GameObject furniture = ObjectDatabase.Instance.GetFurniture(name);
		if (!(furniture != null))
		{
			return false;
		}
		Furniture component = UnityEngine.Object.Instantiate<GameObject>(furniture).GetComponent<Furniture>();
		component.name = furniture.name;
		component.DeserializeClone = true;
		component.DeserializeThis(writeDictionary);
		if (!component.isTemporary)
		{
			component.gameObject.SetActive(true);
			if (component.TwoFloors)
			{
				GameSettings.Instance.sRoomManager.AllFurniture.Add(component);
				component.ExtraParent.DirtyNavMesh = true;
				component.ExtraParent.DirtyPathNodes = true;
				component.ExtraParent.DirtyFloorMesh = true;
				component.Parent.DirtyRoofMesh = true;
			}
			return true;
		}
		return false;
	}

	private static bool MoveFurniture(UndoObject.UndoAction action)
	{
		uint[] ids = action.Get<uint[]>("IDS");
		Furniture furniture = GameSettings.Instance.sRoomManager.AllFurniture.FirstOrDefault((Furniture x) => x.DID == ids[0]);
		Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == ids[1]);
		if (furniture != null && room != null)
		{
			WallEdge wallEdge = action.Get<WallEdge>("Edge1", null);
			WallEdge wallEdge2 = action.Get<WallEdge>("Edge2", null);
			float wallPos = action.Get<float>("WallPos", 0f);
			FurnitureBuilder.MoveFurn(action.Get<Vector3>("Pos"), action.Get<Quaternion>("Rot"), room, wallEdge, wallEdge2, wallPos, action.Get<SnapPoint>("Snap", null), furniture.gameObject, action.Get<float>("LocalRot"));
			furniture.UpdateBoundsMesh();
			FurnitureBuilder.TraverseMove(furniture, room, wallEdge, wallEdge2, wallPos);
		}
		return false;
	}

	private static bool ReplaceFurniture(UndoObject.UndoAction action)
	{
		uint id = action.Get<uint>("ID");
		Furniture furniture = GameSettings.Instance.sRoomManager.AllFurniture.FirstOrDefault((Furniture x) => x.DID == id);
		if (furniture != null)
		{
			WriteDictionary writeDictionary = action.Get<WriteDictionary>("Furn");
			furniture.Undo = true;
			Furniture furnitureComponent = ObjectDatabase.Instance.GetFurnitureComponent(writeDictionary["Type"].ToString());
			Furniture furniture2 = FurnitureReplaceWindow.UpgradeFurniture(furniture, furnitureComponent);
			furniture2.name = furnitureComponent.name;
			furniture2.DeserializeThis(writeDictionary);
			if (!furniture2.isTemporary)
			{
				furniture2.gameObject.SetActive(true);
			}
			return true;
		}
		return false;
	}

	private static bool DestroySegment(UndoObject.UndoAction action)
	{
		uint id = action.Get<uint>("ID");
		RoomSegment roomSegment = GameSettings.Instance.sRoomManager.RoomSegments.FirstOrDefault((RoomSegment x) => x.DID == id);
		if (roomSegment != null)
		{
			UnityEngine.Object.Destroy(roomSegment.gameObject);
			return true;
		}
		return false;
	}

	private static bool CreateSegment(UndoObject.UndoAction action)
	{
		WriteDictionary segD = action.Get<WriteDictionary>("Segment");
		GameObject gameObject = ObjectDatabase.Instance.RoomSegments.FirstOrDefault((GameObject x) => x.name.Equals(segD["Type"].ToString()));
		if (gameObject != null)
		{
			RoomSegment component = UnityEngine.Object.Instantiate<GameObject>(gameObject).GetComponent<RoomSegment>();
			component.name = gameObject.name;
			component.DeserializeClone = true;
			component.DeserializeThis(segD);
			return true;
		}
		return false;
	}

	private static bool DestroyRoom(UndoObject.UndoAction action)
	{
		UndoObject.<DestroyRoom>c__AnonStorey5 <DestroyRoom>c__AnonStorey = new UndoObject.<DestroyRoom>c__AnonStorey5();
		<DestroyRoom>c__AnonStorey.ids = action.Get<uint[]>("IDS");
		int i;
		for (i = 0; i < <DestroyRoom>c__AnonStorey.ids.Length; i++)
		{
			Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == <DestroyRoom>c__AnonStorey.ids[i]);
			if (room != null)
			{
				room.GetFurnitures().ForEach(delegate(Furniture x)
				{
					x.Undo = true;
				});
				UnityEngine.Object.Destroy(room.gameObject);
			}
			else if (<DestroyRoom>c__AnonStorey.ids.Length == 1)
			{
				return false;
			}
		}
		return true;
	}

	private static bool CreateRoom(UndoObject.UndoAction action)
	{
		RoomCloneTool.Instance.BuildPrefab(action.Get<BuildingPrefab>("Build"), 0, false, false, false, new uint[]
		{
			action.Get<uint>("ID")
		}, false);
		return true;
	}

	private static bool MergeRooms(UndoObject.UndoAction action)
	{
		uint[] ids = action.Get<uint[]>("IDS");
		Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == ids[0]);
		Room room2 = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == ids[1]);
		if (!(room != null) || !(room2 != null))
		{
			return false;
		}
		if (!room.TryFixEdges() || !room2.TryFixEdges())
		{
			return false;
		}
		if (room.CanMerge(room2))
		{
			room.MergeWith(room2, null, null);
		}
		return true;
	}

	private static bool SplitRoom(UndoObject.UndoAction action)
	{
		uint id = action.Get<uint>("ID");
		List<Vector2> list = action.Get<List<Vector2>>("Points");
		Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == id);
		if (!(room != null))
		{
			return false;
		}
		List<WallEdge> list2 = new List<WallEdge>();
		for (int i = 0; i < list.Count; i++)
		{
			Vector2 vector = list[i];
			WallEdge wallEdge = null;
			if (i == 0 || i == list.Count - 1)
			{
				foreach (WallEdge wallEdge2 in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(room.Floor))
				{
					if ((wallEdge2.Pos - vector).magnitude < BuildController.Instance.SnapDistance)
					{
						wallEdge = wallEdge2;
						break;
					}
				}
				if (wallEdge == null)
				{
					foreach (WallEdge wallEdge3 in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(room.Floor))
					{
						foreach (KeyValuePair<Room, WallEdge> keyValuePair in wallEdge3.Links)
						{
							Vector2? vector2 = Utilities.ProjectToLine(vector, wallEdge3.Pos, keyValuePair.Value.Pos);
							if (vector2 != null && (vector2.Value - vector).magnitude < BuildController.Instance.SnapDistance)
							{
								wallEdge = new WallEdge(vector2.Value, room.Floor);
								wallEdge.SetSplit(wallEdge3, keyValuePair.Key);
								break;
							}
						}
					}
				}
			}
			if (wallEdge == null)
			{
				wallEdge = new WallEdge(vector, room.Floor);
			}
			list2.Add(wallEdge);
		}
		Dictionary<WallSnap, UndoObject.UndoAction> snaps = room.PrepareSplit(false);
		BuildController.Instance.CurrentSegments = list2;
		BuildController.Instance.FinalizeCuts(true, room.Floor, null);
		BuildController.Instance.CurrentSegments = null;
		GameSettings.Instance.sRoomManager.AllSegments.AddRange(list2);
		Room room2 = room.Split(list2, null, snaps, null, false);
		if (room2 != null)
		{
			GameSettings.Instance.sRoomManager.AddRoom(room2);
			foreach (Furniture furniture in room.GetFurnitures().ToList<Furniture>())
			{
				furniture.UpdateParent(true, false);
			}
			WriteDictionary dictionary = action.Get<WriteDictionary>("Room");
			room2.DeserializeThis(dictionary);
			RoomGroup roomGroup = GameSettings.Instance.GetRoomGroup(room2.RoomGroup);
			if (roomGroup != null)
			{
				roomGroup.AddRoom(room2);
			}
			room2.RecalculateTableGroups();
			room.RecalculateTableGroups();
			return true;
		}
		return false;
	}

	private static bool ChangeRoad(UndoObject.UndoAction action)
	{
		byte[,] array = action.Get<byte[,]>("RoadSegments");
		Vector2 vector = action.Get<Vector2>("Point");
		for (int i = 0; i < array.GetLength(0); i++)
		{
			for (int j = 0; j < array.GetLength(1); j++)
			{
				RoadManager.Instance.PlaceRoad(i + (int)vector.x, j + (int)vector.y, array[i, j]);
			}
		}
		return true;
	}

	private static bool RoomColor(UndoObject.UndoAction action)
	{
		UndoObject.<RoomColor>c__AnonStorey9 <RoomColor>c__AnonStorey = new UndoObject.<RoomColor>c__AnonStorey9();
		<RoomColor>c__AnonStorey.ids = action.Get<uint[]>("IDS");
		Color[,] array = action.Get<Color[,]>("Colors");
		int i;
		for (i = 0; i < <RoomColor>c__AnonStorey.ids.Length; i++)
		{
			Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == <RoomColor>c__AnonStorey.ids[i]);
			if (room != null)
			{
				room.FloorColor = array[i, 0];
				room.InsideColor = array[i, 1];
				room.OutsideColor = array[i, 2];
			}
		}
		return false;
	}

	private static bool RoomMaterial(UndoObject.UndoAction action)
	{
		UndoObject.<RoomMaterial>c__AnonStoreyB <RoomMaterial>c__AnonStoreyB = new UndoObject.<RoomMaterial>c__AnonStoreyB();
		<RoomMaterial>c__AnonStoreyB.ids = action.Get<uint[]>("IDS");
		string[,] array = action.Get<string[,]>("Mats");
		int i;
		for (i = 0; i < <RoomMaterial>c__AnonStoreyB.ids.Length; i++)
		{
			Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == <RoomMaterial>c__AnonStoreyB.ids[i]);
			if (room != null)
			{
				room.FloorMat = array[i, 0];
				if (room.Outdoors)
				{
					room.SetFenceStyle(array[i, 3], null);
				}
				else
				{
					room.InsideMat = array[i, 1];
					room.OutsideMat = array[i, 2];
				}
			}
		}
		return false;
	}

	private static bool FurnitureColor(UndoObject.UndoAction action)
	{
		UndoObject.<FurnitureColor>c__AnonStoreyD <FurnitureColor>c__AnonStoreyD = new UndoObject.<FurnitureColor>c__AnonStoreyD();
		<FurnitureColor>c__AnonStoreyD.ids = action.Get<uint[]>("IDS");
		Color[,] array = action.Get<Color[,]>("Colors");
		int i;
		for (i = 0; i < <FurnitureColor>c__AnonStoreyD.ids.Length; i++)
		{
			Furniture furniture = GameSettings.Instance.sRoomManager.AllFurniture.FirstOrDefault((Furniture x) => x.DID == <FurnitureColor>c__AnonStoreyD.ids[i]);
			if (furniture != null)
			{
				if (furniture.ColorPrimaryEnabled)
				{
					furniture.ColorPrimary = array[i, 0];
				}
				if (furniture.ColorSecondaryEnabled)
				{
					furniture.ColorSecondary = array[i, 1];
				}
				if (furniture.ColorTertiaryEnabled)
				{
					furniture.ColorTertiary = array[i, 2];
				}
			}
		}
		return false;
	}

	private static bool BuyPlot(UndoObject.UndoAction action)
	{
		PlotArea plotArea = action.Get<PlotArea>("Plot");
		GameSettings.Instance.BuyPlot(plotArea);
		plotArea.Monthly = action.Get<float>("Monthly");
		plotArea.MonthsLeft = action.Get<int>("MonthsLeft");
		GameSettings.Instance.MyCompany.MakeTransaction(-action.Get<float>("UpFront"), Company.TransactionCategory.Construction, "Plot");
		return true;
	}

	private static bool SellPlot(UndoObject.UndoAction action)
	{
		PlotArea plotArea = action.Get<PlotArea>("Plot");
		GameSettings.Instance.SellPlot(plotArea);
		plotArea.AddonCost = action.Get<float>("AddonCost");
		GameSettings.Instance.MyCompany.MakeTransaction(action.Get<float>("UpFront"), Company.TransactionCategory.Construction, "Plot");
		return true;
	}

	private static bool RentRoom(UndoObject.UndoAction action)
	{
		uint id = action.Get<uint>("ID");
		Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == id);
		if (room != null)
		{
			room.SetPlayerOwned(true, null);
			GameSettings.Instance.DirtyRentGrid.Add(room.Floor);
			return true;
		}
		return false;
	}

	private static bool UnrentRoom(UndoObject.UndoAction action)
	{
		uint id = action.Get<uint>("ID");
		Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == id);
		if (room != null)
		{
			float rentPrice = room.GetRentPrice();
			GameSettings.Instance.MyCompany.MakeTransaction(rentPrice, Company.TransactionCategory.Bills, "Rent");
			room.SetPlayerOwned(false, null);
			GameSettings.Instance.DirtyRentGrid.Add(room.Floor);
			return true;
		}
		return false;
	}

	private static bool CreateLandmark(UndoObject.UndoAction action)
	{
		RoadManager.Instance.DeserializeLandmark(action.Get<WriteDictionary>("Landmark"));
		return true;
	}

	private static bool DestroyLandmark(UndoObject.UndoAction action)
	{
		uint did = action.Get<uint>("Landmark");
		GameObject gameObject = RoadManager.Instance.FindLandmark(did);
		if (gameObject != null)
		{
			UnityEngine.Object.Destroy(gameObject);
			return true;
		}
		return false;
	}

	private static bool AddTrees(UndoObject.UndoAction action)
	{
		global::TreeInstance[] array = action.Get<global::TreeInstance[]>("Trees");
		if (array.Length > 0)
		{
			for (int i = 0; i < array.Length; i++)
			{
				GameSettings.Instance.TempTrees.Add(array[i]);
			}
			GameSettings.Instance.BatchTempTrees();
		}
		return true;
	}

	private static bool RemoveTrees(UndoObject.UndoAction action)
	{
		foreach (global::TreeInstance treeInstance in action.Get<global::TreeInstance[]>("Trees"))
		{
			if (treeInstance.BelongsTo != null)
			{
				treeInstance.BelongsTo.RemoveTree(treeInstance);
			}
			GameSettings.Instance.Trees.Remove(treeInstance);
			GameSettings.Instance.TreeTree.TryRemoveItem(treeInstance);
		}
		return true;
	}

	public void Execute()
	{
		foreach (UndoObject.UndoAction undoAction in this.Actions)
		{
			MethodInfo method = typeof(UndoObject).GetMethod(undoAction.Type.ToString(), BindingFlags.Static | BindingFlags.NonPublic);
			if (method == null)
			{
				throw new Exception("Undo type could not be handled: " + undoAction.Type.ToString());
			}
			bool flag = (bool)method.Invoke(null, new object[]
			{
				undoAction
			});
			if (!flag && undoAction.Type == UndoObject.UndoAction.ActionType.SplitRoom)
			{
				return;
			}
			if (flag && undoAction.BalanceDiff != 0f)
			{
				GameSettings.Instance.MyCompany.MakeTransaction(undoAction.BalanceDiff, undoAction.BalanceCategory, undoAction.BalanceBill);
			}
		}
	}

	public UndoObject.UndoAction[] Actions;

	public string Description;

	public class UndoAction
	{
		public UndoAction(WriteDictionary oldFurn, Furniture newFurn, float diff)
		{
			this.Type = UndoObject.UndoAction.ActionType.ReplaceFurniture;
			this.Dictionary["Furn"] = oldFurn;
			newFurn.InitWritable();
			this.Dictionary["ID"] = newFurn.DID;
			this.BalanceDiff = diff;
			this.BalanceCategory = Company.TransactionCategory.Construction;
			this.BalanceBill = "Furniture";
		}

		public UndoAction(Furniture[] furns)
		{
			this.Type = UndoObject.UndoAction.ActionType.FurnitureColor;
			Color[,] array = new Color[furns.Length, 3];
			uint[] array2 = new uint[furns.Length];
			for (int i = 0; i < furns.Length; i++)
			{
				Furniture furniture = furns[i];
				array2[i] = furniture.DID;
				array[i, 0] = furniture.ColorPrimary;
				array[i, 1] = furniture.ColorPrimary;
				array[i, 2] = furniture.ColorPrimary;
			}
			this.Dictionary["Colors"] = array;
			this.Dictionary["IDS"] = array2;
		}

		public UndoAction(List<Room> rooms, bool mat)
		{
			this.Type = ((!mat) ? UndoObject.UndoAction.ActionType.RoomColor : UndoObject.UndoAction.ActionType.RoomMaterial);
			string[,] array = null;
			Color[,] array2 = null;
			if (mat)
			{
				array = new string[rooms.Count, 4];
			}
			else
			{
				array2 = new Color[rooms.Count, 3];
			}
			uint[] array3 = new uint[rooms.Count];
			for (int i = 0; i < rooms.Count; i++)
			{
				Room room = rooms[i];
				array3[i] = room.DID;
				if (mat)
				{
					array[i, 0] = room.FloorMat;
					array[i, 1] = room.InsideMat;
					array[i, 2] = room.OutsideMat;
					array[i, 3] = room.FenceStyle;
				}
				else
				{
					array2[i, 0] = room.FloorColor;
					array2[i, 1] = room.InsideColor;
					array2[i, 2] = room.OutsideColor;
				}
			}
			this.Dictionary["Mats"] = array;
			this.Dictionary["Colors"] = array2;
			this.Dictionary["IDS"] = array3;
		}

		public UndoAction(WallSnap snap, bool destroy)
		{
			RoomSegment roomSegment = snap as RoomSegment;
			if (roomSegment != null)
			{
				this.InitSegment(roomSegment, destroy);
			}
			else if (snap is Furniture)
			{
				this.InitFurn((Furniture)snap, destroy);
			}
		}

		public UndoAction(Furniture furn, Room room, Vector3 pos, Quaternion rot, float localRot, SnapPoint point)
		{
			this.Type = UndoObject.UndoAction.ActionType.MoveFurniture;
			this.Dictionary["IDS"] = new uint[]
			{
				furn.DID,
				room.DID
			};
			this.BalanceDiff = 0f;
			this.Dictionary["Pos"] = pos;
			this.Dictionary["Rot"] = rot;
			this.Dictionary["LocalRot"] = localRot;
			if (furn.WallFurn)
			{
				WallEdge firstEdge = furn.FirstEdge;
				WallEdge secondEdge = furn.GetSecondEdge();
				this.Dictionary["Edge1"] = firstEdge;
				this.Dictionary["Edge2"] = secondEdge;
				this.Dictionary["WallPos"] = furn.WallPosition[firstEdge] / (firstEdge.Pos - secondEdge.Pos).magnitude;
			}
			this.Dictionary["Snap"] = point;
		}

		public UndoAction(Room room, bool destroy, float cost)
		{
			this.BalanceCategory = Company.TransactionCategory.Construction;
			if (destroy)
			{
				this.BalanceBill = "Room";
				this.Type = UndoObject.UndoAction.ActionType.DestroyRoom;
				this.Dictionary["IDS"] = new uint[]
				{
					room.DID
				};
			}
			else
			{
				this.Type = UndoObject.UndoAction.ActionType.CreateRoom;
				this.Dictionary["Build"] = BuildingPrefab.SaveRooms(new Room[]
				{
					room
				}, true, false, true, false);
				this.Dictionary["ID"] = room.DID;
			}
			this.BalanceDiff = cost;
		}

		public UndoAction(Room[] rooms, float balance)
		{
			this.BalanceCategory = Company.TransactionCategory.Construction;
			this.Type = UndoObject.UndoAction.ActionType.DestroyRoom;
			this.Dictionary["IDS"] = (from x in rooms
			select x.DID).ToArray<uint>();
			this.BalanceDiff = balance;
		}

		public UndoAction(int x, int y, int w, int h, float balance)
		{
			this.Type = UndoObject.UndoAction.ActionType.ChangeRoad;
			this.BalanceCategory = Company.TransactionCategory.Construction;
			this.BalanceBill = "Road";
			this.BalanceDiff = balance;
			byte[,] array = new byte[w, h];
			this.Dictionary["Point"] = new Vector2((float)x, (float)y);
			for (int i = 0; i < w; i++)
			{
				for (int j = 0; j < h; j++)
				{
					array[i, j] = RoadManager.Instance.GetRoad(x + i, y + j);
				}
			}
			this.Dictionary["RoadSegments"] = array;
		}

		public UndoAction(Room r1, Room r2, float balance)
		{
			this.Type = UndoObject.UndoAction.ActionType.MergeRooms;
			this.BalanceCategory = Company.TransactionCategory.Construction;
			this.BalanceBill = "Room";
			this.BalanceDiff = balance;
			this.Dictionary["IDS"] = new uint[]
			{
				r1.DID,
				r2.DID
			};
		}

		public UndoAction(Room r1, Room r2, List<Vector2> split)
		{
			this.Type = UndoObject.UndoAction.ActionType.SplitRoom;
			this.Dictionary["ID"] = r1.DID;
			this.BalanceCategory = Company.TransactionCategory.Construction;
			this.BalanceBill = "Room";
			this.Dictionary["Points"] = split;
			this.BalanceDiff = 0f;
			this.Dictionary["Room"] = r2.SerializeThis(GameReader.LoadMode.Full);
		}

		public UndoAction(PlotArea area, float upFront)
		{
			this.Type = UndoObject.UndoAction.ActionType.BuyPlot;
			this.Dictionary["MonthsLeft"] = area.MonthsLeft;
			this.Dictionary["Monthly"] = area.Monthly;
			this.Dictionary["UpFront"] = upFront;
			this.Dictionary["Plot"] = area;
		}

		public UndoAction(PlotArea area, float upFront, float addonCost)
		{
			this.Type = UndoObject.UndoAction.ActionType.SellPlot;
			this.Dictionary["Plot"] = area;
			this.Dictionary["AddonCost"] = addonCost;
			this.Dictionary["UpFront"] = upFront;
		}

		public UndoAction(Room r, bool rent)
		{
			this.Type = ((!rent) ? UndoObject.UndoAction.ActionType.UnrentRoom : UndoObject.UndoAction.ActionType.RentRoom);
			this.Dictionary["ID"] = r.DID;
		}

		public UndoAction(Landmark mark, bool create)
		{
			this.Type = ((!create) ? UndoObject.UndoAction.ActionType.DestroyLandmark : UndoObject.UndoAction.ActionType.CreateLandmark);
			if (create)
			{
				this.Dictionary["Landmark"] = mark.SerializeThis(GameReader.LoadMode.Full);
			}
			else
			{
				this.Dictionary["Landmark"] = mark.DID;
			}
		}

		public UndoAction(global::TreeInstance[] trees, bool add)
		{
			this.Type = ((!add) ? UndoObject.UndoAction.ActionType.RemoveTrees : UndoObject.UndoAction.ActionType.AddTrees);
			this.Dictionary["Trees"] = trees;
		}

		public int Count()
		{
			UndoObject.UndoAction.ActionType type = this.Type;
			if (type != UndoObject.UndoAction.ActionType.RoomColor && type != UndoObject.UndoAction.ActionType.RoomMaterial && type != UndoObject.UndoAction.ActionType.FurnitureColor && type != UndoObject.UndoAction.ActionType.DestroyRoom)
			{
				return 1;
			}
			uint[] array = this.Dictionary.Get<uint[]>("IDS", null);
			return (array != null) ? array.Length : 1;
		}

		public T Get<T>(string key)
		{
			return this.Dictionary.Get<T>(key);
		}

		public T Get<T>(string key, T def)
		{
			return this.Dictionary.Get<T>(key, def);
		}

		private void InitFurn(Furniture furn, bool destroy)
		{
			this.BalanceCategory = Company.TransactionCategory.Construction;
			if (destroy)
			{
				this.BalanceBill = "Furniture";
				this.Type = UndoObject.UndoAction.ActionType.DestroyFurniture;
				furn.InitWritable();
				this.Dictionary["ID"] = furn.DID;
				this.BalanceDiff = furn.Cost;
			}
			else
			{
				this.BalanceBill = "Recycle";
				this.Type = UndoObject.UndoAction.ActionType.CreateFurniture;
				if (furn.WallFurn)
				{
					furn.PrepareTempSerialization();
				}
				this.Dictionary["Furn"] = furn.SerializeThis(GameReader.LoadMode.Full);
				this.BalanceDiff = -furn.GetSellPrice();
			}
		}

		private void InitSegment(RoomSegment seg, bool destroy)
		{
			this.BalanceCategory = Company.TransactionCategory.Construction;
			if (destroy)
			{
				this.Type = UndoObject.UndoAction.ActionType.DestroySegment;
				seg.InitWritable();
				this.Dictionary["ID"] = seg.DID;
				RoomSegment segmentComponent = ObjectDatabase.Instance.GetSegmentComponent(seg.name);
				this.BalanceDiff = seg.Cost * (seg.WallWidth / segmentComponent.WallWidth);
				this.BalanceBill = "Segment";
			}
			else
			{
				this.Type = UndoObject.UndoAction.ActionType.CreateSegment;
				seg.PrepareTempSerialization();
				this.Dictionary["Segment"] = seg.SerializeThis(GameReader.LoadMode.Full);
				this.BalanceDiff = 0f;
			}
		}

		public UndoObject.UndoAction.ActionType Type;

		public WriteDictionary Dictionary = new WriteDictionary();

		public float BalanceDiff;

		public Company.TransactionCategory BalanceCategory;

		public string BalanceBill;

		public enum ActionType
		{
			Nothing,
			DestroyFurniture,
			CreateFurniture,
			FurnitureColor,
			ReplaceFurniture,
			MoveFurniture,
			DestroySegment,
			CreateSegment,
			DestroyRoom,
			CreateRoom,
			MergeRooms,
			SplitRoom,
			RoomColor,
			RoomMaterial,
			ChangeRoad,
			BuyPlot,
			SellPlot,
			RentRoom,
			UnrentRoom,
			CreateLandmark,
			DestroyLandmark,
			AddTrees,
			RemoveTrees
		}
	}
}
