﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LampScript : MonoBehaviour
{
	public void PowerToggled(bool ison)
	{
		if (this.subLight != null)
		{
			this.subLight.enabled = ison;
		}
	}

	public void CalcEdge(Rect bounds, Vector3 pos)
	{
		if (this.furn == null || this.furn.Parent == null)
		{
			return;
		}
		this.AtEdge = false;
		Vector2 vector = new Vector2(base.transform.position.x, base.transform.position.z);
		List<WallEdge> edges = this.furn.Parent.Edges;
		if (edges == null)
		{
			return;
		}
		if (this.furn.Parent.Outdoors)
		{
			this.AtEdge = true;
			return;
		}
		for (int i = 0; i < edges.Count; i++)
		{
			int index = (i + 1) % edges.Count;
			if (!edges[index].Links.ContainsValue(edges[i]))
			{
				Vector2? vector2 = Utilities.ProjectToLine(vector, edges[i].Pos, edges[index].Pos);
				if (vector2 != null && (vector2.Value - vector).sqrMagnitude < 2f)
				{
					this.AtEdge = true;
					break;
				}
			}
		}
	}

	private bool BuildModeLighting()
	{
		return HUD.Instance != null && HUD.Instance.BuildMode && HUD.Instance.SunSlider.value < 0.5f;
	}

	private bool InitNow()
	{
		if (!this.init)
		{
			this.init = true;
			this.furn = base.GetComponent<Furniture>();
			if (!(this.furn != null))
			{
				base.enabled = false;
				return false;
			}
			this.subLight = base.GetComponentInChildren<Light>();
			if (!(this.subLight != null))
			{
				base.enabled = false;
				return false;
			}
			this.subLight.enabled = this.furn.IsOn;
			this.initialIntensity = this.subLight.intensity;
		}
		return true;
	}

	private void Start()
	{
		this.InitNow();
	}

	private void Update()
	{
		if (!this.InitNow() || this.furn == null || this.furn.Parent == null || !base.enabled)
		{
			return;
		}
		if (this.furn.isTemporary)
		{
			this.subLight.shadows = LightShadows.Hard;
			this.subLight.enabled = true;
			return;
		}
		bool flag = this.BuildModeLighting();
		this.furn.IsOn = (this.furn.Parent.Floor == -1 || Cheats.ForceLights || (this.furn.Parent.Occupants.Count > 0 && this.furn.Parent.WindowDarkLevel * TimeOfDay.LightLevel < 0.75f) || (this.furn.Parent.Outdoors && TimeOfDay.LightLevel < 0.75f));
		if (this.subLight.enabled && !flag && !this.furn.IsOn)
		{
			this.subLight.enabled = false;
		}
		if (this.furn.IsOn || flag)
		{
			float num = (!flag && this.furn.HasUpg) ? this.furn.upg.Quality : 1f;
			this.subLight.enabled = ((this.furn.Parent.Floor < 0 && GameSettings.Instance.ActiveFloor < 0) || (this.furn.Parent.Floor > -1 && (this.furn.Parent.Floor == GameSettings.Instance.ActiveFloor || (this.AtEdge && this.furn.Parent.Floor < GameSettings.Instance.ActiveFloor))));
			if (this.subLight.enabled)
			{
				this.subLight.intensity = this.initialIntensity * (0.5f + (1f - Mathf.Min(1f, Mathf.Max(0f, TimeOfDay.LightLevel - 0.3f) * 1.5f)) * 0.5f);
				if (num < 0.33f)
				{
					this.subLight.intensity *= UnityEngine.Random.value * 0.5f + 0.5f;
				}
				if (this.subLight.shadows == LightShadows.Hard ^ Options.MoreShadow)
				{
					this.subLight.shadows = ((!Options.MoreShadow) ? LightShadows.None : LightShadows.Hard);
				}
			}
		}
	}

	private Furniture furn;

	private Light subLight;

	private bool AtEdge;

	private float initialIntensity;

	private bool init;
}
