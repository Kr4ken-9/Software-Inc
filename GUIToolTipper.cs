﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUIToolTipper : MonoBehaviour, IPointerEnterHandler, IEventSystemHandler
{
	public void OnPointerEnter(PointerEventData d)
	{
		this.SetTip();
	}

	private void SetTip()
	{
		Tooltip.SetToolTip((!(this.Localize ^ this.OnlyLocalizeDescription)) ? this.ToolTipValue : this.ToolTipValue.Loc(), (!this.Localize) ? this.TooltipDescription : this.TooltipDescription.Loc(), base.GetComponent<RectTransform>());
	}

	public void UpdateTip()
	{
		RectTransform component = base.GetComponent<RectTransform>();
		if (component != null && Tooltip.CurrentRect == component)
		{
			this.SetTip();
		}
	}

	public string ToolTipValue;

	[TextArea(3, 10)]
	public string TooltipDescription;

	public bool Localize = true;

	public bool OnlyLocalizeDescription;
}
