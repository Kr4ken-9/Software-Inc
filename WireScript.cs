﻿using System;
using UnityEngine;

public class WireScript : MonoBehaviour
{
	public Color color
	{
		set
		{
			this.MainWire.material.color = value;
			this.Endings[0].GetComponent<Renderer>().material.color = value;
			this.Endings[1].GetComponent<Renderer>().material.color = value;
		}
	}

	public void SetPos(Vector3 start, Vector3 stop)
	{
		Vector3 b = (stop - start) * 0.5f;
		base.transform.position = start + b + Vector3.up;
		base.transform.rotation = ((!(stop == start)) ? Quaternion.LookRotation(stop - start) : Quaternion.identity);
		base.transform.localScale = new Vector3(1f, 1f, b.magnitude);
		this.Endings[0].localScale = new Vector3(2.5f, (b.magnitude != 0f) ? (1f / b.magnitude * 0.25f) : 0f, 2.5f);
		this.Endings[1].localScale = new Vector3(2.5f, (b.magnitude != 0f) ? (1f / b.magnitude * 0.25f) : 0f, 2.5f);
	}

	public Transform[] Endings;

	public Renderer MainWire;
}
