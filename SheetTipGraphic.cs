﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SheetTipGraphic : MaskableGraphic, IPointerEnterHandler, IPointerDownHandler, IEventSystemHandler
{
	public override Texture mainTexture
	{
		get
		{
			return this.Texture;
		}
	}

	protected override void OnPopulateMesh(VertexHelper vh)
	{
		vh.Clear();
		if (this.Sprites == null || this.Sprites.Length == 0)
		{
			return;
		}
		RectTransform rectTransform = base.rectTransform;
		Vector2 vector = new Vector2(-rectTransform.rect.width * rectTransform.pivot.x, -rectTransform.rect.height * (rectTransform.pivot.y - 1f) - this.SpriteSize);
		if (this.Highlight)
		{
			Rect rect = new Rect(vector.x, vector.y, rectTransform.rect.width, rectTransform.rect.height);
			Color c = new Color(1f, 1f, 1f, 0.8f);
			vh.AddUIVertexQuad(new UIVertex[]
			{
				new UIVertex
				{
					color = c,
					position = new Vector3(rect.xMin, rect.yMax),
					uv0 = this.HightligtUV
				},
				new UIVertex
				{
					color = c,
					position = new Vector3(rect.xMax, rect.yMax),
					uv0 = this.HightligtUV
				},
				new UIVertex
				{
					color = c,
					position = new Vector3(rect.xMax, rect.yMin),
					uv0 = this.HightligtUV
				},
				new UIVertex
				{
					color = c,
					position = new Vector3(rect.xMin, rect.yMin),
					uv0 = this.HightligtUV
				}
			});
		}
		for (int i = 0; i < this.Sprites.Length; i++)
		{
			if (this.Sprites[i] >= 0)
			{
				this.DrawSprite(this.Sprites[i], new Rect((float)i * this.SpriteSize + vector.x, vector.y, this.SpriteSize, this.SpriteSize), vh);
			}
		}
	}

	public void DrawSprite(int sp, Rect pos, VertexHelper vh)
	{
		int num = sp % this.SheetWidth;
		int num2 = sp / this.SheetWidth;
		float num3 = (float)num / (float)this.SheetWidth;
		float num4 = 1f - (float)num2 / (float)this.SheetHeight;
		float x = num3 + 1f / (float)this.SheetWidth;
		float y = num4 - 1f / (float)this.SheetHeight;
		vh.AddUIVertexQuad(new UIVertex[]
		{
			new UIVertex
			{
				color = this.color,
				position = new Vector3(pos.xMin, pos.yMax),
				uv0 = new Vector2(num3, num4)
			},
			new UIVertex
			{
				color = this.color,
				position = new Vector3(pos.xMax, pos.yMax),
				uv0 = new Vector2(x, num4)
			},
			new UIVertex
			{
				color = this.color,
				position = new Vector3(pos.xMax, pos.yMin),
				uv0 = new Vector2(x, y)
			},
			new UIVertex
			{
				color = this.color,
				position = new Vector3(pos.xMin, pos.yMin),
				uv0 = new Vector2(num3, y)
			}
		});
	}

	private void Update()
	{
		if (this._tipping)
		{
			if (Tooltip.CurrentRect != base.rectTransform)
			{
				this._tipping = false;
				return;
			}
			this.UpdateTooltip();
		}
	}

	private void UpdateTooltip()
	{
		Vector2 zero = Vector2.zero;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(base.rectTransform, Input.mousePosition, null, out zero))
		{
			int num = Mathf.FloorToInt((zero.x + base.rectTransform.rect.width * base.rectTransform.pivot.x) / this.SpriteSize);
			if (num >= 0 && num < this.Sprites.Length)
			{
				int num2 = this.Sprites[num];
				if (num2 >= 0)
				{
					string input = this.Tips[num2];
					Tooltip.SetToolTip(input.Loc(), null, base.rectTransform);
				}
				else
				{
					Tooltip.CurrentRect = base.rectTransform;
					Tooltip.Hide();
				}
			}
			else
			{
				Tooltip.Hide();
				this._tipping = false;
			}
		}
		else
		{
			Tooltip.Hide();
			this._tipping = false;
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		this._tipping = true;
		this.UpdateTooltip();
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		this.OnClick.Invoke();
	}

	public Texture2D Texture;

	public int SheetWidth = 1;

	public int SheetHeight = 1;

	public float SpriteSize = 24f;

	public int[] Sprites;

	public string[] Tips;

	private bool _tipping;

	public bool Highlight;

	public Vector2 HightligtUV;

	public UnityEvent OnClick;
}
