﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using ClipperLib;
using Poly2Tri;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public static class Utilities
{
	private static System.Random RNG
	{
		get
		{
			return SafeRandom.Rnd;
		}
	}

	public static T GetRandomWeighted<T>(List<KeyValuePair<float, T>> values)
	{
		if (values == null || values.Count == 0)
		{
			Debug.LogError("Tried to get weighted random value with empty list of values");
			return default(T);
		}
		float num = UnityEngine.Random.value * values.Sum((KeyValuePair<float, T> x) => x.Key);
		float num2 = 0f;
		foreach (KeyValuePair<float, T> keyValuePair in values)
		{
			num2 += keyValuePair.Key;
			if (num <= num2)
			{
				return keyValuePair.Value;
			}
		}
		throw new UnityException("Somehow random choice failed");
	}

	public static float GetMonths(SDateTime start, SDateTime now)
	{
		return (float)(now.Year - start.Year) * 12f + (float)(now.Month - start.Month) + ((float)(now.Day - start.Day) + ((float)(now.Hour - start.Hour) + (float)(now.Minute - start.Minute) / 60f) / 24f) / (float)GameSettings.DaysPerMonth;
	}

	public static float GetDays(SDateTime start, SDateTime now)
	{
		return ((float)(now.Year - start.Year) * 12f + (float)(now.Month - start.Month)) * (float)GameSettings.DaysPerMonth + (float)(now.Day - start.Day) + ((float)(now.Hour - start.Hour) + (float)(now.Minute - start.Minute) / 60f) / 24f;
	}

	public static float GetHours(SDateTime start, SDateTime now)
	{
		return (((float)(now.Year - start.Year) * 12f + (float)(now.Month - start.Month)) * (float)GameSettings.DaysPerMonth + (float)(now.Day - start.Day)) * 24f + (float)(now.Hour - start.Hour) + (float)(now.Minute - start.Minute) / 60f;
	}

	public static int GetMonthsFlat(SDateTime start, SDateTime now)
	{
		return (now.Year - start.Year) * 12 + (now.Month - start.Month);
	}

	public static int GetDaysFlat(SDateTime start, SDateTime now)
	{
		return ((now.Year - start.Year) * 12 + (now.Month - start.Month)) * GameSettings.DaysPerMonth + (now.Day - start.Day);
	}

	public static string Countdown(SDateTime now, SDateTime end)
	{
		if (GameSettings.DaysPerMonth > 1)
		{
			int num = (end.Year * 12 + end.Month) * GameSettings.DaysPerMonth + end.Day - ((now.Year * 12 + now.Month) * GameSettings.DaysPerMonth + now.Day);
			if (num == 0)
			{
				return "Today".Loc();
			}
			if (num < 0)
			{
				return "TimeDiffNotLeft".Loc(new object[]
				{
					"DayPostfix".LocPlural(-num)
				});
			}
			return "TimeDiffLeft".Loc(new object[]
			{
				"DayPostfix".LocPlural(num)
			});
		}
		else
		{
			int num2 = end.Year * 12 + end.Month - (now.Year * 12 + now.Month);
			if (num2 == 0)
			{
				return "This month".Loc();
			}
			if (num2 < 0)
			{
				return "TimeDiffNotLeft".Loc(new object[]
				{
					"MonthPostfix".LocPlural(-num2)
				});
			}
			return "TimeDiffLeft".Loc(new object[]
			{
				"MonthPostfix".LocPlural(num2)
			});
		}
	}

	public static string DateDiff(SDateTime now, SDateTime end)
	{
		int num = (end.Year * 12 + end.Month) * GameSettings.DaysPerMonth + end.Day;
		num -= (now.Year * 12 + now.Month) * GameSettings.DaysPerMonth + now.Day;
		StringBuilder stringBuilder = new StringBuilder();
		int num2;
		num = (num - (num2 = num % GameSettings.DaysPerMonth)) / GameSettings.DaysPerMonth;
		int num4;
		int num3 = (num - (num4 = num % 12)) / 12;
		bool flag = false;
		bool flag2 = true;
		if (num3 > 0)
		{
			flag2 = false;
			flag = true;
			stringBuilder.Append("YearPostfix".LocPlural(num3));
		}
		if (num4 > 0)
		{
			flag2 = false;
			if (flag)
			{
				stringBuilder.Append((num2 != 0) ? ", " : " and ");
			}
			stringBuilder.Append("MonthPostfix".LocPlural(num4));
		}
		if (num2 > 0)
		{
			flag2 = false;
			if (flag)
			{
				stringBuilder.Append(" and ");
			}
			stringBuilder.Append("DayPostfix".LocPlural(num2));
		}
		if (flag2)
		{
			return "HourPostfix".LocPlural(Mathf.CeilToInt(Utilities.GetHours(now, end)));
		}
		return stringBuilder.ToString();
	}

	public static int CompareNumber<T>(Func<T, float> f, T x, T y)
	{
		return f(x).CompareTo(f(y));
	}

	public static int CompareString<T>(Func<T, string> f, T x, T y)
	{
		string text = f(x);
		string text2 = f(y);
		if (text == null)
		{
			if (text2 == null)
			{
				return 0;
			}
			return -1;
		}
		else
		{
			if (text2 == null)
			{
				return 1;
			}
			return text.CompareTo(text2);
		}
	}

	public static float RandomGauss(float mean, float deviation)
	{
		float randomValue;
		for (randomValue = Utilities.RandomValue; randomValue == 0f; randomValue = Utilities.RandomValue)
		{
		}
		float randomValue2 = Utilities.RandomValue;
		float num = Mathf.Sqrt(-2f * Mathf.Log(randomValue)) * Mathf.Sin(6.28318548f * randomValue2);
		return mean + deviation * num;
	}

	public static float RandomGaussClamped(float mean = 0.5f, float deviation = 0.2f)
	{
		float num = Utilities.RandomGauss(mean, deviation);
		if (num < 0f)
		{
			num = -num;
		}
		if (num > 1f)
		{
			num = 1f - (num - 1f);
		}
		return num;
	}

	public static int GaussRange(float mean, int min, int max, float deviation = 0.2f)
	{
		float num = Utilities.RandomGaussClamped(mean, deviation);
		return min + (int)Mathf.Floor(num * (float)(max - min + 1));
	}

	public static float GaussRangeFloat(float mean, float min, float max, float deviation = 0.2f)
	{
		float num = Utilities.RandomGaussClamped(mean, deviation);
		return min + num * (max - min);
	}

	public static Rect Expand(this Rect input, float x, float y)
	{
		return new Rect(input.x - x / 2f, input.y - y / 2f, input.width + x, input.height + y);
	}

	public static string CurrencyDiff(this float x, bool Ext = true)
	{
		return ((x <= 0f) ? string.Empty : "+") + x.Currency(Ext);
	}

	public static string Currency(this float x, bool Ext = true)
	{
		Currency currency = GameData.GetCurrency(Options.Currency);
		float num = Mathf.Abs(x * currency.Rate);
		string str;
		if (num >= 0.01f && num < 10f)
		{
			str = num.ToString("N2");
		}
		else if (Options.CurrencyShortForm)
		{
			if (num > 1E+09f)
			{
				str = (num / 1E+09f).ToString("N0") + "BillionPost".Loc();
			}
			else if (num > 1000000f)
			{
				str = (num / 1000000f).ToString("N0") + "MillionPost".Loc();
			}
			else
			{
				str = num.ToString("N0");
			}
		}
		else
		{
			str = num.ToString("N0");
		}
		return ((x >= 0f) ? string.Empty : "-") + ((!Ext) ? string.Empty : currency.Prefix) + str + ((!Ext) ? string.Empty : currency.Postfix);
	}

	public static string CurrencyInt(this int x, bool Ext = true)
	{
		return ((float)x).Currency(Ext);
	}

	public static float CurrencyMul(this float x)
	{
		return x * GameData.GetCurrency(Options.Currency).Rate;
	}

	public static float CurrencyMulInt(this int x)
	{
		Currency currency = GameData.GetCurrency(Options.Currency);
		return (float)x * currency.Rate;
	}

	public static float FromCurrency(this float x)
	{
		return x / GameData.GetCurrency(Options.Currency).Rate;
	}

	public static float CurrencyRoundUpToNearest(this float x, float multiple)
	{
		float rate = GameData.GetCurrency(Options.Currency).Rate;
		return Mathf.Ceil(x * rate / multiple) * multiple / rate;
	}

	public static float CurrencyRoundDownToNearest(this float x, float multiple)
	{
		float rate = GameData.GetCurrency(Options.Currency).Rate;
		return Mathf.Floor(x * rate / multiple) * multiple / rate;
	}

	public static float RoundUpToNearest(this float x, int multiple)
	{
		return Mathf.Ceil(x / (float)multiple) * (float)multiple;
	}

	public static float RoundDownToNearest(this float x, int multiple)
	{
		return Mathf.Floor(x / (float)multiple) * (float)multiple;
	}

	public static bool Approximate(this Vector3 v1, Vector3 v2)
	{
		return Mathf.Approximately(v1.x, v2.x) && Mathf.Approximately(v1.y, v2.y) && Mathf.Approximately(v1.z, v2.z);
	}

	public static T MaxInstance<T>(this IEnumerable<T> list, Func<T, float> maxFunc)
	{
		float num = float.MinValue;
		T result = default(T);
		foreach (T t in list)
		{
			float num2 = maxFunc(t);
			if (num2 > num)
			{
				num = num2;
				result = t;
			}
		}
		return result;
	}

	public static T MaxInstance<T>(this IList<T> list, Func<T, float> maxFunc)
	{
		float num = float.MinValue;
		T result = default(T);
		for (int i = 0; i < list.Count; i++)
		{
			T t = list[i];
			float num2 = maxFunc(t);
			if (num2 > num)
			{
				num = num2;
				result = t;
			}
		}
		return result;
	}

	public static T MinInstance<T>(this IEnumerable<T> list, Func<T, float> maxFunc)
	{
		float num = float.MaxValue;
		T result = default(T);
		foreach (T t in list)
		{
			float num2 = maxFunc(t);
			if (num2 < num)
			{
				num = num2;
				result = t;
			}
		}
		return result;
	}

	public static T MinInstance<T>(this IList<T> list, Func<T, float> maxFunc)
	{
		float num = float.MaxValue;
		T result = default(T);
		for (int i = 0; i < list.Count; i++)
		{
			T t = list[i];
			float num2 = maxFunc(t);
			if (num2 < num)
			{
				num = num2;
				result = t;
			}
		}
		return result;
	}

	public static IEnumerable<T> ReverseEnum<T>(this IList<T> list)
	{
		for (int i = list.Count - 1; i >= 0; i--)
		{
			yield return list[i];
		}
		yield break;
	}

	public static int AddHour(this int x, int amount)
	{
		return (int)Utilities.Modulo((float)x + (float)amount, 24f);
	}

	public static float PerHour(float perHour, float delta, bool useGameSpeed = true)
	{
		return perHour / 60f * delta * ((!useGameSpeed) ? 1f : GameSettings.GameSpeed);
	}

	public static float PerHour(float perHour, bool useGameSpeed = true)
	{
		return Utilities.PerHour(perHour, Time.deltaTime, useGameSpeed);
	}

	public static float PerDay(float perDay, float delta, bool useGameSpeed = true)
	{
		return perDay / (float)GameSettings.DaysPerMonth / 60f / 24f * delta * ((!useGameSpeed) ? 1f : GameSettings.GameSpeed);
	}

	public static float PerDay(float perDay, bool useGameSpeed = true)
	{
		return Utilities.PerDay(perDay, Time.deltaTime, useGameSpeed);
	}

	public static float Modulo(float a, float b)
	{
		return a - b * Mathf.Floor(a / b);
	}

	public static bool IsEmpty(this string a)
	{
		return string.IsNullOrEmpty(a.Trim());
	}

	public static int Sign(float num)
	{
		return (num >= 0f) ? ((num <= 0f) ? 0 : 1) : -1;
	}

	public static int Sign(int num)
	{
		return (num >= 0) ? ((num <= 0) ? 0 : 1) : -1;
	}

	public static float AngleDistance(float a1, float a2)
	{
		float num = Mathf.Abs(a1 - a2);
		if (num > 180f)
		{
			num = 360f - num;
		}
		return num;
	}

	public static bool AnglePassed(float a1, float a2, float anchor)
	{
		Vector3 lhs = new Vector3(Mathf.Cos(a1 / 180f * 3.14159274f), Mathf.Sin(a1 / 180f * 3.14159274f), 0f);
		Vector3 lhs2 = new Vector3(Mathf.Cos(a2 / 180f * 3.14159274f), Mathf.Sin(a2 / 180f * 3.14159274f), 0f);
		Vector3 rhs = new Vector3(Mathf.Cos(anchor / 180f * 3.14159274f), Mathf.Sin(anchor / 180f * 3.14159274f), 0f);
		Vector3 vector = Vector3.Cross(lhs, rhs);
		Vector3 vector2 = Vector3.Cross(lhs2, rhs);
		return Utilities.Sign(vector.z) != Utilities.Sign(vector2.z);
	}

	public static bool Straight(Vector2 v1, Vector2 v2, Vector2 v3)
	{
		Vector2 vector = v2 - v1;
		Vector2 vector2 = v3 - v2;
		return Mathf.Approximately(Mathf.Atan2(vector.y, vector.x), Mathf.Atan2(vector2.y, vector2.x));
	}

	public static bool Straight(Vector3 v1, Vector3 v2, Vector3 v3)
	{
		Vector3 vector = v2 - v1;
		Vector3 vector2 = v3 - v2;
		return Mathf.Approximately(Mathf.Atan2(vector.z, vector.x), Mathf.Atan2(vector2.z, vector2.x));
	}

	public static List<Vector3> Bezier(int iterations, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
	{
		List<Vector3> list = new List<Vector3>();
		list.Add(p0);
		for (int i = 0; i < iterations - 1; i++)
		{
			float t = (float)(i + 1) / (float)iterations;
			list.Add(Utilities.CalculateBezierPoint(t, p0, p1, p2, p3));
		}
		list.Add(p3);
		return list;
	}

	private static Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
	{
		float num = 1f - t;
		float num2 = num * num;
		float d = num2 * num;
		float num3 = t * t;
		float d2 = num3 * t;
		Vector3 a = d * p0;
		a += 3f * num2 * t * p1;
		a += 3f * num * num3 * p2;
		return a + d2 * p3;
	}

	public static float MapRange(this float x, float a, float b, float c, float d, bool clamp = false)
	{
		if (clamp)
		{
			if (x >= b)
			{
				return d;
			}
			if (x <= a)
			{
				return c;
			}
		}
		float num = b - a;
		float num2 = (num != 0f) ? ((x - a) / num) : (x - a);
		float num3 = d - c;
		return num2 * num3 + c;
	}

	public static float MapRange(this int x, float a, float b, float c, float d, bool clamp = false)
	{
		return ((float)x).MapRange(a, b, c, d, clamp);
	}

	public static float PosNeg(float x, float low, float high)
	{
		if (x > 1f)
		{
			return 1f + (x - 1f) * (high - 1f);
		}
		if (x < 1f)
		{
			return low + x * (1f - low);
		}
		return 1f;
	}

	public static Vector3 RGBToHSV(Color c)
	{
		float num = Mathf.Min(new float[]
		{
			c.r,
			c.g,
			c.b
		});
		float num2 = Mathf.Max(new float[]
		{
			c.r,
			c.g,
			c.b
		});
		float z = num2;
		float num3 = num2 - num;
		float y;
		float num4;
		if (num2 != 0f)
		{
			y = num3 / num2;
			if (c.r == num2)
			{
				num4 = (c.g - c.b) / num3;
			}
			else if (c.g == num2)
			{
				num4 = 2f + (c.b - c.r) / num3;
			}
			else
			{
				num4 = 4f + (c.r - c.g) / num3;
			}
			num4 *= 60f;
			if (num4 < 0f)
			{
				num4 += 360f;
			}
			return new Vector3(num4 / 360f, y, z);
		}
		y = 0f;
		num4 = 0f;
		return new Vector3(num4, y, z);
	}

	public static Vector3 HSVToRGB(float h, float S, float V)
	{
		float num;
		for (num = h; num < 0f; num += 360f)
		{
		}
		while (num >= 360f)
		{
			num -= 360f;
		}
		float z;
		float x;
		float y;
		if (V <= 0f)
		{
			y = (x = (z = 0f));
		}
		else if (S <= 0f)
		{
			z = V;
			y = V;
			x = V;
		}
		else
		{
			float num2 = num / 60f;
			int num3 = Mathf.FloorToInt(num2);
			float num4 = num2 - (float)num3;
			float num5 = V * (1f - S);
			float num6 = V * (1f - S * num4);
			float num7 = V * (1f - S * (1f - num4));
			switch (num3 + 1)
			{
			case 0:
				x = V;
				y = num5;
				z = num6;
				break;
			case 1:
				x = V;
				y = num7;
				z = num5;
				break;
			case 2:
				x = num6;
				y = V;
				z = num5;
				break;
			case 3:
				x = num5;
				y = V;
				z = num7;
				break;
			case 4:
				x = num5;
				y = num6;
				z = V;
				break;
			case 5:
				x = num7;
				y = num5;
				z = V;
				break;
			case 6:
				x = V;
				y = num5;
				z = num6;
				break;
			case 7:
				x = V;
				y = num7;
				z = num5;
				break;
			default:
				z = V;
				y = V;
				x = V;
				break;
			}
		}
		return new Vector3(x, y, z);
	}

	public static T GetRandom<T>(this IList<T> arr)
	{
		return (arr.Count != 0) ? arr[Utilities.RandomRange(0, arr.Count)] : default(T);
	}

	public static T GetRandom<T>(this IList<T> arr, System.Random rnd)
	{
		return (arr.Count != 0) ? arr[rnd.Next(arr.Count)] : default(T);
	}

	public static T GetRandom<T>(this IEnumerable<T> list)
	{
		return list.ToList<T>().GetRandom<T>();
	}

	public static T GetRandom<T>(this IEnumerable<T> list, Func<T, int> priority)
	{
		List<T> list2 = list.ToList<T>();
		List<T> list3 = new List<T>();
		int num = int.MaxValue;
		for (int i = 0; i < list2.Count; i++)
		{
			int num2 = priority(list2[i]);
			if (num2 < num)
			{
				list3.Clear();
				list3.Add(list2[i]);
				num = num2;
			}
			else if (num2 == num)
			{
				list3.Add(list2[i]);
			}
		}
		return list3.GetRandom<T>();
	}

	public static string HourString(int Hour)
	{
		if (SDateTime.AMPM)
		{
			int num = Hour;
			string str = "AM";
			if (num > 11)
			{
				str = "PM";
				if (num > 12)
				{
					num -= 12;
				}
			}
			return ((num != 0) ? num : 12).ToString() + " " + str;
		}
		return Hour.ToString("D2");
	}

	public static string ReadAllText(string filename)
	{
		return Utilities.ReadOnlyReadAllText(filename);
	}

	public static void ForEach<T>(this HashSet<T> input, Action<T> action)
	{
		foreach (T obj in input)
		{
			action(obj);
		}
	}

	public static void AddRange<T>(this HashSet<T> input, IList<T> range)
	{
		for (int i = 0; i < range.Count; i++)
		{
			input.Add(range[i]);
		}
	}

	public static void AddRange<T>(this HashSet<T> input, IEnumerable<T> range)
	{
		foreach (T item in range)
		{
			input.Add(item);
		}
	}

	public static void RemoveRange<T>(this HashSet<T> input, IEnumerable<T> range)
	{
		foreach (T item in range)
		{
			input.Remove(item);
		}
	}

	public static void RemoveAll<T>(this HashSet<T> input, Func<T, bool> predicate)
	{
		foreach (T t in input.ToArray<T>())
		{
			if (predicate(t))
			{
				input.Remove(t);
			}
		}
	}

	public static void RemoveAll<T>(this HashList<T> input, Func<T, bool> predicate)
	{
		foreach (T t in input.ToArray<T>())
		{
			if (predicate(t))
			{
				input.Remove(t);
			}
		}
	}

	public static float AverageOrDefault<T>(this List<T> input, Func<T, float> func, float def)
	{
		return (!input.Any<T>()) ? def : input.Average(func);
	}

	public static float AverageOrDefault<T>(this HashSet<T> input, Func<T, float> func, float def)
	{
		return (!input.Any<T>()) ? def : input.Average(func);
	}

	public static int MinOrDefault(this IEnumerable<int> input, int def)
	{
		return (!input.Any<int>()) ? def : input.Min();
	}

	public static float MinOrDefault(this IEnumerable<float> input, float def)
	{
		return (!input.Any<float>()) ? def : input.Min();
	}

	public static int MaxOrDefault(this IEnumerable<int> input, int def)
	{
		return (!input.Any<int>()) ? def : input.Max();
	}

	public static float MaxOrDefault(this IEnumerable<float> input, float def)
	{
		return (!input.Any<float>()) ? def : input.Max();
	}

	public static HashSet<T> ToHashSet<T>(this IEnumerable<T> list)
	{
		HashSet<T> hashSet = new HashSet<T>();
		hashSet.AddRange(list.ToList<T>());
		return hashSet;
	}

	public static SHashSet<T> ToSHashSet<T>(this IEnumerable<T> list)
	{
		SHashSet<T> shashSet = new SHashSet<T>();
		shashSet.AddRange(list.ToList<T>());
		return shashSet;
	}

	public static string WriteMultipleFiles(string filename, Dictionary<string, byte[]> data)
	{
		try
		{
			using (FileStream fileStream = File.Create(filename))
			{
				foreach (KeyValuePair<string, byte[]> keyValuePair in data)
				{
					byte[] bytesFromString = Utilities.GetBytesFromString(keyValuePair.Key);
					fileStream.Write(Utilities.GetBytesFromInt(bytesFromString.Length), 0, 4);
					fileStream.Write(bytesFromString, 0, bytesFromString.Length);
					fileStream.Write(Utilities.GetBytesFromInt(keyValuePair.Value.Length), 0, 4);
					fileStream.Write(keyValuePair.Value, 0, keyValuePair.Value.Length);
				}
				fileStream.Flush();
			}
		}
		catch (Exception ex)
		{
			return ex.Message;
		}
		return null;
	}

	public static byte[] ReadData(string filename, string header)
	{
		using (FileStream fileStream = File.OpenRead(filename))
		{
			byte[] array;
			for (;;)
			{
				array = new byte[4];
				if (fileStream.Read(array, 0, array.Length) == 0)
				{
					break;
				}
				int intFromBytes = Utilities.GetIntFromBytes(array);
				byte[] array2 = new byte[intFromBytes];
				fileStream.Read(array2, 0, intFromBytes);
				fileStream.Read(array, 0, array.Length);
				string stringFromBytes = Utilities.GetStringFromBytes(array2);
				if (stringFromBytes.Equals(header))
				{
					goto Block_3;
				}
				fileStream.Position += (long)Utilities.GetIntFromBytes(array);
			}
			goto IL_AF;
			Block_3:
			int intFromBytes2 = Utilities.GetIntFromBytes(array);
			byte[] array3 = new byte[intFromBytes2];
			fileStream.Read(array3, 0, array3.Length);
			return array3;
		}
		IL_AF:
		return null;
	}

	public static byte[] GetBytesFromInt(int integer)
	{
		byte[] bytes = BitConverter.GetBytes(integer);
		if (BitConverter.IsLittleEndian)
		{
			Array.Reverse(bytes);
		}
		return bytes;
	}

	public static int GetIntFromBytes(byte[] bytes)
	{
		if (BitConverter.IsLittleEndian)
		{
			Array.Reverse(bytes);
		}
		return BitConverter.ToInt32(bytes, 0);
	}

	public static byte[] GetBytesFromFloats(float[] floats)
	{
		int num = 4;
		byte[] array = new byte[num * floats.Length];
		for (int i = 0; i < floats.Length; i++)
		{
			byte[] bytes = BitConverter.GetBytes(floats[i]);
			for (int j = 0; j < bytes.Length; j++)
			{
				int num2 = j;
				if (BitConverter.IsLittleEndian)
				{
					num2 = bytes.Length - 1 - j;
				}
				array[i * num + j] = bytes[num2];
			}
		}
		return array;
	}

	public static float[] GetFloatsFromBytes(byte[] bytes)
	{
		int num = 4;
		int num2 = bytes.Length / num;
		float[] array = new float[num2];
		if (BitConverter.IsLittleEndian)
		{
			for (int i = 0; i < num2; i++)
			{
				Array.Reverse(bytes, i * num, num);
			}
		}
		for (int j = 0; j < num2; j++)
		{
			array[j] = BitConverter.ToSingle(bytes, j * num);
		}
		return array;
	}

	public static byte[] GetBytesFromString(string str)
	{
		byte[] array = new byte[str.Length * 2];
		Buffer.BlockCopy(str.ToCharArray(), 0, array, 0, array.Length);
		return array;
	}

	public static string GetStringFromBytes(byte[] bytes)
	{
		char[] array = new char[bytes.Length / 2];
		Buffer.BlockCopy(bytes, 0, array, 0, bytes.Length);
		return new string(array);
	}

	public static float Dist(this Vector2 p1, Vector2 p2)
	{
		return Mathf.Sqrt(p1.SqrDist(p2));
	}

	public static float SqrDist(this Vector2 p1, Vector2 p2)
	{
		float num = p2.x - p1.x;
		float num2 = p2.y - p1.y;
		return num * num + num2 * num2;
	}

	public static bool ContainsEntirely(this Rect rect, Vector2 p)
	{
		return p.x >= rect.xMin && p.x <= rect.xMax && p.y >= rect.yMin && p.y <= rect.yMax;
	}

	public static bool ContainsEntirely(this Rect rect, Vector2 p, float expand)
	{
		return p.x >= rect.xMin - expand && p.x <= rect.xMax + expand && p.y >= rect.yMin - expand && p.y <= rect.yMax + expand;
	}

	public static bool CompletelyWithin(this Rect rect, Vector2 p)
	{
		return p.x > rect.xMin && p.x < rect.xMax && p.y > rect.yMin && p.y < rect.yMax;
	}

	public static bool CompletelyWithin(this Rect rect, float x, float y)
	{
		return x > rect.xMin && x < rect.xMax && y > rect.yMin && y < rect.yMax;
	}

	public static bool Contains(this Rect rect, float x, float y)
	{
		return x >= rect.xMin && x < rect.xMax && y >= rect.yMin && y < rect.yMax;
	}

	public static float FullAngleBetween(this Vector2 b, Vector2 a, Vector2 c)
	{
		float num = Mathf.Acos(Vector2.Dot((a - b).normalized, (c - b).normalized)) * 57.29578f;
		if (Utilities.isLeft(b, a, c) > 0)
		{
			num += 180f;
		}
		return num;
	}

	public static float AngleBetween(this Vector2 b, Vector2 a, Vector2 c)
	{
		float f = Mathf.Acos(Vector2.Dot((a - b).normalized, (c - b).normalized)) * 57.29578f;
		return Mathf.Abs(f);
	}

	public static Vector2? ProjectToLine(Vector2 p, Vector2 a, Vector2 b)
	{
		float num = Mathf.Min(a.x, b.x);
		float num2 = Mathf.Max(a.x, b.x);
		float num3 = Mathf.Min(a.y, b.y);
		float num4 = Mathf.Max(a.y, b.y);
		if ((p.x < num || p.x > num2) && (p.y < num3 || p.y > num4))
		{
			return null;
		}
		Vector2 zero = Vector2.zero;
		if (a.x == b.x && a.y == b.y)
		{
			a = new Vector2(a.x + 1E-05f, a.y);
		}
		float num5 = (p.x - a.x) * (b.x - a.x) + (p.y - a.y) * (b.y - a.y);
		float num6 = Mathf.Pow(b.x - a.x, 2f) + Mathf.Pow(b.y - a.y, 2f);
		num5 /= num6;
		zero.x = a.x + num5 * (b.x - a.x);
		zero.y = a.y + num5 * (b.y - a.y);
		bool flag = zero.x >= num && zero.x <= num2 && zero.y >= num3 && zero.y <= num4;
		return (!flag) ? null : new Vector2?(zero);
	}

	public static Vector2 ProjectToLineEndless(Vector2 p, Vector2 a, Vector2 b)
	{
		if (a.x == b.x && a.y == b.y)
		{
			a = new Vector2(a.x + 1E-05f, a.y);
		}
		float num = (p.x - a.x) * (b.x - a.x) + (p.y - a.y) * (b.y - a.y);
		float num2 = Mathf.Pow(b.x - a.x, 2f) + Mathf.Pow(b.y - a.y, 2f);
		num /= num2;
		return new Vector2(a.x + num * (b.x - a.x), a.y + num * (b.y - a.y));
	}

	public static int isLeft(Vector2 p1, Vector2 p2, Vector2 p3)
	{
		float num = (p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y);
		if (Mathf.Approximately(num, 0f))
		{
			return 0;
		}
		if (num > 0f)
		{
			return 1;
		}
		return -1;
	}

	public static bool Overlap(float a, float b, float c, float d)
	{
		return !Mathf.Approximately(Mathf.Max(0f, Mathf.Min(b, d) - Mathf.Max(a, c)), 0f);
	}

	public static bool RelaxedOverlap(float a, float b, float c, float d)
	{
		return !Mathf.Max(0f, Mathf.Min(b, d) - Mathf.Max(a, c)).Appx(0f, 0.0001f);
	}

	public static bool StrictOverlap(float a, float b, float c, float d)
	{
		return b == c || a == d || !Mathf.Approximately(Mathf.Max(0f, Mathf.Min(b, d) - Mathf.Max(a, c)), 0f);
	}

	public static bool IsStrictlyInside(Vector2 p, Vector2[] polygon)
	{
		bool flag = false;
		int num = polygon.Count<Vector2>() - 1;
		for (int i = 0; i < polygon.Count<Vector2>(); i++)
		{
			if (((polygon[i].y < p.y && polygon[num].y > p.y) || (polygon[num].y < p.y && polygon[i].y > p.y)) && polygon[i].x + (p.y - polygon[i].y) / (polygon[num].y - polygon[i].y) * (polygon[num].x - polygon[i].x) < p.x)
			{
				flag = !flag;
			}
			num = i;
		}
		return flag;
	}

	public static bool IsInside(Vector2 p, Vector2[] polygon)
	{
		bool flag = false;
		int i = 0;
		int num = polygon.Length - 1;
		while (i < polygon.Length)
		{
			if (polygon[i].y > p.y != polygon[num].y > p.y && p.x < (polygon[num].x - polygon[i].x) * (p.y - polygon[i].y) / (polygon[num].y - polygon[i].y) + polygon[i].x)
			{
				flag = !flag;
			}
			num = i++;
		}
		return flag;
	}

	public static bool IsInside(Vector2 p, List<Vector2> polygon)
	{
		bool flag = false;
		int i = 0;
		int index = polygon.Count - 1;
		while (i < polygon.Count)
		{
			if (polygon[i].y > p.y != polygon[index].y > p.y && p.x < (polygon[index].x - polygon[i].x) * (p.y - polygon[i].y) / (polygon[index].y - polygon[i].y) + polygon[i].x)
			{
				flag = !flag;
			}
			index = i++;
		}
		return flag;
	}

	public static bool IsInside(Vector2 p, List<WallEdge> polygon)
	{
		bool flag = false;
		int i = 0;
		int index = polygon.Count - 1;
		while (i < polygon.Count)
		{
			if (polygon[i].Pos.y > p.y != polygon[index].Pos.y > p.y && p.x < (polygon[index].Pos.x - polygon[i].Pos.x) * (p.y - polygon[i].Pos.y) / (polygon[index].Pos.y - polygon[i].Pos.y) + polygon[i].Pos.x)
			{
				flag = !flag;
			}
			index = i++;
		}
		return flag;
	}

	public static bool IsInsideSnap(Vector2 p, Vector2[] polygon, float dist)
	{
		int num = 0;
		for (int i = 0; i < polygon.Length; i++)
		{
			Vector2 vector = polygon[i];
			Vector2 vector2 = polygon[(i + 1) % polygon.Length];
			Vector2? vector3 = Utilities.ProjectToLine(p, vector, vector2);
			if (vector3 != null && vector3.Value.Dist(p) < dist)
			{
				return false;
			}
			if (vector.y <= p.y)
			{
				if (vector2.y > p.y && Utilities.isLeft(vector, vector2, p) > 0)
				{
					num++;
				}
			}
			else if (vector2.y <= p.y && Utilities.isLeft(vector, vector2, p) < 0)
			{
				num--;
			}
		}
		return num != 0;
	}

	public static float Cross(this Vector2 v1, Vector2 v2)
	{
		return v1.x * v2.y - v1.y * v2.x;
	}

	public static bool IsZero(this float x)
	{
		return x >= -Mathf.Epsilon && x <= Mathf.Epsilon;
	}

	public static Vector2? GetLineIntersection(Vector2 p, Vector2 p2, Vector2 q, Vector2 q2)
	{
		Vector2 vector = p2 - p;
		Vector2 v = q2 - q;
		float num = vector.Cross(v);
		if (num.IsZero())
		{
			return null;
		}
		float num2 = (q - p).Cross(v) / num;
		float num3 = (q - p).Cross(vector) / num;
		if (!num.IsZero() && 0f <= num2 && num2 <= 1f && 0f <= num3 && num3 <= 1f)
		{
			return new Vector2?(p + vector * num2);
		}
		return null;
	}

	public static bool FasterLineSegmentIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
	{
		float num = p2.x - p1.x;
		float num2 = p2.y - p1.y;
		float num3 = p3.x - p4.x;
		float num4 = p3.y - p4.y;
		float num5 = p1.x - p3.x;
		float num6 = p1.y - p3.y;
		float num7 = num4 * num5 - num3 * num6;
		float num8 = num2 * num3 - num * num4;
		if (num8.IsZero())
		{
			return false;
		}
		float num9 = num * num6 - num2 * num5;
		float num10 = num8;
		if (num10.IsZero())
		{
			return false;
		}
		if (num8 > 0f)
		{
			if (num7 < 0f || num7 > num8)
			{
				return false;
			}
		}
		else if (num7 > 0f || num7 < num8)
		{
			return false;
		}
		if (num10 > 0f)
		{
			if (num9 < 0f || num9 > num10)
			{
				return false;
			}
		}
		else if (num9 > 0f || num9 < num10)
		{
			return false;
		}
		return true;
	}

	public static bool LinesIntersect(Vector2 p, Vector2 p2, Vector2 q, Vector2 q2, bool allowColinear, bool includePoints)
	{
		return Utilities.LinesIntersect(p, p2, q, q2, allowColinear, includePoints, includePoints);
	}

	public static bool LinesIntersect(Vector2 p, Vector2 p2, Vector2 q, Vector2 q2, bool allowColinear, bool includePointsA, bool includePointsB)
	{
		Vector2 vector = p2 - p;
		Vector2 vector2 = q2 - q;
		float num = vector.Cross(vector2);
		float x = (q - p).Cross(vector);
		if (num.IsZero() && x.IsZero())
		{
			if (!allowColinear)
			{
				float num2 = Vector2.Dot(q - p, vector);
				float num3 = Vector2.Dot(vector, vector);
				float num4 = Vector2.Dot(p - q, vector2);
				float num5 = Vector2.Dot(vector2, vector2);
				if (((0f <= num2 && num2 <= num3) || (0f <= num4 && num4 <= num5)) && vector.normalized != vector2.normalized)
				{
					return true;
				}
			}
			return false;
		}
		if (num.IsZero() && !x.IsZero())
		{
			return false;
		}
		float num6 = (q - p).Cross(vector2) / num;
		float num7 = (q - p).Cross(vector) / num;
		return !num.IsZero() && ((!includePointsA) ? (0f < num6 && num6 < 1f && (!Mathf.Approximately(num6, 0f) && !Mathf.Approximately(num6, 1f))) : (0f <= num6 && num6 <= 1f)) && ((!includePointsB) ? (0f < num7 && num7 < 1f && (!Mathf.Approximately(num7, 0f) && !Mathf.Approximately(num7, 1f))) : (0f <= num7 && num7 <= 1f));
	}

	public static List<Vector2> ComputeConvexHull(List<Vector2> points)
	{
		List<Vector2> list = new List<Vector2>();
		if (points.Count == 0)
		{
			return list;
		}
		int num = 0;
		int num2 = 0;
		foreach (Vector2 vector in from x in points
		orderby x.x descending, x.y descending
		select x)
		{
			Vector2 b;
			while (num >= 2 && ((b = list.Last<Vector2>()) - list[list.Count - 2]).Cross(vector - b) >= 0f)
			{
				list.RemoveAt(list.Count - 1);
				num--;
			}
			list.Add(vector);
			num++;
			while (num2 >= 2 && ((b = list.First<Vector2>()) - list[1]).Cross(vector - b) <= 0f)
			{
				list.RemoveAt(0);
				num2--;
			}
			if (num2 != 0)
			{
				list.Insert(0, vector);
			}
			num2++;
		}
		list.RemoveAt(list.Count - 1);
		return list;
	}

	public static bool Clockwise(List<Vector2> s)
	{
		float num = 0f;
		for (int i = 0; i < s.Count; i++)
		{
			Vector2 vector = s[i];
			Vector2 vector2 = s[(i + 1) % s.Count];
			num += (vector2.x - vector.x) * (vector2.y + vector.y);
		}
		return num > 0f;
	}

	public static float SumSafe<T>(this IList<T> list, Func<T, float> convert)
	{
		float num = 0f;
		for (int i = 0; i < list.Count; i++)
		{
			num += convert(list[i]);
		}
		return num;
	}

	public static float SumSafe<T>(this IEnumerable<T> list, Func<T, float> convert)
	{
		float num = 0f;
		foreach (T arg in list)
		{
			num += convert(arg);
		}
		return num;
	}

	public static int SumSafe<T>(this IList<T> list, Func<T, int> convert)
	{
		int num = 0;
		for (int i = 0; i < list.Count; i++)
		{
			num += convert(list[i]);
		}
		return num;
	}

	public static int SumSafe<T>(this IEnumerable<T> list, Func<T, int> convert)
	{
		int num = 0;
		foreach (T arg in list)
		{
			num += convert(arg);
		}
		return num;
	}

	public static float MaxSafe<T>(this IEnumerable<T> list, Func<T, float> convert, float defValue = -3.40282347E+38f)
	{
		float num = defValue;
		foreach (T arg in list)
		{
			num = Mathf.Max(num, convert(arg));
		}
		return num;
	}

	public static void AddRange<T>(this IList<T> l, params T[] a)
	{
		if (a.Length == 1)
		{
			l.Add(a[0]);
		}
		else
		{
			for (int i = 0; i < a.Length; i++)
			{
				l.Add(a[i]);
			}
		}
	}

	public static float MaxSafe<T>(this IList<T> list, Func<T, float> convert, float defValue = -3.40282347E+38f, float empty = 0f)
	{
		if (list.Count == 0)
		{
			return empty;
		}
		float num = defValue;
		for (int i = 0; i < list.Count; i++)
		{
			float num2 = convert(list[i]);
			if (num2 > num)
			{
				num = num2;
			}
		}
		return num;
	}

	public static int MaxSafeInt<T>(this IList<T> list, Func<T, int> convert, int defValue = -2147483648, int empty = 0)
	{
		if (list.Count == 0)
		{
			return empty;
		}
		int num = defValue;
		for (int i = 0; i < list.Count; i++)
		{
			int num2 = convert(list[i]);
			if (num2 > num)
			{
				num = num2;
			}
		}
		return num;
	}

	public static float MinSafe<T>(this IList<T> list, Func<T, float> convert, float defValue = 3.40282347E+38f, float empty = 0f)
	{
		if (list.Count == 0)
		{
			return empty;
		}
		float num = defValue;
		for (int i = 0; i < list.Count; i++)
		{
			float num2 = convert(list[i]);
			if (num2 < num)
			{
				num = num2;
			}
		}
		return num;
	}

	public static int MinSafeInt<T>(this IList<T> list, Func<T, int> convert, int defValue = 2147483647, int empty = 0)
	{
		if (list.Count == 0)
		{
			return empty;
		}
		int num = defValue;
		for (int i = 0; i < list.Count; i++)
		{
			int num2 = convert(list[i]);
			if (num2 < num)
			{
				num = num2;
			}
		}
		return num;
	}

	public static float PolygonArea(IList<Vector2> polygon)
	{
		float num = 0f;
		for (int i = 0; i < polygon.Count; i++)
		{
			int index = (i + 1) % polygon.Count;
			num += (polygon[index].x - polygon[i].x) * (polygon[index].y + polygon[i].y) / 2f;
		}
		return Mathf.Abs(num);
	}

	public static Vector3 MinVector(Vector3 v, Vector3 v2)
	{
		return new Vector3(Mathf.Min(v.x, v2.x), Mathf.Min(v.y, v2.y), Mathf.Min(v.z, v2.z));
	}

	public static Vector3 MaxVector(Vector3 v, Vector3 v2)
	{
		return new Vector3(Mathf.Max(v.x, v2.x), Mathf.Max(v.y, v2.y), Mathf.Max(v.z, v2.z));
	}

	public static IEnumerable<T> Concate<T>(this IEnumerable<T> input, T item)
	{
		foreach (T nItem in input)
		{
			yield return nItem;
		}
		yield return item;
		yield break;
	}

	public static void VBOToMesh(List<UIVertex> vertices, Mesh result)
	{
		VertexHelper vertexHelper = new VertexHelper();
		for (int i = 0; i < vertices.Count; i++)
		{
			vertexHelper.AddVert(vertices[i]);
			if (i % 4 == 0)
			{
				vertexHelper.AddTriangle(i, i + 1, i + 2);
				vertexHelper.AddTriangle(i + 2, i + 3, i);
			}
		}
		vertexHelper.FillMesh(result);
	}

	public static void VBOToHelper(List<UIVertex> vertices, VertexHelper result)
	{
		for (int i = 0; i < vertices.Count; i++)
		{
			result.AddVert(vertices[i]);
			if (i % 4 == 0)
			{
				result.AddTriangle(i, i + 1, i + 2);
				result.AddTriangle(i + 2, i + 3, i);
			}
		}
	}

	public static PolyTree HolePolygon(Vector2[] polygon, List<Vector2[]> holes, bool fixHoles, List<Vector3> holeTr)
	{
		Clipper clipper = new Clipper(0);
		clipper.AddPath((from x in polygon
		select new IntPoint((double)(x.x * Utilities.ClippingScaleFactor), (double)(x.y * Utilities.ClippingScaleFactor))).ToList<IntPoint>(), 0, true);
		Utilities.FixHoleBoundary(holes, polygon);
		if (fixHoles)
		{
			Utilities.FixHoles2(holes, holeTr);
		}
		else
		{
			Utilities.FixHoles(holes, holeTr);
		}
		clipper.AddPaths((from y in holes
		select (from x in y
		select new IntPoint((double)(x.x * Utilities.ClippingScaleFactor), (double)(x.y * Utilities.ClippingScaleFactor))).ToList<IntPoint>()).ToList<List<IntPoint>>(), 1, true);
		PolyTree polyTree = new PolyTree();
		clipper.Execute(2, polyTree, 2, 2);
		return polyTree;
	}

	public static KeyValuePair<Vector2[], int[]> SubtractAndTriangulate(Vector2[] polygon, List<Vector2[]> holes, bool fixHoles, bool divide, List<Vector3> holeTr = null)
	{
		int t = 0;
		List<Vector2> list = new List<Vector2>();
		List<int> list2 = new List<int>();
		PolyTree polyTree = Utilities.HolePolygon(polygon, holes, fixHoles, holeTr);
		for (int i = 0; i < polyTree.Childs.Count; i++)
		{
			t = Utilities.ProcessPolygon(polyTree.Childs[i], t, list, list2, 0, divide);
		}
		return new KeyValuePair<Vector2[], int[]>((from x in list
		select x * (1f / Utilities.ClippingScaleFactor)).ToArray<Vector2>(), list2.ToArray());
	}

	private static bool PolyInPoly(List<Vector2> p1, Vector2[] outer)
	{
		for (int i = 0; i < p1.Count; i++)
		{
			if (!Utilities.IsInside(p1[i], outer))
			{
				return false;
			}
		}
		return true;
	}

	private static void DrawPolygon(List<Vector2> v, float y, Color col)
	{
		for (int i = 0; i < v.Count; i++)
		{
			Vector2 vector = v[i] * (1f / Utilities.ClippingScaleFactor);
			Vector2 vector2 = v[(i + 1) % v.Count] * (1f / Utilities.ClippingScaleFactor);
			Debug.DrawLine(new Vector3(vector.x, y, vector.y), new Vector3(vector2.x, y, vector2.y), col, 30f);
		}
	}

	private static void FixHoleBoundary(List<Vector2[]> holes, Vector2[] polygon)
	{
		float num = 0.5f / Utilities.ClippingScaleFactor;
		float d = 50f / Utilities.ClippingScaleFactor;
		for (int i = 0; i < holes.Count; i++)
		{
			for (int j = 0; j < holes[i].Length; j++)
			{
				for (int k = 0; k < polygon.Length; k++)
				{
					int num2 = (k + 1) % polygon.Length;
					if (holes[i][j] != polygon[k] && holes[i][j] != polygon[num2])
					{
						Vector2? vector = Utilities.ProjectToLine(holes[i][j], polygon[k], polygon[num2]);
						if (vector != null && (vector.Value - holes[i][j]).sqrMagnitude < num)
						{
							holes[i][j] = holes[i][j] - (polygon[num2] - polygon[k]).Turn90().normalized * d;
						}
					}
					num2 = (j + 1) % holes[i].Length;
					int num3 = (k != 0) ? (k - 1) : (polygon.Length - 1);
					int num4 = (k + 1) % polygon.Length;
					if (holes[i][j] != polygon[k] && holes[i][num2] != polygon[k])
					{
						Vector2? vector2 = Utilities.ProjectToLine(polygon[k], holes[i][j], holes[i][num2]);
						if (vector2 != null && (vector2.Value - polygon[k]).sqrMagnitude < num)
						{
							if (!Utilities.Alike(polygon[num3], polygon[k], polygon[num4], num) || !Utilities.LinesIntersect(holes[i][j], holes[i][num2], polygon[k] + (polygon[num3] - polygon[k]) * 100f, polygon[k] + (polygon[num4] - polygon[k]) * 100f, true, false))
							{
								List<Vector2> list = holes[i].ToList<Vector2>();
								list.Insert(j + 1, polygon[k]);
								list[j + 1] = list[j + 1] - ((polygon[num4] - polygon[k]).Turn90().normalized + (polygon[k] - polygon[num3]).Turn90().normalized) * d;
								holes[i] = list.ToArray();
								j++;
							}
						}
					}
				}
			}
		}
	}

	private static void AddSquare(Vector2 p, List<Vector2[]> holes)
	{
		Vector2[] item = new Vector2[]
		{
			new Vector2(p.x, p.y + 0.05f),
			new Vector2(p.x - 0.05f, p.y),
			new Vector2(p.x, p.y - 0.05f),
			new Vector2(p.x + 0.05f, p.y)
		};
		holes.Add(item);
	}

	private static void FixHoles2(List<Vector2[]> holes, List<Vector3> holeTr)
	{
		float num = 1f / Utilities.ClippingScaleFactor;
		int count = holes.Count;
		for (int i = 0; i < count; i++)
		{
			Vector3 a = Vector3.zero;
			if (holeTr != null)
			{
				a = holeTr[i];
			}
			int j = i + 1;
			while (j < count)
			{
				if (holeTr == null)
				{
					goto IL_68;
				}
				Vector3 b = holeTr[j];
				if ((a - b).sqrMagnitude <= 16f)
				{
					goto IL_68;
				}
				IL_2AC:
				j++;
				continue;
				IL_68:
				for (int k = 0; k < holes[i].Length; k++)
				{
					for (int l = 0; l < holes[j].Length; l++)
					{
						int num2 = (k + 1) % holes[i].Length;
						int num3 = (l + 1) % holes[j].Length;
						float num4 = holes[i][k].SqrDist(holes[j][l]);
						if (num4 < num)
						{
							Utilities.AddSquare(holes[i][k], holes);
							break;
						}
						if (num4 >= num && holes[i][k].SqrDist(holes[j][num3]) >= num)
						{
							Vector2? vector = Utilities.ProjectToLine(holes[i][k], holes[j][l], holes[j][num3]);
							if (vector != null && vector.Value.SqrDist(holes[i][k]) < num)
							{
								Utilities.AddSquare(holes[i][k], holes);
								break;
							}
						}
						if (num4 >= num && holes[j][l].SqrDist(holes[i][num2]) >= num)
						{
							Vector2? vector2 = Utilities.ProjectToLine(holes[j][l], holes[i][k], holes[i][num2]);
							if (vector2 != null && vector2.Value.SqrDist(holes[j][l]) < num)
							{
								Utilities.AddSquare(holes[j][l], holes);
							}
						}
					}
				}
				goto IL_2AC;
			}
		}
	}

	private static void FixHoles(List<Vector2[]> holes, List<Vector3> holeTr)
	{
		float num = 1f / Utilities.ClippingScaleFactor;
		for (int i = 0; i < holes.Count; i++)
		{
			Vector3 a = Vector3.zero;
			if (holeTr != null)
			{
				a = holeTr[i];
			}
			int j = i + 1;
			while (j < holes.Count)
			{
				if (holeTr == null)
				{
					goto IL_5F;
				}
				Vector3 b = holeTr[j];
				if ((a - b).sqrMagnitude <= 16f)
				{
					goto IL_5F;
				}
				IL_495:
				j++;
				continue;
				IL_5F:
				for (int k = 0; k < holes[i].Length; k++)
				{
					for (int l = 0; l < holes[j].Length; l++)
					{
						int num2 = (k != 0) ? (k - 1) : (holes[i].Length - 1);
						int num3 = (l != 0) ? (l - 1) : (holes[j].Length - 1);
						int num4 = (k + 1) % holes[i].Length;
						int num5 = (l + 1) % holes[j].Length;
						float num6 = holes[i][k].SqrDist(holes[j][l]);
						if (num6 < num && holes[i][num4].SqrDist(holes[j][num3]) >= num && holes[i][num2].SqrDist(holes[j][num5]) >= num)
						{
							Vector2 b2 = holes[j][l];
							holes[i][k] = holes[i][k] - ((holes[i][num2] + holes[i][num4]) * 0.5f - b2).normalized * (7f / Utilities.ClippingScaleFactor);
						}
						else if (num6 < num)
						{
							holes[i][k] = holes[j][l];
						}
						if (num6 >= num && holes[i][k].SqrDist(holes[j][num5]) >= num)
						{
							Vector2? vector = Utilities.ProjectToLine(holes[i][k], holes[j][l], holes[j][num5]);
							if (vector != null && vector.Value.SqrDist(holes[i][k]) < num)
							{
								holes[i][k] = holes[i][k] + (holes[j][num5] - holes[j][l]).Turn90().normalized * (7f / Utilities.ClippingScaleFactor);
								break;
							}
						}
						if (num6 >= num && holes[j][l].SqrDist(holes[i][num4]) >= num)
						{
							Vector2? vector2 = Utilities.ProjectToLine(holes[j][l], holes[i][k], holes[i][num4]);
							if (vector2 != null && vector2.Value.SqrDist(holes[j][l]) < num)
							{
								holes[j][l] = holes[j][l] + (holes[i][num4] - holes[i][k]).Turn90().normalized * (7f / Utilities.ClippingScaleFactor);
							}
						}
					}
				}
				goto IL_495;
			}
		}
	}

	public static Vector2 Turn90(this Vector2 v)
	{
		return new Vector2(-v.y, v.x);
	}

	private static void FixParallelIssue(List<Vector2> v)
	{
		Dictionary<object, int> dictionary = new Dictionary<object, int>();
		for (int i = 0; i < v.Count; i++)
		{
			int num = -1;
			if (dictionary.TryGetValue(v[i], out num))
			{
				for (int j = i; j > num; j--)
				{
					dictionary.Remove(v[j]);
					v.RemoveAt(j);
				}
				i = num;
			}
			else
			{
				dictionary[v[i]] = i;
			}
		}
	}

	private static int ProcessPolygon(PolyNode poly, int t, List<Vector2> finalPolygon, List<int> triangulation, int layer, bool divide)
	{
		List<Vector2> list = (from x in poly.Contour
		select new Vector2((float)x.X, (float)x.Y)).ToList<Vector2>();
		if (Utilities.Clockwise(list))
		{
			list.Reverse();
		}
		if (divide)
		{
			Utilities.EnsureLength(list, 4f * Utilities.ClippingScaleFactor);
		}
		Utilities.FixParallelIssue(list);
		Triangulator triangulator = new Triangulator(list);
		Dictionary<PolyNode, List<Vector2>> dictionary = poly.Childs.ToDictionary((PolyNode x) => x, (PolyNode x) => (from z in x.Contour
		select new Vector2((float)z.X, (float)z.Y)).ToList<Vector2>());
		for (int i = 0; i < poly.Childs.Count; i++)
		{
			PolyNode polyNode = poly.Childs[i];
			for (int j = 0; j < polyNode.Childs.Count; j++)
			{
				PolyNode polyNode2 = polyNode.Childs[j];
				Vector2[] outer = (from x in polyNode2.Contour
				select new Vector2((float)x.X, (float)x.Y)).ToArray<Vector2>();
				List<KeyValuePair<PolyNode, List<Vector2>>> list2 = dictionary.ToList<KeyValuePair<PolyNode, List<Vector2>>>();
				for (int k = 0; k < list2.Count; k++)
				{
					KeyValuePair<PolyNode, List<Vector2>> keyValuePair = list2[k];
					if (keyValuePair.Key != polyNode && Utilities.PolyInPoly(keyValuePair.Value, outer))
					{
						dictionary.Remove(keyValuePair.Key);
						polyNode2.AddChild(keyValuePair.Key);
					}
				}
			}
		}
		foreach (PolyNode polyNode3 in dictionary.Keys)
		{
			for (int l = 0; l < polyNode3.Childs.Count; l++)
			{
				PolyNode poly2 = polyNode3.Childs[l];
				t = Utilities.ProcessPolygon(poly2, t, finalPolygon, triangulation, layer + 1, divide);
			}
		}
		foreach (List<Vector2> list3 in dictionary.Values)
		{
			if (Utilities.Clockwise(list3))
			{
				list3.Reverse();
			}
			Utilities.FixParallelIssue(list3);
		}
		int[] source = triangulator.Triangulate(from x in dictionary.Values
		select x.ToArray(), new DTSweepContext());
		finalPolygon.AddRange(triangulator.Points);
		triangulation.AddRange(from x in source
		select x + t);
		t += triangulator.Points.Count;
		return t;
	}

	private static void EnsureLength(List<Vector2> pol, float len)
	{
		for (int i = 0; i < pol.Count; i++)
		{
			int index = (i + 1) % pol.Count;
			if (pol[index].Dist(pol[i]) > len)
			{
				pol.Insert(i + 1, (pol[i] + pol[index]) * 0.5f);
				i--;
			}
		}
	}

	public static Vector2 GetHalfway(Vector2 first, Vector2 second, Vector2 third)
	{
		if ((second - first).normalized == (third - second).normalized)
		{
			Vector2 normalized = (third - second).normalized;
			return second + new Vector2(-normalized.y, normalized.x);
		}
		Vector3 b = new Vector3(second.x, 0f, second.y);
		Vector3 a = new Vector3(first.x, 0f, first.y);
		Vector3 a2 = new Vector3(third.x, 0f, third.y);
		Quaternion a3 = Quaternion.LookRotation(a - b);
		Quaternion b2 = Quaternion.LookRotation(a2 - b);
		Quaternion rotation = Quaternion.Lerp(a3, b2, 0.5f);
		Vector3 a4 = (rotation * Vector3.forward).normalized;
		float y = Vector3.Cross(a - b, a2 - b).y;
		if (!Mathf.Approximately(y, 0f) && y < 0f)
		{
			a4 = -a4;
		}
		return new Vector2(a4.x, a4.z);
	}

	public static List<List<T>> SimpleClustering<T>(this List<T> input, Func<T, T, float> distance, float minDist)
	{
		List<List<T>> list = new List<List<T>>();
		if (input.Count == 0)
		{
			return list;
		}
		list.Add(new List<T>());
		list[0].Add(input[0]);
		for (int i = 1; i < input.Count; i++)
		{
			bool flag = false;
			for (int j = 0; j < list.Count; j++)
			{
				for (int k = 0; k < list[j].Count; k++)
				{
					if (distance(input[i], list[j][k]) < minDist)
					{
						list[j].Add(input[i]);
						flag = true;
						break;
					}
				}
				if (flag)
				{
					break;
				}
			}
			if (!flag)
			{
				list.Add(new List<T>
				{
					input[i]
				});
			}
		}
		for (int l = 0; l < list.Count; l++)
		{
			for (int m = l + 1; m < list.Count; m++)
			{
				for (int n = 0; n < list[l].Count; n++)
				{
					bool flag2 = false;
					for (int num = 0; num < list[m].Count; num++)
					{
						if (distance(list[l][n], list[m][num]) < minDist)
						{
							list[l].AddRange(list[m]);
							list.RemoveAt(m);
							m--;
							flag2 = true;
							break;
						}
					}
					if (flag2)
					{
						break;
					}
				}
			}
		}
		return list;
	}

	public static Vector2 GetTriangleCentroid(IList<Vector2> points)
	{
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		int i = 0;
		int index = 2;
		while (i < 3)
		{
			float num4 = points[i].x * points[index].y - points[index].x * points[i].y;
			num += num4;
			num2 += (points[i].x + points[index].x) * num4;
			num3 += (points[i].y + points[index].y) * num4;
			index = i++;
		}
		if (Mathf.Abs(num) < 0.001f)
		{
			return new Vector2((points[0].x + points[1].x + points[2].x) / 3f, (points[0].y + points[1].y + points[2].y) / 3f);
		}
		num *= 3f;
		return new Vector2(num2 / num, num3 / num);
	}

	public static Vector2 GetPolygonCentroid(IList<WallEdge> polygon)
	{
		if (polygon.Count == 0)
		{
			return Vector2.zero;
		}
		if (polygon.Count == 1)
		{
			return polygon[0].Pos;
		}
		if (polygon.Count == 2)
		{
			return (polygon[0].Pos + polygon[1].Pos) * 0.5f;
		}
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		for (int i = 0; i < polygon.Count; i++)
		{
			float x = polygon[i].Pos.x;
			float y = polygon[i].Pos.y;
			int index = (i + 1) % polygon.Count;
			float x2 = polygon[index].Pos.x;
			float y2 = polygon[index].Pos.y;
			float num4 = x * y2 - x2 * y;
			num3 += num4;
			num += (x + x2) * num4;
			num2 += (y + y2) * num4;
		}
		num3 *= 3f;
		num /= num3;
		num2 /= num3;
		return new Vector2(num, num2);
	}

	public static Vector2 GetPolygonCentroid(IList<Vector2> polygon)
	{
		if (polygon.Count == 0)
		{
			return Vector2.zero;
		}
		if (polygon.Count == 1)
		{
			return polygon[0];
		}
		if (polygon.Count == 2)
		{
			return (polygon[0] + polygon[1]) * 0.5f;
		}
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		for (int i = 0; i < polygon.Count; i++)
		{
			float x = polygon[i].x;
			float y = polygon[i].y;
			int index = (i + 1) % polygon.Count;
			float x2 = polygon[index].x;
			float y2 = polygon[index].y;
			float num4 = x * y2 - x2 * y;
			num3 += num4;
			num += (x + x2) * num4;
			num2 += (y + y2) * num4;
		}
		num3 *= 3f;
		num /= num3;
		num2 /= num3;
		return new Vector2(num, num2);
	}

	public static int RandomRange(int min, int max)
	{
		return Utilities.RNG.Next(min, max);
	}

	public static float RandomRange(float min, float max)
	{
		return min + (float)Utilities.RNG.NextDouble() * (max - min);
	}

	public static float RandomValue
	{
		get
		{
			return (float)Utilities.RNG.NextDouble();
		}
	}

	public static bool Appx(this float a, float b, float eps = 0.0001f)
	{
		return Mathf.Abs(a - b) < eps;
	}

	public static bool Alike(Vector2 p1, Vector2 p2, Vector2 p3)
	{
		return Mathf.Approximately((p2.x - p1.x) * (p3.y - p2.y) - (p3.x - p2.x) * (p2.y - p1.y), 0f);
	}

	public static bool Alike(Vector2 p1, Vector2 p2, Vector2 p3, float delta)
	{
		return ((p2.x - p1.x) * (p3.y - p2.y) - (p3.x - p2.x) * (p2.y - p1.y)).Appx(0f, delta);
	}

	public static string ReadOnlyReadAllText(string filename)
	{
		string result = null;
		using (FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
		{
			using (StreamReader streamReader = new StreamReader(fileStream))
			{
				result = streamReader.ReadToEnd();
			}
		}
		return result;
	}

	public static string GetTeamDescription(List<string> teams)
	{
		if (teams.Count > 1)
		{
			return teams.Count + " " + "Teams".Loc().ToLower();
		}
		if (teams.Count == 1)
		{
			return teams[0];
		}
		return "None".Loc();
	}

	public static string GetTeamDescription(HashSet<string> teams)
	{
		if (teams.Count > 1)
		{
			return teams.Count + " " + "Teams".Loc().ToLower();
		}
		if (teams.Count == 1)
		{
			return teams.First<string>();
		}
		return "None".Loc();
	}

	public static string Bandwidth(this float val)
	{
		if (val < 999f)
		{
			return Mathf.CeilToInt(val) + " Mbps";
		}
		if (val < 999999f)
		{
			return (val / 1000f).ToString("F1") + " Gbps";
		}
		return (val / 1000000f).ToString("F1") + " Tbps";
	}

	public static string WithPlusN(this float val, string format = "N2")
	{
		return (val <= 0f) ? val.ToString(format) : ("+" + val.ToString(format));
	}

	public static string ByteSize(this float val, bool withPlus = false)
	{
		string text = (val >= 0f) ? ((val <= 0f || !withPlus) ? string.Empty : "+") : "-";
		val = Mathf.Abs(val);
		if (val < 1f)
		{
			return text + Mathf.RoundToInt(val * 1024f) + "KB";
		}
		if (val < 1024f)
		{
			return text + val.ToString("F1") + "MB";
		}
		if (val < 1048576f)
		{
			return text + (val / 1024f).ToString("F1") + "GB";
		}
		return text + (val / 1024f / 1024f).ToString("F1") + "TB";
	}

	public static int ChancePerInGameMinute(float chance, float delta)
	{
		float num = delta * GameSettings.GameSpeed;
		if (chance > 1f)
		{
			if (num * chance > Utilities.RandomValue)
			{
				return Mathf.CeilToInt(num * chance * Utilities.RandomValue);
			}
		}
		else if (num * chance > Utilities.RandomValue)
		{
			return 1;
		}
		return 0;
	}

	public static float GetBasePrice(string software, string category, float devtime, float quality)
	{
		SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[software];
		SoftwareCategory softwareCategory = softwareType.Categories[category];
		float? idealPrice = softwareType.GetIdealPrice(softwareCategory);
		float num = GameSettings.Instance.simulation.GetFeatureScore(software, category, SDateTime.Now());
		if (num == 0f)
		{
			num = softwareType.MaxDevTime(category, TimeOfDay.Instance.Year);
		}
		if (idealPrice != null)
		{
			return devtime / num * idealPrice.Value * Mathf.Max(0.25f, quality);
		}
		return devtime / num * 160f * softwareCategory.TimeScale * (1f + Mathf.Pow(softwareCategory.Popularity, 3f)) * softwareType.RandomFactor.MapRange(0f, 1f, 1f, 0.25f, false) * quality.MapRange(0f, 1f, 0.75f, 1f, false);
	}

	public static float BandwidthFactor(this float bandwidth, SDateTime time)
	{
		float num = (float)time.ToInt() / 12f / (float)GameSettings.DaysPerMonth / 24f / 60f;
		return bandwidth * (0.75f + Mathf.Log10(num - 69f));
	}

	public static IEnumerable<T> RandomOffset<T>(this List<T> input)
	{
		int offset = Utilities.RandomRange(0, input.Count);
		for (int i = 0; i < input.Count; i++)
		{
			yield return input[(i + offset) % input.Count];
		}
		yield break;
	}

	public static IEnumerable<T> Offset<T>(this List<T> input, int offset)
	{
		for (int i = 0; i < input.Count; i++)
		{
			yield return input[(i + offset) % input.Count];
		}
		yield break;
	}

	public static float GetHypeFactor(float devtime, float premarketing, float rep, float price, SDateTime releaseDate, SDateTime time)
	{
		float months = Utilities.GetMonths(time, releaseDate);
		if (months > devtime)
		{
			return 0f;
		}
		float num;
		if (months < 0f)
		{
			num = 1f / Mathf.Max(1f, -2f * months);
		}
		else
		{
			num = 1f - months / devtime;
		}
		float num2 = (price >= 1f) ? (devtime / price) : devtime;
		return Mathf.Clamp01(-Mathf.Sqrt(Mathf.Abs(num - 1f)) + 1f) * premarketing * rep * num2;
	}

	public static float Diameter(this Rect r)
	{
		return Mathf.Sqrt(r.width * r.width + r.height * r.height);
	}

	public static T[] ReplaceValue<T>(this T[] arr, int i, T value)
	{
		arr[i] = value;
		return arr;
	}

	public static float? AverageOutlier<T>(this List<T> input, Func<T, float> conv, float rejection)
	{
		if (input.Count == 0)
		{
			return new float?(0f);
		}
		List<float> list = new List<float>(input.Count);
		float num = 0f;
		int num2 = 0;
		for (int i = 0; i < input.Count; i++)
		{
			float num3 = conv(input[i]);
			list.Add(num3);
			num += num3;
			num2++;
		}
		if (num == 0f)
		{
			return new float?(0f);
		}
		num /= (float)num2;
		float num4 = 0f;
		num2 = 0;
		for (int j = 0; j < list.Count; j++)
		{
			if (Mathf.Abs(list[j] - num) / num < rejection)
			{
				num4 += list[j];
				num2++;
			}
		}
		if (num2 == 0)
		{
			return null;
		}
		return new float?(num4 / (float)num2);
	}

	public static float Median(this List<float> list)
	{
		List<float> list2 = (from x in list
		orderby x
		select x).ToList<float>();
		int num = list2.Count / 2;
		return (list2.Count % 2 == 0) ? ((list2[num] + list2[num - 1]) / 2f) : list2[num];
	}

	public static float Median<T>(this IEnumerable<T> list, Func<T, float> conv)
	{
		List<float> list2 = (from x in list
		select conv(x) into x
		orderby x
		select x).ToList<float>();
		int num = list2.Count / 2;
		return (list2.Count % 2 == 0) ? ((list2[num] + list2[num - 1]) / 2f) : list2[num];
	}

	public static List<T> OrderByDependency<T>(this IEnumerable<T> list, Func<T, T[]> dependencies, Func<T, bool> first)
	{
		Dictionary<T, T[]> dictionary = list.ToDictionary((T x) => x, (T x) => dependencies(x));
		List<T> list2 = new List<T>();
		T t = list.First((T x) => first(x));
		list2.Add(t);
		dictionary.Remove(t);
		foreach (KeyValuePair<T, T[]> keyValuePair in (from x in dictionary
		where x.Value.Length == 0
		select x).ToList<KeyValuePair<T, T[]>>())
		{
			list2.Add(keyValuePair.Key);
			dictionary.Remove(keyValuePair.Key);
		}
		while (dictionary.Count > 0)
		{
			foreach (KeyValuePair<T, T[]> keyValuePair2 in dictionary.ToList<KeyValuePair<T, T[]>>())
			{
				for (int i = 0; i < keyValuePair2.Value.Length; i++)
				{
					int num = list2.IndexOf(keyValuePair2.Value[i]);
					if (num > -1)
					{
						list2.Insert(num + 1, keyValuePair2.Key);
						dictionary.Remove(keyValuePair2.Key);
						break;
					}
				}
			}
		}
		return list2;
	}

	public static Vector2 FlattenVector3(this Vector3 v)
	{
		return new Vector2(v.x, v.z);
	}

	public static Vector3 FlattenVector4(this Vector4 v)
	{
		return new Vector3(v.x, v.y, v.z);
	}

	public static Vector3 ReplaceY(this Vector3 v, float y)
	{
		return new Vector3(v.x, y, v.z);
	}

	public static Vector3 ToVector3(this Vector2 v, float y)
	{
		return new Vector3(v.x, y, v.y);
	}

	public static Vector4 ToVector4(this Vector3 v, float w)
	{
		return new Vector4(v.x, v.y, v.z, w);
	}

	public static T GetLastOrDefault<T>(this List<T> l, T def)
	{
		return (l.Count != 0) ? l[l.Count - 1] : def;
	}

	public static void ReverseListPart<T>(this T[] list, int from, int to)
	{
		int num = from + (to - from) / 2;
		for (int i = from; i < num; i++)
		{
			int num2 = to - (i - from) - 1;
			T t = list[i];
			list[i] = list[num2];
			list[num2] = t;
		}
	}

	public static Feature GetFeature(this KeyValuePair<string, string> key)
	{
		return GameSettings.Instance.SoftwareTypes[key.Key].Features[key.Value];
	}

	public static Vector2 GetOffset(Vector2 first, Vector2 second, Vector2 third, float offset, bool angleOffset = false)
	{
		if (Utilities.Alike(first, second, third))
		{
			Vector2 vector = (third - second).normalized * offset;
			return second + new Vector2(-vector.y, vector.x);
		}
		Vector3 b = new Vector3(second.x, 0f, second.y);
		Vector3 a = new Vector3(first.x, 0f, first.y);
		Vector3 a2 = new Vector3(third.x, 0f, third.y);
		Quaternion a3 = Quaternion.LookRotation(a - b);
		Quaternion b2 = Quaternion.LookRotation(a2 - b);
		Quaternion rotation = Quaternion.Lerp(a3, b2, 0.5f);
		float num = 1f;
		if (angleOffset)
		{
			num = Mathf.DeltaAngle(a3.eulerAngles.y, b2.eulerAngles.y);
			num = Mathf.Abs(Mathf.Sin(num * 0.0174532924f / 2f));
		}
		Vector3 a4 = (rotation * Vector3.forward).normalized * (offset / num);
		float y = Vector3.Cross(a - b, a2 - b).y;
		if (!Mathf.Approximately(y, 0f) && y < 0f)
		{
			a4 = -a4;
		}
		return second + new Vector2(a4.x, a4.z);
	}

	public static bool ConvertToBool(this string input, string variableName)
	{
		bool result;
		try
		{
			bool flag = Convert.ToBoolean(input);
			result = flag;
		}
		catch (Exception)
		{
			throw new Exception("Failed converting " + variableName + " to boolean");
		}
		return result;
	}

	public static int ConvertToInt(this string input, string variableName)
	{
		int result;
		try
		{
			int num = Convert.ToInt32(input);
			result = num;
		}
		catch (Exception)
		{
			throw new Exception("Failed converting " + variableName + " to integer");
		}
		return result;
	}

	public static float ConvertToFloat(this string input, string variableName)
	{
		float result;
		try
		{
			double num = Convert.ToDouble(input);
			result = (float)num;
		}
		catch (Exception)
		{
			throw new Exception("Failed converting " + variableName + " to float");
		}
		return result;
	}

	public static bool ConvertToBoolDef(this string input, bool defaultValue)
	{
		bool result;
		try
		{
			bool flag = Convert.ToBoolean(input);
			result = flag;
		}
		catch (Exception)
		{
			result = defaultValue;
		}
		return result;
	}

	public static int ConvertToIntDef(this string input, int defaultValue)
	{
		int result;
		try
		{
			int num = Convert.ToInt32(input);
			result = num;
		}
		catch (Exception)
		{
			result = defaultValue;
		}
		return result;
	}

	public static float ConvertToFloatDef(this string input, float defaultValue)
	{
		float result;
		try
		{
			double num = Convert.ToDouble(input);
			result = (float)num;
		}
		catch (Exception)
		{
			result = defaultValue;
		}
		return result;
	}

	public static bool VeryStrictlyBelow(this float x, float y)
	{
		return !x.Appx(y, 0.00015f) && x < y;
	}

	public static Vector2 ClosestPointOnTriangle(Vector2[] triangle, Vector2 point)
	{
		return Utilities.ClosestPointOnTriangle(triangle[0], triangle[1], triangle[2], point);
	}

	public static Vector2 ClosestPointOnTriangle(Vector2 tr0, Vector2 tr1, Vector2 tr2, Vector2 point)
	{
		float num = tr0.x - point.x;
		float num2 = tr0.y - point.y;
		float num3 = tr1.x - tr0.x;
		float num4 = tr1.y - tr0.y;
		float num5 = tr2.x - tr0.x;
		float num6 = tr2.y - tr0.y;
		float num7 = num3 * num3 + num4 * num4;
		float num8 = num3 * num5 + num4 * num6;
		float num9 = num5 * num5 + num6 * num6;
		float num10 = num * num3 + num2 * num4;
		float num11 = num * num5 + num2 * num6;
		float num12 = num7 * num9 - num8 * num8;
		float num13 = num8 * num11 - num9 * num10;
		float num14 = num8 * num10 - num7 * num11;
		if (num13 + num14 <= num12)
		{
			if (num13 < 0f)
			{
				if (num14 < 0f)
				{
					if (num10 < 0f)
					{
						num14 = 0f;
						if (-num10 >= num7)
						{
							num13 = 1f;
						}
						else
						{
							num13 = -num10 / num7;
						}
					}
					else
					{
						num13 = 0f;
						if (num11 >= 0f)
						{
							num14 = 0f;
						}
						else if (-num11 >= num9)
						{
							num14 = 1f;
						}
						else
						{
							num14 = -num11 / num9;
						}
					}
				}
				else
				{
					num13 = 0f;
					if (num11 >= 0f)
					{
						num14 = 0f;
					}
					else if (-num11 >= num9)
					{
						num14 = 1f;
					}
					else
					{
						num14 = -num11 / num9;
					}
				}
			}
			else if (num14 < 0f)
			{
				num14 = 0f;
				if (num10 >= 0f)
				{
					num13 = 0f;
				}
				else if (-num10 >= num7)
				{
					num13 = 1f;
				}
				else
				{
					num13 = -num10 / num7;
				}
			}
			else
			{
				float num15 = 1f / num12;
				num13 *= num15;
				num14 *= num15;
			}
		}
		else if (num13 < 0f)
		{
			float num16 = num8 + num10;
			float num17 = num9 + num11;
			if (num17 > num16)
			{
				float num18 = num17 - num16;
				float num19 = num7 - 2f * num8 + num9;
				if (num18 >= num19)
				{
					num13 = 1f;
					num14 = 0f;
				}
				else
				{
					num13 = num18 / num19;
					num14 = 1f - num13;
				}
			}
			else
			{
				num13 = 0f;
				if (num17 <= 0f)
				{
					num14 = 1f;
				}
				else if (num11 >= 0f)
				{
					num14 = 0f;
				}
				else
				{
					num14 = -num11 / num9;
				}
			}
		}
		else if (num14 < 0f)
		{
			float num16 = num8 + num11;
			float num17 = num7 + num10;
			if (num17 > num16)
			{
				float num18 = num17 - num16;
				float num19 = num7 - 2f * num8 + num9;
				if (num18 >= num19)
				{
					num14 = 1f;
					num13 = 0f;
				}
				else
				{
					num14 = num18 / num19;
					num13 = 1f - num14;
				}
			}
			else
			{
				num14 = 0f;
				if (num17 <= 0f)
				{
					num13 = 1f;
				}
				else if (num10 >= 0f)
				{
					num13 = 0f;
				}
				else
				{
					num13 = -num10 / num7;
				}
			}
		}
		else
		{
			float num18 = num9 + num11 - num8 - num10;
			if (num18 <= 0f)
			{
				num13 = 0f;
				num14 = 1f;
			}
			else
			{
				float num19 = num7 - 2f * num8 + num9;
				if (num18 >= num19)
				{
					num13 = 1f;
					num14 = 0f;
				}
				else
				{
					num13 = num18 / num19;
					num14 = 1f - num13;
				}
			}
		}
		return new Vector2(tr0.x + num13 * num3 + num14 * num5, tr0.y + num13 * num4 + num14 * num6);
	}

	public static void ForEachEnum<T>(this IEnumerable<T> items, Action<T> action)
	{
		foreach (T obj in items)
		{
			action(obj);
		}
	}

	public static void ForEachEnum<T>(this IList<T> items, Action<T> action)
	{
		for (int i = 0; i < items.Count; i++)
		{
			action(items[i]);
		}
	}

	public static Mesh Duplicate(this Mesh m)
	{
		return new Mesh
		{
			vertices = m.vertices,
			normals = m.normals,
			uv = m.uv,
			uv2 = m.uv2,
			tangents = m.tangents,
			colors = m.colors,
			triangles = m.triangles
		};
	}

	public static float ManhattanDist(this Vector3 v1, Vector3 v2)
	{
		return Mathf.Abs(v1.x - v2.x) + Mathf.Abs(v1.z - v2.z);
	}

	public static float ManhattanDist(this Vector2 v1, Vector2 v2)
	{
		return Mathf.Abs(v1.x - v2.x) + Mathf.Abs(v1.y - v2.y);
	}

	public static float MinDist(this Vector2 v1, Vector2 v2)
	{
		return Mathf.Min(Mathf.Abs(v1.x - v2.x), Mathf.Abs(v1.y - v2.y));
	}

	public static float MaxDist(this Vector2 v1, Vector2 v2)
	{
		return Mathf.Max(Mathf.Abs(v1.x - v2.x), Mathf.Abs(v1.y - v2.y));
	}

	public static Rect GetBounds(this IList<Vector2> list)
	{
		float num = float.MaxValue;
		float num2 = float.MaxValue;
		float num3 = float.MinValue;
		float num4 = float.MinValue;
		for (int i = 0; i < list.Count; i++)
		{
			Vector2 vector = list[i];
			num = Mathf.Min(num, vector.x);
			num2 = Mathf.Min(num2, vector.y);
			num3 = Mathf.Max(num3, vector.x);
			num4 = Mathf.Max(num4, vector.y);
		}
		return new Rect(num, num2, num3 - num, num4 - num2);
	}

	public static bool InBasement(int floor)
	{
		return floor < 0;
	}

	public static Rect Flatten(this Bounds bound)
	{
		return new Rect(bound.min.x, bound.min.z, bound.size.x, bound.size.z);
	}

	public static Color Alpha(this Color c, float a)
	{
		return new Color(c.r, c.g, c.b, a);
	}

	public static void PushRange<T>(this Stack<T> stack, IEnumerable<T> items)
	{
		foreach (T t in items)
		{
			stack.Push(t);
		}
	}

	public static void SetEnum<T>(this Animator obj, string id, T anim)
	{
		obj.SetInteger(id, Convert.ToInt32(anim));
	}

	public static byte[] ReadAllBytes(this BinaryReader reader)
	{
		byte[] result;
		using (MemoryStream memoryStream = new MemoryStream())
		{
			byte[] array = new byte[4096];
			int count;
			while ((count = reader.Read(array, 0, array.Length)) != 0)
			{
				memoryStream.Write(array, 0, count);
			}
			result = memoryStream.ToArray();
		}
		return result;
	}

	public static void Append<T, S>(this Dictionary<T, List<S>> dict, T key, S element)
	{
		List<S> list;
		if (!dict.TryGetValue(key, out list))
		{
			list = new List<S>();
			dict[key] = list;
		}
		list.Add(element);
	}

	public static void Append<T, S>(this Dictionary<T, HashSet<S>> dict, T key, S element)
	{
		HashSet<S> hashSet;
		if (!dict.TryGetValue(key, out hashSet))
		{
			hashSet = new HashSet<S>();
			dict[key] = hashSet;
		}
		hashSet.Add(element);
	}

	public static void Append<T, S>(this Dictionary<T, HashList<S>> dict, T key, S element)
	{
		HashList<S> hashList;
		if (!dict.TryGetValue(key, out hashList))
		{
			hashList = new HashList<S>();
			dict[key] = hashList;
		}
		hashList.Add(element);
	}

	public static Dictionary<T2, T3> Append<T, T2, T3>(this Dictionary<T, Dictionary<T2, T3>> dict, T key)
	{
		Dictionary<T2, T3> dictionary;
		if (!dict.TryGetValue(key, out dictionary))
		{
			dictionary = new Dictionary<T2, T3>();
			dict[key] = dictionary;
		}
		return dictionary;
	}

	public static void AddUp<T>(this Dictionary<T, float> dict, T key, float value)
	{
		float num = 0f;
		if (dict.TryGetValue(key, out num))
		{
			dict[key] = num + value;
		}
		else
		{
			dict[key] = value;
		}
	}

	public static void AddTo<T1, T2>(this Dictionary<T1, T2> dict, T1 key, T2 value, Func<T2, T2, T2> change)
	{
		T2 arg;
		if (dict.TryGetValue(key, out arg))
		{
			dict[key] = change(arg, value);
		}
		else
		{
			dict[key] = value;
		}
	}

	public static void AddUp<T>(this Dictionary<T, uint> dict, T key, uint value)
	{
		uint num = 0u;
		if (dict.TryGetValue(key, out num))
		{
			dict[key] = num + value;
		}
		else
		{
			dict[key] = value;
		}
	}

	public static void AddUp<T>(this Dictionary<T, int> dict, T key, int value = 1)
	{
		int num = 0;
		if (dict.TryGetValue(key, out num))
		{
			dict[key] = num + value;
		}
		else
		{
			dict[key] = value;
		}
	}

	public static int Quantize(this float score, int buckets)
	{
		return Mathf.Clamp(Mathf.FloorToInt(score * (float)buckets), 0, buckets - 1);
	}

	public static bool Any<T>(this IList<T> l, Func<T, bool> predicate)
	{
		for (int i = 0; i < l.Count; i++)
		{
			if (predicate(l[i]))
			{
				return true;
			}
		}
		return false;
	}

	public static int Count<T>(this IList<T> l, Func<T, bool> predicate)
	{
		int num = 0;
		for (int i = 0; i < l.Count; i++)
		{
			if (predicate(l[i]))
			{
				num++;
			}
		}
		return num;
	}

	public static void CopyTo(this Stream source, Stream destination, int bufferSize = 81920)
	{
		byte[] array = new byte[bufferSize];
		int count;
		while ((count = source.Read(array, 0, array.Length)) != 0)
		{
			destination.Write(array, 0, count);
		}
	}

	public static byte[] Compress(this byte[] input)
	{
		byte[] result = null;
		using (MemoryStream memoryStream = new MemoryStream())
		{
			using (GZipStream gzipStream = new GZipStream(memoryStream, CompressionMode.Compress, false))
			{
				gzipStream.Write(input, 0, input.Length);
			}
			result = memoryStream.ToArray();
		}
		return result;
	}

	public static byte[] Decompress(this byte[] input)
	{
		byte[] result = null;
		using (MemoryStream memoryStream = new MemoryStream(input))
		{
			using (GZipStream gzipStream = new GZipStream(memoryStream, CompressionMode.Decompress, false))
			{
				using (MemoryStream memoryStream2 = new MemoryStream())
				{
					gzipStream.CopyTo(memoryStream2, 16384);
					result = memoryStream2.ToArray();
				}
			}
		}
		return result;
	}

	public static float SpreadPercentage(this float p, int spread)
	{
		if (spread == 1 || Mathf.Approximately(p, 1f))
		{
			return p;
		}
		if (p < 0.65f || p > 1.32f)
		{
			return Mathf.Pow(p, 1f / (float)spread);
		}
		return 1f + (p - 1f) / (float)spread;
	}

	public static int Sum(this int[] list)
	{
		int num = 0;
		for (int i = 0; i < list.Length; i++)
		{
			num += list[i];
		}
		return num;
	}

	public static int CountLetter(this string input, char c)
	{
		int num = 0;
		for (int i = 0; i < input.Length; i++)
		{
			if (input[i] == c)
			{
				num++;
			}
		}
		return num;
	}

	public static bool IsValidFloat(this float v)
	{
		return !float.IsInfinity(v) && !float.IsNaN(v);
	}

	public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue defaultValue)
	{
		TValue result;
		if (dict.TryGetValue(key, out result))
		{
			return result;
		}
		return defaultValue;
	}

	public static TValue GetOrNull<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key) where TValue : class
	{
		TValue tvalue;
		return (!dict.TryGetValue(key, out tvalue)) ? ((TValue)((object)null)) : tvalue;
	}

	public static TSource LastOrDefault<TSource>(this IList<TSource> source)
	{
		return (source != null && source.Count != 0) ? source[source.Count - 1] : default(TSource);
	}

	public static IEnumerable<TOut> SelectNotNull<T, TOut>(this IEnumerable<T> input, Func<T, TOut> selector)
	{
		foreach (T i in input)
		{
			TOut o = selector(i);
			if (o != null)
			{
				yield return o;
			}
		}
		yield break;
	}

	public static Dictionary<TKey, TValue> DictionaryNotNull<TIn, TKey, TValue>(this IEnumerable<TIn> input, Func<TIn, TKey> key, Func<TIn, TValue> value)
	{
		Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>();
		foreach (TIn arg in input)
		{
			TValue tvalue = value(arg);
			if (tvalue != null)
			{
				TKey key2 = key(arg);
				dictionary[key2] = tvalue;
			}
		}
		return dictionary;
	}

	public static float WeightOne(this float number, float weight)
	{
		return 1f - weight + weight * number;
	}

	public static void Swap<T>(this HashSet<T> set, T f, T s)
	{
		if (set.Remove(f))
		{
			set.Add(s);
		}
	}

	public static string ToDB(this float num)
	{
		return (5f + num * 75f).ToString("F0") + " Db";
	}

	public static void AddOrReplace<T>(this List<T> l, int index, T value)
	{
		if (index < l.Count)
		{
			l[index] = value;
		}
		else
		{
			l.Add(value);
		}
	}

	public static float GetMarketingEffort(float devTime)
	{
		return 14f * Mathf.Log(devTime / 6f + 1.2f) - 3.3f;
	}

	public static string Format(this string format, params object[] args)
	{
		return string.Format(format, args);
	}

	public static string ToPercent(this float value)
	{
		return (value * 100f).ToString("F1") + "%";
	}

	public static string GetRoot()
	{
		return Path.GetFullPath("./");
	}

	public static string CleanFileName(string filename)
	{
		char[] invalidFileNameChars = Path.GetInvalidFileNameChars();
		StringBuilder stringBuilder = new StringBuilder(filename);
		for (int i = stringBuilder.Length - 1; i >= 0; i--)
		{
			for (int j = 0; j < invalidFileNameChars.Length; j++)
			{
				if (stringBuilder[i] == invalidFileNameChars[j])
				{
					stringBuilder.Remove(i, 1);
					break;
				}
			}
		}
		return stringBuilder.ToString();
	}

	public static bool None<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
	{
		if (source == null)
		{
			throw new ArgumentNullException("source");
		}
		if (predicate == null)
		{
			throw new ArgumentNullException("predicate");
		}
		foreach (TSource arg in source)
		{
			if (predicate(arg))
			{
				return false;
			}
		}
		return true;
	}

	public static string YesNo(this bool b)
	{
		return (!b) ? "No".Loc() : "Yes".Loc();
	}

	public static PointerEventData PopulateDefault(this PointerEventData pd)
	{
		pd.pointerId = -1;
		pd.position = Input.mousePosition;
		return pd;
	}

	public static bool IsReferenceNull(this UnityEngine.Object obj)
	{
		return obj == null;
	}

	public static T[] AppendSelfArray<T>(this IList<T> list)
	{
		T[] array = new T[list.Count * 2];
		for (int i = 0; i < list.Count; i++)
		{
			array[i] = list[i];
			array[i + list.Count] = list[i];
		}
		return array;
	}

	public static T[] AppendSelfArray<T>(this IList<T> list, Func<T, T> transform)
	{
		T[] array = new T[list.Count * 2];
		for (int i = 0; i < list.Count; i++)
		{
			array[i] = list[i];
			array[i + list.Count] = transform(list[i]);
		}
		return array;
	}

	public static T2[] AppendSelfArray<T1, T2>(this IList<T1> list, Func<T1, T2> transform1, Func<T1, T2> transform2)
	{
		T2[] array = new T2[list.Count * 2];
		for (int i = 0; i < list.Count; i++)
		{
			array[i] = transform1(list[i]);
			array[i + list.Count] = transform2(list[i]);
		}
		return array;
	}

	public static Vector2[] ToPolygon(this Rect area)
	{
		return new Vector2[]
		{
			new Vector2(area.xMax, area.yMin),
			new Vector2(area.xMax, area.yMax),
			new Vector2(area.xMin, area.yMax),
			new Vector2(area.xMin, area.yMin)
		};
	}

	public static bool IsBetween(this float x, float a, float b)
	{
		return a < x && x < b;
	}

	public static Vector2 GetRandomTrianglePoint(this IList<Vector2> points)
	{
		float randomValue = Utilities.RandomValue;
		float randomValue2 = Utilities.RandomValue;
		float num = Mathf.Sqrt(randomValue);
		return (1f - num) * points[0] + num * (1f - randomValue2) * points[1] + num * randomValue2 * points[2];
	}

	public static T[] RepeatValue<T>(T value, int num)
	{
		T[] array = new T[num];
		for (int i = 0; i < num; i++)
		{
			array[i] = value;
		}
		return array;
	}

	public static T2[] SelectInPlace<T1, T2>(this IList<T1> arr, Func<T1, T2> select)
	{
		T2[] array = new T2[arr.Count];
		for (int i = 0; i < arr.Count; i++)
		{
			array[i] = select(arr[i]);
		}
		return array;
	}

	public static T2[] SelectInPlace<T1, T2>(this IList<T1> arr, Func<T1, int, T2> select)
	{
		T2[] array = new T2[arr.Count];
		for (int i = 0; i < arr.Count; i++)
		{
			array[i] = select(arr[i], i);
		}
		return array;
	}

	public static T[] ConcatArray<T>(this T[] arr, T[] arr2)
	{
		T[] array = new T[arr.Length + arr2.Length];
		for (int i = 0; i < arr.Length; i++)
		{
			array[i] = arr[i];
		}
		for (int j = 0; j < arr2.Length; j++)
		{
			array[j + arr.Length] = arr2[j];
		}
		return array;
	}

	public static T[] ReverseArray<T>(this T[] arr)
	{
		Array.Reverse(arr);
		return arr;
	}

	public static Vector3 Inverse(this Vector3 v)
	{
		return new Vector3(1f / v.x, 1f / v.y, 1f / v.z);
	}

	public static void JoinThreads(this IList<Thread> ts)
	{
		for (int i = 0; i < ts.Count; i++)
		{
			ts[i].Join();
		}
	}

	public static IEnumerable<Transform> GetChildren(this Transform t)
	{
		for (int i = 0; i < t.childCount; i++)
		{
			yield return t.GetChild(i);
		}
		yield break;
	}

	public static IEnumerable<Transform> GetChildren(this Scene s)
	{
		GameObject[] r = s.GetRootGameObjects();
		foreach (GameObject o in r)
		{
			yield return o.transform;
		}
		yield break;
	}

	public static int TimeToHour(this string input, int defaultValue)
	{
		string text = input.Trim().ToLower();
		if (text.Length > 0 && char.IsDigit(text[0]))
		{
			int num = 0;
			try
			{
				if (text.Length > 1 && char.IsDigit(text[1]))
				{
					num = Convert.ToInt32(text.Substring(0, 2));
				}
				else
				{
					num = Convert.ToInt32(text.Substring(0, 1));
				}
			}
			catch (Exception ex)
			{
				return defaultValue;
			}
			if (text.Contains("pm") && num <= 11)
			{
				num += 12;
			}
			if (num == 12 && text.Contains("am"))
			{
				num = 0;
			}
			if (num >= 0 && num <= 23)
			{
				return num;
			}
			return defaultValue;
		}
		return defaultValue;
	}

	public static string HourToTime(int hour, bool AMPM)
	{
		if (AMPM)
		{
			string arg = "AM";
			if (hour > 11)
			{
				arg = "PM";
				if (hour > 12)
				{
					hour -= 12;
				}
			}
			hour = ((hour != 0) ? hour : 12);
			return string.Format("{0:D2} {1}", hour, arg);
		}
		return hour.ToString("D2");
	}

	public static string HourToTime(int hour, int minutes, bool AMPM)
	{
		if (AMPM)
		{
			string arg = "AM";
			if (hour > 11)
			{
				arg = "PM";
				if (hour > 12)
				{
					hour -= 12;
				}
			}
			hour = ((hour != 0) ? hour : 12);
			return string.Format("{0:D2}:{1:D2} {2}", hour, minutes, arg);
		}
		return string.Format("{0:D2}:{1:D2}", hour, minutes);
	}

	public static T2 Mode<T1, T2>(this IList<T1> list, Func<T1, T2> getValue, T2 defaultValue = default(T2))
	{
		Dictionary<T2, int> dictionary = new Dictionary<T2, int>();
		for (int i = 0; i < list.Count; i++)
		{
			T1 arg = list[i];
			dictionary.AddTo(getValue(arg), 1, (int x, int y) => x + y);
		}
		return (dictionary.Count <= 0) ? defaultValue : dictionary.MaxInstance((KeyValuePair<T2, int> x) => (float)x.Value).Key;
	}

	public static bool Mode<T>(this IList<bool> list, Func<T, bool> getValue, bool defaultValue = false)
	{
		return list.Count((bool x) => x) > list.Count / 2;
	}

	public static IEnumerable<T> PickFrom<T>(this IList<T> input, IList<bool> selected)
	{
		int max = Mathf.Min(input.Count, selected.Count);
		for (int i = 0; i < max; i++)
		{
			if (selected[i])
			{
				yield return input[i];
			}
		}
		yield break;
	}

	public static string Temperature(this float t, bool diff)
	{
		if (diff)
		{
			return (!Options.Celsius) ? ((t * 1.8f).WithPlusN("N0") + "F") : (t.WithPlusN("N0") + "C");
		}
		return (!Options.Celsius) ? ((t * 1.8f + 32f).ToString("N0") + "F") : (t.ToString("N0") + "C");
	}

	public static float ClippingScaleFactor = 1000f;
}
