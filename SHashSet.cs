﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AltSerialize;

[Serializable]
public class SHashSet<T> : HashSet<T>, IAltSerializable
{
	public SHashSet()
	{
	}

	protected SHashSet(SerializationInfo info, StreamingContext context)
	{
		T[] array = (T[])info.GetValue("Data", typeof(T[]));
		for (int i = 0; i < array.Length; i++)
		{
			base.Add(array[i]);
		}
	}

	public override void GetObjectData(SerializationInfo info, StreamingContext context)
	{
		T[] array = new T[base.Count];
		int num = 0;
		foreach (T t in this)
		{
			array[num] = t;
			num++;
		}
		info.AddValue("Data", array, array.GetType());
	}

	public void Serialize(AltSerializer serializer)
	{
		serializer.Write(base.Count);
		foreach (T t in this)
		{
			serializer.Serialize(t);
		}
	}

	public void Deserialize(AltSerializer deserializer)
	{
		int num = deserializer.ReadInt32();
		for (int i = 0; i < num; i++)
		{
			base.Add((T)((object)deserializer.Deserialize()));
		}
	}
}
