﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TimeProbe : MonoBehaviour
{
	public static void BeginTime(string timer)
	{
	}

	public static void FinalizeTime(string timer)
	{
	}

	public static void EndTime(string timer)
	{
	}

	public static TimeProbe Instance;

	public Dictionary<string, float> Timers = new Dictionary<string, float>();

	private Dictionary<string, float> ActiveTimers = new Dictionary<string, float>();
}
