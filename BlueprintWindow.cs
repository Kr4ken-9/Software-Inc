﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Steamworks;
using UnityEngine;
using UnityEngine.UI;

public class BlueprintWindow : MonoBehaviour
{
	private void Awake()
	{
		this.MainTexture = new Texture2D(256, 256, TextureFormat.ARGB32, false);
		this.Thumbnail.texture = this.MainTexture;
	}

	private void Start()
	{
		this.List.Items.AddRange(GameData.Prefabs.Cast<object>());
		this.List.OnSelectChange = delegate(bool d)
		{
			BuildingPrefab buildingPrefab = this.List.GetSelected<BuildingPrefab>().FirstOrDefault<BuildingPrefab>();
			if (buildingPrefab != null)
			{
				string path = Path.Combine(buildingPrefab.Root, "Thumbnail.png");
				if (File.Exists(path))
				{
					this.MainTexture.LoadImage(File.ReadAllBytes(path));
					this.MainTexture.Apply();
					this.Thumbnail.texture = this.MainTexture;
				}
				else
				{
					this.Thumbnail.texture = null;
				}
				this.SteamSticker.SetActive(buildingPrefab.SteamID != null);
			}
			else
			{
				this.SteamSticker.SetActive(false);
			}
			this.UpdateInfo();
		};
		this.List.Initialize();
	}

	public void UpdateInfo()
	{
		BuildingPrefab blueprint = this.List.GetSelected<BuildingPrefab>().FirstOrDefault<BuildingPrefab>();
		if (blueprint != null)
		{
			List<string> source = (from x in blueprint.Rooms.SelectMany((BuildingPrefab.RoomObject x) => x.Furniture)
			select x.Name).ToList<string>();
			HashSet<string> hashSet = (from x in source.Distinct<string>()
			where ObjectDatabase.Instance.GetFurniture(x) == null
			select x).ToHashSet<string>();
			List<Furniture> list = (from x in source
			select ObjectDatabase.Instance.GetFurnitureComponent(x) into x
			where x != null
			select x).ToList<Furniture>();
			if (!GameSettings.Instance.EditMode && !Cheats.UnlockFurn)
			{
				hashSet.AddRange(from x in list.Distinct<Furniture>()
				where x.UnlockYear > SDateTime.Now().RealYear
				select x.name);
			}
			string text = (hashSet.Count <= 0) ? "None".Loc() : string.Join("\n", hashSet.ToArray<string>());
			int minF = blueprint.Rooms.Min((BuildingPrefab.RoomObject x) => x.Floor);
			int maxF = blueprint.Rooms.Max((BuildingPrefab.RoomObject x) => x.Floor);
			float num = blueprint.Rooms.Sum((BuildingPrefab.RoomObject z) => z.Area);
			float num2 = blueprint.Rooms.Sum((BuildingPrefab.RoomObject x) => BuildController.GetRoomCost((from y in x.Edges
			select blueprint.Edges[y].ToVector2()).ToList<Vector2>(), x.Area, x.Outdoor, x.Floor - minF - 1, false, false));
			float num3 = blueprint.Rooms.Sum((BuildingPrefab.RoomObject x) => BuildController.GetRoomCost((from y in x.Edges
			select blueprint.Edges[y].ToVector2()).ToList<Vector2>(), x.Area, x.Outdoor, GameSettings.MaxFloor + 1 - (maxF - minF) + x.Floor, false, false));
			float num4 = blueprint.Rooms.SelectMany((BuildingPrefab.RoomObject x) => x.Segments).SumSafe(delegate(BuildingPrefab.SegmentObject x)
			{
				RoomSegment segmentComponent = ObjectDatabase.Instance.GetSegmentComponent(x.Name);
				return (!(segmentComponent == null)) ? (segmentComponent.Cost / segmentComponent.WallWidth * x.Width) : 0f;
			});
			num3 += num4;
			num2 += num4;
			float x2 = list.SumSafe((Furniture x) => x.Cost);
			int num5 = list.Count((Furniture x) => x.Type.Equals("Computer"));
			this.InfoText.text = string.Format("{0}: {1}\n{2}: {3}\n{4}: {5}\n{6}: {7}\n{8}:\n{9}", new object[]
			{
				"Square meters".Loc(),
				num,
				"Building cost".Loc(),
				num2.Currency(true) + " - " + num3.Currency(true),
				"Furniture cost".Loc(),
				x2.Currency(true),
				"Computers".Loc(),
				num5,
				"Incompatible furniture".Loc(),
				text
			});
		}
		else
		{
			this.InfoText.text = string.Empty;
		}
	}

	public void Refresh()
	{
		this.List.Items.Clear();
		this.List.Items.AddRange(GameData.Prefabs.Cast<object>());
		this.List.Select(0);
	}

	public void Build()
	{
		BuildingPrefab buildingPrefab = this.List.GetSelected<BuildingPrefab>().FirstOrDefault<BuildingPrefab>();
		if (buildingPrefab != null)
		{
			RoomCloneTool.Instance.Show(buildingPrefab);
			this.Window.Close();
		}
	}

	public void SaveSelected(bool updateSelected)
	{
		if (updateSelected && this.List.Selected.Count == 0)
		{
			return;
		}
		Room[] array = SelectorController.Instance.Selected.OfType<Room>().ToArray<Room>();
		if (array.Length > 0)
		{
			if (!BuildingPrefab.ValidCheck(array))
			{
				WindowManager.Instance.ShowMessageBox("UnsupportedStructure".Loc(), false, DialogWindow.DialogType.Error);
				return;
			}
			BuildingPrefab prefab = BuildingPrefab.SaveRooms(array, false, true, false, false);
			GameObject obj = MinimapThumbnailMaker.Instance.MinimapMaker.CreateMap(MinimapThumbnailMaker.Instance.MinimapMaker.MapDescFromRooms(array), false);
			Texture2D texture2D = MinimapThumbnailMaker.Instance.RenderObject(obj, MinimapThumbnailMaker.ThumbSize.Big, null);
			byte[] thumbData = texture2D.EncodeToPNG();
			UnityEngine.Object.Destroy(texture2D);
			UnityEngine.Object.Destroy(obj);
			if (updateSelected)
			{
				BuildingPrefab buildingPrefab = this.List.GetSelected<BuildingPrefab>().First<BuildingPrefab>();
				if (!buildingPrefab.CanUpload)
				{
					WindowManager.Instance.ShowMessageBox("OverwriteError".Loc(), false, DialogWindow.DialogType.Error);
					return;
				}
				if (buildingPrefab != null)
				{
					string itemTitle = buildingPrefab.ItemTitle;
					string text = buildingPrefab.FolderPath();
					PublishedFileId_t? steamID = buildingPrefab.SteamID;
					this.List.Items.Remove(buildingPrefab);
					GameData.Prefabs.Remove(buildingPrefab);
					string path = Path.Combine(text, "Building.xml");
					prefab.Root = text;
					prefab.ItemTitle = itemTitle;
					File.WriteAllText(path, XMLParser.ExportXML(prefab.ToXmlNode()));
					path = Path.Combine(text, "Thumbnail.png");
					File.WriteAllBytes(path, thumbData);
					this.List.Items.Add(prefab);
					GameData.Prefabs.Add(prefab);
					prefab.SteamID = steamID;
					prefab.CanUpload = true;
				}
			}
			else
			{
				WindowManager.SpawnInputDialog("BlueprintPrompt".Loc(), "BlueprintPromptTitle".Loc(), "BlueprintPromptDefault".Loc(), delegate(string name)
				{
					string text2 = Utilities.CleanFileName(name);
					string rNameLower = text2.ToLower();
					if (GameData.Prefabs.Any((BuildingPrefab x) => x.ItemTitle.ToLower().Equals(rNameLower)))
					{
						WindowManager.SpawnDialog("BlueprintPromptError".Loc(), true, DialogWindow.DialogType.Error);
					}
					else
					{
						string text3 = Path.Combine(Path.Combine(Utilities.GetRoot(), "Blueprints"), text2);
						try
						{
							Directory.CreateDirectory(text3);
							string path2 = Path.Combine(text3, "Building.xml");
							prefab.Root = text3;
							prefab.ItemTitle = text2;
							File.WriteAllText(path2, XMLParser.ExportXML(prefab.ToXmlNode()));
							path2 = Path.Combine(text3, "Thumbnail.png");
							File.WriteAllBytes(path2, thumbData);
							this.List.Items.Add(prefab);
							GameData.Prefabs.Add(prefab);
						}
						catch (Exception ex)
						{
							WindowManager.SpawnDialog(string.Format("BlueprintSaveError".Loc(), ex.Message), true, DialogWindow.DialogType.Error);
						}
					}
				}, null);
			}
		}
	}

	private void Update()
	{
		if (HUD.Instance == null || !HUD.Instance.BuildMode)
		{
			this.Window.Close();
		}
	}

	public void Show()
	{
		this.Window.Show();
		if (this.List.Items.Count > 0)
		{
			this.List.Select(0);
		}
	}

	public GUIWindow Window;

	public GUIListView List;

	public RawImage Thumbnail;

	public GameObject SteamSticker;

	private Texture2D MainTexture;

	public Text InfoText;
}
