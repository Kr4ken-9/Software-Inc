﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

public class PortraitMaker : MonoBehaviour
{
	private void Awake()
	{
		this.Cam.targetTexture = new RenderTexture(256, 256, 16)
		{
			antiAliasing = 1,
			autoGenerateMips = false,
			filterMode = FilterMode.Point
		};
	}

	public void RenderObject(GameObject obj)
	{
		this.LastActive = RenderTexture.active;
		RenderTexture.active = this.Cam.targetTexture;
		Dictionary<Renderer, KeyValuePair<int, bool>> dictionary = (from x in obj.GetComponentsInChildren<Renderer>()
		where x.shadowCastingMode != ShadowCastingMode.ShadowsOnly
		select x).ToDictionary((Renderer x) => x, (Renderer x) => new KeyValuePair<int, bool>(x.gameObject.layer, x.enabled));
		foreach (KeyValuePair<Renderer, KeyValuePair<int, bool>> keyValuePair in dictionary)
		{
			keyValuePair.Key.gameObject.layer = 8;
			keyValuePair.Key.enabled = true;
		}
		this.Cam.Render();
		foreach (KeyValuePair<Renderer, KeyValuePair<int, bool>> keyValuePair2 in dictionary)
		{
			keyValuePair2.Key.gameObject.layer = keyValuePair2.Value.Key;
			keyValuePair2.Key.enabled = keyValuePair2.Value.Value;
		}
		this.BlitMat.SetFloat("_inputSize", 256f);
		Graphics.Blit(this.Cam.targetTexture, this.FinalTex, this.BlitMat);
		RenderTexture.active = this.LastActive;
	}

	public Camera Cam;

	public RenderTexture FinalTex;

	private RenderTexture LastActive;

	public Material BlitMat;
}
