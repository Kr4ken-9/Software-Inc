﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TeamToggle : MonoBehaviour
{
	private void Start()
	{
		if (this.Company != null)
		{
			this.MainLabel.text = this.Company.Name;
			this.DetailLabel.text = "Subsidiary".Loc() + "\n" + this.Company.Money.Currency(true);
		}
		else
		{
			Team team = GameSettings.Instance.sActorManager.Teams[this.Team];
			this.MainLabel.text = team.Name;
			Actor[] employees = team.GetEmployees();
			float num = 0f;
			float[] array = new float[Employee.RoleCount - 1];
			string[] array2 = new string[]
			{
				"Programmers".Loc(),
				"Designers".Loc(),
				"Artists".Loc(),
				"Marketing".Loc(),
				"Employees".Loc()
			};
			for (int i = 1; i < Employee.RoleCount; i++)
			{
				Employee.EmployeeRole r = (Employee.EmployeeRole)i;
				float num2 = employees.SumSafe((Actor x) => x.employee.GetSkill(r));
				array[i - 1] = num2;
				if (num2 > num)
				{
					num = num2;
				}
			}
			if (num > 0f)
			{
				for (int j = 1; j < Employee.RoleCount; j++)
				{
					if (array[j - 1] / num > 0.8f)
					{
						array2[j - 1] = "<color=#55AA55FF>" + array2[j - 1] + "</color>";
					}
				}
			}
			Text detailLabel = this.DetailLabel;
			string format = "{9} {8}\n{1} {0}    {3} {2}\n{5} {4}    {7} {6}";
			object[] array3 = new object[10];
			array3[0] = array2[0];
			array3[1] = employees.Count((Actor x) => x.employee.IsRole(Employee.RoleBit.Programmer));
			array3[2] = array2[1];
			array3[3] = employees.Count((Actor x) => x.employee.IsRole(Employee.RoleBit.Designer));
			array3[4] = array2[2];
			array3[5] = employees.Count((Actor x) => x.employee.IsRole(Employee.RoleBit.Artist));
			array3[6] = array2[3];
			array3[7] = employees.Count((Actor x) => x.employee.IsRole(Employee.RoleBit.Marketer));
			array3[8] = array2[4];
			array3[9] = employees.Length;
			detailLabel.text = string.Format(format, array3);
		}
	}

	[NonSerialized]
	public SimulatedCompany Company;

	public string Team;

	public Text MainLabel;

	public Text DetailLabel;

	public Toggle MainToggle;
}
