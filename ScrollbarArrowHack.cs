﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ScrollbarArrowHack : MonoBehaviour
{
	private void Start()
	{
		base.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = ((base.GetComponent<Scrollbar>().direction >= Scrollbar.Direction.BottomToTop) ? this.Vertical : this.Horizontal);
		UnityEngine.Object.Destroy(this);
	}

	public Sprite Horizontal;

	public Sprite Vertical;
}
