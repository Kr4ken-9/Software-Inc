﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Employee
{
	public Employee()
	{
	}

	public Employee(SDateTime currentTime, Employee.EmployeeRole role, bool female, Employee.WageBracket bracket, PersonalityGraph graph, bool founder = false, string mainSpec = null, int? preferredAge = null, Team team = null, float leaderSkill = 1f, float maxChance = 0.1f)
	{
		this.Female = female;
		this.Hired = currentTime;
		this.LastWage = currentTime;
		this.Name = GameData.GenerateName(!female);
		this.Founder = founder;
		float num;
		if (founder)
		{
			this.Salary = 0f;
			this._currentRole = Employee.RoleBit.AnyRole;
			this.Founder = true;
			this.FounderSkill();
			this.AgeMonth = Employee.Youngest * 12;
			num = 1f;
		}
		else
		{
			int num2 = Employee.AgeFromBracket(bracket);
			if (preferredAge != null)
			{
				num2 = (preferredAge.Value + num2) / 2;
			}
			this.AgeMonth = num2 * 12 + Utilities.RandomRange(0, 11);
			num = Employee.SkillFromBracket(bracket, (int)role, maxChance);
			this.AskedFor = (this.Salary = Employee.WageFromSkillBracket((int)role, num));
			this.ChooseSkillFrom((int)role, this.Age, num);
		}
		if (team == null)
		{
			this.ChoosePersonality();
		}
		else
		{
			this.ChoosePersonality(team, leaderSkill);
		}
		this.SetPersonlityEffects(graph);
		this.InitalizeSpecializations(new string[]
		{
			mainSpec
		}, (int)role, num);
		this.Demanded = this.Salary - this.Worth((int)role, true);
		this.HiredFor = role;
	}

	public Employee(SDateTime currentTime, bool female, string name, float[] skills, string[] person, string[] mainSpec, PersonalityGraph graph)
	{
		this.Female = female;
		this.Hired = currentTime;
		this.LastWage = currentTime;
		this.Name = name;
		this.Salary = 0f;
		this._currentRole = Employee.RoleBit.AnyRole;
		this.Founder = true;
		this.Skill = skills;
		this.PersonalityTraits = person;
		this.AgeMonth = Employee.Youngest * 12;
		this.SetPersonlityEffects(graph);
		int mainRole = 0;
		float num = 0f;
		for (int i = 0; i < this.Skill.Length; i++)
		{
			if (this.Skill[i] > num)
			{
				num = this.Skill[i];
				mainRole = i;
			}
		}
		this.InitalizeSpecializations(mainSpec, mainRole, 1f);
	}

	public static float RoleBitOrder(Employee.RoleBit role)
	{
		int num = 0;
		for (int i = 0; i < Employee.RoleCount; i++)
		{
			if ((role & (Employee.RoleBit)(1 << i)) > Employee.RoleBit.None)
			{
				num += Employee.RoleOrder[i];
			}
		}
		return (float)num;
	}

	public string FullName
	{
		get
		{
			return this.NickName ?? this.Name;
		}
	}

	public string ExtraName
	{
		get
		{
			return (this.NickName == null) ? this.Name : (this.Name + " (" + this.NickName + ")");
		}
	}

	public Employee.RoleBit CurrentRoleBit
	{
		get
		{
			return this._currentRole;
		}
	}

	public static string GetRoleString(Employee.RoleBit role)
	{
		if (role == Employee.RoleBit.None)
		{
			return "None".Loc();
		}
		StringBuilder stringBuilder = new StringBuilder();
		bool flag = (role & Employee.RoleBit.Lead) > Employee.RoleBit.None;
		if (flag)
		{
			stringBuilder.Append("Leader".Loc());
		}
		int num = 0;
		for (int i = 1; i < Employee.RoleCount; i++)
		{
			if ((role & Employee.RoleToMask[i]) > Employee.RoleBit.None)
			{
				num++;
			}
		}
		bool flag2 = flag && num > 0;
		if (flag2)
		{
			stringBuilder.Append("(");
		}
		if (num == Employee.RoleCount - 1)
		{
			stringBuilder.Append("Any role".Loc());
		}
		else
		{
			bool flag3 = num > 2;
			int num2 = 0;
			for (int j = 1; j < Employee.RoleCount; j++)
			{
				if ((role & Employee.RoleToMask[j]) > Employee.RoleBit.None)
				{
					if (num2 != 0)
					{
						if (!flag3 || num + 1 == num2)
						{
							stringBuilder.Append("AndSeperator".Loc());
						}
						else
						{
							stringBuilder.Append(", ");
						}
					}
					num2++;
					StringBuilder stringBuilder2 = stringBuilder;
					string value;
					if (flag3)
					{
						value = Employee.ShortFormRole[j].Loc();
					}
					else
					{
						Employee.EmployeeRole employeeRole = (Employee.EmployeeRole)j;
						value = employeeRole.ToString().Loc();
					}
					stringBuilder2.Append(value);
					num--;
					if (num == 0)
					{
						break;
					}
				}
			}
		}
		if (flag2)
		{
			stringBuilder.Append(")");
		}
		return stringBuilder.ToString();
	}

	public string RoleString
	{
		get
		{
			return Employee.GetRoleString(this._currentRole);
		}
	}

	public bool IsRole(Employee.RoleBit role)
	{
		return (this._currentRole & role) > Employee.RoleBit.None;
	}

	public bool IsRole(int role)
	{
		return (this._currentRole & (Employee.RoleBit)role) > Employee.RoleBit.None;
	}

	public bool IsRole(byte role)
	{
		return ((byte)this._currentRole & role) > 0;
	}

	public bool IsRole(Employee.EmployeeRole role)
	{
		return this.IsRole(Employee.RoleToMask[(int)role]);
	}

	public bool IsRoleIndex(int idx)
	{
		return this.IsRole(Employee.RoleToMask[idx]);
	}

	public void SetRoles(Employee.RoleBit roles)
	{
		this._currentRole = roles;
	}

	private void UpdateMood(float delta, Actor act)
	{
		List<Employee.ThoughtEffect> list = this.Thoughts.List;
		for (int i = 0; i < list.Count; i++)
		{
			Employee.ThoughtEffect thoughtEffect = list[i];
			MoodEffect mood = thoughtEffect.Mood;
			if (thoughtEffect.Effect > 0f)
			{
				if (mood.Negative)
				{
					if (this.JobSatisfaction > 1f - mood.CutOff)
					{
						this.JobSatisfaction = Mathf.Max(1f - mood.CutOff, this.JobSatisfaction - Utilities.PerHour(thoughtEffect.Effect, delta, false));
					}
					if (this.JobSatisfaction <= 0f)
					{
						this.SatisfactionHitZero = true;
					}
				}
				else if (this.JobSatisfaction < 1f + mood.CutOff)
				{
					this.JobSatisfaction = Mathf.Min(1f + mood.CutOff, this.JobSatisfaction + Utilities.PerHour(thoughtEffect.Effect, delta, false));
				}
			}
			if (thoughtEffect.Frames > 0)
			{
				thoughtEffect.Frames--;
			}
			else
			{
				float num = Utilities.PerHour(mood.Decrement, delta, false);
				if (num > thoughtEffect.Effect)
				{
					this.Thoughts.Remove(thoughtEffect.Mood.Thought);
					i--;
				}
				else
				{
					thoughtEffect.Effect -= num;
				}
			}
		}
		this.JobSatisfaction = Mathf.Clamp(this.JobSatisfaction, 0f, 2f);
		if (this.JobSatisfaction < 0.1f)
		{
			HUD.Instance.AddPopupMessage("UnsatisfiedEmployeesWarning".Loc(), "Exclamation", PopupManager.PopUpAction.GotoEmp, act.DID, PopupManager.NotificationSound.Issue, 0.5f, PopupManager.PopupIDs.JobSatisfcation, 1);
		}
	}

	public void AddInstantMood(string effect, Actor act, float factor = 1f)
	{
		if (this.Founder)
		{
			return;
		}
		if (factor <= 0f)
		{
			return;
		}
		Employee.ThoughtEffect thoughtEffect = this.Thoughts.GetOrNull(effect);
		float num = 0f;
		Employee.ThoughtEffect thoughtEffect2;
		if (thoughtEffect != null)
		{
			thoughtEffect2 = ((thoughtEffect.Mood.CounterMood != null) ? this.Thoughts.GetOrNull(thoughtEffect.Mood.CounterMood) : null);
			float num2 = this.CalculateChange(thoughtEffect.Mood.Increment * factor, ref num, thoughtEffect2);
			if (thoughtEffect.Effect < thoughtEffect.Mood.Max)
			{
				thoughtEffect.Effect = Mathf.Min(thoughtEffect.Mood.Max, thoughtEffect.Effect + num2);
			}
			if (thoughtEffect.Mood.WarningThreshold > 0f && thoughtEffect.Effect > thoughtEffect.Mood.WarningThreshold)
			{
				HUD.Instance.AddPopupMessage(thoughtEffect.Mood.Warning.Loc(), "Exclamation", PopupManager.PopUpAction.GotoEmp, act.DID, PopupManager.NotificationSound.Issue, 0.75f, PopupManager.PopupIDs.EmployeeCountProblem, 1);
			}
		}
		else
		{
			MoodEffect moodEffect = GameData.MoodEffects[effect];
			thoughtEffect2 = ((moodEffect.CounterMood != null) ? this.Thoughts.GetOrNull(moodEffect.CounterMood) : null);
			float value = this.CalculateChange(Mathf.Min(moodEffect.Max, moodEffect.StartValue * factor), ref num, thoughtEffect2);
			thoughtEffect = new Employee.ThoughtEffect(moodEffect, value);
			this.Thoughts[effect] = thoughtEffect;
		}
		thoughtEffect.Frames = 5;
		if (num > 0f && thoughtEffect2 != null)
		{
			thoughtEffect2.Effect = Mathf.Max(0f, thoughtEffect2.Effect - num);
		}
	}

	public void AddMood(string effect, Actor act)
	{
		this.AddMood(effect, act, Time.deltaTime, 1f, true, true);
	}

	public void AddMood(string effect, Actor act, float delta, float factor = 1f, bool useGameSpeed = true, bool scaleStart = true)
	{
		if (this.Founder)
		{
			return;
		}
		Employee.ThoughtEffect thoughtEffect = this.Thoughts.GetOrNull(effect);
		float num = 0f;
		Employee.ThoughtEffect thoughtEffect2;
		if (thoughtEffect != null)
		{
			thoughtEffect2 = ((thoughtEffect.Mood.CounterMood != null) ? this.Thoughts.GetOrNull(thoughtEffect.Mood.CounterMood) : null);
			float num2 = thoughtEffect.Mood.StartValue;
			if (scaleStart)
			{
				num2 *= factor;
			}
			if (thoughtEffect.Effect < thoughtEffect.Mood.Max)
			{
				if (thoughtEffect.Effect < num2)
				{
					float num3 = this.CalculateChange(num2 - thoughtEffect.Effect, ref num, thoughtEffect2);
					thoughtEffect.Effect = Mathf.Min(thoughtEffect.Mood.Max, thoughtEffect.Effect + num3);
				}
				else
				{
					float num4 = this.CalculateChange(Utilities.PerHour(thoughtEffect.Mood.Increment * factor, delta, useGameSpeed), ref num, thoughtEffect2);
					thoughtEffect.Effect = Mathf.Min(thoughtEffect.Mood.Max, thoughtEffect.Effect + num4);
				}
			}
			if (thoughtEffect.Mood.WarningThreshold > 0f && thoughtEffect.Effect > thoughtEffect.Mood.WarningThreshold)
			{
				HUD.Instance.AddPopupMessage(thoughtEffect.Mood.Warning.Loc(), "Exclamation", PopupManager.PopUpAction.GotoEmp, act.DID, PopupManager.NotificationSound.Issue, 0.75f, PopupManager.PopupIDs.EmployeeCountProblem, 1);
			}
		}
		else
		{
			MoodEffect moodEffect = GameData.MoodEffects[effect];
			thoughtEffect2 = ((moodEffect.CounterMood != null) ? this.Thoughts.GetOrNull(moodEffect.CounterMood) : null);
			float value = this.CalculateChange(Mathf.Min(moodEffect.Max, (!scaleStart) ? moodEffect.StartValue : (moodEffect.StartValue * factor)), ref num, thoughtEffect2);
			thoughtEffect = new Employee.ThoughtEffect(moodEffect, value);
			this.Thoughts[effect] = thoughtEffect;
		}
		thoughtEffect.Frames = 5;
		if (num > 0f && thoughtEffect2 != null)
		{
			thoughtEffect2.Effect = Mathf.Max(0f, thoughtEffect2.Effect - num);
		}
	}

	private float CalculateChange(float effect, ref float backValue, Employee.ThoughtEffect thought)
	{
		float num = effect;
		if (thought != null)
		{
			num = Mathf.Max(0f, effect - thought.Effect);
			backValue = Mathf.Max(0f, effect - num);
		}
		return num;
	}

	public void SetMood(string effect, Actor act, float value)
	{
		if (this.Founder)
		{
			return;
		}
		if (value < 0f)
		{
			return;
		}
		if (value == 0f)
		{
			this.Thoughts.Remove(effect);
			return;
		}
		Employee.ThoughtEffect thoughtEffect = this.Thoughts.GetOrNull(effect);
		MoodEffect moodEffect = GameData.MoodEffects[effect];
		if (thoughtEffect != null)
		{
			thoughtEffect.Effect = Mathf.Min(moodEffect.Max, value);
			if (moodEffect.WarningThreshold > 0f && thoughtEffect.Effect > moodEffect.WarningThreshold)
			{
				HUD.Instance.AddPopupMessage(moodEffect.Warning.Loc(), "Exclamation", PopupManager.PopUpAction.GotoEmp, act.DID, PopupManager.NotificationSound.Issue, 0.75f, PopupManager.PopupIDs.EmployeeCountProblem, 1);
			}
		}
		else
		{
			thoughtEffect = new Employee.ThoughtEffect(moodEffect, Mathf.Min(moodEffect.Max, value));
			this.Thoughts[effect] = thoughtEffect;
		}
		thoughtEffect.Frames = 5;
	}

	public string GetHighestThought()
	{
		Employee.ThoughtEffect thoughtEffect = null;
		float num = 0f;
		for (int i = 0; i < this.Thoughts.Count; i++)
		{
			Employee.ThoughtEffect thoughtEffect2 = this.Thoughts[i];
			if (thoughtEffect2.Effect > num)
			{
				num = thoughtEffect2.Effect;
				thoughtEffect = thoughtEffect2;
			}
		}
		return (thoughtEffect != null) ? thoughtEffect.Mood.Thought : null;
	}

	public float Age
	{
		get
		{
			return (float)this.AgeMonth / 12f;
		}
	}

	public void AddToSpecialization(Employee.EmployeeRole role, string specialization, float amount, float delta, bool force = false)
	{
		if (string.IsNullOrEmpty(specialization) || amount < 0f || float.IsNaN(amount) || float.IsInfinity(amount))
		{
			return;
		}
		Dictionary<string, float> dictionary = this.Specialization[(int)role];
		if (dictionary.ContainsKey(specialization))
		{
			if (force)
			{
				dictionary[specialization] = Mathf.Clamp01(dictionary[specialization] + amount);
			}
			else if (dictionary[specialization] < 0.5f)
			{
				dictionary[specialization] = Mathf.Max(0f, dictionary[specialization] + Utilities.PerDay(amount * this.GetEducationFactor(role) / 60f, delta, true));
			}
		}
		else if (force)
		{
			dictionary[specialization] = Mathf.Clamp01(amount);
		}
	}

	public float GetEducationFactor(Employee.EmployeeRole role)
	{
		return this.Age.MapRange((float)Employee.Youngest, (float)Employee.RetirementAge, 1f, 0.9f, true) * this.Skill[(int)role].MapRange(0f, 0.5f, 0.75f, 1f, true) * this.Autodidactic.MapRange(-1f, 1f, 0.75f, 1.5f, false);
	}

	public void AddToSpecializations(Employee.EmployeeRole role, Dictionary<string, float> specializations, float delta)
	{
		foreach (KeyValuePair<string, float> keyValuePair in specializations)
		{
			this.AddToSpecialization(role, keyValuePair.Key, keyValuePair.Value, delta, false);
		}
	}

	public void AddToSpecializations(Employee.EmployeeRole role, float[,] devTimes, string[] specs, float delta, float minimumSpec = 0f)
	{
		for (int i = 0; i < specs.Length; i++)
		{
			if (role == Employee.EmployeeRole.Designer || minimumSpec == 0f || this.GetSpecialization(role, specs[i], false) >= minimumSpec)
			{
				float amount = (role != Employee.EmployeeRole.Designer) ? devTimes[i, (role != Employee.EmployeeRole.Programmer) ? 1 : 0] : (devTimes[i, 0] + devTimes[i, 1]);
				this.AddToSpecialization(role, specs[i], amount, delta, false);
			}
		}
	}

	public void AddToSpecializations(Employee.EmployeeRole role, Dictionary<string, float> specializations, Dictionary<string, float> factor, bool reverse, float delta)
	{
		foreach (KeyValuePair<string, float> keyValuePair in specializations)
		{
			float num = factor[keyValuePair.Key];
			if (reverse)
			{
				num = 1f - num;
			}
			this.AddToSpecialization(role, keyValuePair.Key, keyValuePair.Value * num, delta, false);
		}
	}

	public static bool IsSpecialized(Employee.EmployeeRole role)
	{
		return role == Employee.EmployeeRole.Artist || role == Employee.EmployeeRole.Designer || role == Employee.EmployeeRole.Programmer;
	}

	public float GetSpecialization(Employee.EmployeeRole role, string category, bool scaleWithSkill)
	{
		if (string.IsNullOrEmpty(category))
		{
			return this.GetSkill(role);
		}
		float orDefault = this.Specialization[(int)role].GetOrDefault(category, 0f);
		if (scaleWithSkill)
		{
			float skill = this.GetSkill(role);
			if (skill > 0.75f && skill > orDefault && orDefault < 0.5f)
			{
				return Mathf.Lerp(orDefault, 0.5f, skill.MapRange(0.75f, 1f, 0f, 1f, false));
			}
		}
		return orDefault;
	}

	private void SetPersonlityEffects(PersonalityGraph graph)
	{
		float[][] array = new float[this.PersonalityTraits.Length][];
		for (int i = 0; i < this.PersonalityTraits.Length; i++)
		{
			array[i] = graph.GetEffects(this.PersonalityTraits[i]);
		}
		this.Autodidactic = (from x in array
		select x[0]).Average();
		this.Leadership = (from x in array
		select x[1]).Average();
		this.Diligence = (from x in array
		select x[2]).Average();
	}

	public float GetSkillAverage()
	{
		return this.Skill.Average();
	}

	public float GetSkillI(int i)
	{
		return this.Skill[i];
	}

	public float GetSkill(Employee.EmployeeRole role)
	{
		return this.Skill[(int)role];
	}

	public float GetHireSkill(Employee.EmployeeRole role)
	{
		return this.Skill[(int)role].MapRange(Employee.LowSkillCap[(int)role], 1f, 0f, 1f, true);
	}

	public float Compatibility(Employee other)
	{
		if (other == this || GameSettings.Instance == null)
		{
			return 1f;
		}
		float num = 0f;
		for (int i = 0; i < this.PersonalityTraits.Length; i++)
		{
			for (int j = 0; j < other.PersonalityTraits.Length; j++)
			{
				num += GameSettings.Instance.Personalities[this.PersonalityTraits[i], other.PersonalityTraits[j]];
			}
		}
		return num / (float)(this.PersonalityTraits.Length * other.PersonalityTraits.Length);
	}

	public void InitalizeSpecializations(string[] mainSpec, int mainRole, float skillFact)
	{
		float num = 1f;
		float num2 = this.Age.MapRange((float)Employee.Youngest, (float)Employee.RetirementAge, 0.9f, 0.1f, false);
		foreach (KeyValuePair<int, Dictionary<string, float>> keyValuePair in from x in this.Specialization
		orderby this.Skill[x.Key] descending
		select x)
		{
			string[] array = (!(GameSettings.Instance == null)) ? GameSettings.Instance.GetUnlockedSpecializations((Employee.EmployeeRole)keyValuePair.Key) : GameData.GetUnlockedSpecializations((Employee.EmployeeRole)keyValuePair.Key);
			float num3 = this.Skill[keyValuePair.Key];
			if (keyValuePair.Key == mainRole)
			{
				num3 = Mathf.Lerp(1f, skillFact, this.Age.MapRange((float)Employee.Youngest, 35f, 0f, 1f, true));
			}
			string tSpec = mainSpec[Mathf.Min(mainSpec.Length - 1, keyValuePair.Key - 1)];
			foreach (string key in array.OrderBy((string x) => (!x.Equals(tSpec)) ? UnityEngine.Random.value : -1f))
			{
				keyValuePair.Value[key] = Utilities.RandomGaussClamped(num3 * num, 0.05f);
				num3 *= (float)(1 - 1 / array.Length) * num2;
				if (num3 < 0.1f)
				{
					break;
				}
			}
		}
	}

	private void ChoosePersonality()
	{
		this.PersonalityTraits = new string[Employee.PersonalityNum];
		for (int i = 0; i < Employee.PersonalityNum; i++)
		{
			this.PersonalityTraits[i] = GameSettings.Instance.Personalities.SelectRandom(this.PersonalityTraits);
		}
	}

	public Employee.RoleBit GetBestRoles(bool lead, float cutoff)
	{
		float num = 0f;
		for (int i = (!lead) ? 1 : 0; i < this.Skill.Length; i++)
		{
			if (this.Skill[i] > num)
			{
				num = this.Skill[i];
			}
		}
		Employee.RoleBit roleBit = Employee.RoleBit.None;
		for (int j = (!lead) ? 1 : 0; j < this.Skill.Length; j++)
		{
			if (this.Skill[j] > num * cutoff)
			{
				roleBit |= Employee.RoleToMask[j];
			}
		}
		return roleBit;
	}

	private void ChoosePersonality(Team team, float leaderskill)
	{
		List<Actor> emp = team.GetEmployeesDirect();
		if (emp.Count == 0 || leaderskill < 0.25f)
		{
			this.ChoosePersonality();
			return;
		}
		this.PersonalityTraits = new string[Employee.PersonalityNum];
		int num = 0;
		foreach (string personality in from x in GameSettings.Instance.Personalities.PersonalityTraits
		orderby emp.Count((Actor y) => y.employee.PersonalityTraits.Contains(x)) descending
		select x)
		{
			foreach (KeyValuePair<string, float> keyValuePair in GameSettings.Instance.Personalities.GetCompatibilities(personality).OrderByDescending((KeyValuePair<string, float> x) => x.Value))
			{
				if (UnityEngine.Random.value < leaderskill * 0.9f)
				{
					bool flag = true;
					HashSet<string> incompatibilities = GameSettings.Instance.Personalities.GetIncompatibilities(keyValuePair.Key);
					for (int i = 0; i < num; i++)
					{
						if (incompatibilities.Contains(this.PersonalityTraits[i]))
						{
							flag = false;
							break;
						}
					}
					if (flag)
					{
						this.PersonalityTraits[num] = keyValuePair.Key;
						num++;
						if (num == Employee.PersonalityNum || UnityEngine.Random.value > 0.5f)
						{
							break;
						}
					}
				}
			}
			if (num == Employee.PersonalityNum)
			{
				break;
			}
		}
		if (num < Employee.PersonalityNum)
		{
			this.ChoosePersonality();
		}
	}

	public void FounderSkill()
	{
		for (int i = 0; i < 5; i++)
		{
			this.Skill[i] = 1f;
		}
	}

	private void ChooseSkillFrom(int mainRole, float age, float skill)
	{
		skill *= age.MapRange(20f, 35f, 0.25f, 1f, true);
		List<int> list = new List<int>
		{
			0,
			1,
			2,
			3,
			4
		};
		list.Remove(mainRole);
		list = (from x in list
		orderby UnityEngine.Random.value
		select x).ToList<int>();
		list.Insert(0, mainRole);
		float discreteRep = GameSettings.Instance.MyCompany.DiscreteRep;
		float num = skill;
		num = Mathf.Max(0.05f, num);
		float num2 = age.MapRange((float)Employee.Youngest, (float)Employee.RetirementAge, 4f, 2f, false);
		for (int i = 0; i < list.Count; i++)
		{
			this.Skill[list[i]] = Mathf.Clamp01(num * Utilities.RandomGaussClamped(1f, 0.05f + (1f - discreteRep) * 0.1f));
			if (i == 0)
			{
				this.Skill[list[i]] = this.Skill[list[i]].MapRange(0f, 1f, Employee.LowSkillCap[mainRole], 1f, false);
			}
			num /= Utilities.RandomRange(num2 - 1f, num2 + 1f);
		}
	}

	public float GetSpecAverage()
	{
		Employee.EmployeeRole employeeRole = this.NaturalRole(true, false, false);
		if (employeeRole == Employee.EmployeeRole.Designer)
		{
			return this.Specialization[2].Average((KeyValuePair<string, float> x) => x.Value);
		}
		if (employeeRole == Employee.EmployeeRole.Artist)
		{
			return this.Specialization[2].Average((KeyValuePair<string, float> x) => x.Value);
		}
		if (employeeRole != Employee.EmployeeRole.Programmer)
		{
			return this.Specialization.Average((KeyValuePair<int, Dictionary<string, float>> x) => x.Value.Average((KeyValuePair<string, float> z) => z.Value));
		}
		return this.Specialization[2].Average((KeyValuePair<string, float> x) => x.Value);
	}

	public Employee.EmployeeRole GetRoleOrNatural(bool lead = true, bool onlySoftware = false)
	{
		if ((!lead && this.HiredFor == Employee.EmployeeRole.Lead) || (onlySoftware && (this.HiredFor < Employee.EmployeeRole.Programmer || this.HiredFor > Employee.EmployeeRole.Artist)))
		{
			return this.NaturalRole(lead, onlySoftware, true);
		}
		return this.HiredFor;
	}

	public Employee.EmployeeRole NaturalRole(bool lead = true, bool onlySoftware = false, bool onlyActive = false)
	{
		int result = 1;
		float num = 0f;
		for (int i = (!lead) ? 1 : 0; i < Employee.RoleCount; i++)
		{
			if (!onlyActive || this.IsRole(Employee.RoleToBit[i]))
			{
				if (!onlySoftware || (i >= 1 && i <= 3))
				{
					if (this.Skill[i] > num)
					{
						result = i;
						num = this.Skill[i];
					}
				}
			}
		}
		return (Employee.EmployeeRole)result;
	}

	public void ChangeToNaturalRole(bool lead = true)
	{
		this._currentRole = Employee.RoleToMask[(int)this.NaturalRole(lead, false, false)];
	}

	public Employee.RoleBit BestRoles()
	{
		float num = 0f;
		for (int i = 1; i < Employee.RoleCount; i++)
		{
			num = Mathf.Max(num, this.GetSkillI(i));
		}
		int num2 = 0;
		for (int j = Employee.RoleCount - 1; j >= 1; j--)
		{
			if (this.GetSkillI(j) > num * 0.8f)
			{
				num2 |= 1;
			}
			num2 <<= 1;
		}
		return (Employee.RoleBit)num2;
	}

	public float Worth(int role = -1, bool withDemands = true)
	{
		bool flag = false;
		if (role == -1)
		{
			role = (int)this.NaturalRole(true, false, true);
		}
		if (role == -2)
		{
			flag = true;
			role = (int)this.NaturalRole(true, false, false);
		}
		float num = Employee.GetEmployeeWorth(role, this.Skill[role], this.Age, Utilities.GetMonths(this.Hired, SDateTime.Now()) / 12f);
		if (withDemands)
		{
			num += this.Demanded;
		}
		return (!flag) ? num : Mathf.Max(this.Salary, num);
	}

	public static float GetEmployeeWorth(int role, float skill, float age, float seniority)
	{
		age = 1f - Mathf.Pow(1f - age / (float)Employee.RetirementAge, 2f);
		seniority = Mathf.Min(1f, seniority / 10f);
		return (Employee.SkillBase + skill * Employee.SkillFactor) * seniority.WeightOne(Employee.SeniorityWeight) * Employee.AverageWage * Employee.RoleSalary[role] * age;
	}

	public static float SkillFromWage(int role, float wage, float age, float seniority)
	{
		age = 1f - Mathf.Pow(1f - age / (float)Employee.RetirementAge, 2f);
		seniority = Mathf.Min(1f, seniority / 10f);
		return (wage / Employee.AverageWage / Employee.RoleSalary[role] / age / seniority.WeightOne(Employee.SeniorityWeight) - Employee.SkillBase) / Employee.SkillFactor;
	}

	public static int AgeFromBracket(Employee.WageBracket bracket)
	{
		if (bracket == Employee.WageBracket.Low)
		{
			return Employee.SubAgeBracket(0.3f, bracket);
		}
		if (bracket == Employee.WageBracket.Medium)
		{
			return Employee.SubAgeBracket(0.5f, bracket);
		}
		if (bracket != Employee.WageBracket.High)
		{
			return 20;
		}
		return Employee.SubAgeBracket(0f, bracket);
	}

	private static int SubAgeBracket(float mean, Employee.WageBracket bracket)
	{
		return Utilities.GaussRange(mean, Employee.AgeBrackets[(int)bracket][0], Employee.AgeBrackets[(int)bracket][1], 0.2f);
	}

	public static float SkillFromBracket(Employee.WageBracket bracket, int role, float maxChance)
	{
		float num = 0.25f;
		float num2 = (float)bracket * num;
		float num3 = num2 + num;
		maxChance *= GameSettings.Instance.MyCompany.DiscreteRep;
		if (bracket == Employee.WageBracket.High && UnityEngine.Random.value <= maxChance)
		{
			num2 += num;
			num3 += num;
		}
		return Utilities.GaussRangeFloat(0.5f, Mathf.Max(0.01f, num2), num3, 0.2f);
	}

	public static float WageFromSkillBracket(int role, float skillFact)
	{
		int num = skillFact.Quantize(4);
		int num2 = Mathf.Min(2, num);
		float num3 = 0.25f;
		float employeeWorth = Employee.GetEmployeeWorth(role, (float)num * num3, (float)Employee.AgeBrackets[num2][0], 0f);
		float num4 = Employee.GetEmployeeWorth(role, (float)num * num3 + num3, (float)Employee.AgeBrackets[num2][1], 0f);
		if (num == 3)
		{
			num4 *= 1.25f;
		}
		float t = (skillFact - num3 * (float)num) / num3;
		return Mathf.Lerp(employeeWorth, num4, t);
	}

	public void ChangeSalary(float newSalary, float askedFor, Actor act, bool negotiation)
	{
		this.LastWage = SDateTime.Now();
		this.AskedFor = askedFor;
		if (newSalary < this.AskedFor)
		{
			if (newSalary < this.Salary)
			{
				this.AddInstantMood("SalaryCutComplaint", act, 0.5f * (this.AskedFor / newSalary));
			}
			else
			{
				this.AddInstantMood("LowerSalaryComplaint", act, 0.25f * (this.AskedFor / newSalary) / (this.Salary / this.Worth(-1, false)));
			}
		}
		else if (newSalary > this.Salary)
		{
			this.AddInstantMood("HigherSalary", act, 0.5f * (newSalary / this.AskedFor));
		}
		if (newSalary > this.AskedFor && negotiation)
		{
			this.Demanded += newSalary - this.AskedFor;
		}
		this.Salary = newSalary;
	}

	public void Spawn()
	{
		this.CoffeeQual = 0f;
		this.Hunger = ((!this.Founder) ? ((!this.HadProperFood) ? Utilities.RandomRange(0.2f, 1f) : 0.55f) : 1f);
		this.HadProperFood = false;
		this.Energy = 1f;
		this.Bladder = ((!this.Founder) ? Utilities.RandomRange(0.2f, 1f) : 1f);
	}

	public void ChangeSkill(Employee.EmployeeRole role, float perDay, bool perday)
	{
		perDay *= this.Autodidactic.MapRange(-1f, 1f, 0.75f, 2f, false);
		float num = GameSettings.Instance.Difficulty.MapRange(0f, 2f, 2f, 1f, false);
		perDay *= num;
		if (perday)
		{
			this.Skill[(int)role] = Mathf.Clamp01(this.Skill[(int)role] + Utilities.PerDay(perDay, true));
		}
		else
		{
			this.Skill[(int)role] = Mathf.Clamp01(this.Skill[(int)role] + perDay);
		}
	}

	public void ChangeSkillDirect(Employee.EmployeeRole role, float value)
	{
		this.Skill[(int)role] = Mathf.Clamp01(value);
	}

	public void ChangeSpecializationDirect(Employee.EmployeeRole role, string spec, float value)
	{
		this.Specialization[(int)role][spec] = Mathf.Clamp01(value);
	}

	private float ScaleToOne(float val, float start)
	{
		return 1f - val / start;
	}

	public void Update(float delta, bool forFree, bool goingHome, bool disableNeeds, Employee.Status simBladder, Employee.Status simHunger, float stressFactor, float socialFactor, bool canStressOut, Actor act)
	{
		if (!this.Founder)
		{
			float num = Utilities.RandomGaussClamped(0.5f, 0.2f) + 0.5f;
			if (!disableNeeds)
			{
				if (simHunger == Employee.Status.Enable)
				{
					this.Hunger = Mathf.Max(0f, this.Hunger - delta * Employee.HungerDrain);
				}
				if (simBladder == Employee.Status.Enable)
				{
					this.Bladder = Mathf.Max(0f, this.Bladder - delta * Employee.BladderDrain * num);
				}
			}
			if (this.Stress > 0f || canStressOut)
			{
				this.Stress = Mathf.Clamp01(this.Stress - delta * Employee.StressDrain * stressFactor * num);
			}
			this.Social = Mathf.Clamp01(this.Social - delta * Employee.SocialDrain * socialFactor * num);
			float num2 = this.CoffeeQual.MapRange(0f, 3f, 1f, 0.75f, false);
			this.Energy = Mathf.Max(0f, this.Energy - num2 * delta * Employee.EnergyDrain * num);
			if (this.Stress <= 0.5f)
			{
				this.AddMood("StressProblem", act, delta, this.ScaleToOne(this.Stress, 0.5f), false, true);
			}
			if (this.Social <= 0.5f)
			{
				this.AddMood("SocialProblem", act, delta, this.ScaleToOne(this.Social, 0.5f), false, true);
			}
			if (this.Hunger <= 0.1f && simHunger != Employee.Status.Disable)
			{
				this.AddMood("Starving", act, delta, (!disableNeeds && simHunger != Employee.Status.Freeze) ? this.ScaleToOne(this.Hunger, 0.1f) : 0f, false, false);
			}
			if (this.Bladder <= 0.1f && simBladder != Employee.Status.Disable)
			{
				this.AddMood("HasToPee", act, delta, (!disableNeeds && simBladder != Employee.Status.Freeze) ? this.ScaleToOne(this.Bladder, 0.1f) : 0f, false, false);
			}
			if (this.Energy <= 0.1f && !goingHome)
			{
				this.AddMood("WornOut", act, delta, this.ScaleToOne(this.Energy, 0.1f), false, false);
			}
			if (!forFree)
			{
				float num3 = this.Worth(-1, false);
				if (num3 > 0f && this.Salary > num3 * 1.1f)
				{
					this.SetMood("GoodSalary", act, this.Salary / num3 * 0.2f);
				}
			}
			float months = Utilities.GetMonths(this.Hired, SDateTime.Now());
			if (months > 12f)
			{
				float num4 = Employee.WeightTiredFactor(this.Leadership, 0.6f) * Employee.WeightTiredFactor(this.Diligence, 0.2f) * Employee.WeightTiredFactor(this.Autodidactic, 0.4f);
				num4 /= 8f;
				num4 = Mathf.Pow(1f + num4 / 900f, months - 12f) - 1f;
				this.SetMood("TiredOfJob", act, num4);
			}
			this.UpdateMood(delta, act);
		}
	}

	private static float WeightTiredFactor(float factor, float weight)
	{
		return weight * factor - weight + 2f;
	}

	public float GetAverageEducation(Employee.EmployeeRole r)
	{
		if (r >= Employee.EmployeeRole.Programmer && r <= Employee.EmployeeRole.Artist)
		{
			float num = 0f;
			int num2 = 0;
			string[] array = (!(GameSettings.Instance == null)) ? GameSettings.Instance.GetUnlockedSpecializations(r) : GameData.GetUnlockedSpecializations(r);
			foreach (string category in array)
			{
				num += this.GetSpecialization(r, category, false);
				num2++;
			}
			return num / (float)num2;
		}
		return this.GetSkill(r);
	}

	public override string ToString()
	{
		return this.Name;
	}

	public float GetBenefitValue(string benefit)
	{
		return EmployeeBenefit.GetBenefitValue(this.CustomBenefits, benefit);
	}

	public static int RetirementAge = 65;

	public static int Youngest = 20;

	public static int RoleCount = 5;

	public static byte[] RoleToBit = new byte[]
	{
		1,
		2,
		4,
		8,
		16
	};

	public static Employee.RoleBit[] RoleToMask = new Employee.RoleBit[]
	{
		Employee.RoleBit.Lead,
		Employee.RoleBit.Programmer,
		Employee.RoleBit.Designer,
		Employee.RoleBit.Artist,
		Employee.RoleBit.Marketer
	};

	private static int[] RoleOrder = new int[]
	{
		16,
		4,
		8,
		2,
		1
	};

	public static float[] RoleSalary = new float[]
	{
		2.25f,
		1.25f,
		1.5f,
		0.75f,
		1f
	};

	public static float[] LowSkillCap = new float[]
	{
		0.2f,
		0f,
		0f,
		0f,
		0.2f
	};

	public static float EducationDrain = 5.787037E-06f;

	public static float AverageWage = 6000f;

	public readonly string Name;

	public readonly bool Female = true;

	public string NickName;

	public Dictionary<string, float> CustomBenefits = new Dictionary<string, float>();

	public bool HR;

	private Dictionary<int, Dictionary<string, float>> Specialization = new Dictionary<int, Dictionary<string, float>>
	{
		{
			1,
			new Dictionary<string, float>()
		},
		{
			2,
			new Dictionary<string, float>()
		},
		{
			3,
			new Dictionary<string, float>()
		}
	};

	private Employee.RoleBit _currentRole = Employee.RoleBit.AnyRole;

	public static string[] ShortFormRole = new string[]
	{
		"Lead",
		"Code",
		"Design",
		"Art",
		"PR"
	};

	public Employee.EmployeeRole HiredFor;

	public static float HungerDrain = 0.0009259259f;

	public static float BladderDrain = 0.00104166672f;

	public static float EnergyDrain = 0.00138888892f;

	public static float StressDrain = 0.00138888892f;

	public static float SocialDrain = 0.000115740739f;

	public float Hunger = 1f;

	public float Energy = 1f;

	public float Bladder = 1f;

	public float Social = 1f;

	public float Stress = 1f;

	public float CoffeeQual;

	public bool SatisfactionHitZero;

	public DictionaryList<string, Employee.ThoughtEffect> Thoughts = new DictionaryList<string, Employee.ThoughtEffect>();

	public float Autodidactic;

	public float Leadership;

	public float Diligence;

	public bool HadProperFood;

	public float JobSatisfaction = 1f;

	private float[] Skill = new float[5];

	public string[] PersonalityTraits;

	public float Salary;

	public float AskedFor;

	public float Demanded;

	public SDateTime Hired;

	public SDateTime LastWage;

	public bool Founder;

	public bool Fired;

	public int AgeMonth;

	[NonSerialized]
	public Actor MyActor;

	private static int PersonalityNum = 2;

	public static float SkillBase = 0.65f;

	public static float SkillFactor = 0.6f;

	public static float SeniorityWeight = 0.1f;

	public static int[][] AgeBrackets = new int[][]
	{
		new int[]
		{
			20,
			30
		},
		new int[]
		{
			30,
			50
		},
		new int[]
		{
			45,
			62
		}
	};

	[Serializable]
	public class ThoughtEffect
	{
		public ThoughtEffect()
		{
		}

		public ThoughtEffect(MoodEffect mood, float value)
		{
			this.Thought = mood.Thought;
			this._mood = mood;
			this.Effect = value;
			this.Frames = 5;
		}

		public MoodEffect Mood
		{
			get
			{
				if (this._mood == null)
				{
					this._mood = GameData.MoodEffects[this.Thought];
				}
				return this._mood;
			}
		}

		public void ResetMood()
		{
			this._mood = null;
		}

		public string Thought;

		[NonSerialized]
		private MoodEffect _mood;

		public float Effect;

		public int Frames;
	}

	public enum EmployeeRole
	{
		Lead,
		Programmer,
		Designer,
		Artist,
		Marketer
	}

	[Flags]
	public enum RoleBit
	{
		None = 0,
		Lead = 1,
		Programmer = 2,
		Designer = 4,
		Artist = 8,
		Marketer = 16,
		AnyRole = 30,
		AllRoles = 31,
		Educateable = 14
	}

	public enum WageBracket
	{
		Low,
		Medium,
		High
	}

	public enum Status
	{
		Enable,
		Freeze,
		Disable
	}
}
