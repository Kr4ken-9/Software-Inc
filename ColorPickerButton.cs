﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ColorPickerButton : MonoBehaviour
{
	public Button.ButtonClickedEvent OnClick
	{
		get
		{
			return this.button.onClick;
		}
	}

	public Button button;

	public Image ColorRect;

	public Text Label;
}
