﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

public class Actor : Selectable, IDistributee, IHasSpeed, IStylable
{
	public InteractionPoint UsingPoint
	{
		get
		{
			return this._usingPoint;
		}
		set
		{
			if (this._usingPoint != value)
			{
				if (this._usingPoint != null)
				{
					if (this._usingPoint.Parent.MaxQueue > 0 && this.QueuedFor(this._usingPoint.Parent.Type) && this.InQueue[this._usingPoint.Parent.Type].Parent == this._usingPoint.Parent)
					{
						this.InQueue.Remove(this._usingPoint.Parent.Type);
						this._usingPoint.RemoveFromQueue(this);
					}
					this._usingPoint.UsedBy = null;
				}
				if (value != null && value.Parent.MaxQueue > 0 && value.QueueLength == 0)
				{
					value.AddToQueue(this);
					this.InQueue[value.Parent.Type] = value;
				}
				this._usingPoint = value;
				if (this._usingPoint != null)
				{
					this._usingPoint.UsedBy = this;
				}
			}
		}
	}

	public bool HasAssignedRooms
	{
		get
		{
			return this.AssignedRoomGroups.Count > 0;
		}
	}

	public Employee employee
	{
		get
		{
			return this._employee;
		}
		set
		{
			if (this._employee != null)
			{
				this._employee.MyActor = null;
			}
			this._employee = value;
			if (this._employee != null)
			{
				this._employee.MyActor = this;
			}
		}
	}

	public Furniture Reserved
	{
		get
		{
			return this._reserved;
		}
		set
		{
			if (value != this._reserved)
			{
				if (this._reserved != null)
				{
					this._reserved.Reserved = null;
				}
				this._reserved = value;
				if (this._reserved != null)
				{
					this._reserved.Reserved = this;
				}
			}
		}
	}

	public SDateTime VacationMonth
	{
		get
		{
			return this._vacationMonth;
		}
		set
		{
			this.AlternateVacation = value;
			this._vacationMonth = value;
		}
	}

	public List<ActorBodyItem> BodyItems
	{
		get
		{
			return this._bodyItems;
		}
		set
		{
			this._bodyItems = value;
		}
	}

	public Transform GetTransform()
	{
		return base.transform;
	}

	public float Noisiness
	{
		get
		{
			return this._noisiness;
		}
		set
		{
			float num = 1f;
			this._noisiness = value * num;
		}
	}

	public void UpdateEyes()
	{
		ActorBodyItem actorBodyItem = this._bodyItems.FirstOrDefault((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head);
		if (actorBodyItem != null)
		{
			base.GetComponent<EyeScript>().Face = actorBodyItem.rend.material;
		}
	}

	public void UpdateHairColor(Color col)
	{
		this.HairColor = col;
	}

	public void UpdateSkinColor(Color col)
	{
		this.SkinColor = col;
	}

	public bool IsAssignedRoom(Room r)
	{
		foreach (string name in this.AssignedRoomGroups)
		{
			RoomGroup roomGroup = GameSettings.Instance.GetRoomGroup(name);
			if (roomGroup != null)
			{
				List<Room> rooms = roomGroup.GetRooms();
				for (int i = 0; i < rooms.Count; i++)
				{
					if (r == rooms[i])
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public IEnumerable<Room> GetAssignedRooms()
	{
		bool any = !this.HasAssignedRooms;
		Actor.IteratedAssignedRooms.Clear();
		Actor.DeletedAssignedRooms.Clear();
		foreach (string group in this.AssignedRoomGroups)
		{
			RoomGroup g = GameSettings.Instance.GetRoomGroup(group);
			if (g != null)
			{
				List<Room> rs = g.GetRooms();
				for (int i = 0; i < rs.Count; i++)
				{
					if (!Actor.IteratedAssignedRooms.Contains(rs[i].DID))
					{
						any = true;
						yield return rs[i];
						Actor.IteratedAssignedRooms.Add(rs[i].DID);
					}
				}
			}
			else
			{
				Actor.DeletedAssignedRooms.Add(group);
			}
		}
		if (!any)
		{
			HUD.Instance.AddPopupMessage("StaffRoomAssignError".Loc(), "Exclamation", PopupManager.PopUpAction.GotoEmp, this.DID, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.EmptyRoomGroup, 1);
		}
		for (int j = 0; j < Actor.DeletedAssignedRooms.Count; j++)
		{
			this.AssignedRoomGroups.Remove(Actor.DeletedAssignedRooms[j]);
		}
		yield break;
	}

	public int SpawnTime
	{
		get
		{
			if (!(this.AIScript is EmployeeAI))
			{
				return this.StaffOn;
			}
			if (this.Team != null)
			{
				return (this.GetTeam().WorkStart != 0) ? (this.GetTeam().WorkStart - 1) : 23;
			}
			return 7;
		}
	}

	public bool TakingCourses
	{
		get
		{
			return this.CoursePoints > 0f || this.HREd;
		}
	}

	public int Floor
	{
		get
		{
			return Mathf.FloorToInt((base.transform.position.y + 1f) / 2f);
		}
	}

	public Room currentRoom
	{
		get
		{
			return (!(this._currentRoom == null)) ? this._currentRoom : GameSettings.Instance.sRoomManager.Outside;
		}
		set
		{
			if (value != this._currentRoom)
			{
				if (this._currentRoom != null)
				{
					this._currentRoom.Occupants.Remove(this);
				}
				this._currentRoom = value;
				if (this._currentRoom != null)
				{
					this._currentRoom.Occupants.Add(this);
				}
			}
		}
	}

	public float TeamCompatibility
	{
		get
		{
			if (this.Team == null)
			{
				return -1f;
			}
			return this.teamComp;
		}
		set
		{
			this.teamComp = value;
		}
	}

	public string Team
	{
		get
		{
			return (this.team != null) ? this.team.Name : null;
		}
		set
		{
			if (this.AItype != AI<Actor>.AIType.Employee)
			{
				return;
			}
			if (this.team != null)
			{
				this.team.RemoveEmployee(this);
			}
			if (string.IsNullOrEmpty(value))
			{
				this.team = null;
				if (this.employee.IsRole(Employee.RoleBit.Lead))
				{
					this.employee.ChangeToNaturalRole(false);
				}
			}
			else
			{
				Team orNull = GameSettings.Instance.sActorManager.Teams.GetOrNull(value);
				if (orNull != null)
				{
					this.team = orNull;
					this.team.AddEmployee(this);
					this.team.CheckWorkRoleAssignment();
					this.ScheduleVacation(false);
				}
				else
				{
					this.team = null;
				}
			}
			if (HUD.Instance != null && HUD.Instance.employeeWindow != null && HUD.Instance.employeeWindow.EmployeeList != null)
			{
				HUD.Instance.employeeWindow.UpdateEmployeeList();
			}
		}
	}

	public float RefreshTime
	{
		get
		{
			return this.rTime;
		}
		set
		{
			this.rTime = value;
		}
	}

	public bool IsValid
	{
		get
		{
			return this != null && base.gameObject != null;
		}
	}

	public Team GetTeam()
	{
		return this.team;
	}

	public override string[] GetActions()
	{
		return (this.AItype != AI<Actor>.AIType.Employee) ? ((this.AItype != AI<Actor>.AIType.Guest) ? this.staffActions : new string[0]) : ((!this.employee.Founder) ? this.actions : this.actionsFounder);
	}

	public void SetCar(int carIdx)
	{
		if (this.CarIdx != carIdx)
		{
			this.CarIdx = carIdx;
			this.CarColor3 = RoadManager.Instance.CarPrefabs[this.CarIdx].GetComponent<NormalCar>().ColorPick.Evaluate(UnityEngine.Random.value);
		}
	}

	public string LimitText(float value, float limit)
	{
		if (value > limit)
		{
			value = 1f;
		}
		else
		{
			value /= limit;
		}
		return (value * 100f).ToString("F0");
	}

	public override string GetInfo()
	{
		if (this.AItype == AI<Actor>.AIType.Employee)
		{
			StringBuilder stringBuilder = new StringBuilder(string.Format("{0} - {1}{2}\n{4}: {3}\n{5}", new object[]
			{
				this.employee.FullName,
				this.employee.RoleString,
				(!this.employee.Founder) ? string.Empty : (" - " + "Founder".Loc()),
				this.CurrentState(true),
				"State".Loc(),
				(!this.IsIdle) ? string.Empty : ("NotWorkingState".Loc() + ": " + this.IdleStatus.ToString().Loc())
			}));
			return stringBuilder.ToString();
		}
		if (this.AItype == AI<Actor>.AIType.Guest)
		{
			return (this.deal == null || !this.deal.StillValid(false)) ? this.employee.Name : string.Format("GuestCompany".Loc(), this.employee.Name, this.deal.Client.Name);
		}
		return string.Format("{0} ({3})\n{2}: {1}", new object[]
		{
			this.employee.Name,
			this.CurrentState(true),
			"State".Loc(),
			Utilities.HourToTime(this.StaffOn, SDateTime.AMPM)
		});
	}

	public bool QueuedFor(string type)
	{
		return this.InQueue.ContainsKey(type);
	}

	public bool IsUp(string type)
	{
		InteractionPoint interactionPoint = this.InQueue[type];
		return interactionPoint.IsUp(this);
	}

	public override string[] GetExtendedInfo()
	{
		if (this.AItype == AI<Actor>.AIType.Employee)
		{
			return new string[]
			{
				(!string.IsNullOrEmpty(this.Team)) ? this.Team : "Unassigned".Loc(),
				"YearPostfix".LocPlural(Mathf.FloorToInt(this.employee.Age)),
				((!this.WorksForFree()) ? this.employee.Salary : 0f).Currency(true),
				(this.Effectiveness * 100f).ToString("F2") + "%",
				(this.employee.JobSatisfaction * 100f).ToString("F2") + "%"
			};
		}
		return new string[]
		{
			this.AItype.ToString().Loc()
		};
	}

	public override Color[] GetExtendedColorInfo()
	{
		return new Color[]
		{
			base.GetColorStat(1f),
			base.GetColorStat(1f),
			base.GetColorStat(this.Effectiveness),
			base.GetColorStat(this.employee.JobSatisfaction * 2f)
		};
	}

	public override string[] GetExtendedIconInfo()
	{
		if (this.AItype == AI<Actor>.AIType.Employee)
		{
			return new string[]
			{
				"MoreEmployees",
				"Employee",
				"Money",
				"Cogs",
				"Smiley"
			};
		}
		return new string[]
		{
			"Staff"
		};
	}

	public override string[] GetExtendedTooltipInfo()
	{
		if (this.AItype == AI<Actor>.AIType.Employee)
		{
			return new string[]
			{
				"Age".Loc(),
				"Salary".Loc(),
				"Effectiveness".Loc(),
				"Satisfaction".Loc()
			};
		}
		return null;
	}

	public override IEnumerable<Selectable> GetRelated()
	{
		if (this.AItype == AI<Actor>.AIType.Employee)
		{
			if (this.Owns == null)
			{
				yield break;
			}
			foreach (Furniture item in this.Owns)
			{
				yield return item;
			}
		}
		else if (this.AItype != AI<Actor>.AIType.Guest)
		{
			foreach (Room item2 in this.GetAssignedRooms())
			{
				yield return item2;
			}
		}
		yield break;
	}

	public void MeetNow()
	{
		this.Timer = -1f;
		this.employee.Energy = 1f;
		this.employee.Stress = Mathf.Clamp01(this.employee.Stress + Utilities.GetHours(this.MeetingTime, SDateTime.Now()) / 24f * this.employee.Diligence.MapRange(-1f, 1f, 1.5f, 0.5f, false));
		if (!this.IsCrunching() && this.CrunchHangover > 0f)
		{
			this.CrunchHangover = Mathf.Max(0f, this.CrunchHangover - Utilities.GetMonths(this.MeetingTime, SDateTime.Now()) * 24f);
		}
		this.MeetingTime = SDateTime.Now();
		this.UpdateCostPerMinute();
		int hour = 8;
		Team team = this.GetTeam();
		if (team != null)
		{
			int workStart = team.WorkStart;
			int workEnd = team.WorkEnd;
			hour = ((workStart <= workEnd) ? (workEnd - workStart) : (24 - workStart + workEnd));
		}
		float f = UnityEngine.Random.Range((1f - this.employee.Diligence) * -45f, this.employee.Diligence * 45f);
		this.LeaveTime = SDateTime.Now() + new SDateTime(Mathf.RoundToInt(f), hour, 0, 0, 0);
		this.anim.enabled = true;
		this.GoHomeNow = false;
		for (int i = 0; i < this.Affactors.Length; i++)
		{
			this.Affactors[i] = -2f;
		}
		this.AIScript.currentNode = this.AIScript.BehaviorNodes["Spawn"];
	}

	public string CurrentState(bool withDetail)
	{
		if (this == null)
		{
			return string.Empty;
		}
		if (!base.enabled && this.TakingCourses && (this.SpecialState != Actor.HomeState.Vacation || !this.VacationMonth.EqualsVerySimple(SDateTime.Now())))
		{
			if (withDetail)
			{
				SDateTime? arriveTime = GameSettings.Instance.sActorManager.GetArriveTime(this);
				if (arriveTime != null)
				{
					return string.Format("{0}\n{1}", "In class".Loc(), "ReturnDate".Loc(new object[]
					{
						Utilities.DateDiff(SDateTime.Now(), arriveTime.Value)
					}));
				}
			}
			return "In class".Loc();
		}
		string str = (!withDetail || !base.enabled) ? string.Empty : (" (" + this.AIScript.CurrentNodeLabel.Loc() + ")");
		if (this.SpecialState != Actor.HomeState.Default)
		{
			return this.SpecialState.ToString().Loc() + str;
		}
		if (this.employee.Fired)
		{
			return "Dismissed".Loc() + str;
		}
		if (base.enabled)
		{
			return "At work".Loc() + str;
		}
		return "At home".Loc();
	}

	private void OnDestroy()
	{
		if (GameSettings.IsQuitting || GameSettings.Instance == null)
		{
			return;
		}
		this.currentRoom = null;
		this.Order.RemoveFromStorage();
		GameSettings.Instance.sRoomManager.ClearReservations(this);
		GameSettings.Instance.RegisterActor(this, false);
		if (HUD.Instance != null)
		{
			HUD.Instance.CantGetHome.Remove(this);
			HUD.Instance.wageWindow.List.Items.Remove(this);
			HUD.Instance.RemoveFromIdle(this);
			if (HUD.Instance.DetailWindow.CurrentEmployee == this)
			{
				HUD.Instance.DetailWindow.Window.Close();
			}
		}
		for (int i = 0; i < GameSettings.Instance.sRoomManager.Rooms.Count; i++)
		{
			Room room = GameSettings.Instance.sRoomManager.Rooms[i];
			room.Occupants.Remove(this);
		}
		if (this.UsingPoint != null && this.UsingPoint.UsedBy == this)
		{
			this.UsingPoint.UsedBy = null;
		}
		this.Owns.ToList<Furniture>().ForEach(delegate(Furniture x)
		{
			if (x != null && x.OwnedBy == this)
			{
				x.OwnedBy = null;
			}
		});
		if (this.Team != null)
		{
			this.GetTeam().RemoveEmployee(this);
		}
		if (GameSettings.Instance != null)
		{
			GameSettings.Instance.sActorManager.Guests.Remove(this);
			GameSettings.Instance.sActorManager.Actors.Remove(this);
			GameSettings.Instance.sActorManager.Staff.Remove(this);
			GameSettings.Instance.sActorManager.RemoveFromAwaiting(this);
			GameSettings.Instance.sActorManager.ReadyForBus.Remove(this);
			GameSettings.Instance.sActorManager.ReadyForHome.Remove(this);
		}
		if (SelectorController.Instance != null && SelectorController.Instance.Selected.Contains(this))
		{
			SelectorController.Instance.ToggleRightClickMenu(false);
			SelectorController.Instance.Selected.Remove(this);
		}
	}

	public void UpdateCurrentRoom(bool force = false)
	{
		if ((this.LastPos != base.transform.position || force) && (this.currentRoom.Dummy || this.currentRoom.Floor != this.Floor || !this.currentRoom.IsInside(new Vector2(base.transform.position.x, base.transform.position.z), 0f)))
		{
			Room roomFromPoint = GameSettings.Instance.sRoomManager.GetRoomFromPoint(base.transform.position);
			int num = 0;
			while (roomFromPoint == null && num < 40)
			{
				if (this.Floor > 0)
				{
					base.transform.position = new Vector3(base.transform.position.x, base.transform.position.y - 2f, base.transform.position.z);
				}
				if (this.Floor < 0)
				{
					base.transform.position = new Vector3(base.transform.position.x, base.transform.position.y + 2f, base.transform.position.z);
				}
				roomFromPoint = GameSettings.Instance.sRoomManager.GetRoomFromPoint(base.transform.position);
				num++;
			}
			this.currentRoom = roomFromPoint;
			this.LastPos = base.transform.position;
		}
		if (HUD.Instance != null)
		{
			this.AudioComp.outputAudioMixerGroup = ((!(GameSettings.Instance.sRoomManager.CameraRoom == this.currentRoom)) ? AudioManager.InGameHighPass : AudioManager.InGameNormal);
		}
	}

	public void ResetState()
	{
		this.CensorCube.SetActive(false);
		this.CurrentPath = null;
		this.anim.SetInteger("AnimControl", 0);
		this.anim.Play("Idle", 0, 0f);
		this.UsingPoint = null;
		switch (this.AItype)
		{
		case AI<Actor>.AIType.Employee:
			this.AIScript.currentNode = this.AIScript.BehaviorNodes["GoToDesk"];
			break;
		case AI<Actor>.AIType.Janitor:
		case AI<Actor>.AIType.IT:
			this.AIScript.currentNode = this.AIScript.BehaviorNodes["FindRepair"];
			break;
		case AI<Actor>.AIType.Cleaning:
			this.AIScript.currentNode = this.AIScript.BehaviorNodes["FindCleanRoom"];
			break;
		case AI<Actor>.AIType.Receptionist:
		case AI<Actor>.AIType.Guest:
			this.AIScript.currentNode = this.AIScript.BehaviorNodes["IsOff"];
			break;
		case AI<Actor>.AIType.Courier:
			this.AIScript.currentNode = this.AIScript.BehaviorNodes["HasCopies"];
			break;
		}
		base.transform.position = new Vector3(base.transform.position.x, (float)(this.Floor * 2), base.transform.position.z);
		this.UpdateCurrentRoom(true);
		this.currentRoom.FixActorPosition(this, false);
	}

	private void Init()
	{
		if (this.AIScript == null)
		{
			this.AIScript = AI<Actor>.LoadAI(this.AItype);
		}
		if (this.employee == null || string.IsNullOrEmpty(this.employee.Name))
		{
			this.employee = new Employee(SDateTime.Now(), (Employee.EmployeeRole)UnityEngine.Random.Range(0, 5), this.Female, Employee.WageBracket.Medium, (!(GameSettings.Instance == null)) ? GameSettings.Instance.Personalities : GameData.AllPersonalities(), false, null, null, null, 1f, 0.1f);
		}
		this.AIScript.Initialize();
		this.InitBenefits();
		this._shadow = ActorStyler.Instance.InitShadow(this);
		if (this._savedStyle == null)
		{
			this._savedStyle = ActorStyler.Instance.GenerateStyle(this.Female, "Default");
		}
		ActorStyler.Instance.ApplySavedStyle(this._savedStyle, this);
		if (this.AItype == AI<Actor>.AIType.Employee)
		{
			if (!GameSettings.Instance.sActorManager.Actors.Contains(this))
			{
				GameSettings.Instance.sActorManager.Actors.Add(this);
			}
		}
		else if (this.AItype == AI<Actor>.AIType.Guest)
		{
			GameSettings.Instance.sActorManager.Guests.Add(this);
		}
		else if (!GameSettings.Instance.sActorManager.Staff.Contains(this))
		{
			GameSettings.Instance.sActorManager.Staff.Add(this);
		}
	}

	private void Awake()
	{
		this.Affactors = new float[Actor.AffectorCount];
		for (int i = 0; i < this.Affactors.Length; i++)
		{
			this.Affactors[i] = -2f;
		}
		base.InitWritable();
	}

	private void Start()
	{
		this._eyeScript = base.GetComponent<EyeScript>();
		this.AudioComp = base.GetComponentInChildren<AudioSource>();
		if (!this.Deserialized)
		{
			this.CurrentPath = null;
			if (this.AItype == AI<Actor>.AIType.Guest)
			{
				this._savedStyle = ActorStyler.Instance.GenerateStyle(this.Female, "Business");
			}
			if (this.AItype == AI<Actor>.AIType.Janitor || this.AItype == AI<Actor>.AIType.Courier)
			{
				this._savedStyle = ActorStyler.Instance.GenerateStyle(this.Female, "Handy");
			}
			if (this.AItype == AI<Actor>.AIType.Cleaning)
			{
				this._savedStyle = ActorStyler.Instance.GenerateStyle(this.Female, "Cleaning");
			}
			if (this.AItype == AI<Actor>.AIType.Cook)
			{
				this._savedStyle = ActorStyler.Instance.GenerateStyle(this.Female, "Cook");
			}
			this.Init();
			if (GameSettings.Instance.sActorManager.Teams.Count == 1 && this.Team == null)
			{
				this.Team = GameSettings.Instance.sActorManager.Teams.Keys.First<string>();
			}
			this.ScheduleVacation(true);
			this.MeetingTime = SDateTime.Now();
			this.LastMeeting = this.employee.Hired;
			this.LastSocial = this.employee.Hired;
			if (this.WaitSpawn)
			{
				SDateTime sdateTime = SDateTime.Now();
				SDateTime sdateTime2 = (this.team == null) ? sdateTime : new SDateTime(0, this.team.WorkStart - 1, sdateTime.Day, sdateTime.Month, sdateTime.Year);
				sdateTime2 += new SDateTime(1, 0, 0);
				GameSettings.Instance.sActorManager.AddToAwaiting(this, sdateTime2, false, true);
				base.enabled = false;
				this.SetVisible(false);
			}
			if (this.AItype == AI<Actor>.AIType.Cleaning)
			{
				this.GetItem("Broom", true);
			}
			if (this.AItype == AI<Actor>.AIType.Janitor)
			{
				this.GetItem("Hammer", true);
			}
			if (this.AItype == AI<Actor>.AIType.Guest)
			{
				this.GetItem("Briefcase", true);
			}
		}
		else
		{
			this.UpdateCurrentRoom(true);
			if (base.enabled && this.currentRoom.GetNodeAt(new Vector2(base.transform.position.x, base.transform.position.z)) == null)
			{
				this.currentRoom.FixActorPosition(this, true);
			}
		}
		GameSettings.Instance.RegisterActor(this, true);
		this.Deserialized = false;
		this.UpdateChildren();
	}

	public bool MayPlaySound()
	{
		return this.Floor == GameSettings.Instance.ActiveFloor && (CameraScript.Instance.Listener.transform.position - base.transform.position).sqrMagnitude < this.AudioComp.maxDistance * this.AudioComp.maxDistance;
	}

	private void LateUpdate()
	{
		if (this.AIScript.currentNode.Name.Equals("HaveMeeting") && this.Team != null && this.GetTeam().Talking != null && this.GetTeam().Talking != this && this.GetTeam().Talking.AIScript.currentNode.Name.Equals("HaveMeeting"))
		{
			this.LookAt(this.GetTeam().Talking.NeckBone.position);
		}
	}

	private void LookAt(Vector3 pos)
	{
		Vector3 forward = pos - this.NeckBone.position;
		if (Vector3.Angle(this.NeckBone.up.normalized, forward.normalized) < 70f)
		{
			this.NeckBone.transform.rotation = Quaternion.LookRotation(forward) * Quaternion.Euler(0f, -90f, -90f);
			this.NeckBone.transform.localRotation = this.NeckBone.transform.localRotation * Quaternion.Euler(0f, 0f, -20f);
		}
	}

	private bool IsStandingUp()
	{
		AnimatorStateInfo currentAnimatorStateInfo = this.anim.GetCurrentAnimatorStateInfo(0);
		int integer = this.anim.GetInteger("AnimControl");
		return currentAnimatorStateInfo.IsTag("SitStates") && integer != 3 && integer != 4 && integer != 5 && integer != 7 && integer != 12 && integer != 13;
	}

	private void FixedUpdate()
	{
		if (!base.isActiveAndEnabled)
		{
			return;
		}
		this.anim.speed = GameSettings.GameSpeed;
		bool flag = base.enabled && ((GameSettings.Instance.ActiveFloor >= 0 && this.currentRoom == GameSettings.Instance.sRoomManager.Outside) || Mathf.Abs(base.transform.position.y / 2f - (float)GameSettings.Instance.ActiveFloor) < 0.8f || (this.Floor < GameSettings.Instance.ActiveFloor && (this.IsHighlight || this.currentRoom.Outdoors)) || CameraScript.Instance.FlyMode);
		if (this.LastVisible ^ flag)
		{
			this.SetVisible(flag);
			this._eyeScript.enabled = flag;
		}
		this.AudioComp.volume = ((GameSettings.GameSpeed != 0f) ? 1f : 0f);
		if (GameSettings.GameSpeed == 0f)
		{
			return;
		}
		if (!this.IsCrunching() && this.CrunchHangover > 0f)
		{
			this.CrunchHangover = Mathf.Max(0f, this.CrunchHangover - Time.deltaTime * GameSettings.GameSpeed / (float)GameSettings.DaysPerMonth / 60f);
		}
		if (this.LastCheckWait >= 0f)
		{
			this.LastCheckWait -= Time.deltaTime * GameSettings.GameSpeed;
		}
		this.Noisiness = 1f;
		this.anim.SetBool("RightHand", this.Holding[0] != null && this.Holding[0].HoldStraight);
		this.anim.SetBool("LeftHand", this.Holding[1] != null && this.Holding[1].HoldStraight);
		this._eyeScript.Energy = this.employee.Energy;
		this._eyeScript.Happy = (this.Effectiveness > 2f);
		if (!this.GoHomeNow)
		{
			if (this.AItype == AI<Actor>.AIType.Employee)
			{
				if (SDateTime.Now() > this.LeaveTime)
				{
					this.GoHomeNow = true;
				}
			}
			else if ((SDateTime.Now() - this.MeetingTime).ToInt() > 60 * ((!this.OnCall) ? 4 : 8))
			{
				this.GoHomeNow = true;
			}
		}
		if (this.AItype == AI<Actor>.AIType.Employee && !this.Despawned)
		{
			if (this.SocialFactor > 0f && (this.GoHomeNow || GameSettings.Instance.sActorManager.Actors.Count < 4))
			{
				this.SocialFactor = 0f;
			}
			float num = (this.SocialFactor <= 0f) ? this.employee.Leadership.MapRange(-1f, 1f, 6f, 2f, false) : this.employee.Leadership.MapRange(-1f, 1f, 0.05f, 1.5f, false);
			float num2 = (this.StressFactor <= 0f) ? 1f : this.employee.Diligence.MapRange(-1f, 1f, 0.1f, 1.2f, false);
			float num3 = this._cachedBenefitValue / EmployeeBenefit.MaxBenefits;
			if (num3 >= 0f)
			{
				this.employee.SetMood("GoodBenefits", this, num3);
				this.employee.SetMood("BadBenefits", this, 0f);
			}
			else
			{
				this.employee.SetMood("GoodBenefits", this, 0f);
				this.employee.SetMood("BadBenefits", this, -num3);
			}
			if (this.IsIdle)
			{
				this.employee.AddMood("IdleBored", this);
			}
			this.employee.Update(Time.deltaTime * GameSettings.GameSpeed, this.WorksForFree(), this.GoHomeNow, this.AIScript.currentNode.SpecialID == 99, (this.AIScript.currentNode.SpecialID != 77) ? ((!this.QueuedFor("Toilet")) ? Employee.Status.Enable : Employee.Status.Freeze) : Employee.Status.Disable, (this.AIScript.currentNode.SpecialID != 88) ? ((!this.QueuedFor("FastFood") && !this.QueuedFor("Tray")) ? Employee.Status.Enable : Employee.Status.Freeze) : Employee.Status.Disable, this.StressFactor * num2, this.SocialFactor * num, !this.ShouldWork, this);
			this.SocialFactor = 1f;
			if (!this.IsWorking)
			{
				this.StressFactor = -1f;
			}
			this.Fatigue();
			this.UpdateProblems();
		}
		if (!this.employee.Founder && this.Team != null)
		{
			this.TeamRelationTimer -= Time.deltaTime * GameSettings.GameSpeed;
			if (this.TeamRelationTimer <= 0f)
			{
				this.TeamRelationTimer = Actor.TeamRelationTimerMax + Utilities.RandomRange(-2f, 2f);
				List<Actor> employeesDirect = this.GetTeam().GetEmployeesDirect();
				if (employeesDirect.Count > 1)
				{
					for (int i = 0; i < employeesDirect.Count; i++)
					{
						int num4 = (i + this.TeamRelationNum) % employeesDirect.Count;
						Actor actor = employeesDirect[num4];
						if (actor.enabled && actor != this)
						{
							Employee employee = actor.employee;
							float num5 = this.employee.Compatibility(employee);
							employee.AddInstantMood((num5 < 1f) ? "DislikeTeamWork" : "LikeTeamWork", actor, Mathf.Abs(num5 - 1f));
							this.TeamRelationNum = (num4 + 1) % employeesDirect.Count;
							break;
						}
					}
				}
			}
		}
		bool shouldWork = this.ShouldWork;
		this.ShouldWork = false;
		if (this.anim.GetCurrentAnimatorStateInfo(0).IsTag("VehicleOut"))
		{
			this.TestVarVehicle += Time.deltaTime * GameSettings.GameSpeed;
		}
		else if (this.AIScript.currentNode.SpecialID != 5 && (this.anim.IsInTransition(0) || this.IsStandingUp()))
		{
			this.TestVarStand += Time.deltaTime * GameSettings.GameSpeed;
		}
		else if (this.Turn)
		{
			this.TestVarVehicle = 0f;
			this.TestVarStand = 0f;
			this.TurnAround();
		}
		else
		{
			this.TestVarVehicle = 0f;
			this.TestVarStand = 0f;
			string name = this.AIScript.currentNode.Name;
			this.AIScript.RunSimulation(this);
			if (this.AIScript.currentNode == null)
			{
				Debug.LogError("Went from: " + name + " to nothing");
			}
			else if (this.AIScript.currentNode.SpecialID != 1 && this.AIScript.LastResult > 0)
			{
				this.FreeLoiterTable();
			}
		}
		if (!this.ShouldWork && this.IsWorking)
		{
			this.IsWorking = false;
		}
		if (this.Floor == GameSettings.Instance.ActiveFloor && this.AItype == AI<Actor>.AIType.Employee && this.IsWorking)
		{
			this.NextParticle -= Time.deltaTime * Mathf.Lerp(0.5f, 2f, this.Effectiveness) * (float)Mathf.Max(0, HUD.Instance.GameSpeed);
			if (this.NextParticle <= 0f)
			{
				ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams
				{
					position = this.NeckBone.position,
					startColor = Color.Lerp(HUD.ThemeColors[2], HUD.ThemeColors[0], this.Effectiveness)
				};
				HUD.Instance.EffectivenessEmitter[this.EmitType].Emit(emitParams, Mathf.CeilToInt((this.Effectiveness + 0.1f) * 5f));
				this.NextParticle = 1f;
			}
		}
		if (this.Floor == GameSettings.Instance.ActiveFloor && ((this.AIScript.currentNode.SpecialID == 1 && !this.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk")) || (shouldWork && this.ShouldWork && !this.IsWorking)))
		{
			this.NextParticle -= Time.deltaTime;
			if (this.NextParticle <= 0f)
			{
				ParticleSystem.EmitParams emitParams2 = new ParticleSystem.EmitParams
				{
					position = this.NeckBone.position
				};
				HUD.Instance.ZnoreEmitter.Emit(emitParams2, 1);
				this.NextParticle = 1f;
			}
			this._eyeScript.sleep = true;
		}
		else
		{
			this._eyeScript.sleep = false;
		}
	}

	private void OnDisable()
	{
		this.currentRoom = null;
	}

	private void OnEnable()
	{
		if (this.employee != null)
		{
			this.UpdateAgeLook();
		}
	}

	public void UpdateChildren()
	{
		this.Children = base.GetComponentsInChildren<Renderer>(true);
	}

	public void SetVisible(bool visible)
	{
		this.LastVisible = visible;
		bool flag = false;
		if (this.Children == null)
		{
			this.UpdateChildren();
		}
		for (int i = 0; i < this.Children.Length; i++)
		{
			if (this.Children[i] == null)
			{
				flag = true;
			}
			else
			{
				this.Children[i].enabled = visible;
			}
		}
		for (int j = 0; j < this.Colliders.Length; j++)
		{
			this.Colliders[j].enabled = visible;
		}
		if (flag)
		{
			this.UpdateChildren();
		}
	}

	public void UpdateAgeLook()
	{
		float num = (this.employee.Age < 50f) ? 0f : ((this.employee.Age - 50f) / 10f);
		num = Mathf.Clamp01(num);
		ActorBodyItem actorBodyItem = this._bodyItems.FirstOrDefault((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Hair);
		if (actorBodyItem != null)
		{
			ActorBodyItem.ColorMapping colorMapping = actorBodyItem.Colormap.FirstOrDefault((ActorBodyItem.ColorMapping x) => x.ColorName.Equals("Color"));
			if (colorMapping != null)
			{
				actorBodyItem.rend.material.SetColor(colorMapping.MaterialSlot, this.HairColor * (1f - num) + Color.gray * num);
			}
		}
		ActorBodyItem actorBodyItem2 = this._bodyItems.FirstOrDefault((ActorBodyItem x) => x.Type == ActorBodyItem.BodyType.Head);
		if (actorBodyItem2 != null)
		{
			actorBodyItem2.rend.material.SetFloat("_Overlay2Factor", num);
		}
	}

	public void UpdateCostPerMinute()
	{
		float realSalary = this.GetRealSalary();
		if (realSalary > 0f)
		{
			int num = 8;
			Team team = this.GetTeam();
			if (team != null)
			{
				int workStart = team.WorkStart;
				int workEnd = team.WorkEnd;
				num = ((workStart <= workEnd) ? (workEnd - workStart) : (24 - workStart + workEnd));
			}
			this.CostPerMinute = realSalary / ((float)(num * GameSettings.DaysPerMonth) * 60f);
		}
		else
		{
			this.CostPerMinute = 0f;
		}
	}

	private void TurnAround()
	{
		bool flag = true;
		float y = base.transform.rotation.eulerAngles.y;
		if (Mathf.Approximately(y, this.TargetRot))
		{
			this.Turn = false;
			flag = false;
			this.anim.SetInteger("AnimControl", this.LastAnim);
		}
		float num = this.TargetRot - y;
		int num2 = (Mathf.Abs(num) <= 180f) ? Utilities.Sign(num) : (-Utilities.Sign(num));
		float num3 = Utilities.Modulo(y + (float)num2 * Time.deltaTime * GameSettings.GameSpeed * 200f, 360f);
		if (flag)
		{
			this.anim.SetInteger("AnimControl", (num2 >= 0) ? 8 : 9);
		}
		if (y != Utilities.Modulo(this.TargetRot - 180f, 360f) && Utilities.AnglePassed(y, num3, this.TargetRot))
		{
			num3 = this.TargetRot;
			this.Turn = false;
			this.anim.SetInteger("AnimControl", this.LastAnim);
		}
		base.transform.rotation = Quaternion.Euler(base.transform.rotation.eulerAngles.x, num3, base.transform.rotation.eulerAngles.z);
	}

	public Holdable GetItemAnyHand(string name)
	{
		int num = (!(this.Holding[0] == null)) ? 1 : 0;
		bool flag = num == 0;
		if (this.Holding[num] == null)
		{
			Holdable component = ItemDispenser.Instance.Dispense(name).GetComponent<Holdable>();
			component.transform.SetParent((!flag) ? this.LeftHand : this.RightHand, true);
			component.transform.localPosition = new Vector3((!flag) ? (-component.OffsetTranslation.x) : component.OffsetTranslation.x, component.OffsetTranslation.y, component.OffsetTranslation.z);
			component.transform.localRotation = Quaternion.Euler(component.OffsetRotation.x, component.OffsetRotation.y, (!flag) ? (-component.OffsetRotation.z) : component.OffsetRotation.z);
			this.Holding[(!flag) ? 1 : 0] = component;
			component.GetComponent<Renderer>().enabled = this.LastVisible;
			this.UpdateChildren();
			return component;
		}
		return null;
	}

	public Holdable GetItem(string name, bool right)
	{
		if ((right && this.Holding[0] == null) || (!right && this.Holding[1] == null))
		{
			Holdable component = ItemDispenser.Instance.Dispense(name).GetComponent<Holdable>();
			component.transform.SetParent((!right) ? this.LeftHand : this.RightHand, true);
			component.transform.localPosition = new Vector3(component.OffsetTranslation.x, component.OffsetTranslation.y, (!right) ? (-component.OffsetTranslation.z) : component.OffsetTranslation.z);
			component.transform.localRotation = Quaternion.Euler((!right) ? (-component.OffsetRotation.x) : component.OffsetRotation.x, component.OffsetRotation.y, (!right) ? (-component.OffsetRotation.z) : component.OffsetRotation.z);
			this.Holding[(!right) ? 1 : 0] = component;
			component.GetComponent<Renderer>().enabled = this.LastVisible;
			this.UpdateChildren();
			return component;
		}
		return null;
	}

	public void LeaveItem(Holdable item, bool destroy = false)
	{
		if (this.Holding[0] == item)
		{
			this.Holding[0] = null;
			this.UpdateChildren();
		}
		if (this.Holding[1] == item)
		{
			this.Holding[1] = null;
			this.UpdateChildren();
		}
		if (destroy)
		{
			item.DestroyMe();
		}
	}

	public bool ReTakeItemAnyHand(Holdable item)
	{
		int num = (!(this.Holding[0] == null)) ? 1 : 0;
		bool flag = num == 0;
		if (this.Holding[num] == null)
		{
			item.DecoupleFromParent();
			item.transform.parent = ((!flag) ? this.LeftHand : this.RightHand);
			item.transform.localPosition = item.OffsetTranslation;
			item.transform.localRotation = Quaternion.Euler(item.OffsetRotation);
			this.Holding[(!flag) ? 1 : 0] = item;
			item.GetComponent<Renderer>().enabled = this.LastVisible;
			this.UpdateChildren();
			return true;
		}
		return false;
	}

	public bool ReTakeItem(Holdable item, bool right)
	{
		if ((right && this.Holding[0] == null) || (!right && this.Holding[1] == null))
		{
			item.DecoupleFromParent();
			item.transform.parent = ((!right) ? this.LeftHand : this.RightHand);
			item.transform.localPosition = item.OffsetTranslation;
			item.transform.localRotation = Quaternion.Euler(item.OffsetRotation);
			this.Holding[(!right) ? 1 : 0] = item;
			item.GetComponent<Renderer>().enabled = this.LastVisible;
			this.UpdateChildren();
			return true;
		}
		return false;
	}

	public void InitiateTurn(float targetdir)
	{
		if (GameSettings.GameSpeed > 1f || Mathf.Approximately(base.transform.rotation.eulerAngles.y, targetdir))
		{
			base.transform.rotation = Quaternion.Euler(base.transform.rotation.eulerAngles.x, targetdir, base.transform.rotation.eulerAngles.z);
			return;
		}
		this.Turn = true;
		this.TargetRot = targetdir;
		this.LastAnim = this.anim.GetInteger("AnimControl");
		this.TurnAround();
	}

	public static void ConvertPath(List<Vector3> path)
	{
	}

	public Vector3 GetFuturePoint(float dist)
	{
		if (this.CurrentPath == null)
		{
			return base.transform.position;
		}
		float num = dist;
		float num2 = this.PathProg;
		int i = this.CurrentPathNode;
		int num3 = this.CurrentPathNode + 1;
		while (i < this.CurrentPath.Count - 1)
		{
			Vector3 vector = this.CurrentPath[i];
			if (num2 > 0f)
			{
				vector += (this.CurrentPath[num3] - vector) * num2;
			}
			float magnitude = (vector - this.CurrentPath[num3]).magnitude;
			if (magnitude >= num)
			{
				break;
			}
			i++;
			num3++;
			num2 = 0f;
			num -= magnitude;
		}
		if (i == this.CurrentPath.Count - 1)
		{
			return this.CurrentPath[i];
		}
		return this.CurrentPath[i] + (this.CurrentPath[num3] - this.CurrentPath[i]).normalized * num;
	}

	public bool WalkPath()
	{
		if (this.CurrentPath == null || this.CurrentPath.Count < 2 || (this.CurrentPath.Count == 2 && this.CurrentPath[0].Approximate(this.CurrentPath[1])))
		{
			this.CurrentPath = null;
			return true;
		}
		this.Noisiness = 2f;
		float f = 0f;
		if (this.CurrentPathNode < this.CurrentPath.Count - 1)
		{
			Vector3 to = this.CurrentPath[this.CurrentPathNode + 1] - this.CurrentPath[this.CurrentPathNode];
			if (!Mathf.Approximately(to.y, 0f))
			{
				f = Vector3.Angle(new Vector3(to.x, 0f, to.z), to);
			}
		}
		bool flag = Mathf.Abs(f) > 80f;
		if (flag)
		{
			this.anim.SetInteger("AnimControl", 0);
		}
		else
		{
			if (this.anim.GetInteger("AnimControl") != 1)
			{
				this.anim.SetInteger("AnimControl", 1);
				return false;
			}
			if (GameSettings.GameSpeed == 1f && !this.anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
			{
				return false;
			}
		}
		float num = this.WalkSpeed * Time.deltaTime * GameSettings.GameSpeed;
		Vector3 vector = Vector3.zero;
		Vector3 vector2 = Vector3.zero;
		while (num > 0f && this.CurrentPathNode < this.CurrentPath.Count - 1)
		{
			vector = this.CurrentPath[this.CurrentPathNode];
			vector2 = this.CurrentPath[this.CurrentPathNode + 1];
			float num2 = (vector - vector2).magnitude;
			float num3 = num2;
			num2 = ((num2 != 0f) ? num2 : 1f);
			float num4 = num / num2;
			this.PathProg += num4;
			if (this.PathProg <= 1f)
			{
				break;
			}
			num -= num3 * num4;
			this.PathProg = 0f;
			this.CurrentPathNode++;
		}
		if (this.AItype != AI<Actor>.AIType.Cleaning && UnityEngine.Random.value / GameSettings.GameSpeed < 0.05f)
		{
			Room currentRoom = this.currentRoom;
			if (currentRoom != null)
			{
				if (currentRoom == GameSettings.Instance.sRoomManager.Outside || (currentRoom.Outdoors && currentRoom.FloorMat.Equals("None")))
				{
					this.AvailableDirt = 0.5f;
				}
				else if (this.AvailableDirt > 0.01f)
				{
					float num5 = currentRoom.AddDirt(base.transform.position.x, base.transform.position.z, 0.05f, new Vector2?((vector2 - vector).FlattenVector3()));
					this.AvailableDirt -= num5;
				}
			}
		}
		f = 0f;
		if (this.CurrentPathNode < this.CurrentPath.Count - 1)
		{
			Vector3 to2 = this.CurrentPath[this.CurrentPathNode + 1] - this.CurrentPath[this.CurrentPathNode];
			if (!Mathf.Approximately(to2.y, 0f))
			{
				f = Vector3.Angle(new Vector3(to2.x, 0f, to2.z), to2);
			}
		}
		flag = (Mathf.Abs(f) > 80f);
		if (this.CurrentPathNode == this.CurrentPath.Count - 1)
		{
			base.transform.position = this.CurrentPath[this.CurrentPath.Count - 1];
			if (this.CurrentPath.Count > 1)
			{
				vector = this.CurrentPath[this.CurrentPath.Count - 2];
				vector2 = this.CurrentPath[this.CurrentPath.Count - 1];
				if (vector2 != vector)
				{
					base.transform.rotation = Quaternion.LookRotation(vector2 - vector);
				}
			}
			this.UpdateCurrentRoom(true);
			this.currentRoom.FixActorPosition(this, true);
			this.CurrentPath = null;
			return true;
		}
		vector = this.CurrentPath[this.CurrentPathNode];
		vector2 = this.CurrentPath[this.CurrentPathNode + 1];
		Vector3 a = vector2 - vector;
		base.transform.position = vector + a * this.PathProg;
		if (!flag && vector2 != vector)
		{
			Quaternion b = Quaternion.LookRotation(new Vector3(a.x, 0f, a.z));
			if (Utilities.AngleDistance(base.transform.rotation.eulerAngles.y, b.eulerAngles.y) > 90f)
			{
				this.InitiateTurn(b.eulerAngles.y);
			}
			else
			{
				base.transform.rotation = Quaternion.Lerp(base.transform.rotation, b, Time.deltaTime * 10f * GameSettings.GameSpeed);
			}
		}
		return false;
	}

	public void TurnToFurniture()
	{
		if (this.UsingPoint != null)
		{
			this.InitiateTurn(this.UsingPoint.Rotation);
		}
	}

	public override string Description()
	{
		return "Employees";
	}

	private void Fatigue()
	{
		if (!this.AIScript.currentNode.Name.Equals("Spawn") && !this.GoHomeNow)
		{
			int num = SDateTime.Now().ToInt() - this.MeetingTime.ToInt();
			int num2 = 600 - num;
			if (num2 < 0)
			{
				this.employee.AddMood("WornOut", this, Time.deltaTime, 2f, true, true);
			}
		}
	}

	public void InitPath()
	{
		this.TrappedInToilet = false;
		Actor.ConvertPath(this.CurrentPath);
		this.CurrentPathNode = 0;
		this.PathProg = 0f;
	}

	public bool CanToilet(bool toilet, Room r)
	{
		return !toilet || (r != null && r.Occupants.Count == 0 && r.IsPrivate);
	}

	private bool CheckFilter(Func<Furniture, bool> filter, Furniture furn)
	{
		return filter == null || filter(furn);
	}

	public List<KeyValuePair<InteractionPoint, int>> FindFurniture(string tname, string action, int maxDistance = -1, Room fromRoom = null, bool enforceAssigned = false, Func<Furniture, bool> filter = null)
	{
		bool toilet = "Toilet".Equals(tname);
		if (this.QueuedFor(tname))
		{
			InteractionPoint interactionPoint = this.InQueue[tname];
			if (!this.IsUp(tname) || !this.CanToilet(toilet, interactionPoint.Parent.Parent))
			{
				return new List<KeyValuePair<InteractionPoint, int>>();
			}
			if (interactionPoint.Usable())
			{
				return new List<KeyValuePair<InteractionPoint, int>>
				{
					new KeyValuePair<InteractionPoint, int>(interactionPoint, 1)
				};
			}
			interactionPoint.RemoveFromQueue(this);
			this.InQueue.Remove(tname);
		}
		List<KeyValuePair<InteractionPoint, int>> list = new List<KeyValuePair<InteractionPoint, int>>();
		if (this.Reserved != null && this.Reserved.Type.Equals(tname) && this.CheckFilter(filter, this.Reserved))
		{
			InteractionPoint interactionPoint2 = this.Reserved.GetInteractionPoint(this, action);
			if (interactionPoint2 != null)
			{
				list.Add(new KeyValuePair<InteractionPoint, int>(interactionPoint2, 1));
				return list;
			}
		}
		bool flag = false;
		foreach (Room room in this.GetAssignedRooms())
		{
			flag = true;
			if (!room.NavmeshRebuildStarted)
			{
				HashList<Furniture> furniture = room.GetFurniture(tname);
				int count = furniture.Count;
				for (int i = 0; i < count; i++)
				{
					Furniture furniture2 = furniture[i];
					if (this.CheckFilter(filter, furniture2))
					{
						InteractionPoint interactionPoint3 = furniture2.GetInteractionPoint(this, action);
						if (interactionPoint3 != null)
						{
							list.Add(new KeyValuePair<InteractionPoint, int>(interactionPoint3, 1));
						}
					}
				}
			}
		}
		if (flag && (list.Count > 0 || enforceAssigned))
		{
			return list;
		}
		Furniture furniture3 = null;
		if (this.ReservedFurniture.Count > 0)
		{
			foreach (Furniture furniture4 in this.ReservedFurniture)
			{
				if (furniture4.Type.Equals(tname) && this.CheckFilter(filter, furniture4))
				{
					furniture3 = furniture4;
					break;
				}
			}
		}
		if (furniture3 == null && this.Owns.Count > 0)
		{
			foreach (Furniture furniture5 in this.Owns)
			{
				if (furniture5.Type.Equals(tname) && this.CheckFilter(filter, furniture5))
				{
					furniture3 = furniture5;
					break;
				}
			}
		}
		if (furniture3 != null && this.CanToilet(toilet, furniture3.Parent) && furniture3.Parent != null && furniture3.Parent.AllowedInRoom(this))
		{
			InteractionPoint interactionPoint4 = furniture3.GetInteractionPoint(this, action) ?? furniture3.GetQueueableInteractionPoint(this, action);
			if (interactionPoint4 != null)
			{
				list.Add(new KeyValuePair<InteractionPoint, int>(interactionPoint4, 1));
				return list;
			}
		}
		this.UpdateCurrentRoom(false);
		List<KeyValuePair<Room, int>> connectedRooms = GameSettings.Instance.sRoomManager.GetConnectedRooms(fromRoom ?? this.currentRoom);
		InteractionPoint interactionPoint5 = null;
		float num = float.MaxValue;
		if (this.team != null)
		{
			for (int j = 0; j < connectedRooms.Count; j++)
			{
				Room key = connectedRooms[j].Key;
				if (!key.NavmeshRebuildStarted)
				{
					if ((maxDistance == -1 || connectedRooms[j].Value <= maxDistance) && key.Accessible && key.Teams.Contains(this.team))
					{
						if (key.AllowedInRoom(this))
						{
							if (this.CanToilet(toilet, key))
							{
								int num2 = this.RoomDist(connectedRooms[j].Value);
								HashList<Furniture> furniture6 = key.GetFurniture(tname);
								int count2 = furniture6.Count;
								for (int k = 0; k < count2; k++)
								{
									Furniture furniture7 = furniture6[k];
									if (this.CheckFilter(filter, furniture7))
									{
										InteractionPoint interactionPoint6 = furniture7.GetInteractionPoint(this, action);
										if (interactionPoint6 != null)
										{
											list.Add(new KeyValuePair<InteractionPoint, int>(interactionPoint6, num2));
										}
										else
										{
											interactionPoint6 = furniture7.GetQueueableInteractionPoint(this, action);
											if (interactionPoint6 != null)
											{
												float num3 = (interactionPoint6.transform.position - base.transform.position).sqrMagnitude * (float)num2;
												if (interactionPoint5 == null || num > num3)
												{
													num = num3;
													interactionPoint5 = interactionPoint6;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (list.Count == 0)
		{
			for (int l = 0; l < connectedRooms.Count; l++)
			{
				Room key2 = connectedRooms[l].Key;
				if (!key2.NavmeshRebuildStarted)
				{
					if ((maxDistance == -1 || connectedRooms[l].Value <= maxDistance) && (this.AItype != AI<Actor>.AIType.Employee || key2.Teams.Count <= 0) && key2.Accessible)
					{
						if (key2.AllowedInRoom(this))
						{
							if (this.CanToilet(toilet, key2))
							{
								int num4 = this.RoomDist(connectedRooms[l].Value);
								HashList<Furniture> furniture8 = key2.GetFurniture(tname);
								int count3 = furniture8.Count;
								for (int m = 0; m < count3; m++)
								{
									Furniture furniture9 = furniture8[m];
									if (this.CheckFilter(filter, furniture9))
									{
										InteractionPoint interactionPoint7 = furniture9.GetInteractionPoint(this, action);
										if (interactionPoint7 != null)
										{
											list.Add(new KeyValuePair<InteractionPoint, int>(interactionPoint7, num4));
										}
										else
										{
											interactionPoint7 = furniture9.GetQueueableInteractionPoint(this, action);
											if (interactionPoint7 != null)
											{
												float num5 = (interactionPoint7.transform.position - base.transform.position).sqrMagnitude * (float)num4;
												if (interactionPoint5 == null || num > num5)
												{
													num = num5;
													interactionPoint5 = interactionPoint7;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (interactionPoint5 != null)
		{
			bool flag2 = true;
			for (int n = 0; n < list.Count; n++)
			{
				KeyValuePair<InteractionPoint, int> keyValuePair = list[n];
				if ((keyValuePair.Key.transform.position - base.transform.position).sqrMagnitude * (float)keyValuePair.Value - 36f < num)
				{
					flag2 = false;
					break;
				}
			}
			if (flag2)
			{
				interactionPoint5.AddToQueue(this);
				this.InQueue[tname] = interactionPoint5;
				return new List<KeyValuePair<InteractionPoint, int>>();
			}
		}
		return list;
	}

	private int RoomDist(int dist)
	{
		int num = dist + 1;
		return num * num;
	}

	public Employee.RoleBit GetRole()
	{
		return (this.AItype != AI<Actor>.AIType.Employee) ? Employee.RoleBit.AllRoles : this.employee.CurrentRoleBit;
	}

	public bool PathToPoint(Vector3 e, bool anyRoom = false)
	{
		this.CurrentPath = GameSettings.Instance.sRoomManager.FindPath(base.transform.position, e, this.team, this.GetRole(), anyRoom || this.AItype != AI<Actor>.AIType.Employee);
		if (this.CurrentPath == null)
		{
			return false;
		}
		this.InitPath();
		return true;
	}

	public bool PathToFurniture(InteractionPoint furn, bool warning)
	{
		return furn != null && this.PathToFurniture(new InteractionPoint[]
		{
			furn
		}, warning);
	}

	public bool PathToFurniture(InteractionPoint[] furns, bool warning)
	{
		InteractionPoint interactionPoint = null;
		if (furns.Length <= 0)
		{
			return false;
		}
		bool flag = false;
		foreach (InteractionPoint interactionPoint2 in furns)
		{
			if (!(interactionPoint2 == null))
			{
				Vector2 point = interactionPoint2.Point;
				Vector3 endV = new Vector3(point.x, (float)(interactionPoint2.Parent.Parent.Floor * 2), point.y);
				this.CurrentPath = GameSettings.Instance.sRoomManager.FindPath(base.transform.position, endV, this.team, this.GetRole(), this.AItype != AI<Actor>.AIType.Employee || interactionPoint2.Parent.ForceAccessible || this.TrappedInToilet);
				if (this.CurrentPath != null)
				{
					interactionPoint2.Parent.PathFailCount = 0;
					HUD.Instance.UnreachableFuniture.Remove(interactionPoint2.Parent);
					flag = true;
					interactionPoint = interactionPoint2;
					break;
				}
				interactionPoint2.Parent.PathFailCount++;
				if (warning)
				{
					HUD.Instance.UnreachableFuniture.Add(interactionPoint2.Parent);
				}
			}
		}
		if (!flag)
		{
			return false;
		}
		if (this.UsingPoint != null)
		{
			this.UsingPoint.UsedBy = null;
		}
		interactionPoint.UsedBy = this;
		this.UsingPoint = interactionPoint;
		this.InitPath();
		if (interactionPoint.Parent.Type.Equals("Toilet"))
		{
			this.TrappedInToilet = true;
		}
		return true;
	}

	public int GoToFurniture(string name, string action, int maxDistance, bool warning, Room fromRoom = null, bool loiter = false, bool enforceAssigned = false, Func<Furniture, bool> filter = null)
	{
		if (this.UsingPoint != null && this.UsingPoint.Parent.Broken())
		{
			this.UsingPoint.UsedBy = null;
			this.UsingPoint = null;
		}
		if (this.AtFurniture && this.UsingPoint != null && this.UsingPoint.Parent.Type.Equals(name) && this.UsingPoint.Name.Equals(action) && (this.UsingPoint.Parent.OwnedBy == null || this.UsingPoint.Parent.OwnedBy == this) && (base.transform.position.FlattenVector3() - this.UsingPoint.transform.position.FlattenVector3()).sqrMagnitude < 0.01f)
		{
			return 2;
		}
		this.AtFurniture = false;
		if (this.UsingPoint == null || !this.UsingPoint.Parent.Type.Equals(name) || !this.UsingPoint.Name.Equals(action))
		{
			InteractionPoint usingPoint = this.UsingPoint;
			int role = (int)((!loiter) ? this.GetRole() : (~Employee.RoleBit.Lead));
			List<KeyValuePair<InteractionPoint, int>> list = (from x in this.FindFurniture(name, action, maxDistance, fromRoom, enforceAssigned, filter)
			orderby (float)x.Value * x.Key.transform.position.ManhattanDist(base.transform.position)
			select x).ToList<KeyValuePair<InteractionPoint, int>>();
			InteractionPoint[] array = new InteractionPoint[list.Count];
			int num = 0;
			int num2 = list.Count - 1;
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i].Key.Parent.Parent.OrderByRole(role) == 0)
				{
					array[num] = list[i].Key;
					num++;
				}
				else
				{
					array[num2] = list[i].Key;
					num2--;
				}
			}
			array.ReverseListPart(num, array.Length);
			if (this.PathToFurniture(array, warning))
			{
				if (usingPoint != null)
				{
					usingPoint.UsedBy = null;
				}
				return 1;
			}
			for (int j = 0; j < array.Length; j++)
			{
				if (!(array[j] == null))
				{
					if (array[j].Parent.Reserved == this)
					{
						array[j].Parent.Reserved = null;
					}
				}
			}
			if (usingPoint != null)
			{
				this.AtFurniture = true;
				this.UsingPoint = usingPoint;
			}
			return 0;
		}
		else
		{
			if (this.CurrentPath == null)
			{
				return 0;
			}
			bool flag = this.WalkPath();
			if (flag)
			{
				this.AtFurniture = true;
			}
			return (!flag) ? 1 : 2;
		}
	}

	public int WaitForTimer(float time)
	{
		if (this.Timer == -1f)
		{
			this.Timer = time;
		}
		this.Timer -= Time.deltaTime * GameSettings.GameSpeed;
		if (this.Timer < 0f)
		{
			this.Timer = -1f;
			return 2;
		}
		return 1;
	}

	public void MeetingBoost()
	{
		Team team = this.GetTeam();
		if (team != null && team.Leader != null)
		{
			Employee employee = team.Leader.employee;
			float num = employee.GetSkill(Employee.EmployeeRole.Lead);
			num *= employee.Leadership.MapRange(-1f, 1f, 0.5f, 2f, false);
			this.employee.AddMood("MeetingGreat", this, Time.deltaTime, num, true, true);
			if (this.employee.IsRole(Employee.RoleBit.Lead))
			{
				this.employee.ChangeSkill(Employee.EmployeeRole.Lead, 0.04f * this.currentRoom.GetAuraValue(Furniture.AuraTypes.Skill), true);
			}
			this.employee.ChangeSkill((Employee.EmployeeRole)UnityEngine.Random.Range(1, Employee.RoleCount), num * 0.2f * this.currentRoom.GetAuraValue(Furniture.AuraTypes.Skill), true);
		}
	}

	public WorkItem MyWorkItem()
	{
		int num = this.AutoDevs.Count;
		int num2 = 0;
		int num3 = this.CurrentWorkItem;
		if (this.Team != null)
		{
			num2 = this.GetTeam().WorkItems.Count;
			num += num2;
			if (num > 0)
			{
				num3 %= num;
			}
			if (num2 > 0 && num3 < num2)
			{
				return this.GetTeam().WorkItems[num3];
			}
		}
		else if (num > 0)
		{
			num3 %= num;
		}
		if (this.AutoDevs.Count > 0)
		{
			return this.AutoDevs[num3 - num2];
		}
		return null;
	}

	public int GetWorkCount()
	{
		int num = this.AutoDevs.Count;
		if (this.Team != null)
		{
			num += this.GetTeam().WorkItems.Count;
		}
		return num;
	}

	public void WorkBoost()
	{
		WorkItem workItem = this.MyWorkItem();
		if (workItem != null)
		{
			Employee.EmployeeRole? boostRole = workItem.GetBoostRole(this);
			if (boostRole != null)
			{
				float skill = this.employee.GetSkill(boostRole.Value);
				this.employee.AddMood("LoveWork", this, Time.deltaTime, skill, true, true);
				if (skill < 1f)
				{
					float num = (boostRole.Value == Employee.EmployeeRole.Lead || !this.employee.IsRole(Employee.RoleBit.Lead)) ? 1f : 0.25f;
					this.employee.ChangeSkill(boostRole.Value, Mathf.Max(0f, 0.01f * workItem.GetWorkBoost(boostRole.Value, skill) * num * this.currentRoom.GetAuraValue(Furniture.AuraTypes.Skill)), true);
				}
			}
		}
	}

	public TableScript FindFreeTable(Room.RoomLimits limit, bool largerIsBetter, bool emptyTables, bool preferTeam)
	{
		List<KeyValuePair<Room, int>> connectedRooms = GameSettings.Instance.sRoomManager.GetConnectedRooms(this.currentRoom);
		float num = (!largerIsBetter) ? float.MaxValue : 0f;
		float num2 = (!largerIsBetter) ? float.MaxValue : 0f;
		int num3 = (this.team == null) ? 1 : this.team.Count;
		TableScript tableScript = null;
		HashSet<int> hashSet = Actor.ValidLimits[limit];
		TableScript tableScript2 = null;
		if (preferTeam)
		{
			for (int i = 0; i < connectedRooms.Count; i++)
			{
				KeyValuePair<Room, int> keyValuePair = connectedRooms[i];
				if (keyValuePair.Key.Teams.Contains(this.team) && hashSet.Contains(keyValuePair.Key.ForceRole))
				{
					for (int j = 0; j < keyValuePair.Key.TableParents.Count; j++)
					{
						TableScript tableScript3 = keyValuePair.Key.TableParents[j];
						if (tableScript3.TableReserved == -1)
						{
							int num4 = tableScript3.CountFreeChairs(emptyTables, true);
							float num5 = (float)num4 / Mathf.Sqrt((float)(keyValuePair.Value + 1));
							if (keyValuePair.Key.ForceRole != (int)limit)
							{
								if (num5 > num2)
								{
									num2 = num5;
									tableScript2 = tableScript3;
								}
							}
							else if (num5 > num)
							{
								num = num5;
								tableScript = tableScript3;
								if (num4 >= num3)
								{
									return tableScript;
								}
							}
						}
					}
				}
			}
			if (tableScript != null)
			{
				return tableScript;
			}
			if (tableScript2 != null)
			{
				return tableScript2;
			}
		}
		for (int k = 0; k < connectedRooms.Count; k++)
		{
			KeyValuePair<Room, int> keyValuePair2 = connectedRooms[k];
			if ((keyValuePair2.Key.IsNeutral() || keyValuePair2.Key.CompatibleWithTeam(this.team)) && hashSet.Contains(keyValuePair2.Key.ForceRole))
			{
				for (int l = 0; l < keyValuePair2.Key.TableParents.Count; l++)
				{
					TableScript tableScript4 = keyValuePair2.Key.TableParents[l];
					if ((preferTeam && tableScript4.TableReserved == -1) || (!preferTeam && tableScript4.TableReserved < 1))
					{
						if (tableScript4.CountFreeChairs(emptyTables, false) != 0)
						{
							float num6 = (float)tableScript4.CountFreeChairs(emptyTables, true);
							if (!largerIsBetter)
							{
								if (keyValuePair2.Key.ForceRole != (int)limit)
								{
									if (num6 < num2 * Mathf.Sqrt((float)(keyValuePair2.Value + 1)))
									{
										num2 = num6;
										tableScript2 = tableScript4;
									}
								}
								else if (num6 < num * Mathf.Sqrt((float)(keyValuePair2.Value + 1)))
								{
									num = num6;
									tableScript = tableScript4;
								}
							}
							else
							{
								float num7 = num6;
								num6 /= Mathf.Sqrt((float)(keyValuePair2.Value + 1));
								if (keyValuePair2.Key.ForceRole != (int)limit)
								{
									if (num6 > num2)
									{
										num2 = num6;
										tableScript2 = tableScript4;
									}
								}
								else if (num6 > num)
								{
									num = num6;
									tableScript = tableScript4;
									if (preferTeam && num7 >= (float)num3)
									{
										return tableScript;
									}
								}
							}
						}
					}
				}
			}
		}
		return tableScript ?? tableScript2;
	}

	public List<Actor> CallForMeeting()
	{
		Team team = this.GetTeam();
		int count = team.MeetingTable.CountFreeChairs(false, false);
		List<Actor> list = (from x in team.GetEmployees()
		where x.isActiveAndEnabled
		orderby x.LastMeeting.ToInt()
		select x).ToList<Actor>();
		list.Remove(this);
		return list.Take(count).ToList<Actor>();
	}

	public void ChangeRole(Employee.EmployeeRole role, bool active)
	{
		int num = (int)Employee.RoleToBit[(int)role];
		if (!active)
		{
			num = (~num & 31);
			this.ChangeRole((Employee.RoleBit)(num & (int)this.employee.CurrentRoleBit));
		}
		else
		{
			this.ChangeRole((Employee.RoleBit)(num | (int)this.employee.CurrentRoleBit));
		}
	}

	public void ChangeRole(Employee.RoleBit roles)
	{
		if (!this.employee.IsRole(Employee.RoleBit.Lead) || (roles & Employee.RoleBit.Lead) != Employee.RoleBit.None || (!this.AIScript.currentNode.Name.Equals("HaveMeeting") && !this.AIScript.currentNode.Name.Equals("GoToMeeting") && !this.AIScript.currentNode.Name.Equals("HasMakeMeeting")))
		{
			Team team = this.GetTeam();
			if ((roles & Employee.RoleBit.Lead) > Employee.RoleBit.None)
			{
				if (team != null)
				{
					if (team.Leader != null && team.Leader != this)
					{
						team.Leader.ChangeRole(Employee.EmployeeRole.Lead, false);
					}
					team.Leader = this;
					this.employee.SetRoles(roles);
				}
			}
			else
			{
				if (this.employee.IsRole(Employee.RoleBit.Lead))
				{
					this.KillAutoDev();
					if (team != null)
					{
						team.Leader = null;
					}
				}
				this.employee.SetRoles(roles);
			}
			this.NegotiateSalary = (this.employee.Salary < this.employee.Worth(-1, true) && !this.WorksForFree() && !this.employee.Fired);
			if (team != null)
			{
				team.CalculateCompatibility();
				team.CheckWorkRoleAssignment();
			}
		}
	}

	public void ShutdownPC()
	{
		if (this.UsingPoint != null && this.UsingPoint.Parent.Type.Equals("Computer"))
		{
			this.UsingPoint.Parent.IsOn = false;
		}
	}

	public float GetPCAddonBonus(Employee.EmployeeRole role)
	{
		return this.UsingPoint.Parent.GetEffectivenessValue(role);
	}

	public bool IsCrunching()
	{
		return !this.employee.Founder && this.team != null && this.GetTeam().CrunchMode;
	}

	private float GetCrunchBuff(float crunchHours)
	{
		if (crunchHours < 7f)
		{
			return 0.5f + 0.5f * (1f - crunchHours / 7f);
		}
		if (crunchHours < 21f)
		{
			return 0.1f + 0.4f * (1f - (crunchHours - 7f) / 14f);
		}
		return 0.1f;
	}

	public void DoWork(float delta)
	{
		if (this.AIScript.currentNode.SpecialID != 5)
		{
			return;
		}
		WorkItem workItem = this.MyWorkItem();
		if (workItem != null && this.Effectiveness > 0f)
		{
			float num = 1f;
			WorkItem.HasWorkReturn hasWorkReturn = (!workItem.Paused) ? workItem.HasWork(this) : WorkItem.HasWorkReturn.Ignore;
			if (hasWorkReturn == WorkItem.HasWorkReturn.True)
			{
				bool flag = this.IsCrunching();
				num = workItem.StressMultiplier();
				if (flag)
				{
					this.employee.AddMood("CrunchTimeProb", this, delta, num * 0.333f, true, true);
					this.CrunchHangover += Utilities.PerHour(6f * num, delta, true) / (float)GameSettings.DaysPerMonth;
					num *= 2f;
				}
				if (this.WorkCyclesLeft == 1)
				{
					this.LastWorkItems[this.LastWorkCounter] = workItem.ID;
					this.LastWorkCounter = (this.LastWorkCounter + 1) % this.LastWorkItems.Length;
				}
				float num2 = (this.employee.Autodidactic >= 0f) ? 1f : this.employee.Autodidactic.MapRange(-1f, 1f, 1.1f, 1f, false);
				this.IsWorking = true;
				this.MakeUnIdle();
				workItem.DoWork(this, this.Effectiveness * num2 * ((!flag) ? 1f : 3f), delta);
				workItem.AddCost(GameSettings.GameSpeed * this.CostPerMinute * delta);
				this.EmitType = workItem.EmitType(this);
			}
			else
			{
				this.WorkCyclesLeft = 0;
				this.IsWorking = false;
			}
			this.UpdateStateInfluence = true;
			this.WorkCyclesLeft--;
			if (this.WorkCyclesLeft <= 0)
			{
				if (this.GetWorkCount() == 0)
				{
					hasWorkReturn = WorkItem.HasWorkReturn.Ignore;
					this.CurrentWorkItem = 0;
				}
				else
				{
					int workCount = this.GetWorkCount();
					bool flag2 = true;
					int num3 = 0;
					while (num3 <= workCount && flag2)
					{
						this.CurrentWorkItem = (this.CurrentWorkItem + 1) % workCount;
						flag2 = (this.MyWorkItem() == null);
						if (!flag2)
						{
							WorkItem.HasWorkReturn hasWorkReturn2 = (!this.MyWorkItem().Paused) ? this.MyWorkItem().HasWork(this) : WorkItem.HasWorkReturn.Ignore;
							if (hasWorkReturn2 == WorkItem.HasWorkReturn.True)
							{
								hasWorkReturn = WorkItem.HasWorkReturn.True;
								flag2 = false;
							}
							else
							{
								if (hasWorkReturn2 > hasWorkReturn)
								{
									hasWorkReturn = hasWorkReturn2;
								}
								flag2 = true;
							}
						}
						num3++;
					}
				}
				WorkItem workItem2 = this.MyWorkItem();
				if (workItem2 != null)
				{
					this.WorkCyclesLeft = workItem2.Priority;
				}
				else
				{
					this.WorkCyclesLeft = 1;
				}
			}
			switch (hasWorkReturn)
			{
			case WorkItem.HasWorkReturn.True:
			case WorkItem.HasWorkReturn.Waiting:
				this.MakeUnIdle();
				goto IL_2DA;
			case WorkItem.HasWorkReturn.Finished:
				this.MakeIdle(Actor.WorkStatus.NoActiveWork);
				goto IL_2DA;
			case WorkItem.HasWorkReturn.NotApplicable:
				this.MakeIdle(Actor.WorkStatus.NotApplicable);
				goto IL_2DA;
			}
			this.MakeIdle(Actor.WorkStatus.NoWork);
			IL_2DA:
			this.StressFactor = -1f;
			if (this.IsWorking)
			{
				this.StressFactor = (float)this.JobDiffCount();
				if (this.StressFactor > 2f)
				{
					this.StressFactor = Mathf.Log(this.StressFactor, 2f);
				}
				this.StressFactor *= num;
			}
		}
		else
		{
			if (workItem == null)
			{
				this.MakeIdle(Actor.WorkStatus.NoWork);
			}
			else if (this.Effectiveness <= 0f)
			{
				this.MakeIdle(Actor.WorkStatus.NoEffectiveness);
			}
			this.IsWorking = false;
			this.UpdateStateInfluence = true;
		}
	}

	private int JobDiffCount()
	{
		int num = 0;
		for (int i = 0; i < this.LastWorkItems.Length; i++)
		{
			num++;
			for (int j = 0; j < i; j++)
			{
				if (this.LastWorkItems[j] == this.LastWorkItems[i])
				{
					num--;
					break;
				}
			}
		}
		return num;
	}

	public void FreeLoiterTable()
	{
		if (this.LoiterTable != null)
		{
			if (this.LoiterTable.TableReserved == 0 && this.LoiterTable.IsOnlyUser(this))
			{
				this.LoiterTable.ReserveTables(false, false);
			}
			this.LoiterTable = null;
			if (this.UsingPoint != null && this.UsingPoint.Parent.Type.Equals("Chair"))
			{
				this.AtFurniture = false;
			}
		}
	}

	public float Affect(Actor.Affector cat, float value, float lowerCutoff = 1f, bool force = false)
	{
		value = ((value <= lowerCutoff || value >= 1f) ? value : 1f);
		if (!force && this.employee.Founder && value <= 1f)
		{
			return 1f;
		}
		this.Affactors[(int)cat] = value - 1f;
		return value;
	}

	public void NoAffect(Actor.Affector cat, bool remove = false)
	{
		if (remove)
		{
			this.Affactors[(int)cat] = -2f;
		}
		else
		{
			this.Affactors[(int)cat] = 0f;
		}
	}

	public float GetStateInfluence(float delta)
	{
		if (this.employee.Founder)
		{
			SDateTime d = SDateTime.Now() - this.MeetingTime;
			int num = (new SDateTime(0, 10, 0, 0, 0) - d).ToInt();
			if (num < 0)
			{
				this.Affect(Actor.Affector.Slavery, 0f, 0f, true);
				return 0f;
			}
			this.NoAffect(Actor.Affector.Slavery, true);
		}
		float num2 = 1f;
		bool flag = this.IsWorking && this.AIScript.currentNode != null && this.AIScript.currentNode.SpecialID == 5;
		if (!this.employee.Founder)
		{
			num2 *= this.Affect(Actor.Affector.Hunger, 1f - Mathf.Pow(1f - this.employee.Hunger, 3f), 0.25f, false);
			num2 *= this.Affect(Actor.Affector.Energy, 1f - Mathf.Pow(1f - this.employee.Energy, 3f), 0.25f, false);
			num2 *= this.Affect(Actor.Affector.Bladder, 1f - Mathf.Pow(1f - this.employee.Bladder, 5f), 0.25f, false);
			num2 *= this.Affect(Actor.Affector.JobSatisfaction, Utilities.PosNeg(1f - Mathf.Pow(1f - this.employee.JobSatisfaction, 3f), 0.1f, 1.5f), 1f, false);
			if (this.employee.Stress < 0.5f)
			{
				num2 *= this.Affect(Actor.Affector.Stress, this.employee.Stress * 2f, 1f, false);
			}
			else
			{
				this.NoAffect(Actor.Affector.Stress, false);
			}
			if (this.employee.Social < 0.5f)
			{
				num2 *= this.Affect(Actor.Affector.Social, 0.25f + this.employee.Social * 1.5f, 1f, false);
			}
			else
			{
				this.NoAffect(Actor.Affector.Social, false);
			}
			if (!this.IsCrunching() && this.CrunchHangover > 0f)
			{
				num2 *= this.Affect(Actor.Affector.CrunchHangover, this.GetCrunchBuff(this.CrunchHangover), 1f, false);
			}
			else
			{
				this.NoAffect(Actor.Affector.CrunchHangover, true);
			}
			if (this.employee.Fired)
			{
				num2 *= this.Affect(Actor.Affector.Fired, 0.5f, 1f, false);
			}
			if (this.currentRoom != GameSettings.Instance.sRoomManager.Outside && flag)
			{
				float auraValue = this.currentRoom.GetAuraValue(Furniture.AuraTypes.Effectiveness);
				num2 *= this.Affect(Actor.Affector.RoomAura, auraValue, 1f, false);
				float num3 = Mathf.Pow(1f - Mathf.Abs(21f - this.currentRoom.Temperature) / 24f, 2f);
				bool flag2 = this.currentRoom.Temperature < 21f;
				num2 *= this.Affect(Actor.Affector.Temperature, num3, 0.5f, false);
				if ((double)num3 < 0.5)
				{
					this.employee.AddMood((!flag2) ? "IsBurning" : "IsFreezing", this, delta, 0.5f - num3, true, true);
					if (num3 < 0.3f && flag)
					{
						HUD.Instance.AddPopupMessage((!flag2) ? "BurningWarning".Loc() : "FreezingWarning".Loc(), "Exclamation", PopupManager.PopUpAction.GotoEmp, this.DID, PopupManager.NotificationSound.Issue, 0.25f, PopupManager.PopupIDs.EmployeeCountProblem, 1);
					}
				}
			}
			else
			{
				this.NoAffect(Actor.Affector.Temperature, false);
			}
		}
		if (this.Team != null)
		{
			num2 *= this.Affect(Actor.Affector.TeamCompatibility, Utilities.PosNeg(this.TeamCompatibility, 0.25f, 1.12f), 1f, false);
		}
		if (this.UsingPoint != null && flag)
		{
			float environment = this.UsingPoint.Parent.Parent.GetEnvironment();
			num2 *= this.Affect(Actor.Affector.Environment, Utilities.PosNeg(environment, 0.5f, 1.12f), 0.75f, false);
			float comfort = this.UsingPoint.Parent.GetComfort();
			num2 *= this.Affect(Actor.Affector.Comfort, Utilities.PosNeg(comfort, 0.5f, 1.1f), 0.75f, false);
			if (!this.employee.Founder)
			{
				float num4 = 1f - Mathf.Pow(this.UsingPoint.Parent.Parent.DarknessLevel, 3f);
				if ((double)num4 < 0.5)
				{
					this.employee.AddMood("NoSee", this, delta, 0.5f - num4, true, true);
					if (num4 < 0.2f && flag)
					{
						HUD.Instance.AddPopupMessage("NoSeeWarning".Loc(), "Exclamation", PopupManager.PopUpAction.GotoEmp, this.DID, PopupManager.NotificationSound.Issue, 0.25f, PopupManager.PopupIDs.EmployeeCountProblem, 1);
					}
				}
				num2 *= this.Affect(Actor.Affector.Lighting, 0.25f + 0.75f * num4, 0.7f, false);
				if (environment < 0.5f)
				{
					this.employee.AddMood((this.UsingPoint.Parent.Parent.FurnEnvironment >= this.UsingPoint.Parent.Parent.DirtScore) ? "RoomDirty" : "RoomLowEnv", this, delta, 1f - environment, true, true);
				}
				else if (environment > 1f)
				{
					this.employee.AddMood("RoomNotDirty", this, delta, environment, true, true);
				}
				this.employee.AddMood("RoomAuraBoost", this, delta, this.currentRoom.GetAuraValue(Furniture.AuraTypes.Mood), true, true);
				if (comfort < 0.75f)
				{
					this.employee.AddMood("UncomfortableFurniture", this, delta, 1f - comfort, true, true);
				}
				if (this.UsingPoint.Parent.HasUpg)
				{
					Upgradable upg = this.UsingPoint.Parent.upg;
					if (this.UsingPoint.Parent.Type.Equals("Computer"))
					{
						float num5 = this.UsingPoint.Parent.FinalNoise;
						num5 *= num5;
						num2 *= this.Affect(Actor.Affector.Noise, 0.1f + (1f - num5) * 0.9f, 1f, false);
						if (num5 > 0.25f && flag)
						{
							this.employee.AddMood("NoiseComplaint", this, delta, num5, true, true);
							if (num5 > 0.5f)
							{
								HUD.Instance.AddPopupMessage("NoiseWarning".Loc(), "Exclamation", PopupManager.PopUpAction.GotoEmp, this.DID, PopupManager.NotificationSound.Issue, 0.3f, PopupManager.PopupIDs.EmployeeCountProblem, 1);
							}
						}
						if (num5 == 0f)
						{
							this.employee.AddMood("NoNoiseGood", this, delta, 1f, true, true);
						}
						if (upg.Quality < 0.25f)
						{
							this.employee.AddMood("ComputerBad", this, delta, 1f - upg.Quality * 4f, true, true);
							num2 *= this.Affect(Actor.Affector.Computer, 0.75f + upg.Quality, 1f, false);
						}
						else
						{
							this.NoAffect(Actor.Affector.Computer, false);
						}
					}
				}
				else
				{
					this.NoAffect(Actor.Affector.Computer, false);
				}
			}
		}
		else
		{
			this.NoAffect(Actor.Affector.Lighting, false);
			this.NoAffect(Actor.Affector.Environment, false);
			this.NoAffect(Actor.Affector.Comfort, false);
			this.NoAffect(Actor.Affector.Computer, false);
			this.NoAffect(Actor.Affector.Noise, false);
		}
		if (!this.employee.Founder)
		{
			if (this.currentRoom.Floor == -1)
			{
				num2 *= this.Affect(Actor.Affector.Basement, 0.8f, 1f, false);
				this.employee.AddMood("BasementComplaint", this, delta, 1f, true, true);
			}
			else
			{
				this.NoAffect(Actor.Affector.Basement, true);
			}
			if (this.UsingPoint != null && this.AnyNonTeamWorking())
			{
				num2 *= this.Affect(Actor.Affector.OtherTeams, 0.5f, 1f, false);
				this.employee.AddMood("OtherTeamComplaint", this, delta, 1f, true, true);
			}
			else
			{
				this.NoAffect(Actor.Affector.OtherTeams, false);
			}
		}
		if (this.employee.IsRole(Employee.RoleBit.Lead) && this.currentRoom.Occupants.Count == 1)
		{
			num2 *= this.Affect(Actor.Affector.OwnOffice, 1.5f, 1f, false);
		}
		else
		{
			this.NoAffect(Actor.Affector.OwnOffice, true);
		}
		return num2;
	}

	private bool AnyNonTeamWorking()
	{
		List<Actor> occupants = this.currentRoom.Occupants;
		for (int i = 0; i < occupants.Count; i++)
		{
			Actor actor = occupants[i];
			if (actor != this && actor.IsWorking && actor.enabled && actor.Team != null && !actor.Team.Equals(this.Team))
			{
				return true;
			}
		}
		return false;
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		this.AItype = (AI<Actor>.AIType)dictionary.Get<int>("AIType", 0);
		Furniture furniture = (Furniture)base.GetDeserializedObject(dictionary.Get<uint>("UsingFurniture", 0u));
		if (furniture != null)
		{
			InteractionPoint interactionPoint = furniture.GetInteractionPoint("Use", false);
			if (interactionPoint != null)
			{
				interactionPoint.UsedBy = this;
				this.UsingPoint = interactionPoint;
			}
		}
		Furniture furniture2 = (Furniture)base.GetDeserializedObject(dictionary.Get<uint>("UsingFurniture2", 0u));
		if (furniture2 != null)
		{
			int newP = dictionary.Get<int>("UsingPoint", 0);
			InteractionPoint interactionPoint2 = furniture2.InteractionPoints.FirstOrDefault((InteractionPoint x) => x.Id == newP);
			if (interactionPoint2 != null)
			{
				interactionPoint2.UsedBy = this;
				this.UsingPoint = interactionPoint2;
			}
		}
		this.TeamRelationNum = dictionary.Get<int>("TeamRelationNum", 0);
		this.TeamRelationTimer = (float)dictionary.Get<int>("TeamRelationTimer", 0);
		this.Effectiveness = (float)dictionary.Get<int>("Effectiveness", 1);
		this.SpecialState = dictionary.Get<Actor.HomeState>("SpecialState", Actor.HomeState.Default);
		this.Boxes = dictionary.Get<int>("Boxes", 0);
		this.CostPerMinute = dictionary.Get<float>("CostPerMinute", 0f);
		this._cachedBenefits = dictionary.Get<Dictionary<string, float>>("CachedBenefits", this._cachedBenefits);
		this._cachedBenefitValue = dictionary.Get<float>("CachedBenefitValue", -1f);
		this.ChristmasBonus = dictionary.Get<float>("ChristmasBonus", 0f);
		uint[] array = dictionary.Get<uint[]>("Reservations", new uint[0]);
		foreach (uint id in array)
		{
			Furniture furniture3 = base.GetDeserializedObject(id) as Furniture;
			if (furniture3 != null)
			{
				furniture3.Reserved = this;
			}
		}
		if (dictionary.Contains("VacationMonthNew"))
		{
			this.VacationMonth = dictionary.Get<SDateTime>("VacationMonthNew", SDateTime.NextMonth(6));
		}
		else
		{
			int month = dictionary.Get<int>("VacationMonth", 6);
			this.VacationMonth = SDateTime.NextMonth(month);
		}
		this.AlternateVacation = dictionary.Get<SDateTime>("AlternateVacation", this.VacationMonth);
		SVector3[] array3 = dictionary.Get<SVector3[]>("CurrentPath", null);
		List<Vector3> currentPath;
		if (array3 == null)
		{
			currentPath = null;
		}
		else
		{
			currentPath = (from x in array3
			select x.ToVector3()).ToList<Vector3>();
		}
		this.CurrentPath = currentPath;
		this.LastMeeting = dictionary.Get<SDateTime>("LastMeeting", new SDateTime(SDateTime.BaseYear));
		this.MeetingTime = dictionary.Get<SDateTime>("MeetingTime2", SDateTime.Now());
		this.PathProg = dictionary.Get<float>("PathProg", 0f);
		this.PathProg = ((!float.IsNaN(this.PathProg)) ? this.PathProg : 0f);
		this.CurrentPathNode = dictionary.Get<int>("CurrentPathNode", 0);
		this.Timer = dictionary.Get<float>("Timer", this.Timer);
		this.employee = dictionary.Get<Employee>("employee", null);
		this.LastSocial = dictionary.Get<SDateTime>("LastSocial", this.employee.Hired);
		this.Female = (this.employee == null || this.employee.Female);
		uint[] source = dictionary.Get<uint[]>("Owns", new uint[0]);
		this.Owns = (from x in source
		select (Furniture)base.GetDeserializedObject(x) into x
		where x != null
		select x).ToHashSet<Furniture>();
		this.Owns.ToList<Furniture>().ForEach(delegate(Furniture x)
		{
			x.SetOwnedByDeserializing(this);
		});
		base.transform.position = dictionary.Get<SVector3>("position", Vector3.zero).ToVector3();
		base.transform.rotation = dictionary.Get<SVector3>("rotation", Quaternion.identity).ToQuaternion();
		this.Turn = dictionary.Get<bool>("Turn", false);
		this.TargetRot = dictionary.Get<float>("TargetRot", 0f);
		this.LastAnim = dictionary.Get<int>("LastAnim", 0);
		this.anim.SetInteger("AnimControl", dictionary.Get<int>("animation", 0));
		this.IdleStatus = (Actor.WorkStatus)dictionary.Get<int>("IdleStatus", 0);
		this.IsIdle = dictionary.Get<bool>("IsIdle", false);
		if (this.IsIdle)
		{
			HUD.Instance.AddToIdle(this);
		}
		if (dictionary.Contains("AnimState"))
		{
			int num = (int)dictionary["AnimState"];
			if (this.anim.HasState(0, num))
			{
				float normalizedTime = dictionary.Get<float>("AnimTime", 0f);
				this.anim.Play(num, 0, normalizedTime);
			}
		}
		this.AvailableDirt = dictionary.Get<float>("AvailableDirt", 0f);
		this.Despawned = dictionary.Get<bool>("Despawned", true);
		this.Reserved = (Furniture)base.GetDeserializedObject(dictionary.Get<uint>("ReservedFurniture", 0u));
		string[] array4 = dictionary.Get<string[]>("Holding", new string[2]);
		if (array4[0] != null)
		{
			this.GetItem(array4[0], true);
		}
		if (array4[1] != null)
		{
			this.GetItem(array4[1], false);
		}
		if (dictionary.Get<bool>("Coffee", false))
		{
			if (this.Holding[0] != null && this.Holding[0].GetComponent<CoffeeScript>() != null)
			{
				this.coffee = this.Holding[0].GetComponent<CoffeeScript>();
			}
			else if (this.Holding[1] != null && this.Holding[1].GetComponent<CoffeeScript>() != null)
			{
				this.coffee = this.Holding[1].GetComponent<CoffeeScript>();
			}
		}
		object deserializedObject = base.GetDeserializedObject(dictionary.Get<uint>("LoiterTable", 0u));
		if (deserializedObject != null)
		{
			this.LoiterTable = ((Furniture)deserializedObject).GetComponent<TableScript>();
		}
		this.CleaningRoom = (base.GetDeserializedObject(dictionary.Get<uint>("CleaningRoom", 0u)) as Room);
		this.CourseRole = (Employee.EmployeeRole)dictionary.Get<int>("CourseRole", 0);
		this.CoursePoints = dictionary.Get<float>("CoursePoints", 0f);
		this.CourseSpec = dictionary.Get<string>("CourseSpec", null);
		this.LastCourse = dictionary.Get<SDateTime>("LastCourse", this.employee.Hired);
		this.CleaningPoints = new Stack<Vector3>(from x in dictionary.Get<SVector3[]>("CleaningPoints", new SVector3[0])
		select x.ToVector3());
		this.StaffOn = dictionary.Get<int>("StaffOn", 8);
		this.StaffOff = dictionary.Get<int>("StaffOff", 16);
		this.OnCall = dictionary.Get<bool>("OnCall", false);
		this.CurrentWorkItem = dictionary.Get<int>("CurrentWorkItem", 0);
		this.WorkCyclesLeft = dictionary.Get<int>("WorkCyclesLeft", 1);
		this.SkinColor = dictionary.Get<SVector3>("SkinColor", new SVector3(1f, 1f, 1f, 1f)).ToColor();
		this.HairColor = dictionary.Get<SVector3>("HairColor", new SVector3(1f, 0f, 0f, 1f)).ToColor();
		this._savedStyle = ((!dictionary.Contains("NewStyle")) ? ActorStyler.Instance.GenerateStyle(this.Female, "Default") : dictionary.Get<ActorBodyItem.BodyItemObject[]>("NewStyle", new ActorBodyItem.BodyItemObject[0]));
		this.TrappedInToilet = dictionary.Get<bool>("TrappedInToilet", false);
		this.GoHomeNow = dictionary.Get<bool>("GoHomeNow", false);
		this.Init();
		if (dictionary.Contains("currentNode"))
		{
			this.AIScript.currentNode = this.AIScript.BehaviorNodes[(string)dictionary["currentNode"]];
		}
		this.UpdateChildren();
		this.HREd = dictionary.Get<bool>("HREd", false);
		this.CarIdx = dictionary.Get<int>("CarIdx", -1);
		this.CarColor3 = dictionary.Get<SVector3>("CarColor3", (this.CarIdx <= 0) ? new SVector3(1f, 1f, 1f, 1f) : RoadManager.Instance.CarPrefabs[this.CarIdx].GetComponent<NormalCar>().ColorPick.Evaluate(UnityEngine.Random.value));
		int hour = dictionary.Get<int>("LeaveTime", 16);
		SDateTime sdateTime = SDateTime.Now();
		this.LeaveTime = dictionary.Get<SDateTime>("LeaveTime2", new SDateTime(0, hour, sdateTime.Day + 1, sdateTime.Month, sdateTime.Year));
		bool flag = dictionary.Get<bool>("IsEnabled", false);
		base.enabled = flag;
		this.anim.enabled = flag;
		this.SetVisible(flag);
		this.NegotiateSalary = dictionary.Get<bool>("NegotiateSalary", false);
		this.IgnoreOffSalary = dictionary.Get<bool>("IgnoreOffSalary", false);
		this.AssignedRoomGroups = dictionary.Get<SHashSet<string>>("AssignedRoomGroups", new SHashSet<string>());
		this.CrunchHangover = dictionary.Get<float>("CrunchHangover", 0f);
		this.deal = dictionary.Get<Deal>("Deal", null);
		if (dictionary.Contains("FoodHold"))
		{
			this.Food = this.Holding[dictionary.Get<int>("FoodHold", 0)];
		}
		else if (dictionary.Contains("FoodChair"))
		{
			Furniture furniture4 = (Furniture)base.GetDeserializedObject(dictionary.Get<uint>("FoodChair", 0u));
			this.Food = ItemDispenser.Instance.Dispense("FoodPlate").GetComponent<Holdable>();
			if (furniture4 != null)
			{
				if (dictionary.Contains("FoodTable"))
				{
					Furniture furniture5 = (Furniture)base.GetDeserializedObject(dictionary.Get<uint>("FoodTable", 0u));
					if (furniture5 != null)
					{
						furniture5.Table.PlaceHoldable(this.Food, furniture4.SnappedTo.Links.First<SnapPoint>());
					}
					else if (!this.ReTakeItem(this.Food, true))
					{
						this.Food.DestroyMe();
					}
				}
				else
				{
					int num2 = dictionary.Get<int>("FoodPoint", 0);
					furniture4.Table.PlaceHoldable(this.Food, furniture4.SnapPoints[num2]);
				}
			}
			else if (!this.ReTakeItem(this.Food, true))
			{
				this.Food.DestroyMe();
			}
		}
		this.TestVarStand = dictionary.Get<float>("TestVarStand", 0f);
		this.TestVarVehicle = dictionary.Get<float>("TestVarVehicle", 0f);
		this.Order = dictionary.Get<ProductPrintOrder>("PrintOrder", new ProductPrintOrder());
		return this;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["IsEnabled"] = (mode == GameReader.LoadMode.Full && base.isActiveAndEnabled);
		dictionary["VacationMonthNew"] = this.VacationMonth;
		dictionary["AlternateVacation"] = this.AlternateVacation;
		dictionary["CachedBenefits"] = this._cachedBenefits;
		dictionary["CachedBenefitValue"] = this._cachedBenefitValue;
		dictionary["TeamRelationNum"] = this.TeamRelationNum;
		dictionary["TeamRelationTimer"] = this.TeamRelationTimer;
		dictionary["ChristmasBonus"] = this.ChristmasBonus;
		if (mode == GameReader.LoadMode.Full)
		{
			dictionary["UsingFurniture2"] = ((!(this.UsingPoint == null)) ? this.UsingPoint.Parent.DID : 0u);
			dictionary["UsingPoint"] = ((!(this.UsingPoint == null)) ? this.UsingPoint.Id : 0);
			dictionary["Reservations"] = (from x in GameSettings.Instance.sRoomManager.AllFurniture
			where x.Reserved == this
			select x.DID).ToArray<uint>();
			dictionary["Owns"] = (from x in this.Owns
			select x.DID).ToArray<uint>();
			string key = "CurrentPath";
			object value;
			if (this.CurrentPath == null)
			{
				value = null;
			}
			else
			{
				value = (from x in this.CurrentPath
				select x).ToArray<SVector3>();
			}
			dictionary[key] = value;
			dictionary["PathProg"] = this.PathProg;
			dictionary["CurrentPathNode"] = this.CurrentPathNode;
			dictionary["Boxes"] = this.Boxes;
			dictionary["Timer"] = this.Timer;
			dictionary["currentNode"] = this.AIScript.currentNode.Name;
			dictionary["position"] = base.transform.position;
			dictionary["rotation"] = base.transform.rotation;
			dictionary["animation"] = this.anim.GetInteger("AnimControl");
			dictionary["AnimState"] = this.anim.GetCurrentAnimatorStateInfo(0).shortNameHash;
			dictionary["AnimTime"] = this.anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
			dictionary["LoiterTable"] = ((!(this.LoiterTable == null)) ? this.LoiterTable.FurnComp.DID : 0u);
			dictionary["CleaningRoom"] = ((!(this.CleaningRoom == null)) ? this.CleaningRoom.DID : 0u);
			dictionary["Holding"] = this.Holding.SelectInPlace((Holdable x) => (!(x == null)) ? x.gameObject.name : null);
			dictionary["Coffee"] = (this.coffee != null);
			dictionary["TrappedInToilet"] = this.TrappedInToilet;
			dictionary["IsIdle"] = this.IsIdle;
			dictionary["IdleStatus"] = (int)this.IdleStatus;
			dictionary["Turn"] = this.Turn;
			dictionary["TargetRot"] = this.TargetRot;
			dictionary["LastAnim"] = this.LastAnim;
			if (this.Food != null)
			{
				if (this.Holding[0] == this.Food)
				{
					dictionary["FoodHold"] = 0;
				}
				else if (this.Holding[1] == this.Food)
				{
					dictionary["FoodHold"] = 1;
				}
				else if (this.UsingPoint != null)
				{
					dictionary["FoodChair"] = this.UsingPoint.Parent.DID;
					if (this.UsingPoint.Parent.SnappedTo != null)
					{
						dictionary["FoodTable"] = this.UsingPoint.Parent.SnappedTo.Parent.DID;
					}
					else
					{
						dictionary["FoodPoint"] = Array.IndexOf<InteractionPoint>(this.UsingPoint.Parent.InteractionPoints, this.UsingPoint);
					}
				}
			}
			dictionary["CleaningPoints"] = (from x in this.CleaningPoints
			select x).ToArray<SVector3>();
			dictionary["GoHomeNow"] = this.GoHomeNow;
			dictionary["AssignedRoomGroups"] = this.AssignedRoomGroups;
			dictionary["LeaveTime2"] = this.LeaveTime;
			dictionary["Deal"] = this.deal;
			dictionary["TestVarStand"] = this.TestVarStand;
			dictionary["TestVarVehicle"] = this.TestVarVehicle;
			dictionary["PrintOrder"] = this.Order;
			dictionary["AvailableDirt"] = this.AvailableDirt;
			dictionary["ReservedFurniture"] = ((!(this.Reserved == null)) ? this.Reserved.DID : 0u);
		}
		dictionary["SpecialState"] = this.SpecialState;
		dictionary["Effectiveness"] = this.Effectiveness;
		dictionary["LastMeeting"] = this.LastMeeting;
		dictionary["LastSocial"] = this.LastSocial;
		dictionary["MeetingTime2"] = this.MeetingTime;
		dictionary["employee"] = this.employee;
		dictionary["AIType"] = (int)this.AItype;
		dictionary["StaffOn"] = this.StaffOn;
		dictionary["StaffOff"] = this.StaffOff;
		dictionary["OnCall"] = this.OnCall;
		dictionary["CurrentWorkItem"] = this.CurrentWorkItem;
		dictionary["WorkCyclesLeft"] = this.WorkCyclesLeft;
		dictionary["Female"] = this.Female;
		dictionary["CoursePoints"] = this.CoursePoints;
		dictionary["CourseRole"] = (int)this.CourseRole;
		dictionary["CourseSpec"] = this.CourseSpec;
		dictionary["LastCourse"] = this.LastCourse;
		dictionary["CostPerMinute"] = this.CostPerMinute;
		dictionary["Despawned"] = this.Despawned;
		dictionary["SkinColor"] = this.SkinColor;
		dictionary["HairColor"] = this.HairColor;
		dictionary["NewStyle"] = (from x in this._bodyItems
		select x.Save()).ToArray<ActorBodyItem.BodyItemObject>();
		dictionary["HREd"] = this.HREd;
		dictionary["CarIdx"] = this.CarIdx;
		dictionary["CarColor3"] = this.CarColor3;
		dictionary["NegotiateSalary"] = this.NegotiateSalary;
		dictionary["IgnoreOffSalary"] = this.IgnoreOffSalary;
		dictionary["CrunchHangover"] = this.CrunchHangover;
	}

	public override string WriteName()
	{
		return "Actor";
	}

	public void InitSpawnPath()
	{
		this.CurrentPathNode = 0;
		this.PathProg = 0f;
		this.CurrentPath = new List<Vector3>(new Vector3[]
		{
			base.transform.position,
			base.transform.position + new Vector3(UnityEngine.Random.Range(-0.5f, 0.5f), 0f, UnityEngine.Random.Range(-0.5f, 0.5f))
		});
	}

	public void Fire()
	{
		if (!this.employee.Fired)
		{
			this.Dismiss();
			if (this.AItype == AI<Actor>.AIType.Employee)
			{
				this.QuitAffectTeam();
				float benefitValue = this.GetBenefitValue("Severance pay");
				if (benefitValue > 0f)
				{
					GameSettings.Instance.MyCompany.MakeTransaction(-benefitValue * this.GetRealSalary(), Company.TransactionCategory.Benefits, "Severance pay");
				}
			}
		}
	}

	public void Dismiss()
	{
		if (this != null)
		{
			this.KillAutoDev();
			this.MakeUnIdle();
			this.GoHomeNow = true;
			this.employee.Fired = true;
			this.IgnoreOffSalary = true;
			if (!base.enabled)
			{
				UnityEngine.Object.Destroy(base.gameObject);
				this.OnDestroy();
			}
		}
	}

	public bool WorksForFree()
	{
		return this.employee.Founder;
	}

	public float GetRealSalary()
	{
		if (this.AItype != AI<Actor>.AIType.Employee && this.OnCall)
		{
			return 0f;
		}
		return (!this.WorksForFree()) ? this.employee.Salary : 0f;
	}

	private void UpdateProblems()
	{
		if (!this.employee.Fired && !this.employee.Founder && this.employee.SatisfactionHitZero)
		{
			this.employee.SatisfactionHitZero = false;
			this.EscalateProblem();
			this.employee.JobSatisfaction = 1f;
		}
	}

	private void EscalateProblem()
	{
		IEnumerable<string> source = (from x in this.employee.Thoughts.List
		where x.Mood.Negative && x.Mood.QuitReason != null && x.Effect > 0f
		orderby x.Effect descending
		select x.Mood.QuitReason).Distinct<string>();
		if (Actor.<>f__mg$cache0 == null)
		{
			Actor.<>f__mg$cache0 = new Func<string, string>(Localization.Loc);
		}
		string[] array = source.Select(Actor.<>f__mg$cache0).ToArray<string>();
		float num = this.employee.Thoughts.List.MaxSafe((Employee.ThoughtEffect x) => x.Effect, float.MinValue, 0f);
		float num2 = (this.employee.Salary >= this.employee.Worth(-2, true)) ? Mathf.Round(num * 25f) : (this.employee.Worth(-2, true) - this.employee.Salary);
		num2 = Mathf.Max(50f, Mathf.Ceil(num2 / 50f) * 50f);
		if (Utilities.GetMonths(this.employee.LastWage, SDateTime.Now()) == 0f)
		{
			return;
		}
		this.employee.Demanded += num2;
		if (this.Team != null && this.GetTeam().Leader != this && this.GetTeam().HREnabled && this.GetTeam().HR.HandleComplaints)
		{
			bool flag = this.GetTeam().HR.HandleComplaint(num2, this.GetTeam());
			if (flag)
			{
				this.employee.ChangeSalary(this.employee.Salary + num2, this.employee.AskedFor + num2, this, false);
			}
			else
			{
				this.employee.Fired = true;
				if (UnityEngine.Random.value * num * (1f - this.GetTeam().Leader.employee.GetSkill(Employee.EmployeeRole.Lead)) > 0.25f)
				{
					GameSettings.Instance.MyCompany.ChangeBusinessRep(-0.25f, "Disgruntled employee", 1f);
					Newspaper.GenerateEmployeeComplaint(this.employee, Newspaper.MakeList(array));
				}
			}
		}
		else
		{
			HUD.Instance.complaintWindow.Show(this, array, num2, num);
		}
	}

	public override string ToString()
	{
		return (this.employee != null) ? this.employee.FullName : "N/A";
	}

	public void Snapshot()
	{
		if (base.gameObject == null)
		{
			return;
		}
		bool isHighlight = this.IsHighlight;
		bool isSecondary = this.IsSecondary;
		base.Highlight(false, false);
		HUD.Instance.Portraits.transform.position = this.NeckBone.position + this.NeckBone.rotation * Vector3.up * 0.65f + Vector3.up * 0.1f;
		HUD.Instance.Portraits.transform.rotation = Quaternion.LookRotation(this.NeckBone.position + Vector3.up * 0.08f - HUD.Instance.Portraits.transform.position);
		if (this._eyeScript != null)
		{
			this._eyeScript.Face.SetInt("_EyeNum", this._eyeScript.BlinkIndices[0]);
		}
		HUD.Instance.Portraits.RenderObject(base.gameObject);
		this.IsHighlight = isHighlight;
		this.IsSecondary = isSecondary;
		base.Highlight(this.IsHighlight, this.IsSecondary);
	}

	public static int GoHomeState(Actor self)
	{
		if (self.CurrentPath == null)
		{
			if (!(self.MyCar != null))
			{
				return 0;
			}
			Vector3 position = self.MyCar.SpawnPoints[self.CarSpawnID].transform.position;
			self.CurrentPath = GameSettings.Instance.sRoomManager.FindPath(self.transform.position, position, self.team, self.GetRole(), self.TrappedInToilet || self.AItype != AI<Actor>.AIType.Employee);
			if (self.CurrentPath == null)
			{
				self.CurrentPath = GameSettings.Instance.sRoomManager.FindPath(self.transform.position, position, self.team, self.GetRole(), true);
			}
			if (self.CurrentPath == null)
			{
				return 0;
			}
			HUD.Instance.CantGetHome.Remove(self);
			self.InitPath();
			return 1;
		}
		else
		{
			if (self.WalkPath())
			{
				if (self.MyCar != null)
				{
					CarSpawn carSpawn = self.MyCar.SpawnPoints[self.CarSpawnID];
					self.transform.rotation = carSpawn.transform.rotation;
					self.anim.Play(CarSpawn.AnimationInStates[carSpawn.SubAnimation], 0, 0f);
				}
				self.MeetingTime = SDateTime.Now();
				return 2;
			}
			return 1;
		}
	}

	public static int ShouldUseBus(Actor self)
	{
		return (!(self.MyCar == null)) ? 0 : 2;
	}

	public static int GoHomeBusStop(Actor self)
	{
		if (!GameSettings.Instance.sActorManager.ReadyForHome.Contains(self))
		{
			if (self.CurrentPath == null)
			{
				Vector3 position = GameSettings.Instance.BusStopSign.transform.position;
				Vector3 e = new Vector3(position.x + UnityEngine.Random.Range(0f, 0.75f), 0f, position.z + UnityEngine.Random.Range(-3f, -0.1f));
				bool flag = self.PathToPoint(e, false);
				if (!flag)
				{
					flag = self.PathToPoint(e, true);
				}
				if (!flag)
				{
					HUD.Instance.CantGetHome.Add(self);
					return 0;
				}
				if (self.MyCar != null)
				{
					self.MyCar.SpawnPoints[self.CarSpawnID].Occupants.Remove(self);
					self.MyCar = null;
				}
				HUD.Instance.CantGetHome.Remove(self);
			}
			if (self.WalkPath())
			{
				GameSettings.Instance.sActorManager.ReadyForHome.Add(self);
			}
			return 1;
		}
		if (self.CurrentPath == null)
		{
			self.anim.SetInteger("AnimControl", 0);
			if (self.MyCar != null)
			{
				self.MyCar.SpawnPoints[self.CarSpawnID].Occupants.Remove(self);
				self.MyCar = null;
				return 0;
			}
			return 1;
		}
		else
		{
			if (self.MyCar == null)
			{
				self.CurrentPath = null;
				return 0;
			}
			if (self.WalkPath())
			{
				CarSpawn carSpawn = self.MyCar.SpawnPoints[self.CarSpawnID];
				self.transform.rotation = carSpawn.transform.rotation;
				GameSettings.Instance.sActorManager.ReadyForHome.Remove(self);
				self.anim.Play(CarSpawn.AnimationInStates[carSpawn.SubAnimation], 0, 0f);
				self.MeetingTime = SDateTime.Now();
				return 2;
			}
			return 1;
		}
	}

	public int HandleLoiter(bool canComplain)
	{
		if (this.AItype == AI<Actor>.AIType.Employee)
		{
			this.MakeIdle(Actor.WorkStatus.NoComputer);
		}
		if (this.GoHomeNow || this.employee.Fired)
		{
			return 2;
		}
		if (this.CurrentPath != null && this.UsingPoint == null)
		{
			this.WalkPath();
			return 1;
		}
		if (this.Timer != -1f)
		{
			return this.WaitForTimer(this.Timer);
		}
		if (this.LoiterTable != null)
		{
			int num = this.GoToFurniture("Chair", "Use", -1, false, null, false, false, null);
			if (num == 0 || this.UsingPoint == null)
			{
				this.FreeLoiterTable();
				return 2;
			}
			if (num == 2)
			{
				this.anim.SetInteger("AnimControl", (!this.UsingPoint.Parent.CanLean) ? 12 : 7);
				this.TurnToFurniture();
				return this.WaitForTimer((float)UnityEngine.Random.Range(1, 10));
			}
			return 1;
		}
		else if (this.UsingPoint != null && this.UsingPoint.Parent.Type.Equals("Couch"))
		{
			int num2 = this.GoToFurniture("Couch", "Use", -1, false, null, true, false, null);
			if (num2 == 0)
			{
				this.FreeLoiterTable();
				return 2;
			}
			if (num2 == 2)
			{
				this.anim.SetInteger("AnimControl", 12);
				this.TurnToFurniture();
				return this.WaitForTimer((float)UnityEngine.Random.Range(1, 10));
			}
			return 1;
		}
		else
		{
			int num3 = this.GoToFurniture("Couch", "Use", -1, false, null, true, false, null);
			if (num3 != 0)
			{
				return 1;
			}
			if (this.LoiterTable == null)
			{
				if (this.Timer == -1f)
				{
					TableScript tableScript = this.FindFreeTable(Room.RoomLimits.Lounge, false, false, false);
					if (tableScript != null)
					{
						Furniture freeChair = tableScript.GetFreeChair(this, false);
						if (freeChair != null)
						{
							InteractionPoint interactionPoint = freeChair.GetInteractionPoint(this, "Use");
							if (interactionPoint != null && this.PathToFurniture(interactionPoint, false))
							{
								this.LoiterTable = tableScript;
								this.LoiterTable.ReserveTables(true, true);
								return 1;
							}
						}
					}
					if (!this.currentRoom.IsNeutral())
					{
						Room room = (from x in GameSettings.Instance.sRoomManager.GetConnectedRooms(this.currentRoom)
						select x.Key).FirstOrDefault((Room x) => !x.Dummy && x.IsNeutral());
						if (room != null)
						{
							Vector2? vector = room.FindRandomSpot();
							if (vector != null && this.PathToPoint(new Vector3(vector.Value.x, (float)(room.Floor * 2), vector.Value.y), false))
							{
								return 1;
							}
						}
					}
					if (canComplain)
					{
						this.employee.AddInstantMood("NoSitting", this, 1f);
						this.anim.SetInteger("AnimControl", 0);
						return this.WaitForTimer((float)UnityEngine.Random.Range(1, 10));
					}
				}
				this.anim.SetInteger("AnimControl", 0);
				return this.WaitForTimer((float)UnityEngine.Random.Range(1, 10));
			}
			return 2;
		}
	}

	public void OnDespawn()
	{
		if (this.MyCar != null)
		{
			CarSpawn carSpawn = this.MyCar.SpawnPoints[this.CarSpawnID];
			carSpawn.Occupants.Remove(this);
			this.MyCar = null;
		}
		for (int i = 0; i < this.Holding.Length; i++)
		{
			Holdable holdable = this.Holding[i];
			if (holdable != null && holdable.DestroyOnDespawn)
			{
				this.LeaveItem(holdable, true);
			}
		}
		if (this.team != null)
		{
			for (int j = 0; j < this.team.WorkItems.Count; j++)
			{
				SoftwareWorkItem softwareWorkItem = this.team.WorkItems[j] as SoftwareWorkItem;
				if (softwareWorkItem != null)
				{
					softwareWorkItem.Working.Remove(this.DID);
				}
			}
		}
		this.anim.enabled = false;
		base.enabled = false;
		this.SetVisible(false);
		this.GoHomeNow = false;
	}

	private void OnDrawGizmos()
	{
		if (this.CurrentPath != null && this.CurrentPath.Count > 0)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawSphere(this.CurrentPath[0], 0.1f);
			for (int i = 1; i < this.CurrentPath.Count; i++)
			{
				Gizmos.DrawSphere(this.CurrentPath[i], 0.1f);
				Gizmos.DrawLine(this.CurrentPath[i - 1] + Vector3.up * 0.1f, this.CurrentPath[i] + Vector3.up * 0.1f);
			}
			Gizmos.color = Color.white;
		}
		if (this.MyCar != null)
		{
			Gizmos.DrawLine(base.transform.position, this.MyCar.transform.position);
		}
	}

	public override int GetFloor()
	{
		return this.Floor;
	}

	public override bool IsSelectableInView()
	{
		return this.currentRoom.Outdoors || this.currentRoom == GameSettings.Instance.sRoomManager.Outside || this.GetFloor() == GameSettings.Instance.ActiveFloor;
	}

	public void UpdateNow(float delta)
	{
		this.UpdateCurrentRoom(false);
		if (this.UpdateStateInfluence)
		{
			this.UpdateStateInfluence = false;
			this.Effectiveness = this.GetStateInfluence(delta);
		}
	}

	public void UpdateNow2(float delta)
	{
		if (this.ShouldWork)
		{
			this.DoWork(delta);
		}
	}

	public bool NeedUpdate(bool firstFunction)
	{
		return base.enabled;
	}

	public bool WageNegotiationNecessary()
	{
		return this.employee.Salary == 0f || Mathf.Abs(this.employee.Salary - this.employee.Worth(-2, true)) / this.employee.Salary > 0.01f;
	}

	public GameObject GetGameObject()
	{
		return (!(this == null)) ? base.gameObject : null;
	}

	public float GetSpeed(float angle)
	{
		return 0f;
	}

	public bool ShouldTrySocial()
	{
		return this.employee.Stress == 0f || SDateTime.Now().ToInt() - this.LastSocial.ToInt() > 180;
	}

	public void MakeIdle(Actor.WorkStatus status)
	{
		if (this.employee.Fired)
		{
			this.MakeUnIdle();
			return;
		}
		this.IdleStatus = status;
		if (!this.IsIdle)
		{
			HUD.Instance.AddToIdle(this);
		}
		this.IsIdle = true;
	}

	public void MakeUnIdle()
	{
		this.IdleStatus = Actor.WorkStatus.Working;
		if (this.IsIdle)
		{
			HUD.Instance.RemoveFromIdle(this);
		}
		this.IsIdle = false;
	}

	public void KillAutoDev()
	{
		for (int i = this.AutoDevs.Count - 1; i > -1; i--)
		{
			AutoDevWorkItem autoDevWorkItem = this.AutoDevs[i];
			if (autoDevWorkItem.Leader == this)
			{
				autoDevWorkItem.Leader = null;
			}
		}
	}

	public void ScheduleVacation(bool fromNow)
	{
		if (fromNow)
		{
			if (this.Team != null)
			{
				int nextVacation = this.GetTeam().GetNextVacation(this);
				SDateTime sdateTime = new SDateTime(0, 0, 0, nextVacation, SDateTime.Now().Year + 1);
				this.VacationMonth = ((Utilities.GetMonths(SDateTime.Now(), sdateTime) <= 14f) ? sdateTime : SDateTime.NextMonth(nextVacation));
			}
			else
			{
				this.VacationMonth = (SDateTime.Now() + 12).SimplifyMore();
			}
		}
		else if (this.Team != null)
		{
			int nextVacation2 = this.GetTeam().GetNextVacation(this);
			int month = this.VacationMonth.Month;
			int num = Mathf.Abs(month - nextVacation2);
			if (num > 5)
			{
				num = 12 - num;
			}
			this.AlternateVacation = ((num > 2) ? this.VacationMonth : new SDateTime(0, 0, 0, nextVacation2, this.VacationMonth.Year));
		}
	}

	public float GetBenefitValue(string benefit)
	{
		return (this.employee != null) ? this.employee.GetBenefitValue(benefit) : EmployeeBenefit.GetBenefitValue((!(GameSettings.Instance == null)) ? GameSettings.Instance.CompanyBenefits : null, benefit);
	}

	private void InitBenefits()
	{
		if (this._cachedBenefitValue == -1f)
		{
			this._cachedBenefitValue = 0f;
			foreach (KeyValuePair<string, EmployeeBenefit> keyValuePair in EmployeeBenefit.Benefits)
			{
				float benefitValue = this.GetBenefitValue(keyValuePair.Key);
				if (keyValuePair.Value.OnChange != null && !Mathf.Approximately(benefitValue, keyValuePair.Value.Default))
				{
					keyValuePair.Value.OnChange(this, keyValuePair.Value.Default, benefitValue);
				}
				this._cachedBenefitValue += keyValuePair.Value.GetScore(benefitValue) * keyValuePair.Value.GetWeight(this.employee);
			}
		}
	}

	public void CacheBenefits()
	{
		this._cachedBenefitValue = 0f;
		foreach (KeyValuePair<string, EmployeeBenefit> keyValuePair in EmployeeBenefit.Benefits)
		{
			float benefitValue = this.GetBenefitValue(keyValuePair.Key);
			if (keyValuePair.Value.OnChange != null)
			{
				this._cachedBenefits[keyValuePair.Key] = benefitValue;
			}
			this._cachedBenefitValue += keyValuePair.Value.GetScore(benefitValue) * keyValuePair.Value.GetWeight(this.employee);
		}
	}

	public void ApplyNewBenefits()
	{
		if (this._cachedBenefitValue == -1f)
		{
			this.InitBenefits();
			return;
		}
		float num = 0f;
		foreach (KeyValuePair<string, EmployeeBenefit> keyValuePair in EmployeeBenefit.Benefits)
		{
			float benefitValue = this.GetBenefitValue(keyValuePair.Key);
			float num2;
			if (keyValuePair.Value.OnChange != null && this._cachedBenefits.TryGetValue(keyValuePair.Key, out num2) && !Mathf.Approximately(benefitValue, num2))
			{
				keyValuePair.Value.OnChange(this, num2, benefitValue);
			}
			num += keyValuePair.Value.GetScore(benefitValue) * keyValuePair.Value.GetWeight(this.employee);
		}
		float num3 = (num - this._cachedBenefitValue) / (EmployeeBenefit.MaxBenefits / 2f);
		if (num3 >= 0f)
		{
			this.employee.AddInstantMood("NewGoodBenfits", this, num3);
		}
		else
		{
			this.employee.AddInstantMood("NewBadBenefits", this, -num3);
		}
		this.CacheBenefits();
	}

	public void QuitAffectTeam()
	{
		if (this.Team != null)
		{
			float months = Utilities.GetMonths(this.employee.Hired, SDateTime.Now());
			if (months > 6f)
			{
				float num = months.MapRange(6f, 24f, 0f, 1f, true);
				List<Actor> employeesDirect = this.GetTeam().GetEmployeesDirect();
				for (int i = 0; i < employeesDirect.Count; i++)
				{
					Actor actor = employeesDirect[i];
					float months2 = Utilities.GetMonths(actor.employee.Hired, SDateTime.Now());
					if (months2 > 6f)
					{
						float num2 = this.employee.Compatibility(actor.employee);
						float num3 = months2.MapRange(6f, 24f, 0f, 1f, true);
						if (num2 > 1f)
						{
							actor.employee.AddInstantMood("TeammateLeft", actor, num3 * num * (num2 - 1f));
						}
					}
				}
			}
		}
	}

	public void PostUpdate()
	{
		LOD[] lods = this.LOD.GetLODs();
		lods[0].renderers = (from x in this._bodyItems.Concate(this._shadow)
		select x.rend).ToArray<Renderer>();
		this.LOD.SetLODs(lods);
	}

	public void SetLOD2Color(string part, Color col)
	{
		MaterialPropertyBlock block = new MaterialPropertyBlock();
		block.SetColor("_Color", col);
		if (part != null)
		{
			if (!(part == "Head"))
			{
				if (!(part == "Upper"))
				{
					if (!(part == "Lower"))
					{
						if (!(part == "Hair"))
						{
							if (part == "Feet")
							{
								this.LOD2Feet.ForEachEnum(delegate(Renderer x)
								{
									x.SetPropertyBlock(block);
								});
							}
						}
						else
						{
							this.LOD2Hair.ForEachEnum(delegate(Renderer x)
							{
								x.SetPropertyBlock(block);
							});
						}
					}
					else
					{
						this.LOD2LowerBody.ForEachEnum(delegate(Renderer x)
						{
							x.SetPropertyBlock(block);
						});
					}
				}
				else
				{
					this.LOD2UpperBody.ForEachEnum(delegate(Renderer x)
					{
						x.SetPropertyBlock(block);
					});
				}
			}
			else
			{
				this.LOD2Head.ForEachEnum(delegate(Renderer x)
				{
					x.SetPropertyBlock(block);
				});
			}
		}
	}

	public static int AffectorCount = 21;

	public static float HumanHeight = 1.4f;

	public int EmitType;

	public float TestVarStand;

	public float TestVarVehicle;

	public float CrunchHangover;

	private static float TeamRelationTimerMax = 30f;

	private int TeamRelationNum;

	private float TeamRelationTimer;

	[NonSerialized]
	public float ChristmasBonus;

	private InteractionPoint _usingPoint;

	public HashSet<Furniture> Owns = new HashSet<Furniture>();

	public List<Vector3> CurrentPath;

	public AudioSource AudioComp;

	public AudioClip[] KeyboardSFX;

	public AudioClip[] TalkSFX;

	public AudioClip[] FemaleTalkSFX;

	public BoxCollider[] Colliders;

	public AudioClip HammerHitSFX;

	public SDateTime LastMeeting;

	public SDateTime MeetingTime;

	public CarScript MyCar;

	public MeshRenderer VisibilityRenderer;

	public LODGroup LOD;

	[NonSerialized]
	public List<AutoDevWorkItem> AutoDevs = new List<AutoDevWorkItem>();

	public int CarSpawnID;

	[NonSerialized]
	public float LastCheckWait = -1f;

	public GameObject CensorCube;

	public Renderer[] LOD2UpperBody;

	public Renderer[] LOD2LowerBody;

	public Renderer[] LOD2Head;

	public Renderer[] LOD2Feet;

	public Renderer[] LOD2Hair;

	[NonSerialized]
	public SHashSet<string> AssignedRoomGroups = new SHashSet<string>();

	[NonSerialized]
	public ProductPrintOrder Order = new ProductPrintOrder();

	[NonSerialized]
	private ActorBodyItem _shadow;

	public int Boxes;

	public int CarIdx = -1;

	public Color CarColor3;

	public int CurrentPathNode;

	public SDateTime LeaveTime;

	public float PathProg;

	public float WalkSpeed = 2f;

	public float Timer = -1f;

	public float SocialFactor;

	public float StressFactor;

	public bool TrappedInToilet;

	public bool NegotiateSalary = true;

	public bool IgnoreOffSalary;

	public SDateTime LastSocial;

	public CoffeeScript coffee;

	public Holdable Food;

	private Employee _employee;

	private Furniture _reserved;

	[NonSerialized]
	private float CostPerMinute;

	public Transform NeckBone;

	public Transform LeftHand;

	public Transform RightHand;

	public SDateTime _vacationMonth;

	public SDateTime AlternateVacation;

	public bool AtFurniture;

	public bool WaitSpawn = true;

	public bool Despawned = true;

	public bool HREd;

	public bool IsIdle;

	public Actor.WorkStatus IdleStatus;

	public Room CleaningRoom;

	[NonSerialized]
	public Deal deal;

	public Stack<Vector3> CleaningPoints = new Stack<Vector3>();

	public float AvailableDirt;

	private string[] actions = new string[]
	{
		"Dismiss",
		"Change Team",
		"Change Role",
		"Select Team",
		"Select Owned",
		"Change Salary",
		"Send home",
		"Details",
		"Pair Use",
		"Educate"
	};

	private string[] actionsFounder = new string[]
	{
		"Change Team",
		"Change Role",
		"Select Team",
		"Select Owned",
		"Send home",
		"Details",
		"Pair Use",
		"Educate"
	};

	private string[] staffActions = new string[]
	{
		"Dismiss",
		"Send home",
		"RoomPair"
	};

	public Animator anim;

	public Color SkinColor;

	public Color HairColor;

	[NonSerialized]
	public uint[] LastWorkItems = new uint[8];

	public int LastWorkCounter;

	public bool IsTalking;

	public bool GoHomeNow;

	public bool OnCall;

	public float NextParticle = 1f;

	public bool IsWorking;

	public float Effectiveness;

	public TableScript LoiterTable;

	public int StaffOn = 8;

	public int StaffOff = 12;

	public AI<Actor>.AIType AItype;

	public AI<Actor> AIScript;

	[NonSerialized]
	public HashSet<Furniture> ReservedFurniture = new HashSet<Furniture>();

	public Actor.HomeState SpecialState;

	public int CurrentWorkItem;

	public int WorkCyclesLeft = 1;

	public bool Female;

	public float CoursePoints;

	public Employee.EmployeeRole CourseRole;

	public string CourseSpec;

	public SDateTime LastCourse;

	public bool Turn;

	public float TargetRot;

	public Holdable[] Holding = new Holdable[2];

	[NonSerialized]
	public float[] Affactors;

	[NonSerialized]
	public Dictionary<string, InteractionPoint> InQueue = new Dictionary<string, InteractionPoint>();

	private Room _currentRoom;

	private static float[] AnimationSpeeds = new float[]
	{
		0f,
		1f,
		10f,
		30f
	};

	private EyeScript _eyeScript;

	private Renderer[] Children;

	[NonSerialized]
	private List<ActorBodyItem> _bodyItems = new List<ActorBodyItem>();

	[NonSerialized]
	public ActorBodyItem.BodyItemObject[] _savedStyle;

	private float _noisiness = 1f;

	private static readonly HashSet<uint> IteratedAssignedRooms = new HashSet<uint>();

	private static readonly List<string> DeletedAssignedRooms = new List<string>();

	private float teamComp = -1f;

	public int LastAnim;

	private Team team;

	[NonSerialized]
	private float rTime;

	[NonSerialized]
	private Vector3 LastPos;

	[NonSerialized]
	public bool LastVisible = true;

	public static Dictionary<Room.RoomLimits, HashSet<int>> ValidLimits = new Dictionary<Room.RoomLimits, HashSet<int>>
	{
		{
			Room.RoomLimits.Canteen,
			new HashSet<int>
			{
				-1,
				-3
			}
		},
		{
			Room.RoomLimits.Lounge,
			new HashSet<int>
			{
				-1,
				-2
			}
		},
		{
			Room.RoomLimits.Meeting,
			new HashSet<int>
			{
				-1,
				-4
			}
		}
	};

	public bool ShouldWork;

	[NonSerialized]
	private bool UpdateStateInfluence;

	[NonSerialized]
	private Dictionary<string, float> _cachedBenefits = new Dictionary<string, float>();

	[NonSerialized]
	private float _cachedBenefitValue = -1f;

	[CompilerGenerated]
	private static Func<string, string> <>f__mg$cache0;

	public enum AnimationStates
	{
		Idle,
		Walk,
		Fridge,
		Work,
		SitStill,
		Talk,
		Coffee,
		Relax,
		TurnRight,
		TurnLeft,
		Dust,
		Repair,
		SitHandsdown,
		HappyKeyboard,
		WaterState,
		DrinkWater,
		TalkWater,
		StandUpTalking,
		VehicleOut,
		PickBox,
		OpenVan
	}

	public enum WorkStatus
	{
		Working,
		NoWork,
		NoComputer,
		NotApplicable,
		NoEffectiveness,
		NoActiveWork
	}

	public enum HomeState
	{
		Default,
		Retired,
		Vacation,
		Sick,
		Dead,
		Hospitalized
	}

	public enum Affector
	{
		Slavery,
		Hunger,
		Energy,
		Bladder,
		JobSatisfaction,
		Stress,
		Social,
		Salary,
		Fired,
		TeamCompatibility,
		RoomAura,
		Temperature,
		Lighting,
		Environment,
		Comfort,
		Computer,
		Noise,
		Basement,
		OtherTeams,
		OwnOffice,
		CrunchHangover
	}
}
