﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ContractWindow : MonoBehaviour
{
	private void Start()
	{
		this.SpecLegend.Colors = (this.SpecChart.Colors = HUD.ThemeColors.ToList<Color>());
		this.ContractInfo.SetData(new string[]
		{
			"Company".Loc(),
			"Code units".Loc(),
			"Art units".Loc(),
			"Recommended designers".Loc(),
			"Recommended programmers".Loc(),
			"Recommended artists".Loc(),
			"Time-limit".Loc() + "(" + "Months".Loc() + ")",
			"Expected quality".Loc(),
			"Upfront".Loc(),
			"When done".Loc(),
			"Late penalty".Loc(),
			"Cost per bug".Loc()
		}, new string[0]);
		this.ResultInfo.SetData(new string[]
		{
			"Income".Loc(),
			"Upfront".Loc(),
			"Bug penalty".Loc(),
			"Late penalty".Loc(),
			"Completion penalty".Loc(),
			"Cancel penalty".Loc(),
			"Net profit".Loc(),
			"Quality assesment".Loc(),
			"Delivered".Loc()
		}, new string[0]);
		this.Contracts.OnSelectChange = delegate(bool d)
		{
			ContractWork[] selected = this.Contracts.GetSelected<ContractWork>();
			this.FeatureList.text = string.Empty;
			if (selected.Length == 1)
			{
				ContractWork contractWork = selected[0];
				this.UpdateSpecs(contractWork);
				SoftwareType type = GameSettings.Instance.SoftwareTypes[contractWork.SoftwareType];
				float devTime = type.DevTime(contractWork.Features, null, null);
				float num = type.CodeArtRatio(contractWork.Features);
				int[] optimalEmployeeCount = type.GetOptimalEmployeeCount(devTime, num);
				this.ContractInfo.UpdateValues(new string[]
				{
					contractWork.Company,
					contractWork.CodeUnits.ToString("N2"),
					contractWork.ArtUnits.ToString("N2"),
					Mathf.Ceil((float)optimalEmployeeCount[0]).ToString(),
					Mathf.Ceil((float)optimalEmployeeCount[1] * num).ToString(),
					Mathf.Ceil((float)optimalEmployeeCount[1] * (1f - num)).ToString(),
					contractWork.Months.ToString(),
					SoftwareType.GetQualityLabel(contractWork.Quality),
					contractWork.Initial.CurrencyInt(true),
					contractWork.Done.CurrencyInt(true),
					contractWork.Penalty.CurrencyInt(true),
					contractWork.PerBug.Currency(true)
				});
				this.FeatureList.text = string.Join("\n", (from x in contractWork.Features
				select Localization.GetFeature(type, x)[0]).ToArray<string>());
			}
			else
			{
				this.UpdateSpecs(null);
				this.ContractInfo.UpdateValues(new string[0]);
			}
		};
		this.ContractResults.OnSelectChange = delegate(bool d)
		{
			ContractResult[] selected = this.ContractResults.GetSelected<ContractResult>();
			if (selected.Length == 1)
			{
				ContractResult contractResult = selected[0];
				this.ResultInfo.UpdateValues(new string[]
				{
					contractResult.Income.Currency(true),
					contractResult.Contract.Initial.CurrencyInt(true),
					string.Concat(new string[]
					{
						contractResult.Bugs.ToString(),
						" x ",
						contractResult.Contract.PerBug.Currency(true),
						" = ",
						contractResult.BugPenalty.Currency(true)
					}),
					contractResult.LatePenalty.Currency(true),
					contractResult.QualityPenalty.Currency(true),
					contractResult.CancelPenalty.Currency(true),
					contractResult.FinalResult.Currency(true),
					ContractWindow.QualityAssess(contractResult.QualityResult).Loc(),
					(contractResult.MonthDiff >= 0) ? ((contractResult.MonthDiff != 0) ? string.Format(((GameSettings.DaysPerMonth <= 1) ? "ContractLate" : "ContractLateDay").Loc(), contractResult.MonthDiff) : "ContractOnTime".Loc()) : string.Format(((GameSettings.DaysPerMonth <= 1) ? "ContractEarly" : "ContractEarlyDay").Loc(), -contractResult.MonthDiff)
				});
			}
			else
			{
				this.ResultInfo.UpdateValues(new string[0]);
			}
		};
	}

	private void UpdateSpecs(ContractWork work)
	{
		this.SpecLegend.Items.Clear();
		if (work != null)
		{
			Dictionary<string, float> specializationMonths = GameSettings.Instance.SoftwareTypes[work.SoftwareType].GetSpecializationMonths(work.Features, "Default", null, null);
			this.SpecChart.Values = (from x in specializationMonths
			select x.Value).ToList<float>();
			this.SpecLegend.Items.AddRange(specializationMonths.Keys);
		}
		else
		{
			this.SpecChart.Values.Clear();
		}
		this.SpecChart.UpdateCachedPie();
	}

	public static string QualityAssess(float input)
	{
		if (input < 0.9f)
		{
			return "Inadequate";
		}
		if (input < 1.05f)
		{
			return "Satisfactory";
		}
		return "Outstanding";
	}

	public void UpdateTeamLabel()
	{
		this.TeamLabel.text = Utilities.GetTeamDescription(this.Teams);
	}

	public void Show()
	{
		if (this.Window.Shown)
		{
			this.Window.Close();
			return;
		}
		this.Teams.Clear();
		this.Teams.AddRange(GameSettings.Instance.GetDefaultTeams("Contracts"));
		this.UpdateTeamLabel();
		this.SCMCombo.UpdateContent<string>(new string[]
		{
			"None"
		}.Concat(GameSettings.Instance.GetServerNames()));
		this.Window.Show();
		TutorialSystem.Instance.StartTutorial("Contracts", false);
	}

	public void PickTeams()
	{
		HUD.Instance.TeamSelectWindow.Show(false, this.Teams, delegate(string[] ts)
		{
			this.Teams.Clear();
			this.Teams.AddRange(ts);
			this.UpdateTeamLabel();
		}, null);
	}

	public void AddWork()
	{
		ContractWork contractWork = ContractWork.GenerateWork(this.ContractDifficulty());
		if (contractWork != null)
		{
			this.Contracts.Items.Add(contractWork);
		}
	}

	public float ContractDifficulty()
	{
		float r = GameSettings.Instance.MyCompany.DiscreteRep;
		if (this.Contracts.Items.Count == 0)
		{
			return r;
		}
		List<float> list = (from x in this.Contracts.Items.OfType<ContractWork>()
		select x.Difficulty into x
		where x < r
		orderby x
		select x).ToList<float>();
		list.Add(r);
		float num = list[0];
		float num2 = 0f;
		float num3 = list[0];
		for (int i = 1; i < list.Count - 1; i++)
		{
			float num4 = list[i + 1] - list[i];
			if (num4 > num)
			{
				num = num4;
				num2 = list[i];
				num3 = list[i + 1];
			}
		}
		return num2 + (num3 - num2) / 2f;
	}

	public void UpdateContracts(SDateTime time)
	{
		if (!this.Window.gameObject.activeSelf)
		{
			foreach (ContractWork contractWork in this.Contracts.Items.Cast<ContractWork>().ToArray<ContractWork>())
			{
				if (Utilities.GetMonths(contractWork.Added, time) > (float)UnityEngine.Random.Range(2, 12))
				{
					this.Contracts.Items.Remove(contractWork);
					this.AddWork();
				}
			}
		}
		int num = Mathf.CeilToInt(GameSettings.Instance.MyCompany.DiscreteRep * 15f + 5f) - this.Contracts.Items.Count;
		int num2 = UnityEngine.Random.Range(num / 2, num);
		for (int j = 0; j < num2; j++)
		{
			this.AddWork();
		}
	}

	public void AcceptJobs()
	{
		string scm = (this.SCMCombo.Selected >= 1) ? this.SCMCombo.SelectedItemString : null;
		ContractWork[] selected = this.Contracts.GetSelected<ContractWork>();
		if (selected.Length > 0)
		{
			GameSettings.Instance.TeamDefaults["Contracts"] = this.Teams.ToHashSet<string>();
			List<Team> list = this.Teams.SelectNotNull((string x) => GameSettings.Instance.sActorManager.Teams.GetOrNull(x)).ToList<Team>();
			this.Contracts.Selected.Clear();
			foreach (ContractWork contractWork in selected)
			{
				this.Contracts.Items.Remove(contractWork);
				WorkItem workItem = contractWork.GenerateWorkItem(scm);
				foreach (Team team in list)
				{
					workItem.AddDevTeam(team, false);
				}
				GameSettings.Instance.MyCompany.WorkItems.Add(workItem);
				SoftwareAlpha softwareAlpha = workItem as SoftwareAlpha;
				if (softwareAlpha != null)
				{
					softwareAlpha.CheckCompetency();
				}
				HUD.Instance.AddPopupMessage(string.Format("ContractIncomeMsg".Loc(), contractWork.Initial.CurrencyInt(true)), "Money", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Good, 0f, PopupManager.PopupIDs.None, 6);
				GameSettings.Instance.MyCompany.MakeTransaction((float)contractWork.Initial, Company.TransactionCategory.Contracts, null);
			}
		}
	}

	public void RejectJobs()
	{
		ContractWork[] selected = this.Contracts.GetSelected<ContractWork>();
		if (selected.Length > 0)
		{
			WindowManager.Instance.ShowMessageBox("RejectConfirmation".Loc(), true, DialogWindow.DialogType.Question, delegate
			{
				this.Contracts.Selected.Clear();
				ContractWork[] selected;
				foreach (ContractWork item in selected)
				{
					this.Contracts.Items.Remove(item);
				}
			}, "Reject contract", null);
		}
	}

	public GUIWindow Window;

	public GUIListView Contracts;

	public GUIListView ContractResults;

	public Text FeatureList;

	public GUIPieChart SpecChart;

	public GUILegend SpecLegend;

	public GUICombobox SCMCombo;

	public VarValueSheet ContractInfo;

	public VarValueSheet ResultInfo;

	public Text TeamLabel;

	[NonSerialized]
	public HashSet<string> Teams = new HashSet<string>();
}
