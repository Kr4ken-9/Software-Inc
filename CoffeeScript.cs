﻿using System;
using UnityEngine;

public class CoffeeScript : MonoBehaviour
{
	private void Update()
	{
		float t = 1f - this.Life / 30f;
		this.CoffeeQuad.localPosition = new Vector3(0f, Mathf.Lerp(this.StartY, this.EndY, t), 0f);
		this.CoffeeQuad.localScale = Vector3.one * Mathf.Lerp(this.StartScale, this.EndScale, t);
		if (this.Life > 0f)
		{
			this.Life -= Time.deltaTime * GameSettings.GameSpeed;
		}
	}

	public float Life = 30f;

	public Transform CoffeeQuad;

	public float StartScale;

	public float EndScale;

	public float StartY;

	public float EndY;
}
