﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class NumberRangeWindow : MonoBehaviour
{
	public void Show(float from, float to, Action<float, float> action)
	{
		this.Window.Show();
		this.FromField.text = from.ToString("#,0.##");
		this.ToField.text = to.ToString("#,0.##");
		this.WhenDone = action;
	}

	public void ClickOK()
	{
		bool flag = true;
		float arg = 0f;
		float arg2 = 0f;
		try
		{
			arg = (float)Convert.ToDouble(this.FromField.text.Replace(",", string.Empty));
			arg2 = (float)Convert.ToDouble(this.ToField.text.Replace(",", string.Empty));
		}
		catch (Exception)
		{
			flag = false;
		}
		if (flag)
		{
			this.WhenDone(arg, arg2);
		}
		this.Window.Close();
	}

	public InputField FromField;

	public InputField ToField;

	public GUIWindow Window;

	public Action<float, float> WhenDone;
}
