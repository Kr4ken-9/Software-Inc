﻿using System;
using UnityEngine;

public class LerpMove : MonoBehaviour
{
	private void Start()
	{
		base.transform.position = this.Positions[this.index];
		base.transform.rotation = Quaternion.Euler(this.Rotations[this.index]);
		base.GetComponent<Camera>().farClipPlane = this.Far[this.index];
		this.next = this.Times[this.index];
		this.index = 0;
	}

	private void Update()
	{
		if (this.index == this.Positions.Length - 1)
		{
			return;
		}
		float t = this.AC.Evaluate(1f - this.next / this.Times[this.index]);
		base.transform.position = Vector3.Lerp(this.Positions[this.index], this.Positions[this.index + 1], t);
		base.transform.rotation = Quaternion.Lerp(Quaternion.Euler(this.Rotations[this.index]), Quaternion.Euler(this.Rotations[this.index + 1]), t);
		base.GetComponent<Camera>().farClipPlane = Mathf.Lerp(this.Far[this.index], this.Far[this.index + 1], t);
		this.next -= Time.deltaTime;
		if (this.next <= 0f)
		{
			this.index++;
			base.transform.position = this.Positions[this.index];
			base.transform.rotation = Quaternion.Euler(this.Rotations[this.index]);
			base.GetComponent<Camera>().farClipPlane = this.Far[this.index];
			this.next = this.Times[this.index];
		}
	}

	public Vector3[] Positions;

	public Vector3[] Rotations;

	public float[] Far;

	public float[] Times;

	public AnimationCurve AC;

	public float next;

	public int index;
}
