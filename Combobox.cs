﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Combobox<T>
{
	public Combobox(Rect loc, T[] items, string defaultstr, GUISkin guiskin)
	{
		this.Location = loc;
		this.Items.AddRange(items);
		this.Default = defaultstr;
		this.GUIskin = guiskin;
	}

	public T SelectedItem
	{
		get
		{
			return (this.selected != -1) ? this.Items[this.selected] : default(T);
		}
		set
		{
			this.Selected = this.Items.IndexOf(value);
		}
	}

	public int Selected
	{
		get
		{
			return this.selected;
		}
		set
		{
			int num = this.selected;
			this.selected = Mathf.Min(this.Items.Count - 1, Mathf.Max(-1, value));
			if (this.OnSelectedChanged != null && num != value)
			{
				this.OnSelectedChanged();
			}
		}
	}

	public bool Draw()
	{
		bool result = false;
		bool flag = (float)(this.Items.Count * 24) > this.Height;
		int num = (int)Mathf.Floor(this.Height / 24f);
		if (this.Open)
		{
			bool flag2 = Input.GetMouseButtonUp(0);
			Vector2 mousePosition = Event.current.mousePosition;
			for (int i = 0; i < num; i++)
			{
				if (i + this.scroll >= this.Items.Count)
				{
					break;
				}
				Rect position = new Rect(this.Location.x, this.Location.y + this.Location.height + (float)(i * 24), this.Location.width - (float)((!flag) ? 0 : 14), 24f);
				GUI.Box(position, this.ToText(this.Items[i + this.scroll]), this.GUIskin.button);
				if (Input.GetMouseButtonUp(0) && position.Contains(mousePosition))
				{
					this.Selected = i + this.scroll;
					result = true;
				}
			}
			if (flag)
			{
				Rect position2 = new Rect(this.Location.x + this.Location.width - 14f, this.Location.y + 24f, 24f, this.Height);
				GUI.Box(position2, string.Empty, this.GUIskin.verticalScrollbar);
				float num2 = this.Height / (float)(this.Items.Count - num + 1);
				Rect position3 = new Rect(position2.x, Mathf.Max(16f, position2.y + (float)this.scroll / (float)(this.Items.Count - num + 1) * this.Height), position2.width, num2);
				GUI.Box(position3, string.Empty, this.GUIskin.verticalScrollbarThumb);
				if (Input.GetMouseButton(0) && position2.Contains(mousePosition))
				{
					this.scroll = Mathf.Clamp(Mathf.FloorToInt((mousePosition.y - this.Location.y - 24f) / num2), 0, this.Items.Count - num);
				}
				if (Input.GetMouseButtonUp(0) && position2.Contains(mousePosition))
				{
					flag2 = false;
				}
			}
			this.Open = (this.hasClicked || !flag2);
		}
		if (this.hasClicked && !Input.GetMouseButton(0) && !Input.GetMouseButtonUp(0))
		{
			this.hasClicked = false;
		}
		if (GUI.Button(this.Location, this.GetText(), this.GUIskin.button))
		{
			this.Open = !this.Open;
			this.hasClicked = true;
		}
		return result;
	}

	private string ToText(T item)
	{
		if (this.Translate != null)
		{
			return this.Translate(item);
		}
		return item.ToString();
	}

	public string GetText()
	{
		return (this.Selected != -1) ? this.ToText(this.Items[this.Selected]) : this.Default;
	}

	public Rect Location;

	public List<T> Items = new List<T>();

	public string Default;

	public bool Open;

	public bool hasClicked;

	public int scroll;

	public float Height = 192f;

	private int selected = -1;

	private GUISkin GUIskin;

	public Action OnSelectedChanged;

	public Func<T, string> Translate;
}
