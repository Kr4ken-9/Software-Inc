﻿using System;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class GUILoading : MonoBehaviour
{
	private void OnDestroy()
	{
		if (GUILoading.Instance == this)
		{
			GUILoading.Instance = null;
		}
	}

	public static void SetState(bool active)
	{
		if (GUILoading.Instance != null)
		{
			GUILoading.Instance.gameObject.SetActive(active);
		}
	}

	private void OnEnable()
	{
		this._rot = 0f;
		this.BlurScript.enabled = true;
		WindowManager.DisableAll(true, null);
	}

	private void OnDisable()
	{
		if (this.BlurScript != null && !GameSettings.IsQuitting)
		{
			this.BlurScript.enabled = false;
			WindowManager.EnableAll();
		}
	}

	private void Awake()
	{
		GUILoading.Instance = this;
		base.gameObject.SetActive(false);
	}

	private void Update()
	{
		this._rot += -Time.deltaTime * 100f;
		base.transform.rotation = Quaternion.Euler(0f, 0f, (float)Mathf.RoundToInt(this._rot));
	}

	public BlurOptimized BlurScript;

	public static GUILoading Instance;

	private float _rot;
}
