﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BusScript : MonoBehaviour
{
	public void Reset()
	{
		this.Wait = -1f;
		this.WaitingToFill = false;
	}

	public void Init()
	{
		this.Car.CurrentSpeed = this.Car.Speed;
		List<Actor> list = GameSettings.Instance.sActorManager.ReadyForBus.Take(this.Car.Capacity).ToList<Actor>();
		if (list.Count > 0)
		{
			list.ForEach(delegate(Actor x)
			{
				GameSettings.Instance.sActorManager.ReadyForBus.Remove(x);
			});
		}
		this.Car.CanDestroy = (list.Count == 0);
		for (int i = 0; i < list.Count; i++)
		{
			Actor actor = list[i];
			this.Car.AddOccupant(actor, true);
		}
		base.transform.position = new Vector3(6f, 0f, -4f);
	}

	private void Update()
	{
		if (this.Car.WaitingFor.Count > 0)
		{
			return;
		}
		if (this.Wait == -1f)
		{
			if (this.WaitingToFill)
			{
				if (!this.Input.AnyActive())
				{
					this.Input.Occupants.Clear();
					this.WaitingToFill = false;
					this.Wait = 1f;
				}
				else
				{
					this.Car.CurrentSpeed = 0f;
				}
			}
			else
			{
				float num = GameSettings.Instance.BusStopSign.transform.position.z - 2f;
				float num2 = Mathf.Clamp(num - base.transform.position.z, 0f, 3f) / 3f;
				this.Car.CurrentSpeed = this.Car.Speed * Mathf.Max(0.01f, num2);
				if (num2 == 0f)
				{
					this.Car.PlaySFX(this.BusStopSound);
					this.Car.CurrentSpeed = 0f;
					this.Car.AudioE = false;
					base.transform.position = new Vector3(base.transform.position.x, base.transform.position.y, num);
					this.Car.BeginSpawn();
					if (GameSettings.Instance.sActorManager.ReadyForHome.Count > 0)
					{
						int num3 = 0;
						foreach (Actor actor in from x in GameSettings.Instance.sActorManager.ReadyForHome
						where x != null
						select x)
						{
							actor.CurrentPathNode = 0;
							actor.PathProg = 0f;
							actor.CurrentPath = new List<Vector3>
							{
								actor.transform.position,
								this.Input.transform.position
							};
							this.Input.Occupants.Add(actor);
							actor.MyCar = this.Car;
							actor.CarSpawnID = this.Input.ID;
							num3++;
							if (num3 == this.Car.Capacity)
							{
								break;
							}
						}
						this.Input.OpenDoor();
						this.WaitingToFill = true;
					}
					else
					{
						this.Wait = 2f;
					}
				}
			}
		}
		else
		{
			if (this.AllOut() && this.Wait > 0f)
			{
				this.Car.CurrentSpeed = 0f;
				this.Wait -= Time.deltaTime * GameSettings.GameSpeed;
			}
			else if (!this.IsSpawning())
			{
				this.Car.BeginSpawn();
			}
			if (this.Wait <= 0f)
			{
				this.Input.CloseDoor();
				this.Car.CurrentSpeed = Mathf.Lerp(this.Car.CurrentSpeed, this.Car.Speed, Time.deltaTime * GameSettings.GameSpeed * this.Car.Speed);
				this.Car.AudioE = true;
			}
		}
		if (base.transform.position.z > 260f)
		{
			BusScript.Present = false;
			RoadManager.Instance.DestroyCar(this.Car);
		}
		base.transform.position = base.transform.position + base.transform.rotation * Vector3.forward * this.Car.CurrentSpeed * Time.deltaTime * GameSettings.GameSpeed;
	}

	public bool IsSpawning()
	{
		for (int i = 0; i < this.Car.SpawnPoints.Length; i++)
		{
			CarSpawn carSpawn = this.Car.SpawnPoints[i];
			if (!(carSpawn == this.Input))
			{
				if (carSpawn.isSpawning)
				{
					return true;
				}
			}
		}
		return false;
	}

	public bool AllOut()
	{
		for (int i = 0; i < this.Car.SpawnPoints.Length; i++)
		{
			CarSpawn carSpawn = this.Car.SpawnPoints[i];
			if (!(carSpawn == this.Input))
			{
				foreach (Actor actor in carSpawn.Occupants)
				{
					if (actor != null && !actor.isActiveAndEnabled)
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	public void Serialize(WriteDictionary dict)
	{
		dict["Wait"] = this.Wait;
		dict["WaitingToFill"] = this.WaitingToFill;
	}

	public void Deserialize(WriteDictionary dict)
	{
		this.Wait = dict.Get<float>("Wait", -1f);
		this.WaitingToFill = dict.Get<bool>("WaitingToFill", false);
		foreach (Actor actor in this.Input.Occupants)
		{
			actor.MyCar = this.Car;
			actor.CarSpawnID = this.Input.ID;
		}
		BusScript.Present = true;
	}

	public float Wait = -1f;

	public CarScript Car;

	public static bool Present;

	private bool WaitingToFill;

	public CarSpawn Input;

	public AudioClip BusStopSound;
}
