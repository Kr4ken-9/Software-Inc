﻿using System;
using System.Collections.Generic;

public class NameRedirection : Attribute
{
	public NameRedirection(params string[] oldNames)
	{
		this.OldNames.AddRange(oldNames);
	}

	public HashSet<string> OldNames = new HashSet<string>();
}
