﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface IStylable
{
	List<ActorBodyItem> BodyItems { get; set; }

	Transform GetTransform();

	void UpdateEyes();

	void UpdateHairColor(Color col);

	void UpdateSkinColor(Color col);

	void PostUpdate();

	void SetLOD2Color(string part, Color col);
}
