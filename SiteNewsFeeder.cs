﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SiteNewsFeeder : MonoBehaviour
{
	public static void AbortIfActive()
	{
		if (SiteNewsFeeder.newsThread != null)
		{
			SiteNewsFeeder.newsThread.Abort();
			SiteNewsFeeder.newsThread = null;
		}
	}

	private void Update()
	{
		if (this.Countdown > 0f && !SiteNewsFeeder._hasResult)
		{
			this.Countdown -= Time.deltaTime;
			if (this.Countdown <= 0f)
			{
				base.StartCoroutine(this.FetchNews2());
			}
		}
		if (!this.loaded && SiteNewsFeeder._hasResult)
		{
			this.loaded = true;
			RectTransform component = base.GetComponent<RectTransform>();
			Vector2 anchoredPosition = component.anchoredPosition;
			component.anchoredPosition = new Vector2(-component.sizeDelta.x, component.anchoredPosition.y);
			ShortcutExtensions46.DOAnchorPos(component, anchoredPosition, 0.5f, false);
			CanvasGroup cg = base.GetComponent<CanvasGroup>();
			DOTween.To(() => cg.alpha, delegate(float x)
			{
				cg.alpha = x;
			}, 1f, 0.5f);
			for (int i = 0; i < SiteNewsFeeder._result.Length; i++)
			{
				this.AddArticle(SiteNewsFeeder._result[i][0], SiteNewsFeeder._result[i][1], SiteNewsFeeder._result[i][2]);
			}
		}
	}

	private IEnumerator FetchNews2()
	{
		WWW job = new WWW("https://SoftwareInc.Coredumping.com/NewsFeed.php");
		yield return job;
		if (string.IsNullOrEmpty(job.error))
		{
			string[] array = job.text.Split(new string[]
			{
				"||"
			}, StringSplitOptions.RemoveEmptyEntries);
			List<string[]> list = new List<string[]>();
			for (int i = 0; i < array.Length; i += 3)
			{
				if (i + 1 >= array.Length || i + 2 >= array.Length)
				{
					break;
				}
				List<string[]> list2 = list;
				DateTime dateTime;
				string[] item;
				if (DateTime.TryParseExact(array[i + 2], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
				{
					string[] array2 = new string[3];
					array2[0] = array[i];
					array2[1] = array[i + 1];
					item = array2;
					array2[2] = dateTime.ToString("yyy-MM-dd");
				}
				else
				{
					string[] array3 = new string[3];
					array3[0] = array[i];
					array3[1] = array[i + 1];
					item = array3;
					array3[2] = "N/A";
				}
				list2.Add(item);
			}
			SiteNewsFeeder._result = list.ToArray();
			SiteNewsFeeder._hasResult = true;
		}
		yield break;
	}

	private void FetchNews()
	{
		try
		{
			WebRequest webRequest = WebRequest.Create("https://SoftwareInc.Coredumping.com/NewsFeed.php");
			webRequest.Timeout = 2000;
			WebResponse response = webRequest.GetResponse();
			Stream responseStream = response.GetResponseStream();
			if (responseStream != null)
			{
				StreamReader streamReader = new StreamReader(responseStream);
				string text = streamReader.ReadToEnd();
				string[] array = text.Split(new string[]
				{
					"||"
				}, StringSplitOptions.RemoveEmptyEntries);
				List<string[]> list = new List<string[]>();
				for (int i = 0; i < array.Length; i += 3)
				{
					if (i + 1 >= array.Length || i + 2 >= array.Length)
					{
						break;
					}
					List<string[]> list2 = list;
					DateTime dateTime;
					string[] item;
					if (DateTime.TryParseExact(array[i + 2], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
					{
						string[] array2 = new string[3];
						array2[0] = array[i];
						array2[1] = array[i + 1];
						item = array2;
						array2[2] = dateTime.ToString("yyy-MM-dd");
					}
					else
					{
						string[] array3 = new string[3];
						array3[0] = array[i];
						array3[1] = array[i + 1];
						item = array3;
						array3[2] = "N/A";
					}
					list2.Add(item);
				}
				SiteNewsFeeder._result = list.ToArray();
				streamReader.Close();
			}
			response.Close();
			SiteNewsFeeder._hasResult = true;
			SiteNewsFeeder.newsThread = null;
		}
		catch (Exception ex)
		{
			SiteNewsFeeder.newsThread = null;
		}
		SiteNewsFeeder.newsThread = null;
	}

	private void AddArticle(string title, string content, string date)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.SplitterPrefab);
		gameObject.transform.SetParent(this.ContentPanel.transform, false);
		GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.TitlePrefab);
		gameObject2.GetComponent<Text>().text = title;
		gameObject2.transform.SetParent(this.ContentPanel.transform, false);
		GameObject gameObject3 = UnityEngine.Object.Instantiate<GameObject>(this.DatePrefab);
		gameObject3.GetComponent<Text>().text = date;
		gameObject3.transform.SetParent(this.ContentPanel.transform, false);
		GameObject gameObject4 = UnityEngine.Object.Instantiate<GameObject>(this.ContentPrefab);
		gameObject4.GetComponent<Text>().text = content;
		gameObject4.transform.SetParent(this.ContentPanel.transform, false);
	}

	public GameObject TitlePrefab;

	public GameObject ContentPrefab;

	public GameObject SplitterPrefab;

	public GameObject DatePrefab;

	public GameObject ContentPanel;

	public float Countdown = 0.5f;

	private static string[][] _result;

	private static bool _hasResult;

	private bool loaded;

	public static Thread newsThread;
}
