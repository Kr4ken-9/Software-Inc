﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PosNegBar : MaskableGraphic
{
	public override Texture mainTexture
	{
		get
		{
			return this.IconTexture;
		}
	}

	public float Positive
	{
		get
		{
			return this._positive;
		}
		set
		{
			this._positive = value;
			this.SetVerticesDirty();
		}
	}

	public float Negative
	{
		get
		{
			return this._negative;
		}
		set
		{
			this._negative = value;
			this.SetVerticesDirty();
		}
	}

	public void SetValues(float positive, float negative)
	{
		this._positive = positive;
		this._negative = negative;
		this.SetVerticesDirty();
	}

	protected override void OnPopulateMesh(VertexHelper vh)
	{
		vh.Clear();
		if (this.Positive <= 0f && this.Negative <= 0f)
		{
			return;
		}
		Vector2 a = Vector2.zero;
		Vector2 a2 = Vector2.zero;
		a.x = 0f;
		a.y = 0f;
		a2.x = 1f;
		a2.y = 1f;
		a.x -= base.rectTransform.pivot.x;
		a.y -= base.rectTransform.pivot.y;
		a2.x -= base.rectTransform.pivot.x;
		a2.y -= base.rectTransform.pivot.y;
		a.x *= base.rectTransform.rect.width;
		a.y *= base.rectTransform.rect.height;
		a2.x *= base.rectTransform.rect.width;
		a2.y *= base.rectTransform.rect.height;
		a += new Vector2((float)this.Padding.left, (float)this.Padding.bottom);
		a2 -= new Vector2((float)this.Padding.right, (float)this.Padding.top);
		if (this.FromCenter)
		{
			float num = (a.x + a2.x) / 2f;
			int num2 = Mathf.CeilToInt(this.Positive);
			for (int i = 0; i < num2; i++)
			{
				float alpha = 1f;
				if (i == num2 - 1)
				{
					alpha = 1f - ((float)num2 - this.Positive);
				}
				this.DrawIcon(num + (float)i * (this.IconSize.x + this.IconSpacing), a2.y, this.PositiveColor, alpha, false, vh);
			}
			num2 = Mathf.CeilToInt(this.Negative);
			for (int j = 0; j < num2; j++)
			{
				float alpha2 = 1f;
				if (j == num2 - 1)
				{
					alpha2 = 1f - ((float)num2 - this.Negative);
				}
				this.DrawIcon(num - (float)(j + 1) * (this.IconSize.x + this.IconSpacing), a2.y, this.NegativeColor, alpha2, true, vh);
			}
		}
		else
		{
			int num3 = Mathf.CeilToInt(this.Positive);
			for (int k = 0; k < num3; k++)
			{
				float alpha3 = 1f;
				if (k == num3 - 1)
				{
					alpha3 = 1f - ((float)num3 - this.Positive);
				}
				this.DrawIcon(a2.x - (float)(k + 1) * (this.IconSize.x + this.IconSpacing), a2.y, this.PositiveColor, alpha3, false, vh);
			}
			num3 = Mathf.CeilToInt(this.Negative);
			for (int l = 0; l < num3; l++)
			{
				float alpha4 = 1f;
				if (l == num3 - 1)
				{
					alpha4 = 1f - ((float)num3 - this.Negative);
				}
				this.DrawIcon(a.x + (float)l * (this.IconSize.x + this.IconSpacing), a2.y, this.NegativeColor, alpha4, true, vh);
			}
		}
	}

	private void DrawIcon(float x, float y, Color col, float alpha, bool leftToRight, VertexHelper vh)
	{
		Color c = (col * this.color).Alpha(alpha);
		int num = (!leftToRight) ? 1 : 0;
		int num2 = (!leftToRight) ? 0 : 1;
		vh.AddUIVertexQuad(new UIVertex[]
		{
			new UIVertex
			{
				position = new Vector3(x, y, 0f),
				uv0 = new Vector2((float)num, 1f),
				color = c
			},
			new UIVertex
			{
				position = new Vector3(x + this.IconSize.x, y, 0f),
				uv0 = new Vector2((float)num2, 1f),
				color = c
			},
			new UIVertex
			{
				position = new Vector3(x + this.IconSize.x, y - this.IconSize.y, 0f),
				uv0 = new Vector2((float)num2, 0f),
				color = c
			},
			new UIVertex
			{
				position = new Vector3(x, y - this.IconSize.y, 0f),
				uv0 = new Vector2((float)num, 0f),
				color = c
			}
		});
	}

	public Vector2 IconSize;

	public Color PositiveColor;

	public Color NegativeColor;

	public bool FromCenter = true;

	public Texture IconTexture;

	public RectOffset Padding;

	public float IconSpacing;

	private float _positive = 3.5f;

	private float _negative = 3.5f;
}
