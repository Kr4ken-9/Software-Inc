﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActiveFurnDebug : MonoBehaviour
{
	public static void Activate(bool en)
	{
		if (ActiveFurnDebug.Instance != null)
		{
			ActiveFurnDebug.Instance.enabled = en;
		}
	}

	private void Awake()
	{
		ActiveFurnDebug.Instance = this;
	}

	private void OnDestroy()
	{
		ActiveFurnDebug.Instance = null;
	}

	private void OnRenderObject()
	{
		List<Furniture> list = SelectorController.Instance.Selected.OfType<Furniture>().ToList<Furniture>();
		if (list.Count > 0)
		{
			this.mat.SetPass(0);
			GL.Begin(7);
			for (int i = 0; i < list.Count; i++)
			{
				Furniture furniture = list[i];
				if (furniture.FinalBoundary != null && furniture.FinalBoundary.Length > 1)
				{
					this.DrawBounds(furniture.FinalBoundary, (float)(furniture.Parent.Floor * 2) + furniture.Height1, (float)(furniture.Parent.Floor * 2) + furniture.Height2, this.Build1, this.Build2);
				}
				if (furniture.FinalNav != null && furniture.FinalNav.Length > 0)
				{
					this.DrawBounds(furniture.FinalNav, (float)(furniture.Parent.Floor * 2) - 0.1f, (float)(furniture.Parent.Floor * 2) - 0.05f, this.Nav1, this.Nav2);
				}
				for (int j = 0; j < furniture.SnapPoints.Length; j++)
				{
					SnapPoint snapPoint = furniture.SnapPoints[j];
					this.DrawLine(snapPoint.transform.position, snapPoint.transform.position + snapPoint.transform.up.normalized * 0.2f, this.Snap, 0.015f);
					this.DrawLine(snapPoint.transform.position - snapPoint.transform.right.normalized * 0.1f, snapPoint.transform.position + snapPoint.transform.right.normalized * 0.1f, this.Snap, 0.015f);
					this.DrawLine(snapPoint.transform.position, snapPoint.transform.position + snapPoint.transform.forward.normalized * 0.3f, this.Snap, 0.015f);
				}
				for (int k = 0; k < furniture.InteractionPoints.Length; k++)
				{
					InteractionPoint interactionPoint = furniture.InteractionPoints[k];
					this.DrawLine(interactionPoint.transform.position, interactionPoint.transform.position + interactionPoint.transform.up.normalized * 0.2f, this.Use, 0.015f);
					this.DrawLine(interactionPoint.transform.position - interactionPoint.transform.right.normalized * 0.1f, interactionPoint.transform.position + interactionPoint.transform.right.normalized * 0.1f, this.Use, 0.015f);
					this.DrawLine(interactionPoint.transform.position, interactionPoint.transform.position + interactionPoint.transform.forward.normalized * 0.3f, this.Use, 0.015f);
				}
			}
			GL.End();
		}
	}

	private void DrawBounds(Vector2[] bounds, float h1, float h2, Color c1, Color c2)
	{
		for (int i = 0; i < bounds.Length; i++)
		{
			Color col = Color.Lerp(c1, c2, (float)i / (float)(bounds.Length - 1));
			Vector2 vector = bounds[i];
			Vector2 vector2 = bounds[(i + 1) % bounds.Length];
			this.DrawLine(new Vector3(vector.x, h1, vector.y), new Vector3(vector.x, h2, vector.y), col, 0.015f);
			this.DrawLine(new Vector3(vector.x, h1, vector.y), new Vector3(vector2.x, h1, vector2.y), col, 0.015f);
			this.DrawLine(new Vector3(vector.x, h2, vector.y), new Vector3(vector2.x, h2, vector2.y), col, 0.015f);
		}
	}

	public void DrawLine(Vector3 p1, Vector3 p2, Color col, float width = 0.015f)
	{
		GL.Color(col);
		Vector3 b = Vector3.Cross(p1 - p2, p1 - base.transform.position).normalized * width / 2f;
		GL.Vertex(p1 + b);
		GL.Vertex(p2 + b);
		GL.Vertex(p2 - b);
		GL.Vertex(p1 - b);
	}

	public static ActiveFurnDebug Instance;

	public Material mat;

	public Color Build1;

	public Color Build2;

	public Color Nav1;

	public Color Nav2;

	public Color Snap;

	public Color Use;
}
