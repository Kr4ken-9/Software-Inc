﻿using System;
using System.ComponentModel;
using UnityEngine;

public abstract class ModBehaviour : MonoBehaviour
{
	public abstract void OnDeactivate();

	public abstract void OnActivate();

	public void SaveSetting(string settingName, string value)
	{
		this.ParentMod.SaveSetting(settingName, value);
	}

	public T LoadSetting<T>(string settingName, T defaultValue)
	{
		string o;
		if (this.ParentMod.Settings.TryGetValue(settingName, out o))
		{
			try
			{
				return (T)((object)TypeDescriptor.GetConverter(typeof(T)).ConvertFrom(o));
			}
			catch (Exception)
			{
				return defaultValue;
			}
			return defaultValue;
		}
		return defaultValue;
	}

	public ModController.DLLMod ParentMod;
}
