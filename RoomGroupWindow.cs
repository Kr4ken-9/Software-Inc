﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomGroupWindow : MonoBehaviour
{
	public void Toggle()
	{
		this.Window.Toggle(false);
	}

	public void Show(List<Room> rooms)
	{
		WindowManager.SpawnInputDialog("NewRoomGroupPrompt".Loc(), string.Empty, "New room group".Loc(), delegate(string s)
		{
			if (GameSettings.Instance.GetRoomGroups().Any((string x) => x.Equals(s)))
			{
				WindowManager.Instance.ShowMessageBox("RoomGroupNameError".Loc(), true, DialogWindow.DialogType.Error);
			}
			else
			{
				RoomGroup roomGroup = GameSettings.Instance.AddRoomGroup(s);
				foreach (Room room in rooms)
				{
					roomGroup.AddRoom(room);
				}
				this.Window.Show();
			}
		}, null);
	}

	public void UpdateList()
	{
		this.GroupList.Items = GameSettings.Instance.GetUnderlyingRoomGroups().Cast<object>().ToList<object>();
	}

	public void ToggleRoomGroupOverlay()
	{
		DataOverlay.Instance.ActivateFunc((!DataOverlay.HasActive) ? "Room grouping" : null);
	}

	public void AssignSelectedRooms()
	{
		RoomGroup[] selected = this.GroupList.GetSelected<RoomGroup>();
		List<Room> list = SelectorController.Instance.Selected.OfType<Room>().ToList<Room>();
		if (selected.Length > 0 && list.Count > 0)
		{
			foreach (Room room in list)
			{
				GameSettings.Instance.RemoveRoomFromGroups(room);
				selected[0].AddRoom(room);
			}
		}
	}

	public void AddNewRoom()
	{
		WindowManager.SpawnInputDialog("NewRoomGroupPrompt".Loc(), string.Empty, "New room group".Loc(), delegate(string s)
		{
			if (GameSettings.Instance.GetRoomGroups().Any((string x) => x.Equals(s)))
			{
				WindowManager.Instance.ShowMessageBox("RoomGroupNameError".Loc(), true, DialogWindow.DialogType.Error);
			}
			else
			{
				GameSettings.Instance.AddRoomGroup(s);
			}
		}, null);
	}

	public GUIWindow Window;

	public GUIListView GroupList;
}
