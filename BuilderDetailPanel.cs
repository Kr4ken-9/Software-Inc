﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BuilderDetailPanel : MonoBehaviour
{
	public void Disable()
	{
		if (!this.IsStatic)
		{
			this.dTween = TweenSettingsExtensions.OnComplete<Tweener>(ShortcutExtensions46.DOFade(this.AlphaGroup, 0f, 0.2f), delegate
			{
				this.dTween = null;
				base.gameObject.SetActive(false);
			});
		}
	}

	private void EnableMe()
	{
		if (!this.IsStatic)
		{
			if (this.dTween != null)
			{
				TweenExtensions.Kill(this.dTween, false);
				this.dTween = null;
			}
			base.gameObject.SetActive(true);
			if (this.AlphaGroup.alpha < 1f)
			{
				ShortcutExtensions46.DOFade(this.AlphaGroup, 1f, 0.2f);
			}
		}
	}

	private void DestroyProgressBars()
	{
		foreach (GameObject obj in this.ProgressBars)
		{
			UnityEngine.Object.Destroy(obj);
		}
		this.ProgressBars.Clear();
		this._hasAddedBonusPanel = false;
		this._hasAddedConsPanel = false;
	}

	public void SetGeneric(string name, string description, Sprite thumbnail, float price)
	{
		this.DestroyProgressBars();
		this.EnableMe();
		this.Name.text = name;
		this.Description.text = description;
		this.Thumbnail.sprite = thumbnail;
		this.Price.text = ((price <= 0f) ? string.Empty : price.Currency(true));
		this.UnlockPanel.SetActive(false);
		this.UpdateLayout();
	}

	public void SetFurniture(Furniture furn)
	{
		List<Furniture> source = (from x in ObjectDatabase.Instance.GetAllFurniture()
		select x.GetComponent<Furniture>()).ToList<Furniture>();
		this.DestroyProgressBars();
		this.EnableMe();
		string[] furniture = Localization.GetFurniture(furn.name, furn.ButtonDescription);
		this.Name.text = furniture[0];
		this.Description.text = furniture[1];
		this.Thumbnail.sprite = furn.Thumbnail;
		this.Price.text = furn.Cost.Currency(true);
		if (furn.Type.Equals("Coffee"))
		{
			float num = source.Max((Furniture x) => x.Coffee);
			this.AddProgressBar(this.BaseDetailPanel, "Quality".Loc(), furn.Coffee / num, false, false, true);
		}
		if (furn.Type.Equals("Computer"))
		{
			float num2 = source.Max((Furniture x) => x.ComputerPower);
			this.AddProgressBar(this.BaseDetailPanel, "Power".Loc(), furn.ComputerPower / num2, false, false, true);
		}
		if (furn.Type.Equals("Server"))
		{
			Server component = furn.GetComponent<Server>();
			if (component != null)
			{
				float num3 = (from x in source
				select x.GetComponent<Server>() into x
				where x != null
				select x).Max((Server x) => x.Power);
				this.AddProgressBar(this.BaseDetailPanel, "Bandwidth".Loc() + ": " + component.Power.Bandwidth(), Mathf.Sqrt(component.Power / num3), false, false, true);
			}
		}
		if (furn.Type.Equals("ProductPrinter"))
		{
			ProductPrinter component2 = furn.GetComponent<ProductPrinter>();
			if (component2 != null)
			{
				float num4 = (from x in source
				select x.GetComponent<ProductPrinter>() into x
				where x != null
				select x).Max((ProductPrinter x) => x.PrintSpeed * (float)x.PrintAmount);
				float num5 = component2.PrintSpeed * (float)component2.PrintAmount;
				this.AddProgressBar(this.BaseDetailPanel, "CopyPerMonth".Loc(new object[]
				{
					num5 * 24f
				}), num5 / num4, false, false, true);
				float num6 = (float)(from x in source
				select x.GetComponent<ProductPrinter>() into x
				where x != null
				select x).Max((ProductPrinter x) => x.PrintAmount);
				this.AddProgressBar(this.BaseDetailPanel, "CopyPerBox".Loc(new object[]
				{
					component2.PrintAmount
				}), (float)component2.PrintAmount / num6, false, false, true);
				float num7 = (from x in source
				select x.GetComponent<ProductPrinter>() into x
				where x != null
				select x).Max((ProductPrinter x) => x.PrintPrice);
				this.AddProgressBar(this.ConsumptionPanel, "PricePerCopy".Loc(new object[]
				{
					component2.PrintPrice.Currency(true)
				}), component2.PrintPrice / num7, false, true, true);
			}
		}
		if (furn.HeatCoolPotential != 0f)
		{
			float num8 = source.Max((Furniture x) => x.HeatCoolArea);
			this.AddProgressBar(this.BaseDetailPanel, string.Concat(new object[]
			{
				"Area".Loc(),
				": ",
				furn.HeatCoolArea,
				" m2"
			}), Mathf.Sqrt(furn.HeatCoolArea / num8), false, false, true);
			float num9;
			if (furn.HeatCoolPotential < 0f)
			{
				num9 = source.Max((Furniture x) => -x.HeatCoolPotential);
			}
			else
			{
				num9 = source.Max((Furniture x) => x.HeatCoolPotential);
			}
			string str = furn.HeatCoolPotential.Temperature(true);
			this.AddProgressBar(this.BaseDetailPanel, "Power".Loc() + ": " + str, Mathf.Abs(furn.HeatCoolPotential) / num9, false, false, true);
		}
		if (furn.Lighting > 0f)
		{
			float num10 = source.Max((Furniture x) => x.Lighting);
			this.AddProgressBar(this.BaseDetailPanel, "Lighting".Loc(), furn.Lighting / num10, false, false, false);
		}
		if (furn.Environment != 1f)
		{
			if (furn.Environment > 1f)
			{
				float num11 = source.Max((Furniture x) => x.Environment) - 1f;
				this.AddProgressBar(this.BaseDetailPanel, "Environment".Loc(), (furn.Environment - 1f) / num11, true, false, false);
			}
			else
			{
				float num12 = -(source.Min((Furniture x) => x.Environment) - 1f);
				this.AddProgressBar(this.BaseDetailPanel, "Environment".Loc(), (furn.Environment - 1f) / num12, true, false, false);
			}
		}
		if (furn.Comfort > 0f)
		{
			float num13 = source.Max((Furniture x) => x.Comfort);
			this.AddProgressBar(this.BaseDetailPanel, "Comfort".Loc(), furn.Comfort / num13, false, false, false);
		}
		if (furn.Noisiness > 0f)
		{
			float num14 = source.Max((Furniture x) => x.Noisiness);
			this.AddProgressBar(this.BaseDetailPanel, "Noise".Loc(), 1f - Mathf.Pow(1f - furn.Noisiness / num14, 3f), false, true, false);
		}
		Upgradable component3 = furn.GetComponent<Upgradable>();
		if (component3 != null)
		{
			float num15 = (from x in source
			where x.Type.Equals(furn.Type)
			select x.GetComponent<Upgradable>() into x
			where x != null
			select x).Max((Upgradable x) => x.TimeToAtrophy);
			this.AddProgressBar(this.ConsumptionPanel, "Durability".Loc() + ": " + "MonthPostfix".LocPlural((int)component3.TimeToAtrophy), component3.TimeToAtrophy / num15, false, false, false);
		}
		if (furn.Water > 0f)
		{
			float num16 = (from x in source
			where x.Water > 0f
			select x).Max((Furniture x) => x.Water);
			this.AddProgressBar(this.ConsumptionPanel, "Water".Loc(), furn.Water / num16, false, true, false);
		}
		if (furn.Wattage > 0f)
		{
			float num17 = (from x in source
			where x.Wattage > 0f
			select x).Max((Furniture x) => x.Wattage);
			this.AddProgressBar(this.ConsumptionPanel, "Electricity".Loc(), furn.Wattage / num17, false, true, false);
		}
		if (!furn.IsPlayerControlled())
		{
			this.UnlockPanel.SetActive(true);
			this.Unlock.text = "Landlord".Loc();
		}
		else if (GameSettings.Instance.EditMode || TimeOfDay.Instance.Year + SDateTime.BaseYear >= furn.UnlockYear)
		{
			this.UnlockPanel.SetActive(false);
		}
		else
		{
			this.UnlockPanel.SetActive(true);
			this.Unlock.text = furn.UnlockYear.ToString();
		}
		if (furn.Type.Equals("PCAddon") && furn.RoleBuffs != null)
		{
			this.BonusTitle.text = "Work boost".Loc() + ":";
			for (int i = 0; i < furn.RoleBuffs.Length; i++)
			{
				GameObject bonusDetailPanel = this.BonusDetailPanel;
				Employee.EmployeeRole employeeRole = (Employee.EmployeeRole)i;
				this.AddProgressBar(bonusDetailPanel, employeeRole.ToString().Loc(), furn.RoleBuffs[i], false, false, true);
			}
		}
		else if (furn.AuraValues != null && furn.AuraValues.Length > 0)
		{
			this.BonusTitle.text = "Room boost".Loc() + ":";
			for (int j = 0; j < furn.AuraValues.Length; j++)
			{
				if (furn.AuraValues[j] != -1f)
				{
					GameObject bonusDetailPanel2 = this.BonusDetailPanel;
					Furniture.AuraTypes auraTypes = (Furniture.AuraTypes)j;
					this.AddProgressBar(bonusDetailPanel2, auraTypes.ToString().Loc(), furn.AuraValues[j], true, false, false);
				}
			}
		}
		this.UpdateLayout();
	}

	private void UpdateLayout()
	{
		Canvas.ForceUpdateCanvases();
		this.BonusDetailPanel.SetActive(this._hasAddedBonusPanel);
		this.ConsumptionPanel.SetActive(this._hasAddedConsPanel);
		this.RightSidePanel.SetActive(this.BonusDetailPanel.activeSelf || this.ConsumptionPanel.activeSelf);
		if (!this.RightSidePanel.activeSelf && this.BaseDetailPanel.transform.childCount == 0)
		{
			this.BaseDetailPanel.SetActive(false);
			base.GetComponent<RectTransform>().sizeDelta = new Vector2(518f, 108f);
		}
		else
		{
			this.BaseDetailPanel.SetActive(true);
			base.GetComponent<RectTransform>().sizeDelta = new Vector2(518f, 192f);
		}
		Canvas.ForceUpdateCanvases();
		foreach (GUIProgressBar guiprogressBar in from x in this.ProgressBars
		select x.GetComponent<GUIProgressBar>())
		{
			guiprogressBar.Value = guiprogressBar.Value;
		}
	}

	public void AddProgressBar(GameObject panel, string label, float value, bool dbl, bool bad, bool Solid = false)
	{
		if (!this._hasAddedConsPanel && panel == this.ConsumptionPanel)
		{
			this._hasAddedConsPanel = true;
		}
		else if (!this._hasAddedBonusPanel && panel == this.BonusDetailPanel)
		{
			this._hasAddedBonusPanel = true;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>((!dbl) ? this.ProgressBarPrefab : this.DoubleProgressBarPrefab);
		GUIProgressBar component = gameObject.GetComponent<GUIProgressBar>();
		if (bad)
		{
			Color startColor = component.StartColor;
			component.StartColor = component.EndColor;
			component.EndColor = startColor;
		}
		else if (!dbl)
		{
			component.StartColor = component.EndColor;
		}
		if (Solid)
		{
			component.StartColor = component.EndColor;
		}
		gameObject.GetComponentInChildren<Text>().text = label;
		this.ProgressBars.Add(gameObject);
		gameObject.transform.SetParent(panel.transform, false);
		component.Value = value;
	}

	public bool IsStatic;

	public GameObject ProgressBarPrefab;

	public GameObject DoubleProgressBarPrefab;

	public GameObject BaseDetailPanel;

	public GameObject BonusDetailPanel;

	public GameObject RightSidePanel;

	public GameObject ConsumptionPanel;

	public GameObject UnlockPanel;

	public List<GameObject> ProgressBars = new List<GameObject>();

	public Image Thumbnail;

	public CanvasGroup AlphaGroup;

	public Text Name;

	public Text Price;

	public Text Description;

	public Text BonusTitle;

	public Text Unlock;

	[NonSerialized]
	public Tweener dTween;

	[NonSerialized]
	private bool _hasAddedConsPanel;

	[NonSerialized]
	private bool _hasAddedBonusPanel;
}
