﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GradientPanel : MaskableGraphic
{
	protected override void OnPopulateMesh(VertexHelper vh)
	{
		base.OnPopulateMesh(vh);
		vh.Clear();
		if (this.Gradients != null && this.Gradients.Count > 0)
		{
			Vector2 a = new Vector2(-base.rectTransform.pivot.x * base.rectTransform.rect.width, -base.rectTransform.pivot.y * base.rectTransform.rect.height);
			for (int i = 0; i < this.Gradients.Count; i++)
			{
				KeyValuePair<Color, bool> keyValuePair = this.Gradients[i];
				bool flag = i == 0 || !this.Gradients[i - 1].Value;
				bool flag2 = !keyValuePair.Value;
				bool flag3 = i == this.Gradients.Count - 1 || !this.Gradients[i + 1].Value;
				if (flag2 || (flag && flag3))
				{
					this.AddQuad(new Rect(a + new Vector2(0f, (float)i * this.GradientHeight + 1f), new Vector2(base.rectTransform.rect.width, this.GradientHeight - 2f)), keyValuePair.Key, keyValuePair.Key, vh);
				}
				else
				{
					this.AddQuad(new Rect(a + new Vector2(0f, (float)i * this.GradientHeight + (float)((!flag) ? 0 : 1)), new Vector2(base.rectTransform.rect.width, this.GradientHeight / 2f)), keyValuePair.Key, keyValuePair.Key, vh);
					this.AddQuad(new Rect(a + new Vector2(0f, (float)i * this.GradientHeight + this.GradientHeight / 2f), new Vector2(base.rectTransform.rect.width, this.GradientHeight / 2f - (float)((!flag3) ? 0 : 1))), keyValuePair.Key, (!flag3) ? this.Gradients[i + 1].Key : keyValuePair.Key, vh);
				}
			}
		}
	}

	private void AddQuad(Rect pos, Color c1, Color c2, VertexHelper vh)
	{
		vh.AddUIVertexQuad(new UIVertex[]
		{
			new UIVertex
			{
				position = new Vector3(pos.xMin, -pos.yMin, 0f),
				color = c1
			},
			new UIVertex
			{
				position = new Vector3(pos.xMax, -pos.yMin, 0f),
				color = c1
			},
			new UIVertex
			{
				position = new Vector3(pos.xMax, -pos.yMax, 0f),
				color = c2
			},
			new UIVertex
			{
				position = new Vector3(pos.xMin, -pos.yMax, 0f),
				color = c2
			}
		});
	}

	public List<KeyValuePair<Color, bool>> Gradients = new List<KeyValuePair<Color, bool>>();

	public float GradientHeight = 24f;
}
