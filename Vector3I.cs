﻿using System;
using UnityEngine;

[Serializable]
public struct Vector3I
{
	public Vector3I(int xx, int yy, int zz)
	{
		this.x = xx;
		this.y = yy;
		this.z = zz;
	}

	public Vector3I(float xx, float yy, float zz)
	{
		this.x = (int)xx;
		this.y = (int)yy;
		this.z = (int)zz;
	}

	public Vector3I(Vector3 v)
	{
		this.x = Mathf.FloorToInt(v.x);
		this.y = Mathf.RoundToInt(v.y);
		this.z = Mathf.FloorToInt(v.z);
	}

	public Vector3 ToVector3()
	{
		return new Vector3((float)this.x, (float)this.y, (float)this.z);
	}

	public Vector3 ToSwincVector3()
	{
		return new Vector3((float)this.x, (float)(this.y * 2), (float)this.z);
	}

	public Vector2 ToVector2()
	{
		return new Vector2((float)this.x, (float)this.z);
	}

	public static Vector3I operator +(Vector3I a, Vector3I b)
	{
		return new Vector3I(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	public static Vector3I operator -(Vector3I a, Vector3I b)
	{
		return new Vector3I(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	public override bool Equals(object obj)
	{
		if (obj is Vector3I)
		{
			Vector3I vector3I = (Vector3I)obj;
			return vector3I.x == this.x && vector3I.y == this.y && vector3I.z == this.z;
		}
		if (obj is Vector3)
		{
			Vector3 vector = (Vector3)obj;
			return vector.x == (float)this.x && vector.y == (float)this.y && vector.z == (float)this.z;
		}
		return false;
	}

	public override int GetHashCode()
	{
		return base.GetHashCode();
	}

	public int x;

	public int y;

	public int z;
}
