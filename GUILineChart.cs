﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUILineChart : Graphic, IPointerEnterHandler, IEventSystemHandler
{
	public int Highlighted
	{
		get
		{
			return this._highlighted;
		}
		set
		{
			if (this._highlighted != value)
			{
				this._highlighted = value;
				this.UpdateCachedLines();
			}
		}
	}

	public void UpdateCachedLines()
	{
		this.DrawCharts(this.CachedData);
		this.SetVerticesDirty();
	}

	protected override void OnPopulateMesh(VertexHelper h)
	{
		if (!this.Cached)
		{
			this.DrawCharts(this.CachedData);
		}
		if (this.CachedData == null || this.CachedData.Count == 0)
		{
			h.Clear();
			return;
		}
		h.Clear();
		Utilities.VBOToHelper(this.CachedData, h);
	}

	private void DrawCharts(List<UIVertex> vbo)
	{
		if (this.Values == null || this.Values.Count == 0 || this.Colors == null || this.Colors.Count == 0)
		{
			vbo.Clear();
			return;
		}
		Vector2 zero = Vector2.zero;
		Vector2 zero2 = Vector2.zero;
		zero.x = 0f;
		zero.y = 0f;
		zero2.x = 1f;
		zero2.y = 1f;
		zero.x -= base.rectTransform.pivot.x;
		zero.y -= base.rectTransform.pivot.y;
		zero2.x -= base.rectTransform.pivot.x;
		zero2.y -= base.rectTransform.pivot.y;
		zero.x *= base.rectTransform.rect.width;
		zero.y *= base.rectTransform.rect.height;
		zero2.x *= base.rectTransform.rect.width;
		zero2.y *= base.rectTransform.rect.height;
		vbo.Clear();
		int num = (from x in this.Values
		select x.Count).Max() - 1;
		float num2 = (zero2.x - zero.x) / (float)num;
		float num3;
		if (this.ForceLimits)
		{
			num3 = this.LowerLimit;
		}
		else
		{
			num3 = (from x in this.Values
			select (x.Count != 0) ? x.Min() : 0f).Min();
		}
		float num4 = num3;
		float num5;
		if (this.ForceLimits)
		{
			num5 = this.UpperLimit;
		}
		else
		{
			num5 = (from x in this.Values
			select (x.Count != 0) ? x.Max() : 1f).Max();
		}
		float num6 = num5;
		float num7 = Mathf.Max(Mathf.Abs(num4), Mathf.Abs(num6));
		num7 = Mathf.Pow(10f, Mathf.Floor(Mathf.Log10(num7)));
		num4 = num4.CurrencyRoundDownToNearest(num7);
		num6 = num6.CurrencyRoundUpToNearest(num7);
		int num8 = 0;
		if (num > 1 && this.Values[0].Count < 512)
		{
			float num9 = zero2.x - zero.x;
			int num10 = 24;
			float num11 = num9 / (float)num;
			float num12 = Mathf.Floor(Mathf.Max(1f, (float)num10 / num11)) * num11;
			for (float num13 = num12; num13 < num9; num13 += num12)
			{
				GUILineChart.DrawLine(new Vector2(zero.x + num13, zero.y), new Vector2(zero.x + num13, zero2.y), 1f, Color.gray, vbo);
				num8 += 4;
				if (num8 > 40000)
				{
					return;
				}
			}
		}
		for (int i = 0; i < this.Values.Count; i++)
		{
			Color color = (this.Highlighted <= -1 || this.Highlighted == i) ? this.Colors[i % this.Colors.Count] : this.Colors[i % this.Colors.Count].Alpha(0.5f);
			float width = (this.Highlighted != i) ? 2f : 3f;
			List<float> list = this.Values[i];
			if (list.Count != 0)
			{
				int num14 = Mathf.Max(1, Mathf.FloorToInt((float)list.Count / (zero2.x - zero.x)));
				int num15 = 0;
				float x2 = list[0];
				int num16 = 0;
				float num17 = 0f;
				for (int j = 1; j < list.Count; j++)
				{
					if (j % num14 == 0 || j == list.Count - 1)
					{
						float num18 = list[j];
						if (num16 > 0)
						{
							num18 += num17;
							num16++;
							num18 /= (float)num16;
						}
						GUILineChart.DrawLine(new Vector2(zero.x + (float)num15 * num2, x2.MapRange(num4, num6, zero.y, zero2.y, false)), new Vector2(zero.x + (float)j * num2, num18.MapRange(num4, num6, zero.y, zero2.y, false)), width, color, vbo);
						num15 = j;
						x2 = num18;
						num8 += 4;
						num17 = 0f;
						num16 = 0;
						if (num8 > 40000)
						{
							return;
						}
					}
					else
					{
						num17 += list[j];
						num16++;
					}
				}
			}
		}
		if (num6 > 0f && num4 < 0f)
		{
			float y = 0.MapRange(num4, num6, zero.y, zero2.y, false);
			GUILineChart.DrawLine(new Vector2(zero.x, y), new Vector2(zero2.x, y), 2f, Color.white, vbo);
		}
	}

	public static void DrawLine(Vector2 p1, Vector2 p2, float width, Color color, List<UIVertex> vbo, float bottom, float top)
	{
		Vector2 normalized = new Vector2(p1.y - p2.y, p2.x - p1.x);
		normalized = normalized.normalized;
		UIVertex simpleVert = UIVertex.simpleVert;
		simpleVert.position = new Vector2(p1.x + normalized.x * width / 2f, p1.y + normalized.y * width / 2f);
		float num = (simpleVert.position.y - bottom) / (top - bottom);
		num = Mathf.Lerp(0.8f, 1f, 1f - num);
		simpleVert.color = GUILineChart.MultCol(color, num);
		vbo.Add(simpleVert);
		simpleVert.position = new Vector2(p2.x + normalized.x * width / 2f, p2.y + normalized.y * width / 2f);
		num = (simpleVert.position.y - bottom) / (top - bottom);
		num = Mathf.Lerp(0.8f, 1f, 1f - num);
		simpleVert.color = GUILineChart.MultCol(color, num);
		vbo.Add(simpleVert);
		simpleVert.position = new Vector2(p2.x - normalized.x * width / 2f, p2.y - normalized.y * width / 2f);
		num = (simpleVert.position.y - bottom) / (top - bottom);
		num = Mathf.Lerp(0.8f, 1f, 1f - num);
		simpleVert.color = GUILineChart.MultCol(color, num);
		vbo.Add(simpleVert);
		simpleVert.position = new Vector2(p1.x - normalized.x * width / 2f, p1.y - normalized.y * width / 2f);
		num = (simpleVert.position.y - bottom) / (top - bottom);
		num = Mathf.Lerp(0.8f, 1f, 1f - num);
		simpleVert.color = GUILineChart.MultCol(color, num);
		vbo.Add(simpleVert);
	}

	private static Color MultCol(Color c, float t)
	{
		return new Color(c.r * t, c.g * t, c.b * t, c.a);
	}

	public static void DrawLine(Vector2 p1, Vector2 p2, float width, Color color, List<UIVertex> vbo)
	{
		Vector2 normalized = new Vector2(p1.y - p2.y, p2.x - p1.x);
		normalized = normalized.normalized;
		UIVertex simpleVert = UIVertex.simpleVert;
		simpleVert.position = new Vector2(p1.x + normalized.x * width / 2f, p1.y + normalized.y * width / 2f);
		simpleVert.color = color;
		vbo.Add(simpleVert);
		simpleVert.position = new Vector2(p2.x + normalized.x * width / 2f, p2.y + normalized.y * width / 2f);
		simpleVert.color = color;
		vbo.Add(simpleVert);
		simpleVert.position = new Vector2(p2.x - normalized.x * width / 2f, p2.y - normalized.y * width / 2f);
		simpleVert.color = color;
		vbo.Add(simpleVert);
		simpleVert.position = new Vector2(p1.x - normalized.x * width / 2f, p1.y - normalized.y * width / 2f);
		simpleVert.color = color;
		vbo.Add(simpleVert);
	}

	private void Update()
	{
		if (this._toolTip)
		{
			if (Tooltip.IsShowing)
			{
				if (this.Values.Count == 0)
				{
					Tooltip.Hide();
					this._toolTip = false;
					this.Highlighted = -1;
					return;
				}
				Vector2 vector;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(base.rectTransform, Input.mousePosition, null, out vector);
				int num = 0;
				float num2 = float.MaxValue;
				float num3 = 0f;
				int num4 = Mathf.Clamp(Mathf.RoundToInt((vector.x + base.rectTransform.pivot.x * base.rectTransform.rect.width) / base.rectTransform.rect.width * (float)(this.Values[num].Count - 1)), 0, this.Values[num].Count - 1);
				float num5 = (from x in this.Values
				select (x.Count != 0) ? x.Min() : 0f).Min();
				float num6 = (from x in this.Values
				select (x.Count != 0) ? x.Max() : 1f).Max();
				float num7 = Mathf.Max(Mathf.Abs(num5), Mathf.Abs(num6));
				num7 = Mathf.Pow(10f, Mathf.Floor(Mathf.Log10(num7)));
				num5 = num5.CurrencyRoundDownToNearest(num7);
				num6 = num6.CurrencyRoundUpToNearest(num7);
				for (int i = 0; i < this.Values.Count; i++)
				{
					float num8 = this.Values[i][num4].MapRange(num5, num6, -base.rectTransform.rect.height * base.rectTransform.pivot.y, base.rectTransform.rect.height * base.rectTransform.pivot.y, false);
					float num9 = Mathf.Abs(num8 - vector.y);
					if (num9 < num2)
					{
						num = i;
						num2 = num9;
						num3 = num8;
					}
				}
				this.Highlighted = num;
				Vector2 vector2 = new Vector2((float)num4 * (base.rectTransform.rect.width / (float)Mathf.Max(1, this.Values[num].Count - 1)) - base.rectTransform.pivot.x * base.rectTransform.rect.width, num3 + 32f);
				Tooltip.UpdateToolTip(this.ToolTipFunc(num4, this.Values[num][num4]), null, new Vector2(base.rectTransform.position.x / Options.UISize + vector2.x, -((float)Screen.height - base.rectTransform.position.y) / Options.UISize + vector2.y));
			}
			else
			{
				this.Highlighted = -1;
				this._toolTip = false;
			}
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (this.ToolTipFunc != null && this.Values != null)
		{
			if (this.Values.Any((List<float> x) => x.Count > 0))
			{
				this._toolTip = true;
				Tooltip.SetToolTip("0", null, base.rectTransform);
			}
		}
	}

	public List<List<float>> Values = new List<List<float>>
	{
		new List<float>
		{
			-1f,
			8f,
			4f,
			6f
		},
		new List<float>
		{
			-4f,
			3f,
			2f
		}
	};

	public List<Color> Colors = new List<Color>
	{
		Color.white
	};

	public List<UIVertex> CachedData = new List<UIVertex>();

	public bool Cached;

	public Func<int, float, string> ToolTipFunc;

	public bool ForceLimits;

	public float LowerLimit;

	public float UpperLimit = 1f;

	[NonSerialized]
	private int _highlighted = -1;

	private bool _toolTip;
}
