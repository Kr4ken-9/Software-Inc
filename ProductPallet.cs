﻿using System;
using UnityEngine;

public class ProductPallet : Writeable
{
	public Furniture Furn
	{
		get
		{
			if (this._furn == null)
			{
				this._furn = base.GetComponent<Furniture>();
			}
			return this._furn;
		}
	}

	private void Awake()
	{
		if (this.Orders == null)
		{
			this.Orders = new ProductPrintOrder[this.MaxOrders];
		}
		this.MatBlock = new MaterialPropertyBlock();
		this.RefreshBoxes();
	}

	private void Start()
	{
		if (GameSettings.Instance != null)
		{
			TutorialSystem.Instance.StartTutorial("Distribution", false);
		}
	}

	public void AddOrder(ProductPrintOrder order)
	{
		this.Orders[this.CurrentAmount] = order;
		this.CurrentAmount++;
		this.RefreshBoxes();
	}

	private void RefreshBoxes()
	{
		this.MatBlock.SetFloat("_CutOff", this.BoxCutoff[this.CurrentAmount]);
		this.Boxes.SetPropertyBlock(this.MatBlock);
	}

	public ProductPrintOrder Take(out int boxes, int max)
	{
		int num = Mathf.Min(max, this.CurrentAmount);
		ProductPrintOrder[] array = new ProductPrintOrder[num];
		boxes = num;
		for (int i = 0; i < num; i++)
		{
			ProductPrintOrder productPrintOrder = this.Orders[this.CurrentAmount - 1];
			if (productPrintOrder != null)
			{
				array[i] = productPrintOrder;
				this.Orders[this.CurrentAmount - 1] = null;
				this.CurrentAmount--;
			}
		}
		ProductPrintOrder result = ProductPrintOrder.Merge(array);
		this.RefreshBoxes();
		return result;
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		this.Orders = dictionary.Get<ProductPrintOrder[]>("PalletOrders", new ProductPrintOrder[this.MaxOrders]);
		this.CurrentAmount = dictionary.Get<int>("PalletCurrentAmount", 0);
		this.RefreshBoxes();
		return this;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["PalletOrders"] = this.Orders;
		dictionary["PalletCurrentAmount"] = this.CurrentAmount;
	}

	private void OnDestroy()
	{
		if (GameSettings.Instance != null)
		{
			for (int i = 0; i < this.Orders.Length; i++)
			{
				ProductPrintOrder productPrintOrder = this.Orders[i];
				if (productPrintOrder != null)
				{
					productPrintOrder.RemoveFromStorage();
				}
			}
		}
	}

	public float[] BoxCutoff;

	public int MaxOrders = 27;

	[NonSerialized]
	public ProductPrintOrder[] Orders;

	public Renderer Boxes;

	public int CurrentAmount;

	public MaterialPropertyBlock MatBlock;

	[NonSerialized]
	private Furniture _furn;
}
