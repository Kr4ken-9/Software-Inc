﻿using System;
using System.Collections.Generic;

public class SortedLinkedList<T1, T2>
{
	public SortedLinkedList(Func<T1, T1, int> comp)
	{
		this.Comparer = comp;
	}

	private SortedLinkedList<T1, T2>.Node CreateNode(T1 key, T2 value)
	{
		if (this.currentNode < this.pool.Count)
		{
			SortedLinkedList<T1, T2>.Node node = this.pool[this.currentNode];
			node.Key = key;
			node.Value = value;
			node.Parent = null;
			node.Lower = null;
			node.Higher = null;
			this.currentNode++;
			return node;
		}
		SortedLinkedList<T1, T2>.Node node2 = new SortedLinkedList<T1, T2>.Node(key, value);
		this.pool.Add(node2);
		this.currentNode++;
		return node2;
	}

	public void Add(T1 key, T2 value)
	{
		SortedLinkedList<T1, T2>.Node node = this.CreateNode(key, value);
		this.Add(node, this.root);
		if (this.lowest == null || this.Comparer(node.Key, this.lowest.Key) < 0)
		{
			this.lowest = node;
		}
	}

	public void Clear()
	{
		this.root = null;
		this.lowest = null;
		this.currentNode = 0;
	}

	public T2 Pop()
	{
		SortedLinkedList<T1, T2>.Node node = this.lowest;
		this.Remove(node);
		return node.Value;
	}

	private void Add(SortedLinkedList<T1, T2>.Node node, SortedLinkedList<T1, T2>.Node from)
	{
		if (this.root == null)
		{
			this.root = node;
			this.lowest = node;
			return;
		}
		while (from != null)
		{
			int num = this.Comparer(node.Key, from.Key);
			if (num < 0)
			{
				if (from.Lower == null)
				{
					from.Lower = node;
					node.Parent = from;
					break;
				}
				from = from.Lower;
			}
			else
			{
				if (from.Higher == null)
				{
					from.Higher = node;
					node.Parent = from;
					break;
				}
				from = from.Higher;
			}
		}
	}

	private SortedLinkedList<T1, T2>.Node FindLowestFrom(SortedLinkedList<T1, T2>.Node node)
	{
		if (node == null)
		{
			return null;
		}
		for (SortedLinkedList<T1, T2>.Node node2 = node; node2 != null; node2 = node2.Lower)
		{
			if (node2.Lower == null)
			{
				return node2;
			}
		}
		return null;
	}

	private SortedLinkedList<T1, T2>.Node FindHighestFrom(SortedLinkedList<T1, T2>.Node node)
	{
		if (node == null)
		{
			return null;
		}
		for (SortedLinkedList<T1, T2>.Node node2 = node; node2 != null; node2 = node2.Higher)
		{
			if (node2.Higher == null)
			{
				return node2;
			}
		}
		return null;
	}

	private void Remove(SortedLinkedList<T1, T2>.Node node)
	{
		if (this.root == node)
		{
			if (node.Lower != null)
			{
				this.root = node.Lower;
				node.Lower.Parent = null;
			}
			else if (node.Higher != null)
			{
				this.root = node.Higher;
				node.Higher.Parent = null;
			}
			else
			{
				this.root = null;
			}
		}
		if (this.lowest == node)
		{
			if (node.Lower != null)
			{
				this.lowest = this.FindLowestFrom(node.Lower);
			}
			else if (node.Higher != null)
			{
				this.lowest = this.FindLowestFrom(node.Higher);
			}
			else if (node.Parent != null)
			{
				this.lowest = node.Parent;
			}
			else
			{
				this.lowest = this.FindLowestFrom(this.root);
			}
		}
		if (node.Parent != null)
		{
			if (node.Parent.Lower == node)
			{
				node.Parent.Lower = null;
				if (node.Higher != null)
				{
					node.Parent.Lower = node.Higher;
					node.Higher.Parent = node.Parent;
					if (node.Lower != null)
					{
						SortedLinkedList<T1, T2>.Node node2 = this.FindLowestFrom(node.Higher);
						node2.Lower = node.Lower;
						node.Lower.Parent = node2;
					}
				}
				else if (node.Lower != null)
				{
					node.Parent.Lower = node.Lower;
					node.Lower.Parent = node.Parent;
				}
			}
			else if (node.Parent.Higher == node)
			{
				node.Parent.Higher = null;
				if (node.Lower != null)
				{
					node.Parent.Higher = node.Lower;
					node.Lower.Parent = node.Parent;
					if (node.Higher != null)
					{
						SortedLinkedList<T1, T2>.Node node3 = this.FindHighestFrom(node.Lower);
						node3.Higher = node.Higher;
						node.Higher.Parent = node3;
					}
				}
				else if (node.Higher != null)
				{
					node.Parent.Higher = node.Higher;
					node.Higher.Parent = node.Parent;
				}
			}
		}
		node.Parent = null;
		node.Lower = null;
		node.Higher = null;
	}

	private SortedLinkedList<T1, T2>.Node root;

	private SortedLinkedList<T1, T2>.Node lowest;

	private Func<T1, T1, int> Comparer;

	private List<SortedLinkedList<T1, T2>.Node> pool = new List<SortedLinkedList<T1, T2>.Node>();

	private int currentNode;

	private class Node
	{
		public Node(T1 key, T2 value)
		{
			this.Key = key;
			this.Value = value;
		}

		public T1 Key;

		public T2 Value;

		public SortedLinkedList<T1, T2>.Node Lower;

		public SortedLinkedList<T1, T2>.Node Higher;

		public SortedLinkedList<T1, T2>.Node Parent;
	}
}
