﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ReputationBars : MonoBehaviour, IPointerEnterHandler, IEventSystemHandler
{
	private void Update()
	{
		if (this.IsOpen)
		{
			this.UpdateBars();
			int num = Mathf.Clamp(this.contentRect.childCount * 24, 24, Screen.height - 128);
			this.DropdownPanel.sizeDelta = new Vector2(this.DropdownPanel.sizeDelta.x, Mathf.Lerp(this.DropdownPanel.sizeDelta.y, (float)num, Time.deltaTime * 20f));
			if (!RectTransformUtility.RectangleContainsScreenPoint(this.DropdownPanel, Input.mousePosition, null) && !RectTransformUtility.RectangleContainsScreenPoint(this.SelfRect, Input.mousePosition, null))
			{
				this.IsOpen = false;
			}
		}
		else
		{
			this.DropdownPanel.sizeDelta = new Vector2(this.DropdownPanel.sizeDelta.x, Mathf.Lerp(this.DropdownPanel.sizeDelta.y, 0f, Time.deltaTime * 20f));
		}
	}

	public void UpdateBars()
	{
		if (this.company == null)
		{
			return;
		}
		Dictionary<string, Dictionary<string, float>> softwareRep = this.company.GetSoftwareRep();
		if (softwareRep.Sum((KeyValuePair<string, Dictionary<string, float>> x) => x.Value.Count) != this.bars.Sum((KeyValuePair<string, Dictionary<string, ReputationItem>> x) => x.Value.Count))
		{
			int num = 1;
			using (IEnumerator<KeyValuePair<string, Dictionary<string, float>>> enumerator = (from x in softwareRep
			orderby x.Key.LocSW()
			select x).GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					KeyValuePair<string, Dictionary<string, float>> item = enumerator.Current;
					int count = GameSettings.Instance.SoftwareTypes[item.Key].Categories.Count;
					Dictionary<string, ReputationItem> dictionary;
					if (!this.bars.TryGetValue(item.Key, out dictionary))
					{
						dictionary = new Dictionary<string, ReputationItem>();
						this.bars[item.Key] = dictionary;
						GameObject gameObject = this.MakeBar(item.Key.LocSW(), num, count == 1);
						if (count == 1)
						{
							ReputationItem component = gameObject.GetComponent<ReputationItem>();
							component.Label.fontSize = 16;
							dictionary["N/ATHISISBASE"] = component;
						}
					}
					num++;
					if (count == 1)
					{
						dictionary["N/ATHISISBASE"].SetRep(item.Value.First<KeyValuePair<string, float>>().Value);
					}
					else
					{
						foreach (KeyValuePair<string, float> keyValuePair in from x in item.Value
						orderby x.Key.LocSWC(item.Key)
						select x)
						{
							ReputationItem component2;
							if (!dictionary.TryGetValue(keyValuePair.Key, out component2))
							{
								component2 = this.MakeBar(keyValuePair.Key.LocSWC(item.Key), num, true).GetComponent<ReputationItem>();
								dictionary[keyValuePair.Key] = component2;
							}
							component2.SetRep(keyValuePair.Value);
							num++;
						}
					}
				}
			}
		}
		else
		{
			foreach (KeyValuePair<string, Dictionary<string, ReputationItem>> keyValuePair2 in this.bars)
			{
				foreach (KeyValuePair<string, ReputationItem> keyValuePair3 in keyValuePair2.Value)
				{
					if (keyValuePair3.Key.Equals("N/ATHISISBASE"))
					{
						keyValuePair3.Value.SetRep(softwareRep[keyValuePair2.Key].First<KeyValuePair<string, float>>().Value);
					}
					else
					{
						keyValuePair3.Value.SetRep(softwareRep[keyValuePair2.Key][keyValuePair3.Key]);
					}
				}
			}
		}
	}

	private GameObject MakeBar(string name, int i, bool data)
	{
		if (data)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.RepItemPrefab);
			gameObject.GetComponent<ReputationItem>().Label.text = name;
			gameObject.transform.SetParent(this.contentPanel.transform, false);
			gameObject.transform.SetSiblingIndex(i);
			return gameObject;
		}
		GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.LabelPrefab);
		gameObject2.transform.SetParent(this.contentPanel.transform, false);
		gameObject2.transform.SetSiblingIndex(i);
		gameObject2.GetComponent<Text>().text = name;
		gameObject2.GetComponent<Text>().fontSize = 16;
		return gameObject2;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		this.IsOpen = true;
	}

	public GameObject RepItemPrefab;

	public GameObject LabelPrefab;

	public GameObject contentPanel;

	public RectTransform contentRect;

	public RectTransform DropdownPanel;

	public RectTransform SelfRect;

	private bool IsOpen;

	[NonSerialized]
	public Company company;

	private Dictionary<string, Dictionary<string, ReputationItem>> bars = new Dictionary<string, Dictionary<string, ReputationItem>>();
}
