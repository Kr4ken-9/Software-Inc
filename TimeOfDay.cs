﻿using System;
using System.Collections.Generic;
using System.Linq;
using Poly2Tri;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeOfDay : MonoBehaviour
{
	private void Start()
	{
		TimeOfDay.Instance = this;
		this.GroundMat = this.GroundPlane.GetComponent<Renderer>().material;
		this.GroundTop.sharedMaterial = this.GroundMat;
		this.Snow.Play();
		this.targetDate = (float)this.GetDate(true).ToInt();
	}

	public void UpdateGroundTopMesh()
	{
		if (GameSettings.Instance != null)
		{
			List<Furniture> list = (from x in GameSettings.Instance.sRoomManager.AllFurniture
			where x.Parent != null && x.Parent.Floor == -1 && x.TwoFloors
			select x).ToList<Furniture>();
			Mesh mesh = new Mesh();
			Vector2[] array = new Vector2[]
			{
				new Vector2(0f, 0f),
				new Vector2(0f, 256f),
				new Vector2(256f, 256f),
				new Vector2(256f, 0f)
			};
			if (list.Count > 0)
			{
				Triangulator triangulator = new Triangulator(array);
				int[] array2 = triangulator.Triangulate(list.Select(delegate(Furniture x)
				{
					if (x.FinalNav == null)
					{
						x.UpdateBoundaryPoints();
					}
					return x.FinalNav;
				}).ToList<Vector2[]>(), new DTSweepContext());
				mesh.vertices = triangulator.Points.SelectInPlace((Vector2 x) => x.ToVector3(0f));
				array2.ReverseListPart(0, array2.Length);
				mesh.triangles = array2;
			}
			else
			{
				mesh.vertices = array.SelectInPlace((Vector2 x) => x.ToVector3(0f));
				mesh.triangles = new int[]
				{
					0,
					1,
					2,
					2,
					3,
					0
				};
			}
			Vector3[] vertices = mesh.vertices;
			mesh.normals = Utilities.RepeatValue<Vector3>(Vector3.up, vertices.Length);
			mesh.tangents = Utilities.RepeatValue<Vector4>(new Vector4(1f, 0f, 0f, -1f), vertices.Length);
			mesh.uv = vertices.SelectInPlace((Vector3 x) => new Vector2(x.x, x.z));
			this.GroundTop.GetComponent<MeshFilter>().sharedMesh = mesh;
		}
	}

	public void UpdateExtraLayerColor()
	{
		this.GroundPlane2.GetComponent<Renderer>().material.color = this.CurrentWeather.GroundColor;
	}

	public void InitYear(int year)
	{
		this.Year = year;
		this.currentDate = new SDateTime(0, year);
		this.targetDate = (float)this.GetDate(true).ToInt();
		HUD.Instance.UpdateFurnitureButtons();
	}

	public void RunUpdate()
	{
		bool disableSunUpdate = this.DisableSunUpdate;
		this.DisableSunUpdate = false;
		this.disableCars = true;
		this.Update();
		this.disableCars = false;
		this.DisableSunUpdate = disableSunUpdate;
	}

	private Color GetDayColor(float Hour, float Minute)
	{
		float num = (float)this.Month;
		num = Mathf.Abs(num - 6f) / 6f;
		float num2 = Hour + Minute / 60f;
		num2 /= 24f;
		Color a = this.CurrentWeather.SummerGradient.Evaluate(num2);
		Color b = this.CurrentWeather.WinterGradient.Evaluate(num2);
		return Color.Lerp(a, b, num);
	}

	private Color GetSkyColor(float Hour, float Minute)
	{
		float num = Hour + Minute / 60f;
		num /= 24f;
		return this.CurrentWeather.SkyGradient.Evaluate(num);
	}

	public Color GetLeaveColor(bool first)
	{
		float time = ((float)this.Month + ((float)this.Hour + this.Minute / 60f) / 24f) / 12f;
		return (!first) ? this.CurrentWeather.TreeGradient2.Evaluate(time) : this.CurrentWeather.TreeGradient.Evaluate(time);
	}

	private Color GetAmbientColor(float Hour, float Minute)
	{
		float num = Hour + Minute / 60f;
		num /= 24f;
		return this.CurrentWeather.AmbientGradient.Evaluate(num);
	}

	private Quaternion GetDayRotation(float Hour, float Minute)
	{
		float num = Hour + Minute / 60f;
		num /= 24f;
		if ((double)num > 0.5)
		{
			num = 1f - num;
		}
		num *= 6.28318548f;
		num += 0.3926991f;
		Vector3 a = new Vector3(-Mathf.Sin(num), 1f, Mathf.Cos(num));
		return Quaternion.LookRotation(-a);
	}

	private void SnowSim()
	{
		this.SnowAmount = this.GetSnowTemp(0f);
		float snowTemp = this.GetSnowTemp(0.0416666679f);
		bool flag = snowTemp > 0f;
		if (this.Snow.isEmitting != flag)
		{
			if (flag)
			{
				this.Snow.Play();
			}
			else
			{
				this.Snow.Stop();
			}
		}
		if (this.Snow.isPlaying)
		{
			ParticleSystem.EmissionModule emission = this.Snow.emission;
			this.Snow.main.simulationSpeed = Mathf.Min(100f, GameSettings.GameSpeed);
			emission.rateOverTimeMultiplier = 500f * snowTemp;
		}
		Color color = DataOverlay.Instance.GetColor(this.GetGroundColor());
		Color color2 = RoomMaterialController.GetColor(RoomMaterialController.Instance.GroundColorID);
		if (Mathf.Abs(color.r - color2.r) + Mathf.Abs(color.g - color2.g) + Mathf.Abs(color.b - color2.b) > 0.04f)
		{
			RoomMaterialController.WriteColor(RoomMaterialController.Instance.GroundColorID, color);
		}
		this.GroundMat.color = color;
	}

	public Color GetGroundColor()
	{
		return Color.Lerp(this.CurrentWeather.GroundColor, new Color(0.7f, 0.7f, 0.7f), this.SnowAmount);
	}

	private void RainSim()
	{
	}

	public float GetSnowTemp(float offset)
	{
		float num = this.YearFloat() + offset;
		if (num > 1f)
		{
			num -= 1f;
		}
		if (num < 0f)
		{
			num = 1f - num;
		}
		return Mathf.Clamp01(-this.CurrentWeather.TempMin.Evaluate(num) / 2f);
	}

	public float GetMonthFloat(float mid, float range)
	{
		float num = (float)this.Month + ((float)this.Hour + this.Minute / 60f) / 24f;
		num = (num + (11f - mid) + range / 2f) % 12f;
		if (num > range)
		{
			return 0f;
		}
		return 1f - Mathf.Abs(num / range - 0.5f) * 2f;
	}

	private void CarSpawn()
	{
		if (!this.disableCars && UnityEngine.Random.value < GameData.EnvCars[(int)GameSettings.Instance.EnvType] * Time.deltaTime * GameSettings.GameSpeed)
		{
			CarScript carScript = RoadManager.Instance.CreateCar((UnityEngine.Random.value <= 0.9f) ? 1 : 2);
			carScript.Init();
		}
	}

	private void Update()
	{
		if (this.GroundTopDirty)
		{
			this.UpdateGroundTopMesh();
			this.GroundTopDirty = false;
		}
		if (!this.DisableSunUpdate)
		{
			this.CarSpawn();
			this.Clouds.main.simulationSpeed = (float)HUD.Instance.GameSpeed;
			this.hasUpdatedDate = false;
			this.Minute += Time.deltaTime * GameSettings.GameSpeed;
			bool flag = false;
			while (this.Minute > 59f && !GameSettings.ForcePause)
			{
				this.Minute -= 59f;
				flag |= this.AddHour(!flag);
			}
			this.targetDate = Mathf.Lerp(this.targetDate, this.ToFloat(), 0.1f);
			float[] hudtime = this.GetHUDTime(this.targetDate);
			Light component = base.GetComponent<Light>();
			Color dayColor = this.GetDayColor(hudtime[1], hudtime[0]);
			component.color = ((GameSettings.Instance.ActiveFloor != -1) ? dayColor : Color.black);
			RenderSettings.fogDensity = ((!CameraScript.Instance.FlyMode) ? ((GameSettings.Instance.ActiveFloor != -1) ? (0.001f + 0.003f * (1f - component.color.grayscale)) : 0.001f) : (0.018f / CameraScript.FlyCamDistance));
			TimeOfDay.LightLevel = Mathf.Min(1f, dayColor.grayscale * 1.5f);
			this.SnowSim();
			this.RainSim();
			this.RainRend.enabled = (GameSettings.Instance.ActiveFloor >= 0);
			this.SnowRend.enabled = (GameSettings.Instance.ActiveFloor >= 0);
			base.transform.rotation = this.GetDayRotation(hudtime[1], hudtime[0]);
			Camera mainCam = CameraScript.Instance.mainCam;
			Color color = (GameSettings.Instance.ActiveFloor != -1) ? this.GetSkyColor(hudtime[1], hudtime[0]) : Color.black;
			RenderSettings.fogColor = color;
			mainCam.backgroundColor = color;
			RenderSettings.ambientLight = ((GameSettings.Instance.ActiveFloor != -1) ? this.GetAmbientColor(hudtime[1], hudtime[0]) : new Color(0.2f, 0.2f, 0.2f));
			if (DataOverlay.HasActive)
			{
				component.color = Color.white * component.color.grayscale;
				Camera mainCam2 = CameraScript.Instance.mainCam;
				Color color2 = Color.white * RenderSettings.fogColor.grayscale;
				RenderSettings.fogColor = color2;
				mainCam2.backgroundColor = color2;
				RenderSettings.ambientLight = Color.white * RenderSettings.ambientLight.grayscale;
			}
			GameSettings.Instance.LeaveMat.SetColor("_Color1", DataOverlay.Instance.GetColor(this.GetLeaveColor(true)));
			GameSettings.Instance.LeaveMat.SetColor("_Color2", DataOverlay.Instance.GetColor(this.GetLeaveColor(false)));
			GameSettings.Instance.LeaveMat.SetFloat("_Snow", this.SnowAmount);
			this.UpdateTemperature();
			this.Clouds.gameObject.SetActive(GameSettings.Instance.ActiveFloor >= 0);
		}
	}

	private void UpdateTemperature()
	{
		float time = this.YearFloat();
		this.Temperature = this.CurrentWeather.TempMin.Evaluate(time) + this.CurrentWeather.TemperatureCurve.Evaluate(this.DayFloat()) * this.CurrentWeather.TempRange.Evaluate(time);
	}

	private float ToFloat()
	{
		return this.Minute + ((float)this.Hour + ((float)this.Day + ((float)this.Month + (float)this.Year * 12f) * (float)GameSettings.DaysPerMonth) * 24f) * 60f;
	}

	private float YearFloat()
	{
		return ((float)this.Month + ((float)this.Day + ((float)this.Hour + this.Minute / 60f) / 24f) / (float)GameSettings.DaysPerMonth) / 12f;
	}

	private float DayFloat()
	{
		return ((float)this.Hour + this.Minute / 60f) / 24f;
	}

	private float[] ToTime(float d)
	{
		int num = Mathf.FloorToInt(d);
		return new float[]
		{
			d % 60f,
			(float)(num / 60 % 24),
			(float)(num / 1440 % 12),
			(float)(num / 17280)
		};
	}

	private float[] GetHUDTime(float d)
	{
		float[] array = this.ToTime(d);
		if (HUD.Instance != null && HUD.Instance.BuildMode)
		{
			float value = HUD.Instance.SunSlider.value;
			array[0] = value * 840f % 60f;
			array[1] = Mathf.Floor(value * 14f);
		}
		return array;
	}

	public bool AddHour(bool canBankrupt)
	{
		this.hasUpdatedDate = false;
		this.Hour++;
		if (this.Hour == 24)
		{
			this.Hour = 0;
			this.AddDay();
		}
		this.UpdateTemperature();
		return this.UpdateHour(canBankrupt);
	}

	public void AddDay()
	{
		this.hasUpdatedDate = false;
		this.Day++;
		if (this.Day == GameSettings.DaysPerMonth)
		{
			this.Day = 0;
			this.Month++;
			if (this.Month == 12)
			{
				this.Month = 0;
				this.Year++;
			}
			this.UpdateDay();
			this.UpdateMonth();
			if (Options.AutoSave && GameSettings.Instance.MyCompany.Money > 0f)
			{
				SaveGameManager.Instance.AutoSave();
			}
		}
		else
		{
			this.UpdateDay();
			if (Options.AutoSave && GameSettings.Instance.MyCompany.Money > 0f)
			{
				SaveGameManager.Instance.AutoSave();
			}
		}
	}

	private bool UpdateHour(bool canBankrupt)
	{
		HUD.Instance.popupManager.UpdateCooldowns();
		if (!BusScript.Present)
		{
			CarScript carScript = RoadManager.Instance.CreateCar(0);
			carScript.Init();
			BusScript.Present = true;
		}
		IServerHost[] allServers = GameSettings.Instance.GetAllServers();
		Dictionary<IServerHost, List<IServerHost>> dictionary = new Dictionary<IServerHost, List<IServerHost>>();
		foreach (IServerHost serverHost in allServers)
		{
			if (serverHost.PowerSum == 0f && serverHost.Fallback != null)
			{
				IServerHost fallback = serverHost.Fallback;
				while (fallback.PowerSum == 0f && fallback.Fallback != null && fallback.Fallback != serverHost)
				{
					fallback = fallback.Fallback;
				}
				if (!dictionary.ContainsKey(fallback))
				{
					dictionary[fallback] = new List<IServerHost>();
				}
				dictionary[fallback].Add(serverHost);
			}
		}
		int num = 0;
		foreach (IServerHost serverHost2 in allServers)
		{
			if (serverHost2.PowerSum == 0f && serverHost2.Fallback != null)
			{
				serverHost2.Available = 1f;
			}
			else
			{
				Dictionary<IServerItem, float> dictionary2 = serverHost2.Items.ToDictionary((IServerItem x) => x, (IServerItem x) => x.GetLoadRequirement().BandwidthFactor(this.GetDate(false)));
				List<IServerHost> list;
				if (dictionary.TryGetValue(serverHost2, out list))
				{
					for (int k = 0; k < list.Count; k++)
					{
						IServerHost serverHost3 = list[k];
						for (int l = 0; l < serverHost3.Items.Count; l++)
						{
							IServerItem serverItem = serverHost3.Items[l];
							dictionary2[serverItem] = serverItem.GetLoadRequirement().BandwidthFactor(this.GetDate(false));
						}
					}
				}
				float num2 = 0f;
				if (dictionary2.Count > 0)
				{
					num2 = dictionary2.Sum((KeyValuePair<IServerItem, float> x) => x.Value);
					if (num2 < serverHost2.PowerSum)
					{
						foreach (KeyValuePair<IServerItem, float> keyValuePair in dictionary2)
						{
							keyValuePair.Key.HandleLoad(1f);
						}
					}
					else
					{
						num++;
						float load = (num2 != 0f) ? Mathf.Clamp01(serverHost2.PowerSum / num2) : 1f;
						foreach (KeyValuePair<IServerItem, float> keyValuePair2 in dictionary2)
						{
							keyValuePair2.Key.HandleLoad(load);
						}
					}
				}
				float num3 = (serverHost2.PowerSum != 0f) ? (num2 / serverHost2.PowerSum) : 0f;
				serverHost2.Available = ((num3 <= 1f) ? (1f - num3) : 0f);
				if (serverHost2 is Server)
				{
					Server server = serverHost2 as Server;
					float num4 = Mathf.Max(1f, num3 + 0.25f);
					if (!float.IsNaN(num4) && !float.IsInfinity(num4))
					{
						server.furn.upg.AtrophyModifier = num4;
					}
				}
			}
		}
		this.BadServerCounter.SetNumber(num);
		IServerItem[] array = GameSettings.Instance.UnsupportedServerItems.ToArray();
		for (int m = 0; m < array.Length; m++)
		{
			array[m].HandleLoad(0f);
		}
		foreach (Team team in GameSettings.Instance.sActorManager.Teams.Values)
		{
			if (team.HREnabled)
			{
				team.HR.Update(team);
			}
		}
		foreach (Furniture furniture in GameSettings.Instance.sRoomManager.AllFurniture)
		{
			if (furniture.HeatCoolPotential != 0f && furniture.IsOn)
			{
				bool cool = furniture.HeatCoolPotential < 0f;
				if (furniture.TemperatureController)
				{
					furniture.upg.AtrophyModifier = furniture.RoomTemperature.AverageOrDefault((Room x) => (!cool) ? x.TempHeatUsageMax : x.TempCoolUsageMax, 0f);
					furniture.CalculateManualUsage(furniture.RoomTemperature.SumSafe((Room x) => (!cool) ? x.TempHeatUsageAgg : x.TempCoolUsageAgg));
				}
				else
				{
					furniture.upg.AtrophyModifier = ((!cool) ? furniture.Parent.TempHeatUsageMax : furniture.Parent.TempCoolUsageMax);
					furniture.CalculateManualUsage((!cool) ? furniture.Parent.TempHeatUsageAgg : furniture.Parent.TempCoolUsageAgg);
				}
			}
		}
		for (int n = 0; n < GameSettings.Instance.sRoomManager.Rooms.Count; n++)
		{
			Room room = GameSettings.Instance.sRoomManager.Rooms[n];
			room.ResetTempUsage();
			room.UpdateTemperature(true);
		}
		SupportWork[] array2 = GameSettings.Instance.MyCompany.WorkItems.OfType<SupportWork>().ToArray<SupportWork>();
		for (int num5 = 0; num5 < array2.Length; num5++)
		{
			array2[num5].Simulate(this.GetDate(false));
		}
		this.canSkip = this.CanSkip();
		float num6 = (from x in GameSettings.Instance.sActorManager.Staff
		where x.OnCall
		select x.employee.Salary).Sum();
		if (num6 > 0f)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(-num6, Company.TransactionCategory.Staff, "On call");
		}
		if (canBankrupt)
		{
			if (this.Banktupcy == null && GameSettings.Instance.MyCompany.Money < 0f)
			{
				SDateTime sdateTime = SDateTime.Now();
				this.Banktupcy = new SDateTime?(new SDateTime(0, sdateTime.Hour - 1, sdateTime.Day, sdateTime.Month + 1, sdateTime.Year));
				WindowManager.Instance.ShowMessageBox("NewDebtLoss".Loc(new object[]
				{
					(this.Banktupcy.Value + new SDateTime(0, 1, 0, 0, 0)).ToString()
				}), true, DialogWindow.DialogType.Warning);
				return true;
			}
			if (this.Banktupcy != null)
			{
				SDateTime? banktupcy = this.Banktupcy;
				if (SDateTime.Now() > banktupcy)
				{
					if (GameSettings.Instance.MyCompany.Money < 0f)
					{
						GameSettings.ForcePause = true;
						GameSettings.FreezeGame = true;
						DialogWindow dialogWindow = WindowManager.SpawnDialog();
						DialogWindow dialogWindow2 = dialogWindow;
						string msg = "BankruptLoseMsg".Loc(new object[]
						{
							GameSettings.Instance.MyCompany.Money.Currency(true)
						});
						bool draw = false;
						DialogWindow.DialogType type = DialogWindow.DialogType.Error;
						KeyValuePair<string, Action>[] array3 = new KeyValuePair<string, Action>[2];
						array3[0] = new KeyValuePair<string, Action>("Finances", delegate
						{
							HUD.Instance.financeWindow.Window.Modal = true;
							HUD.Instance.financeWindow.Show();
						});
						array3[1] = new KeyValuePair<string, Action>("Quit", delegate
						{
							GameSettings.ForcePause = false;
							ErrorLogging.FirstOfScene = true;
							ErrorLogging.SceneChanging = true;
							SceneManager.LoadScene("MainMenu");
						});
						dialogWindow2.Show(msg, draw, type, array3);
						return true;
					}
					HUD.Instance.AddPopupMessage("NewDebtLossPass".Loc(), "Info", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Good, 0f, PopupManager.PopupIDs.None, 1);
					this.Banktupcy = null;
				}
			}
		}
		return false;
	}

	private void UpdateDay()
	{
		GameSettings.Instance.MyCompany.SimulateFanLoss(this.GetDate(false));
		GameSettings.Instance.ResetUndo();
		List<MarketingPlan> list = GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>().ToList<MarketingPlan>();
		for (int i = 0; i < list.Count; i++)
		{
			MarketingPlan marketingPlan = list[i];
			if (marketingPlan.Type == MarketingPlan.TaskType.PostMarket)
			{
				marketingPlan.AddEffect(true);
			}
		}
		foreach (SoftwareAlpha softwareAlpha in GameSettings.Instance.PressBuildQueue)
		{
			if (!softwareAlpha.Done)
			{
				float quality = softwareAlpha.GetQuality();
				float months = Utilities.GetMonths(softwareAlpha.DevStart, SDateTime.Now());
				float num = (softwareAlpha.ReleaseDate == null) ? (softwareAlpha.DevTime - months) : (-SoftwareWorkItem.Lateness(softwareAlpha.ReleaseDate.Value));
				int months2 = Mathf.RoundToInt(num);
				num = Mathf.Max(0f, num);
				if (GameSettings.Instance.Difficulty == 0)
				{
					num *= 1.4f;
				}
				if (GameSettings.Instance.Difficulty == 1)
				{
					num *= 1.2f;
				}
				float num2 = 1f / softwareAlpha.DevTime / Mathf.Lerp(1f, 2f, softwareAlpha.CodeArtRatio);
				float num3 = Mathf.Clamp01(quality + num * num2);
				float num4 = Mathf.Pow(num3 - 0.5f, 2f);
				num4 = ((num3 <= 0.5f) ? (-num4) : (num4 / 0.25f));
				float reputation = GameSettings.Instance.MyCompany.GetReputation(softwareAlpha._type, softwareAlpha._category);
				num4 *= reputation.WeightOne(0.9f);
				num4 *= softwareAlpha.PressBuildEffect;
				float featureScore = GameSettings.Instance.simulation.GetFeatureScore(softwareAlpha._type, softwareAlpha._category, SDateTime.Now());
				float num5 = (featureScore != 0f) ? (softwareAlpha.Type.FeatureScore(softwareAlpha.Features) / featureScore) : 1f;
				num4 *= num5;
				float num6 = softwareAlpha.Followers / softwareAlpha.MaxFollowers;
				num4 *= Mathf.Clamp01(10f * num6).WeightOne(0.85f);
				SoftwareProduct sequelTo = (softwareAlpha.SequelTo == null) ? null : GameSettings.Instance.simulation.GetProduct(softwareAlpha.SequelTo.Value, false);
				float number = SoftwareProduct.CalculateSequelBonus(sequelTo, softwareAlpha.DevTime, 1f);
				num4 *= number.WeightOne(0.1f);
				float pressBuildEffect = softwareAlpha.PressBuildEffect;
				softwareAlpha.PressBuildEffect = 0f;
				if (softwareAlpha.ReleaseDate == null)
				{
					num4 *= 0.25f;
				}
				float followerWish = MarketingPlan.GetFollowerWish(softwareAlpha.MaxFollowers);
				softwareAlpha.Followers += num4 * followerWish * 0.5f;
				softwareAlpha.FollowerChange += num4 * followerWish * 0.1f;
				if (!softwareAlpha.AutoDev)
				{
					Newspaper.GeneratePressbuildReview(new ArticleGenerator.PressBuildReviewData(GameSettings.Instance.MyCompany.Name, softwareAlpha.SoftwareName, num3, num5, pressBuildEffect, num6, reputation, months2, softwareAlpha.ReleaseDate));
				}
			}
		}
		GameSettings.Instance.PressBuildQueue.Clear();
		foreach (Deal deal in HUD.Instance.dealWindow.AllDeals.Values.OfType<Deal>())
		{
			if (deal.Active)
			{
				float num7 = deal.Payout();
				if (num7 > 0f && deal.Company != null && deal.Client != null)
				{
					string bill = deal.Title();
					deal.Company.MakeTransaction(num7, Company.TransactionCategory.Deals, bill);
					deal.Client.MakeTransaction(-num7, Company.TransactionCategory.Deals, bill);
				}
				if (deal.Company != null)
				{
					deal.Company.ChangeBusinessRep(deal.ReputationEffect(false).SpreadPercentage(GameSettings.DaysPerMonth), deal.ReputationCategory(), 1f);
				}
			}
		}
		GameSettings.Instance.simulation.SimulateMonth(this.GetDate(false), false);
		foreach (ProductDetailWindow productDetailWindow in WindowManager.FindWindowTypeEnum<ProductDetailWindow>())
		{
			productDetailWindow.UpdateMe();
		}
		for (int j = 0; j < GameSettings.Instance.FollowerSimulation.Count; j++)
		{
			SoftwareWorkItem softwareWorkItem = GameSettings.Instance.FollowerSimulation[j];
			if (!softwareWorkItem.Done)
			{
				softwareWorkItem.ReEvaluateMaxFollowers();
			}
		}
		List<WorkItem> list2 = GameSettings.Instance.MyCompany.WorkItems.ToList<WorkItem>();
		for (int k = 0; k < list2.Count; k++)
		{
			ResearchWork researchWork = list2[k] as ResearchWork;
			if (researchWork != null)
			{
				researchWork.UpdateValid();
			}
			else
			{
				AutoDevWorkItem autoDevWorkItem = list2[k] as AutoDevWorkItem;
				if (autoDevWorkItem != null)
				{
					autoDevWorkItem.UpdateSupportMarket();
				}
			}
		}
		HUD.Instance.ApplyProductWindowFilters();
		HUD.Instance.distributionWindow.Chart.UpdateCachedLines();
		HUD.Instance.distributionWindow.UpdateDistributionDeals();
		HUD.Instance.distributionWindow.RefreshOrders();
		if (this.Day != 0)
		{
			HUD.Instance.financeWindow.UpdateSheet(false);
			Newspaper.StoryRollover(this.GetDate(false), true, false);
		}
		HUD.Instance.UpdateCashflow();
		if (GameSettings.Instance.Vacations > 0)
		{
			HUD.Instance.AddPopupMessage("VacationNotify".Loc(new object[]
			{
				GameSettings.Instance.Vacations
			}), "Info", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Neutral, 0f, PopupManager.PopupIDs.None, 6);
			GameSettings.Instance.Vacations = 0;
		}
		HUD.Instance.comingReleaseWindow.UpdateList();
		if (this.SickCount > 0)
		{
			HUD.Instance.AddPopupMessage("SickNotifyPlural".Loc(new object[]
			{
				"EmployeeCount".Loc(new object[]
				{
					this.SickCount
				})
			}), "Info", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Neutral, 0.25f, PopupManager.PopupIDs.None, 6);
		}
		else if (this.Sick.Count > 0)
		{
			if (this.Sick.Count == 1)
			{
				HUD.Instance.AddPopupMessage("SickNotify".Loc(new object[]
				{
					this.Sick[0].employee.FullName
				}), "Info", PopupManager.PopUpAction.GotoEmp, this.Sick[0].DID, PopupManager.NotificationSound.Neutral, 0.25f, PopupManager.PopupIDs.None, 6);
			}
			else
			{
				HUD instance = HUD.Instance;
				string input = "SickNotifyPlural";
				object[] array = new object[1];
				array[0] = Newspaper.MakeList((from x in this.Sick
				select x.employee.FullName).ToList<string>());
				instance.AddPopupMessage(input.Loc(array), "Info", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Neutral, 0.25f, PopupManager.PopupIDs.None, 6);
			}
		}
		this.Sick.Clear();
		this.SickCount = 0;
		GameSettings.Instance.simulation.TurnLoss();
	}

	public void AddToSick(Actor self)
	{
		if (this.SickCount > 0)
		{
			this.SickCount++;
		}
		else if (this.Sick.Count > 2)
		{
			this.SickCount = this.Sick.Count + 1;
		}
		else
		{
			this.Sick.Add(self);
		}
	}

	private void UpdateMonth()
	{
		int month = this.Month;
		switch (month)
		{
		case 0:
			UISoundFX.ChangeMusicState("Spring");
			break;
		default:
			switch (month)
			{
			case 6:
				UISoundFX.ChangeMusicState("Autumn");
				break;
			case 9:
				UISoundFX.ChangeMusicState("Winter");
				break;
			}
			break;
		case 3:
			UISoundFX.ChangeMusicState("Summer");
			break;
		}
		HUD.Instance.dealWindow.CancelDueWork();
		HUD.Instance.dealWindow.GenerateBid();
		foreach (Actor actor in GameSettings.Instance.sActorManager.Actors)
		{
			actor.employee.AgeMonth++;
			if (!actor.IgnoreOffSalary)
			{
				if (actor.TakingCourses)
				{
					GameSettings.Instance.MyCompany.MakeTransaction(-actor.GetRealSalary(), Company.TransactionCategory.Salaries, null);
				}
				else if (actor.SpecialState == Actor.HomeState.Vacation)
				{
					float benefitValue = actor.GetBenefitValue("Paid vacation");
					if (benefitValue > 0f)
					{
						GameSettings.Instance.MyCompany.MakeTransaction(-benefitValue * actor.GetRealSalary(), Company.TransactionCategory.Benefits, "Paid vacation");
					}
				}
			}
			if (this.Month == 0 && !actor.employee.Fired)
			{
				float num = actor.ChristmasBonus * actor.GetRealSalary();
				if (num > 0f)
				{
					GameSettings.Instance.MyCompany.MakeTransaction(-num, Company.TransactionCategory.Benefits, "Christmas bonus");
				}
				actor.ChristmasBonus = actor.GetBenefitValue("Christmas bonus");
			}
			actor.IgnoreOffSalary = false;
		}
		foreach (Team team in GameSettings.Instance.sActorManager.Teams.Values)
		{
			team.HR.SpentLast = team.HR.Spent;
			team.HR.Spent = 0f;
		}
		for (int i = 0; i < GameSettings.Instance.PlayerPlots.Count; i++)
		{
			PlotArea plotArea = GameSettings.Instance.PlayerPlots[i];
			if (plotArea.MonthsLeft > 0)
			{
				plotArea.MonthsLeft--;
				GameSettings.Instance.MyCompany.MakeTransaction(-plotArea.Monthly, Company.TransactionCategory.Bills, "Plot");
			}
		}
		HUD.Instance.contractWindow.UpdateContracts(this.GetDate(false));
		if (GameSettings.Instance.SalaryDue > 0f)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(-GameSettings.Instance.SalaryDue, Company.TransactionCategory.Salaries, null);
			GameSettings.Instance.SalaryDue = 0f;
		}
		if (GameSettings.Instance.StaffSalaryDue > 0f)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(-GameSettings.Instance.StaffSalaryDue, Company.TransactionCategory.Staff, "Salary");
			GameSettings.Instance.StaffSalaryDue = 0f;
		}
		foreach (Actor actor2 in GameSettings.Instance.sActorManager.Actors)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(-actor2.GetBenefitValue("Pension"), Company.TransactionCategory.Benefits, "Pension");
		}
		foreach (Furniture furniture in GameSettings.Instance.sRoomManager.AllFurniture)
		{
			furniture.TurnMonth();
		}
		float electricityBill = GameSettings.Instance.ElectricityBill;
		if (electricityBill > 0f)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(-electricityBill, Company.TransactionCategory.Bills, "Electricity");
			GameSettings.Instance.ElectricityBill = 0f;
		}
		float waterbill = GameSettings.Instance.Waterbill;
		if (waterbill > 0f)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(-waterbill, Company.TransactionCategory.Bills, "Water");
			GameSettings.Instance.Waterbill = 0f;
		}
		float num2 = (from x in GameSettings.Instance.sRoomManager.AllFurniture
		where x != null && x.Type.Equals("Server")
		select x).Sum(delegate(Furniture x)
		{
			Server component = x.GetComponent<Server>();
			return component.Power * (1f - component.Rep.Available) * Server.ISPCost;
		});
		if (num2 > 0f)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(-num2, Company.TransactionCategory.Bills, "Internet");
		}
		if (GameSettings.Instance.RentMode)
		{
			float num3 = (from x in GameSettings.Instance.sRoomManager.Rooms
			where x.PlayerOwned
			select x).SumSafe((Room x) => BuildController.GetRoomCost(x.Edges, x.Area, x.Outdoors, x.Floor, false, true));
			GameSettings.Instance.MyCompany.MakeTransaction(-num3, Company.TransactionCategory.Bills, "Rent");
		}
		GameSettings.Instance.PaybackLoan();
		HUD.Instance.loanWindow.UpdateLoans();
		float num4 = GameSettings.Instance.Insurance.Money * InsuranceAccount.MonthlyInterest;
		GameSettings.Instance.MyCompany.AddToCashflow(num4, Company.TransactionCategory.Interest);
		GameSettings.Instance.Insurance.Money += num4;
		GameSettings.Instance.Insurance.UpdateDeposits();
		foreach (Company company in GameSettings.Instance.simulation.GetAllCompanies())
		{
			company.PayDividends();
		}
		foreach (Company company2 in GameSettings.Instance.simulation.GetAllCompanies())
		{
			company2.EndDay();
		}
		HUD.Instance.UpdateCashflow();
		GameSettings.Instance.MyCompany.LicenseIncome = 0f;
		float monthFloat = this.GetMonthFloat(10f, 2f);
		this.BiggestGrowth();
		Newspaper.StoryRollover(this.GetDate(false), true, false);
		List<float> list = null;
		if (GameSettings.Instance.MyCompany.Cashflow.TryGetValue("Balance", out list) && list.Count > 1)
		{
			float num5 = 0f;
			int num6 = 0;
			float num7 = 0f;
			float num8 = 1f;
			for (int j = 1; j < Mathf.Min(7, list.Count); j++)
			{
				num5 += (list[list.Count - j] - list[list.Count - j - 1]) * num8;
				num7 += num8;
				num6++;
				num8 = num8 / 3f * 2f;
			}
			HUD.Instance.BankruptcyWarning.SetActive(GameSettings.Instance.MyCompany.Money + GameSettings.Instance.Insurance.Money + num5 / num7 * (float)num6 < 0f);
		}
		else
		{
			HUD.Instance.BankruptcyWarning.SetActive(false);
		}
		if (this.Month == 11)
		{
			HUD.Instance.wageWindow.List.Items.Clear();
			foreach (Actor actor3 in GameSettings.Instance.sActorManager.Actors)
			{
				actor3.ApplyNewBenefits();
				if (!actor3.employee.Fired && !actor3.WorksForFree() && (actor3.NegotiateSalary || Utilities.GetMonths(actor3.employee.LastWage, this.GetDate(false)) >= 8f) && actor3.WageNegotiationNecessary())
				{
					if (actor3.Team != null && actor3.GetTeam().Leader != actor3 && actor3.GetTeam().HREnabled && actor3.GetTeam().HR.HandleWages)
					{
						Team team2 = actor3.GetTeam();
						team2.HR.HandleWage(actor3, team2);
					}
					else
					{
						HUD.Instance.wageWindow.List.Items.Add(actor3);
					}
				}
			}
			if (HUD.Instance.wageWindow.List.Items.Count > 0)
			{
				HUD.Instance.wageWindow.Show(true);
			}
		}
		else
		{
			IEnumerable<Actor> enumerable = from x in GameSettings.Instance.sActorManager.Actors
			where x.NegotiateSalary
			select x;
			foreach (Actor actor4 in enumerable)
			{
				if (actor4.employee.Fired || actor4.WorksForFree() || !actor4.WageNegotiationNecessary())
				{
					actor4.NegotiateSalary = false;
				}
				else if (actor4.Team != null && actor4.GetTeam().Leader != actor4 && actor4.GetTeam().HREnabled && actor4.GetTeam().HR.HandleWages)
				{
					Team team3 = actor4.GetTeam();
					team3.HR.HandleWage(actor4, team3);
				}
				else
				{
					HUD.Instance.wageWindow.List.Items.Add(actor4);
				}
			}
			if (HUD.Instance.wageWindow.List.Items.Count > 0)
			{
				HUD.Instance.wageWindow.Show(true);
			}
		}
		GameSettings.Instance.FlipBills();
		HUD.Instance.financeWindow.UpdateSheet(false);
		HUD.Instance.UpdateFurnitureButtons();
		HUD.Instance.eventWindow.UpdateEvents();
		HUD.Instance.researchWindow.UpdateLists();
	}

	private void BiggestGrowth()
	{
		Company company = GameSettings.Instance.simulation.GetAllCompanies().MaxInstance(delegate(Company x)
		{
			List<float> list2 = x.Cashflow["Balance"];
			float num2 = (list2.Count <= 5) ? 0f : (list2[list2.Count - 1] - list2[list2.Count - 2]);
			return num2 * (num2 / x.Money);
		});
		List<float> list = company.Cashflow["Balance"];
		float num = (list.Count <= 5) ? 0f : (list[list.Count - 1] - list[list.Count - 2]);
		if (num > 10000f && num / company.Money > 0.25f)
		{
			Newspaper.GenerateGrowth(company, num);
		}
	}

	public void SkipTime()
	{
		if (this.canSkip)
		{
			(from x in RoadManager.Instance.Cars
			where x.CanDestroy
			select x).ToList<CarScript>().ForEach(delegate(CarScript x)
			{
				RoadManager.Instance.DestroyCar(x);
			});
			BusScript.Present = false;
		}
		this.IsSkipping = true;
		if (this.canSkip && !GameSettings.ForcePause)
		{
			UISoundFX.PlaySFX("SkipDay", -1f, 0f);
		}
		bool flag = false;
		while (this.canSkip && !GameSettings.ForcePause)
		{
			if (this.Minute != 0f)
			{
				GameSettings.Instance.SimulateWork(SDateTime.Now(), 60f - this.Minute);
			}
			else
			{
				GameSettings.Instance.SimulateWork(SDateTime.Now(), 60f);
			}
			this.Minute = 0f;
			flag |= this.AddHour(!flag);
		}
		this.IsSkipping = false;
	}

	public bool CanSkip()
	{
		if (GameSettings.Instance == null || HUD.Instance == null)
		{
			return false;
		}
		if (GameSettings.Instance.MyCompany.Money < 0f && this.Banktupcy != null && Utilities.GetHours(SDateTime.Now(), this.Banktupcy.Value) < 3.5f)
		{
			return false;
		}
		if ((HUD.Instance.UMLGame != null && HUD.Instance.UMLGame.isActiveAndEnabled) || (HUD.Instance.CodeGame != null && HUD.Instance.CodeGame.isActiveAndEnabled))
		{
			return false;
		}
		if (!(from x in GameSettings.Instance.sActorManager.Actors
		where x != null
		select x).Any((Actor x) => x.enabled))
		{
			if (!(from x in GameSettings.Instance.sActorManager.Staff
			where x != null
			select x).Any((Actor x) => x.enabled) && GameSettings.Instance.sActorManager.ReadyForBus.Count <= 0)
			{
				List<Actor> awaiting = GameSettings.Instance.sActorManager.GetAwaiting();
				if (awaiting.Count == 0)
				{
					HUD.Instance.SkipDayHint.SetActive(false);
					return false;
				}
				foreach (Actor actor in from x in awaiting
				where x != null
				select x)
				{
					SDateTime? arriveTime = GameSettings.Instance.sActorManager.GetArriveTime(actor);
					if (arriveTime != null && (arriveTime.Value - this.GetDate(false)).ToInt() <= 59)
					{
						HUD.Instance.SkipDayHint.SetActive(false);
						return false;
					}
				}
				if ((from x in RoadManager.Instance.Cars
				where x != null
				select x).Any((CarScript x) => !x.CanDestroy))
				{
					HUD.Instance.SkipDayHint.SetActive(false);
					return false;
				}
				HUD.Instance.SkipDayHint.SetActive(HUD.Instance.ShouldShowSkipHint);
				return true;
			}
		}
		HUD.Instance.SkipDayHint.SetActive(false);
		return false;
	}

	public void UpdateTime(SDateTime time)
	{
		this.Minute = (float)time.Minute;
		this.Hour = time.Hour;
		this.Day = time.Day;
		this.Month = time.Month;
		this.Year = time.Year;
		this.hasUpdatedDate = false;
	}

	public static SDateTime GetDateLocked()
	{
		if (TimeOfDay.Instance == null)
		{
			return default(SDateTime);
		}
		object timeLock = TimeOfDay.Instance.TimeLock;
		SDateTime date;
		lock (timeLock)
		{
			date = TimeOfDay.Instance.GetDate(false);
		}
		return date;
	}

	public SDateTime GetDate(bool forceUpdate = false)
	{
		if (!this.hasUpdatedDate || forceUpdate)
		{
			this.currentDate = new SDateTime((int)this.Minute, this.Hour, this.Day, this.Month, this.Year);
			this.hasUpdatedDate = true;
		}
		return this.currentDate;
	}

	public static TimeOfDay Instance;

	public float Minute;

	public int Hour;

	public int Day;

	public int Month;

	public int Year;

	public static float LightLevel = 1f;

	public bool IsSkipping;

	public GameObject GroundPlane;

	public GameObject GroundPlane2;

	public Renderer GroundTop;

	public Renderer SnowRend;

	public Renderer RainRend;

	public Material GroundMat;

	private SDateTime currentDate;

	public float targetDate;

	public float Temperature;

	private bool hasUpdatedDate;

	public bool canSkip;

	[NonSerialized]
	public bool DisableSunUpdate = true;

	public ParticleSystem Clouds;

	public ParticleSystem Rain;

	public ParticleSystem Snow;

	public Material[] RoadMaterials;

	public WeatherPreset CurrentWeather;

	[NonSerialized]
	public bool IsRaining;

	[NonSerialized]
	public float WetRoads;

	[NonSerialized]
	public SDateTime? Banktupcy;

	[NonSerialized]
	public List<Actor> Sick = new List<Actor>();

	public int SickCount;

	public ButtonCounter BadServerCounter;

	public AnimationCurve HouseOnage;

	public bool GroundTopDirty = true;

	[NonSerialized]
	private bool disableCars;

	public float SnowAmount;

	[NonSerialized]
	public object TimeLock = new object();
}
