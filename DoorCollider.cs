﻿using System;
using UnityEngine;

public class DoorCollider : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		Actor component = other.transform.parent.GetComponent<Actor>();
		if (component != null)
		{
			Vector2 p = component.transform.position.FlattenVector3();
			Vector2 p2 = component.GetFuturePoint(1.5f).FlattenVector3();
			Vector2 vector = this.Left.position.FlattenVector3();
			Vector2 p3 = vector + this.Left.forward.FlattenVector3();
			if (Utilities.isLeft(vector, p3, p) != Utilities.isLeft(vector, p3, p2))
			{
				for (int i = 0; i < this.Doors.Length; i++)
				{
					this.Doors[i].DoorCollision(this.Front);
				}
			}
		}
	}

	public bool Front;

	public DoorScript[] Doors;

	public Transform Left;
}
