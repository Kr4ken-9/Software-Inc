﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CourierAI : AI<Actor>
{
	public override void InitTree()
	{
		string name = "Spawn";
		if (CourierAI.<>f__mg$cache0 == null)
		{
			CourierAI.<>f__mg$cache0 = new Func<Actor, int>(CourierAI.Spawn);
		}
		BehaviorNode<Actor> behaviorNode = new BehaviorNode<Actor>(name, CourierAI.<>f__mg$cache0, true, -1);
		string name2 = "HasCopies";
		if (CourierAI.<>f__mg$cache1 == null)
		{
			CourierAI.<>f__mg$cache1 = new Func<Actor, int>(CourierAI.HasCopies);
		}
		BehaviorNode<Actor> behaviorNode2 = new BehaviorNode<Actor>(name2, CourierAI.<>f__mg$cache1, false, -1);
		string name3 = "GotoPrinter";
		if (CourierAI.<>f__mg$cache2 == null)
		{
			CourierAI.<>f__mg$cache2 = new Func<Actor, int>(CourierAI.GotoPrinter);
		}
		BehaviorNode<Actor> behaviorNode3 = new BehaviorNode<Actor>(name3, CourierAI.<>f__mg$cache2, true, -1);
		string name4 = "TakePrinter";
		if (CourierAI.<>f__mg$cache3 == null)
		{
			CourierAI.<>f__mg$cache3 = new Func<Actor, int>(CourierAI.TakePrinter);
		}
		BehaviorNode<Actor> behaviorNode4 = new BehaviorNode<Actor>(name4, CourierAI.<>f__mg$cache3, true, -1);
		string name5 = "GotoPallet";
		if (CourierAI.<>f__mg$cache4 == null)
		{
			CourierAI.<>f__mg$cache4 = new Func<Actor, int>(CourierAI.GotoPallet);
		}
		BehaviorNode<Actor> behaviorNode5 = new BehaviorNode<Actor>(name5, CourierAI.<>f__mg$cache4, true, -1);
		string name6 = "TakePallet";
		if (CourierAI.<>f__mg$cache5 == null)
		{
			CourierAI.<>f__mg$cache5 = new Func<Actor, int>(CourierAI.TakePallet);
		}
		BehaviorNode<Actor> behaviorNode6 = new BehaviorNode<Actor>(name6, CourierAI.<>f__mg$cache5, true, -1);
		string name7 = "GotoVan";
		if (CourierAI.<>f__mg$cache6 == null)
		{
			CourierAI.<>f__mg$cache6 = new Func<Actor, int>(CourierAI.GotoVan);
		}
		BehaviorNode<Actor> behaviorNode7 = new BehaviorNode<Actor>(name7, CourierAI.<>f__mg$cache6, true, -1);
		string name8 = "DropProducts";
		if (CourierAI.<>f__mg$cache7 == null)
		{
			CourierAI.<>f__mg$cache7 = new Func<Actor, int>(CourierAI.DropProducts);
		}
		BehaviorNode<Actor> behaviorNode8 = new BehaviorNode<Actor>(name8, CourierAI.<>f__mg$cache7, true, -1);
		string name9 = "AnyCopies";
		if (CourierAI.<>f__mg$cache8 == null)
		{
			CourierAI.<>f__mg$cache8 = new Func<Actor, int>(CourierAI.AnyCopies);
		}
		BehaviorNode<Actor> behaviorNode9 = new BehaviorNode<Actor>(name9, CourierAI.<>f__mg$cache8, false, -1);
		string name10 = "IsOff";
		if (CourierAI.<>f__mg$cache9 == null)
		{
			CourierAI.<>f__mg$cache9 = new Func<Actor, int>(CourierAI.IsOff);
		}
		BehaviorNode<Actor> behaviorNode10 = new BehaviorNode<Actor>(name10, CourierAI.<>f__mg$cache9, false, -1);
		string name11 = "Loiter";
		if (CourierAI.<>f__mg$cacheA == null)
		{
			CourierAI.<>f__mg$cacheA = new Func<Actor, int>(CourierAI.Loiter);
		}
		BehaviorNode<Actor> behaviorNode11 = new BehaviorNode<Actor>(name11, CourierAI.<>f__mg$cacheA, true, 1);
		string name12 = "GoHome";
		if (CourierAI.<>f__mg$cacheB == null)
		{
			CourierAI.<>f__mg$cacheB = new Func<Actor, int>(Actor.GoHomeState);
		}
		BehaviorNode<Actor> behaviorNode12 = new BehaviorNode<Actor>(name12, CourierAI.<>f__mg$cacheB, true, -1);
		string name13 = "Despawn";
		if (CourierAI.<>f__mg$cacheC == null)
		{
			CourierAI.<>f__mg$cacheC = new Func<Actor, int>(CourierAI.Despawn);
		}
		BehaviorNode<Actor> behaviorNode13 = new BehaviorNode<Actor>(name13, CourierAI.<>f__mg$cacheC, true, -1);
		string name14 = "GoHomeBusStop";
		if (CourierAI.<>f__mg$cacheD == null)
		{
			CourierAI.<>f__mg$cacheD = new Func<Actor, int>(Actor.GoHomeBusStop);
		}
		BehaviorNode<Actor> behaviorNode14 = new BehaviorNode<Actor>(name14, CourierAI.<>f__mg$cacheD, true, -1);
		string name15 = "ShouldUseBus";
		if (CourierAI.<>f__mg$cacheE == null)
		{
			CourierAI.<>f__mg$cacheE = new Func<Actor, int>(Actor.ShouldUseBus);
		}
		BehaviorNode<Actor> behaviorNode15 = new BehaviorNode<Actor>(name15, CourierAI.<>f__mg$cacheE, true, -1);
		this.BehaviorNodes.Add("GoHomeBusStop", behaviorNode14);
		this.BehaviorNodes.Add("ShouldUseBus", behaviorNode15);
		this.BehaviorNodes.Add("Spawn", behaviorNode);
		this.BehaviorNodes.Add("HasCopies", behaviorNode2);
		this.BehaviorNodes.Add("GotoPrinter", behaviorNode3);
		this.BehaviorNodes.Add("TakePrinter", behaviorNode4);
		this.BehaviorNodes.Add("GotoPallet", behaviorNode5);
		this.BehaviorNodes.Add("TakePallet", behaviorNode6);
		this.BehaviorNodes.Add("GotoVan", behaviorNode7);
		this.BehaviorNodes.Add("DropProducts", behaviorNode8);
		this.BehaviorNodes.Add("AnyCopies", behaviorNode9);
		this.BehaviorNodes.Add("IsOff", behaviorNode10);
		this.BehaviorNodes.Add("Loiter", behaviorNode11);
		this.BehaviorNodes.Add("GoHome", behaviorNode12);
		this.BehaviorNodes.Add("Despawn", behaviorNode13);
		behaviorNode.Success = behaviorNode2;
		behaviorNode2.Success = behaviorNode7;
		behaviorNode2.Failure = behaviorNode5;
		behaviorNode5.Success = behaviorNode6;
		behaviorNode5.Failure = behaviorNode3;
		behaviorNode6.Success = behaviorNode2;
		behaviorNode3.Success = behaviorNode4;
		behaviorNode3.Failure = behaviorNode10;
		behaviorNode4.Success = behaviorNode2;
		behaviorNode9.Success = behaviorNode7;
		behaviorNode9.Failure = behaviorNode15;
		behaviorNode7.Success = behaviorNode8;
		behaviorNode8.Success = behaviorNode15;
		behaviorNode8.Failure = behaviorNode2;
		behaviorNode10.Success = behaviorNode9;
		behaviorNode10.Failure = behaviorNode11;
		behaviorNode11.Success = behaviorNode2;
		behaviorNode12.Success = behaviorNode13;
		behaviorNode12.Failure = behaviorNode14;
		behaviorNode13.Success = AI<Actor>.DummyNode;
		behaviorNode15.Success = behaviorNode14;
		behaviorNode15.Failure = behaviorNode12;
		behaviorNode14.Success = behaviorNode13;
		behaviorNode14.Failure = behaviorNode11;
	}

	private static int Spawn(Actor self)
	{
		if (self.CurrentPath == null)
		{
			return 2;
		}
		return (!self.WalkPath()) ? 1 : 2;
	}

	private static int HasCopies(Actor self)
	{
		return (!((!self.OnCall) ? (self.Boxes >= CourierAI.MaxBoxes / GameSettings.DaysPerMonth) : (self.Boxes >= CourierAI.MaxBoxes))) ? 0 : 2;
	}

	private static int GotoPrinter(Actor self)
	{
		int num = self.GoToFurniture("ProductPrinter", "Use", -1, false, null, false, true, delegate(Furniture x)
		{
			if (x == null)
			{
				return false;
			}
			ProductPrinter component = x.GetComponent<ProductPrinter>();
			return component != null && component.LocalOrder != null;
		});
		if (num == 2)
		{
			self.TurnToFurniture();
		}
		return num;
	}

	private static int TakePrinter(Actor self)
	{
		self.anim.SetInteger("AnimControl", 0);
		self.Order = ProductPrintOrder.Merge(new ProductPrintOrder[]
		{
			self.Order,
			self.UsingPoint.Parent.GetComponent<ProductPrinter>().Take()
		});
		if (self.OnCall)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(-CourierAI.BoxPrice, Company.TransactionCategory.Staff, "On call");
		}
		self.UsingPoint = null;
		self.Boxes++;
		return 2;
	}

	private static int GotoPallet(Actor self)
	{
		int num = self.GoToFurniture("Pallet", "Use", -1, false, null, false, true, (Furniture x) => x != null && x.GetComponent<ProductPallet>().CurrentAmount > 0);
		if (num == 2)
		{
			self.TurnToFurniture();
		}
		return num;
	}

	private static int TakePallet(Actor self)
	{
		int num = self.WaitForTimer(0.5f);
		if (num == 2 && self.UsingPoint != null)
		{
			self.anim.SetEnum("AnimControl", Actor.AnimationStates.Idle);
			ProductPallet component = self.UsingPoint.Parent.GetComponent<ProductPallet>();
			int num2 = 0;
			self.Order = ProductPrintOrder.Merge(new ProductPrintOrder[]
			{
				self.Order,
				component.Take(out num2, (!self.OnCall) ? (CourierAI.MaxBoxes / GameSettings.DaysPerMonth) : CourierAI.MaxBoxes)
			});
			if (self.OnCall)
			{
				GameSettings.Instance.MyCompany.MakeTransaction(-CourierAI.BoxPrice * (float)num2, Company.TransactionCategory.Staff, "On call");
			}
			self.Boxes += num2;
			self.UsingPoint = null;
		}
		else
		{
			self.anim.SetEnum("AnimControl", Actor.AnimationStates.PickBox);
		}
		return num;
	}

	private static int AnyCopies(Actor self)
	{
		return (self.Order.TotalCopies <= 0u) ? 0 : 2;
	}

	private static int GotoVan(Actor self)
	{
		if (self.MyCar == null)
		{
			self.CurrentPath = null;
			self.Boxes = 0;
			self.Order.Apply();
			self.Order = new ProductPrintOrder();
			return 2;
		}
		if (self.CurrentPath == null)
		{
			if (self.PathToPoint(self.MyCar.SpawnPoints[2].transform.position, true))
			{
				return 1;
			}
			self.Boxes = 0;
			self.Order.Apply();
			self.Order = new ProductPrintOrder();
			return 2;
		}
		else
		{
			if (self.WalkPath())
			{
				self.transform.rotation = self.MyCar.SpawnPoints[2].transform.rotation;
				return 2;
			}
			return 1;
		}
	}

	private static int DropProducts(Actor self)
	{
		if (self.MyCar == null)
		{
			self.CurrentPath = null;
			self.Boxes = 0;
			self.Order.Apply();
			self.Order = new ProductPrintOrder();
			return (!self.OnCall || self.GoHomeNow) ? 2 : 0;
		}
		if (self.MyCar.SpawnPoints[2].OpenAmount == 0f)
		{
			self.anim.SetEnum("AnimControl", Actor.AnimationStates.OpenVan);
		}
		else if (self.MyCar.SpawnPoints[2].OpenAmount == 1f)
		{
			self.anim.SetEnum("AnimControl", Actor.AnimationStates.Idle);
			self.CurrentPath = null;
			self.Boxes = 0;
			self.Order.Apply();
			self.Order = new ProductPrintOrder();
			self.MyCar.SpawnPoints[2].CloseDoor();
			return (!self.OnCall || self.GoHomeNow) ? 2 : 0;
		}
		return 1;
	}

	private static int Loiter(Actor self)
	{
		return self.HandleLoiter(false);
	}

	private static int IsOff(Actor self)
	{
		if (self.GoHomeNow || self.OnCall)
		{
			self.FreeLoiterTable();
			if (self.UsingPoint != null)
			{
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
			return 2;
		}
		return 0;
	}

	private static int Despawn(Actor self)
	{
		foreach (InteractionPoint interactionPoint in self.InQueue.Values)
		{
			interactionPoint.RemoveFromQueue(self);
		}
		self.InQueue.Clear();
		GameSettings.Instance.StaffSalaryDue += self.GetRealSalary() / (float)GameSettings.DaysPerMonth;
		if (self.OnCall)
		{
			UnityEngine.Object.Destroy(self.gameObject);
		}
		else
		{
			SDateTime meetingTime = self.MeetingTime;
			GameSettings.Instance.sActorManager.AddToAwaiting(self, new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), self.StaffOn - 1, meetingTime.Day + 1, meetingTime.Month, meetingTime.Year), false, true);
			self.OnDespawn();
		}
		return 2;
	}

	public static int MaxBoxes = 54;

	public static float BoxPrice = 125f;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache5;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache6;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache7;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache8;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache9;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheA;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheB;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheC;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheD;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheE;
}
