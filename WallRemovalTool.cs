﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class WallRemovalTool : MonoBehaviour
{
	private void Awake()
	{
		if (WallRemovalTool.Instance != null)
		{
			UnityEngine.Object.Destroy(WallRemovalTool.Instance.gameObject);
		}
		WallRemovalTool.Instance = this;
		this.mesh = new Mesh();
		this.mesh.MarkDynamic();
		base.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		if (WallRemovalTool.Instance == this)
		{
			WallRemovalTool.Instance = null;
		}
	}

	public void Show()
	{
		BuildController.Instance.ClearBuild(false, false, false, false);
		base.gameObject.SetActive(true);
		HUD.Instance.UpdateBorderOverlay();
	}

	private void BuildMesh()
	{
		Room room = this.Edge1.GetRoom(this.Edge2);
		Room room2 = this.Edge2.GetRoom(this.Edge1);
		WallEdge wallEdge = this.Edge1;
		do
		{
			WallEdge wallEdge2 = wallEdge.FindConnectionIn(room);
			if (wallEdge.Links.GetOrNull(room2) != wallEdge2)
			{
				break;
			}
			wallEdge = wallEdge2;
		}
		while (wallEdge != this.Edge1);
		List<Vector2> list = new List<Vector2>();
		int num = room.Edges.Count;
		do
		{
			list.Add(wallEdge.Pos);
			wallEdge = wallEdge.Links[room];
			num--;
		}
		while (num >= 0 && wallEdge.Links[room].Links.GetOrNull(room2) == wallEdge);
		list.Add(wallEdge.Pos);
		Vector3[] array = new Vector3[list.Count * 2];
		Vector2[] uv = new Vector2[list.Count * 2];
		int[] array2 = new int[(list.Count - 1) * 12];
		for (int i = 0; i < list.Count; i++)
		{
			array[i * 2] = new Vector3(list[i].x, (float)(room.Floor * 2), list[i].y);
			array[i * 2 + 1] = new Vector3(list[i].x, (float)(room.Floor * 2 + ((!room.Outdoors) ? 2 : 1)), list[i].y);
		}
		for (int j = 0; j < list.Count - 1; j++)
		{
			array2[j * 12] = j * 2;
			array2[j * 12 + 1] = j * 2 + 1;
			array2[j * 12 + 2] = j * 2 + 3;
			array2[j * 12 + 3] = j * 2 + 3;
			array2[j * 12 + 4] = j * 2 + 2;
			array2[j * 12 + 5] = j * 2;
			array2[j * 12 + 6] = j * 2;
			array2[j * 12 + 7] = j * 2 + 2;
			array2[j * 12 + 8] = j * 2 + 3;
			array2[j * 12 + 9] = j * 2 + 3;
			array2[j * 12 + 10] = j * 2 + 1;
			array2[j * 12 + 11] = j * 2;
		}
		this.mesh.triangles = new int[0];
		this.mesh.vertices = array;
		this.mesh.uv = uv;
		this.mesh.triangles = array2;
		int num2 = list.Count / 2 - 1;
		Vector2 v = (list[num2] + list[num2 + 1]) * 0.5f;
		base.transform.position = v.ToVector3((float)GameSettings.Instance.ActiveFloor * 2f + 2.1f);
		Vector3 a = (list[num2] - list[num2 + 1]).ToVector3(0f);
		base.transform.rotation = Quaternion.LookRotation(-a, Vector3.up);
	}

	private void OnDisable()
	{
		if (HUD.Instance != null)
		{
			HUD.Instance.UpdateBorderOverlay();
		}
	}

	private void Update()
	{
		if (Input.GetMouseButtonUp(1))
		{
			base.gameObject.SetActive(false);
		}
		Vector2 mouseProj = HUD.Instance.GetMouseProj(1f, true);
		if (this.Edge1 == null)
		{
			foreach (WallEdge wallEdge in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(GameSettings.Instance.ActiveFloor))
			{
				if (wallEdge.Links.Count > 1)
				{
					foreach (KeyValuePair<Room, WallEdge> keyValuePair in wallEdge.Links)
					{
						foreach (KeyValuePair<Room, WallEdge> keyValuePair2 in keyValuePair.Value.Links)
						{
							if (keyValuePair2.Key.Outdoors == keyValuePair.Key.Outdoors && keyValuePair2.Value == wallEdge)
							{
								Vector2? vector = Utilities.ProjectToLine(mouseProj, wallEdge.Pos, keyValuePair.Value.Pos);
								if (vector != null && (vector.Value - mouseProj).magnitude <= this.DistanceLimit)
								{
									if (!keyValuePair.Key.TryFixEdges() || !keyValuePair2.Key.TryFixEdges())
									{
										this.Edge1 = null;
										this.Edge2 = null;
										return;
									}
									this.Edge1 = wallEdge;
									this.Edge2 = keyValuePair.Value;
									this.Valid = keyValuePair.Key.CanMerge(keyValuePair2.Key);
									if (this.Valid)
									{
										this.BuildMesh();
									}
									this.rend.enabled = this.Valid;
									break;
								}
							}
						}
						if (this.Edge1 != null)
						{
							break;
						}
					}
					if (this.Edge1 != null)
					{
						break;
					}
				}
			}
		}
		else
		{
			Vector2? vector2 = Utilities.ProjectToLine(mouseProj, this.Edge1.Pos, this.Edge2.Pos);
			if (vector2 == null || (vector2.Value - mouseProj).magnitude > this.DistanceLimit)
			{
				this.Edge1 = null;
				this.rend.enabled = false;
			}
			if (this.Edge1 != null && this.Valid)
			{
				Room room = this.Edge1.GetRoom(this.Edge2);
				if (room == null)
				{
					this.Edge1 = null;
					this.rend.enabled = false;
				}
				else
				{
					if (!room.IsInside(mouseProj))
					{
						WallEdge edge = this.Edge1;
						this.Edge1 = this.Edge2;
						this.Edge2 = edge;
						base.transform.rotation *= Quaternion.Euler(0f, 180f, 0f);
					}
					Graphics.DrawMesh(this.mesh, Vector3.zero, Quaternion.identity, this.Mat, 0, CameraScript.Instance.mainCam);
					if (Input.GetMouseButtonDown(0))
					{
						Room room2 = this.Edge1.GetRoom(this.Edge2);
						Room room3 = this.Edge2.GetRoom(this.Edge1);
						List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
						Dictionary<WallSnap, UndoObject.UndoAction> snaps = room2.PrepareSplit(true);
						List<Vector2> split = room2.MergeWith(room3, snaps, list);
						list.Insert(0, new UndoObject.UndoAction(room2, room3, split));
						GameSettings.Instance.AddUndo(list.ToArray());
						this.Edge1 = null;
						this.Edge2 = null;
						this.rend.enabled = false;
					}
				}
			}
		}
	}

	public static WallRemovalTool Instance;

	public WallEdge Edge1;

	public WallEdge Edge2;

	public float DistanceLimit;

	private bool Valid;

	private Mesh mesh;

	public Material Mat;

	public MeshRenderer rend;
}
