﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class ConfigFile
{
	public List<string> this[string key]
	{
		get
		{
			List<string> result = null;
			this.Values.TryGetValue(key, out result);
			return result;
		}
		set
		{
			this.Values[key] = value;
		}
	}

	public static ConfigFile FromDictionary(Dictionary<string, string> dict)
	{
		ConfigFile configFile = new ConfigFile();
		foreach (KeyValuePair<string, string> keyValuePair in dict)
		{
			configFile[keyValuePair.Key] = new List<string>
			{
				keyValuePair.Value
			};
		}
		return configFile;
	}

	public void Add(string key, string value)
	{
		if (this.Values.ContainsKey(key))
		{
			this.Values[key].Add(value);
		}
		else
		{
			this.Values[key] = new List<string>
			{
				value
			};
		}
	}

	public void AddRange(string key, IEnumerable<string> values)
	{
		if (this.Values.ContainsKey(key))
		{
			this.Values[key].AddRange(values);
		}
		else
		{
			this.Values[key] = values.ToList<string>();
		}
	}

	public string Get(string key)
	{
		return this.GetOrDefault(key, "Error");
	}

	public string Get(string key, string def)
	{
		return this.GetOrDefault(key, def);
	}

	public string GetOrDefault(string key, string def)
	{
		List<string> list = this[key];
		if (list != null && list.Count > 0)
		{
			return list[0];
		}
		return def;
	}

	public static ConfigFile Load(string lines)
	{
		return ConfigFile.Load(lines.Split(new string[]
		{
			Environment.NewLine,
			"\r\n",
			"\n"
		}, StringSplitOptions.RemoveEmptyEntries));
	}

	public static ConfigFile Load(string[] lines)
	{
		ConfigFile configFile = new ConfigFile();
		string text = null;
		List<string> list = new List<string>();
		foreach (string text2 in lines)
		{
			if (text2.Length > 0 && text2[0] == '[' && text != null)
			{
				configFile[text] = list.ToList<string>();
				list.Clear();
				text = null;
			}
			if (text == null)
			{
				int num = text2.LastIndexOf(']');
				if (text2.Length > 0 && text2[0] == '[' && num > 1)
				{
					text = text2.Substring(1, num - 1);
				}
			}
			else
			{
				list.Add(text2);
			}
		}
		if (text != null)
		{
			configFile[text] = list;
		}
		return configFile;
	}

	public string Serialize()
	{
		StringBuilder stringBuilder = new StringBuilder();
		foreach (KeyValuePair<string, List<string>> keyValuePair in this.Values)
		{
			stringBuilder.AppendLine("[" + keyValuePair.Key + "]");
			foreach (string value in keyValuePair.Value)
			{
				stringBuilder.AppendLine(value);
			}
		}
		return stringBuilder.ToString();
	}

	public Dictionary<string, List<string>> Values = new Dictionary<string, List<string>>();
}
