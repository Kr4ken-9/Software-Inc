﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MoneyPanel : MonoBehaviour, IPointerEnterHandler, IEventSystemHandler
{
	private void Update()
	{
		if (this.IsOpen)
		{
			if (this.DropDownPanel.sizeDelta.y == 0f)
			{
				TweenSettingsExtensions.SetEase<Tweener>(ShortcutExtensions46.DOSizeDelta(this.DropDownPanel, new Vector2(this.DropDownPanel.sizeDelta.x, 174f), 0.2f, true), 6);
			}
			float money = GameSettings.Instance.Insurance.Money;
			float num = -GameSettings.Instance.Loans.Sum((KeyValuePair<int, float> x) => (float)x.Key * x.Value);
			float num2 = GameSettings.Instance.MyCompany.Subsidiaries.SumSafe((uint x) => GameSettings.Instance.simulation.GetCompany(x).GetMoneyWithInsurance(false));
			float num3 = (from x in GameSettings.Instance.MyCompany.OwnedStock
			where x.Target != x.Owner
			select x).Sum((Stock x) => x.CurrentWorth);
			float num4;
			if (GameSettings.Instance.RentMode)
			{
				num4 = 0f;
			}
			else
			{
				num4 = GameSettings.Instance.PlayerPlots.SumSafe((PlotArea x) => x.Price - x.Monthly * (float)x.MonthsLeft);
			}
			float num5 = num4;
			this.MoneyText[0].text = money.Currency(true);
			this.MoneyText[1].text = num3.Currency(true);
			this.MoneyText[2].text = num.Currency(true);
			this.MoneyText[3].text = num2.Currency(true);
			this.MoneyText[4].text = num5.Currency(true);
			this.MoneyText[5].text = (GameSettings.Instance.MyCompany.Money + money + num + num3 + num2 + num5).Currency(true);
			if (!RectTransformUtility.RectangleContainsScreenPoint(this.DropDownPanel, Input.mousePosition, null) && !RectTransformUtility.RectangleContainsScreenPoint(this.SelfRect, Input.mousePosition, null))
			{
				this.IsOpen = false;
			}
		}
		else if (this.DropDownPanel.sizeDelta.y == 174f)
		{
			TweenSettingsExtensions.SetEase<Tweener>(ShortcutExtensions46.DOSizeDelta(this.DropDownPanel, new Vector2(this.DropDownPanel.sizeDelta.x, 0f), 0.2f, true), 6);
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		this.IsOpen = true;
	}

	public Text[] MoneyText;

	public RectTransform DropDownPanel;

	public RectTransform SelfRect;

	private bool IsOpen;
}
