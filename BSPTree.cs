﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BSPTree<T>
{
	public BSPTree(int maxNodes, int maxLevels, Func<T, bool, float, int> comp, Func<List<T>, bool, float> med)
	{
		this.MaxNodes = maxNodes;
		this.MaxLevels = maxLevels;
		this.Comparer = comp;
		this.Median = med;
	}

	public List<T> GetNodes(Vector2 p)
	{
		if (this.Smaller == null)
		{
			return this.Nodes;
		}
		float num = (!this.Vertical) ? p.y : p.x;
		if (num > this.Middle)
		{
			return this.Larger.GetNodes(p);
		}
		return this.Smaller.GetNodes(p);
	}

	public void AddNode(T node)
	{
		if (this.Smaller != null)
		{
			int num = this.Comparer(node, this.Vertical, this.Middle);
			if (num <= 0)
			{
				this.Smaller.AddNode(node);
			}
			if (num >= 0)
			{
				this.Larger.AddNode(node);
			}
		}
		else
		{
			this.Nodes.Add(node);
			if (this.Nodes.Count >= this.MaxNodes && this.MaxLevels > 0)
			{
				this.Smaller = new BSPTree<T>(this.MaxNodes, this.MaxLevels - 1, this.Comparer, this.Median);
				this.Smaller.Vertical = !this.Vertical;
				this.Larger = new BSPTree<T>(this.MaxNodes, this.MaxLevels - 1, this.Comparer, this.Median);
				this.Larger.Vertical = !this.Vertical;
				this.Middle = this.Median(this.Nodes, this.Vertical);
				for (int i = 0; i < this.Nodes.Count; i++)
				{
					this.AddNode(this.Nodes[i]);
				}
				this.Nodes.Clear();
			}
		}
	}

	public int MaxNodes;

	public int MaxLevels;

	public List<T> Nodes = new List<T>();

	public Func<T, bool, float, int> Comparer;

	public Func<List<T>, bool, float> Median;

	public float Middle;

	public bool Vertical;

	public BSPTree<T> Smaller;

	public BSPTree<T> Larger;
}
