﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class InsuranceWindow : MonoBehaviour
{
	public void Show(bool toggle = true)
	{
		if (this.Window.Shown && toggle)
		{
			this.Window.Close();
			return;
		}
		this._newRetires = 0;
		this.RetireCounter.SetNumber(0);
		this.UpdateTexts();
		this.Window.Show();
		TutorialSystem.Instance.StartTutorial("Savings account", false);
	}

	private void Update()
	{
		InsuranceAccount insAcc = GameSettings.Instance.Insurance;
		float num = (from x in insAcc.Deposits
		where Utilities.GetMonthsFlat(x.Value, SDateTime.Now()) > 0
		select x).SumSafe((KeyValuePair<float, SDateTime> x) => x.Key + insAcc.GetDepositInterest(x));
		if (num > 0f)
		{
			this.FundsText.text = string.Concat(new string[]
			{
				"SavingsFunds".Loc(),
				": ",
				Mathf.Floor(insAcc.Money).Currency(true),
				" - ",
				"Free to withdraw".Loc(),
				": ",
				Mathf.Floor(insAcc.Money - num).Currency(true)
			});
		}
		else
		{
			this.FundsText.text = "SavingsFunds".Loc() + ": " + Mathf.Floor(insAcc.Money).Currency(true);
		}
		insAcc.GetDeposits(this.InterestText[0], this.InterestText[1], this.InterestText[2]);
	}

	public void UpdateTexts()
	{
		InsuranceAccount insurance = GameSettings.Instance.Insurance;
		Company myCompany = GameSettings.Instance.MyCompany;
		float maxWithdraw = insurance.GetMaxWithdraw();
		float num = this.valueSlider.value.MapRange(0f, 1f, 0f, Mathf.Max(0f, myCompany.Money - 100f), false);
		float num2 = this.valueSlider.value.MapRange(0f, 1f, 0f, Mathf.Max(0f, maxWithdraw), false);
		if (this.Round && this.valueSlider.value != 1f)
		{
			float num3 = Mathf.Max(1f, Mathf.Pow(10f, Mathf.Floor(Mathf.Log10(myCompany.Money - 100f))) / 10f);
			num = Mathf.Clamp(Mathf.Round(num / num3) * num3, 0f, myCompany.Money - 100f);
			float num4 = Mathf.Max(1f, Mathf.Pow(10f, Mathf.Floor(Mathf.Log10(maxWithdraw))) / 10f);
			num2 = Mathf.Clamp(Mathf.Round(num2 / num4) * num4, 0f, maxWithdraw);
		}
		if (!num2.IsValidFloat())
		{
			num2 = 0f;
		}
		if (!num.IsValidFloat())
		{
			num = 0f;
		}
		this.DepositInput.text = num.CurrencyMul().ToString("N0");
		this.WithdrawInput.text = num2.CurrencyMul().ToString("N0");
		this.Round = true;
	}

	public void AddRetire()
	{
		this._newRetires++;
		this.RetireCounter.SetNumber(this._newRetires);
	}

	public void FixInputfields(int type)
	{
		InsuranceAccount insurance = GameSettings.Instance.Insurance;
		Company myCompany = GameSettings.Instance.MyCompany;
		if (type == 0)
		{
			try
			{
				float num = (float)Convert.ToDouble(this.DepositInput.text);
				num = num.FromCurrency();
				float num2 = myCompany.Money - 100f;
				if (num2 > 0f)
				{
					num = Mathf.Clamp(num, 0f, num2);
					this.ResetTexts(num / num2);
				}
				else
				{
					this.ResetTexts(0f);
				}
			}
			catch (Exception)
			{
				this.ResetTexts(0f);
			}
		}
		if (type == 1)
		{
			try
			{
				float num3 = (float)Convert.ToDouble(this.WithdrawInput.text);
				num3 = num3.FromCurrency();
				float maxWithdraw = insurance.GetMaxWithdraw();
				if (maxWithdraw > 0f)
				{
					num3 = Mathf.Clamp(num3, 0f, maxWithdraw);
					this.ResetTexts(num3 / maxWithdraw);
				}
				else
				{
					this.ResetTexts(0f);
				}
			}
			catch (Exception)
			{
				this.ResetTexts(0f);
			}
		}
	}

	public void Deposit()
	{
		Company myCompany = GameSettings.Instance.MyCompany;
		try
		{
			float num = ((float)Convert.ToDouble(this.DepositInput.text)).FromCurrency();
			num = Mathf.Clamp(num, 0f, myCompany.Money - 100f);
			if (num > 0f)
			{
				GameSettings.Instance.Insurance.Deposit(num);
				GameSettings.Instance.Insurance.ChangeAmount(num);
				this.ResetTexts(0f);
			}
		}
		catch (Exception)
		{
		}
	}

	private void ResetTexts(float val = 0f)
	{
		this.Round = false;
		this.valueSlider.value = val;
		this.Round = false;
		this.UpdateTexts();
	}

	public void Withdraw()
	{
		InsuranceAccount insAcc = GameSettings.Instance.Insurance;
		try
		{
			float with = ((float)Convert.ToDouble(this.WithdrawInput.text)).FromCurrency();
			with = Mathf.Clamp(with, 0f, insAcc.GetMaxWithdraw());
			if (with > 0f)
			{
				float withdrawCost = insAcc.GetWithdrawCost(with);
				if (withdrawCost > 0f)
				{
					WindowManager.Instance.ShowMessageBox("WithdrawCost".Loc(new object[]
					{
						with.Currency(true),
						withdrawCost.Currency(true)
					}), true, DialogWindow.DialogType.Question, delegate
					{
						insAcc.Withdraw(with);
						GameSettings.Instance.Insurance.ChangeAmount(-with);
						this.ResetTexts(0f);
					}, null, null);
				}
				else
				{
					insAcc.Withdraw(with);
					GameSettings.Instance.Insurance.ChangeAmount(-with);
					this.ResetTexts(0f);
				}
			}
		}
		catch (Exception)
		{
		}
	}

	public GUIWindow Window;

	public Slider valueSlider;

	public Text FundsText;

	public Text[] InterestText;

	public InputField DepositInput;

	public InputField WithdrawInput;

	public GUIListView Terminations;

	public ButtonCounter RetireCounter;

	public bool Round = true;

	[NonSerialized]
	private int _newRetires;
}
