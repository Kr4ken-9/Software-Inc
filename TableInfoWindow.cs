﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TableInfoWindow : MonoBehaviour
{
	public void Init(string title, string header, string[] var, string[] value, string ID)
	{
		this.Sheet.SetData(var, value);
		this.Window.NonLocTitle = title;
		this.Header.text = header;
		if (ID != null)
		{
			this.Window.OnClose = delegate
			{
				WindowManager.DeregisterTableInfoWindow(ID);
			};
		}
	}

	public GUIWindow Window;

	public VarValueSheet Sheet;

	public Text Header;
}
