﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class FPSLabel : MonoBehaviour
{
	private void Start()
	{
		this.timeleft = this.updateInterval;
		this.d = Time.realtimeSinceStartup;
	}

	private void Update()
	{
		float num = Time.realtimeSinceStartup - this.d;
		this.d = Time.realtimeSinceStartup;
		this.timeleft -= num;
		this.accum += Time.timeScale / num;
		this.frames++;
		if ((double)this.timeleft <= 0.0)
		{
			float num2 = this.accum / (float)this.frames;
			this.textElement.text = num2.ToString("F2");
			if (num2 < 30f)
			{
				this.textElement.color = Color.yellow * 0.5f;
			}
			else if (num2 < 10f)
			{
				this.textElement.color = Color.red * 0.5f;
			}
			else
			{
				this.textElement.color = Color.green * 0.5f;
			}
			this.timeleft = this.updateInterval;
			this.accum = 0f;
			this.frames = 0;
		}
	}

	public Text textElement;

	public float updateInterval = 0.5f;

	private float accum;

	private int frames;

	private float timeleft;

	private float d;
}
