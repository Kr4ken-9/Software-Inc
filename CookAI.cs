﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CookAI : AI<Actor>
{
	public override void InitTree()
	{
		string name = "Spawn";
		if (CookAI.<>f__mg$cache0 == null)
		{
			CookAI.<>f__mg$cache0 = new Func<Actor, int>(CookAI.Spawn);
		}
		BehaviorNode<Actor> behaviorNode = new BehaviorNode<Actor>(name, CookAI.<>f__mg$cache0, true, -1);
		string name2 = "GoHome";
		if (CookAI.<>f__mg$cache1 == null)
		{
			CookAI.<>f__mg$cache1 = new Func<Actor, int>(Actor.GoHomeState);
		}
		BehaviorNode<Actor> behaviorNode2 = new BehaviorNode<Actor>(name2, CookAI.<>f__mg$cache1, true, -1);
		string name3 = "Despawn";
		if (CookAI.<>f__mg$cache2 == null)
		{
			CookAI.<>f__mg$cache2 = new Func<Actor, int>(CookAI.Despawn);
		}
		BehaviorNode<Actor> behaviorNode3 = new BehaviorNode<Actor>(name3, CookAI.<>f__mg$cache2, true, -1);
		string name4 = "IsOff";
		if (CookAI.<>f__mg$cache3 == null)
		{
			CookAI.<>f__mg$cache3 = new Func<Actor, int>(CookAI.IsOff);
		}
		BehaviorNode<Actor> behaviorNode4 = new BehaviorNode<Actor>(name4, CookAI.<>f__mg$cache3, false, -1);
		string name5 = "CanCook";
		if (CookAI.<>f__mg$cache4 == null)
		{
			CookAI.<>f__mg$cache4 = new Func<Actor, int>(CookAI.CanCook);
		}
		BehaviorNode<Actor> behaviorNode5 = new BehaviorNode<Actor>(name5, CookAI.<>f__mg$cache4, false, -1);
		string name6 = "FindFridge";
		if (CookAI.<>f__mg$cache5 == null)
		{
			CookAI.<>f__mg$cache5 = new Func<Actor, int>(CookAI.FindFridge);
		}
		BehaviorNode<Actor> behaviorNode6 = new BehaviorNode<Actor>(name6, CookAI.<>f__mg$cache5, true, -1);
		string name7 = "GetFood";
		if (CookAI.<>f__mg$cache6 == null)
		{
			CookAI.<>f__mg$cache6 = new Func<Actor, int>(CookAI.GetFood);
		}
		BehaviorNode<Actor> behaviorNode7 = new BehaviorNode<Actor>(name7, CookAI.<>f__mg$cache6, true, -1);
		string name8 = "CookFood";
		if (CookAI.<>f__mg$cache7 == null)
		{
			CookAI.<>f__mg$cache7 = new Func<Actor, int>(CookAI.CookFood);
		}
		BehaviorNode<Actor> behaviorNode8 = new BehaviorNode<Actor>(name8, CookAI.<>f__mg$cache7, false, -1);
		string name9 = "FindStove";
		if (CookAI.<>f__mg$cache8 == null)
		{
			CookAI.<>f__mg$cache8 = new Func<Actor, int>(CookAI.FindStove);
		}
		BehaviorNode<Actor> behaviorNode9 = new BehaviorNode<Actor>(name9, CookAI.<>f__mg$cache8, true, -1);
		string name10 = "FoodReady";
		if (CookAI.<>f__mg$cache9 == null)
		{
			CookAI.<>f__mg$cache9 = new Func<Actor, int>(CookAI.FoodReady);
		}
		BehaviorNode<Actor> behaviorNode10 = new BehaviorNode<Actor>(name10, CookAI.<>f__mg$cache9, false, -1);
		string name11 = "FetchFood";
		if (CookAI.<>f__mg$cacheA == null)
		{
			CookAI.<>f__mg$cacheA = new Func<Actor, int>(CookAI.FetchFood);
		}
		BehaviorNode<Actor> behaviorNode11 = new BehaviorNode<Actor>(name11, CookAI.<>f__mg$cacheA, true, -1);
		string name12 = "FindTray";
		if (CookAI.<>f__mg$cacheB == null)
		{
			CookAI.<>f__mg$cacheB = new Func<Actor, int>(CookAI.FindTray);
		}
		BehaviorNode<Actor> behaviorNode12 = new BehaviorNode<Actor>(name12, CookAI.<>f__mg$cacheB, true, -1);
		string name13 = "GotoTray";
		if (CookAI.<>f__mg$cacheC == null)
		{
			CookAI.<>f__mg$cacheC = new Func<Actor, int>(CookAI.GotoTray);
		}
		BehaviorNode<Actor> behaviorNode13 = new BehaviorNode<Actor>(name13, CookAI.<>f__mg$cacheC, true, -1);
		string name14 = "Loiter";
		if (CookAI.<>f__mg$cacheD == null)
		{
			CookAI.<>f__mg$cacheD = new Func<Actor, int>(CookAI.Loiter);
		}
		BehaviorNode<Actor> behaviorNode14 = new BehaviorNode<Actor>(name14, CookAI.<>f__mg$cacheD, true, 1);
		string name15 = "CookLoiter";
		if (CookAI.<>f__mg$cacheE == null)
		{
			CookAI.<>f__mg$cacheE = new Func<Actor, int>(CookAI.CookLoiter);
		}
		BehaviorNode<Actor> behaviorNode15 = new BehaviorNode<Actor>(name15, CookAI.<>f__mg$cacheE, true, -1);
		string name16 = "GoHomeBusStop";
		if (CookAI.<>f__mg$cacheF == null)
		{
			CookAI.<>f__mg$cacheF = new Func<Actor, int>(Actor.GoHomeBusStop);
		}
		BehaviorNode<Actor> behaviorNode16 = new BehaviorNode<Actor>(name16, CookAI.<>f__mg$cacheF, true, -1);
		string name17 = "ShouldUseBus";
		if (CookAI.<>f__mg$cache10 == null)
		{
			CookAI.<>f__mg$cache10 = new Func<Actor, int>(Actor.ShouldUseBus);
		}
		BehaviorNode<Actor> behaviorNode17 = new BehaviorNode<Actor>(name17, CookAI.<>f__mg$cache10, true, -1);
		this.BehaviorNodes.Add("Spawn", behaviorNode);
		this.BehaviorNodes.Add("GoHome", behaviorNode2);
		this.BehaviorNodes.Add("Despawn", behaviorNode3);
		this.BehaviorNodes.Add("IsOff", behaviorNode4);
		this.BehaviorNodes.Add("CanCook", behaviorNode5);
		this.BehaviorNodes.Add("FindFridge", behaviorNode6);
		this.BehaviorNodes.Add("GetFood", behaviorNode7);
		this.BehaviorNodes.Add("CookFood", behaviorNode8);
		this.BehaviorNodes.Add("FindStove", behaviorNode9);
		this.BehaviorNodes.Add("FoodReady", behaviorNode10);
		this.BehaviorNodes.Add("FetchFood", behaviorNode11);
		this.BehaviorNodes.Add("FindTray", behaviorNode12);
		this.BehaviorNodes.Add("GotoTray", behaviorNode13);
		this.BehaviorNodes.Add("Loiter", behaviorNode14);
		this.BehaviorNodes.Add("CookLoiter", behaviorNode15);
		this.BehaviorNodes.Add("GoHomeBusStop", behaviorNode16);
		this.BehaviorNodes.Add("ShouldUseBus", behaviorNode17);
		behaviorNode.Success = behaviorNode8;
		behaviorNode5.Success = behaviorNode6;
		behaviorNode5.Failure = behaviorNode15;
		behaviorNode6.Success = behaviorNode7;
		behaviorNode6.Failure = behaviorNode4;
		behaviorNode7.Success = behaviorNode4;
		behaviorNode8.Success = behaviorNode9;
		behaviorNode8.Failure = behaviorNode10;
		behaviorNode9.Success = behaviorNode10;
		behaviorNode9.Failure = behaviorNode5;
		behaviorNode10.Success = behaviorNode11;
		behaviorNode10.Failure = behaviorNode5;
		behaviorNode11.Success = behaviorNode12;
		behaviorNode11.Failure = behaviorNode15;
		behaviorNode12.Success = behaviorNode13;
		behaviorNode12.Failure = behaviorNode15;
		behaviorNode13.Success = behaviorNode10;
		behaviorNode13.Failure = behaviorNode12;
		behaviorNode15.Success = behaviorNode4;
		behaviorNode15.Failure = behaviorNode14;
		behaviorNode14.Success = behaviorNode4;
		behaviorNode4.Success = behaviorNode17;
		behaviorNode4.Failure = behaviorNode8;
		behaviorNode2.Success = behaviorNode3;
		behaviorNode2.Failure = behaviorNode16;
		behaviorNode3.Success = AI<Actor>.DummyNode;
		behaviorNode17.Success = behaviorNode16;
		behaviorNode17.Failure = behaviorNode2;
		behaviorNode16.Success = behaviorNode3;
		behaviorNode16.Failure = behaviorNode14;
	}

	private static int Spawn(Actor self)
	{
		if (self.CurrentPath == null)
		{
			return 2;
		}
		return (!self.WalkPath()) ? 1 : 2;
	}

	private static int Despawn(Actor self)
	{
		GameSettings.Instance.StaffSalaryDue += self.GetRealSalary() / (float)GameSettings.DaysPerMonth;
		foreach (InteractionPoint interactionPoint in self.InQueue.Values)
		{
			interactionPoint.RemoveFromQueue(self);
		}
		self.InQueue.Clear();
		if (self.OnCall)
		{
			UnityEngine.Object.Destroy(self.gameObject);
		}
		else
		{
			SDateTime sdateTime = SDateTime.Now();
			GameSettings.Instance.sActorManager.AddToAwaiting(self, new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), self.StaffOn - 1, sdateTime.Day + ((self.StaffOn <= TimeOfDay.Instance.Hour) ? 1 : 0), sdateTime.Month, sdateTime.Year), false, true);
			self.OnDespawn();
		}
		return 2;
	}

	private static int IsOff(Actor self)
	{
		if (self.GoHomeNow)
		{
			self.FreeLoiterTable();
			if (self.UsingPoint != null)
			{
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
			if (self.Holding[0] != null)
			{
				self.LeaveItem(self.Holding[0], true);
			}
			if (self.Reserved != null)
			{
				CookAI.ClearReserved(self);
			}
			return 2;
		}
		return 0;
	}

	private static void ClearReserved(Actor self)
	{
		if (self.Reserved != null)
		{
			List<KeyValuePair<Transform, Holdable>> list = self.Reserved.Holdables.ToList<KeyValuePair<Transform, Holdable>>();
			for (int i = 0; i < list.Count; i++)
			{
				KeyValuePair<Transform, Holdable> keyValuePair = list[i];
				if (keyValuePair.Value != null)
				{
					keyValuePair.Value.GetComponent<Holdable>().DestroyMe();
					self.Reserved.Holdables[keyValuePair.Key] = null;
				}
			}
			self.Reserved.Reserved = null;
			self.Reserved = null;
		}
	}

	private static int CanCook(Actor self)
	{
		if (self.Holding[0] != null)
		{
			return 0;
		}
		if (self.Reserved == null)
		{
			if (self.Owns.Count > 0)
			{
				foreach (Furniture furn in self.Owns)
				{
					if (CookAI.CheckStove(self, furn, true))
					{
						break;
					}
				}
			}
			if (self.Reserved == null)
			{
				if (self.HasAssignedRooms)
				{
					foreach (Room room in self.GetAssignedRooms())
					{
						CookAI.CheckRoomStove(self, room);
						if (self.Reserved != null)
						{
							break;
						}
					}
				}
				else
				{
					self.UpdateCurrentRoom(false);
					List<KeyValuePair<Room, int>> connectedRooms = GameSettings.Instance.sRoomManager.GetConnectedRooms(self.currentRoom);
					for (int i = 0; i < connectedRooms.Count; i++)
					{
						CookAI.CheckRoomStove(self, connectedRooms[i].Key);
						if (self.Reserved != null)
						{
							break;
						}
					}
				}
			}
			if (self.Reserved == null)
			{
				HUD.Instance.AddPopupMessage("CookStoveWarning".Loc(), "Exclamation", PopupManager.PopUpAction.GotoEmp, self.DID, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.CookStove, 1);
			}
		}
		return (!(self.Reserved != null) || !self.Reserved.CanPlaceHoldable()) ? 0 : 2;
	}

	private static void CheckRoomStove(Actor self, Room room)
	{
		HashList<Furniture> furniture = room.GetFurniture("Stove");
		for (int i = 0; i < furniture.Count; i++)
		{
			if (CookAI.CheckStove(self, furniture[i], false))
			{
				break;
			}
		}
	}

	private static bool CheckStove(Actor self, Furniture furn, bool checkRoom)
	{
		if (CookAI.CheckReservation(self, furn) && (!checkRoom || !self.HasAssignedRooms || self.IsAssignedRoom(furn.Parent)))
		{
			InteractionPoint interactionPoint = furn.GetInteractionPoint(self, "Use");
			if (interactionPoint != null)
			{
				Vector2 point = interactionPoint.Point;
				Vector3 endV = new Vector3(point.x, (float)(furn.Parent.Floor * 2), point.y);
				if (GameSettings.Instance.sRoomManager.FindPath(self.transform.position, endV, null, Employee.RoleBit.None, true) != null)
				{
					furn.Reserved = self;
					self.Reserved = furn;
					return true;
				}
			}
		}
		return false;
	}

	private static bool CheckReservation(Actor self, Furniture furn)
	{
		if (!(furn.Reserved != null))
		{
			return true;
		}
		if (furn.Reserved == self)
		{
			return true;
		}
		if (furn.Reserved.Reserved != furn)
		{
			furn.Reserved = null;
			return true;
		}
		return false;
	}

	private static int FindFridge(Actor self)
	{
		int num = self.GoToFurniture("Fridge", "Use", -1, false, null, false, true, null);
		if (num == 2)
		{
			self.FreeLoiterTable();
			self.anim.SetInteger("AnimControl", 2);
			self.TurnToFurniture();
		}
		else if (num == 0)
		{
			self.anim.SetInteger("AnimControl", 0);
		}
		return num;
	}

	private static int GetFood(Actor self)
	{
		int num = self.WaitForTimer(2f);
		if (num == 2)
		{
			self.anim.SetInteger("AnimControl", 0);
			self.GetItem("Pot", true);
			self.anim.SetInteger("AnimControl", 0);
			if (self.UsingPoint != null)
			{
				self.UsingPoint.Parent.InteractEnd();
				self.UsingPoint = null;
			}
		}
		return num;
	}

	private static int CookFood(Actor self)
	{
		if (self.Reserved)
		{
			return (!(self.Holding[0] != null) || !self.Holding[0].name.Equals("Pot")) ? 0 : 2;
		}
		if (self.Holding[0] != null && self.Holding[0].name.Equals("Pot"))
		{
			self.LeaveItem(self.Holding[0], true);
		}
		return 0;
	}

	private static int FindStove(Actor self)
	{
		if (self.Reserved == null)
		{
			self.UsingPoint = null;
			self.CurrentPath = null;
			return 0;
		}
		if (self.CurrentPath == null)
		{
			InteractionPoint interactionPoint = self.Reserved.GetInteractionPoint(self, "Use");
			if (interactionPoint != null && self.PathToFurniture(interactionPoint, false))
			{
				self.FreeLoiterTable();
				self.UsingPoint = interactionPoint;
				return 1;
			}
			CookAI.ClearReserved(self);
			self.anim.SetInteger("AnimControl", 0);
			return 0;
		}
		else
		{
			if (self.WalkPath())
			{
				self.TurnToFurniture();
				Holdable holdable = self.Holding[0];
				self.LeaveItem(holdable, false);
				self.Reserved.PlaceHoldable(holdable);
				self.UsingPoint = null;
				return 2;
			}
			return 1;
		}
	}

	private static int FoodReady(Actor self)
	{
		if (self.Holding[0] != null)
		{
			if (self.Holding[0].name.Equals("Pot"))
			{
				return 0;
			}
			return 2;
		}
		else
		{
			if (self.Reserved != null && self.Reserved.GetComponent<StoveScript>().HasReady())
			{
				self.FreeLoiterTable();
				return 2;
			}
			return 0;
		}
	}

	private static int FetchFood(Actor self)
	{
		if (self.Reserved == null)
		{
			self.UsingPoint = null;
			self.CurrentPath = null;
			return 0;
		}
		if (self.Holding[0] != null)
		{
			return 2;
		}
		if (self.CurrentPath == null)
		{
			InteractionPoint interactionPoint = self.Reserved.GetInteractionPoint(self, "Use");
			if (interactionPoint != null && self.PathToFurniture(interactionPoint, false))
			{
				return 1;
			}
			return 0;
		}
		else
		{
			if (!self.WalkPath())
			{
				return 1;
			}
			self.TurnToFurniture();
			Holdable holdable = self.Reserved.GetComponent<StoveScript>().TakeReady();
			if (holdable == null)
			{
				return 0;
			}
			holdable.DestroyMe();
			self.GetItem("FoodPlate", true);
			self.UsingPoint = null;
			return 2;
		}
	}

	private static int FindTray(Actor self)
	{
		if (self.QueuedFor("Tray"))
		{
			InteractionPoint interactionPoint = self.InQueue["Tray"];
			if (interactionPoint == null)
			{
				self.InQueue.Remove("Tray");
			}
			else
			{
				if (!self.IsUp("Tray"))
				{
					return 1;
				}
				if (!interactionPoint.Usable())
				{
					interactionPoint.RemoveFromQueue(self);
					self.InQueue.Remove("Tray");
				}
				else
				{
					if (self.PathToFurniture(interactionPoint, true))
					{
						return 2;
					}
					interactionPoint.RemoveFromQueue(self);
					self.InQueue.Remove("Tray");
				}
			}
		}
		InteractionPoint interactionPoint2 = null;
		int num = -1;
		bool flag = true;
		foreach (Room r in self.GetAssignedRooms())
		{
			CookAI.CheckRoomForTray(self, r, ref interactionPoint2, ref num, ref flag);
			if (interactionPoint2 != null && !flag)
			{
				break;
			}
		}
		if (interactionPoint2 == null)
		{
			self.UpdateCurrentRoom(false);
			List<KeyValuePair<Room, int>> connectedRooms = GameSettings.Instance.sRoomManager.GetConnectedRooms(self.currentRoom);
			for (int i = 0; i < connectedRooms.Count; i++)
			{
				KeyValuePair<Room, int> keyValuePair = connectedRooms[i];
				if (keyValuePair.Value > 3)
				{
					return 0;
				}
				CookAI.CheckRoomForTray(self, keyValuePair.Key, ref interactionPoint2, ref num, ref flag);
				if (interactionPoint2 != null && !flag)
				{
					break;
				}
			}
		}
		if (!(interactionPoint2 != null))
		{
			return 0;
		}
		if (flag)
		{
			interactionPoint2.AddToQueue(self);
			self.InQueue["Tray"] = interactionPoint2;
			return 1;
		}
		return (!self.PathToFurniture(interactionPoint2, true)) ? 0 : 2;
	}

	private static void CheckRoomForTray(Actor self, Room r, ref InteractionPoint result, ref int plates, ref bool queued)
	{
		if (r.NavmeshRebuildStarted || !r.Accessible)
		{
			return;
		}
		HashList<Furniture> furniture = r.GetFurniture("Tray");
		for (int i = 0; i < furniture.Count; i++)
		{
			Furniture furniture2 = furniture[i];
			InteractionPoint interactionPoint = furniture2.GetInteractionPoint(self, "Serve");
			if (interactionPoint != null)
			{
				if (result == null || queued || furniture2.HasHoldables < plates)
				{
					queued = false;
					result = interactionPoint;
					plates = furniture2.HasHoldables;
					if (plates == 0)
					{
						break;
					}
				}
			}
			else if (result == null)
			{
				InteractionPoint queueableInteractionPoint = furniture2.GetQueueableInteractionPoint(self, "Serve");
				if (queueableInteractionPoint != null)
				{
					result = queueableInteractionPoint;
					plates = furniture2.HasHoldables;
				}
			}
		}
	}

	private static int GotoTray(Actor self)
	{
		int num = (!self.WalkPath()) ? 1 : 2;
		if (num == 2)
		{
			if (!(self.UsingPoint != null) || !self.UsingPoint.Parent.CanPlaceHoldable())
			{
				self.UsingPoint = null;
				self.anim.SetInteger("AnimControl", 0);
				return 0;
			}
			self.TurnToFurniture();
			self.UsingPoint.Parent.PlaceHoldable(self.Holding[0]);
			self.LeaveItem(self.Holding[0], false);
			GameSettings.Instance.MyCompany.MakeTransaction(-10f, Company.TransactionCategory.Bills, "Food");
		}
		return num;
	}

	private static int CookLoiter(Actor self)
	{
		if (self.CurrentPath != null)
		{
			if (self.WalkPath())
			{
				self.TurnToFurniture();
			}
			return 1;
		}
		if (self.UsingPoint == null || !self.UsingPoint.Parent.Type.Equals("Stove"))
		{
			if (self.Reserved != null)
			{
				InteractionPoint interactionPoint = self.Reserved.GetInteractionPoint(self, "Use");
				if (interactionPoint != null && self.PathToFurniture(interactionPoint, false))
				{
					self.FreeLoiterTable();
					return 1;
				}
			}
			return 0;
		}
		self.anim.SetInteger("AnimControl", 0);
		return self.WaitForTimer((float)UnityEngine.Random.Range(1, 10));
	}

	private static int Loiter(Actor self)
	{
		return self.HandleLoiter(false);
	}

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache5;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache6;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache7;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache8;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache9;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheA;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheB;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheC;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheD;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheE;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheF;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache10;
}
