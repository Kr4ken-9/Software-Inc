﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class RoomGroup
{
	public RoomGroup()
	{
	}

	public RoomGroup(string name)
	{
		this.Name = name;
	}

	public int Count
	{
		get
		{
			return this._rooms.Count;
		}
	}

	public List<Room> GetRooms()
	{
		if (this._actualRooms == null)
		{
			this._actualRooms = (from id in this._rooms
			select GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == id) into x
			where x != null
			select x).ToList<Room>();
		}
		return this._actualRooms;
	}

	public void AddRoom(Room room)
	{
		if (!this._rooms.Contains(room.DID))
		{
			this._rooms.Add(room.DID);
			if (this._actualRooms == null)
			{
				this._actualRooms = new List<Room>();
			}
			this._actualRooms.Add(room);
			room.RoomGroup = this.Name;
			if (room.Outdoors && this.Outdoor != null)
			{
				this.Outdoor.Apply(room, null);
			}
			if (!room.Outdoors && this.Indoor != null)
			{
				this.Indoor.Apply(room, null);
			}
		}
	}

	public void RemoveRoom(Room room)
	{
		if (this._rooms.Contains(room.DID))
		{
			this._actualRooms = null;
			this._rooms.Remove(room.DID);
		}
	}

	public string Name;

	public RoomStyle Indoor;

	public RoomStyle Outdoor;

	private SHashSet<uint> _rooms = new SHashSet<uint>();

	[NonSerialized]
	private List<Room> _actualRooms;
}
