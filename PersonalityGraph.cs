﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class PersonalityGraph
{
	public PersonalityGraph()
	{
	}

	public PersonalityGraph(XMLParser.XMLNode node)
	{
		this.Replace = node.TryGetAttribute("Replace", null).ConvertToBoolDef(false);
		foreach (XMLParser.XMLNode xmlnode in node.GetNode("Personalities", true).GetNodes("Personality", false))
		{
			string value = xmlnode.GetNode("Name", true).Value;
			if (!this.PersonalityTraits.Contains(value))
			{
				this.PersonalityTraits.Add(value);
				this.SetIncompatibility(value, value);
			}
			foreach (XMLParser.XMLNode xmlnode2 in xmlnode.GetNode("Relationships", true).GetNodes("Relation", false))
			{
				this.SetCompatibility(value, xmlnode2.Attributes["Name"], (float)Convert.ToDouble(xmlnode2.Value));
			}
			float[] value2 = new float[]
			{
				Mathf.Clamp((float)Convert.ToDouble(xmlnode.GetNode("WorkLearn", true).Value), -1f, 1f),
				Mathf.Clamp((float)Convert.ToDouble(xmlnode.GetNode("Social", true).Value), -1f, 1f),
				Mathf.Clamp((float)Convert.ToDouble(xmlnode.GetNode("LazyStress", true).Value), -1f, 1f)
			};
			this.PersonalityEffects[value] = value2;
		}
		foreach (XMLParser.XMLNode xmlnode3 in node.GetNode("Incompatibilities", true).GetNodes("Incompatible", false))
		{
			XMLParser.XMLNode[] nodes4 = xmlnode3.GetNodes("Personality", true);
			if (nodes4.Length < 2)
			{
				throw new Exception("Two personalitites must be given for each incompatibility");
			}
			this.SetIncompatibility(nodes4[0].Value, nodes4[1].Value);
		}
	}

	public float this[string p1, string p2]
	{
		get
		{
			if (p1.Equals(p2))
			{
				return 2f;
			}
			float result = 1f;
			if (this.Compatibility.TryGetValue(new KeyValuePair<string, string>(p1, p2), out result))
			{
				return result;
			}
			return 1f;
		}
	}

	public List<KeyValuePair<string, float>> GetCompatibilities(string personality)
	{
		List<KeyValuePair<string, float>> list = new List<KeyValuePair<string, float>>();
		for (int i = 0; i < this.PersonalityTraits.Count; i++)
		{
			list.Add(new KeyValuePair<string, float>(this.PersonalityTraits[i], this[personality, this.PersonalityTraits[i]]));
		}
		return list;
	}

	public string SelectRandom(string[] previous)
	{
		HashSet<string> hashSet = previous.SelectMany((string x) => this.GetIncompatibilities(x)).ToHashSet<string>();
		foreach (string text in from x in this.PersonalityTraits
		orderby UnityEngine.Random.value
		select x)
		{
			if (!previous.Contains(text) && !hashSet.Contains(text))
			{
				return text;
			}
		}
		return null;
	}

	public HashSet<string> GetIncompatibilities(string personality)
	{
		List<string> list;
		if (personality != null && this.Incompatibility.TryGetValue(personality, out list))
		{
			return list.ToHashSet<string>();
		}
		return new HashSet<string>();
	}

	public float[] GetEffects(string personality)
	{
		float[] result;
		if (personality != null && this.PersonalityEffects.TryGetValue(personality, out result))
		{
			return result;
		}
		return new float[PersonalityGraph.Effects];
	}

	public void MergeWith(PersonalityGraph graph)
	{
		if (graph == null)
		{
			return;
		}
		foreach (KeyValuePair<KeyValuePair<string, string>, float> keyValuePair in graph.Compatibility)
		{
			this.SetCompatibility(keyValuePair.Key.Key, keyValuePair.Key.Value, keyValuePair.Value);
		}
		foreach (KeyValuePair<string, float[]> keyValuePair2 in graph.PersonalityEffects)
		{
			if (!this.PersonalityTraits.Contains(keyValuePair2.Key))
			{
				this.PersonalityTraits.Add(keyValuePair2.Key);
				this.SetIncompatibility(keyValuePair2.Key, keyValuePair2.Key);
			}
			this.PersonalityEffects[keyValuePair2.Key] = keyValuePair2.Value;
		}
		foreach (KeyValuePair<string, List<string>> keyValuePair3 in graph.Incompatibility)
		{
			if (this.Incompatibility.ContainsKey(keyValuePair3.Key))
			{
				this.Incompatibility[keyValuePair3.Key].AddRange(keyValuePair3.Value);
			}
			else
			{
				this.Incompatibility[keyValuePair3.Key] = keyValuePair3.Value.ToList<string>();
			}
		}
	}

	public PersonalityGraph Clone()
	{
		PersonalityGraph personalityGraph = new PersonalityGraph();
		personalityGraph.PersonalityTraits = this.PersonalityTraits.ToList<string>();
		foreach (KeyValuePair<KeyValuePair<string, string>, float> keyValuePair in this.Compatibility)
		{
			personalityGraph.SetCompatibility(keyValuePair.Key.Key, keyValuePair.Key.Value, keyValuePair.Value);
		}
		personalityGraph.Incompatibility = this.Incompatibility.ToDictionary((KeyValuePair<string, List<string>> x) => x.Key, (KeyValuePair<string, List<string>> x) => x.Value);
		personalityGraph.PersonalityEffects = this.PersonalityEffects.ToDictionary((KeyValuePair<string, float[]> x) => x.Key, (KeyValuePair<string, float[]> x) => x.Value);
		return personalityGraph;
	}

	private void SetIncompatibility(string p1, string p2)
	{
		if (!this.Incompatibility.ContainsKey(p1))
		{
			this.Incompatibility[p1] = new List<string>();
		}
		if (!this.Incompatibility.ContainsKey(p2))
		{
			this.Incompatibility[p2] = new List<string>();
		}
		if (!this.Incompatibility[p1].Contains(p2))
		{
			this.Incompatibility[p1].Add(p2);
			if (!p1.Equals(p2))
			{
				this.Incompatibility[p2].Add(p1);
			}
		}
	}

	private void SetCompatibility(string p1, string p2, float score)
	{
		if (!this.PersonalityTraits.Contains(p2))
		{
			this.PersonalityTraits.Add(p2);
			this.SetIncompatibility(p2, p2);
		}
		this.Compatibility[new KeyValuePair<string, string>(p1, p2)] = score;
		this.Compatibility[new KeyValuePair<string, string>(p2, p1)] = score;
	}

	private Dictionary<KeyValuePair<string, string>, float> Compatibility = new Dictionary<KeyValuePair<string, string>, float>();

	private Dictionary<string, List<string>> Incompatibility = new Dictionary<string, List<string>>();

	private Dictionary<string, float[]> PersonalityEffects = new Dictionary<string, float[]>();

	public List<string> PersonalityTraits = new List<string>();

	public static int Effects = 3;

	public bool Replace;
}
