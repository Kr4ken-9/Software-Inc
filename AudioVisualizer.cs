﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class AudioVisualizer : MonoBehaviour
{
	private void Awake()
	{
		AudioVisualizer.Instance = this;
		this.AudioProbeTex = new Texture2D(256 * this.Resolution, 256 * this.Resolution, TextureFormat.ARGB32, false);
		this.AudioProbeTex.wrapMode = TextureWrapMode.Clamp;
		this.AudioProbeMat = new Material(this.ProbeMaterial);
		this.AudioProbeMat.mainTexture = this.AudioProbeTex;
	}

	private void OnDestroy()
	{
		AudioVisualizer.Instance = null;
		UnityEngine.Object.Destroy(this.AudioProbeMat);
		UnityEngine.Object.Destroy(this.AudioProbeTex);
	}

	public void ForceRedraw()
	{
		if (this.LastRoom != null)
		{
			this.ForceDraw = true;
			this.IterateX = 0;
			this.IterateY = 0;
		}
	}

	private void ClearBacking()
	{
		for (int i = this.XMIN; i < this.XMAX; i++)
		{
			for (int j = this.YMIN; j < this.YMAX; j++)
			{
				this.SetPixel(i, j, new Color32(0, 0, 0, 0));
			}
		}
	}

	private void SetPixel(int x, int y, Color32 c)
	{
		x -= this.XMIN;
		y -= this.YMIN;
		int num = x + y * (this.XMAX - this.XMIN);
		if (num > -1 && num < this.AudioProbeBacking.Length)
		{
			this.AudioProbeBacking[num] = c;
		}
	}

	private void UpdateBacking()
	{
		this.AudioProbeTex.SetPixels32(this.XMIN, this.YMIN, this.XMAX - this.XMIN, this.YMAX - this.YMIN, this.AudioProbeBacking);
		this.AudioProbeTex.Apply();
	}

	private float TestCalc(Room r, Vector2 p)
	{
		float num = float.MaxValue;
		for (int i = 0; i < r.Edges.Count; i++)
		{
			WallEdge wallEdge = r.Edges[i];
			float magnitude = (p - wallEdge.Pos).magnitude;
			if (magnitude < num)
			{
				num = magnitude;
			}
			WallEdge wallEdge2 = r.Edges[(i + 1) % r.Edges.Count];
			Vector2? vector = Utilities.ProjectToLine(p, wallEdge.Pos, wallEdge2.Pos);
			if (vector != null)
			{
				magnitude = (p - vector.Value).magnitude;
				if (magnitude < num)
				{
					num = magnitude;
				}
			}
		}
		return Mathf.Clamp01(num / 4f);
	}

	private void CalculateAudioProbes(Room room, bool refresh)
	{
		if (refresh)
		{
			this.IterateX = 0;
			this.IterateY = 0;
		}
		if (room != null && room.FloorMesh != null)
		{
			if ((!refresh || GameSettings.GameSpeed <= 0f || room.Occupants.Count <= 0) && !this.ForceDraw)
			{
				return;
			}
			int num = Mathf.FloorToInt(room.RoomBounds.xMin);
			int num2 = Mathf.CeilToInt(room.RoomBounds.xMax);
			int num3 = Mathf.FloorToInt(room.RoomBounds.yMin);
			int num4 = Mathf.CeilToInt(room.RoomBounds.yMax);
			if (refresh)
			{
				this.XMIN = num * this.Resolution - 1;
				this.YMIN = num3 * this.Resolution - 1;
				this.XMAX = num2 * this.Resolution + 1;
				this.YMAX = num4 * this.Resolution + 1;
				this.AudioProbeBacking = new Color32[(this.XMAX - this.XMIN) * (this.YMAX - this.YMIN)];
				this.ClearBacking();
				this.AudioProbeMesh = room.FloorMesh.GetComponent<MeshFilter>().sharedMesh;
			}
			float num5 = 0f;
			float realtimeSinceStartup = Time.realtimeSinceStartup;
			Vector2[] expanded = room.GetExpanded(-0.1f);
			int num6 = Mathf.CeilToInt((float)(num2 - num) / (float)this.MaxChunk);
			int num7 = Mathf.CeilToInt((float)(num4 - num3) / (float)this.MaxChunk);
			int iterateX = this.IterateX;
			int iterateY = this.IterateY;
			List<Furniture> list = new List<Furniture>();
			do
			{
				int num8 = this.IterateX * this.MaxChunk;
				int num9 = this.IterateY * this.MaxChunk;
				int num10 = Mathf.Min(num2, num + num8 + this.MaxChunk);
				int num11 = Mathf.Min(num4, num3 + num9 + this.MaxChunk);
				list.Clear();
				HashList<Furniture> furniture = room.GetFurniture("Cubicle");
				for (int i = 0; i < furniture.Count; i++)
				{
					Furniture furniture2 = furniture[i];
					if (furniture2.OriginalPosition.x > (float)(num + num8 - 2) && furniture2.OriginalPosition.x < (float)(num10 + 2) && furniture2.OriginalPosition.z > (float)(num3 + num9 - 2) && furniture2.OriginalPosition.z < (float)(num11 + 2))
					{
						list.Add(furniture2);
					}
				}
				int num12 = (num + num8) * this.Resolution;
				num10 *= this.Resolution;
				int num13 = (num3 + num9) * this.Resolution;
				num11 *= this.Resolution;
				for (int j = num12; j < num10; j++)
				{
					for (int k = num13; k < num11; k++)
					{
						Vector2 p = new Vector2(((float)j + 0.5f) / (float)this.Resolution, ((float)k + 0.5f) / (float)this.Resolution);
						if (Utilities.IsInside(p, expanded))
						{
							this.SetPixel(j, k, this.ColorGrad.Evaluate(Mathf.Clamp01(Furniture.RecalculateNoise(p, false, room, null, list, true, HUD.Instance.BuildMode))).Alpha(0.5f));
						}
						else
						{
							this.SetPixel(j, k, new Color32(0, 0, 0, 0));
						}
					}
				}
				this.IterateX++;
				if (this.IterateX >= num6)
				{
					this.IterateX = 0;
					if (this.IterateY >= num7)
					{
						this.IterateY = 0;
						this.ForceDraw = false;
					}
					else
					{
						this.IterateY++;
					}
				}
				num5 += Time.realtimeSinceStartup - realtimeSinceStartup;
				realtimeSinceStartup = Time.realtimeSinceStartup;
			}
			while (num5 < this.MaxDrawTime && (iterateX != this.IterateX || iterateY != this.IterateY));
			this.UpdateBacking();
		}
	}

	private void DrawProbes()
	{
		if (this.LastRoom != null)
		{
			Graphics.DrawMesh(this.AudioProbeMesh, Vector3.up * ((float)GameSettings.Instance.ActiveFloor * 2f + 0.1f), Quaternion.identity, this.AudioProbeMat, 0);
		}
	}

	private void Update()
	{
		this.SphereCount = 0;
		foreach (Room room in GameSettings.Instance.sRoomManager.GetRooms())
		{
			if (!room.Dummy && room.Floor == GameSettings.Instance.ActiveFloor)
			{
				List<Furniture> furnitures = room.GetFurnitures();
				for (int i = 0; i < furnitures.Count; i++)
				{
					Furniture furniture = furnitures[i];
					if (furniture.Noisiness > 0f && (HUD.Instance.BuildMode || furniture.IsOn))
					{
						this.DrawSphere(furniture.transform, furniture.Noisiness * this.TempScale, Vector3.up * ((furniture.Height1 + furniture.Height2) / 2f));
					}
				}
			}
		}
		foreach (Actor actor in GameSettings.Instance.sActorManager.Actors)
		{
			if (actor.enabled && actor.Floor == GameSettings.Instance.ActiveFloor)
			{
				this.DrawSphere(actor.NeckBone, actor.Noisiness * this.TempScale, Vector3.zero);
			}
		}
		Vector2 mouseProj = HUD.Instance.GetMouseProj(1f, true);
		Room roomFromPoint = GameSettings.Instance.sRoomManager.GetRoomFromPoint(GameSettings.Instance.ActiveFloor, mouseProj, true);
		bool flag = true;
		if (roomFromPoint != null && !roomFromPoint.Outdoors && roomFromPoint != GameSettings.Instance.sRoomManager.Outside)
		{
			if (this.LastRoom != roomFromPoint)
			{
				this.ForceDraw = true;
				this.LastRoom = roomFromPoint;
				this.CalculateAudioProbes(roomFromPoint, true);
				flag = false;
			}
			if (GUICheck.OverGUI)
			{
				this.AudioLabel.gameObject.SetActive(false);
			}
			else
			{
				this.AudioLabel.gameObject.SetActive(true);
				float num = Mathf.Clamp01(Furniture.RecalculateNoise(mouseProj, false, roomFromPoint, null, null, true, HUD.Instance.BuildMode));
				this.AudioLabel.text = num.ToDB();
				this.AudioLabel.color = this.ColorGrad.Evaluate(num);
				this.AudioLabel.rectTransform.anchoredPosition = new Vector2(Input.mousePosition.x / Options.UISize, Input.mousePosition.y / Options.UISize);
			}
		}
		else
		{
			if (this.LastRoom != null)
			{
				this.LastRoom = null;
				this.CalculateAudioProbes(null, true);
				flag = false;
			}
			this.AudioLabel.gameObject.SetActive(false);
		}
		if (flag)
		{
			this.CalculateAudioProbes(this.LastRoom, false);
		}
		this.DrawProbes();
		if (this.SphereCount > 0)
		{
			if (SystemInfo.supportsInstancing)
			{
				Graphics.DrawMeshInstanced(this.SphereMesh, 0, this.SphereMat, this.Spheres, this.SphereCount, null, ShadowCastingMode.Off, false, 0, CameraScript.Instance.mainCam);
			}
			else
			{
				for (int j = 0; j < this.SphereCount; j++)
				{
					Graphics.DrawMesh(this.SphereMesh, this.Spheres[j], this.SphereMat, 0, CameraScript.Instance.mainCam);
				}
			}
		}
	}

	private void OnDisable()
	{
		if (!GameSettings.IsQuitting && this.AudioLabel != null && this.AudioLabel.gameObject != null)
		{
			this.AudioLabel.gameObject.SetActive(false);
		}
	}

	private void DrawSphere(Transform t, float size, Vector3 offset)
	{
		if (this.SphereCount < this.Spheres.Length)
		{
			this.Spheres[this.SphereCount] = Matrix4x4.TRS(t.position + offset, Quaternion.identity, Vector3.one * size);
			this.SphereCount++;
		}
	}

	[NonSerialized]
	public Room LastRoom;

	public Mesh SphereMesh;

	public Mesh ProbeMesh;

	public Material SphereMat;

	public Material ProbeMaterial;

	public float TempScale = 4f;

	public Text AudioLabel;

	public Gradient ColorGrad;

	public Texture2D AudioProbeTex;

	public Color32[] AudioProbeBacking;

	public Mesh AudioProbeMesh;

	public Material AudioProbeMat;

	public static AudioVisualizer Instance;

	public int Resolution = 1;

	public int IterateX;

	public int IterateY;

	public int MaxChunk = 4;

	private int XMIN;

	private int YMIN;

	private int XMAX;

	private int YMAX;

	private bool ForceDraw;

	public float MaxDrawTime = 0.1f;

	private Matrix4x4[] Spheres = new Matrix4x4[128];

	private int SphereCount;
}
