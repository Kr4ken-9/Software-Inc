﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TabbedControl
{
	public TabbedControl(Rect r, GUISkin skin)
	{
		this.rect = r;
		this.GUIskin = skin;
	}

	public void Draw()
	{
		this.SelectedTab = Mathf.Min(this.Tabs.Count - 1, this.SelectedTab);
		if (this.Tabs.Count == 0)
		{
			return;
		}
		this.SelectedTab = GUI.SelectionGrid(new Rect(this.rect.x, this.rect.y, this.rect.width, 24f), this.SelectedTab, (from x in this.Tabs
		select x.Key).ToArray<string>(), this.Tabs.Count, this.GUIskin.button);
		GUI.BeginGroup(new Rect(this.rect.x, this.rect.y + 24f, this.rect.width, this.rect.height - 24f), this.GUIskin.box);
		this.Tabs[this.SelectedTab].Value(this.rect.width, this.rect.height - 24f);
		GUI.EndGroup();
	}

	public Rect rect;

	public GUISkin GUIskin;

	public List<KeyValuePair<string, Action<float, float>>> Tabs = new List<KeyValuePair<string, Action<float, float>>>();

	public int SelectedTab;
}
