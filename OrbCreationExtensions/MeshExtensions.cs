﻿using System;
using UnityEngine;

namespace OrbCreationExtensions
{
	public static class MeshExtensions
	{
		public static void RecalculateTangents(this Mesh mesh)
		{
			int vertexCount = mesh.vertexCount;
			Vector3[] vertices = mesh.vertices;
			Vector3[] normals = mesh.normals;
			Vector2[] uv = mesh.uv;
			int[] triangles = mesh.triangles;
			int num = triangles.Length / 3;
			Vector4[] array = new Vector4[vertexCount];
			Vector3[] array2 = new Vector3[vertexCount];
			Vector3[] array3 = new Vector3[vertexCount];
			int num2 = 0;
			if (uv.Length <= 0)
			{
				return;
			}
			for (int i = 0; i < num; i++)
			{
				int num3 = triangles[num2];
				int num4 = triangles[num2 + 1];
				int num5 = triangles[num2 + 2];
				Vector3 vector = vertices[num3];
				Vector3 vector2 = vertices[num4];
				Vector3 vector3 = vertices[num5];
				Vector2 vector4 = uv[num3];
				Vector2 vector5 = uv[num4];
				Vector2 vector6 = uv[num5];
				float num6 = vector2.x - vector.x;
				float num7 = vector3.x - vector.x;
				float num8 = vector2.y - vector.y;
				float num9 = vector3.y - vector.y;
				float num10 = vector2.z - vector.z;
				float num11 = vector3.z - vector.z;
				float num12 = vector5.x - vector4.x;
				float num13 = vector6.x - vector4.x;
				float num14 = vector5.y - vector4.y;
				float num15 = vector6.y - vector4.y;
				float num16 = num12 * num15 - num13 * num14;
				float num17 = (num16 != 0f) ? (1f / num16) : 0f;
				Vector3 b = new Vector3((num15 * num6 - num14 * num7) * num17, (num15 * num8 - num14 * num9) * num17, (num15 * num10 - num14 * num11) * num17);
				Vector3 b2 = new Vector3((num12 * num7 - num13 * num6) * num17, (num12 * num9 - num13 * num8) * num17, (num12 * num11 - num13 * num10) * num17);
				array2[num3] += b;
				array2[num4] += b;
				array2[num5] += b;
				array3[num3] += b2;
				array3[num4] += b2;
				array3[num5] += b2;
				num2 += 3;
			}
			for (int j = 0; j < vertexCount; j++)
			{
				Vector3 lhs = normals[j];
				Vector3 rhs = array2[j];
				Vector3.OrthoNormalize(ref lhs, ref rhs);
				array[j].x = rhs.x;
				array[j].y = rhs.y;
				array[j].z = rhs.z;
				array[j].w = ((Vector3.Dot(Vector3.Cross(lhs, rhs), array3[j]) >= 0f) ? 1f : -1f);
			}
			mesh.tangents = array;
		}
	}
}
