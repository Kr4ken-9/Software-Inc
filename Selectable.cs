﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Selectable : Writeable
{
	public bool IsSelected
	{
		get
		{
			return this.IsHighlight;
		}
	}

	public virtual string[] GetActions()
	{
		return new string[0];
	}

	public virtual void TeamChange()
	{
	}

	public void RefreshHighlight()
	{
		this.Highlight(this.IsHighlight, this.IsSecondary);
	}

	public void Highlight(bool highlight, bool secondary = false)
	{
		if (GameSettings.IsQuitting || SelectorController.Instance == null || SelectorController.Instance.Selected == null || SelectorController.Instance.SecondaryHighlights == null || this == null || base.transform == null || base.gameObject == null)
		{
			return;
		}
		if (highlight)
		{
			if (secondary)
			{
				this.IsSecondary = true;
				SelectorController.Instance.SecondaryHighlights.Add(this);
			}
			else
			{
				this.IsHighlight = true;
				foreach (Selectable selectable in from x in this.GetRelated()
				where x != null && x.gameObject != null
				select x)
				{
					if (SelectorController.Instance.Selected.Contains(selectable))
					{
						SelectorController.Instance.SecondaryHighlights.Add(selectable);
						selectable.IsSecondary = true;
					}
					else
					{
						selectable.Highlight(true, true);
					}
				}
			}
		}
		else if (secondary)
		{
			this.IsSecondary = false;
		}
		else
		{
			this.IsHighlight = false;
		}
		bool flag = this.Highlightables == null;
		if (!flag)
		{
			for (int i = 0; i < this.Highlightables.Length; i++)
			{
				if (this.Highlightables[i] == null)
				{
					flag = true;
					break;
				}
			}
		}
		if (flag)
		{
			List<Renderer> list = new List<Renderer>();
			this.UpdateHighlightables(base.transform, list);
			this.Highlightables = list.ToArray();
		}
		for (int j = 0; j < this.Highlightables.Length; j++)
		{
			this.UpdateHighlight(this.Highlightables[j]);
		}
	}

	private void UpdateHighlight(Renderer rend)
	{
		if (this.SingleMat())
		{
			if (this.IsHighlight)
			{
				rend.sharedMaterials = new Material[]
				{
					SelectorController.Instance.PrimaryHighlightMat
				};
			}
			else if (this.IsSecondary)
			{
				rend.sharedMaterials = new Material[]
				{
					SelectorController.Instance.SecondaryHighlightMat
				};
			}
			else
			{
				rend.sharedMaterials = new Material[0];
			}
		}
		else
		{
			Material material = rend.sharedMaterials[0];
			if (this.IsHighlight)
			{
				rend.sharedMaterials = new Material[]
				{
					material,
					SelectorController.Instance.PrimaryHighlightMat
				};
			}
			else if (this.IsSecondary)
			{
				rend.sharedMaterials = new Material[]
				{
					material,
					SelectorController.Instance.SecondaryHighlightMat
				};
			}
			else if (rend.materials.Length == 2)
			{
				rend.sharedMaterials = new Material[]
				{
					material
				};
			}
		}
	}

	private void UpdateHighlightables(Transform node, List<Renderer> result)
	{
		if (node == null || (node.gameObject != base.gameObject && node.GetComponent<Selectable>() != null))
		{
			return;
		}
		Renderer component = node.GetComponent<Renderer>();
		if (component != null && component.tag.Equals("Highlight"))
		{
			result.Add(component);
		}
		for (int i = 0; i < node.childCount; i++)
		{
			this.UpdateHighlightables(node.GetChild(i), result);
		}
	}

	public virtual IEnumerable<Selectable> GetRelated()
	{
		yield break;
	}

	public virtual string GetInfo()
	{
		return "N/A";
	}

	public virtual string[] GetExtendedInfo()
	{
		return null;
	}

	public virtual string[] GetExtendedIconInfo()
	{
		return null;
	}

	public virtual string[] GetExtendedTooltipInfo()
	{
		return null;
	}

	public virtual Color[] GetExtendedColorInfo()
	{
		return null;
	}

	public virtual string GetMultiIcon()
	{
		return null;
	}

	public virtual string GetMultiDesc()
	{
		return null;
	}

	public virtual string GetMultiValue(IEnumerable<Selectable> selected)
	{
		return null;
	}

	public virtual string Description()
	{
		return "Selectables";
	}

	public virtual string GetPanelActionName()
	{
		return null;
	}

	public virtual string GetPanelActionTip()
	{
		return null;
	}

	public virtual void InvokePanelAction(List<UndoObject.UndoAction> undos)
	{
	}

	public virtual Selectable PanelActionDivert()
	{
		return this;
	}

	protected Color GetColorStat(float stat)
	{
		if (stat < 1f)
		{
			return Color.Lerp(new Color32(220, 55, 87, byte.MaxValue), new Color(0.3f, 0.3f, 0.3f), stat);
		}
		if (stat > 1f)
		{
			return Color.Lerp(new Color(0.3f, 0.3f, 0.3f), new Color32(129, 189, 99, byte.MaxValue), stat - 1f);
		}
		return new Color(0.3f, 0.3f, 0.3f);
	}

	public virtual int GetFloor()
	{
		return -5;
	}

	public bool IsSelectable()
	{
		return !this.IsSelectionRestricted() && this.IsSelectableInView();
	}

	public virtual bool IsSelectionRestricted()
	{
		return false;
	}

	public virtual bool IsSelectableInView()
	{
		return this.GetFloor() == GameSettings.Instance.ActiveFloor;
	}

	public virtual bool SingleMat()
	{
		return false;
	}

	public static Color32 NormalHighlight = new Color32(100, 149, 77, byte.MaxValue);

	public static Color32 SecondaryHighlight = new Color32(77, 86, 149, byte.MaxValue);

	public bool CanSelect = true;

	[NonSerialized]
	protected Renderer[] Highlightables;

	protected bool IsHighlight;

	protected bool IsSecondary;
}
