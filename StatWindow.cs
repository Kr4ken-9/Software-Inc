﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StatWindow : MonoBehaviour
{
	private void Start()
	{
		this.legend.Colors = HUD.ThemeColors.ToList<Color>();
		this.legend.OnToggle = delegate
		{
			this.ReallyUpdateGraph();
		};
		ToggleGroup component = this.GroupingPanel.GetComponent<ToggleGroup>();
		foreach (KeyValuePair<string, Func<SoftwareProduct, string>> keyValuePair in StatWindow.GroupingFunctions)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.TogglePrefab);
			Toggle component2 = gameObject.GetComponent<Toggle>();
			component2.isOn = false;
			component2.group = component;
			component2.onValueChanged.AddListener(delegate(bool x)
			{
				this.UpdateGrouping();
			});
			component2.GetComponentInChildren<Text>().text = keyValuePair.Key;
			this.GroupToggles.Add(keyValuePair.Key, component2);
			gameObject.transform.SetParent(this.GroupingPanel.transform);
		}
		ToggleGroup component3 = this.ValuePanel.GetComponent<ToggleGroup>();
		foreach (KeyValuePair<string, Func<SoftwareProduct, float>> keyValuePair2 in StatWindow.ValueFunctions)
		{
			GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.TogglePrefab);
			Toggle component4 = gameObject2.GetComponent<Toggle>();
			component4.isOn = false;
			component4.group = component3;
			component4.onValueChanged.AddListener(delegate(bool x)
			{
				this.UpdateValue();
			});
			component4.GetComponentInChildren<Text>().text = keyValuePair2.Key;
			this.ValueToggles.Add(keyValuePair2.Key, component4);
			gameObject2.transform.SetParent(this.ValuePanel.transform);
		}
		this.GroupToggles[this.ActiveGroupingFunc].isOn = true;
		this.ValueToggles[this.ActiveValueFunc].isOn = true;
	}

	private void UpdateGrouping()
	{
		if (this.GroupToggles.Any((KeyValuePair<string, Toggle> x) => x.Value.isOn))
		{
			this.ActiveGroupingFunc = this.GroupToggles.First((KeyValuePair<string, Toggle> x) => x.Value.isOn).Key;
			this.UpdateGraph();
		}
	}

	private void UpdateValue()
	{
		if (this.ValueToggles.Any((KeyValuePair<string, Toggle> x) => x.Value.isOn))
		{
			this.ActiveValueFunc = this.ValueToggles.First((KeyValuePair<string, Toggle> x) => x.Value.isOn).Key;
			this.UpdateGraph();
		}
	}

	private void UpdateGraph()
	{
		if (GameSettings.Instance == null || GameSettings.Instance.simulation == null)
		{
			return;
		}
		Func<SoftwareProduct, float> valueFunc = StatWindow.ValueFunctions[this.ActiveValueFunc];
		Func<SoftwareProduct, string> keySelector = StatWindow.GroupingFunctions[this.ActiveGroupingFunc];
		this.LastResult.Clear();
		int earliest = GameSettings.Instance.simulation.GetAllProducts().Min((SoftwareProduct x) => x.Release.Year * 12 + x.Release.Month);
		int num = GameSettings.Instance.simulation.GetAllProducts().Max((SoftwareProduct x) => x.Release.Year * 12 + x.Release.Month);
		int num2 = num - earliest;
		IOrderedEnumerable<IGrouping<string, SoftwareProduct>> orderedEnumerable = from x in GameSettings.Instance.simulation.GetAllProducts().GroupBy(keySelector)
		orderby x.Key
		select x;
		foreach (IGrouping<string, SoftwareProduct> grouping in orderedEnumerable)
		{
			float[] array = new float[num2];
			Dictionary<int, float> dictionary = (from x in grouping
			group x by x.Release.Year * 12 + x.Release.Month - earliest).ToDictionary((IGrouping<int, SoftwareProduct> x) => x.Key, (IGrouping<int, SoftwareProduct> x) => x.SumSafe(valueFunc));
			for (int i = 0; i < num2; i++)
			{
				float num3 = 0f;
				if (dictionary.TryGetValue(i, out num3))
				{
					array[i] = num3;
				}
			}
			this.LastResult.Add(grouping.Key, array);
		}
		this.legend.Items = (from x in orderedEnumerable
		select x.Key).ToList<string>();
		this.legend.UpdateItems();
		this.ReallyUpdateGraph();
	}

	private void ReallyUpdateGraph()
	{
		this.graph.Colors.Clear();
		List<List<float>> list = new List<List<float>>();
		int num = 0;
		foreach (KeyValuePair<string, float[]> keyValuePair in from x in this.LastResult
		orderby x.Key
		select x)
		{
			if (this.legend.IsOn(num))
			{
				this.graph.Colors.Add(this.legend.Colors[num % this.legend.Colors.Count]);
				list.Add(keyValuePair.Value.ToList<float>());
			}
			num++;
		}
		this.graph.Values = list;
		this.graph.UpdateCachedLines();
	}

	public static Dictionary<string, Func<SoftwareProduct, string>> GroupingFunctions = new Dictionary<string, Func<SoftwareProduct, string>>
	{
		{
			"None",
			(SoftwareProduct x) => "All"
		},
		{
			"Company",
			(SoftwareProduct x) => x.DevCompany.Name
		},
		{
			"Type",
			(SoftwareProduct x) => x._type.LocSW()
		},
		{
			"TypeCategory",
			(SoftwareProduct x) => x._type.LocSW() + " - " + x._category.LocSWC(x._type)
		}
	};

	public static Dictionary<string, Func<SoftwareProduct, float>> ValueFunctions = new Dictionary<string, Func<SoftwareProduct, float>>
	{
		{
			"Income",
			(SoftwareProduct x) => x.Sum
		},
		{
			"Loss",
			(SoftwareProduct x) => x.Loss
		},
		{
			"Profit",
			(SoftwareProduct x) => x.Sum - x.Loss
		},
		{
			"Releases",
			(SoftwareProduct x) => 1f
		},
		{
			"Quality",
			(SoftwareProduct x) => x.RealQuality
		},
		{
			"Units",
			(SoftwareProduct x) => x.UnitSum
		},
		{
			"ActiveUsers",
			(SoftwareProduct x) => (float)x.Userbase
		}
	};

	public GameObject TogglePrefab;

	public GUIWindow window;

	public GameObject GroupingPanel;

	public GameObject ValuePanel;

	public GUILineChart graph;

	public GUILegend legend;

	[NonSerialized]
	private Dictionary<string, Toggle> GroupToggles = new Dictionary<string, Toggle>();

	[NonSerialized]
	private Dictionary<string, Toggle> ValueToggles = new Dictionary<string, Toggle>();

	[NonSerialized]
	private string ActiveGroupingFunc = "None";

	private string ActiveValueFunc = "Releases";

	[NonSerialized]
	public Dictionary<string, float[]> LastResult = new Dictionary<string, float[]>();
}
