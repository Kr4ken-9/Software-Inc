﻿using System;
using System.Linq;
using UnityEngine;

public class BoneChild : MonoBehaviour
{
	private void Start()
	{
		this.InitBones();
		if (this.BoneParent != null)
		{
			this.SetParent(this.BoneParent);
		}
	}

	private void InitBones()
	{
		if (this.Bones == null)
		{
			this.Bones = (from x in base.GetComponentsInChildren<Transform>()
			where x != base.transform && x.GetComponent<SkinnedMeshRenderer>() == null
			select x).ToArray<Transform>();
		}
	}

	private void OnDestroy()
	{
		foreach (Transform transform in from x in this.Bones
		where x != null
		select x)
		{
			UnityEngine.Object.Destroy(transform.gameObject);
		}
	}

	public void SetParent(GameObject parent)
	{
		this.InitBones();
		Transform[] componentsInChildren = parent.GetComponentsInChildren<Transform>();
		this.BoneParent = parent;
		base.transform.parent = parent.transform;
		base.transform.localPosition = Vector3.zero;
		base.transform.localRotation = Quaternion.identity;
		base.transform.localScale = Vector3.one;
		Transform[] bones = this.Bones;
		for (int i = 0; i < bones.Length; i++)
		{
			Transform bone = bones[i];
			Transform transform = componentsInChildren.FirstOrDefault((Transform x) => x.name.Equals(bone.name));
			if (transform != null)
			{
				bone.parent = transform;
			}
			bone.localPosition = Vector3.zero;
			bone.localRotation = Quaternion.identity;
			bone.name = "Rigged" + bone.name;
		}
	}

	private Transform[] Bones;

	public GameObject BoneParent;
}
