﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Versioning : MonoBehaviour
{
	public static string VersionString
	{
		get
		{
			return string.Concat(new string[]
			{
				Versioning.Types[Versioning.Type],
				(!Versioning.Steam) ? string.Empty : " Steam",
				(!Versioning.Demo) ? string.Empty : " Demo",
				" ",
				Versioning.Major.ToString(),
				".",
				Versioning.Minor.ToString(),
				".",
				Versioning.Revision.ToString(),
				", ",
				Versioning.GetPlatform()
			});
		}
	}

	public static string SimpleVersionString
	{
		get
		{
			return string.Concat(new string[]
			{
				Versioning.Types[Versioning.Type],
				" ",
				Versioning.Major.ToString(),
				".",
				Versioning.Minor.ToString(),
				".",
				Versioning.Revision.ToString()
			});
		}
	}

	public static string GetPlatform()
	{
		return Application.platform.ToString();
	}

	private void Start()
	{
		this.UpdateText();
	}

	public static Versioning.Version DisectVersionString(string v)
	{
		string[] array = v.Split(new char[]
		{
			' '
		});
		string t = string.Empty;
		int m = 0;
		int mi = 0;
		int num = 0;
		bool d = false;
		bool flag = false;
		for (int i = 0; i < array.Length; i++)
		{
			string text = array[i];
			if (i == 0)
			{
				t = text;
			}
			else if (i == 1)
			{
				if (text.Equals("Demo"))
				{
					d = true;
				}
				else
				{
					if (!text.Equals("Steam"))
					{
						Versioning.DissectVersionNum(text, ref m, ref mi, ref num);
						break;
					}
					flag = true;
				}
			}
			else if (i == 2)
			{
				if (!flag)
				{
					Versioning.DissectVersionNum(text, ref m, ref mi, ref num);
					break;
				}
				if (!text.Equals("Demo"))
				{
					Versioning.DissectVersionNum(text, ref m, ref mi, ref num);
					break;
				}
				d = true;
			}
			else if (i == 3)
			{
				Versioning.DissectVersionNum(text, ref m, ref mi, ref num);
				break;
			}
		}
		return new Versioning.Version(t, m, mi, flag, d);
	}

	private static void DissectVersionNum(string t, ref int major, ref int minor, ref int revision)
	{
		t = t.Replace(",", string.Empty);
		if (t.Contains("."))
		{
			string[] array = t.Split(new char[]
			{
				'.'
			});
			major = Convert.ToInt32(array[0]);
			minor = Convert.ToInt32(array[1]);
			if (array.Length > 2)
			{
				revision = Convert.ToInt32(array[2]);
			}
		}
		else
		{
			major = Convert.ToInt32(t);
			minor = 0;
		}
	}

	private void UpdateText()
	{
		this.text.text = Versioning.VersionString;
	}

	public static readonly string[] Types = new string[]
	{
		"Pre-alpha",
		"Alpha",
		"Beta",
		"Release"
	};

	public static int Type = 1;

	public static int Major = 10;

	public static int Minor = 3;

	public static int Revision = 2;

	public static bool Demo = false;

	public static bool Steam = true;

	public Text text;

	public struct Version
	{
		public Version(string t, int m, int mi, bool s, bool d)
		{
			this.Type = t;
			this.Major = m;
			this.Minor = mi;
			this.Steam = s;
			this.Demo = d;
		}

		public int GetTypeIndex()
		{
			return Array.IndexOf<string>(Versioning.Types, this.Type);
		}

		public string SimpleVersion()
		{
			return string.Concat(new string[]
			{
				this.Type,
				" ",
				this.Major.ToString(),
				".",
				this.Minor.ToString(),
				".",
				Versioning.Revision.ToString()
			});
		}

		public string Type;

		public int Major;

		public int Minor;

		public bool Steam;

		public bool Demo;
	}
}
