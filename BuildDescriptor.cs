﻿using System;

public struct BuildDescriptor
{
	public BuildDescriptor(BuildDescriptor.BuildType type, string funCat, string search, Furniture furn, params string[] cat)
	{
		this.Type = type;
		this.Category = cat;
		this.FunctionalCategory = funCat;
		this.SearchString = search.ToLower();
		this.Furniture = furn;
	}

	public readonly BuildDescriptor.BuildType Type;

	public readonly string[] Category;

	public readonly string FunctionalCategory;

	public readonly string SearchString;

	public readonly Furniture Furniture;

	public enum BuildType
	{
		Construction,
		Furniture,
		Roads,
		Environment
	}

	public enum CategoryType
	{
		Room,
		Function
	}
}
