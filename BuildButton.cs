﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IEventSystemHandler
{
	public void SetAttributes(string realName, string name, string desc, Sprite t, float price)
	{
		this.Name = name;
		this.Description = desc;
		this.Thumbnail = t;
		this.Price = price;
		base.name = "BuildButton" + realName;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (this.furn != null)
		{
			HUD.Instance.BuildDetailPanel.SetFurniture(this.furn);
		}
		else
		{
			HUD.Instance.BuildDetailPanel.SetGeneric(this.Name, this.Description, this.Thumbnail, this.Price);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		HUD.Instance.BuildDetailPanel.Disable();
	}

	[NonSerialized]
	public Furniture furn;

	public string Name;

	public string Description;

	public Sprite Thumbnail;

	public float Price;

	public Image ButtonImage;
}
