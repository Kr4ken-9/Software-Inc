﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;

public class FurnitureBuilder : MonoBehaviour
{
	private void Start()
	{
		HintController.Instance.Show(HintController.Hints.HintMultipleBuild);
		this.RotationSnap = BuildController.Instance.FurnitureAngle;
		this.Furn = this.FurnPrefab.GetComponent<Furniture>();
		if (!this.Furn.CanRotate || this.Furn.WallFurn)
		{
			this.Arrow.SetActive(false);
		}
		if (this.IsProto && !this.CopyProto)
		{
			this.Furn.IgnoreBoundary = true;
			foreach (SnapPoint snapPoint in this.Furn.SnapPoints)
			{
				if (snapPoint.UsedBy != null)
				{
					snapPoint.UsedBy.IgnoreBoundary = true;
				}
			}
		}
		if (this.Furn.BuildBoundary != null && this.Furn.BuildBoundary.Length > 0)
		{
			this.BuildBoundary.Add(this.Furn.BuildBoundary.ToArray<Vector2>());
			this.Heights.Add(new Vector2(this.Furn.Height1, this.Furn.Height2));
		}
		if (this.IsProto)
		{
			Vector3 position = this.Furn.transform.position;
			Quaternion rotation = this.Furn.transform.rotation;
			Vector3 localScale = this.Furn.transform.localScale;
			this.Furn.transform.position = Vector3.zero;
			this.Furn.transform.rotation = Quaternion.identity;
			this.Furn.transform.localScale = Vector3.one;
			this.AddSnapFurnBounds(this.Furn);
			this.Furn.transform.position = position;
			this.Furn.transform.rotation = rotation;
			this.Furn.transform.localScale = localScale;
		}
		if (this.IsProto)
		{
			base.transform.rotation = this.FurnPrefab.transform.rotation;
		}
		else
		{
			Vector3 forward = BuildController.Instance.GridMatrix.inverse.MultiplyVector(Vector3.forward);
			base.transform.rotation = Quaternion.LookRotation(forward);
		}
		this.BuildMesh();
		this.LerpingPos = base.transform.position;
		if (this.Parent != null)
		{
			base.transform.position = Quaternion.Inverse(this.Parent.Furn.transform.rotation) * (this.Furn.OriginalPosition - this.Parent.Furn.OriginalPosition);
			base.transform.SetParent(this.Parent.transform, false);
			base.transform.rotation = this.Furn.transform.rotation;
			this.Rend.sharedMaterial = this.Green;
			this.Arrow.SetActive(false);
		}
		this.LastRot = base.transform.rotation.eulerAngles.y;
	}

	private void AddSnapFurnBounds(Furniture furn)
	{
		foreach (SnapPoint snapPoint in furn.SnapPoints)
		{
			if (snapPoint.UsedBy != null)
			{
				if (snapPoint.UsedBy.BuildBoundary != null && snapPoint.UsedBy.BuildBoundary.Length > 0)
				{
					this.BuildBoundary.Add(this.GetRealPoints(Matrix4x4.TRS(snapPoint.transform.localPosition, snapPoint.transform.localRotation, Vector3.one), snapPoint.UsedBy.BuildBoundary));
					this.Heights.Add(new Vector2(snapPoint.UsedBy.Height1, snapPoint.UsedBy.Height2));
				}
				this.AddSnapFurnBounds(snapPoint.UsedBy);
			}
		}
	}

	private Matrix4x4 MakeSnap(Vector3 pos, Quaternion or)
	{
		float num = (Mathf.PerlinNoise(pos.x / 2f, pos.z / 2f) * 2f + Time.realtimeSinceStartup * 2f) % 2f;
		if (num > 1f)
		{
			num = 2f - num;
		}
		num = 1f - num * num;
		return Matrix4x4.TRS(pos + Vector3.up * (num * 0.1f + 0.3f), or, Vector3.one);
	}

	private void DrawSnaps(Room room)
	{
		Quaternion or = Quaternion.Euler(0f, Mathf.Round(CameraScript.Instance.transform.rotation.eulerAngles.y / 90f) * 90f + 90f, -90f);
		if (SystemInfo.supportsInstancing)
		{
			int num = 0;
			int num2 = 1;
			if (this._snaps.Count == 0)
			{
				this._snaps.Add(new Matrix4x4[FurnitureBuilder.GPUInstanceArrows]);
			}
			Matrix4x4[] array = this._snaps[0];
			List<Furniture> furnitures = room.GetFurnitures();
			for (int i = 0; i < furnitures.Count; i++)
			{
				Furniture furniture = furnitures[i];
				for (int j = 0; j < furniture.SnapPoints.Length; j++)
				{
					SnapPoint snapPoint = furniture.SnapPoints[j];
					if (snapPoint.IsValid && snapPoint.UsedBy == null && snapPoint.Name.Equals(this.Furn.SnapsTo))
					{
						array[num] = this.MakeSnap(snapPoint.transform.position, or);
						num++;
						if (num == FurnitureBuilder.GPUInstanceArrows)
						{
							num = 0;
							if (num2 == this._snaps.Count)
							{
								this._snaps.Add(new Matrix4x4[FurnitureBuilder.GPUInstanceArrows]);
							}
							array = this._snaps[num2];
							num2++;
						}
					}
				}
			}
			if (num == 0)
			{
				num2--;
				num = FurnitureBuilder.GPUInstanceArrows;
			}
			for (int k = 0; k < num2; k++)
			{
				int count = (k != num2 - 1) ? FurnitureBuilder.GPUInstanceArrows : num;
				Graphics.DrawMeshInstanced(this.SnapIndicatorMesh, 0, this.SnapIndicatorMat, this._snaps[k], count, null, ShadowCastingMode.On, false);
			}
		}
		else
		{
			List<Furniture> furnitures2 = room.GetFurnitures();
			for (int l = 0; l < furnitures2.Count; l++)
			{
				Furniture furniture2 = furnitures2[l];
				for (int m = 0; m < furniture2.SnapPoints.Length; m++)
				{
					SnapPoint snapPoint2 = furniture2.SnapPoints[m];
					if (snapPoint2.IsValid && snapPoint2.UsedBy == null && snapPoint2.Name.Equals(this.Furn.SnapsTo))
					{
						Graphics.DrawMesh(this.SnapIndicatorMesh, this.MakeSnap(snapPoint2.transform.position, or), this.SnapIndicatorMat, 0);
					}
				}
			}
		}
	}

	private void AddSnapFurns(Furniture furn, List<Furniture> furns)
	{
		foreach (SnapPoint snapPoint in from x in furn.SnapPoints
		where x.UsedBy != null
		select x)
		{
			furns.Add(snapPoint.UsedBy);
			this.AddSnapFurns(snapPoint.UsedBy, furns);
		}
	}

	private void TraverseTransform(Transform t, Furniture localFurn, SnapPoint s)
	{
		if (s.Parent != this.Furn && s.Parent.SnappedTo != null)
		{
			this.TraverseTransform(t, s.Parent, s.Parent.SnappedTo);
		}
		t.position += t.rotation * s.transform.localPosition;
		t.rotation *= Quaternion.Euler(0f, localFurn.RotationOffset, 0f) * s.transform.localRotation;
	}

	public void BuildMesh()
	{
		List<CombineInstance> list = new List<CombineInstance>();
		List<Furniture> list2 = new List<Furniture>
		{
			this.Furn
		};
		if (this.IsProto)
		{
			this.AddSnapFurns(this.Furn, list2);
		}
		foreach (Furniture furniture in list2)
		{
			Vector3 position = furniture.transform.position;
			Quaternion rotation = furniture.transform.rotation;
			Vector3 localScale = furniture.transform.localScale;
			furniture.transform.position = Vector3.zero;
			furniture.transform.rotation = Quaternion.identity;
			if (furniture.SnappedTo != null && furniture != this.Furn)
			{
				this.TraverseTransform(furniture.transform, furniture, furniture.SnappedTo);
			}
			furniture.transform.localScale = Vector3.one;
			MeshFilter[] componentsInChildren = furniture.GetComponentsInChildren<MeshFilter>(true);
			foreach (MeshFilter meshFilter in componentsInChildren)
			{
				if (!meshFilter.tag.Equals("HideUnaffected") && !meshFilter.tag.Equals("IgnoreMesh"))
				{
					list.Add(new CombineInstance
					{
						mesh = meshFilter.sharedMesh,
						transform = meshFilter.transform.localToWorldMatrix
					});
				}
			}
			furniture.transform.position = position;
			furniture.transform.rotation = rotation;
			furniture.transform.localScale = localScale;
		}
		base.GetComponent<MeshFilter>().mesh.CombineMeshes(list.ToArray());
	}

	public bool IsValid(Matrix4x4 mat, Room r, Furniture ignore = null)
	{
		if (r == null)
		{
			return false;
		}
		if (!r.IsPlayerControlled())
		{
			ErrorOverlay.Instance.ShowError("NotPlayerOwnedPlace", false, false, 0f, true);
			return false;
		}
		for (int i = 0; i < this.BuildBoundary.Count; i++)
		{
			if (!FurnitureBuilder.IsValid(this.GetRealPoints(mat, this.BuildBoundary[i]), this.Heights[i].x, this.Heights[i].y, r, this.Furn.WallFurn, new Furniture[]
			{
				ignore
			}))
			{
				ErrorOverlay.Instance.ShowError("FurnitureOccupied", false, false, 0f, true);
				return false;
			}
		}
		return true;
	}

	public static bool IsValid(Vector2[] ps, float height1, float height2, Room r, bool wallFurn, params Furniture[] ignore)
	{
		if (r == null)
		{
			return false;
		}
		if (ps.Length == 0)
		{
			return true;
		}
		if (ps.Length == 1)
		{
			if (!wallFurn && !r.IsInside(ps[0]))
			{
				return false;
			}
			List<Furniture> furnitures = r.GetFurnitures();
			for (int i = 0; i < furnitures.Count; i++)
			{
				Furniture furniture = furnitures[i];
				if (!furniture.IgnoreBoundary && furniture.FinalBoundary != null && furniture.FinalBoundary.Length != 0 && Mathf.Abs(furniture.OriginalPosition.x - ps[0].x) <= 2f && Mathf.Abs(furniture.OriginalPosition.z - ps[0].y) <= 2f && Utilities.Overlap(furniture.Height1, furniture.Height2, height1, height2) && !ignore.Contains(furniture))
				{
					if (Utilities.IsInside(ps[0], furniture.FinalBoundary))
					{
						return false;
					}
				}
			}
		}
		else
		{
			bool flag = ps.Length > 2;
			if (!wallFurn)
			{
				for (int j = 0; j < ps.Length; j++)
				{
					Vector2 p = ps[j];
					if (ps.Length > 2)
					{
						if (!r.IsInside(p))
						{
							return false;
						}
					}
					else if (!flag && r.IsInside(p, 0.01f))
					{
						flag = true;
					}
					Vector2 p2 = ps[(j + 1) % ps.Length];
					for (int k = 0; k < r.Edges.Count; k++)
					{
						Vector2 pos = r.Edges[k].Pos;
						Vector2 pos2 = r.Edges[(k + 1) % r.Edges.Count].Pos;
						if (Utilities.LinesIntersect(p, p2, pos, pos2, true, false))
						{
							return false;
						}
					}
				}
			}
			if (!flag)
			{
				return false;
			}
			List<Furniture> furnitures2 = r.GetFurnitures();
			for (int l = 0; l < furnitures2.Count; l++)
			{
				Furniture furniture2 = furnitures2[l];
				if (!ignore.Contains(furniture2) && !furniture2.IgnoreBoundary && furniture2.FinalBoundary != null && furniture2.FinalBoundary.Length != 0 && Utilities.Overlap(furniture2.Height1, furniture2.Height2, height1, height2))
				{
					int num = 0;
					for (int m = 0; m < ps.Length; m++)
					{
						Vector2 vector = ps[m];
						Vector2 p3 = ps[(m + 1) % ps.Length];
						for (int n = 0; n < furniture2.FinalBoundary.Length; n++)
						{
							Vector2 vector2 = furniture2.FinalBoundary[n];
							Vector2 q = furniture2.FinalBoundary[(n + 1) % furniture2.FinalBoundary.Length];
							if (vector == vector2)
							{
								num++;
							}
							if (Utilities.LinesIntersect(vector, p3, vector2, q, true, false))
							{
								return false;
							}
							if (Utilities.IsInside(vector2, ps))
							{
								return false;
							}
						}
						if (Utilities.IsInside(vector, furniture2.FinalBoundary))
						{
							return false;
						}
					}
					if (num == ps.Length)
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	private void OnDestroy()
	{
		CostDisplay.Instance.Hide();
		if (BuildController.Instance != null)
		{
			BuildController.Instance.ResetTempGrid();
		}
		if (this.IsProto && !this.CopyProto)
		{
			this.MakeAllIgnored(false);
		}
		foreach (FurnitureBuilder furnitureBuilder in this.Children)
		{
			if (furnitureBuilder != null)
			{
				UnityEngine.Object.Destroy(furnitureBuilder.gameObject);
			}
		}
	}

	private void MakeAllIgnored(bool ignore)
	{
		this.Furn.IgnoreBoundary = ignore;
		foreach (SnapPoint snapPoint in this.Furn.SnapPoints)
		{
			if (snapPoint.UsedBy != null)
			{
				snapPoint.UsedBy.IgnoreBoundary = ignore;
			}
		}
	}

	public Vector2[] GetRealPoints(Matrix4x4 matrix, Vector2[] points)
	{
		return points.SelectInPlace((Vector2 x) => matrix.MultiplyPoint(x.ToVector3(0f)).FlattenVector3());
	}

	private void NormalBuildCode(bool usePos)
	{
		if (this.TurnMode && !usePos)
		{
			if (this.rotationCountDown < 0f)
			{
				if (this.Furn.IsSnapping)
				{
					if (this.Furn.CanRotate)
					{
						if (this.LastSnap != null)
						{
							Vector2 mousePos = BuildController.Instance.GetMousePos(new Plane(Vector3.up, Vector3.up * this.LastSnap.transform.position.y));
							Vector3 a4 = new Vector3(mousePos.x, this.LastSnap.transform.position.y, mousePos.y);
							float y = Quaternion.LookRotation(a4 - this.LastSnap.GetRealPos()).eulerAngles.y;
							float y2 = this.LastSnap.transform.rotation.eulerAngles.y;
							this.rotationOffset = Mathf.Round((y - y2) / this.RotationSnap) * this.RotationSnap;
							base.transform.rotation = Quaternion.Euler(base.transform.rotation.eulerAngles.x, this.rotationOffset, base.transform.rotation.eulerAngles.z) * this.LastSnap.transform.rotation;
						}
					}
					else if (this.OriginalSnap != null)
					{
						SnapPoint[] array = this.LastSnap.Parent.Parent.GetFurnitures().SelectMany((Furniture z) => from x in z.SnapPoints
						where x.UsedBy == null && x.Name.Equals(this.OriginalSnap.Name) && (this.OriginalSnap.GetRealPos() - x.GetRealPos()).sqrMagnitude < 0.5f
						select x).ToArray<SnapPoint>();
						if (array.Length > 1)
						{
							Vector2 mousePos2 = BuildController.Instance.GetMousePos(new Plane(Vector3.up, Vector3.up * this.OriginalSnap.transform.position.y));
							Vector3 a2 = new Vector3(mousePos2.x, this.OriginalSnap.transform.position.y, mousePos2.y);
							float a = Quaternion.LookRotation(a2 - this.OriginalSnap.GetRealPos()).eulerAngles.y;
							foreach (SnapPoint snapPoint in from x in array
							orderby Mathf.Abs(Mathf.DeltaAngle(x.transform.rotation.eulerAngles.y, a))
							select x)
							{
								Vector3 position = snapPoint.transform.position;
								Quaternion q = Quaternion.Euler(0f, this.rotationOffset, 0f) * snapPoint.transform.rotation;
								if (this.IsValid(Matrix4x4.TRS(position, q, Vector3.one), snapPoint.Parent.Parent, snapPoint.Parent))
								{
									this.LastRoom = snapPoint.Parent.Parent;
									this.LastSnap = snapPoint;
									base.transform.position = position;
									base.transform.rotation = Quaternion.Euler(base.transform.rotation.eulerAngles.x, q.eulerAngles.y, base.transform.rotation.eulerAngles.z);
									break;
								}
							}
						}
					}
				}
				else if (this.Furn.CanRotate)
				{
					Vector2 p = BuildController.Instance.GetMousePos(new Plane(Vector3.up, Vector3.up * (float)(GameSettings.Instance.ActiveFloor * 2)));
					float y3 = Quaternion.LookRotation(new Vector3(p.x, 0f, p.y) - this.OriginalPosition).eulerAngles.y;
					float y4 = Quaternion.LookRotation(BuildController.Instance.GridMatrix.MultiplyVector(Vector3.forward)).eulerAngles.y;
					float num = Mathf.Round((y3 + y4) / this.RotationSnap) * this.RotationSnap - y4;
					if (base.transform.rotation.eulerAngles.y != num)
					{
						HintController.Instance.Show(HintController.Hints.HintGridSnapToWall);
					}
					base.transform.rotation = Quaternion.Euler(base.transform.rotation.eulerAngles.x, num, base.transform.rotation.eulerAngles.z);
					if (!this.Furn.Type.Equals("Elevator"))
					{
						Quaternion rotation = Quaternion.Euler(0f, base.transform.rotation.eulerAngles.y, 0f);
						Vector3 forward = BuildController.Instance.GridMatrix.MultiplyVector(rotation * new Vector3(0f, 0f, 1f));
						rotation = Quaternion.LookRotation(forward);
						if (this.Furn.OnXEdge ^ this.Furn.OnYEdge)
						{
							rotation = Quaternion.Euler(0f, rotation.eulerAngles.y, 0f);
						}
						else
						{
							rotation = Quaternion.Euler(0f, Mathf.Round(rotation.eulerAngles.y / 90f) * 90f, 0f);
						}
						bool flag = BuildController.Instance.GridMatrix.MultiplyVector(Vector3.left).magnitude > 1f;
						Vector3 vector = rotation * new Vector3((!this.Furn.OnXEdge && !flag) ? 0.5f : 0f, 0f, (!this.Furn.OnYEdge && !flag) ? 0.5f : 0f);
						p = BuildController.Instance.CorrectMousePos(this.finalP, new Vector2(vector.x, vector.z));
						this.LastRoom = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Floor == GameSettings.Instance.ActiveFloor && this.ValidIn(this.Furn, x) == null && x.IsInside(p));
						base.transform.position = new Vector3(p.x, (float)(((!(this.LastRoom == null)) ? this.LastRoom.Floor : GameSettings.Instance.ActiveFloor) * 2), p.y);
					}
				}
			}
			if (this.Furn.CanRotate)
			{
				this.UpdateRotSound();
			}
		}
		else
		{
			if (this.Furn.CanRotate)
			{
				ErrorOverlay.Instance.ShowError((!this.Furn.IsSnapping) ? this.GetHint(true, true, true) : this.GetHint(true, false, false), false, false, 0f, false);
			}
			else
			{
				ErrorOverlay.Instance.ShowError(this.GetHint(false, false, false), false, false, 0f, false);
			}
			string text = null;
			if (this.Furn.IsSnapping)
			{
				Room room = (!usePos) ? null : GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Floor == GameSettings.Instance.ActiveFloor && this.ValidIn(this.Furn, x) == null && x.IsInside(this.transform.position.FlattenVector3()));
				Vector2 p1 = (!usePos) ? Vector2.zero : base.transform.position.FlattenVector3();
				Room room2 = null;
				if (!usePos)
				{
					p1 = BuildController.Instance.GetMousePos(new Plane(Vector3.up, Vector3.up * (float)(GameSettings.Instance.ActiveFloor * 2)));
					Vector2 p2 = BuildController.Instance.GetMousePos(new Plane(Vector3.up, Vector3.up * (float)(GameSettings.Instance.ActiveFloor * 2 + 2)));
					room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Floor == GameSettings.Instance.ActiveFloor && this.ValidIn(this.Furn, x) == null && x.IsInside(p1));
					room2 = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Floor == GameSettings.Instance.ActiveFloor && this.ValidIn(this.Furn, x) == null && x.IsInside(p2));
				}
				Ray ray = BuildController.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
				ray = new Ray(BuildController.Instance.MainCamera.transform.position, ray.direction);
				if (this.LastSnap != null)
				{
					float magnitude = (BuildController.Instance.MainCamera.transform.position - this.LastSnap.GetRealPos()).magnitude;
					Vector3 point = ray.GetPoint(magnitude);
					if ((point - this.LastSnap.GetRealPos()).magnitude > 1f)
					{
						this.LastSnap = null;
					}
				}
				if (this.LastSnap == null)
				{
					HashSet<Furniture> hashSet = new HashSet<Furniture>();
					if (room != null)
					{
						hashSet.AddRange(room.GetFurnitures());
						this.DrawSnaps(room);
					}
					if (room2 != null && room != room2)
					{
						hashSet.AddRange(room2.GetFurnitures());
						this.DrawSnaps(room2);
					}
					foreach (Furniture furniture in hashSet)
					{
						foreach (SnapPoint snapPoint2 in from x in furniture.SnapPoints
						where (x.UsedBy == null || (this.IsProto && !this.CopyProto && x.UsedBy == this.Furn)) && x.Name.Equals(this.Furn.SnapsTo)
						select x)
						{
							Vector3 a3 = base.transform.position;
							if (!usePos)
							{
								float magnitude2 = (BuildController.Instance.MainCamera.transform.position - snapPoint2.transform.position).magnitude;
								a3 = ray.GetPoint(magnitude2);
							}
							Vector3 realPos = snapPoint2.GetRealPos();
							if ((a3 - realPos).magnitude <= BuildController.Instance.SnapDistance)
							{
								Vector3 vector2 = realPos;
								Quaternion quaternion = Quaternion.Euler(0f, this.rotationOffset, 0f) * snapPoint2.transform.rotation;
								if (this.IsValid(Matrix4x4.TRS(vector2, quaternion, Vector3.one), snapPoint2.Parent.Parent, snapPoint2.Parent))
								{
									this.LastRoom = snapPoint2.Parent.Parent;
									this.LastSnap = snapPoint2;
									if (!usePos)
									{
										base.transform.position = vector2;
										base.transform.rotation = quaternion;
									}
									break;
								}
							}
						}
						if (this.LastSnap != null)
						{
							this.UpdateTick();
							break;
						}
					}
				}
				if (this.LastSnap == null)
				{
					base.transform.position = new Vector3(p1.x, (float)(GameSettings.Instance.ActiveFloor * 2), p1.y);
					ErrorOverlay.Instance.ShowError("FurnitureSnapError", false, false, 0f, true);
				}
			}
			else
			{
				if (this.Furn.CanRotate)
				{
					if (InputController.GetKeyDown(InputController.Keys.FurnitureClock, false))
					{
						base.transform.rotation = this.GetFurnitureRotation() * Quaternion.Euler(0f, this.RotationSnap, 0f);
						HintController.Instance.Show(HintController.Hints.HintGridSnapToWall);
					}
					if (InputController.GetKeyDown(InputController.Keys.FurnitureAntiClock, false))
					{
						base.transform.rotation = this.GetFurnitureRotation() * Quaternion.Euler(0f, -this.RotationSnap, 0f);
						HintController.Instance.Show(HintController.Hints.HintGridSnapToWall);
					}
					this.UpdateRotSound();
				}
				Vector2 p3 = (!usePos) ? Vector2.zero : base.transform.position.FlattenVector3();
				if (!usePos)
				{
					bool flag2 = false;
					if (this.Furn.Type.Equals("Elevator"))
					{
						Vector2 mousePos3 = BuildController.Instance.GetMousePos(new Plane(Vector3.up, Vector3.up * (float)(GameSettings.Instance.ActiveFloor * 2)));
						Vector3 pp = new Vector3(mousePos3.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 2), mousePos3.y);
						Room roomFromPoint = GameSettings.Instance.sRoomManager.GetRoomFromPoint(pp);
						if (roomFromPoint != null)
						{
							Furniture furniture2 = roomFromPoint.GetFurniture("Elevator").FirstOrDefault((Furniture x) => (x.transform.position - pp).sqrMagnitude < 0.8f);
							if (furniture2 != null)
							{
								p3 = new Vector2(furniture2.OriginalPosition.x, furniture2.OriginalPosition.z);
								this.finalP = p3;
								flag2 = true;
							}
						}
						pp = new Vector3(mousePos3.x, (float)(GameSettings.Instance.ActiveFloor * 2 - 2), mousePos3.y);
						roomFromPoint = GameSettings.Instance.sRoomManager.GetRoomFromPoint(pp);
						if (roomFromPoint != null)
						{
							Furniture furniture3 = roomFromPoint.GetFurniture("Elevator").FirstOrDefault((Furniture x) => (x.transform.position - pp).sqrMagnitude < 0.8f);
							if (furniture3 != null)
							{
								p3 = new Vector2(furniture3.OriginalPosition.x, furniture3.OriginalPosition.z);
								this.finalP = p3;
								flag2 = true;
							}
						}
					}
					if (!flag2)
					{
						this.finalP = BuildController.Instance.GetMousePos(new Plane(Vector3.up, Vector3.up * (float)(GameSettings.Instance.ActiveFloor * 2)));
						Vector3 forward2 = BuildController.Instance.GridMatrix.MultiplyVector(this.GetFurnitureRotation() * new Vector3(0f, 0f, 1f));
						Quaternion rotation2 = Quaternion.Euler(0f, Quaternion.LookRotation(forward2).eulerAngles.y, 0f);
						bool flag3 = BuildController.Instance.GridMatrix.MultiplyVector(Vector3.left).magnitude > 1f;
						Vector3 vector3 = rotation2 * new Vector3((!this.Furn.OnXEdge && !flag3) ? 0.5f : 0f, 0f, (!this.Furn.OnYEdge && !flag3) ? 0.5f : 0f);
						p3 = BuildController.Instance.CorrectMousePos(this.finalP, new Vector2(vector3.x, vector3.z));
					}
					base.transform.position = new Vector3(p3.x, (float)(GameSettings.Instance.ActiveFloor * 2), p3.y);
				}
				if (this.LastRoom == null || !this.LastRoom.IsStrictlyInside(p3) || this.LastRoom.Floor != GameSettings.Instance.ActiveFloor)
				{
					this.LastRoom = FurnitureBuilder.GetBestRoom(GameSettings.Instance.ActiveFloor, p3, this.Furn, base.transform);
					if (this.LastRoom != null)
					{
						text = this.ValidIn(this.Furn, this.LastRoom);
						if (text != null)
						{
							this.LastRoom = null;
							ErrorOverlay.Instance.ShowError(text, false, false, 0f, true);
						}
					}
				}
				if (this.LastRoom != null && text == null)
				{
					this.UpdateTick();
				}
				if (this.LastRoom == null && text == null)
				{
					ErrorOverlay.Instance.ShowError("FurnitureInRoom", false, false, 0f, true);
				}
			}
		}
	}

	public static Room GetBestRoom(int floor, Vector2 p, Furniture furn, Transform trans)
	{
		return FurnitureBuilder.GetBestRoom(floor, p, furn, (!(trans == null)) ? trans.localToWorldMatrix : furn.transform.localToWorldMatrix);
	}

	public static Room GetBestRoom(int floor, Vector2 p, Furniture furn, Matrix4x4 trans)
	{
		FurnitureBuilder.BestRoomCache.Clear();
		for (int i = 0; i < GameSettings.Instance.sRoomManager.Rooms.Count; i++)
		{
			Room room = GameSettings.Instance.sRoomManager.Rooms[i];
			if (room.Floor == floor && room.IsInside(p, -0.01f))
			{
				FurnitureBuilder.BestRoomCache.Add(room);
			}
		}
		if (FurnitureBuilder.BestRoomCache.Count == 0)
		{
			return null;
		}
		if (FurnitureBuilder.BestRoomCache.Count == 1)
		{
			return FurnitureBuilder.BestRoomCache[0];
		}
		p = furn.GetMeanPos(trans);
		for (int j = 0; j < FurnitureBuilder.BestRoomCache.Count; j++)
		{
			Room room2 = FurnitureBuilder.BestRoomCache[j];
			if (room2.IsInside(p))
			{
				return room2;
			}
		}
		return FurnitureBuilder.BestRoomCache[0];
	}

	private void UpdateRotSound()
	{
		float y = base.transform.rotation.eulerAngles.y;
		if (Mathf.Abs(this.LastRot - y) > 1f)
		{
			UISoundFX.PlaySFX("FurnRotate", -1f, 0f);
		}
		this.LastRot = y;
	}

	private void WallBuildCode(Room rr, bool usePos)
	{
		Vector2 pos = (!usePos) ? BuildController.Instance.GetMousePos(new Plane(Vector3.up, Vector3.up * ((float)(GameSettings.Instance.ActiveFloor * 2) + this.Furn.YOffset))) : base.transform.position.FlattenVector3();
		Vector2 p = (!usePos) ? BuildController.Instance.CorrectMousePos(pos, true) : pos;
		IEnumerable<Room> enumerable;
		if (rr == null)
		{
			enumerable = from x in GameSettings.Instance.sRoomManager.Rooms
			where x.Floor == GameSettings.Instance.ActiveFloor && x.IsInside(pos)
			select x;
		}
		else
		{
			(enumerable = new Room[1])[0] = rr;
		}
		IEnumerable<Room> enumerable2 = enumerable;
		this.edge1 = null;
		this.edge2 = null;
		if (!usePos)
		{
			base.transform.position = new Vector3(pos.x, (float)(GameSettings.Instance.ActiveFloor * 2), pos.y);
		}
		float num = this.Furn.WallWidth / 2f + Room.WallOffset / 2f;
		float num2 = 1f;
		foreach (Room room in enumerable2)
		{
			if (!room.Outdoors || this.Furn.ValidOutdoors)
			{
				foreach (WallEdge wallEdge in room.Edges)
				{
					WallEdge a = wallEdge;
					WallEdge wallEdge2 = wallEdge.Links[room];
					if (a.Pos.Dist(wallEdge2.Pos) >= num * 2f)
					{
						if (this.Furn.OnlyExteriorWalls && wallEdge2.Links.ContainsValue(a))
						{
							Room key = wallEdge2.Links.First((KeyValuePair<Room, WallEdge> x) => x.Value == a).Key;
							if (this.Furn.PokesThroughWall || (!room.Outdoors && !key.Outdoors))
							{
								continue;
							}
						}
						Vector2? vector = Utilities.ProjectToLine(pos, a.Pos, wallEdge2.Pos);
						if (vector != null)
						{
							float num3 = vector.Value.Dist(pos);
							if (num3 < num2)
							{
								Vector2? vector2 = Utilities.ProjectToLine(p, a.Pos, wallEdge2.Pos);
								if (vector2 != null)
								{
									if (vector2.Value.Dist(a.Pos) < num)
									{
										vector2 = new Vector2?(a.Pos + (wallEdge2.Pos - a.Pos).normalized * num);
									}
									if (vector2.Value.Dist(wallEdge2.Pos) < num)
									{
										vector2 = new Vector2?(wallEdge2.Pos + (a.Pos - wallEdge2.Pos).normalized * num);
									}
									Vector2 value = vector2.Value;
									if (a.ValidSegment(ref value, this.Furn.WallWidth, wallEdge2, true, true, this.Furn.ValidOnFence, this.Furn.Height1, this.Furn.Height2, false, vector.Value.x - value.x, vector.Value.y - value.y, false, null))
									{
										num2 = num3;
										base.transform.position = new Vector3(value.x, (float)(GameSettings.Instance.ActiveFloor * 2), value.y);
										Vector2 vector3 = wallEdge2.Pos - a.Pos;
										base.transform.transform.rotation = Quaternion.LookRotation(new Vector3(-vector3.y, 0f, vector3.x));
										this.edge1 = a;
										this.edge2 = wallEdge2;
										this.WallPosition = a.Pos.Dist(value) / a.Pos.Dist(wallEdge2.Pos);
										this.LastRoom = room;
									}
								}
							}
						}
					}
				}
				if (this.edge1 != null)
				{
					this.UpdateTick();
					break;
				}
			}
		}
		if (this.edge1 == null)
		{
			ErrorOverlay.Instance.ShowError((!this.Furn.OnlyExteriorWalls) ? "FurnitureOnWall" : "ExteriorWallFurn", false, false, 0f, true);
		}
	}

	private string GetHint(bool rotate, bool rotateKey, bool alignKey)
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (rotate)
		{
			if (rotateKey)
			{
				stringBuilder.AppendLine("RotateFurnHint2".Loc(new object[]
				{
					InputController.GetKeyBindString(InputController.Keys.FurnitureAntiClock, false),
					InputController.GetKeyBindString(InputController.Keys.FurnitureClock, false)
				}));
			}
			else
			{
				stringBuilder.AppendLine("RotateFurnHint".Loc());
			}
		}
		if (alignKey)
		{
			stringBuilder.AppendLine("AlignWallKeyHint".Loc(new object[]
			{
				InputController.GetKeyBindString(InputController.Keys.AlignNearest, false)
			}));
		}
		stringBuilder.AppendLine("ShiftFurnHint".Loc());
		return stringBuilder.ToString().Trim();
	}

	private string ValidIn(Furniture furn, Room room)
	{
		if (room.Outdoors)
		{
			return (!furn.ValidOutdoors) ? "IndoorError" : null;
		}
		return (!furn.ValidIndoors) ? "OutdoorError" : null;
	}

	private bool CheckAllValid()
	{
		return this.CheckBasement() && ((this.IsProto && !this.CopyProto) || GameSettings.Instance.MyCompany.CanMakeTransaction(-this.Furn.Cost)) && (!this.Furn.WallFurn || this.edge1 != null) && (!this.Furn.IsSnapping || this.LastSnap != null) && this.LastRoom != null && this.IsValid(Matrix4x4.TRS(base.transform.position, this.GetFurnitureRotation(), Vector3.one), this.LastRoom, (!(this.LastSnap != null)) ? null : this.LastSnap.Parent) && (!this.Furn.TwoFloors || this.IsValid(Matrix4x4.TRS(base.transform.position, this.GetFurnitureRotation(), Vector3.one), FurnitureBuilder.GetBestRoom(this.LastRoom.Floor + 1, base.transform.position.FlattenVector3(), this.Furn, base.transform), null));
	}

	private void UpdateTick()
	{
		if (base.transform.position != this.LastPos)
		{
			UISoundFX.PlaySFX("Tick", -1f, 0f);
		}
		this.LastPos = base.transform.position;
	}

	private void Update()
	{
		if (this.IsProto && (this.FurnPrefab == null || this.Furn == null))
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		if (this.Parent != null)
		{
			this.Rend.sharedMaterial = this.Parent.Rend.sharedMaterial;
			return;
		}
		if (this.rotationCountDown >= 0f)
		{
			this.rotationCountDown -= Time.deltaTime;
		}
		if (Input.GetMouseButtonUp(1))
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		if (this.Furn.WallFurn)
		{
			this.WallBuildCode(null, false);
		}
		else
		{
			if (!this.Furn.IsSnapping && this.Furn.CanRotate && this.LastRoom != null && InputController.GetKeyDown(InputController.Keys.AlignNearest, false))
			{
				bool flag = false;
				Vector2 vector = base.transform.position.FlattenVector3();
				float num = 25f;
				Vector2 vector2 = Vector2.zero;
				Vector2 vector3 = Vector2.zero;
				for (int i = 0; i < this.LastRoom.Edges.Count; i++)
				{
					WallEdge wallEdge = this.LastRoom.Edges[i];
					WallEdge wallEdge2 = this.LastRoom.Edges[(i + 1) % this.LastRoom.Edges.Count];
					Vector2? vector4 = Utilities.ProjectToLine(vector, wallEdge.Pos, wallEdge2.Pos);
					if (vector4 != null)
					{
						float sqrMagnitude = (vector - vector4.Value).sqrMagnitude;
						if (sqrMagnitude < num)
						{
							flag = true;
							num = sqrMagnitude;
							vector2 = wallEdge.Pos;
							vector3 = wallEdge2.Pos;
						}
					}
				}
				if (flag)
				{
					if ((vector3 - vector).sqrMagnitude < (vector2 - vector).sqrMagnitude)
					{
						BuildController.Instance.SetTempGrid(vector3, vector2, true);
					}
					else
					{
						BuildController.Instance.SetTempGrid(vector2, vector3, false);
					}
					Vector3 forward = BuildController.Instance.GridMatrix.inverse.MultiplyVector(Vector3.forward);
					base.transform.rotation = Quaternion.LookRotation(forward);
				}
			}
			this.NormalBuildCode(false);
		}
		float cost = this.GetCost();
		if (cost > 0f)
		{
			CostDisplay.Instance.Show(cost, base.transform.position + Vector3.up * 2.5f, (GameSettings.Instance.MyCompany.Money < cost) ? Color.red : Color.white);
		}
		bool flag2 = this.CheckAllValid();
		this.Rend.sharedMaterial = ((!flag2) ? this.Red : this.Green);
		if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
		{
			this.rotationCountDown = 0.2f;
			this.TurnMode = true;
			this.OriginalPosition = base.transform.position;
			this.OriginalSnap = this.LastSnap;
		}
		if (Input.GetMouseButtonUp(0) && this.TurnMode)
		{
			this.TurnMode = false;
			if (flag2)
			{
				List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
				if (this.IsProto && !this.CopyProto && this.Children.Count > 0)
				{
					base.transform.rotation = this.GetFurnitureRotation();
					foreach (FurnitureBuilder furnitureBuilder in this.Children)
					{
						furnitureBuilder.transform.parent.SetParent(null, true);
						if (furnitureBuilder.Furn.WallFurn)
						{
							furnitureBuilder.WallBuildCode(this.LastRoom, true);
						}
						else
						{
							furnitureBuilder.NormalBuildCode(true);
						}
					}
					List<FurnitureBuilder> list2 = new List<FurnitureBuilder>(this.Children);
					list2.Add(this);
					bool flag3 = true;
					while (flag3)
					{
						flag3 = false;
						for (int j = 0; j < list2.Count; j++)
						{
							FurnitureBuilder furnitureBuilder2 = list2[j];
							if (!furnitureBuilder2.CheckAllValid())
							{
								furnitureBuilder2.MakeAllIgnored(false);
								flag3 = true;
								list2.RemoveAt(j);
								j--;
							}
						}
					}
					foreach (FurnitureBuilder furnitureBuilder3 in list2)
					{
						furnitureBuilder3.FinalBuild(list);
					}
				}
				else
				{
					this.FinalBuild(list);
					Quaternion rotation = base.transform.rotation;
					base.transform.rotation = this.GetFurnitureRotation();
					foreach (FurnitureBuilder furnitureBuilder4 in this.Children)
					{
						furnitureBuilder4.transform.parent.SetParent(null, true);
						if (furnitureBuilder4.Furn.WallFurn)
						{
							furnitureBuilder4.WallBuildCode(this.LastRoom, true);
						}
						else
						{
							furnitureBuilder4.NormalBuildCode(true);
						}
						if (furnitureBuilder4.CheckAllValid())
						{
							furnitureBuilder4.FinalBuild(list);
						}
						furnitureBuilder4.transform.parent.SetParent(base.transform, false);
					}
					base.transform.rotation = rotation;
				}
				if (list.Count > 0)
				{
					GameSettings.Instance.AddUndo(list.ToArray());
				}
				if ((!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)) || (this.IsProto && !this.CopyProto))
				{
					UnityEngine.Object.Destroy(base.gameObject);
				}
			}
			else
			{
				UISoundFX.PlaySFX("BuildError", -1f, 0f);
			}
		}
		if (!this.Furn.WallFurn)
		{
			Vector3 toDirection = this.LerpingPos - base.transform.position;
			Vector2 vector5 = Vector2.ClampMagnitude(new Vector2(toDirection.x, toDirection.z), 0.5f);
			toDirection = new Vector3(vector5.x, 1f, vector5.y);
			this.LerpingPos = base.transform.position + new Vector3(vector5.x, 0f, vector5.y);
			this.LerpingPos = Vector3.Lerp(this.LerpingPos, base.transform.position, Time.deltaTime * 10f);
			Quaternion quaternion = Quaternion.FromToRotation(Vector3.up, toDirection) * this.GetFurnitureRotation();
			base.transform.rotation = Quaternion.Euler(quaternion.eulerAngles.x, base.transform.rotation.eulerAngles.y, quaternion.eulerAngles.z);
		}
	}

	private void FinalBuild(List<UndoObject.UndoAction> undo)
	{
		if (this.IsProto)
		{
			if (this.CopyProto)
			{
				if (this.Parent == null)
				{
					CostDisplay.Instance.FloatAway();
					UISoundFX.PlaySFX("PlaceFurniture", -1f, 0f);
					UISoundFX.PlaySFX("Kaching", -1f, 0f);
				}
				Furniture furniture = FurnitureBuilder.MakeFurn(base.transform.position, this.GetFurnitureRotation(), this.LastRoom, this.edge1, this.edge2, this.WallPosition, this.LastSnap, this.FurnPrefab, this.rotationOffset, true, this.Furn.Cost);
				undo.Add(new UndoObject.UndoAction(furniture, true));
				this.LastSnap = null;
				FurnitureBuilder.CopyStyle(this.Furn, furniture);
				this.TraverseCopy(this.Furn, furniture);
			}
			else
			{
				Furniture component = this.FurnPrefab.GetComponent<Furniture>();
				undo.Add(new UndoObject.UndoAction(component, component.Parent, component.OriginalPosition, component.transform.rotation, component.RotationOffset, component.SnappedTo));
				FurnitureBuilder.MoveFurn(base.transform.position, this.GetFurnitureRotation(), this.LastRoom, this.edge1, this.edge2, this.WallPosition, this.LastSnap, this.FurnPrefab, this.rotationOffset);
				this.LastSnap = null;
				this.Furn.UpdateBoundsMesh();
				FurnitureBuilder.TraverseMove(this.Furn, this.LastRoom, this.edge1, this.edge2, this.WallPosition);
			}
		}
		else
		{
			if (this.Parent == null)
			{
				CostDisplay.Instance.FloatAway();
				UISoundFX.PlaySFX("PlaceFurniture", -1f, 0f);
				UISoundFX.PlaySFX("Kaching", -1f, 0f);
			}
			Furniture snap = FurnitureBuilder.MakeFurn(base.transform.position, this.GetFurnitureRotation(), this.LastRoom, this.edge1, this.edge2, this.WallPosition, this.LastSnap, this.FurnPrefab, this.rotationOffset, false, this.GetCost());
			undo.Add(new UndoObject.UndoAction(snap, true));
			this.LastSnap = null;
		}
	}

	private Quaternion GetFurnitureRotation()
	{
		return Quaternion.Euler(0f, base.transform.rotation.eulerAngles.y, 0f);
	}

	private bool CheckBasement()
	{
		if (!this.Furn.BasementValid && GameSettings.Instance.ActiveFloor < 0)
		{
			ErrorOverlay.Instance.ShowError("BasementFurnError", false, false, 0f, true);
			return false;
		}
		return true;
	}

	public float GetCost()
	{
		float num = 0f;
		for (int i = 0; i < this.Children.Count; i++)
		{
			num += this.Children[i].GetCost();
		}
		if (!this.IsProto)
		{
			return this.Furn.Cost;
		}
		if (this.CopyProto)
		{
			return num + this.Furn.Cost + (from x in this.Furn.SnapPoints
			where x.UsedBy != null
			select x).Sum((SnapPoint x) => x.UsedBy.Cost);
		}
		return num;
	}

	private void TraverseCopy(Furniture f, Furniture replacement)
	{
		foreach (SnapPoint snapPoint in f.SnapPoints)
		{
			if (snapPoint.UsedBy != null)
			{
				int localId = snapPoint.Id;
				SnapPoint snapPoint2 = replacement.SnapPoints.First((SnapPoint x) => x.Id == localId);
				Furniture furniture = FurnitureBuilder.MakeFurn(snapPoint2.transform.position, Quaternion.Euler(0f, snapPoint.UsedBy.RotationOffset, 0f) * snapPoint2.transform.rotation, this.LastRoom, this.edge1, this.edge2, this.WallPosition, snapPoint2, snapPoint.UsedBy.gameObject, snapPoint.UsedBy.RotationOffset, true, snapPoint.UsedBy.gameObject.GetComponent<Furniture>().Cost);
				this.LastSnap = null;
				FurnitureBuilder.CopyStyle(snapPoint.UsedBy, furniture);
				this.TraverseCopy(snapPoint.UsedBy, furniture);
			}
		}
	}

	public static void TraverseMove(Furniture f, Room room, WallEdge e1, WallEdge e2, float wallPos)
	{
		foreach (SnapPoint snapPoint in f.SnapPoints)
		{
			if (snapPoint.UsedBy != null)
			{
				FurnitureBuilder.MoveFurn(snapPoint.GetRealPos(), Quaternion.Euler(0f, snapPoint.UsedBy.RotationOffset, 0f) * snapPoint.transform.rotation, room, e1, e2, wallPos, snapPoint, snapPoint.UsedBy.gameObject, snapPoint.UsedBy.RotationOffset);
				snapPoint.UsedBy.UpdateBoundsMesh();
				FurnitureBuilder.TraverseMove(snapPoint.UsedBy, room, e1, e2, wallPos);
			}
		}
	}

	public static void CopyStyle(Furniture furn1, Furniture furn2)
	{
		furn2.name = furn2.name.Replace("(Clone)", string.Empty).Trim();
		if (furn2.ColorPrimaryEnabled)
		{
			furn2.ColorPrimary = furn1.ColorPrimary;
		}
		if (furn2.ColorSecondaryEnabled)
		{
			furn2.ColorSecondary = furn1.ColorSecondary;
		}
		if (furn2.ColorTertiaryEnabled)
		{
			furn2.ColorTertiary = furn1.ColorTertiary;
		}
		furn2.DisableInitColor = true;
	}

	public static void CopyStyle(BuildingPrefab.FurnitureObject furn1, Furniture furn2)
	{
		furn2.name = furn2.name.Replace("(Clone)", string.Empty).Trim();
		if (furn2.ColorPrimaryEnabled)
		{
			furn2.ColorPrimary = furn1.Colors[0];
		}
		if (furn2.ColorSecondaryEnabled)
		{
			furn2.ColorSecondary = furn1.Colors[1];
		}
		if (furn2.ColorTertiaryEnabled)
		{
			furn2.ColorTertiary = furn1.Colors[2];
		}
		furn2.DisableInitColor = true;
	}

	public static Furniture MakeFurn(Vector3 pos, Quaternion rot, Room room, WallEdge edge1, WallEdge edge2, float wallPos, SnapPoint snap, GameObject furnFab, float rotOffset, bool isInstance, float cost)
	{
		if (isInstance)
		{
			furnFab = ObjectDatabase.Instance.GetFurniture(furnFab.name);
		}
		GameSettings.Instance.MyCompany.MakeTransaction(-cost, Company.TransactionCategory.Construction, "Furniture");
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(furnFab);
		Furniture component = gameObject.GetComponent<Furniture>();
		component.transform.position = pos;
		component.OriginalPosition = pos;
		component.transform.rotation = rot;
		component.Parent = room;
		if (component.WallFurn)
		{
			component.Init(edge1, edge2, wallPos, false);
		}
		if (component.IsSnapping)
		{
			component.SnappedTo = snap;
			component.RotationOffset = rotOffset;
			snap.UsedBy = component;
		}
		if (component.SnapsTo.Equals("OnTable"))
		{
			room.RecalculateTableGroups();
		}
		if (snap != null && snap.Parent.Table != null)
		{
			snap.Parent.Table.UpdateStatus();
		}
		component.UpdateFreeNavs(false);
		room.RecalculateStateVariables(false);
		room.RefreshNoise();
		if (component.TwoFloors)
		{
			component.Parent.DirtyRoofMesh = true;
			component.ExtraParent = FurnitureBuilder.GetBestRoom(room.Floor + 1, pos.FlattenVector3(), component, null);
			if (component.ExtraParent != null)
			{
				component.ExtraParent.AddFurniture(component);
				component.ExtraParent.DirtyNavMesh = true;
				component.ExtraParent.DirtyPathNodes = true;
				component.ExtraParent.DirtyFloorMesh = true;
			}
		}
		component.gameObject.SetActive(true);
		return component;
	}

	public static void MoveFurn(Vector3 pos, Quaternion rot, Room room, WallEdge edge1, WallEdge edge2, float wallPos, SnapPoint snap, GameObject f, float rotOffset)
	{
		Furniture component = f.GetComponent<Furniture>();
		component.SymbolicDestroy();
		component.transform.position = pos;
		component.OriginalPosition = pos;
		component.transform.rotation = rot;
		component.Parent = room;
		room.AddFurniture(component);
		if (component.WallFurn)
		{
			component.Init(edge1, edge2, wallPos, false);
		}
		if (component.IsSnapping)
		{
			component.SnappedTo = snap;
			component.RotationOffset = rotOffset;
			snap.UsedBy = component;
		}
		Server component2 = component.GetComponent<Server>();
		if (component2 != null)
		{
			component2.ReWireAll();
		}
		component.UpdateBoundaryPoints();
		component.CleanFloor();
		if (component.Height1 < Actor.HumanHeight && component.FinalBoundary != null && component.FinalBoundary.Length > 0)
		{
			room.DirtyNavMesh = true;
		}
		if (component.Table != null || component.SnapsTo.Equals("OnTable"))
		{
			room.RecalculateTableGroups();
		}
		if (snap != null && snap.Parent.Table != null)
		{
			snap.Parent.Table.UpdateStatus();
		}
		component.CalcEdge();
		component.UpdateFreeNavs(false);
		room.RecalculateStateVariables(false);
		room.RefreshNoise();
		Furniture.UpdateEdgeDetection();
		if (component.TwoFloors)
		{
			component.Parent.DirtyRoofMesh = true;
			component.ExtraParent = FurnitureBuilder.GetBestRoom(room.Floor + 1, pos.FlattenVector3(), component, null);
			if (component.ExtraParent != null)
			{
				component.ExtraParent.AddFurniture(component);
				component.ExtraParent.DirtyNavMesh = true;
				component.ExtraParent.DirtyPathNodes = true;
				component.ExtraParent.DirtyFloorMesh = true;
			}
		}
		if (component.IsConnecter)
		{
			component.pathNode.Point = component.transform.position + Vector3.up * 0.5f;
		}
	}

	private void OnDrawGizmos()
	{
		foreach (Vector2[] points in this.BuildBoundary)
		{
			Vector2[] realPoints = this.GetRealPoints(base.transform.localToWorldMatrix, points);
			Gizmos.color = Color.yellow;
			for (int i = 0; i < realPoints.Length; i++)
			{
				int num = (i + 1) % realPoints.Length;
				Gizmos.DrawLine(new Vector3(realPoints[i].x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), realPoints[i].y), new Vector3(realPoints[num].x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), realPoints[num].y));
			}
		}
		Gizmos.color = Color.white;
		Gizmos.DrawSphere(this.LerpingPos, 0.1f);
	}

	public GameObject FurnPrefab;

	public Material Green;

	public Material Red;

	public Renderer Rend;

	[NonSerialized]
	public Room LastRoom;

	public SnapPoint LastSnap;

	public SnapPoint OriginalSnap;

	public float RotationSnap = 45f;

	private Furniture Furn;

	public bool IsProto;

	public bool CopyProto;

	private bool TurnMode;

	private float rotationCountDown;

	private float rotationOffset;

	private WallEdge edge1;

	private WallEdge edge2;

	private float WallPosition;

	[NonSerialized]
	private List<Vector2[]> BuildBoundary = new List<Vector2[]>();

	[NonSerialized]
	private List<Vector2> Heights = new List<Vector2>();

	public Material SnapIndicatorMat;

	public Mesh SnapIndicatorMesh;

	public GameObject Arrow;

	public Vector3 LerpingPos;

	[NonSerialized]
	public FurnitureBuilder Parent;

	[NonSerialized]
	public List<FurnitureBuilder> Children = new List<FurnitureBuilder>();

	public static int GPUInstanceArrows = 128;

	[NonSerialized]
	private readonly List<Matrix4x4[]> _snaps = new List<Matrix4x4[]>();

	public float LastRot;

	private static List<Room> BestRoomCache = new List<Room>();

	private Vector2 finalP;

	private Vector3 OriginalPosition;

	private Vector3 LastPos;
}
