﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TriangleNode
{
	public TriangleNode(Vector2[] points, int[] indices)
	{
		this.Points = points;
		this.PortalPoints = this.Points.ToArray<Vector2>();
		this.PointIndices = indices;
		bool flag = true;
		this.HashCode = 0;
		for (int i = 0; i < this.Points.Length; i++)
		{
			if (flag)
			{
				flag = false;
				this.HashCode = this.Points[i].x.GetHashCode();
			}
			else
			{
				this.HashCode = (this.HashCode * 397 ^ this.Points[i].x.GetHashCode());
			}
			this.HashCode = (this.HashCode * 397 ^ this.Points[i].y.GetHashCode());
		}
		this.Center = Utilities.GetTriangleCentroid(this.Points);
		this.PathNode = new PathNode<TriangleNode>(this, this);
		this.Area = this.GetArea();
		float num = float.MaxValue;
		float num2 = float.MaxValue;
		float num3 = float.MinValue;
		float num4 = float.MinValue;
		for (int j = 0; j < points.Length; j++)
		{
			num = Mathf.Min(points[j].x, num);
			num2 = Mathf.Min(points[j].y, num2);
			num3 = Mathf.Max(points[j].x, num3);
			num4 = Mathf.Max(points[j].y, num4);
		}
		this.rect = new Rect(num, num2, num3 - num, num4 - num2);
	}

	public override int GetHashCode()
	{
		return this.HashCode;
	}

	private bool CheckProjection(Vector2 a, Vector2 b, Vector2 c, float max)
	{
		Vector2? vector = Utilities.ProjectToLine(a, b, c);
		return vector == null || (a - vector.Value).magnitude > max;
	}

	public bool IsInside(Vector2 p)
	{
		return this.rect.ContainsEntirely(p) && this.InsideTriangle(p, this.Points[0], this.Points[1], this.Points[2]);
	}

	public void SetConnection(TriangleNode n, int e, float minWidthPenalty)
	{
		if ((this.Points[e] - this.Points[(e + 1) % 3]).sqrMagnitude < minWidthPenalty)
		{
			this.Weight[n] = 20f;
		}
		else
		{
			this.Weight[n] = 1f;
		}
		this.Connections[e] = n;
		this.PathNode.AddConnection(n.PathNode);
	}

	public void UpdateWeight()
	{
		int num = 0;
		for (int i = 0; i < this.Connections.Length; i++)
		{
			if (this.Connections[i] != null)
			{
				num++;
				float orDefault = this.Connections[i].Weight.GetOrDefault(this, 0f);
				float num2 = (this.Points[i] - this.Points[(i + 1) % 3]).sqrMagnitude;
				if (num2 < 0.36f)
				{
					float num3 = 20f * (2f - num2 / 0.36f);
					if (num3 > orDefault)
					{
						this.Weight[this.Connections[i]] = num3;
						this.Connections[i].Weight[this] = num3;
					}
				}
				else if (this.Connections[(i + 1) % 3] == null)
				{
					num2 = 2f * this.Area / (this.Points[(i + 2) % 3] - this.Points[(i + 1) % 3]).magnitude;
					if (num2 < 0.36f)
					{
						float num4 = 20f * (2f - num2 / 0.36f);
						if (num4 > orDefault)
						{
							this.Weight[this.Connections[i]] = num4;
							this.Connections[i].Weight[this] = num4;
						}
					}
				}
			}
		}
		if (num == 3 && this.Area > 1f)
		{
			for (int j = 0; j < this.Connections.Length; j++)
			{
				TriangleNode triangleNode = this.Connections[j];
				float orDefault2 = this.Weight.GetOrDefault(triangleNode, 1f);
				float num5 = 0.5f + (1f - (Mathf.Min(2f, this.Area) - 1f)) * 0.5f;
				if (orDefault2 <= 1f && num5 < orDefault2)
				{
					this.Weight[triangleNode] = num5;
					triangleNode.Weight[this] = num5;
				}
			}
		}
	}

	public static KeyValuePair<Vector2[], int[]> GenerateMesh(IEnumerable<TriangleNode> nodes)
	{
		Dictionary<Vector2, int> dictionary = new Dictionary<Vector2, int>();
		List<int> list = new List<int>();
		int num = 0;
		foreach (TriangleNode triangleNode in nodes)
		{
			for (int i = 0; i < 3; i++)
			{
				int item = -1;
				if (dictionary.TryGetValue(triangleNode.PortalPoints[i], out item))
				{
					list.Add(item);
				}
				else
				{
					dictionary.Add(triangleNode.PortalPoints[i], num);
					list.Add(num);
					num++;
				}
			}
		}
		return new KeyValuePair<Vector2[], int[]>((from x in dictionary
		orderby x.Value
		select x.Key).ToArray<Vector2>(), list.ToArray());
	}

	public void FixPortalsInitial(Dictionary<int, Vector2> offsets, float agentRadius)
	{
		TriangleNode.PortalCheck[0] = this.Points[0];
		TriangleNode.PortalCheck[1] = this.Points[1];
		TriangleNode.PortalCheck[2] = this.Points[2];
		for (int i = 0; i < this.Points.Length; i++)
		{
			int key = this.PointIndices[i];
			Vector2 vector;
			if (offsets.TryGetValue(key, out vector))
			{
				TriangleNode.PortalCheck[i] = vector;
				if (2f * this.Area / (this.Points[(i + 2) % 3] - this.Points[(i + 1) % 3]).magnitude < agentRadius * 0.5f)
				{
					offsets.Remove(key);
				}
				else
				{
					Vector2 vector2 = TriangleNode.PortalCheck[i] - TriangleNode.PortalCheck[(i + 1) % 3];
					Vector2 vector3 = TriangleNode.PortalCheck[(i + 2) % 3] - TriangleNode.PortalCheck[(i + 1) % 3];
					float num = vector2.x * vector3.y - vector2.y * vector3.x;
					if (num > 0f)
					{
						offsets.Remove(key);
					}
				}
			}
		}
	}

	public void FixPortals(Dictionary<int, Vector2> offsets)
	{
		for (int i = 0; i < this.Points.Length; i++)
		{
			int key = this.PointIndices[i];
			Vector2 vector;
			if (offsets.TryGetValue(key, out vector))
			{
				this.PortalPoints[i] = vector;
			}
		}
	}

	public static TriangleNode[] GenerateMap(Vector2[] vertices, int[] indices, float agentRadius)
	{
		int num = indices.Length / 3;
		int[][] array = new int[num][];
		int num2 = num * 3;
		if (num2 != indices.Length)
		{
			throw new Exception(string.Concat(new object[]
			{
				"Got vertex count for navmesh: ",
				indices.Length,
				", but should have: ",
				num2
			}));
		}
		int num3 = 0;
		for (int i = 0; i < num2; i += 3)
		{
			if (indices[i] != -1 && indices[i + 1] != -1 && indices[i + 2] != -1)
			{
				if (indices[i] >= vertices.Length || indices[i] < 0)
				{
					throw new Exception(string.Concat(new object[]
					{
						"Got out of bounds vertex index: ",
						indices[i],
						", with vertices: ",
						vertices.Length
					}));
				}
				if (indices[i + 1] >= vertices.Length || indices[i + 1] < 0)
				{
					throw new Exception(string.Concat(new object[]
					{
						"Got out of bounds vertex index: ",
						indices[i + 1],
						", with vertices: ",
						vertices.Length
					}));
				}
				if (indices[i + 2] >= vertices.Length || indices[i + 2] < 0)
				{
					throw new Exception(string.Concat(new object[]
					{
						"Got out of bounds vertex index: ",
						indices[i + 2],
						", with vertices: ",
						vertices.Length
					}));
				}
				Vector2 vector = vertices[indices[i]] - vertices[indices[i + 1]];
				Vector2 vector2 = vertices[indices[i + 2]] - vertices[indices[i + 1]];
				float num4 = vector.x * vector2.y - vector.y * vector2.x;
				if (num4 < 0f)
				{
					array[num3] = new int[]
					{
						indices[i],
						indices[i + 1],
						indices[i + 2]
					};
					num3++;
				}
				else if (num4 > 0f)
				{
					array[num3] = new int[]
					{
						indices[i + 2],
						indices[i + 1],
						indices[i]
					};
					num3++;
				}
			}
		}
		TriangleNode[] array2 = new TriangleNode[num3];
		Dictionary<int, List<KeyValuePair<int, int>>> dictionary = new Dictionary<int, List<KeyValuePair<int, int>>>();
		for (int j = 0; j < array2.Length; j++)
		{
			for (int k = j + 1; k < array2.Length; k++)
			{
				int[] array3 = TriangleNode.ShareEdge(array[j], array[k]);
				if (array3 != null)
				{
					dictionary.Append(j, new KeyValuePair<int, int>(k, array3[0]));
					dictionary.Append(k, new KeyValuePair<int, int>(j, array3[1]));
				}
			}
		}
		for (int l = 0; l < array2.Length; l++)
		{
			int[] array4 = array[l];
			array2[l] = new TriangleNode(new Vector2[]
			{
				vertices[array4[0]],
				vertices[array4[1]],
				vertices[array4[2]]
			}, array4);
		}
		float minWidthPenalty = agentRadius * agentRadius;
		foreach (KeyValuePair<int, List<KeyValuePair<int, int>>> keyValuePair in dictionary)
		{
			for (int m = 0; m < keyValuePair.Value.Count; m++)
			{
				KeyValuePair<int, int> keyValuePair2 = keyValuePair.Value[m];
				array2[keyValuePair.Key].SetConnection(array2[keyValuePair2.Key], keyValuePair2.Value, minWidthPenalty);
			}
		}
		int[,] array5 = new int[vertices.Length, 2];
		for (int n = 0; n < array5.GetLength(0); n++)
		{
			array5[n, 0] = -1;
			array5[n, 1] = -1;
		}
		for (int num5 = 0; num5 < array2.Length; num5++)
		{
			for (int num6 = 0; num6 < 3; num6++)
			{
				if (array2[num5].Connections[num6] == null)
				{
					array5[array2[num5].PointIndices[num6], 0] = array2[num5].PointIndices[(num6 + 1) % 3];
					array5[array2[num5].PointIndices[(num6 + 1) % 3], 1] = array2[num5].PointIndices[num6];
				}
			}
		}
		Dictionary<int, Vector2> dictionary2 = new Dictionary<int, Vector2>();
		for (int num7 = 0; num7 < array5.GetLength(0); num7++)
		{
			int num8 = array5[num7, 0];
			int num9 = array5[num7, 1];
			if (num8 > -1 && num9 > -1)
			{
				Vector2 offset = Utilities.GetOffset(vertices[num9], vertices[num7], vertices[num8], agentRadius, false);
				dictionary2[num7] = offset;
			}
		}
		for (int num10 = 0; num10 < array2.Length; num10++)
		{
			array2[num10].UpdateWeight();
			array2[num10].FixPortalsInitial(dictionary2, agentRadius);
		}
		for (int num11 = 0; num11 < array2.Length; num11++)
		{
			array2[num11].FixPortals(dictionary2);
		}
		return array2;
	}

	public float GetArea()
	{
		float magnitude = (this.Points[0] - this.Points[1]).magnitude;
		float magnitude2 = (this.Points[1] - this.Points[2]).magnitude;
		float magnitude3 = (this.Points[2] - this.Points[0]).magnitude;
		float num = (magnitude + magnitude2 + magnitude3) / 2f;
		return Mathf.Sqrt(num * (num - magnitude) * (num - magnitude2) * (num - magnitude3));
	}

	private static int[] ShareEdge(int[] tr1, int[] tr2)
	{
		for (int i = 0; i < tr1.Length; i++)
		{
			for (int j = 0; j < tr2.Length; j++)
			{
				if (tr1[i] == tr2[j])
				{
					int num = (i != 2) ? (i + 1) : 0;
					int num2 = (j != 0) ? (j - 1) : 2;
					int num3 = (j != 2) ? (j + 1) : 0;
					if (tr1[num] == tr2[num2])
					{
						return new int[]
						{
							i,
							num2
						};
					}
					if (tr1[num] == tr2[num3])
					{
						return new int[]
						{
							i,
							j
						};
					}
				}
			}
		}
		return null;
	}

	private float sign(Vector2 p1, Vector2 p2, Vector2 p3)
	{
		return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
	}

	private bool InsideTriangle(Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3)
	{
		bool flag = this.sign(pt, v1, v2) < 0f;
		bool flag2 = this.sign(pt, v2, v3) < 0f;
		bool flag3 = this.sign(pt, v3, v1) < 0f;
		return flag == flag2 && flag2 == flag3;
	}

	private static int IndexOf(TriangleNode[] array, TriangleNode n)
	{
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i] == n)
			{
				return i;
			}
		}
		return -1;
	}

	public static void GetPortals(List<PathNode<TriangleNode>> nodes, Vector2 start, List<TriangleNode.Portal> result)
	{
		int i = 0;
		while (i < nodes.Count - 1)
		{
			int num = TriangleNode.IndexOf(nodes[i].Point.Connections, nodes[i + 1].Point);
			if (num < 0)
			{
				goto IL_97;
			}
			Vector2 vector = nodes[i].Point.PortalPoints[num];
			Vector2 vector2 = nodes[i].Point.PortalPoints[(num + 1) % 3];
			if (i != 0 || Utilities.isLeft(vector, vector2, start) > 0)
			{
				result.Add(new TriangleNode.Portal(vector2, vector));
				goto IL_97;
			}
			IL_116:
			i++;
			continue;
			IL_97:
			num = TriangleNode.IndexOf(nodes[i + 1].Point.Connections, nodes[i].Point);
			if (num >= 0)
			{
				Vector2 l = nodes[i + 1].Point.PortalPoints[num];
				Vector2 r = nodes[i + 1].Point.PortalPoints[(num + 1) % 3];
				result.Add(new TriangleNode.Portal(l, r));
				goto IL_116;
			}
			goto IL_116;
		}
	}

	private static float triarea2(Vector2 a, Vector2 b, Vector2 c)
	{
		float num = b.x - a.x;
		float num2 = b.y - a.y;
		float num3 = c.x - a.x;
		float num4 = c.y - a.y;
		return num3 * num2 - num * num4;
	}

	private static bool FastVectorEquals(Vector2 v1, Vector2 v2)
	{
		return v1.x.Appx(v2.x, 0.0001f) && v1.y.Appx(v2.y, 0.0001f);
	}

	public static List<Vector2> StringPull(List<TriangleNode.Portal> portals)
	{
		List<Vector2> list = new List<Vector2>();
		Vector2 vector = portals[0].Left;
		Vector2 vector2 = portals[0].Left;
		Vector2 vector3 = portals[0].Right;
		int num = 0;
		int num2 = 0;
		list.Add(vector);
		int i = 1;
		while (i < portals.Count)
		{
			Vector2 left = portals[i].Left;
			Vector2 right = portals[i].Right;
			if (TriangleNode.triarea2(vector, vector3, right) > 0f)
			{
				goto IL_DB;
			}
			if (TriangleNode.FastVectorEquals(vector, vector3) || TriangleNode.triarea2(vector, vector2, right) > 0f)
			{
				vector3 = right;
				num2 = i;
				goto IL_DB;
			}
			list.Add(vector2);
			vector = vector2;
			int num3 = num;
			vector2 = vector;
			vector3 = vector;
			num = num3;
			num2 = num3;
			i = num3;
			IL_13F:
			i++;
			continue;
			IL_DB:
			if (TriangleNode.triarea2(vector, vector2, left) < 0f)
			{
				goto IL_13F;
			}
			if (TriangleNode.FastVectorEquals(vector, vector2) || TriangleNode.triarea2(vector, vector3, left) < 0f)
			{
				vector2 = left;
				num = i;
				goto IL_13F;
			}
			list.Add(vector3);
			vector = vector3;
			num3 = num2;
			vector2 = vector;
			vector3 = vector;
			num = num3;
			num2 = num3;
			i = num3;
			goto IL_13F;
		}
		list.Add(portals.Last<TriangleNode.Portal>().Left);
		return list;
	}

	public static uint MainPathToggle = 0u;

	public bool StartEnd;

	public Vector2 Center;

	public Vector2[] Points;

	public Vector2[] PortalPoints;

	public int[] PointIndices;

	public TriangleNode[] Connections = new TriangleNode[3];

	public Dictionary<TriangleNode, float> Weight = new Dictionary<TriangleNode, float>(3);

	public PathNode<TriangleNode> PathNode;

	public Rect rect;

	public float Area;

	public Vector2 preferredPoint = Vector2.zero;

	public uint PathToggle;

	private int HashCode;

	private static readonly Vector2[] PortalCheck = new Vector2[3];

	public struct Portal
	{
		public Portal(Vector2 l, Vector2 r)
		{
			this.Left = l;
			this.Right = r;
		}

		public Portal(Vector2 b)
		{
			this.Left = b;
			this.Right = b;
		}

		public readonly Vector2 Left;

		public readonly Vector2 Right;
	}
}
