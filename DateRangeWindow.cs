﻿using System;
using UnityEngine;

public class DateRangeWindow : MonoBehaviour
{
	public void Show(SDateTime from, SDateTime to, Action<SDateTime, SDateTime> action)
	{
		this.Window.Show();
		this.FromDate.CurrentDate = from;
		this.ToDate.CurrentDate = to;
		this.WhenDone = action;
	}

	public void ClickOK()
	{
		this.WhenDone(this.FromDate.CurrentDate, this.ToDate.CurrentDate);
		this.Window.Close();
	}

	public DatePicker FromDate;

	public DatePicker ToDate;

	public GUIWindow Window;

	public Action<SDateTime, SDateTime> WhenDone;
}
