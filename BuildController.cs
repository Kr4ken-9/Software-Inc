﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildController : MonoBehaviour
{
	private void Start()
	{
		this.OldMatrix = this.GridMatrix;
		if (BuildController.Instance != null)
		{
			UnityEngine.Object.Destroy(BuildController.Instance.gameObject);
		}
		BuildController.Instance = this;
	}

	public void AngleToggle(Image AngleImg)
	{
		this.AnlgeNum = (this.AnlgeNum + 1) % this.Angles.Length;
		this.FurnitureAngle = this.Angles[this.AnlgeNum];
		AngleImg.sprite = this.AngleSprites[this.AnlgeNum];
		if (this.CurrentFurnitureBuilder != null)
		{
			this.CurrentFurnitureBuilder.GetComponent<FurnitureBuilder>().RotationSnap = this.FurnitureAngle;
		}
	}

	private void OnDestroy()
	{
		if (BuildController.Instance == this)
		{
			BuildController.Instance = null;
		}
	}

	public void SetTempGrid(Vector2 p1, Vector2 p2, bool invert)
	{
		if (!this.InTempGrid)
		{
			this.OldMatrix = this.GridMatrix;
			this.InTempGrid = true;
		}
		float d = 1f / this.GridMatrix.MultiplyVector(Vector3.right).magnitude;
		Vector3 vector = (p2 - p1).Turn90().ToVector3(0f);
		if (invert)
		{
			vector = -vector;
		}
		this.GridMatrix = Matrix4x4.TRS(p1.ToVector3(0f), Quaternion.LookRotation(vector), Vector3.one * d).inverse;
		this.UpdateGridVisual();
	}

	public void ResetTempGrid()
	{
		if (this.InTempGrid)
		{
			this.InTempGrid = false;
			this.GridMatrix = this.OldMatrix;
			this.UpdateGridVisual();
		}
	}

	public bool IsBuildingRoom()
	{
		return this.CurrentSegments != null || this.RectPoints != null;
	}

	public Room MakeRoom(IEnumerable<WallEdge> s, bool outdoors, List<UndoObject.UndoAction> undos, bool dust = true, bool optimize = true)
	{
		return this.MakeRoom(s, GameSettings.Instance.ActiveFloor, undos, false, dust, optimize, outdoors);
	}

	public Room MakeRoom(IEnumerable<WallEdge> s, int floor, List<UndoObject.UndoAction> undos, bool clone, bool dust = true, bool optimize = true, bool outdoors = false)
	{
		Room component = UnityEngine.Object.Instantiate<GameObject>(this.RoomPrefab).GetComponent<Room>();
		component.Outdoors = outdoors;
		component.Init(s, floor, (!dust) ? null : this.DirtEmitter, false, optimize);
		component.RefreshEdges(undos, clone);
		return component;
	}

	public void StartMerge()
	{
		this.ClearBuild(false, false, false, false);
		this.mergeNow = true;
	}

	public void StartAlign()
	{
		this.OldMatrix = this.GridMatrix;
		this.ClearBuild(false, false, false, false);
		this.alignNow = true;
		HUD.Instance.UpdateBorderOverlay();
	}

	public void RotateGrid(float deg)
	{
		this.OldMatrix = (this.GridMatrix *= Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, deg, 0f), Vector3.one));
		this.UpdateGridVisual();
	}

	public void SizeGrid(float size)
	{
		float magnitude = (Matrix4x4.Scale(new Vector3(size, 1f, size)) * this.GridMatrix).MultiplyVector(Vector3.right).magnitude;
		if ((magnitude < 1f && !magnitude.Appx(1f, 0.0001f)) || (magnitude > 9f && !magnitude.Appx(9f, 0.0001f)))
		{
			return;
		}
		this.OldMatrix = (this.GridMatrix = Matrix4x4.Scale(new Vector3(size, 1f, size)) * this.GridMatrix);
		this.UpdateGridVisual();
	}

	public void TranslateGridX(float x)
	{
		this.OldMatrix = (this.GridMatrix = Matrix4x4.TRS(new Vector3(x, 0f, 0f), Quaternion.identity, Vector3.one) * this.GridMatrix);
		this.UpdateGridVisual();
	}

	public void TranslateGridY(float y)
	{
		this.OldMatrix = (this.GridMatrix = Matrix4x4.TRS(new Vector3(0f, 0f, y), Quaternion.identity, Vector3.one) * this.GridMatrix);
		this.UpdateGridVisual();
	}

	public void ResetGrid()
	{
		this.OldMatrix = (this.GridMatrix = Matrix4x4.identity);
		this.InTempGrid = false;
		this.UpdateGridVisual();
	}

	public void UpdateGridVisual()
	{
		this.MainGridMaterial.SetFloat("_UV00", this.GridMatrix.m00);
		this.MainGridMaterial.SetFloat("_UV10", this.GridMatrix.m02);
		this.MainGridMaterial.SetFloat("_UV20", this.GridMatrix.m03);
		this.MainGridMaterial.SetFloat("_UV01", this.GridMatrix.m20);
		this.MainGridMaterial.SetFloat("_UV11", this.GridMatrix.m22);
		this.MainGridMaterial.SetFloat("_UV21", this.GridMatrix.m23);
		this.MainGridMaterial.SetFloat("_UV02", this.GridMatrix.m30);
		this.MainGridMaterial.SetFloat("_UV12", this.GridMatrix.m32);
		this.MainGridMaterial.SetFloat("_UV22", this.GridMatrix.m33);
		this.SnapDistance = 1f / this.GridMatrix.MultiplyVector(Vector3.right).magnitude;
		this.SnapDistance /= 2f;
		this.SnapDistance = Mathf.Max(0.5f, this.SnapDistance);
	}

	private void UpdateTempWall(Vector2 mousePos)
	{
		if (this.CurrentTempWall == null)
		{
			return;
		}
		Vector2 vector = mousePos;
		if (this.CurrentSegments.Count == 0)
		{
			this.CurrentTempWall.transform.position = new Vector3(vector.x, (float)(GameSettings.Instance.ActiveFloor * 2) + ((!this.FenceMode) ? 1f : 0.5f), vector.y);
			this.CurrentTempWall.transform.localScale = new Vector3(Room.WallOffset + 0.01f, (!this.FenceMode) ? 2.01f : 1.01f, Room.WallOffset + 0.01f);
			this.CurrentTempWall.transform.rotation = Quaternion.identity;
			return;
		}
		Vector2 pos = this.CurrentSegments.Last<WallEdge>().Pos;
		vector = (pos + mousePos) * 0.5f;
		Vector2 lhs = mousePos - pos;
		if (lhs != Vector2.zero)
		{
			float magnitude = lhs.magnitude;
			this.CurrentTempWall.transform.position = new Vector3(vector.x, (float)(GameSettings.Instance.ActiveFloor * 2) + ((!this.FenceMode) ? 1f : 0.5f), vector.y);
			this.CurrentTempWall.transform.localScale = new Vector3(Room.WallOffset + 0.01f, (!this.FenceMode) ? 2.01f : 1.01f, magnitude);
			this.CurrentTempWall.transform.rotation = Quaternion.LookRotation(new Vector3(lhs.x, 0f, lhs.y));
		}
	}

	public void AddSegment(WallEdge s)
	{
		if (this.CurrentTempWall != null)
		{
			this.UpdateTempWall(s.Pos);
		}
		this.CurrentSegments.Add(s);
		if (this.CurrentSegments.Count > 1)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.TempWallPrefab);
			this.TempWallList.Add(gameObject);
			this.CurrentTempWall = gameObject;
		}
	}

	private bool TooClose(Vector2 pos, WallEdge ignore = null)
	{
		if (this.CurrentSegments != null)
		{
			if (this.CurrentSegments.Any((WallEdge x) => x != ignore && x.Pos.Dist(pos).VeryStrictlyBelow(this.MinWallDistance)))
			{
				ErrorOverlay.Instance.ShowError("RoomNarrowError", false, true, 4f, true);
				return true;
			}
			if (this.isCutting != null)
			{
				if (this.isCutting.Edges.Any(delegate(WallEdge x)
				{
					float num2 = x.Pos.Dist(pos);
					return num2 >= this.SnapDistance && num2.VeryStrictlyBelow(this.MinWallDistance);
				}))
				{
					ErrorOverlay.Instance.ShowError("RoomNarrowError", false, true, 4f, true);
					return true;
				}
				for (int i = 0; i < this.isCutting.Edges.Count; i++)
				{
					Vector2 pos2 = this.isCutting.Edges[i].Pos;
					Vector2 pos3 = this.isCutting.Edges[(i + 1) % this.isCutting.Edges.Count].Pos;
					Vector2? vector = Utilities.ProjectToLine(pos, pos2, pos3);
					if (vector != null)
					{
						float num = vector.Value.Dist(pos);
						if (num >= this.SnapDistance && num.VeryStrictlyBelow(this.MinWallDistance))
						{
							ErrorOverlay.Instance.ShowError("RoomNarrowError", false, true, 4f, true);
							return true;
						}
					}
				}
			}
			for (int j = 0; j < this.CurrentSegments.Count - 1; j++)
			{
				if (this.CurrentSegments[j] != ignore && this.CurrentSegments[j + 1] != ignore)
				{
					Vector2 pos4 = this.CurrentSegments[j].Pos;
					Vector2 pos5 = this.CurrentSegments[j + 1].Pos;
					Vector2? vector2 = Utilities.ProjectToLine(pos, pos4, pos5);
					if (vector2 != null && vector2.Value.Dist(pos).VeryStrictlyBelow(this.MinWallDistance))
					{
						ErrorOverlay.Instance.ShowError("RoomNarrowError", false, true, 4f, true);
						return true;
					}
				}
			}
		}
		return false;
	}

	public void ActivateBuildMode(bool fence)
	{
		HintController.Instance.Show(HintController.Hints.HintMultipleBuild);
		this.ClearBuild(false, false, false, false);
		this.FenceMode = fence;
		if (this.FenceMode && GameSettings.Instance.ActiveFloor < 0)
		{
			GameSettings.Instance.ActiveFloor = 0;
			Furniture.UpdateEdgeDetection();
			GameSettings.Instance.sRoomManager.ChangeFloor();
		}
		this.CurrentSegments = new List<WallEdge>();
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.TempWallPrefab);
		this.TempWallList.Add(gameObject);
		this.CurrentTempWall = gameObject;
	}

	public Vector2 GetMousePos(Plane plane)
	{
		Ray ray = this.MainCamera.ScreenPointToRay(Input.mousePosition);
		float distance = 0f;
		plane.Raycast(ray, out distance);
		Vector3 point = ray.GetPoint(distance);
		return new Vector2(point.x, point.z);
	}

	public Vector2 CorrectMousePos(Vector2 mouse, Vector2 Offset)
	{
		Vector3 v = this.GridMatrix.MultiplyPoint(new Vector3(mouse.x, 0f, mouse.y));
		v = new Vector3(Mathf.Round(v.x - Offset.x) + Offset.x, Mathf.Round(v.y), Mathf.Round(v.z - Offset.y) + Offset.y);
		v = this.GridMatrix.inverse.MultiplyPoint(v);
		return new Vector2(v.x, v.z);
	}

	public Vector2 CorrectMousePos(Vector2 mouse, bool offset = false)
	{
		Vector3 v = this.GridMatrix.MultiplyPoint(new Vector3(mouse.x, 0f, mouse.y));
		if (offset)
		{
			v = new Vector3(Mathf.Round(v.x - 0.5f) + 0.5f, Mathf.Round(v.y), Mathf.Round(v.z - 0.5f) + 0.5f);
		}
		else
		{
			v = new Vector3(Mathf.Round(v.x), Mathf.Round(v.y), Mathf.Round(v.z));
		}
		v = this.GridMatrix.inverse.MultiplyPoint(v);
		return new Vector2(v.x, v.z);
	}

	private void Update()
	{
		if (this.IsActive())
		{
			SelectorController.CanClick = false;
		}
		if (InputController.GetKeyUp(InputController.Keys.AnchorGrid, false))
		{
			this.StartAlign();
		}
		if (InputController.GetKeyUp(InputController.Keys.ResetGrid, false))
		{
			this.ResetGrid();
		}
		Plane plane = new Plane(Vector3.up, Vector3.up * (float)GameSettings.Instance.ActiveFloor * 2f);
		Vector2 mousePos = this.GetMousePos(plane);
		Vector2 vector = this.CorrectMousePos(mousePos, false);
		Vector2 vector2 = vector;
		if (this.CurrentSegments != null)
		{
			vector2 = this.SnapToWall(vector2);
			this.UpdateTempWall(vector2);
			if (Input.GetMouseButtonUp(1))
			{
				if (this.CurrentSegments.Count > 0)
				{
					UISoundFX.PlaySFX("PlaceWallRev", -1f, 0f);
					if (this.CurrentSegments.Count > 1)
					{
						this.TempWallList.Remove(this.CurrentTempWall);
						UnityEngine.Object.Destroy(this.CurrentTempWall);
					}
					else
					{
						this.isCutting = null;
					}
					this.CurrentSegments.RemoveAt(this.CurrentSegments.Count - 1);
					this.CurrentTempWall = ((!this.TempWallList.Any<GameObject>()) ? null : this.TempWallList.Last<GameObject>());
				}
				else
				{
					this.ClearBuild(false, false, false, false);
				}
			}
		}
		if (!EventSystem.current.IsPointerOverGameObject())
		{
			this.RoomBuildingCode(vector2);
			this.SegmentBuildCode();
			this.AlignCode(mousePos);
		}
		this.UpdateRectBuilding(vector);
	}

	public void BeginRectBuilding(bool fence)
	{
		HintController.Instance.Show(HintController.Hints.HintMultipleBuild);
		this.ClearBuild(false, false, false, false);
		this.FenceMode = fence;
		if (this.FenceMode && GameSettings.Instance.ActiveFloor < 0)
		{
			GameSettings.Instance.ActiveFloor = 0;
			Furniture.UpdateEdgeDetection();
			GameSettings.Instance.sRoomManager.ChangeFloor();
		}
		for (int i = 0; i < 4; i++)
		{
			GameObject item = UnityEngine.Object.Instantiate<GameObject>(this.TempWallPrefab);
			this.TempWallList.Add(item);
		}
		this.RectPoints = new Vector2[]
		{
			Vector2.zero,
			Vector2.zero,
			Vector2.zero,
			Vector2.zero
		};
	}

	private void UpdateRectBuilding(Vector2 p)
	{
		if (this.RectPoints != null)
		{
			if (this.LastPos != p)
			{
				if (this.RectDragging)
				{
					float pitch = 1f + Mathf.Clamp(((this.RectPoints[0] - this.RectPoints[2]).magnitude - 4f) / 12f, 0f, 2f);
					UISoundFX.PlaySFX("Tick", pitch, 0f);
				}
				else
				{
					UISoundFX.PlaySFX("Tick", -1f, 0f);
				}
			}
			this.LastPos = p;
			if (this.RectDragging)
			{
				if (Input.GetMouseButtonUp(1))
				{
					this.RectDragging = false;
					BuildingHUD.Instance.Enable(false, false, false);
					CostDisplay.Instance.Hide();
					return;
				}
				if (p == this.RectPoints[0])
				{
					BuildingHUD.Instance.Enable(false, false, false);
					CostDisplay.Instance.Hide();
					return;
				}
				this.RectPoints[2] = p;
				Matrix4x4 inverse = this.GridMatrix.inverse;
				Vector3 vector = this.GridMatrix.MultiplyPoint(new Vector3(this.RectPoints[0].x, 0f, this.RectPoints[0].y));
				Vector3 vector2 = this.GridMatrix.MultiplyPoint(new Vector3(this.RectPoints[2].x, 0f, this.RectPoints[2].y));
				Vector3 vector3 = inverse.MultiplyPoint(new Vector3(vector2.x, 0f, vector.z));
				Vector3 vector4 = inverse.MultiplyPoint(new Vector3(vector.x, 0f, vector2.z));
				this.RectPoints[1] = new Vector2(vector3.x, vector3.z);
				this.RectPoints[3] = new Vector2(vector4.x, vector4.z);
				float num = Mathf.Abs(this.RectPoints[0].x - this.RectPoints[2].x);
				float num2 = Mathf.Abs(this.RectPoints[0].y - this.RectPoints[2].y);
				float roomCost = BuildController.GetRoomCost(num * 2f + num2 * 2f, num * num2, this.FenceMode, GameSettings.Instance.ActiveFloor, false, false);
				if (roomCost < 4f)
				{
					BuildingHUD.Instance.Enable(false, false, false);
					CostDisplay.Instance.Hide();
					return;
				}
				CostDisplay.Instance.Show(roomCost, ((this.RectPoints[0] + this.RectPoints[2]) * 0.5f).ToVector3((float)(GameSettings.Instance.ActiveFloor * 2 + 2)), (!GameSettings.Instance.MyCompany.CanMakeTransaction(-roomCost)) ? Color.red : Color.white);
				for (int i = 0; i < 4; i++)
				{
					int num3 = (i + 1) % 4;
					Vector2 vector5 = (this.RectPoints[i] + this.RectPoints[num3]) * 0.5f;
					this.TempWallList[i].transform.position = new Vector3(vector5.x, (float)(GameSettings.Instance.ActiveFloor * 2) + ((!this.FenceMode) ? 1f : 0.5f), vector5.y);
					Vector2 vector6 = this.RectPoints[i] - this.RectPoints[num3];
					this.TempWallList[i].transform.localScale = new Vector3(Room.WallOffset + 0.1f, (!this.FenceMode) ? 2.01f : 1.01f, Room.WallOffset + 0.1f + vector6.magnitude - 0.01f);
					this.TempWallList[i].transform.rotation = Quaternion.LookRotation(new Vector3(vector6.x, 0f, vector6.y));
				}
				BuildingHUD.Instance.Enable(true, true, false);
				BuildingHUD.Instance.SetDimension(new Vector3(this.RectPoints[0].x, (float)(GameSettings.Instance.ActiveFloor * 2), this.RectPoints[0].y), new Vector3(this.RectPoints[1].x, (float)(GameSettings.Instance.ActiveFloor * 2), this.RectPoints[1].y), false);
				BuildingHUD.Instance.SetDimension(new Vector3(this.RectPoints[0].x, (float)(GameSettings.Instance.ActiveFloor * 2), this.RectPoints[0].y), new Vector3(this.RectPoints[3].x, (float)(GameSettings.Instance.ActiveFloor * 2), this.RectPoints[3].y), true);
				if (Input.GetMouseButtonUp(0))
				{
					if (GameSettings.Instance.MyCompany.CanMakeTransaction(-roomCost))
					{
						this.CurrentSegments = new List<WallEdge>();
						List<WallEdge> list = GameSettings.Instance.sRoomManager.GetEdgesOnFloor(GameSettings.Instance.ActiveFloor).ToList<WallEdge>();
						for (int j = 3; j >= 0; j--)
						{
							Vector2 ps = this.RectPoints[j];
							WallEdge wallEdge = list.FirstOrDefault((WallEdge x) => (x.Pos - ps).magnitude < this.SnapDistance);
							if (wallEdge == null)
							{
								wallEdge = new WallEdge(ps, GameSettings.Instance.ActiveFloor);
								for (int k = 0; k < list.Count; k++)
								{
									WallEdge wallEdge2 = list[k];
									foreach (KeyValuePair<Room, WallEdge> keyValuePair in wallEdge2.Links)
									{
										Vector2? vector7 = Utilities.ProjectToLine(ps, wallEdge2.Pos, keyValuePair.Value.Pos);
										if (vector7 != null && (ps - vector7.Value).magnitude < this.SnapDistance)
										{
											wallEdge.Pos = vector7.Value;
											wallEdge.SetSplit(wallEdge2, keyValuePair.Key);
											break;
										}
									}
									if (wallEdge.IsSplitter)
									{
										break;
									}
								}
							}
							this.CurrentSegments.Add(wallEdge);
						}
						Vector2 a = this.RectPoints[0];
						Vector2 b = this.RectPoints[2];
						this.ProjectToWalls(GameSettings.Instance.ActiveFloor);
						int num4 = 0;
						if (this.CurrentSegments.Count == 4)
						{
							for (int l = 0; l < this.CurrentSegments.Count; l++)
							{
								WallEdge wallEdge3 = this.CurrentSegments[l];
								WallEdge wallEdge4 = this.CurrentSegments[(l + 1) % this.CurrentSegments.Count];
								if ((wallEdge3.Links.Count > 0 || wallEdge3.IsSplitter) && (wallEdge4.Links.Count > 0 || wallEdge4.IsSplitter) && (wallEdge3.UpAgainst(wallEdge4) || wallEdge4.UpAgainst(wallEdge3)))
								{
									num4++;
								}
								if (wallEdge3.Links.Count == 0 && !wallEdge3.IsSplitter)
								{
									Room roomFromPoint = GameSettings.Instance.sRoomManager.GetRoomFromPoint(GameSettings.Instance.ActiveFloor, wallEdge3.Pos, true);
									if (roomFromPoint == GameSettings.Instance.sRoomManager.Outside || roomFromPoint == null)
									{
										num4 = 0;
										break;
									}
								}
							}
						}
						bool flag = true;
						Room split = this.CurrentSegments[0].GetSplitRooms[0];
						if (split != null && this.CurrentSegments.All((WallEdge x) => x.IsSplitter && x.GetSplitRooms[0] == split))
						{
							ErrorOverlay.Instance.ShowError("RoomInRoomError", false, true, 4f, true);
							flag = false;
						}
						if (flag)
						{
							for (int m = this.CurrentSegments.Count - 1; m >= 0; m--)
							{
								Vector2 pos = this.CurrentSegments[m].Pos;
								Vector2 pos2 = this.CurrentSegments[(m + 1) % this.CurrentSegments.Count].Pos;
								foreach (WallEdge wallEdge5 in list)
								{
									foreach (KeyValuePair<Room, WallEdge> keyValuePair2 in wallEdge5.Links)
									{
										if (Utilities.LinesIntersect(pos, pos2, wallEdge5.Pos, keyValuePair2.Value.Pos, true, false))
										{
											ErrorOverlay.Instance.ShowError("RoomIntersectError", false, true, 4f, true);
											flag = false;
											break;
										}
									}
									if (!flag)
									{
										break;
									}
								}
								if (!flag)
								{
									break;
								}
							}
						}
						if (flag)
						{
							Vector2 point = (a + b) * 0.5f;
							Room roomFromPoint2 = GameSettings.Instance.sRoomManager.GetRoomFromPoint(GameSettings.Instance.ActiveFloor, point, true);
							bool flag2 = roomFromPoint2 != null && roomFromPoint2 != GameSettings.Instance.sRoomManager.Outside && num4 > 0 && num4 < 3;
							if (flag2)
							{
								for (int n = 0; n < this.CurrentSegments.Count; n++)
								{
									WallEdge wallEdge6 = this.CurrentSegments[n];
									WallEdge wallEdge7 = this.CurrentSegments[(n + 1) % this.CurrentSegments.Count];
									WallEdge wallEdge8 = this.CurrentSegments[(n + 2) % this.CurrentSegments.Count];
									if ((wallEdge6.Links.Count > 0 || wallEdge6.IsSplitter) && (wallEdge7.Links.Count > 0 || wallEdge7.IsSplitter) && (wallEdge8.Links.Count > 0 || wallEdge8.IsSplitter))
									{
										this.CurrentSegments.Remove(wallEdge7);
										n--;
									}
								}
								if (this.CurrentSegments.Count > 0)
								{
									int num5 = 0;
									while (num5 < 4 && !this.IsValidSplit())
									{
										WallEdge item = this.CurrentSegments[0];
										this.CurrentSegments.RemoveAt(0);
										this.CurrentSegments.Add(item);
										num5++;
									}
								}
								if (this.IsValidSplit())
								{
									this.isCutting = roomFromPoint2;
									if (this.ValidSplit())
									{
										float roomCost2 = BuildController.GetRoomCost((from x in this.CurrentSegments
										select x.Pos).ToList<Vector2>(), roomFromPoint2.Outdoors, roomFromPoint2.Floor, true, false);
										if (GameSettings.Instance.MyCompany.CanMakeTransaction(-roomCost2) && this.isCutting != null && !this.CurrentSegments.Any((WallEdge x) => !x.IsSplitter && x.Links.Count == 0 && this.TooClose(x.Pos, x)))
										{
											string debug = string.Join("\n", (from x in this.CurrentSegments
											select x.Pos.x + ";" + x.Pos.y).ToArray<string>()) + "\n\n" + string.Join("\n", (from x in this.isCutting.Edges
											select x.Pos.x + ";" + x.Pos.y).ToArray<string>());
											List<UndoObject.UndoAction> list2 = new List<UndoObject.UndoAction>();
											this.FinalizeCuts(true, GameSettings.Instance.ActiveFloor, list2);
											Dictionary<WallSnap, UndoObject.UndoAction> snaps = this.isCutting.PrepareSplit(true);
											GameSettings.Instance.sRoomManager.AllSegments.AddRange(this.CurrentSegments);
											Room room = this.isCutting.Split(this.CurrentSegments, list2, snaps, debug, true);
											if (room != null)
											{
												CostDisplay.Instance.FloatAway(roomCost2);
												GameSettings.Instance.MyCompany.MakeTransaction(-roomCost2, Company.TransactionCategory.Construction, "Room");
												UISoundFX.PlaySFX("PlaceRoom", -1f, 0f);
												UISoundFX.PlaySFX("Kaching", -1f, 0f);
												GameSettings.Instance.sRoomManager.AddRoom(room);
												List<UndoObject.UndoAction> list3 = new List<UndoObject.UndoAction>
												{
													new UndoObject.UndoAction(this.isCutting, room, roomCost2)
												};
												foreach (Furniture furniture in this.isCutting.GetFurnitures().ToList<Furniture>())
												{
													if (!furniture.UpdateParent(true, false))
													{
														list3.Add(new UndoObject.UndoAction(furniture, false));
														list3.AddRange(from x in furniture.IterateSnap(null)
														select new UndoObject.UndoAction(x, false));
													}
												}
												list3.AddRange(list2);
												GameSettings.Instance.AddUndo(list3.ToArray());
												room.RecalculateTableGroups();
												this.isCutting.RecalculateTableGroups();
												this.isCutting = null;
												if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
												{
													this.BeginRectBuilding(this.FenceMode);
												}
												else
												{
													this.ClearBuild(false, false, false, false);
												}
											}
											else
											{
												this.isCutting.OptimizeSegments();
												foreach (WallEdge item2 in this.CurrentSegments)
												{
													if (!this.isCutting.Edges.Contains(item2))
													{
														GameSettings.Instance.sRoomManager.AllSegments.Remove(item2);
													}
												}
												this.isCutting = null;
												if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
												{
													this.ActivateBuildMode(this.FenceMode);
												}
												else
												{
													this.ClearBuild(false, false, false, false);
												}
											}
										}
										else
										{
											UISoundFX.PlaySFX("BuildError", -1f, 0f);
											this.CurrentSegments = null;
										}
									}
									else
									{
										UISoundFX.PlaySFX("BuildError", -1f, 0f);
										this.CurrentSegments = null;
									}
								}
								else
								{
									UISoundFX.PlaySFX("BuildError", -1f, 0f);
									this.CurrentSegments = null;
								}
							}
							else
							{
								List<Room> rs = (from x in GameSettings.Instance.sRoomManager.GetRooms()
								where x.Floor == GameSettings.Instance.ActiveFloor
								select x).ToList<Room>();
								if (flag)
								{
									if ((from x in this.CurrentSegments
									where x.Links.Count == 0 && !x.IsSplitter
									select x).Any((WallEdge x) => rs.Any((Room z) => z.IsInside(x.Pos))))
									{
										ErrorOverlay.Instance.ShowError("RoomInRoomError", false, true, 4f, true);
										flag = false;
									}
								}
								if (flag)
								{
									if (!GameSettings.Instance.sRoomManager.IsSupported(from x in this.CurrentSegments
									select x.Pos, GameSettings.Instance.ActiveFloor, null, true, null))
									{
										ErrorOverlay.Instance.ShowError("UnsupportedStructure", false, true, 4f, true);
										flag = false;
									}
								}
								if (flag && (BuildController.IsOnRoad(this.RectPoints[0], this.RectPoints[1]) || BuildController.IsOnRoad(this.RectPoints[1], this.RectPoints[2]) || BuildController.IsOnRoad(this.RectPoints[2], this.RectPoints[3]) || BuildController.IsOnRoad(this.RectPoints[3], this.RectPoints[0])))
								{
									ErrorOverlay.Instance.ShowError("RoomOnRoad", false, true, 4f, true);
									flag = false;
								}
								if (flag && !GameSettings.Instance.PlayerOwnedArea(this.RectPoints))
								{
									ErrorOverlay.Instance.ShowError("RoomOutOfPlot", false, true, 4f, true);
									flag = false;
								}
								if (flag && !this.ContainsPoints() && !this.ClampingRoom() && !this.CurrentSegments.Any((WallEdge x) => this.TooClose(x.Pos, x)))
								{
									CostDisplay.Instance.FloatAway(roomCost);
									GameSettings.Instance.MyCompany.MakeTransaction(-roomCost, Company.TransactionCategory.Construction, "Room");
									UISoundFX.PlaySFX("PlaceRoom", -1f, 0f);
									UISoundFX.PlaySFX("Kaching", -1f, 0f);
									List<UndoObject.UndoAction> list4 = new List<UndoObject.UndoAction>();
									this.FinalizeCuts(false, GameSettings.Instance.ActiveFloor, list4);
									GameSettings.Instance.sRoomManager.AllSegments.AddRange(this.CurrentSegments);
									Room room2 = this.MakeRoom(this.CurrentSegments, this.FenceMode, list4, true, true);
									list4.Insert(0, new UndoObject.UndoAction(room2, true, roomCost));
									GameSettings.Instance.AddUndo(list4.ToArray());
									GameSettings.Instance.sRoomManager.AddRoom(room2);
									if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
									{
										this.BeginRectBuilding(this.FenceMode);
									}
									else
									{
										this.ClearBuild(false, false, false, false);
									}
								}
								else
								{
									UISoundFX.PlaySFX("BuildError", -1f, 0f);
									this.CurrentSegments = null;
								}
							}
						}
						else
						{
							UISoundFX.PlaySFX("BuildError", -1f, 0f);
							this.CurrentSegments = null;
						}
					}
					else
					{
						UISoundFX.PlaySFX("BuildError", -1f, 0f);
					}
				}
			}
			else if (!EventSystem.current.IsPointerOverGameObject())
			{
				if (Input.GetMouseButtonUp(1))
				{
					this.ClearBuild(false, false, false, false);
					return;
				}
				for (int num6 = 0; num6 < this.TempWallList.Count; num6++)
				{
					GameObject gameObject = this.TempWallList[num6];
					gameObject.transform.position = new Vector3(p.x, (float)(GameSettings.Instance.ActiveFloor * 2) + ((!this.FenceMode) ? 1f : 0.5f), p.y);
					gameObject.transform.localScale = new Vector3(Room.WallOffset + 0.01f, (!this.FenceMode) ? 2.01f : 1.01f, Room.WallOffset + 0.01f);
				}
				for (int num7 = 0; num7 < this.RectPoints.Length; num7++)
				{
					this.RectPoints[num7] = p;
				}
				if (Input.GetMouseButtonDown(0))
				{
					this.RectDragging = true;
				}
			}
		}
	}

	public bool IsValidSplit()
	{
		if (this.CurrentSegments.Count < 2)
		{
			return false;
		}
		WallEdge wallEdge = this.CurrentSegments[0];
		WallEdge wallEdge2 = this.CurrentSegments[this.CurrentSegments.Count - 1];
		return (wallEdge.IsSplitter || wallEdge.Links.Count > 0) && (wallEdge2.IsSplitter || wallEdge2.Links.Count > 0);
	}

	public bool IsActive()
	{
		return this.alignNow || this.RectPoints != null || this.CurrentSegments != null || this.CurrentTempSegment != null || this.CurrentFurnitureBuilder != null || this.CurrentTempWall != null || RoadBuildCube.Instance.gameObject.activeSelf || RoomCloneTool.Instance.gameObject.activeSelf || WallRemovalTool.Instance.gameObject.activeSelf || EnvironmentEditor.Instance.gameObject.activeSelf;
	}

	private bool AnyIntersections(WallEdge edge1, Vector2 b, WallEdge extraCheck = null)
	{
		Vector2 pos = edge1.Pos;
		for (int i = 0; i < this.CurrentSegments.Count - 1; i++)
		{
			Vector2 pos2 = this.CurrentSegments[i].Pos;
			Vector2 pos3 = this.CurrentSegments[i + 1].Pos;
			if (Utilities.LinesIntersect(pos, b, pos2, pos3, false, i < this.CurrentSegments.Count - 2))
			{
				ErrorOverlay.Instance.ShowError("RoomIntersectError", false, true, 4f, true);
				return true;
			}
		}
		foreach (WallEdge wallEdge in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(GameSettings.Instance.ActiveFloor))
		{
			Vector2 pos4 = wallEdge.Pos;
			foreach (WallEdge wallEdge2 in wallEdge.Links.Values)
			{
				if (!edge1.CanIntersect(wallEdge, wallEdge2))
				{
					if (extraCheck == null || !extraCheck.CanIntersect(wallEdge, wallEdge2))
					{
						Vector2 pos5 = wallEdge2.Pos;
						if (Utilities.LinesIntersect(pos, b, pos4, pos5, true, false, true))
						{
							ErrorOverlay.Instance.ShowError("RoomIntersectError", false, true, 4f, true);
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	private bool AngleTooSteep(WallEdge e, bool last = false)
	{
		if (last)
		{
			Vector2 pos = this.CurrentSegments[this.CurrentSegments.Count - 1].Pos;
			Vector2 pos2 = this.CurrentSegments[0].Pos;
			Vector2 pos3 = this.CurrentSegments[1].Pos;
			if (pos2.AngleBetween(pos, pos3) < this.MinAngle)
			{
				ErrorOverlay.Instance.ShowError("RoomAngleError2".Loc(new object[]
				{
					this.MinAngle
				}), false, true, 4f, false);
				return true;
			}
		}
		else
		{
			if (this.CurrentSegments.Count == 0 || (!this.CurrentSegments.Last<WallEdge>().UpAgainst(e) && !e.UpAgainst(this.CurrentSegments.Last<WallEdge>())))
			{
				if (e.Links.Count > 0 && this.CurrentSegments.Count > 0 && (this.isCutting != null || (this.CurrentSegments.Count == 1 && e.Links.Values.Any((WallEdge x) => this.CurrentSegments[0].Links.ContainsValue(x)))))
				{
					Vector2 pos4 = this.CurrentSegments.Last<WallEdge>().Pos;
					foreach (WallEdge wallEdge in e.Links.Values)
					{
						if (e.Pos.AngleBetween(pos4, wallEdge.Pos) < this.MinAngle)
						{
							ErrorOverlay.Instance.ShowError("RoomAngleError2".Loc(new object[]
							{
								this.MinAngle
							}), false, true, 4f, false);
							return true;
						}
					}
				}
				if (this.CurrentSegments.Count > 0 && this.CurrentSegments.Last<WallEdge>().Links.Count > 0 && (this.isCutting != null || (this.CurrentSegments.Count == 1 && e.Links.Values.Any((WallEdge x) => this.CurrentSegments[0].Links.ContainsValue(x)))))
				{
					Vector2 pos5 = this.CurrentSegments.Last<WallEdge>().Pos;
					foreach (WallEdge wallEdge2 in this.CurrentSegments.Last<WallEdge>().Links.Values)
					{
						if (pos5.AngleBetween(e.Pos, wallEdge2.Pos) < this.MinAngle)
						{
							ErrorOverlay.Instance.ShowError("RoomAngleError2".Loc(new object[]
							{
								this.MinAngle
							}), false, true, 4f, false);
							return true;
						}
					}
					foreach (WallEdge wallEdge3 in this.CurrentSegments.Last<WallEdge>().FindAllConnectionIn())
					{
						if (pos5.AngleBetween(e.Pos, wallEdge3.Pos) < this.MinAngle)
						{
							ErrorOverlay.Instance.ShowError("RoomAngleError2".Loc(new object[]
							{
								this.MinAngle
							}), false, true, 4f, false);
							return true;
						}
					}
				}
				if (this.CurrentSegments.Count > 0 && e.IsSplitter)
				{
					WallEdge[] getSplitEdges = e.GetSplitEdges;
					Vector2 pos6 = this.CurrentSegments.Last<WallEdge>().Pos;
					for (int i = 0; i < getSplitEdges.Length; i++)
					{
						if (e.Pos.AngleBetween(pos6, getSplitEdges[i].Pos) < this.MinAngle)
						{
							ErrorOverlay.Instance.ShowError("RoomAngleError2".Loc(new object[]
							{
								this.MinAngle
							}), false, true, 4f, false);
							return true;
						}
					}
				}
				if (this.CurrentSegments.Count > 0 && this.CurrentSegments.Last<WallEdge>().IsSplitter)
				{
					WallEdge[] getSplitEdges2 = this.CurrentSegments.Last<WallEdge>().GetSplitEdges;
					Vector2 pos7 = this.CurrentSegments.Last<WallEdge>().Pos;
					for (int j = 0; j < getSplitEdges2.Length; j++)
					{
						if (pos7.AngleBetween(e.Pos, getSplitEdges2[j].Pos) < this.MinAngle)
						{
							ErrorOverlay.Instance.ShowError("RoomAngleError2".Loc(new object[]
							{
								this.MinAngle
							}), false, true, 4f, false);
							return true;
						}
					}
				}
				if (this.CurrentSegments.Count <= 0 || e.Links.Count <= 0 || !(this.isCutting != null))
				{
					goto IL_5FF;
				}
				WallEdge wallEdge4 = e.Links[this.isCutting];
				Vector2 pos8 = this.CurrentSegments.Last<WallEdge>().Pos;
				if (e.Pos.AngleBetween(pos8, wallEdge4.Pos) < this.MinAngle)
				{
					ErrorOverlay.Instance.ShowError("RoomAngleError2".Loc(new object[]
					{
						this.MinAngle
					}), false, true, 4f, false);
					return true;
				}
				wallEdge4 = e.FindConnectionIn(this.isCutting);
				if (wallEdge4 != null && e.Pos.AngleBetween(pos8, wallEdge4.Pos) < this.MinAngle)
				{
					ErrorOverlay.Instance.ShowError("RoomAngleError2".Loc(new object[]
					{
						this.MinAngle
					}), false, true, 4f, false);
					return true;
				}
			}
			IL_5FF:
			if (this.CurrentSegments.Count > 1)
			{
				Vector2 pos9 = this.CurrentSegments[this.CurrentSegments.Count - 2].Pos;
				Vector2 pos10 = this.CurrentSegments[this.CurrentSegments.Count - 1].Pos;
				if (pos10.AngleBetween(pos9, e.Pos) < this.MinAngle)
				{
					ErrorOverlay.Instance.ShowError("RoomAngleError2".Loc(new object[]
					{
						this.MinAngle
					}), false, true, 4f, false);
					return true;
				}
			}
		}
		return false;
	}

	private static float FloorFact(int floor)
	{
		return Mathf.Pow(1.1f, (float)Mathf.Max(0, floor));
	}

	public static float GetRoomCost(float wallLength, float area, bool outdoor, int floor, bool justSplit, bool rent)
	{
		if (rent)
		{
			return BuildController.FloorFact(floor) * area * ((!outdoor) ? BuildController.RoomRentPrice : BuildController.OutdoorRentPrice);
		}
		float num = (!justSplit) ? (BuildController.FloorFact(floor) * area * ((!outdoor) ? BuildController.RoomPrice : BuildController.OutdoorPrice)) : 0f;
		float num2 = wallLength * ((!outdoor) ? BuildController.WallPrice : BuildController.FencePrice);
		return num + num2;
	}

	public static float GetRoomCost(IList<Vector2> wallEdges, float area, bool outdoor, int floor, bool justSplit, bool rent)
	{
		float num = 0f;
		if (!rent)
		{
			for (int i = 0; i < wallEdges.Count - 1; i++)
			{
				num += (wallEdges[i] - wallEdges[i + 1]).magnitude;
			}
			if (!justSplit)
			{
				num += (wallEdges[0] - wallEdges[wallEdges.Count - 1]).magnitude;
			}
		}
		return BuildController.GetRoomCost(num, area, outdoor, floor, justSplit, rent);
	}

	public static float GetRoomCost(IList<WallEdge> wallEdges, float area, bool outdoor, int floor, bool justSplit, bool rent)
	{
		float num = 0f;
		if (!rent)
		{
			for (int i = 0; i < wallEdges.Count - 1; i++)
			{
				num += (wallEdges[i].Pos - wallEdges[i + 1].Pos).magnitude;
			}
			if (!justSplit)
			{
				num += (wallEdges[0].Pos - wallEdges[wallEdges.Count - 1].Pos).magnitude;
			}
		}
		return BuildController.GetRoomCost(num, area, outdoor, floor, justSplit, rent);
	}

	public static float GetRoomCost(IList<Vector2> wallEdges, bool outdoor, int floor, bool justSplit, bool rent)
	{
		return BuildController.GetRoomCost(wallEdges, (rent || !justSplit) ? Utilities.PolygonArea(wallEdges) : 0f, outdoor, floor, justSplit, rent);
	}

	public static bool IsOnRoad(Vector2 p1, Vector2 p2, int floor)
	{
		if (floor > -1 && floor < 2)
		{
			Vector2 a = p2 - p1;
			float magnitude = a.magnitude;
			float num = 0f;
			a = a.normalized;
			Vector2 vector = p1;
			int num2 = 1;
			while (!BuildController.IsPointOnRoad(vector))
			{
				if (vector == p2)
				{
					return false;
				}
				num += (float)num2;
				if (num > magnitude)
				{
					vector = p2;
				}
				else
				{
					vector += a * (float)num2;
				}
			}
			return true;
		}
		return false;
	}

	public static bool IsOnRoad(Vector2 p1, Vector2 p2)
	{
		return BuildController.IsOnRoad(p1, p2, GameSettings.Instance.ActiveFloor);
	}

	private static bool IsPointOnRoad(Vector2 p)
	{
		int num = Mathf.FloorToInt(p.x / RoadManager.Instance.RoadSize);
		int num2 = Mathf.FloorToInt(p.y / RoadManager.Instance.RoadSize);
		bool flag = RoadManager.Instance.GetRoad(num, num2) > 0;
		bool flag2 = Mathf.Approximately(p.x % RoadManager.Instance.RoadSize, 0f);
		bool flag3 = Mathf.Approximately(p.y % RoadManager.Instance.RoadSize, 0f);
		if (flag2 && flag3)
		{
			return flag && RoadManager.Instance.GetRoad(num - 1, num2) > 0 && RoadManager.Instance.GetRoad(num, num2 - 1) > 0 && RoadManager.Instance.GetRoad(num - 1, num2 - 1) > 0;
		}
		if (flag2)
		{
			return flag && RoadManager.Instance.GetRoad(num - 1, num2) > 0;
		}
		if (flag3)
		{
			return flag && RoadManager.Instance.GetRoad(num, num2 - 1) > 0;
		}
		return flag;
	}

	public bool ClampingRoom()
	{
		if (this.CurrentSegments.Any((WallEdge x) => x.Links.Count == 0 && !x.IsSplitter))
		{
			return false;
		}
		List<Room> list = this.CurrentSegments.SelectMany((WallEdge x) => x.Links.Keys).ToList<Room>();
		list.AddRange((from x in this.CurrentSegments
		where x.IsSplitter
		select x).SelectMany((WallEdge x) => x.GetSplitRooms));
		list = (from x in list
		where x != null
		select x).Distinct<Room>().ToList<Room>();
		if (list.Count == 0)
		{
			return false;
		}
		List<WallEdge> list2 = this.CurrentSegments.ToList<WallEdge>();
		if (Utilities.Clockwise((from x in list2
		select x.Pos).ToList<Vector2>()))
		{
			list2.Reverse();
		}
		foreach (Room key in list)
		{
			for (int i = 0; i < list2.Count; i++)
			{
				int index = (i + 1) % list2.Count;
				WallEdge e1 = list2[i];
				WallEdge e2 = list2[index];
				if (e1.Links.Count > 0)
				{
					if (e2.Links.Count > 0)
					{
						if (e1.Links.ContainsKey(key) && e1.Links[key] == e2)
						{
							return true;
						}
					}
					else
					{
						WallEdge wallEdge = e2.GetSplitEdges.First((WallEdge x) => x != e1);
						if (e1.Links.ContainsKey(key) && wallEdge.Links.ContainsKey(key) && wallEdge.Links[key] != e1)
						{
							return true;
						}
					}
				}
				else if (e2.Links.Count > 0)
				{
					WallEdge wallEdge2 = e1.GetSplitEdges.First((WallEdge x) => x != e2);
					if (wallEdge2.Links.ContainsKey(key) && wallEdge2.Links[key] == e2)
					{
						return true;
					}
				}
				else
				{
					WallEdge[] getSplitEdges = e1.GetSplitEdges;
					WallEdge[] getSplitEdges2 = e2.GetSplitEdges;
					WallEdge wallEdge3 = getSplitEdges[0];
					WallEdge wallEdge4 = getSplitEdges[1];
					if (getSplitEdges.Contains(getSplitEdges2[0]) && getSplitEdges.Contains(getSplitEdges2[1]))
					{
						if (!wallEdge3.Links.ContainsKey(key) || wallEdge3.Links[key] != wallEdge4)
						{
							WallEdge wallEdge5 = wallEdge3;
							wallEdge3 = wallEdge4;
							wallEdge4 = wallEdge5;
						}
						if (wallEdge3.Links.ContainsKey(key) && wallEdge3.Links[key] == wallEdge4)
						{
							float sqrMagnitude = (wallEdge3.Pos - e1.Pos).sqrMagnitude;
							float sqrMagnitude2 = (wallEdge3.Pos - e2.Pos).sqrMagnitude;
							if (sqrMagnitude < sqrMagnitude2)
							{
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	public Vector2 SnapToWall(Vector2 p)
	{
		foreach (WallEdge wallEdge in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(GameSettings.Instance.ActiveFloor))
		{
			if ((p - wallEdge.Pos).magnitude < this.SnapDistance)
			{
				return wallEdge.Pos;
			}
		}
		foreach (WallEdge wallEdge2 in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(GameSettings.Instance.ActiveFloor))
		{
			foreach (WallEdge wallEdge3 in wallEdge2.Links.Values)
			{
				Vector2? vector = Utilities.ProjectToLine(p, wallEdge2.Pos, wallEdge3.Pos);
				if (vector != null && (p - vector.Value).magnitude < this.SnapDistance)
				{
					return vector.Value;
				}
			}
		}
		return p;
	}

	public bool CanChangeFloor()
	{
		return (this.CurrentSegments == null || this.CurrentSegments.Count == 0) && (this.CurrentTempSegment == null || this.DynSegmentE1 == null) && !this.RectDragging && !EnvironmentEditor.Instance.gameObject.activeSelf;
	}

	private void RoomBuildingCode(Vector2 p)
	{
		float num = 0f;
		if (this.CurrentSegments != null)
		{
			if (this.LastPos != p)
			{
				if (this.CurrentSegments.Count > 0)
				{
					float pitch = 1f + Mathf.Clamp(((this.CurrentSegments[this.CurrentSegments.Count - 1].Pos - p).magnitude - 4f) / 12f, 0f, 2f);
					UISoundFX.PlaySFX("Tick", pitch, 0f);
				}
				else
				{
					UISoundFX.PlaySFX("Tick", -1f, 0f);
				}
			}
			this.LastPos = p;
			if (this.CurrentSegments.Count > 0)
			{
				BuildingHUD.Instance.Enable(true, false, this.CurrentSegments.Count > 0);
				Vector2 pos = this.CurrentSegments.Last<WallEdge>().Pos;
				int num2 = GameSettings.Instance.ActiveFloor * 2;
				BuildingHUD.Instance.SetDimension(new Vector3(pos.x, (float)num2, pos.y), new Vector3(p.x, (float)num2, p.y), true);
				if (this.CurrentSegments.Count > 1)
				{
					Vector2 pos2 = this.CurrentSegments[this.CurrentSegments.Count - 2].Pos;
					BuildingHUD.Instance.SetRot(new Vector3(pos2.x, (float)num2, pos2.y), new Vector3(pos.x, (float)num2, pos.y), new Vector3(p.x, (float)num2, p.y));
				}
				else if (this.CurrentSegments.Count > 0)
				{
					Vector3 vector = this.GridMatrix.MultiplyVector(Vector3.forward);
					Vector2 vector2 = pos + new Vector2(vector.x, vector.z);
					BuildingHUD.Instance.SetRot(new Vector3(vector2.x, (float)num2, vector2.y), new Vector3(pos.x, (float)num2, pos.y), new Vector3(p.x, (float)num2, p.y));
				}
			}
			else
			{
				BuildingHUD.Instance.Enable(false, false, false);
			}
			if (this.CurrentSegments.Count == 1 && (this.CurrentSegments[0].IsSplitter || this.CurrentSegments[0].Links.Count > 0))
			{
				num = BuildController.GetRoomCost(new List<Vector2>
				{
					this.CurrentSegments[0].Pos,
					p
				}, this.FenceMode, GameSettings.Instance.ActiveFloor, true, false);
			}
			else if (this.isCutting != null)
			{
				num = BuildController.GetRoomCost((from x in this.CurrentSegments
				select x.Pos).Concate(p).ToList<Vector2>(), this.isCutting.Outdoors, GameSettings.Instance.ActiveFloor, true, false);
			}
			else if (this.CurrentSegments.Count > 2)
			{
				num = BuildController.GetRoomCost((from x in this.CurrentSegments
				select x.Pos).ToList<Vector2>(), this.FenceMode, GameSettings.Instance.ActiveFloor, false, false);
			}
		}
		bool flag = true;
		if (this.CurrentSegments != null)
		{
			if (!GameSettings.Instance.PlayerOwnedPoint(p))
			{
				ErrorOverlay.Instance.ShowError("RoomOutOfPlot", false, false, 0f, true);
				flag = false;
			}
			else if (this.CurrentSegments.Count > 0 && !GameSettings.Instance.PlayerOwnedLine(this.CurrentSegments[this.CurrentSegments.Count - 1].Pos, p))
			{
				ErrorOverlay.Instance.ShowError("RoomOutOfPlot", false, false, 0f, true);
				flag = false;
			}
			else if (BuildController.IsOnRoad(p, (this.CurrentSegments.Count <= 0) ? p : this.CurrentSegments.Last<WallEdge>().Pos))
			{
				ErrorOverlay.Instance.ShowError("RoomOnRoad", false, false, 0f, true);
				flag = false;
			}
			else if (!GameSettings.Instance.sRoomManager.IsSupported(p, GameSettings.Instance.ActiveFloor, null))
			{
				ErrorOverlay.Instance.ShowError("UnsupportedStructure", false, false, 0f, true);
				flag = false;
			}
		}
		if (!flag && this.CurrentSegments != null && Input.GetMouseButtonUp(0))
		{
			UISoundFX.PlaySFX("BuildError", -1f, 0f);
		}
		if (flag && this.CurrentSegments != null && Input.GetMouseButtonUp(0))
		{
			int count = this.CurrentSegments.Count;
			if (this.CurrentSegments.Count > 2 && p.Dist(this.CurrentSegments[0].Pos) < this.SnapDistance)
			{
				if (!this.ContainsPoints() && !this.AngleTooSteep(this.CurrentSegments.First<WallEdge>(), false) && !this.AngleTooSteep(null, true) && this.isCutting == null && !this.ClampingRoom())
				{
					List<Vector2> list = (from x in this.CurrentSegments
					select x.Pos).ToList<Vector2>();
					if (GameSettings.Instance.PlayerOwnedArea(list))
					{
						if (GameSettings.Instance.sRoomManager.IsSupported(list, GameSettings.Instance.ActiveFloor, null, true, null))
						{
							float roomCost = BuildController.GetRoomCost((from x in this.CurrentSegments
							select x.Pos).ToList<Vector2>(), this.FenceMode, GameSettings.Instance.ActiveFloor, false, false);
							if (GameSettings.Instance.MyCompany.CanMakeTransaction(-roomCost))
							{
								CostDisplay.Instance.FloatAway(roomCost);
								GameSettings.Instance.MyCompany.MakeTransaction(-roomCost, Company.TransactionCategory.Construction, "Room");
								UISoundFX.PlaySFX("PlaceRoom", -1f, 0f);
								UISoundFX.PlaySFX("Kaching", -1f, 0f);
								List<UndoObject.UndoAction> list2 = new List<UndoObject.UndoAction>();
								this.FinalizeCuts(false, GameSettings.Instance.ActiveFloor, list2);
								GameSettings.Instance.sRoomManager.AllSegments.AddRange(this.CurrentSegments);
								Room room = this.MakeRoom(this.CurrentSegments, this.FenceMode, list2, true, true);
								list2.Insert(0, new UndoObject.UndoAction(room, true, roomCost));
								GameSettings.Instance.AddUndo(list2.ToArray());
								room.Outdoors = this.FenceMode;
								GameSettings.Instance.sRoomManager.AddRoom(room);
								if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
								{
									this.ActivateBuildMode(this.FenceMode);
								}
								else
								{
									this.ClearBuild(false, false, false, false);
								}
							}
							else
							{
								UISoundFX.PlaySFX("BuildError", -1f, 0f);
							}
						}
						else
						{
							ErrorOverlay.Instance.ShowError("UnsupportedStructure", false, true, 4f, true);
							UISoundFX.PlaySFX("BuildError", -1f, 0f);
						}
					}
					else
					{
						ErrorOverlay.Instance.ShowError("RoomOutOfPlot", false, true, 4f, true);
						UISoundFX.PlaySFX("BuildError", -1f, 0f);
					}
				}
			}
			else if (!this.TooClose(p, null))
			{
				bool flag2 = true;
				Room room2 = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Floor == GameSettings.Instance.ActiveFloor && x.IsInside(p));
				WallEdge wallEdge = GameSettings.Instance.sRoomManager.GetEdgesOnFloor(GameSettings.Instance.ActiveFloor).FirstOrDefault((WallEdge x) => x.Pos.Dist(p) < this.SnapDistance);
				bool flag3 = wallEdge != null && !this.CurrentSegments.Contains(wallEdge) && (this.isCutting == null || wallEdge.Links.ContainsKey(this.isCutting));
				if (flag3)
				{
					flag2 = (this.CurrentSegments.Count == 0 || !this.AnyIntersections(this.CurrentSegments.Last<WallEdge>(), p, null));
				}
				if (flag3 && flag2)
				{
					if (!this.AngleTooSteep(wallEdge, false))
					{
						bool flag4 = true;
						if (this.CurrentSegments.Count > 0)
						{
							WallEdge wallEdge2 = this.CurrentSegments.Last<WallEdge>();
							float x3 = (wallEdge2.Pos.x + wallEdge.Pos.x) / 2f;
							float y = (wallEdge2.Pos.y + wallEdge.Pos.y) / 2f;
							Vector2 ppp = new Vector2(x3, y);
							Room room3 = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Floor == GameSettings.Instance.ActiveFloor && x.IsInside(ppp));
							if (room3 != null && wallEdge2.CouldSplit(room3) && wallEdge.CouldSplit(room3) && !wallEdge2.UpAgainst(wallEdge) && !wallEdge.UpAgainst(wallEdge2))
							{
								if (this.CurrentSegments.Count == 1)
								{
									this.isCutting = room3;
									if (this.AngleTooSteep(wallEdge, false))
									{
										this.isCutting = null;
										flag4 = false;
									}
								}
								else
								{
									flag4 = false;
								}
							}
						}
						if (flag4)
						{
							this.CurrentSegments.Add(wallEdge);
							if (this.isCutting != null && this.CurrentSegments.Count > 1)
							{
								float roomCost2 = BuildController.GetRoomCost((from x in this.CurrentSegments
								select x.Pos).ToList<Vector2>(), this.isCutting.Outdoors, GameSettings.Instance.ActiveFloor, true, false);
								if (GameSettings.Instance.MyCompany.CanMakeTransaction(-roomCost2) && this.ValidSplit())
								{
									string debug = string.Join("\n", (from x in this.CurrentSegments
									select x.Pos.x + ";" + x.Pos.y).ToArray<string>()) + "\n\n" + string.Join("\n", (from x in this.isCutting.Edges
									select x.Pos.x + ";" + x.Pos.y).ToArray<string>());
									List<UndoObject.UndoAction> list3 = new List<UndoObject.UndoAction>();
									this.FinalizeCuts(true, GameSettings.Instance.ActiveFloor, list3);
									Dictionary<WallSnap, UndoObject.UndoAction> snaps = this.isCutting.PrepareSplit(true);
									GameSettings.Instance.sRoomManager.AllSegments.AddRange(this.CurrentSegments);
									Room room4 = this.isCutting.Split(this.CurrentSegments, list3, snaps, debug, true);
									if (room4 != null)
									{
										CostDisplay.Instance.FloatAway(roomCost2);
										GameSettings.Instance.MyCompany.MakeTransaction(-roomCost2, Company.TransactionCategory.Construction, "Room");
										UISoundFX.PlaySFX("PlaceRoom", -1f, 0f);
										UISoundFX.PlaySFX("Kaching", -1f, 0f);
										GameSettings.Instance.sRoomManager.AddRoom(room4);
										List<UndoObject.UndoAction> list4 = new List<UndoObject.UndoAction>
										{
											new UndoObject.UndoAction(this.isCutting, room4, roomCost2)
										};
										foreach (Furniture furniture in this.isCutting.GetFurnitures().ToList<Furniture>())
										{
											if (!furniture.UpdateParent(true, false))
											{
												list4.Add(new UndoObject.UndoAction(furniture, false));
												list4.AddRange(from x in furniture.IterateSnap(null)
												select new UndoObject.UndoAction(x, false));
											}
										}
										list4.AddRange(list3);
										GameSettings.Instance.AddUndo(list4.ToArray());
										room4.RecalculateTableGroups();
										this.isCutting.RecalculateTableGroups();
										this.isCutting = null;
										if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
										{
											this.ActivateBuildMode(this.FenceMode);
										}
										else
										{
											this.ClearBuild(false, false, false, false);
										}
									}
									else
									{
										this.isCutting.OptimizeSegments();
										foreach (WallEdge item in this.CurrentSegments)
										{
											if (!this.isCutting.Edges.Contains(item))
											{
												GameSettings.Instance.sRoomManager.AllSegments.Remove(item);
											}
										}
										this.isCutting = null;
										if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
										{
											this.ActivateBuildMode(this.FenceMode);
										}
										else
										{
											this.ClearBuild(false, false, false, false);
										}
									}
								}
								else
								{
									this.CurrentSegments.Remove(wallEdge);
									UISoundFX.PlaySFX("BuildError", -1f, 0f);
								}
							}
							else
							{
								this.CurrentSegments.Remove(wallEdge);
								if (flag2)
								{
									this.AddSegment(wallEdge);
								}
							}
						}
					}
				}
				else
				{
					WallEdge split = null;
					foreach (WallEdge wallEdge3 in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(GameSettings.Instance.ActiveFloor))
					{
						foreach (KeyValuePair<Room, WallEdge> keyValuePair in wallEdge3.Links)
						{
							Vector2? vector3 = Utilities.ProjectToLine(p, wallEdge3.Pos, keyValuePair.Value.Pos);
							if (vector3 != null && vector3.Value.Dist(p) < this.SnapDistance)
							{
								split = new WallEdge(vector3.Value, GameSettings.Instance.ActiveFloor);
								split.SetSplit(wallEdge3, keyValuePair.Key);
								break;
							}
						}
						if (split != null)
						{
							break;
						}
					}
					bool flag5 = split != null && !this.TooClose(split.Pos, null);
					if (flag5)
					{
						flag2 = (this.CurrentSegments.Count == 0 || !this.AnyIntersections(this.CurrentSegments.Last<WallEdge>(), p, split));
					}
					if (flag5 && flag2)
					{
						if (!this.AngleTooSteep(split, false))
						{
							bool flag6 = true;
							if (this.CurrentSegments.Count > 0)
							{
								WallEdge wallEdge4 = this.CurrentSegments.Last<WallEdge>();
								float x2 = (wallEdge4.Pos.x + split.Pos.x) / 2f;
								float y2 = (wallEdge4.Pos.y + split.Pos.y) / 2f;
								Vector2 ppp = new Vector2(x2, y2);
								Room room5 = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Floor == GameSettings.Instance.ActiveFloor && x.IsInside(ppp));
								if (room5 != null && wallEdge4.CouldSplit(room5) && split.CouldSplit(room5) && !wallEdge4.UpAgainst(split) && !split.UpAgainst(wallEdge4))
								{
									this.isCutting = room5;
									if (this.CurrentSegments.Count > 1 || this.TooClose(split.Pos, null) || this.AngleTooSteep(split, false))
									{
										this.isCutting = null;
										flag6 = false;
									}
								}
							}
							else if (split.GetSplitEdges.Any((WallEdge x) => x.Pos.Dist(split.Pos).VeryStrictlyBelow(this.MinWallDistance)))
							{
								ErrorOverlay.Instance.ShowError("RoomNarrowError", false, true, 4f, true);
								flag6 = false;
							}
							if (flag6)
							{
								this.CurrentSegments.Add(split);
								if (this.isCutting != null && this.CurrentSegments.Count > 1)
								{
									float roomCost3 = BuildController.GetRoomCost((from x in this.CurrentSegments
									select x.Pos).ToList<Vector2>(), this.isCutting.Outdoors, GameSettings.Instance.ActiveFloor, true, false);
									if (GameSettings.Instance.MyCompany.CanMakeTransaction(-roomCost3) && this.ValidSplit())
									{
										string debug2 = string.Join("\n", (from x in this.CurrentSegments
										select x.Pos.x + ";" + x.Pos.y).ToArray<string>()) + "\n\n" + string.Join("\n", (from x in this.isCutting.Edges
										select x.Pos.x + ";" + x.Pos.y).ToArray<string>());
										List<UndoObject.UndoAction> list5 = new List<UndoObject.UndoAction>();
										this.FinalizeCuts(true, GameSettings.Instance.ActiveFloor, list5);
										Dictionary<WallSnap, UndoObject.UndoAction> snaps2 = this.isCutting.PrepareSplit(true);
										GameSettings.Instance.sRoomManager.AllSegments.AddRange(this.CurrentSegments);
										Room room6 = this.isCutting.Split(this.CurrentSegments, list5, snaps2, debug2, true);
										if (room6 != null)
										{
											CostDisplay.Instance.FloatAway(roomCost3);
											GameSettings.Instance.MyCompany.MakeTransaction(-roomCost3, Company.TransactionCategory.Construction, "Room");
											UISoundFX.PlaySFX("PlaceRoom", -1f, 0f);
											UISoundFX.PlaySFX("Kaching", -1f, 0f);
											GameSettings.Instance.sRoomManager.AddRoom(room6);
											List<UndoObject.UndoAction> list6 = new List<UndoObject.UndoAction>
											{
												new UndoObject.UndoAction(this.isCutting, room6, roomCost3)
											};
											foreach (Furniture furniture2 in this.isCutting.GetFurnitures().ToList<Furniture>())
											{
												if (!furniture2.UpdateParent(true, false))
												{
													list6.Add(new UndoObject.UndoAction(furniture2, false));
													list6.AddRange(from x in furniture2.IterateSnap(null)
													select new UndoObject.UndoAction(x, false));
												}
											}
											list6.AddRange(list5);
											GameSettings.Instance.AddUndo(list6.ToArray());
											room6.RecalculateTableGroups();
											this.isCutting.RecalculateTableGroups();
											this.isCutting = null;
											if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
											{
												this.ActivateBuildMode(this.FenceMode);
											}
											else
											{
												this.ClearBuild(false, false, false, false);
											}
										}
										else
										{
											this.isCutting.OptimizeSegments();
											foreach (WallEdge item2 in this.CurrentSegments)
											{
												if (!this.isCutting.Edges.Contains(item2))
												{
													GameSettings.Instance.sRoomManager.AllSegments.Remove(item2);
												}
											}
											this.isCutting = null;
											if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
											{
												this.ActivateBuildMode(this.FenceMode);
											}
											else
											{
												this.ClearBuild(false, false, false, false);
											}
										}
									}
									else
									{
										this.CurrentSegments.Remove(split);
										UISoundFX.PlaySFX("BuildError", -1f, 0f);
									}
								}
								else
								{
									this.CurrentSegments.Remove(split);
									if (this.CurrentSegments.Count == 0 || !this.AnyIntersections(this.CurrentSegments.Last<WallEdge>(), p, split))
									{
										this.AddSegment(split);
									}
								}
							}
						}
					}
					else if (this.isCutting == room2)
					{
						if (this.CurrentSegments.Count == 0 || !this.AnyIntersections(this.CurrentSegments.Last<WallEdge>(), p, null))
						{
							WallEdge wallEdge5 = new WallEdge(p, GameSettings.Instance.ActiveFloor);
							if (!this.AngleTooSteep(wallEdge5, false))
							{
								this.AddSegment(wallEdge5);
							}
						}
					}
					else if (this.CurrentSegments.Count == 1 && this.CurrentSegments[0].CouldSplit(room2) && !this.AnyIntersections(this.CurrentSegments.Last<WallEdge>(), p, null))
					{
						this.isCutting = room2;
						if (!this.TooClose(p, null))
						{
							WallEdge wallEdge6 = new WallEdge(p, GameSettings.Instance.ActiveFloor);
							if (!this.AngleTooSteep(wallEdge6, false))
							{
								this.AddSegment(wallEdge6);
							}
						}
					}
					else if (room2 != null)
					{
						ErrorOverlay.Instance.ShowError("RoomInRoomError", false, true, 4f, true);
					}
				}
			}
			if (this.CurrentSegments != null && this.CurrentSegments.Count > count)
			{
				UISoundFX.PlaySFX("PlaceWall", -1f, 0f);
			}
			else if (this.CurrentSegments != null)
			{
				UISoundFX.PlaySFX("BuildError", -1f, 0f);
			}
		}
		if (this.CurrentSegments != null && num > 0f)
		{
			Vector2 v;
			if ((this.CurrentSegments.Count > 0 && this.CurrentSegments[0].IsSplitter) || this.isCutting != null)
			{
				v = (this.CurrentSegments[this.CurrentSegments.Count - 1].Pos + p) * 0.5f;
			}
			else
			{
				v = Utilities.GetPolygonCentroid(this.CurrentSegments);
			}
			CostDisplay.Instance.Show(num, v.ToVector3((float)(GameSettings.Instance.ActiveFloor * 2 + 2)), (!GameSettings.Instance.MyCompany.CanMakeTransaction(-num)) ? Color.red : Color.white);
		}
		else
		{
			CostDisplay.Instance.Hide();
		}
	}

	public bool ContainsPoints()
	{
		Vector2[] array = (from x in this.CurrentSegments
		select x.Pos).ToArray<Vector2>();
		List<Room> list = (from x in GameSettings.Instance.sRoomManager.Rooms
		where x.Floor == GameSettings.Instance.ActiveFloor
		select x).ToList<Room>();
		using (IEnumerator<WallEdge> enumerator = GameSettings.Instance.sRoomManager.GetEdgesOnFloor(GameSettings.Instance.ActiveFloor).GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				WallEdge item = enumerator.Current;
				if (!array.Contains(item.Pos))
				{
					list.RemoveAll((Room x) => item.Links.Keys.Contains(x));
				}
				if (Utilities.IsInsideSnap(item.Pos, array, this.SnapDistance))
				{
					ErrorOverlay.Instance.ShowError("RoomInsideError", false, true, 4f, true);
					return true;
				}
				foreach (WallEdge wallEdge in item.Links.Values)
				{
					Vector2 p = (item.Pos + wallEdge.Pos) * 0.5f;
					if (Utilities.IsInsideSnap(p, array, this.SnapDistance))
					{
						ErrorOverlay.Instance.ShowError("RoomInsideError", false, true, 4f, true);
						return true;
					}
				}
			}
		}
		if (list.Count > 0)
		{
			ErrorOverlay.Instance.ShowError("RoomInsideError", false, true, 4f, true);
			return true;
		}
		return false;
	}

	public void BeginBuildFurniture(GameObject furnPrefab)
	{
		this.ClearBuild(false, false, false, false);
		this.CurrentFurnitureBuilder = UnityEngine.Object.Instantiate<GameObject>(this.FurnitureBuilderPrefab);
		this.CurrentFurnitureBuilder.GetComponent<FurnitureBuilder>().FurnPrefab = furnPrefab;
	}

	private void AlignCode(Vector2 p)
	{
		if (this.alignNow)
		{
			float d = 1f / this.GridMatrix.MultiplyVector(Vector3.right).magnitude;
			bool flag = false;
			Ray ray = this.MainCamera.ScreenPointToRay(Input.mousePosition);
			foreach (RaycastHit raycastHit in from x in Physics.RaycastAll(ray)
			orderby x.distance
			select x)
			{
				Furniture component = raycastHit.collider.GetComponent<Furniture>();
				if (component != null && component.Parent.Floor == GameSettings.Instance.ActiveFloor)
				{
					Vector3 vector = component.transform.rotation * (Vector3.one * 0.5f);
					Vector3 vector2 = new Vector3(component.OriginalPosition.x + ((!component.OnXEdge) ? vector.x : 0f), 0f, component.OriginalPosition.z + ((!component.OnYEdge) ? vector.z : 0f));
					this.GridMatrix = Matrix4x4.TRS(vector2, component.transform.rotation, Vector3.one * d).inverse;
					Graphics.DrawMesh(this.AlignMesh, Matrix4x4.TRS(vector2 + Vector3.up * ((float)(component.Parent.Floor * 2) + 1.5f), Quaternion.identity, new Vector3(0.2f, 3f, 0.2f)), this.AlignMaterial, 0);
					this.UpdateGridVisual();
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				bool flag2 = false;
				int num = 0;
				WallEdge wallEdge = null;
				WallEdge wallEdge2 = null;
				foreach (Room room in from x in GameSettings.Instance.sRoomManager.Rooms
				where x.Floor == GameSettings.Instance.ActiveFloor && x.IsInsideBounds(p, this.SnapDistance)
				select x)
				{
					foreach (WallEdge wallEdge3 in room.Edges)
					{
						WallEdge wallEdge4 = wallEdge3.Links[room];
						Vector2? vector3 = Utilities.ProjectToLine(p, wallEdge3.Pos, wallEdge4.Pos);
						if (vector3 != null && vector3.Value.Dist(p) < this.SnapDistance)
						{
							wallEdge = wallEdge3;
							wallEdge2 = wallEdge4;
							float num2 = wallEdge.Pos.Dist(p) / wallEdge2.Pos.Dist(wallEdge.Pos);
							flag2 = false;
							num = 0;
							if (num2 > 0.7f)
							{
								wallEdge2 = wallEdge3;
								wallEdge = wallEdge4;
							}
							else if (num2 > 0.6f)
							{
								flag2 = true;
								num = 1;
							}
							else if (num2 > 0.4f)
							{
								flag2 = true;
							}
							else if (num2 > 0.3f)
							{
								num = -1;
								flag2 = true;
							}
							break;
						}
					}
					if (wallEdge != null)
					{
						break;
					}
				}
				if (wallEdge != null)
				{
					Vector3 vector4 = new Vector3(wallEdge.Pos.x, 0f, wallEdge.Pos.y);
					Vector3 vector5 = new Vector3(wallEdge2.Pos.x, 0f, wallEdge2.Pos.y);
					if (flag2)
					{
						vector4 = (vector4 + vector5) * 0.5f + (float)num * (vector5 - vector4).normalized * 0.5f;
					}
					this.GridMatrix = Matrix4x4.TRS(new Vector3(vector4.x, 0f, vector4.z), Quaternion.LookRotation(vector4 - vector5), Vector3.one * d).inverse;
					if (flag2)
					{
						Graphics.DrawMesh(this.AlignMesh, Matrix4x4.TRS(new Vector3(vector4.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 2), vector4.z), Quaternion.identity, new Vector3(0.2f, 4f, 0.2f)), this.AlignMaterial, 0);
						Graphics.DrawMesh(this.AlignMesh, Matrix4x4.TRS(new Vector3(wallEdge.Pos.x, (float)(GameSettings.Instance.ActiveFloor * 2) + 1.5f, wallEdge.Pos.y), Quaternion.identity, new Vector3(0.2f, 3f, 0.2f)), this.AlignMaterial, 0);
					}
					else
					{
						Graphics.DrawMesh(this.AlignMesh, Matrix4x4.TRS(new Vector3(wallEdge.Pos.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 2), wallEdge.Pos.y), Quaternion.identity, new Vector3(0.2f, 4f, 0.2f)), this.AlignMaterial, 0);
					}
					Graphics.DrawMesh(this.AlignMesh, Matrix4x4.TRS(new Vector3(wallEdge2.Pos.x, (float)(GameSettings.Instance.ActiveFloor * 2) + 1.5f, wallEdge2.Pos.y), Quaternion.identity, new Vector3(0.2f, 3f, 0.2f)), this.AlignMaterial, 0);
					this.UpdateGridVisual();
				}
			}
			if (Input.GetMouseButtonUp(0))
			{
				this.OldMatrix = this.GridMatrix;
				this.ClearBuild(false, false, false, false);
			}
			if (Input.GetMouseButtonUp(1))
			{
				this.ClearBuild(false, false, false, false);
			}
		}
	}

	public void ClearBuild(bool clone = false, bool road = false, bool plot = false, bool environment = false)
	{
		CostDisplay.Instance.Hide();
		this.TempWallList.ForEach(delegate(GameObject x)
		{
			UnityEngine.Object.Destroy(x);
		});
		this.TempWallList.Clear();
		this.RectDragging = false;
		this.RectPoints = null;
		this.CurrentTempWall = null;
		this.CurrentSegments = null;
		this.isCutting = null;
		UnityEngine.Object.Destroy(this.CurrentTempSegment);
		this.CurrentTempSegment = null;
		this.DynSegmentE1 = null;
		this.DynSegmentE2 = null;
		this.mergeNow = false;
		this.alignNow = false;
		this.Arrow.SetActive(false);
		if (this.CurrentFurnitureBuilder != null)
		{
			UnityEngine.Object.Destroy(this.CurrentFurnitureBuilder);
		}
		this.GridMatrix = this.OldMatrix;
		this.UpdateGridVisual();
		BuildingHUD.Instance.Enable(false, false, false);
		HUD.Instance.UpdateBorderOverlay();
		if (!clone)
		{
			RoomCloneTool.Instance.gameObject.SetActive(false);
		}
		if (!road)
		{
			RoadBuildCube.Instance.gameObject.SetActive(false);
		}
		if (!plot && PlotController.Instance != null)
		{
			PlotController.Instance.gameObject.SetActive(false);
		}
		if (!environment && EnvironmentEditor.Instance != null)
		{
			EnvironmentEditor.Instance.gameObject.SetActive(false);
		}
		if (WallRemovalTool.Instance != null)
		{
			WallRemovalTool.Instance.gameObject.SetActive(false);
		}
	}

	public void BeginSegmentBuild(GameObject segmentPrefab)
	{
		this.WallSegmentPrefab = segmentPrefab;
		RoomSegment component = this.WallSegmentPrefab.GetComponent<RoomSegment>();
		if (component.DynamicWidth && component.MaxDynamicWidth <= 0f)
		{
			HintController.Instance.Show(HintController.Hints.HintAutoScaleSegment);
		}
		HintController.Instance.Show(HintController.Hints.HintMultipleBuild);
		this.ClearBuild(false, false, false, false);
		this.CurrentTempSegment = UnityEngine.Object.Instantiate<GameObject>(this.TempWallSegmentPrefab);
		this.CurrentTempSegment.transform.localScale = new Vector3(Room.WallOffset + 0.1f, 2.1f, component.WallWidth);
	}

	private void CheckLastTick()
	{
		Vector2 vector = this.CurrentTempSegment.transform.position.FlattenVector3();
		if (this.LastPos != vector)
		{
			UISoundFX.PlaySFX("Tick", -1f, 0f);
		}
		this.LastPos = vector;
	}

	private void SegmentBuildCode()
	{
		if (this.CurrentTempSegment != null)
		{
			if (Input.GetMouseButtonUp(1))
			{
				this.ClearBuild(false, false, false, false);
				return;
			}
			RoomSegment component = this.WallSegmentPrefab.GetComponent<RoomSegment>();
			if (component.DynamicWidth)
			{
				ErrorOverlay.Instance.ShowError("DynamicSegmentHint", false, false, 0f, true);
			}
			this.Arrow.SetActive(component.Directional);
			CostDisplay.Instance.Show(component.Cost, this.CurrentTempSegment.transform.position, (!GameSettings.Instance.MyCompany.CanMakeTransaction(-component.Cost)) ? Color.red : Color.white);
			Vector2 pos = this.GetMousePos(new Plane(Vector3.up, Vector3.up * ((float)(GameSettings.Instance.ActiveFloor * 2) + component.YOffset)));
			IEnumerable<Room> enumerable = (!component.DynamicWidth || this.DynSegmentE1 == null) ? (from x in GameSettings.Instance.sRoomManager.Rooms
			where x.Floor == GameSettings.Instance.ActiveFloor && x.IsInsideBounds(pos, this.SnapDistance)
			select x) : null;
			float num = 1f;
			if ((!component.DynamicWidth || this.DynSegmentE1 == null) && !enumerable.Any<Room>())
			{
				this.Arrow.SetActive(false);
				this.SetSegmentPos(new Vector3(pos.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), pos.y), new Vector3(Room.WallOffset + 0.1f, 2.1f, component.WallWidth));
			}
			else if (component.DynamicWidth && this.DynSegmentE1 != null)
			{
				float num2 = this.DynSegmentPos;
				float mult = component.WallWidth;
				Vector2 p = this.CorrectMousePos(pos, false);
				Vector2 p2 = Utilities.ProjectToLineEndless(p, this.DynSegmentE1.Pos, this.DynSegmentE2.Pos);
				float num3 = this.DynSegmentE1.Pos.Dist(this.DynSegmentE2.Pos);
				float num4 = this.DynSegmentE1.Pos.Dist(p2);
				float num5 = this.DynSegmentE2.Pos.Dist(p2);
				float num8;
				if (num4 / num3 > this.DynSegmentPos && (num5 < num3 || num4 > num5))
				{
					float num6 = this.DynSegmentPos * num3 - component.WallWidth / 2f;
					num4 = Mathf.Clamp(num4, num6 + component.WallWidth, num3);
					if (component.MaxDynamicWidth > 0f)
					{
						if (num4 - num6 > component.MaxDynamicWidth)
						{
							num4 = num6 + component.MaxDynamicWidth;
						}
					}
					else if (Input.GetKey(KeyCode.LeftControl))
					{
						float? num7 = this.DynSegmentE1.FirstValidFrom(num6, this.DynSegmentE2);
						if (num7 != null)
						{
							num4 = num7.Value;
						}
						else
						{
							num4 = num3;
						}
						num7 = this.DynSegmentE2.FirstValidFrom(num3 - num6, this.DynSegmentE1);
						if (num7 != null)
						{
							num6 = num3 - num7.Value;
						}
						else
						{
							num6 = 0f;
						}
					}
					Vector2 a = this.DynSegmentE1.Pos + (this.DynSegmentE2.Pos - this.DynSegmentE1.Pos).normalized * num6;
					Vector2 b = this.DynSegmentE1.Pos + (this.DynSegmentE2.Pos - this.DynSegmentE1.Pos).normalized * num4;
					Vector2 vector = (a + b) * 0.5f;
					float magnitude = (a - b).magnitude;
					Vector2 rhs = vector;
					if (this.DynSegmentE1.ValidSegment(ref vector, magnitude, this.DynSegmentE2, false, false, !component.InsideSegment, component.Height1, component.Height2, true, 0f, 0f, false, null) && vector == rhs)
					{
						num8 = Mathf.Abs(num6 - num4) / component.WallWidth;
						this.SetSegmentPos(new Vector3(vector.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), vector.y), new Vector3(Room.WallOffset + 0.1f, 2.1f, magnitude));
						this.CheckLastTick();
						num2 = (num6 + num4) / 2f;
						num2 /= num3;
						mult = magnitude;
					}
					else
					{
						float? num9 = this.DynSegmentE1.FirstValidFrom(num6, this.DynSegmentE2);
						if (num9 != null)
						{
							num4 = num9.Value;
							a = this.DynSegmentE1.Pos + (this.DynSegmentE2.Pos - this.DynSegmentE1.Pos).normalized * num6;
							b = this.DynSegmentE1.Pos + (this.DynSegmentE2.Pos - this.DynSegmentE1.Pos).normalized * num4;
							vector = (a + b) * 0.5f;
							magnitude = (a - b).magnitude;
							num8 = Mathf.Abs(num6 - num4) / component.WallWidth;
							this.SetSegmentPos(new Vector3(vector.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), vector.y), new Vector3(Room.WallOffset + 0.1f, 2.1f, magnitude));
							this.CheckLastTick();
							num2 = (num6 + num4) / 2f;
							num2 /= num3;
							mult = magnitude;
						}
						else
						{
							num8 = 1f;
							this.SetSegmentPos(new Vector3(this.DynSegmentVec.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), this.DynSegmentVec.y), new Vector3(Room.WallOffset + 0.1f, 2.1f, component.WallWidth));
							this.CheckLastTick();
						}
					}
				}
				else
				{
					if (num5 > num3)
					{
						num4 = 0f;
					}
					float num10 = this.DynSegmentPos * num3 + component.WallWidth / 2f;
					num4 = Mathf.Clamp(num4, 0f, num10 - component.WallWidth);
					if (component.MaxDynamicWidth > 0f)
					{
						if (num10 - num4 > component.MaxDynamicWidth)
						{
							num4 = num10 - component.MaxDynamicWidth;
						}
					}
					else if (Input.GetKey(KeyCode.LeftControl))
					{
						float? num11 = this.DynSegmentE1.FirstValidFrom(num10, this.DynSegmentE2);
						if (num11 != null)
						{
							num4 = num11.Value;
						}
						else
						{
							num4 = num3;
						}
						num11 = this.DynSegmentE2.FirstValidFrom(num3 - num10, this.DynSegmentE1);
						if (num11 != null)
						{
							num10 = num3 - num11.Value;
						}
						else
						{
							num10 = 0f;
						}
					}
					Vector2 a2 = this.DynSegmentE1.Pos + (this.DynSegmentE2.Pos - this.DynSegmentE1.Pos).normalized * num10;
					Vector2 b2 = this.DynSegmentE1.Pos + (this.DynSegmentE2.Pos - this.DynSegmentE1.Pos).normalized * num4;
					Vector2 vector2 = (a2 + b2) * 0.5f;
					float magnitude2 = (a2 - b2).magnitude;
					Vector2 rhs2 = vector2;
					if (this.DynSegmentE1.ValidSegment(ref vector2, magnitude2, this.DynSegmentE2, false, false, !component.InsideSegment, component.Height1, component.Height2, true, 0f, 0f, false, null) && vector2 == rhs2)
					{
						num8 = Mathf.Abs(num10 - num4) / component.WallWidth;
						this.SetSegmentPos(new Vector3(vector2.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), vector2.y), new Vector3(Room.WallOffset + 0.1f, 2.1f, magnitude2));
						this.CheckLastTick();
						num2 = (num10 + num4) / 2f;
						num2 /= num3;
						mult = magnitude2;
					}
					else
					{
						float? num12 = this.DynSegmentE2.FirstValidFrom(num3 - num10, this.DynSegmentE1);
						if (num12 != null)
						{
							num4 = num3 - num12.Value;
							a2 = this.DynSegmentE1.Pos + (this.DynSegmentE2.Pos - this.DynSegmentE1.Pos).normalized * num10;
							b2 = this.DynSegmentE1.Pos + (this.DynSegmentE2.Pos - this.DynSegmentE1.Pos).normalized * num4;
							vector2 = (a2 + b2) * 0.5f;
							magnitude2 = (a2 - b2).magnitude;
							num8 = Mathf.Abs(num10 - num4) / component.WallWidth;
							this.SetSegmentPos(new Vector3(vector2.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), vector2.y), new Vector3(Room.WallOffset + 0.1f, 2.1f, magnitude2));
							this.CheckLastTick();
							num2 = (num10 + num4) / 2f;
							num2 /= num3;
							mult = magnitude2;
						}
						else
						{
							num8 = 1f;
							this.SetSegmentPos(new Vector3(this.DynSegmentVec.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), this.DynSegmentVec.y), new Vector3(Room.WallOffset + 0.1f, 2.1f, component.WallWidth));
							this.CheckLastTick();
						}
					}
				}
				CostDisplay.Instance.Show(component.Cost * num8, this.CurrentTempSegment.transform.position, (!GameSettings.Instance.MyCompany.CanMakeTransaction(-component.Cost * num8)) ? Color.red : Color.white);
				if (Input.GetMouseButtonUp(0))
				{
					if (!GameSettings.Instance.MyCompany.CanMakeTransaction(-component.Cost * num8))
					{
						UISoundFX.PlaySFX("BuildError", -1f, 0f);
					}
					else
					{
						CostDisplay.Instance.FloatAway();
						UISoundFX.PlaySFX("PlaceFurniture", -1f, 0f);
						UISoundFX.PlaySFX("Kaching", -1f, 0f);
						GameSettings.Instance.MyCompany.MakeTransaction(-component.Cost * num8, Company.TransactionCategory.Construction, "Segment");
						GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.WallSegmentPrefab);
						RoomSegment component2 = gameObject.GetComponent<RoomSegment>();
						component2.FixDynamicWidth(mult);
						component2.Floor = GameSettings.Instance.ActiveFloor;
						component2.transform.position = new Vector3(0f, (float)(GameSettings.Instance.ActiveFloor * 2), 0f);
						component2.Init(this.DynSegmentE1, this.DynSegmentE2, num2, false);
						this.DynSegmentE1 = null;
						this.DynSegmentE2 = null;
						this.SetSegmentScale(new Vector3(Room.WallOffset + 0.1f, 2.1f, component.WallWidth));
						component2.name = component2.name.Replace("(Clone)", string.Empty).Trim();
						GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
						{
							new UndoObject.UndoAction(component2, true)
						});
						if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
						{
							this.ClearBuild(false, false, false, false);
						}
					}
				}
			}
			else
			{
				Vector2 p3 = this.CorrectMousePos(pos, Mathf.RoundToInt(component.WallWidth) % 2 == 1);
				float num13 = component.WallWidth / 2f;
				WallEdge wallEdge = null;
				WallEdge wallEdge2 = null;
				foreach (Room room in enumerable)
				{
					foreach (WallEdge wallEdge3 in room.Edges)
					{
						WallEdge wallEdge4 = wallEdge3;
						WallEdge wallEdge5 = wallEdge3.Links[room];
						if (wallEdge4.Pos.Dist(wallEdge5.Pos) >= num13 * 2f)
						{
							if (!component.OnlyInterior || (!room.Outdoors && wallEdge5.Links.ContainsValue(wallEdge4) && !wallEdge5.GetRoom(wallEdge4).Outdoors))
							{
								Vector2? vector3 = Utilities.ProjectToLine(pos, wallEdge4.Pos, wallEdge5.Pos);
								if (vector3 != null)
								{
									float num14 = vector3.Value.Dist(pos);
									if (num14 < num)
									{
										Vector2? vector4 = Utilities.ProjectToLine(p3, wallEdge4.Pos, wallEdge5.Pos);
										if (vector4 != null)
										{
											if (vector4.Value.Dist(wallEdge4.Pos) < num13)
											{
												vector4 = new Vector2?(wallEdge4.Pos + (wallEdge5.Pos - wallEdge4.Pos).normalized * num13);
											}
											if (vector4.Value.Dist(wallEdge5.Pos) < num13)
											{
												vector4 = new Vector2?(wallEdge5.Pos + (wallEdge4.Pos - wallEdge5.Pos).normalized * num13);
											}
											Vector2 p4 = vector4.Value;
											if (wallEdge4.ValidSegment(ref p4, component.WallWidth, wallEdge5, false, false, !component.InsideSegment, component.Height1, component.Height2, true, vector3.Value.x - p4.x, vector3.Value.y - p4.y, false, null))
											{
												num = num14;
												if (component.DynamicWidth && component.MaxDynamicWidth <= 0f && Input.GetKey(KeyCode.LeftControl))
												{
													float num15 = wallEdge4.Pos.Dist(wallEdge5.Pos);
													float num16 = wallEdge4.Pos.Dist(p4);
													float num17 = num15;
													float? num18 = wallEdge4.FirstValidFrom(num16, wallEdge5);
													if (num18 != null)
													{
														num17 = num18.Value;
													}
													num18 = wallEdge5.FirstValidFrom(num15 - num16, wallEdge4);
													if (num18 != null)
													{
														num16 = num15 - num18.Value;
													}
													else
													{
														num16 = 0f;
													}
													float num19 = Mathf.Abs(num17 - num16);
													float num20 = component.Cost * (num19 / component.WallWidth);
													CostDisplay.Instance.Show(num20, this.CurrentTempSegment.transform.position, (!GameSettings.Instance.MyCompany.CanMakeTransaction(-num20)) ? Color.red : Color.white);
													p4 = wallEdge4.Pos + (wallEdge5.Pos - wallEdge4.Pos) * ((num17 + num16) * 0.5f / num15);
													this.SetSegmentScale(new Vector3(Room.WallOffset + 0.1f, 2.1f, num19));
												}
												else
												{
													this.SetSegmentScale(new Vector3(Room.WallOffset + 0.1f, 2.1f, component.WallWidth));
												}
												this.SetSegmentPos(new Vector3(p4.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), p4.y));
												if (component.Directional && Utilities.isLeft(wallEdge4.Pos, wallEdge5.Pos, pos) > 0)
												{
													wallEdge = wallEdge5;
													wallEdge2 = wallEdge4;
												}
												else
												{
													wallEdge = wallEdge4;
													wallEdge2 = wallEdge5;
												}
												Vector2 vector5 = wallEdge.Pos - wallEdge2.Pos;
												this.SetSegmentRotation(Quaternion.LookRotation(new Vector3(vector5.x, 0f, vector5.y)));
											}
										}
									}
								}
							}
						}
					}
				}
				if (wallEdge != null)
				{
					this.CheckLastTick();
				}
				if (wallEdge == null)
				{
					this.Arrow.SetActive(false);
					this.SetSegmentPos(new Vector3(pos.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), pos.y), new Vector3(Room.WallOffset + 0.1f, 2.1f, component.WallWidth));
				}
				else if (Input.GetMouseButtonDown(0))
				{
					if (component.DynamicWidth)
					{
						if (this.DynSegmentE1 == null)
						{
							Vector3 position = this.CurrentTempSegment.transform.position;
							this.DynSegmentVec = new Vector2(position.x, position.z);
							this.DynSegmentE1 = wallEdge;
							this.DynSegmentE2 = wallEdge2;
							this.DynSegmentPos = this.DynSegmentE1.Pos.Dist(this.DynSegmentVec) / this.DynSegmentE1.Pos.Dist(this.DynSegmentE2.Pos);
						}
					}
					else if (!GameSettings.Instance.MyCompany.CanMakeTransaction(-component.Cost))
					{
						UISoundFX.PlaySFX("BuildError", -1f, 0f);
					}
					else
					{
						CostDisplay.Instance.FloatAway();
						UISoundFX.PlaySFX("PlaceFurniture", -1f, 0f);
						UISoundFX.PlaySFX("Kaching", -1f, 0f);
						GameSettings.Instance.MyCompany.MakeTransaction(-component.Cost, Company.TransactionCategory.Construction, "Segment");
						GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.WallSegmentPrefab);
						Vector3 position2 = this.CurrentTempSegment.transform.position;
						Vector2 p5 = new Vector2(position2.x, position2.z);
						WallEdge wallEdge6 = wallEdge;
						WallEdge wallEdge7 = wallEdge2;
						RoomSegment component3 = gameObject2.GetComponent<RoomSegment>();
						component3.Floor = GameSettings.Instance.ActiveFloor;
						component3.transform.position = new Vector3(0f, (float)(GameSettings.Instance.ActiveFloor * 2), 0f);
						component3.Init(wallEdge6, wallEdge7, wallEdge6.Pos.Dist(p5) / wallEdge6.Pos.Dist(wallEdge7.Pos), false);
						component3.name = component3.name.Replace("(Clone)", string.Empty).Trim();
						GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
						{
							new UndoObject.UndoAction(component3, true)
						});
						if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
						{
							this.ClearBuild(false, false, false, false);
						}
					}
				}
			}
		}
	}

	private void SetSegmentScale(Vector3 scale)
	{
		this.SetSegmentPos(this.CurrentTempSegment.transform.position, scale);
	}

	private void SetSegmentPos(Vector3 pos)
	{
		this.SetSegmentPos(pos, this.CurrentTempSegment.transform.localScale);
	}

	private void SetSegmentPos(Vector3 pos, Vector3 scale)
	{
		this.CurrentTempSegment.transform.localScale = scale;
		this.CurrentTempSegment.transform.position = pos;
		this.Arrow.transform.position = pos + Vector3.up * 2f;
	}

	private void SetSegmentRotation(Quaternion rotation)
	{
		this.CurrentTempSegment.transform.rotation = rotation;
		this.Arrow.transform.rotation = Quaternion.Euler(0f, 90f, 0f) * rotation;
	}

	public void ProjectToWalls(int floor)
	{
		for (int i = 0; i < this.CurrentSegments.Count; i++)
		{
			WallEdge wallEdge = this.CurrentSegments[i];
			WallEdge wallEdge2 = this.CurrentSegments[(i + 1) % this.CurrentSegments.Count];
			foreach (WallEdge wallEdge3 in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(floor))
			{
				if (wallEdge3 != wallEdge && wallEdge3 != wallEdge2)
				{
					Vector2? vector = Utilities.ProjectToLine(wallEdge3.Pos, wallEdge.Pos, wallEdge2.Pos);
					if (vector != null && wallEdge3.Pos.Dist(vector.Value) < this.SnapDistance)
					{
						this.CurrentSegments.Insert(i + 1, wallEdge3);
						i--;
						break;
					}
				}
			}
		}
	}

	public bool ValidSplit()
	{
		if (this.isCutting == null || this.CurrentSegments.Count < 2 || (!this.CurrentSegments[0].Links.ContainsKey(this.isCutting) && !this.CurrentSegments[0].GetSplitRooms.Contains(this.isCutting)))
		{
			return false;
		}
		foreach (WallEdge wallEdge in this.isCutting.Edges)
		{
			for (int i = 0; i < this.CurrentSegments.Count - 1; i++)
			{
				WallEdge wallEdge2 = this.CurrentSegments[i];
				WallEdge wallEdge3 = this.CurrentSegments[i + 1];
				if (wallEdge2.UpAgainst(wallEdge3) || wallEdge3.UpAgainst(wallEdge2))
				{
					return false;
				}
				if (wallEdge != wallEdge2 && wallEdge != wallEdge3)
				{
					Vector2? vector = Utilities.ProjectToLine(wallEdge.Pos, wallEdge2.Pos, wallEdge3.Pos);
					if (vector != null && wallEdge.Pos.Dist(vector.Value).VeryStrictlyBelow(this.MinWallDistance))
					{
						ErrorOverlay.Instance.ShowError("RoomNarrowError", false, true, 4f, true);
						return false;
					}
				}
			}
		}
		return true;
	}

	public void FinalizeCuts(bool splitting, int floor, List<UndoObject.UndoAction> undos)
	{
		for (int i = 0; i < this.CurrentSegments.Count; i++)
		{
			if (splitting && i == this.CurrentSegments.Count - 1)
			{
				break;
			}
			WallEdge wallEdge = this.CurrentSegments[i];
			WallEdge wallEdge2 = this.CurrentSegments[(i + 1) % this.CurrentSegments.Count];
			foreach (WallEdge wallEdge3 in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(floor))
			{
				if (wallEdge3 != wallEdge && wallEdge3 != wallEdge2)
				{
					Vector2? vector = Utilities.ProjectToLine(wallEdge3.Pos, wallEdge.Pos, wallEdge2.Pos);
					if (vector != null && wallEdge3.Pos.Dist(vector.Value) < this.SnapDistance)
					{
						this.CurrentSegments.Insert(i + 1, wallEdge3);
						i--;
						break;
					}
				}
			}
		}
		foreach (WallEdge wallEdge4 in this.CurrentSegments)
		{
			if (wallEdge4.IsSplitter)
			{
				float num = float.MaxValue;
				WallEdge wallEdge5 = null;
				Room room = wallEdge4.GetSplitRooms[0];
				foreach (WallEdge wallEdge6 in room.Edges)
				{
					WallEdge wallEdge7 = wallEdge6.Links[room];
					Vector2? vector2 = Utilities.ProjectToLine(wallEdge4.Pos, wallEdge6.Pos, wallEdge7.Pos);
					if (vector2 != null)
					{
						float num2 = vector2.Value.Dist(wallEdge4.Pos);
						if (num2 < this.SnapDistance && num2 < num)
						{
							num = num2;
							wallEdge5 = wallEdge6;
						}
					}
				}
				if (wallEdge5 != null)
				{
					wallEdge4.SetSplit(wallEdge5, room);
					wallEdge4.SplitSegment(undos);
					GameSettings.Instance.sRoomManager.AllSegments.Add(wallEdge4);
				}
				else
				{
					wallEdge4.ResetSplit();
				}
			}
		}
	}

	public void RoomFromClipboard()
	{
	}

	public void MakeRoomFromString(string text)
	{
		string[] array = text.Split(new string[]
		{
			Environment.NewLine,
			"\r\n",
			"\n"
		}, StringSplitOptions.RemoveEmptyEntries);
		string[] array2 = array[0].Split(new char[]
		{
			';'
		}, StringSplitOptions.RemoveEmptyEntries);
		List<WallEdge> list = new List<WallEdge>();
		for (int i = 0; i < array2.Length; i += 2)
		{
			float x = (float)Convert.ToDouble(array2[i]);
			float y = (float)Convert.ToDouble(array2[i + 1]);
			list.Add(new WallEdge(new Vector2(x, y), GameSettings.Instance.ActiveFloor));
		}
		Room room = this.MakeRoom(list, GameSettings.Instance.ActiveFloor, null, true, false, false, false);
		GameSettings.Instance.sRoomManager.AllSegments.AddRange(room.Edges);
		List<Furniture> list2 = new List<Furniture>();
		for (int j = 1; j < array.Length; j++)
		{
			string[] array3 = array[j].Split(new char[]
			{
				';'
			}, StringSplitOptions.RemoveEmptyEntries);
			GameObject furniture = ObjectDatabase.Instance.GetFurniture(array3[0]);
			Vector3 position = new Vector3((float)Convert.ToDouble(array3[1]), 0f, (float)Convert.ToDouble(array3[2]));
			Quaternion rot = Quaternion.Euler(0f, (float)Convert.ToDouble(array3[3]), 0f);
			WallEdge wallEdge = null;
			WallEdge wallEdge2 = null;
			float wallPos = 0f;
			SnapPoint snapPoint = null;
			if (furniture.GetComponent<Furniture>().WallFurn)
			{
				wallEdge = list[Convert.ToInt32(array3[4])];
				wallEdge2 = list[Convert.ToInt32(array3[5])];
				wallPos = (float)Convert.ToDouble(array3[6]) / (wallEdge.Pos - wallEdge2.Pos).magnitude;
			}
			if (furniture.GetComponent<Furniture>().IsSnapping)
			{
				snapPoint = list2[Convert.ToInt32(array3[7])].SnapPoints[Convert.ToInt32(array3[8])];
				position = snapPoint.transform.position;
			}
			Furniture item = FurnitureBuilder.MakeFurn(position, rot, room, wallEdge, wallEdge2, wallPos, snapPoint, furniture, 0f, false, 0f);
			list2.Add(item);
		}
	}

	public static float FencePrice = 50f;

	public static float WallPrice = 250f;

	public static float OutdoorPrice = 100f;

	public static float RoomPrice = 600f;

	public static float OutdoorRentPrice = 5f;

	public static float RoomRentPrice = 15f;

	public GameObject TempWallPrefab;

	public GameObject RoomPrefab;

	public GameObject TempWallSegmentPrefab;

	public GameObject WallSegmentPrefab;

	public GameObject FurnitureBuilderPrefab;

	public GameObject PillarPrefab;

	public ParticleSystem DirtEmitter;

	[NonSerialized]
	public List<WallEdge> CurrentSegments;

	[NonSerialized]
	public Vector2[] RectPoints;

	public List<GameObject> TempWallList = new List<GameObject>();

	public GameObject CurrentTempWall;

	public GameObject CurrentTempSegment;

	public GameObject CurrentFurnitureBuilder;

	public Room SelectedRoom;

	public bool mergeNow;

	public bool alignNow;

	public bool FenceMode;

	public Room isCutting;

	[NonSerialized]
	public float SnapDistance = 0.5f;

	public static BuildController Instance;

	public Matrix4x4 GridMatrix;

	private Matrix4x4 OldMatrix;

	public Material MainGridMaterial;

	public float MinAngle;

	public float MinWallDistance;

	public Camera MainCamera;

	public Mesh AlignMesh;

	public Material AlignMaterial;

	public float FurnitureAngle = 45f;

	public float[] Angles;

	public Sprite[] AngleSprites;

	private int AnlgeNum;

	public Vector2 LastPos;

	public bool InTempGrid;

	public GameObject Arrow;

	private bool RectDragging;

	private float DynSegmentPos;

	private Vector2 DynSegmentVec;

	private WallEdge DynSegmentE1;

	private WallEdge DynSegmentE2;
}
