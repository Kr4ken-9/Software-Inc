﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class RoomDescriptor
{
	public RoomDescriptor(RoomDescriptor.RoomObject[] r, SVector3[] e)
	{
		this.Rooms = r;
		this.Edges = e;
	}

	public static RoomDescriptor SaveRooms(IEnumerable<Room> rooms, GameReader.LoadMode mode)
	{
		Dictionary<WallEdge, int> dictionary = new Dictionary<WallEdge, int>();
		List<WallEdge> list = new List<WallEdge>();
		List<RoomDescriptor.RoomObject> list2 = new List<RoomDescriptor.RoomObject>();
		List<int> list3 = new List<int>();
		int num = 0;
		foreach (Room room in rooms)
		{
			for (int i = 0; i < room.Edges.Count; i++)
			{
				WallEdge wallEdge = room.Edges[i];
				int item = 0;
				if (dictionary.TryGetValue(wallEdge, out item))
				{
					list3.Add(item);
				}
				else
				{
					list3.Add(num);
					dictionary[wallEdge] = num;
					list.Add(wallEdge);
					num++;
				}
			}
			list2.Add(new RoomDescriptor.RoomObject(room.SerializeThis(mode), list3.ToArray(), room.Floor));
			list3.Clear();
		}
		return new RoomDescriptor(list2.ToArray(), (from x in list
		select x.Pos).ToArray<SVector3>());
	}

	public void BuildRooms(Func<GameObject> makeRoom)
	{
		WallEdge[] edges = this.Edges.SelectInPlace((SVector3 x) => new WallEdge(x, 0));
		GameSettings.Instance.sRoomManager.AllSegments.AddRange(edges);
		foreach (RoomDescriptor.RoomObject roomObject in this.Rooms)
		{
			GameObject gameObject = makeRoom();
			Room component = gameObject.GetComponent<Room>();
			component.DeserializeThis(roomObject.RoomData);
			IEnumerable<int> source = roomObject.Edges;
			if (roomObject.Edges[0] == roomObject.Edges[roomObject.Edges.Length - 1])
			{
				source = source.Take(roomObject.Edges.Length - 1);
			}
			component.Init(from x in source
			select edges[x], roomObject.Floor, null, false, false);
		}
	}

	public RoomDescriptor.RoomObject[] Rooms;

	public SVector3[] Edges;

	[Serializable]
	public class RoomObject
	{
		public RoomObject(WriteDictionary data, int[] edge, int floor)
		{
			this.RoomData = data;
			this.Edges = edge;
			this.Floor = floor;
		}

		public WriteDictionary RoomData;

		public int Floor;

		public int[] Edges;
	}
}
