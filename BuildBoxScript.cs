﻿using System;
using UnityEngine;

public class BuildBoxScript : MonoBehaviour
{
	private void Start()
	{
		this._high = (base.transform.localScale.y > 1.1f);
		this.InitMat();
	}

	private void InitMat()
	{
		this._mat = new Material((!this._high) ? this.LowMat : this.HighMat);
		Renderer component = base.GetComponent<Renderer>();
		component.sharedMaterial = this._mat;
	}

	private void Update()
	{
		bool flag = base.transform.localScale.y > 1.1f;
		if (this._high != flag)
		{
			this._high = flag;
			this.InitMat();
		}
		this._mat.mainTextureScale = new Vector2(base.transform.localScale.z, 1f);
	}

	public Material HighMat;

	public Material LowMat;

	[NonSerialized]
	private Material _mat;

	[NonSerialized]
	private bool _high;
}
