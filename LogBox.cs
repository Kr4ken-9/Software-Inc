﻿using System;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LogBox : MonoBehaviour
{
	public string RealLog
	{
		get
		{
			return this._realLog;
		}
		set
		{
			this._realLog = value;
			this.Log = value.Split(new char[]
			{
				'\n'
			});
			this.CurrentLine = 0;
			this.DisableUpdate = true;
			this.Scrollbar.size = Mathf.Clamp01(this.OwnerPanel.rect.height / (float)this.text.font.lineHeight / (float)this.Log.Length);
			this.DisableUpdate = false;
			this.UpdateScrollDirect();
		}
	}

	public void Scroll(BaseEventData data)
	{
		this.CurrentLine = Mathf.Clamp(this.CurrentLine + ((((PointerEventData)data).scrollDelta.y >= 0f) ? (-this.JumpSpeed) : this.JumpSpeed), 0, Mathf.CeilToInt((float)(this.Log.Length + 1) - this.OwnerPanel.rect.height / (float)this.text.font.lineHeight));
		this.UpdateScrollDirect();
	}

	public void UpdateScrollDirect()
	{
		this.DisableUpdate = true;
		this.Scrollbar.value = (float)this.CurrentLine / ((float)(this.Log.Length + 1) - this.OwnerPanel.rect.height / (float)this.text.font.lineHeight);
		this.UpdateText();
		this.DisableUpdate = false;
	}

	public void UpdateScrollIndirect()
	{
		if (!this.DisableUpdate)
		{
			this.CurrentLine = Mathf.CeilToInt(this.Scrollbar.value * ((float)(this.Log.Length + 1) - this.OwnerPanel.rect.height / (float)this.text.font.lineHeight));
			this.UpdateText();
		}
	}

	public void UpdateText()
	{
		int num = Mathf.CeilToInt(this.OwnerPanel.rect.height / (float)this.text.font.lineHeight);
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < num; i++)
		{
			int num2 = i + this.CurrentLine;
			if (num2 >= this.Log.Length)
			{
				break;
			}
			stringBuilder.AppendLine(this.Log[num2]);
		}
		this.text.text = stringBuilder.ToString();
	}

	public void UpdateLog()
	{
		string logFile = FeedbackWindow.GetLogFile();
		if (File.Exists(logFile))
		{
			try
			{
				using (FileStream fileStream = File.Open(logFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
				{
					using (StreamReader streamReader = new StreamReader(fileStream))
					{
						this.RealLog = streamReader.ReadToEnd();
					}
				}
			}
			catch (Exception ex)
			{
				this.RealLog = "Error loading log file:\n" + ex.ToString();
			}
		}
		else
		{
			this.RealLog = "Log file not found: " + logFile;
		}
	}

	public void CopyClipboard()
	{
		GUIUtility.systemCopyBuffer = this._realLog;
	}

	public Scrollbar Scrollbar;

	public Text text;

	[NonSerialized]
	public string[] Log = new string[0];

	[NonSerialized]
	private string _realLog = string.Empty;

	public int CurrentLine;

	public int JumpSpeed = 4;

	public RectTransform OwnerPanel;

	public bool DisableUpdate;
}
