﻿using System;
using UnityEngine;

[Serializable]
public class ContractResult
{
	public ContractResult()
	{
	}

	public ContractResult(ContractWork work, bool cancelled, int bugs, float quality, int months, float progress)
	{
		this.Contract = work;
		this.MonthDiff = months - this.Contract.Months * GameSettings.DaysPerMonth;
		this.Date = SDateTime.Now();
		this.Bugs = Mathf.Max(0, bugs - 10);
		this.QualityResult = quality / this.Contract.Quality;
		if (cancelled)
		{
			this.BugPenalty = 0f;
			this.Income = 0f;
			this.LatePenalty = 0f;
			this.QualityPenalty = 0f;
			this.CancelPenalty = (float)this.Contract.Penalty;
			this.Status = ContractResult.ContractStatus.Cancelled;
			GameSettings.Instance.MyCompany.ChangeBusinessRep(-this.Contract.Months.MapRange(1f, 12f, 0.05f, 0.25f, false), "Contract", 1f);
			HUD.Instance.AddPopupMessage("ContractCancel".Loc(new object[]
			{
				this.Contract.Penalty.CurrencyInt(true)
			}), "Money", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Issue, new Color(0.7f, 0f, 0f, 1f), 0f, PopupManager.PopupIDs.None, 6);
		}
		else
		{
			this.Status = ContractResult.ContractStatus.Fulfilled;
			float num = Mathf.Pow(Mathf.Min(1f, progress), 3f);
			if (num < 1f)
			{
				this.Status = ContractResult.ContractStatus.Incomplete;
			}
			this.Income = Mathf.Round(num * (float)this.Contract.Done);
			this.BugPenalty = (float)this.Bugs * this.Contract.PerBug;
			this.LatePenalty = 0f;
			if (this.MonthDiff > 0)
			{
				this.LatePenalty = (float)this.Contract.Penalty;
				this.Status = ContractResult.ContractStatus.Late;
			}
			this.QualityPenalty = Mathf.Max(0f, (float)this.Contract.Done - this.Income);
			this.CancelPenalty = 0f;
			HUD.Instance.AddPopupMessage("ContractFinish3".Loc(new object[]
			{
				this.Status.ToString().Loc().ToLower(),
				this.Result.Currency(true),
				ContractWindow.QualityAssess(this.QualityResult).ToLower()
			}), "Money", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Good, (this.Result >= 0f) ? new Color32(50, 50, 50, byte.MaxValue) : new Color(0.7f, 0f, 0f, 1f), 0f, PopupManager.PopupIDs.None, 6);
			float num2 = this.Contract.GetDevTime() * (float)GameSettings.DaysPerMonth;
			float num3 = quality - this.Contract.Quality + 0.05f;
			num3 = Mathf.Sign(num3) * Mathf.Sqrt(num3) * this.Contract.Quality;
			float num4 = -((float)this.MonthDiff / num2) - (float)this.Bugs / Mathf.Max(2000f, num2 / (float)GameSettings.DaysPerMonth * SoftwareAlpha.BugLimitFactor) + num3 + 0.01f;
			GameSettings.Instance.MyCompany.ChangeBusinessRep(num4 * this.Contract.Months.MapRange(1f, 12f, 0.1f, 0.5f, false), "Contract", 1f);
		}
		GameSettings.Instance.MyCompany.MakeTransaction(this.Result, Company.TransactionCategory.Contracts, null);
	}

	public float Result
	{
		get
		{
			return this.Income - this.BugPenalty - this.LatePenalty - this.CancelPenalty;
		}
	}

	public float FinalResult
	{
		get
		{
			return this.Result + (float)this.Contract.Initial;
		}
	}

	public int ActualMonth
	{
		get
		{
			return this.Contract.Months + this.MonthDiff;
		}
	}

	public override string ToString()
	{
		return this.Contract.Company;
	}

	public readonly ContractWork Contract;

	public readonly ContractResult.ContractStatus Status;

	public readonly float BugPenalty;

	public readonly float Income;

	public readonly float LatePenalty;

	public readonly float QualityPenalty;

	public readonly float CancelPenalty;

	public readonly float QualityResult = 1f;

	public readonly int MonthDiff;

	public readonly int Bugs;

	public readonly SDateTime Date;

	public enum ContractStatus
	{
		Fulfilled,
		Late,
		Incomplete,
		Cancelled
	}
}
