﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedImage : MonoBehaviour
{
	private void Start()
	{
		this.img = base.GetComponent<RawImage>();
	}

	private void Update()
	{
		this.NextFrame -= Time.deltaTime;
		if (this.NextFrame <= 0f)
		{
			this.Frame = (this.Frame + 1) % this.Frames;
			if (this.Frame == this.Frames - 1)
			{
				this.NextFrame += this.Speed + this.LastFrameWait;
			}
			else
			{
				this.NextFrame += this.Speed;
			}
		}
		float num = 1f / (float)this.Cells;
		this.img.uvRect = new Rect((float)(this.Frame % this.Cells) * num, 1f - (float)(this.Frame / this.Cells) * num - num, num, num);
	}

	public int Cells;

	public int Frames;

	public int Frame;

	public float Speed;

	public float LastFrameWait;

	private float NextFrame;

	private RawImage img;
}
