﻿using System;
using System.Collections.Generic;

public interface IServerHost
{
	string ServerName { get; set; }

	List<IServerItem> Items { get; set; }

	float PowerSum { get; set; }

	float Available { get; set; }

	int Count { get; set; }

	IServerHost Fallback { get; set; }
}
