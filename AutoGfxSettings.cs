﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using SSAA;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CinematicEffects;
using UnityStandardAssets.ImageEffects;

public class AutoGfxSettings : MonoBehaviour
{
	public static int GetSetting()
	{
		int value = 0;
		bool flag = true;
		for (int i = 0; i < AutoGfxSettings.PublicCheck.Length; i++)
		{
			if (AutoGfxSettings.PublicCheck[i]())
			{
				if (!flag)
				{
					return -1;
				}
				value = i + 1;
			}
			else
			{
				flag = false;
			}
		}
		return Array.IndexOf<int>(AutoGfxSettings.DefaultSettings, value);
	}

	public static void SetLevel(int lvl)
	{
		for (int i = 0; i < AutoGfxSettings.PublicSteps.Length; i++)
		{
			AutoGfxSettings.PublicSteps[i](i < lvl);
		}
	}

	private void Start()
	{
		this.LastVsync = Options.VSync;
		this.LastTargetFrame = Options.TargetFrameRate;
		Options.VSync = 0;
		Options.TargetFrameRate = -1;
		base.StartCoroutine(this.WaitForGo());
	}

	private IEnumerator WaitForGo()
	{
		yield return new WaitForSeconds(1f);
		this.Label.text = "Rendering...";
		this.lastDelta = Time.realtimeSinceStartup;
		this.Started = true;
		yield break;
	}

	private void Update()
	{
		if (!this.Started)
		{
			return;
		}
		this.FPSMax += Time.realtimeSinceStartup - this.lastDelta;
		this.lastDelta = Time.realtimeSinceStartup;
		this.CurrentFPSCount++;
		if (this.CurrentFPSCount >= this.FPSCounts)
		{
			this.avgFPS.Add(1f / (this.FPSMax / (float)this.CurrentFPSCount));
			this.CurrentFPSCount = 0;
			this.FPSMax = 0f;
			this.CurrentStep++;
			this.UpdateLabel();
			if (this.CurrentStep < AutoGfxSettings.LocalSteps.Length)
			{
				if (this.avgFPS[this.avgFPS.Count - 1] < 90f)
				{
					this.BreakNow();
				}
				else
				{
					AutoGfxSettings.LocalSteps[this.CurrentStep](this);
				}
			}
			else
			{
				Options.VSync = this.LastVsync;
				Options.TargetFrameRate = this.LastTargetFrame;
				this.ChangeSettings();
				this.Started = false;
				base.StartCoroutine(this.Finish());
			}
		}
	}

	private void BreakNow()
	{
		Text label = this.Label;
		label.text += "FPS too low, stopping test";
		for (int i = this.avgFPS.Count; i <= AutoGfxSettings.PublicSteps.Length; i++)
		{
			this.avgFPS.Add(0f);
		}
		Options.VSync = this.LastVsync;
		Options.TargetFrameRate = this.LastTargetFrame;
		this.ChangeSettings();
		this.Started = false;
		base.StartCoroutine(this.Finish());
	}

	private IEnumerator Finish()
	{
		yield return new WaitForSeconds(2f);
		ErrorLogging.SceneChanging = true;
		SceneManager.LoadScene("MainMenu");
		yield break;
	}

	private void ChangeSettings()
	{
		int num = 0;
		for (int i = 1; i < this.avgFPS.Count; i++)
		{
			if (this.avgFPS[i] > 110f)
			{
				num = i;
			}
		}
		for (int j = 0; j < AutoGfxSettings.PublicSteps.Length; j++)
		{
			AutoGfxSettings.PublicSteps[j](j < num);
		}
	}

	private void UpdateLabel()
	{
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i <= this.CurrentStep; i++)
		{
			stringBuilder.AppendLine(AutoGfxSettings.StepNames[i] + " - " + this.avgFPS[i].ToString("F0") + " FPS");
		}
		this.Label.text = stringBuilder.ToString();
	}

	static AutoGfxSettings()
	{
		// Note: this type is marked as 'beforefieldinit'.
		Func<bool>[] array = new Func<bool>[14];
		int num = 0;
		if (AutoGfxSettings.<>f__mg$cache0 == null)
		{
			AutoGfxSettings.<>f__mg$cache0 = new Func<bool>(Options.get_Bloom);
		}
		array[num] = AutoGfxSettings.<>f__mg$cache0;
		int num2 = 1;
		if (AutoGfxSettings.<>f__mg$cache1 == null)
		{
			AutoGfxSettings.<>f__mg$cache1 = new Func<bool>(Options.get_TiltShift);
		}
		array[num2] = AutoGfxSettings.<>f__mg$cache1;
		int num3 = 2;
		if (AutoGfxSettings.<>f__mg$cache2 == null)
		{
			AutoGfxSettings.<>f__mg$cache2 = new Func<bool>(Options.get_AmbientOcclusion);
		}
		array[num3] = AutoGfxSettings.<>f__mg$cache2;
		int num4 = 3;
		if (AutoGfxSettings.<>f__mg$cache3 == null)
		{
			AutoGfxSettings.<>f__mg$cache3 = new Func<bool>(Options.get_FXAA);
		}
		array[num4] = AutoGfxSettings.<>f__mg$cache3;
		int num5 = 4;
		if (AutoGfxSettings.<>f__mg$cache4 == null)
		{
			AutoGfxSettings.<>f__mg$cache4 = new Func<bool>(Options.get_SMAA);
		}
		array[num5] = AutoGfxSettings.<>f__mg$cache4;
		int num6 = 5;
		if (AutoGfxSettings.<>f__mg$cache5 == null)
		{
			AutoGfxSettings.<>f__mg$cache5 = new Func<bool>(Options.get_Shadows);
		}
		array[num6] = AutoGfxSettings.<>f__mg$cache5;
		array[6] = (() => Options.Shadows && Options.ShadowQuality == 3);
		int num7 = 7;
		if (AutoGfxSettings.<>f__mg$cache6 == null)
		{
			AutoGfxSettings.<>f__mg$cache6 = new Func<bool>(Options.get_SSR);
		}
		array[num7] = AutoGfxSettings.<>f__mg$cache6;
		array[8] = (() => Options.SSAA >= 12);
		array[9] = (() => Options.SSAA >= 14);
		int num8 = 10;
		if (AutoGfxSettings.<>f__mg$cache7 == null)
		{
			AutoGfxSettings.<>f__mg$cache7 = new Func<bool>(Options.get_MoreShadow);
		}
		array[num8] = AutoGfxSettings.<>f__mg$cache7;
		array[11] = (() => Options.SSAA >= 16);
		array[12] = (() => Options.SSAA >= 18);
		array[13] = (() => Options.SSAA >= 20);
		AutoGfxSettings.PublicCheck = array;
		AutoGfxSettings.LocalSteps = new Action<AutoGfxSettings>[]
		{
			delegate(AutoGfxSettings o)
			{
				o.Bloom.enabled = true;
			},
			delegate(AutoGfxSettings o)
			{
				o.TiltScript.enabled = true;
			},
			delegate(AutoGfxSettings o)
			{
				o.SSAO.enabled = true;
			},
			delegate(AutoGfxSettings o)
			{
				o.AntiAlias.enabled = true;
			},
			delegate(AutoGfxSettings o)
			{
				o.SMAA.enabled = true;
			},
			delegate(AutoGfxSettings o)
			{
				Options.ShadowQuality = 0;
				o.MainLight.shadows = LightShadows.Soft;
			},
			delegate(AutoGfxSettings o)
			{
				Options.ShadowQuality = 3;
			},
			delegate(AutoGfxSettings o)
			{
				o.SSR.enabled = true;
			},
			delegate(AutoGfxSettings o)
			{
				o.SSAAObj.enabled = true;
			},
			delegate(AutoGfxSettings o)
			{
				internal_SSAA.ChangeScale(1.4f);
			},
			delegate(AutoGfxSettings o)
			{
				o.Lights.ForEachEnum(delegate(Light x)
				{
					x.shadows = LightShadows.Hard;
				});
			},
			delegate(AutoGfxSettings o)
			{
				internal_SSAA.ChangeScale(1.6f);
			},
			delegate(AutoGfxSettings o)
			{
				internal_SSAA.ChangeScale(1.8f);
			},
			delegate(AutoGfxSettings o)
			{
				internal_SSAA.ChangeScale(2f);
			}
		};
		AutoGfxSettings.PublicSteps = new Action<bool>[]
		{
			delegate(bool v)
			{
				Options.Bloom = v;
			},
			delegate(bool v)
			{
				Options.TiltShift = v;
			},
			delegate(bool v)
			{
				Options.AmbientOcclusion = v;
			},
			delegate(bool v)
			{
				Options.FXAA = v;
			},
			delegate(bool v)
			{
				Options.SMAA = v;
			},
			delegate(bool v)
			{
				if (v)
				{
					Options.Shadows = true;
					Options.ShadowQuality = 0;
				}
				else
				{
					Options.Shadows = false;
				}
			},
			delegate(bool v)
			{
				if (v)
				{
					Options.Shadows = true;
					Options.ShadowQuality = 3;
				}
			},
			delegate(bool v)
			{
				Options.SSR = v;
			},
			delegate(bool v)
			{
				if (v)
				{
					Options.SSAA = 12;
				}
				else
				{
					Options.SSAA = 10;
				}
			},
			delegate(bool v)
			{
				if (v)
				{
					Options.SSAA = 14;
				}
			},
			delegate(bool v)
			{
				Options.MoreShadow = v;
			},
			delegate(bool v)
			{
				if (v)
				{
					Options.SSAA = 16;
				}
			},
			delegate(bool v)
			{
				if (v)
				{
					Options.SSAA = 18;
				}
			},
			delegate(bool v)
			{
				if (v)
				{
					Options.SSAA = 20;
				}
			}
		};
	}

	public Antialiasing AntiAlias;

	public BloomOptimized Bloom;

	public ScreenSpaceReflection SSR;

	public AntiAliasing SMAA;

	public SuperSampling_SSAA SSAAObj;

	public SSAOPro SSAO;

	public TiltShift TiltScript;

	public Light[] Lights;

	public Light MainLight;

	public Text Label;

	public int CurrentStep = -1;

	private int LastVsync;

	private int LastTargetFrame;

	private List<float> avgFPS = new List<float>();

	private float FPSMax;

	private float lastDelta;

	public int FPSCounts = 10;

	private int CurrentFPSCount;

	private bool Started;

	public static int[] DefaultSettings = new int[]
	{
		0,
		4,
		7,
		10,
		14
	};

	public static readonly string[] StepNames = new string[]
	{
		"Nothing",
		"Bloom",
		"Tilt shift",
		"SSAO",
		"Simple AA",
		"Complex AA",
		"Low quality shadows",
		"High quality shadows",
		"Reflections",
		"Resolution x 1.2",
		"Resolution x 1.4",
		"More shadows",
		"Resolution x 1.6",
		"Resolution x 1.8",
		"Resolution x 2"
	};

	public static readonly Func<bool>[] PublicCheck;

	public static readonly Action<AutoGfxSettings>[] LocalSteps;

	public static readonly Action<bool>[] PublicSteps;

	[CompilerGenerated]
	private static Func<bool> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<bool> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<bool> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<bool> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<bool> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<bool> <>f__mg$cache5;

	[CompilerGenerated]
	private static Func<bool> <>f__mg$cache6;

	[CompilerGenerated]
	private static Func<bool> <>f__mg$cache7;
}
