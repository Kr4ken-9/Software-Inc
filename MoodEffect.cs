﻿using System;

[Serializable]
public class MoodEffect
{
	public MoodEffect(XMLParser.XMLNode node)
	{
		this.Negative = node.Name.Equals("Negative");
		this.Thought = node.GetNode("Thought", true).Value;
		this.StartValue = node.GetNodeValue("Start", null).ConvertToFloat("MoodEffect.StartValue");
		this.Increment = node.GetNodeValue("Inc", null).ConvertToFloat("MoodEffect.Increment");
		this.Decrement = node.GetNodeValue("Dec", null).ConvertToFloat("MoodEffect.Decrement");
		this.Max = node.GetNodeValue("Max", null).ConvertToFloat("MoodEffect.Max");
		this.CutOff = node.GetNodeValue("CutOff", "1").ConvertToFloat("MoodEffect.CutOff");
		this.WarningThreshold = node.GetNodeValue("WarnThresh", "0").ConvertToFloat("MoodEffect.WarningThreshold");
		this.Warning = node.GetNodeValue("Warn", null);
		this.QuitReason = node.GetNodeValue("QuitReason", null);
		this.CounterMood = node.GetNodeValue("CounterMood", null);
	}

	public override string ToString()
	{
		return this.Thought;
	}

	public string Thought;

	public bool Negative;

	public float StartValue;

	public float Increment;

	public float Decrement;

	public float Max;

	public float CutOff;

	public float WarningThreshold;

	public string QuitReason;

	public string Warning;

	public string CounterMood;
}
