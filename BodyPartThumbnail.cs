﻿using System;
using UnityEngine;

public class BodyPartThumbnail : MonoBehaviour
{
	public Transform Face;

	public Transform Leg;

	public Transform Torso;
}
