﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TextLoc : MonoBehaviour
{
	private void OnEnable()
	{
		if (this.localized)
		{
			return;
		}
		this.Original = base.GetComponent<Text>().text;
		this.LocalizeThis();
		this.localized = true;
		UnityEngine.Object.Destroy(this);
	}

	public void LocalizeThis()
	{
		string text = this.Original.Trim().Loc();
		if (this.Caps)
		{
			text = text.ToUpper();
		}
		if (this.Colon)
		{
			text += ":";
		}
		base.GetComponent<Text>().text = text;
	}

	public bool Colon;

	public bool Caps;

	[NonSerialized]
	private string Original;

	[NonSerialized]
	private bool localized;
}
