﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class HUD : MonoBehaviour
{
	public int GameSpeed
	{
		get
		{
			return Array.IndexOf<int>(HUD.speeds, (int)GameSettings.GameSpeed);
		}
		set
		{
			if (GameSettings.Instance != null && GameSettings.Instance.EditMode)
			{
				value = 0;
			}
			if (value > -1 && value < HUD.speeds.Length)
			{
				if (value > 0 && this.BuildMode)
				{
					this.BuildMode = false;
				}
				if (GameSettings.GameSpeed != (float)HUD.speeds[value] && !GameSettings.ForcePause)
				{
					switch (value)
					{
					case 0:
						UISoundFX.PlaySFX("Pause", -1f, 0f);
						break;
					case 1:
						UISoundFX.PlaySFX("NormalSpeed", -1f, 0f);
						break;
					case 2:
						UISoundFX.PlaySFX("FastSpeed", -1f, 0f);
						break;
					case 3:
						UISoundFX.PlaySFX("FasterSpeed", -1f, 0f);
						break;
					}
				}
				GameSettings.GameSpeed = (float)HUD.speeds[value];
			}
		}
	}

	public bool BuildMode
	{
		get
		{
			return this.buildMode;
		}
		set
		{
			if (GameSettings.Instance != null && GameSettings.Instance.EditMode)
			{
				if (this.BuildMode)
				{
					return;
				}
				value = true;
			}
			if (this.buildMode != value)
			{
				if (value)
				{
					UISoundFX.ChangeMusicState("BuildMode");
					UISoundFX.PlaySFX("BuildModeEnter", -1f, 0f);
				}
				else
				{
					UISoundFX.ChangeMusicState("MainScene");
					string state = (new string[]
					{
						"Spring",
						"Summer",
						"Autumn",
						"Winter"
					})[TimeOfDay.Instance.Month / 3];
					UISoundFX.ChangeMusicState(state);
					UISoundFX.PlaySFX("BuildModeExit", -1f, 0f);
				}
			}
			bool forcePause = value;
			GameSettings.ForcePause = forcePause;
			this.buildMode = forcePause;
			if (GameSettings.Instance != null)
			{
				GameSettings.Instance.UpdateGridVisibility();
				if (this.buildMode)
				{
					this.BuildModeMainButtons[0].SetActive(GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode);
					this.BuildModeMainButtons[2].SetActive(GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode);
					this.BuildModeMainButtons[3].SetActive(GameSettings.Instance.EditMode);
					this.BuildModeMainButtons[4].SetActive(!GameSettings.Instance.EditMode && !GameSettings.Instance.RentMode);
					this.BuildModeMainButtons[5].SetActive(GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode);
					this.BuildModeMainButtons[6].SetActive(GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode);
					this.BuildModeMainButtonPanel.SetActive(GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode);
				}
				if (!GameSettings.Instance.EditMode && GameSettings.Instance.RentMode)
				{
					this.SetBuildType(1);
				}
				GameSettings.Instance.UndoButton.SetActive(this.buildMode && GameSettings.Instance.UndoCount > 0);
				this.PlotHolder.SetActive(value);
			}
			if (AudioVisualizer.Instance != null)
			{
				AudioVisualizer.Instance.ForceRedraw();
			}
			this.MainWorkItemPanel.gameObject.SetActive(!value);
			for (int i = 0; i < this.HideInBuild.Length; i++)
			{
				this.HideInBuild[i].SetActive(!value);
			}
			this.UpdateBorderOverlay();
			this.popupManager.gameObject.SetActive(!value);
			for (int j = 0; j < this.TopPanels.Length; j++)
			{
				this.TopPanels[j].SetActive(!value);
			}
			this.DayLabel.transform.parent.gameObject.SetActive(!value && GameSettings.DaysPerMonth > 1);
			if (value)
			{
				this.newspaper.ShowNow(false);
				SpecializationProgress.Instance.Hide();
				WindowManager.DisableAll(false, TutorialSystem.Instance.Window);
				if (GameSettings.Instance.EditMode)
				{
					TutorialSystem.Instance.StartTutorial("Custom Map", false);
				}
				else
				{
					TutorialSystem.Instance.StartTutorial((!GameSettings.Instance.RentMode) ? "Build Mode" : "Leasing", false);
				}
				GameSettings.ForcePause = true;
				SDateTime sdateTime = SDateTime.Now();
				float num = (float)sdateTime.Hour;
				int num2 = sdateTime.Minute;
				if (num >= 14f)
				{
					num = 14f - (num - 14f) * 1.4f;
					num2 = -num2;
				}
				float value2 = (num + (float)num2 / 60f) / 14f;
				this.SunSlider.value = value2;
			}
			else
			{
				WindowManager.EnableAll();
				BuildController.Instance.ClearBuild(false, false, false, false);
				GameSettings.ForcePause = false;
				if (CameraScript.Instance != null)
				{
					CameraScript.Instance.TopDown = false;
				}
			}
		}
	}

	private void OnDestroy()
	{
		if (HUD.Instance == this)
		{
			HUD.Instance = null;
		}
	}

	public void OnWorkItemToggle()
	{
		if (SelectorController.Instance != null)
		{
			SelectorController.Instance.UpdateTeamSelection(this.SelectionFilterToggle.isOn);
		}
		this.AllWorkTogglesOff = true;
		for (int i = 0; i < this.WorkItemToggles.Length - 1; i++)
		{
			if (this.WorkItemToggles[i].isOn)
			{
				this.AllWorkTogglesOff = false;
			}
		}
		foreach (WorkItem workItem in GameSettings.Instance.MyCompany.WorkItems)
		{
			if (workItem.guiItem != null)
			{
				workItem.guiItem.UpdateActivation();
			}
		}
		this.WorkItemScroll.value = 1f;
		NoDragScrollRect component = this.MainWorkItemPanel.GetComponent<NoDragScrollRect>();
		component.OnChange(component.normalizedPosition);
	}

	private void Start()
	{
		if (Options.MainPanelOffset != null)
		{
			this.MainContentPanel.offsetMin = new Vector2(Options.MainPanelOffset.Value.x, 0f);
			this.MainContentPanel.offsetMax = new Vector2(-Options.MainPanelOffset.Value.y, 0f);
			this.MainContentPanel.GetComponent<Image>().enabled = true;
			Mask mask = this.MainContentPanel.gameObject.AddComponent<Mask>();
			mask.showMaskGraphic = false;
		}
		this.WorkToToggle = new Dictionary<string, Toggle>();
		for (int i = 0; i < this.WorkItemToggles.Length; i++)
		{
			this.WorkToToggle[this.WorkItemNames[i]] = this.WorkItemToggles[i];
		}
		this.Aud = base.GetComponent<AudioSource>();
		if (HUD.Instance == null)
		{
			HUD.Instance = this;
			this.AllFurniture = (from x in ObjectDatabase.Instance.GetAllFurniture()
			select x.GetComponent<Furniture>()).ToList<Furniture>();
			this.InitializeBuildMode();
			this.BuildMode = false;
			this.UpdateIdleCounter();
			this.CashflowChart.Values = new List<List<float>>
			{
				new List<float>
				{
					0f,
					0f,
					0f,
					0f,
					0f
				}
			};
			return;
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public bool GetWorkTypeToggled(string type)
	{
		if (this.AllWorkTogglesOff)
		{
			return true;
		}
		Toggle orNull = this.WorkToToggle.GetOrNull(type);
		return orNull == null || orNull.isOn;
	}

	public void SetBorderOverlayPanel(string type = null, string icon = null, Color color = default(Color), bool showMoney = true)
	{
		if (type == null || GameSettings.Instance == null)
		{
			this.BuildModePanel.SetActive(false);
		}
		else
		{
			this.BuildModeIcon.sprite = IconManager.GetIcon(icon);
			this.BuildModeIcon.color = color.Alpha(1f);
			this.BuildModeTypeText.text = type.Loc();
			this.BuildModePanelImage.color = color;
			this.BuildMoneyLabel.gameObject.SetActive(showMoney && !GameSettings.Instance.EditMode);
			this.BuildModePanel.SetActive(true);
		}
	}

	public void UpdateFurnitureButtons()
	{
		foreach (KeyValuePair<Button, BuildDescriptor> keyValuePair in from x in this.BuildButtons
		where x.Value.Type == BuildDescriptor.BuildType.Furniture
		select x)
		{
			if (!keyValuePair.Value.Furniture.IsPlayerControlled())
			{
				if (keyValuePair.Key.interactable)
				{
					keyValuePair.Key.interactable = false;
				}
			}
			else if (Cheats.UnlockFurn || GameSettings.Instance.EditMode || TimeOfDay.Instance.Year + SDateTime.BaseYear >= keyValuePair.Value.Furniture.UnlockYear)
			{
				if (!keyValuePair.Key.interactable)
				{
					keyValuePair.Key.interactable = true;
				}
			}
			else if (keyValuePair.Key.interactable)
			{
				keyValuePair.Key.interactable = false;
			}
		}
	}

	public void ShowIdleEmployees()
	{
		this.employeeWindow.Show(from x in this._idleEmployees
		where x != null && x.gameObject != null
		select x);
	}

	public void RemoveFromIdle(Actor actor)
	{
		this._idleEmployees.Remove(actor);
		this.UpdateIdleCounter();
	}

	public void AddToIdle(Actor actor)
	{
		this._idleEmployees.Add(actor);
		this.UpdateIdleCounter();
	}

	private void UpdateIdleCounter()
	{
		this.IdleCounter.text = this._idleEmployees.Count.ToString();
	}

	public GUIWorkItem SpawnWorkItem(WorkItem work, int type)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.WorkItemPrefab[type]);
		GUIWorkItem component = gameObject.GetComponent<GUIWorkItem>();
		component.work = work;
		component.transform.SetParent(this.WorkItemPanel, false);
		component.InitPos();
		return component;
	}

	private void AddCustomBuildButton(BuildDescriptor.BuildType bt, string realName, string name, string desc, float price, string category, string funcCat, string search, Action OnClick, Sprite thumb)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.BuyButtonPrefab);
		gameObject.transform.SetParent(this.BuildButtonPanel.transform, false);
		BuildDescriptor value = new BuildDescriptor(bt, funcCat, search, null, new string[]
		{
			category
		});
		Button component = gameObject.GetComponent<Button>();
		component.onClick.AddListener(delegate
		{
			OnClick();
		});
		BuildButton component2 = gameObject.GetComponent<BuildButton>();
		component2.SetAttributes(realName, name, desc, thumb, price);
		component2.ButtonImage.sprite = thumb;
		this.BuildButtons.Add(component, value);
	}

	private bool Match(BuildDescriptor bd, BuildDescriptor.CategoryType function)
	{
		if (function == BuildDescriptor.CategoryType.Room && bd.Category.Length == 0)
		{
			return false;
		}
		if (bd.Type != BuildDescriptor.BuildType.Furniture && function == BuildDescriptor.CategoryType.Function)
		{
			return false;
		}
		if (function == BuildDescriptor.CategoryType.Function || bd.Category.Length == 1)
		{
			HUD.CategoryKey key = new HUD.CategoryKey(bd.Type, (function != BuildDescriptor.CategoryType.Function) ? bd.Category[0] : bd.FunctionalCategory, function);
			Toggle toggle;
			return this.BuildCategories.TryGetValue(key, out toggle) && toggle.isOn;
		}
		for (int i = 0; i < bd.Category.Length; i++)
		{
			string category = bd.Category[i];
			HUD.CategoryKey key2 = new HUD.CategoryKey(bd.Type, category, BuildDescriptor.CategoryType.Room);
			Toggle toggle2;
			if (this.BuildCategories.TryGetValue(key2, out toggle2) && toggle2.isOn)
			{
				return true;
			}
		}
		return false;
	}

	private void AddCategory(BuildDescriptor.BuildType bt, string cat, BuildDescriptor.CategoryType function)
	{
		HUD.CategoryKey key = new HUD.CategoryKey(bt, cat, function);
		if (this.BuildCategories.ContainsKey(key))
		{
			return;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.TogglePrefab);
		gameObject.GetComponentInChildren<Text>().text = cat.LocTry();
		Toggle component = gameObject.GetComponent<Toggle>();
		component.onValueChanged.AddListener(delegate(bool x)
		{
			this.UpdateCatFilter(bt, function);
		});
		component.group = this.CategoryPanel.GetComponent<ToggleGroup>();
		gameObject.transform.SetParent(this.CategoryPanel.transform, false);
		this.BuildCategories.Add(key, component);
	}

	private void InitializeBuildMode()
	{
		foreach (string cat in (from x in ObjectDatabase.Instance.RoomSegments
		select x.GetComponent<RoomSegment>().Type).Distinct<string>())
		{
			this.AddCategory(BuildDescriptor.BuildType.Construction, cat, BuildDescriptor.CategoryType.Room);
		}
		foreach (Furniture furniture in this.AllFurniture)
		{
			if (furniture.Category.Length > 0 && furniture.Category[0].Equals("Construction"))
			{
				this.AddCategory(BuildDescriptor.BuildType.Construction, furniture.Type, BuildDescriptor.CategoryType.Room);
			}
			else
			{
				for (int i = 0; i < furniture.Category.Length; i++)
				{
					string cat2 = furniture.Category[i];
					this.AddCategory(BuildDescriptor.BuildType.Furniture, cat2, BuildDescriptor.CategoryType.Room);
				}
				this.AddCategory(BuildDescriptor.BuildType.Furniture, furniture.FunctionCategory, BuildDescriptor.CategoryType.Function);
			}
		}
		this.AddCategory(BuildDescriptor.BuildType.Construction, "Wall", BuildDescriptor.CategoryType.Room);
		this.AddCategory(BuildDescriptor.BuildType.Construction, "Fence", BuildDescriptor.CategoryType.Room);
		this.AddCategory(BuildDescriptor.BuildType.Roads, "Road", BuildDescriptor.CategoryType.Room);
		this.AddCategory(BuildDescriptor.BuildType.Roads, "Parking", BuildDescriptor.CategoryType.Room);
		int num = 0;
		foreach (KeyValuePair<HUD.CategoryKey, Toggle> keyValuePair in from x in this.BuildCategories
		orderby x.Key.Category.LocTry()
		select x)
		{
			keyValuePair.Value.transform.SetSiblingIndex(num);
			num++;
		}
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Construction, "Wall", "Free angle rooms".Loc(), "ContructRoomDesc".Loc(new object[]
		{
			BuildController.WallPrice.Currency(true),
			BuildController.RoomPrice.Currency(true),
			string.Empty,
			"RDescRooms".Loc(),
			"RDescRoomWalls".Loc(),
			"RDescRoomSq".Loc()
		}), BuildController.RoomPrice + BuildController.WallPrice * 4f, "Wall", null, "Wall".LocTry() + "Room".LocTry(), delegate
		{
			BuildController.Instance.ActivateBuildMode(false);
		}, this.RoomThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Construction, "RectangleWall", "Rectangle rooms".Loc(), "ContructRoomDesc".Loc(new object[]
		{
			BuildController.WallPrice.Currency(true),
			BuildController.RoomPrice.Currency(true),
			"RDescRectangle".Loc(),
			"RDescRooms".Loc(),
			"RDescRoomWalls".Loc(),
			"RDescRoomSq".Loc()
		}), BuildController.RoomPrice + BuildController.WallPrice * 4f, "Wall", null, "Wall".LocTry() + "Room".LocTry(), delegate
		{
			BuildController.Instance.BeginRectBuilding(false);
		}, this.RectRoomThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Construction, "DestroyWall", "Destroy Wall".Loc(), "Tear down walls between rooms".Loc(), 0f, "Wall", null, "Fence".LocTry() + "Wall".LocTry() + "Room".LocTry() + "Destroy Wall".LocTry(), delegate
		{
			WallRemovalTool.Instance.Show();
		}, this.RemoveWallThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Construction, "Fence", "Free angle outdoor areas".Loc(), "ContructRoomDesc".Loc(new object[]
		{
			BuildController.WallPrice.Currency(true),
			BuildController.RoomPrice.Currency(true),
			string.Empty,
			"RDescOutdoorAreas".Loc(),
			"RDescOutdoorFence".Loc(),
			"RDescOutdoorSq".Loc()
		}), BuildController.OutdoorPrice + BuildController.FencePrice * 4f, "Fence", null, "Fence".LocTry() + "Room".LocTry(), delegate
		{
			BuildController.Instance.ActivateBuildMode(true);
		}, this.FenceThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Construction, "RectangleFence", "Rectangle outdoor areas".Loc(), "ContructRoomDesc".Loc(new object[]
		{
			BuildController.WallPrice.Currency(true),
			BuildController.RoomPrice.Currency(true),
			"RDescRectangle".Loc(),
			"RDescOutdoorAreas".Loc(),
			"RDescOutdoorFence".Loc(),
			"RDescOutdoorSq".Loc()
		}), BuildController.OutdoorPrice + BuildController.FencePrice * 4f, "Fence", null, "Fence".LocTry() + "Room".LocTry(), delegate
		{
			BuildController.Instance.BeginRectBuilding(true);
		}, this.RectFenceThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Roads, "RemoveRoad", "Remove road".Loc(), string.Empty, RoadBuildCube.RoadCost, "Road", null, "Destroy".LocTry() + "Remove".LocTry(), delegate
		{
			RoadBuildCube.Instance.Type = 0;
			RoadBuildCube.Instance.gameObject.SetActive(true);
		}, this.NoRoadThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Roads, "BuildRoad", "Build road".Loc(), string.Empty, RoadBuildCube.RoadCost, "Road", null, "Road".LocTry() + "Normal".LocTry(), delegate
		{
			RoadBuildCube.Instance.Type = 1;
			RoadBuildCube.Instance.gameObject.SetActive(true);
		}, this.RoadThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Roads, "HorizontalParking", "Horizontal parking".Loc(), string.Empty, RoadBuildCube.RoadCost, "Parking", null, "Parking".LocTry() + "Horizontal".LocTry(), delegate
		{
			RoadBuildCube.Instance.Type = 2;
			RoadBuildCube.Instance.gameObject.SetActive(true);
			RoadBuildCube.Instance.ShowArrow(0f);
		}, this.ParkHorThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Roads, "VerticalParking", "Vertical parking".Loc(), string.Empty, RoadBuildCube.RoadCost, "Parking", null, "Parking".LocTry() + "Vertical".LocTry(), delegate
		{
			RoadBuildCube.Instance.Type = 3;
			RoadBuildCube.Instance.gameObject.SetActive(true);
			RoadBuildCube.Instance.ShowArrow(90f);
		}, this.ParkVertThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Environment, "Houses", "Houses".Loc(), "HouseEditToolDesc".Loc(), 0f, "Environment", null, "Environment".LocTry() + "Houses".LocTry(), delegate
		{
			EnvironmentEditor.Instance.Show(EnvironmentEditor.EditorType.House);
		}, this.HouseThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Environment, "Skyscrapers", "Skyscrapers".Loc(), "SkyscraperEditToolDesc".Loc(), 0f, "Environment", null, "Environment".LocTry() + "Skyscrapers".LocTry(), delegate
		{
			EnvironmentEditor.Instance.Show(EnvironmentEditor.EditorType.Skyscraper);
		}, this.SkyscraperThumbnail);
		this.AddCustomBuildButton(BuildDescriptor.BuildType.Environment, "Trees", "Trees".Loc(), "TreeEditToolDesc".Loc(), 0f, "Environment", null, "Environment".LocTry() + "Trees".LocTry(), delegate
		{
			EnvironmentEditor.Instance.Show(EnvironmentEditor.EditorType.Trees);
		}, this.TreeThumbnail);
		foreach (Furniture furniture2 in this.AllFurniture)
		{
			this.AddFurnitureButton(furniture2.gameObject);
		}
		foreach (RoomSegment roomSegment in from x in ObjectDatabase.Instance.RoomSegments
		select x.GetComponent<RoomSegment>())
		{
			string[] furniture3 = Localization.GetFurniture(roomSegment.name, roomSegment.Desc);
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.BuyButtonPrefab);
			gameObject.transform.SetParent(this.BuildButtonPanel.transform, false);
			BuildDescriptor value = new BuildDescriptor(BuildDescriptor.BuildType.Construction, null, roomSegment.Type.LocTry() + furniture3[0], null, new string[]
			{
				roomSegment.Type
			});
			Button component = gameObject.GetComponent<Button>();
			RoomSegment localSegment = roomSegment;
			component.onClick.AddListener(delegate
			{
				BuildController.Instance.BeginSegmentBuild(localSegment.gameObject);
			});
			BuildButton component2 = gameObject.GetComponent<BuildButton>();
			component2.SetAttributes(roomSegment.name, furniture3[0], furniture3[1], roomSegment.Thumbnail, roomSegment.Cost);
			component2.ButtonImage.sprite = roomSegment.Thumbnail;
			this.BuildButtons.Add(component, value);
		}
		this.LastType = BuildDescriptor.BuildType.Roads;
		this.SetBuildType(0);
	}

	public void AddFurnitureButton(GameObject furniture)
	{
		Furniture component = furniture.GetComponent<Furniture>();
		if (component.Category.Length > 0 && component.Category[0].Equals("Construction"))
		{
			string[] furniture2 = Localization.GetFurniture(component.name, component.ButtonDescription);
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.BuyButtonPrefab);
			gameObject.name = "BuildButton" + component.name;
			gameObject.transform.SetParent(this.BuildButtonPanel.transform, false);
			BuildDescriptor value = new BuildDescriptor(BuildDescriptor.BuildType.Construction, null, component.Type.LocTry() + furniture2[0], null, new string[]
			{
				component.Type
			});
			Button component2 = gameObject.GetComponent<Button>();
			Furniture localSegment = component;
			component2.onClick.AddListener(delegate
			{
				BuildController.Instance.BeginBuildFurniture(localSegment.gameObject);
			});
			BuildButton component3 = gameObject.GetComponent<BuildButton>();
			component3.furn = component;
			component3.ButtonImage.sprite = component.Thumbnail;
			this.BuildButtons.Add(component2, value);
		}
		else
		{
			string[] furniture3 = Localization.GetFurniture(component.name, component.ButtonDescription);
			GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.BuyButtonPrefab);
			gameObject2.name = "BuildButton" + component.name;
			gameObject2.transform.SetParent(this.BuildButtonPanel.transform, false);
			BuildDescriptor.BuildType type = BuildDescriptor.BuildType.Furniture;
			string functionCategory = component.FunctionCategory;
			string empty = string.Empty;
			IList<string> category = component.Category;
			if (HUD.<>f__mg$cache0 == null)
			{
				HUD.<>f__mg$cache0 = new Func<string, string>(Localization.LocTry);
			}
			BuildDescriptor value2 = new BuildDescriptor(type, functionCategory, string.Join(empty, category.SelectInPlace(HUD.<>f__mg$cache0)) + component.FunctionCategory.LocTry() + component.Type.LocTry() + furniture3[0], component, component.Category);
			Button component4 = gameObject2.GetComponent<Button>();
			Furniture localFurn = component;
			BuildButton component5 = gameObject2.GetComponent<BuildButton>();
			component5.furn = component;
			component5.ButtonImage.sprite = component.Thumbnail;
			component4.onClick.AddListener(delegate
			{
				BuildController.Instance.BeginBuildFurniture(localFurn.gameObject);
			});
			this.BuildButtons.Add(component4, value2);
		}
	}

	public void RemoveFurnitureButton(GameObject furn)
	{
		foreach (KeyValuePair<Button, BuildDescriptor> keyValuePair in this.BuildButtons.ToList<KeyValuePair<Button, BuildDescriptor>>())
		{
			if (keyValuePair.Value.Furniture == furn.GetComponent<Furniture>())
			{
				this.BuildButtons.Remove(keyValuePair.Key);
				UnityEngine.Object.Destroy(keyValuePair.Key.gameObject);
				break;
			}
		}
	}

	private void UpdateCatFilter(BuildDescriptor.BuildType type, BuildDescriptor.CategoryType cType)
	{
		this.ClearSearch();
		bool flag = (from x in this.BuildCategories
		where x.Key.Match(type, cType)
		select x).All((KeyValuePair<HUD.CategoryKey, Toggle> x) => !x.Value.isOn);
		foreach (KeyValuePair<Button, BuildDescriptor> keyValuePair in this.BuildButtons)
		{
			if (keyValuePair.Value.Type == type)
			{
				bool active = flag || this.Match(keyValuePair.Value, cType);
				keyValuePair.Key.gameObject.SetActive(active);
			}
			else
			{
				keyValuePair.Key.gameObject.SetActive(false);
			}
		}
		this.BuildMenuScroll.value = 1f;
	}

	public void SetBuildType(int type)
	{
		this.ClearSearch();
		this.FurnitureCategoryPanel.SetActive(type == 1);
		this.LastType = (BuildDescriptor.BuildType)type;
		this.BuildScrollBar.value = 1f;
		foreach (KeyValuePair<HUD.CategoryKey, Toggle> keyValuePair in this.BuildCategories)
		{
			keyValuePair.Value.gameObject.SetActive(keyValuePair.Key.Match((BuildDescriptor.BuildType)type, BuildDescriptor.CategoryType.Room));
		}
		this.UpdateCatFilter((BuildDescriptor.BuildType)type, BuildDescriptor.CategoryType.Room);
	}

	public void SetCatType(int type)
	{
		this.BuildScrollBar.value = 1f;
		foreach (KeyValuePair<HUD.CategoryKey, Toggle> keyValuePair in this.BuildCategories)
		{
			keyValuePair.Value.gameObject.SetActive(keyValuePair.Key.Match(this.LastType, (BuildDescriptor.CategoryType)type));
		}
		this.UpdateCatFilter(this.LastType, (BuildDescriptor.CategoryType)type);
	}

	private void ClearSearch()
	{
		this.disableSearch = true;
		this.SearchBar.text = string.Empty;
		this.disableSearch = false;
	}

	public void SearchBuildMode()
	{
		if (this.disableSearch)
		{
			return;
		}
		string value = this.SearchBar.text.ToLower();
		foreach (KeyValuePair<Button, BuildDescriptor> keyValuePair in this.BuildButtons)
		{
			bool flag = GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode || keyValuePair.Value.Type == BuildDescriptor.BuildType.Furniture;
			if (keyValuePair.Value.Type == BuildDescriptor.BuildType.Environment && !GameSettings.Instance.EditMode)
			{
				flag = false;
			}
			keyValuePair.Key.gameObject.SetActive(flag && keyValuePair.Value.SearchString.Contains(value));
		}
		this.BuildMenuScroll.value = 1f;
	}

	public void UpdateCashflow()
	{
		if (GameSettings.Instance == null)
		{
			return;
		}
		List<List<float>> list = (from x in GameSettings.Instance.MyCompany.Cashflow
		where !x.Key.Equals("Balance")
		select x.Value).ToList<List<float>>();
		if (GameSettings.DaysPerMonth > 1)
		{
			float f = (from x in GameSettings.Instance.MyCompany.tempCashflow
			where !x.Key.Equals("Balance")
			select x).Sum((KeyValuePair<string, float> x) => x.Value);
			this.CashflowChart.Values[0][4] = Mathf.Pow(Mathf.Abs(f), 0.4f) * Mathf.Sign(f);
			for (int i = 0; i < 4; i++)
			{
				int j = i;
				if (list[0].Count - 1 - j >= 0)
				{
					float f2 = (from x in list
					select x[x.Count - 1 - j]).Sum();
					this.CashflowChart.Values[0][3 - i] = Mathf.Pow(Mathf.Abs(f2), 0.4f) * Mathf.Sign(f2);
				}
			}
		}
		else
		{
			for (int k = 0; k < 5; k++)
			{
				int j = k;
				if (list[0].Count - 1 - j >= 0)
				{
					float f3 = (from x in list
					select x[x.Count - 1 - j]).Sum();
					this.CashflowChart.Values[0][4 - k] = Mathf.Pow(Mathf.Abs(f3), 0.4f) * Mathf.Sign(f3);
				}
			}
		}
		this.CashflowChart.UpdateCachedBars();
	}

	public void AddPopupMessage(string msg, string icon, PopupManager.PopUpAction action, uint target, PopupManager.NotificationSound sfx, Color color, float importance, PopupManager.PopupIDs id = PopupManager.PopupIDs.None, int cooldown = 1)
	{
		this.popupManager.AddPopup(msg, icon, action, target, color, importance, sfx, cooldown, id);
	}

	public void AddPopupMessage(string msg, string icon, PopupManager.PopUpAction action, uint target, PopupManager.NotificationSound sfx, float importance, PopupManager.PopupIDs id = PopupManager.PopupIDs.None, int cooldown = 1)
	{
		this.popupManager.AddPopup(msg, icon, action, target, new Color32(50, 50, 50, byte.MaxValue), importance, sfx, cooldown, id);
	}

	private void Update()
	{
		for (int i = 0; i < this.EffectivenessEmitter.Length; i++)
		{
			ParticleSystem particleSystem = this.EffectivenessEmitter[i];
			particleSystem.main.simulationSpeed = (float)this.GameSpeed;
		}
		this.WorkItemDragging();
		this.BuildModePanelImage.color = this.BuildModePanelImage.color.Alpha((Mathf.Cos(Time.realtimeSinceStartup * 3.14159274f * 1.5f) + 1.5f) * 0.4f * 0.8f);
		bool flag = GameSettings.FreezeGame && (!OptionsWindow.Instance.Window.Shown || !OptionsWindow.Instance.Panels[1].activeSelf);
		if (this._lastBlur ^ flag)
		{
			this._lastBlur = flag;
			this._blurStamp = Time.realtimeSinceStartup;
			if (flag)
			{
				this.BlurScript.enabled = true;
			}
		}
		if (!flag && this.BlurScript.blurSize == 0f)
		{
			this.BlurScript.enabled = false;
		}
		if (this.BlurScript.enabled)
		{
			this.BlurScript.blurSize = Mathf.Lerp((float)((!flag) ? 4 : 0), (float)((!flag) ? 0 : 4), Mathf.Min(1f, (Time.realtimeSinceStartup - this._blurStamp) * this.BlurSpeed));
		}
		this.ColorScript.enabled = flag;
		this.PlotHolder.transform.position = new Vector3(0f, (float)GameSettings.Instance.ActiveFloor * 2f + 0.03f, 0f);
		this.UpdateWarnings();
		this.MoneyLabel.text = GameSettings.Instance.MyCompany.Money.Currency(true);
		this.MoneyLabelBack.color = ((GameSettings.Instance.MyCompany.Money <= 0f) ? new Color32(248, 87, 87, 191) : new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, 191));
		this.FanLabel.text = GameSettings.Instance.MyCompany.Fans.ToString("N0");
		if (this.BuildMode)
		{
			this.BuildMoneyLabel.text = GameSettings.Instance.MyCompany.Money.Currency(true);
		}
		this.TimeLabel.text = SDateTime.Now().ToTimeString(true);
		if (GameSettings.DaysPerMonth > 1)
		{
			this.DayLabel.text = string.Concat(new object[]
			{
				"Day".Loc(),
				" ",
				TimeOfDay.Instance.Day + 1,
				"/",
				GameSettings.DaysPerMonth
			});
			this.DateLabel.text = SDateTime.Now().ToVeryCompactString();
		}
		else
		{
			this.DateLabel.text = SDateTime.Now().ToCompactString();
		}
		if (this.lastRep != GameSettings.Instance.MyCompany.BusinessReputation)
		{
			this.ReputationBar.Colors[1] = ((this.lastRep <= GameSettings.Instance.MyCompany.BusinessReputation) ? new Color32(126, 246, 105, byte.MaxValue) : new Color32(byte.MaxValue, 114, 114, byte.MaxValue));
			this.lastRep = GameSettings.Instance.MyCompany.BusinessReputation;
		}
		this.ReputationBar[1] = GameSettings.Instance.MyCompany.BusinessReputation * 6f;
		this.ReputationBar[2] = GameSettings.Instance.MyCompany.DiscreteRep * 6f;
		this.TemperatureTip.ToolTipValue = TimeOfDay.Instance.Temperature.Temperature(false);
		this.TemperatureTip.UpdateTip();
		float num = TimeOfDay.Instance.Temperature.MapRange(-5f, 35f, 0f, 1f, true);
		this.TemperatureProg.offsetMax = new Vector2(this.TemperatureProg.offsetMax.x, (num - 1f) * this.TemperatureHolder.rect.height);
		Color color = Color.Lerp(HUD.ThemeColors[1], HUD.ThemeColors[2], num);
		for (int j = 0; j < this.TemperatureColor.Length; j++)
		{
			this.TemperatureColor[j].color = color;
		}
		this.ClockArm.rotation = Quaternion.Euler(0f, 0f, -((float)(TimeOfDay.Instance.Hour % 12) + TimeOfDay.Instance.Minute / 60f) / 12f * 360f);
		this.SkipButton.interactable = (!this.BuildMode && TimeOfDay.Instance.canSkip);
		if ((Options.AutoSkip || InputController.GetKeyUp(InputController.Keys.SkipDay, false)) && this.SkipButton.interactable)
		{
			this.SkipDay();
		}
		this.BuildPanel.anchoredPosition = new Vector2(this.BuildPanel.anchoredPosition.x, (float)Mathf.CeilToInt(Mathf.Lerp(this.BuildPanel.anchoredPosition.y, (!this.buildMode) ? (-this.BuildPanel.sizeDelta.y - 10f) : 0f, Time.deltaTime * 10f)));
		bool flag2 = this.BuildPanel.anchoredPosition.y > -(this.BuildPanel.sizeDelta.y - 10f);
		if (this.BuildPanel.gameObject.activeSelf ^ flag2)
		{
			this.BuildPanel.gameObject.SetActive(flag2);
		}
		this.BuildHelperPanel.anchoredPosition = new Vector2((float)Mathf.CeilToInt(Mathf.Lerp(this.BuildHelperPanel.anchoredPosition.x, (!this.buildMode) ? (-this.BuildHelperPanel.sizeDelta.x - 10f) : 0f, Time.deltaTime * 10f)), this.BuildHelperPanel.anchoredPosition.y);
		for (int k = 0; k < this.MainPanels.Length; k++)
		{
			float num2 = (float)this.MainPanels.Length - (float)k;
			this.MainPanels[k].anchoredPosition = new Vector2(this.MainPanels[k].anchoredPosition.x, (float)Mathf.CeilToInt(Mathf.Lerp(this.MainPanels[k].anchoredPosition.y, (!this.buildMode) ? 0f : (-this.MainPanels[0].sizeDelta.y - 12f), Time.deltaTime * 5f * num2)));
		}
		bool active = GameSettings.Instance.MyCompany.WorkItems.Any((WorkItem x) => !x.Hidden) && !this.BuildMode;
		this.workItemTogglePanel.SetActive(active);
		this.MainWorkItemPanel.gameObject.SetActive(active);
		if (this.WorkItemScroll.gameObject.activeSelf)
		{
			this.MainWorkItemPanel.sizeDelta = new Vector2(266f, this.MainWorkItemPanel.sizeDelta.y);
		}
		else
		{
			this.MainWorkItemPanel.sizeDelta = new Vector2(256f, this.MainWorkItemPanel.sizeDelta.y);
			this.WorkItemScroll.value = 1f;
		}
		if (!GameSettings.FreezeGame)
		{
			if (InputController.GetKeyUp(InputController.Keys.SaveGame, false))
			{
				if (GameSettings.Instance.AssociatedSave != null)
				{
					GameSettings.Instance.AssociatedSave.SaveNow(false, true, SaveGameManager.CurrentMode);
				}
				else
				{
					SaveGameManager.Instance.AutoSave();
				}
			}
			if (InputController.GetKeyUp(InputController.Keys.HideHUD, false))
			{
				if (WindowManager.Instance.MainPanel.activeSelf)
				{
					WindowManager.DisableAll(true, null);
				}
				else
				{
					WindowManager.EnableAll();
				}
			}
			if (InputController.GetKeyDown(InputController.Keys.ToggleBuildMode, false))
			{
				this.BuildMode = !this.BuildMode;
			}
		}
		if (this.BuildMode && InputController.GetKeyDown(InputController.Keys.Undo, false))
		{
			GameSettings.Instance.Undo();
		}
		if (InputController.GetKeyUp(InputController.Keys.Pause, false))
		{
			if (this.GameSpeed == 0)
			{
				this.disableSpeedPanel = true;
				this.GameSpeed = this.BeforePause;
				this.disableSpeedPanel = false;
			}
			else
			{
				this.disableSpeedPanel = true;
				this.BeforePause = this.GameSpeed;
				this.GameSpeed = 0;
				this.disableSpeedPanel = false;
			}
		}
		if (InputController.GetKeyUp(InputController.Keys.Speed1, false))
		{
			this.disableSpeedPanel = true;
			this.GameSpeed = 1;
			this.disableSpeedPanel = false;
		}
		if (InputController.GetKeyUp(InputController.Keys.Speed2, false))
		{
			this.disableSpeedPanel = true;
			this.GameSpeed = 2;
			this.disableSpeedPanel = false;
		}
		if (InputController.GetKeyUp(InputController.Keys.Speed3, false))
		{
			this.disableSpeedPanel = true;
			this.GameSpeed = 3;
			this.disableSpeedPanel = false;
		}
		if (Input.GetKeyUp(KeyCode.Escape) && !WindowManager.HasModal)
		{
			this.newspaper.ShowNow(false);
			this.pauseWindow.ToggleShow();
		}
		if (InputController.GetKeyUp(InputController.Keys.ToggleWalls, false))
		{
			this.LowerWallsButton();
		}
		if (InputController.GetKeyUp(InputController.Keys.ToggleLights, false))
		{
			this.HideLampsToggle.isOn = !this.HideLampsToggle.isOn;
		}
		if (InputController.GetKeyUp(InputController.Keys.ToggleAudioOverlay, false))
		{
			this.AudioOverlayToggle.isOn = !this.AudioOverlayToggle.isOn;
		}
		if (InputController.GetKeyUp(InputController.Keys.CloseAllWindows, false) && WindowManager.ModalWindows.Count == 0)
		{
			WindowManager.Instance.ShowMessageBox("CloseAllWindowWarning".Loc(), true, DialogWindow.DialogType.Warning, delegate
			{
				foreach (GUIWindow guiwindow in WindowManager.ActiveWindows.ToList<GUIWindow>())
				{
					if (guiwindow != TutorialSystem.Instance.Window)
					{
						guiwindow.Close();
					}
				}
			}, "Close all windows", null);
		}
		if (InputController.GetKeyUp(InputController.Keys.CloseActiveWindow, false))
		{
			GUIWindow frontMostWindow = WindowManager.Instance.GetFrontMostWindow();
			if (frontMostWindow != null && !frontMostWindow.Modal && frontMostWindow != TutorialSystem.Instance.Window)
			{
				frontMostWindow.Close();
			}
		}
	}

	public void PlayAnyClip(AudioClip clip)
	{
		this.Aud.PlayOneShot(clip);
	}

	public Vector2 GetMouseProj(float offsetY = 0f, bool useFloor = true)
	{
		if (useFloor)
		{
			offsetY += (float)GameSettings.Instance.ActiveFloor * 2f;
		}
		Ray ray = CameraScript.Instance.mainCam.ScreenPointToRay(Input.mousePosition);
		Plane plane = new Plane(Vector3.up, Vector3.up * offsetY);
		float distance = 0f;
		plane.Raycast(ray, out distance);
		Vector3 point = ray.GetPoint(distance);
		return new Vector2(point.x, point.z);
	}

	public Rect MakeRect(Vector2 v1, Vector2 v2)
	{
		float num = Mathf.Min(v1.x, v2.x);
		float num2 = Mathf.Max(v1.x, v2.x);
		float num3 = Mathf.Min(v1.y, v2.y);
		float num4 = Mathf.Max(v1.y, v2.y);
		return new Rect(num, num3, num2 - num - 1f, num4 - num3 - 1f);
	}

	public Vector2 IntVec(Vector2 v, bool floor)
	{
		if (floor)
		{
			return new Vector2(Mathf.Floor(v.x), Mathf.Floor(v.y));
		}
		return new Vector2(Mathf.Ceil(v.x), Mathf.Ceil(v.y));
	}

	private void OnDrawGizmos()
	{
		if (this.path != null && this.path.Count > 0)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawSphere(this.path[0] + Vector3.up * 0.05f, 0.1f);
			for (int i = 1; i < this.path.Count; i++)
			{
				Gizmos.DrawSphere(this.path[i], 0.1f);
				Gizmos.DrawLine(this.path[i - 1] + Vector3.up * 0.05f, this.path[i] + Vector3.up * 0.05f);
			}
			Gizmos.color = Color.white;
		}
	}

	public void EmployeesButton()
	{
		this.employeeWindow.Show(null);
	}

	public void DealsButton()
	{
		this.dealWindow.Toggle();
	}

	public void HireEmployeesButton()
	{
		this.hireWindow.ShowLookWindow();
		TutorialSystem.Instance.StartTutorial("Hiring", false);
	}

	public void NewspaperButton()
	{
		this.newspaper.ShowNow(true);
	}

	public void TeamsButton()
	{
		this.TeamWindow.Window.Toggle(false);
	}

	public void StaffButton()
	{
		this.staffWindow.Show();
	}

	public void ServerButton()
	{
		this.ServerProcessWarning.SetActive(false);
		this.serverWindow.Window.Toggle(false);
	}

	public void MyReleasesButton()
	{
		ProductWindow productWindow = this.GetProductWindow("YourRelease");
		if (productWindow.Window.Shown)
		{
			productWindow.Window.Close();
		}
		else
		{
			productWindow.SetFilters(false, true, true);
			productWindow.Show(false, "PlayerReleases".Loc(), false);
			productWindow.SetCompany(GameSettings.Instance.MyCompany.Name);
		}
	}

	public void AllReleasesButton()
	{
		ProductWindow productWindow = this.GetProductWindow("AllRelease");
		if (productWindow.Window.Shown)
		{
			productWindow.Window.Close();
		}
		else
		{
			productWindow.SetFilters(true, true, true);
			productWindow.Show(true, "AllReleaes".Loc(), false);
			productWindow.ApplyFilters();
		}
	}

	public void DevelopButton()
	{
		this.docWindow.ToggleVisible();
	}

	public void InsuranceButton()
	{
		this.insuranceWindow.Show(true);
	}

	public void ContractButton()
	{
		this.contractWindow.Show();
	}

	public void CompanyButton()
	{
		this.companyWindow.Window.Toggle(false);
	}

	public void ResearchButton()
	{
		this.researchWindow.ToggleShow();
	}

	public void StockButton()
	{
		this.companyWindow.ToggleCompanyDetails(GameSettings.Instance.MyCompany);
	}

	public void LoanButton()
	{
		this.loanWindow.Show();
	}

	public void BuildModeButton()
	{
		this.BuildMode = !this.BuildMode;
	}

	public void HideCeilingButton()
	{
		GameSettings.Instance.HideCeilingFurniture = this.HideLampsToggle.isOn;
		Furniture.UpdateEdgeDetection();
		GameSettings.Instance.sRoomManager.ChangeFloor();
	}

	public void WireModeButton()
	{
		GameSettings.Instance.WireMode = this.WireModeToggle.isOn;
		this.UpdateBorderOverlay();
	}

	public void UpdateBorderOverlay()
	{
		if (WallRemovalTool.Instance != null && WallRemovalTool.Instance.gameObject.activeSelf)
		{
			this.SetBorderOverlayPanel("ActionMergeRooms", "Room", new Color(1f, 1f, 0.5f, 0.8f), false);
		}
		else if (BuildController.Instance != null && BuildController.Instance.alignNow)
		{
			this.SetBorderOverlayPanel("Anchor grid", "Grid", new Color(0.5f, 0.5f, 1f, 0.8f), false);
		}
		else if (GameSettings.Instance != null && GameSettings.Instance.WireMode)
		{
			this.SetBorderOverlayPanel("WireMode", "Wires", new Color(1f, 0.5f, 0.5f, 0.8f), false);
		}
		else if (PlotController.Instance != null && PlotController.Instance.gameObject.activeSelf)
		{
			this.SetBorderOverlayPanel("Plot view", "Plot", new Color(0.5f, 1f, 1f, 0.8f), true);
		}
		else if (this.buildMode)
		{
			this.SetBorderOverlayPanel("BuildMode", "BuildMode", new Color(1f, 1f, 1f, 0.8f), true);
		}
		else if (GameSettings.GameSpeed == 0f && !GameSettings.ForcePause)
		{
			this.SetBorderOverlayPanel(string.Empty, "Pause2", new Color(0.5f, 1f, 0.5f, 0.8f), false);
		}
		else
		{
			this.SetBorderOverlayPanel(null, null, default(Color), true);
		}
	}

	public void LowerWallsButton()
	{
		int num = (int)GameSettings.WallsDown;
		num = (num + 1) % 4;
		this.LowerWallImage.sprite = this.lowerWallImages[num];
		GameSettings.WallsDown = (GameSettings.WallState)num;
		Furniture.UpdateEdgeDetection();
		GameSettings.Instance.sRoomManager.ChangeFloor();
	}

	public void DataOverlayButton()
	{
		DataOverlay.Instance.Window.Toggle(false);
		this.DataOverlayToggle.isOn = DataOverlay.HasActive;
	}

	public void TeamTextButton()
	{
		GameSettings.Instance.sRoomManager.TeamText = !GameSettings.Instance.sRoomManager.TeamText;
	}

	public void ShowCompanyChart()
	{
		this.companyChart.Show(GameSettings.Instance.MyCompany);
	}

	public void DistributionButton()
	{
		this.distributionWindow.Show(null);
	}

	public void EventButton()
	{
		this.eventWindow.Window.Toggle(false);
	}

	public void UpdateSpeed()
	{
		if (this.disableSpeedPanel)
		{
			return;
		}
		this.disableSpeedPanel = true;
		for (int i = 0; i < this.SpeedToggles.Length; i++)
		{
			if (this.SpeedToggles[i].isOn)
			{
				this.GameSpeed = i;
				break;
			}
		}
		this.disableSpeedPanel = false;
	}

	public void SkipDay()
	{
		this.ShouldShowSkipHint = false;
		TimeOfDay.Instance.SkipTime();
	}

	public void ComingReleaseButton()
	{
		this.comingReleaseWindow.Toggle();
	}

	public void FixUpperDayPanel()
	{
		if (GameSettings.DaysPerMonth > 1)
		{
			RectTransform component = this.TimeLabel.transform.parent.GetComponent<RectTransform>();
			RectTransform component2 = this.DateLabel.transform.parent.GetComponent<RectTransform>();
			component.sizeDelta = new Vector2(component.sizeDelta.x * 2f / 3f, component.sizeDelta.y);
			component.anchoredPosition = new Vector2(-component.sizeDelta.x - 1f, component.anchoredPosition.y);
			component2.sizeDelta = new Vector2(component2.sizeDelta.x * 2f / 3f - 1f, component2.sizeDelta.y);
			component2.anchoredPosition = new Vector2(component2.sizeDelta.x + 1f, component2.anchoredPosition.y);
		}
		this.DayLabel.transform.parent.gameObject.SetActive(GameSettings.DaysPerMonth > 1);
	}

	public void CloneTool()
	{
		Room[] sel = (from x in SelectorController.Instance.Selected.OfType<Room>()
		where x != null && x.gameObject != null
		select x).ToArray<Room>();
		if (sel.Length > 0)
		{
			int num = sel.Sum((Room x) => x.GetFurnitures().Count);
			if (sel.Length > 5 || num > 30)
			{
				WindowManager.Instance.ShowMessageBox("CloneComplexityWarning".Loc(), false, DialogWindow.DialogType.Warning, delegate
				{
					RoomCloneTool.Instance.Show(sel);
				}, "Room cloning", null);
			}
			else
			{
				RoomCloneTool.Instance.Show(sel);
			}
		}
	}

	public void DrawProgressbar(Rect rect, float percent, Color color)
	{
		GUI.Box(rect, string.Empty, this.ProgressBack);
		if (percent > 0f)
		{
			GUI.color = color;
			GUI.Box(new Rect(rect.x, rect.y, rect.width * percent, rect.height), string.Empty, this.ProgressFront);
			GUI.color = Color.white;
		}
	}

	public void ShowAutoProjDetail(AutoDevWorkItem workItem)
	{
		AutoDevDetailWindow autoDevDetailWindow = WindowManager.FindWindowType<AutoDevDetailWindow>().FirstOrDefault((AutoDevDetailWindow x) => x.workItem == workItem);
		if (autoDevDetailWindow != null)
		{
			WindowManager.Focus(autoDevDetailWindow.Window);
		}
		else
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.AutoProjectDetailWindow);
			gameObject.GetComponent<AutoDevDetailWindow>().workItem = workItem;
			gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		}
	}

	public void DrawBalancebar(Rect rect, float percent, Color color)
	{
		GUI.Box(rect, string.Empty, this.ProgressBack);
		GUI.color = color;
		if (percent > 0f)
		{
			GUI.Box(new Rect(rect.x + rect.width / 2f, rect.y, rect.width * percent / 2f, rect.height), string.Empty, this.ProgressFront);
		}
		else
		{
			percent = -percent;
			GUI.Box(new Rect(rect.x + rect.width / 2f - rect.width * percent / 2f, rect.y, rect.width * percent / 2f, rect.height), string.Empty, this.ProgressFront);
		}
		GUI.color = Color.white;
	}

	private void UpdateWarnings()
	{
		if (GameSettings.Instance != null && !GameSettings.FreezeGame)
		{
			Vector3 mousePosition = Input.mousePosition;
			this.WarningOverlay.BeginMessageUpdate();
			for (int i = 0; i < GameSettings.Instance.sRoomManager.Rooms.Count; i++)
			{
				Room room = GameSettings.Instance.sRoomManager.Rooms[i];
				if (this.BuildMode)
				{
					if (!CameraScript.Instance.TopDown && room.Floor == GameSettings.Instance.ActiveFloor && room.Problems.Count > 0 && room.OuterWalls != null && room.OuterWalls.GetComponent<Renderer>().isVisible)
					{
						Vector3 p = new Vector3(room.Center.x, (float)(room.Floor * 2 + 1), room.Center.y);
						this.WarningOverlay.AddMessages(this.WTS(p), mousePosition, room.Problems);
					}
				}
				else if (!room.CanClean && room.Floor == GameSettings.Instance.ActiveFloor && room.OuterWalls != null && room.OuterWalls.GetComponent<Renderer>().isVisible)
				{
					Vector3 p2 = new Vector3(room.Center.x, (float)(room.Floor * 2 + 1), room.Center.y);
					this.WarningOverlay.AddMessages(this.WTS(p2), mousePosition, "CleaningBlocked".Loc());
				}
			}
			if (this.UnreachableFuniture.Count > 0)
			{
				foreach (Furniture furniture in this.UnreachableFuniture)
				{
					if (furniture.Parent != null && furniture.Parent.Floor == GameSettings.Instance.ActiveFloor)
					{
						string[] furniture2 = Localization.GetFurniture(furniture.name, null);
						this.WarningOverlay.AddMessages(this.WTS(furniture.transform.position + Vector3.up * furniture.Height2), mousePosition, "UnreachableFurniture".Loc(new object[]
						{
							furniture2[0].ToLower()
						}));
					}
				}
			}
			if (this.NoChairPC.Count > 0)
			{
				foreach (Furniture furniture3 in this.NoChairPC)
				{
					if (furniture3.Parent != null && furniture3.Parent.Floor == GameSettings.Instance.ActiveFloor)
					{
						string[] furniture4 = Localization.GetFurniture(furniture3.name, null);
						this.WarningOverlay.AddMessages(this.WTS(furniture3.transform.position + Vector3.up * furniture3.Height2), mousePosition, "MissingChairHint".Loc(new object[]
						{
							furniture4[0].ToLower()
						}));
					}
				}
			}
			if (this.CantGetHome.Count > 0)
			{
				foreach (Actor actor in this.CantGetHome)
				{
					if (Mathf.FloorToInt((actor.transform.position.y + 1f) / 2f) == GameSettings.Instance.ActiveFloor)
					{
						Vector3 p3 = new Vector3(actor.transform.position.x, Mathf.Floor(actor.transform.position.y / 2f) * 2f + 2f, actor.transform.position.z);
						this.WarningOverlay.AddMessages(this.WTS(p3), mousePosition, "EmployeeStuck".Loc(new object[]
						{
							actor.ToString()
						}));
					}
				}
			}
			if (this.BlockedDoorways.Count > 0)
			{
				foreach (IRoomConnector roomConnector in this.BlockedDoorways)
				{
					Transform objectTransform = roomConnector.ObjectTransform;
					if (Mathf.FloorToInt((objectTransform.position.y + 1f) / 2f) == GameSettings.Instance.ActiveFloor)
					{
						this.WarningOverlay.AddMessages(this.WTS(objectTransform.position + 2f * Vector3.up), mousePosition, "PathwayBlocked".Loc());
					}
				}
			}
			if (this.BuildMode && GameSettings.Instance.ActiveFloor >= 0 && this.UnreachableParking.Count > 0)
			{
				foreach (RoadNode roadNode in this.UnreachableParking)
				{
					this.WarningOverlay.AddMessages(this.WTS(roadNode.transform.position), mousePosition, "UnreachableParking".Loc());
				}
			}
			this.WarningOverlay.EndMessageUpdate();
		}
	}

	private Vector2 WTS(Vector3 p)
	{
		return this.MainCamera.WorldToScreenPoint(p);
	}

	public void UpdateServerWarning()
	{
		if (GameSettings.Instance.UnsupportedServerItems.Count > 0)
		{
			this.ServerProcessWarning.SetActive(true);
			this.ServerImageButton.color = Color.red;
		}
		else
		{
			this.ServerProcessWarning.SetActive(false);
			this.ServerImageButton.color = Color.white;
		}
	}

	public void AddDrawLine(Vector3 a, Vector3 b)
	{
	}

	public void AddDebugLog(string value)
	{
	}

	private int GetIndex(float y)
	{
		int num = 0;
		float num2 = 0f;
		for (int i = 0; i < this.WorkItemPanel.childCount; i++)
		{
			Transform child = this.WorkItemPanel.GetChild(i);
			if (child.gameObject.activeSelf)
			{
				if (child.gameObject == this.WorkItemDrag.gameObject)
				{
					if (num2 + 132f > y)
					{
						return num;
					}
					num2 += child.GetComponent<RectTransform>().rect.height + 4f;
				}
				else
				{
					num2 += child.GetComponent<RectTransform>().rect.height + 4f;
					if (num2 > y)
					{
						return num;
					}
				}
			}
			num++;
		}
		return num;
	}

	public void WorkItemDragging()
	{
		if (this.DraggingWork != null)
		{
			if (!this.WorkItemDrag.gameObject.activeSelf)
			{
				this.WorkItemDrag.gameObject.SetActive(true);
				this.WorkItemDrag.GetComponent<LayoutElement>().minHeight = (float)((!this.DraggingWork.work.Collapsed) ? 128 : 43);
			}
			Vector2 vector;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(this.WorkItemPanel, Input.mousePosition, null, out vector);
			this.WorkItemDrag.SetSiblingIndex(this.GetIndex(-vector.y));
			if (Input.GetMouseButtonUp(0))
			{
				this.DraggingWork.transform.SetSiblingIndex(this.WorkItemDrag.GetSiblingIndex());
				this.WorkItemDrag.gameObject.SetActive(false);
				this.DraggingWork.gameObject.SetActive(true);
				this.DraggingWork = null;
				for (int i = 0; i < this.WorkItemPanel.childCount; i++)
				{
					GUIWorkItem component = this.WorkItemPanel.GetChild(i).GetComponent<GUIWorkItem>();
					if (component != null)
					{
						component.work.SiblingIndex = i;
					}
				}
			}
		}
		else if (this.WorkItemDrag.gameObject.activeSelf)
		{
			this.WorkItemDrag.gameObject.SetActive(false);
		}
	}

	public ProductWindow GetProductWindow(string id)
	{
		if (id == null)
		{
			return this.productWindow;
		}
		ProductWindow productWindow = null;
		if (!this._pWindows.TryGetValue(id, out productWindow))
		{
			productWindow = UnityEngine.Object.Instantiate<ProductWindow>(this.productWindow);
			productWindow.name = id + "ProductWindow";
			productWindow.Window.StartHidden = false;
			productWindow.Window.gameObject.SetActive(false);
			GUIWindow window = productWindow.Window;
			window.WindowSizeID += id;
			productWindow.Init();
			productWindow.ApplyFilters();
			productWindow.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
			this._pWindows[id] = productWindow;
		}
		return productWindow;
	}

	public void ApplyProductWindowFilters()
	{
		foreach (KeyValuePair<string, ProductWindow> keyValuePair in this._pWindows)
		{
			keyValuePair.Value.ApplyFilters();
		}
	}

	public void ToggleAssignOverlay(bool value)
	{
		GameSettings.Instance.AssignOverlay = value;
		foreach (Furniture furniture in GameSettings.Instance.sRoomManager.AllFurniture)
		{
			if (furniture != null)
			{
				furniture.CheckAssignMat();
			}
		}
	}

	public Camera MainCamera;

	public bool ShouldShowSkipHint = true;

	public static HUD Instance;

	public OverlayWarning WarningOverlay;

	public GameObject[] TopPanels;

	public GameObject BuildPlane;

	public GameObject BuildModePanel;

	public GameObject RoomHelpAnim;

	public GameObject RoomObject;

	public GameObject FlatPlane;

	public GameObject RoomWall;

	public GameObject roomCorner;

	public GameObject RoomDarkness;

	public GameObject[] HideInBuild;

	public Image BuildModePanelImage;

	public Image BuildModeIcon;

	public IconFillBar ReputationBar;

	public UMLMiniGame UMLGame;

	public CodeMiniGame CodeGame;

	public Scrollbar BuildMenuScroll;

	public Slider SunSlider;

	private float lastRep;

	public RectTransform MainWorkItemPanel;

	public RectTransform WorkItemPanel;

	public RectTransform WorkItemDrag;

	[NonSerialized]
	public GUIWorkItem DraggingWork;

	public GameObject CategoryPanel;

	public GameObject BuildButtonPanel;

	public GameObject BuyButtonPrefab;

	public GameObject TogglePrefab;

	public GameObject workItemTogglePanel;

	public GameObject BuildModeMainButtonPanel;

	public GameObject[] BuildModeMainButtons;

	public GameObject[] WorkItemPrefab;

	public BuilderDetailPanel BuildDetailPanel;

	public InputField SearchBar;

	public BlurOptimized BlurScript;

	public ColorCorrectionLookup ColorScript;

	public Sprite RoomThumbnail;

	public Sprite RectRoomThumbnail;

	public Sprite RemoveWallThumbnail;

	public Sprite FenceThumbnail;

	public Sprite RectFenceThumbnail;

	public Sprite RoadThumbnail;

	public Sprite NoRoadThumbnail;

	public Sprite ParkHorThumbnail;

	public Sprite ParkVertThumbnail;

	public Sprite HouseThumbnail;

	public Sprite TreeThumbnail;

	public Sprite SkyscraperThumbnail;

	public Toggle[] SpeedToggles;

	public Sprite[] lowerWallImages;

	public Image LowerWallImage;

	public Toggle DataOverlayToggle;

	public Toggle HideLampsToggle;

	public Toggle WireModeToggle;

	public Toggle AudioOverlayToggle;

	public Button SkipButton;

	private Dictionary<HUD.CategoryKey, Toggle> BuildCategories = new Dictionary<HUD.CategoryKey, Toggle>();

	private Dictionary<Button, BuildDescriptor> BuildButtons = new Dictionary<Button, BuildDescriptor>();

	private Vector3 sPoint;

	public ErrorButton errorButton;

	public List<Vector3> path = new List<Vector3>();

	public bool FinishPath;

	private bool AllWorkTogglesOff = true;

	public GUIStyle ProgressBack;

	public GUIStyle ProgressFront;

	public GUIBarChart CashflowChart;

	public ReputationBars mainReputataionBars;

	public Text MoneyLabel;

	public Text TimeLabel;

	public Text DateLabel;

	public Text PopularityLabel;

	public Text BuildMoneyLabel;

	public Text DayLabel;

	public Text BuildModeTypeText;

	public Text FanLabel;

	public RawImage MoneyLabelBack;

	public RectTransform ClockArm;

	public GameObject BankruptcyWarning;

	public GameObject SkipDayHint;

	public GameObject ServerProcessWarning;

	public Image ServerImageButton;

	public DesignDocumentWindow docWindow;

	[SerializeField]
	private ProductWindow productWindow;

	public TexturePickerWindow textureWindow;

	public BenefitWindow benefitWindow;

	public MarketingWindow marketingWindow;

	public RoleSelectWindow roleSelect;

	public ContractWindow contractWindow;

	public ResearchWindow researchWindow;

	public PauseWindow pauseWindow;

	public HireWindow hireWindow;

	public CompanyWorksheet financeWindow;

	public StaffWindow staffWindow;

	public TeamWindow TeamWindow;

	public TeamSelectWindow TeamSelectWindow;

	public EducationWindow educationWindow;

	public EmployeeWindow employeeWindow;

	public AutoDevWindow AutoDevWindow;

	public LoanWindow loanWindow;

	public ReviewWindow reviewWindow;

	public EventWindow eventWindow;

	public InsuranceWindow insuranceWindow;

	public ComingReleaseWindow comingReleaseWindow;

	public DetailWindow DetailWindow;

	public RoleGrid roleGrid;

	public WageWindow wageWindow;

	public ComplaintWindow complaintWindow;

	public DistributionWindow distributionWindow;

	public ServerWindow serverWindow;

	public CopyOrderWindow copyOrderWindow;

	public DealWindow dealWindow;

	public Newspaper newspaper;

	public RoomGroupWindow roomGroupWindow;

	public BlueprintWindow blueprint;

	public DateRangeWindow dateRangeWindow;

	public NumberRangeWindow numberRangeWindow;

	public Texture2D ColorMap;

	public Texture2D LightMap;

	public Texture2D TopBar;

	public ParticleSystem[] EffectivenessEmitter;

	public ParticleSystem DirtEmitter;

	public ParticleSystem ZnoreEmitter;

	public PopupManager popupManager;

	public PortraitMaker Portraits;

	public Text IdleCounter;

	public bool SeeThroughWalls;

	public static Color[] ThemeColors = new Color[]
	{
		new Color32(126, 207, 112, byte.MaxValue),
		new Color32(95, 122, 155, byte.MaxValue),
		new Color32(220, 108, 130, byte.MaxValue),
		new Color32(236, 157, 112, byte.MaxValue),
		new Color32(126, 95, 160, byte.MaxValue),
		new Color32(90, 203, 207, byte.MaxValue),
		new Color32(216, 194, 89, byte.MaxValue),
		new Color32(199, 40, 40, byte.MaxValue)
	};

	public Material LineMat;

	public CompanyWindow companyWindow;

	public CompanyChart companyChart;

	public GameObject PlotHolder;

	public List<Furniture> AllFurniture;

	private bool buildMode;

	public RectTransform[] MainPanels;

	public RectTransform BuildPanel;

	public RectTransform BuildHelperPanel;

	public RectTransform MainContentPanel;

	public RectTransform TemperatureProg;

	public RectTransform TemperatureHolder;

	public Image[] TemperatureColor;

	public GUIToolTipper TemperatureTip;

	private static int[] speeds = new int[]
	{
		0,
		1,
		10,
		30
	};

	public Texture2D[] PlayButtons;

	private Rect LastRect;

	private float lastTick;

	public Scrollbar WorkItemScroll;

	public Scrollbar BuildScrollBar;

	public GameObject FurnitureCategoryPanel;

	public bool disableSpeedPanel;

	private int BeforePause = 1;

	public Texture2D ErrorIcon;

	public GUIStyle ErrorBox;

	public Toggle[] WorkItemToggles;

	public Toggle SelectionFilterToggle;

	public string[] WorkItemNames;

	[NonSerialized]
	public Dictionary<string, Toggle> WorkToToggle;

	[NonSerialized]
	public HashSet<Furniture> UnreachableFuniture = new HashSet<Furniture>();

	[NonSerialized]
	public HashSet<Furniture> NoChairPC = new HashSet<Furniture>();

	[NonSerialized]
	public HashSet<IRoomConnector> BlockedDoorways = new HashSet<IRoomConnector>();

	[NonSerialized]
	public HashSet<RoadNode> UnreachableParking = new HashSet<RoadNode>();

	[NonSerialized]
	public HashSet<Actor> CantGetHome = new HashSet<Actor>();

	private AudioSource Aud;

	[NonSerialized]
	private HashSet<Actor> _idleEmployees = new HashSet<Actor>();

	public BuildDescriptor.BuildType LastType;

	private bool disableSearch;

	private bool _lastBlur;

	private float _blurStamp;

	public float BlurSpeed = 2f;

	public GameObject AutoProjectDetailWindow;

	[NonSerialized]
	private Dictionary<string, ProductWindow> _pWindows = new Dictionary<string, ProductWindow>();

	[CompilerGenerated]
	private static Func<string, string> <>f__mg$cache0;

	private struct CategoryKey
	{
		public CategoryKey(BuildDescriptor.BuildType buildType, string category, BuildDescriptor.CategoryType function)
		{
			this.BuildType = (int)buildType;
			this.Category = category;
			this.Function = (int)function;
		}

		public bool Match(BuildDescriptor.BuildType buildType, BuildDescriptor.CategoryType function)
		{
			return buildType == (BuildDescriptor.BuildType)this.BuildType && function == (BuildDescriptor.CategoryType)this.Function;
		}

		public bool Equals(HUD.CategoryKey other)
		{
			return this.BuildType == other.BuildType && string.Equals(this.Category, other.Category) && this.Function == other.Function;
		}

		public override bool Equals(object obj)
		{
			return !object.ReferenceEquals(null, obj) && obj is HUD.CategoryKey && this.Equals((HUD.CategoryKey)obj);
		}

		public override int GetHashCode()
		{
			int num = this.BuildType;
			num = (num * 397 ^ ((this.Category == null) ? 0 : this.Category.GetHashCode()));
			return num * 397 ^ this.Function;
		}

		public readonly int BuildType;

		public readonly string Category;

		public readonly int Function;
	}
}
