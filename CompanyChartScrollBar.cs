﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class CompanyChartScrollBar : MonoBehaviour, IScrollHandler, IEventSystemHandler
{
	public void OnScroll(PointerEventData d)
	{
		this.chart.OnScroll(d);
	}

	public CompanyChart chart;
}
