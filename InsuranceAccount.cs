﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class InsuranceAccount
{
	public float GetMaxWithdraw()
	{
		float num = 0f;
		for (int i = 0; i < this.Deposits.Count; i++)
		{
			num += this.GetDepositInterest(this.Deposits[i]);
		}
		return Mathf.Floor(this.Money - num);
	}

	public float GetDepositInterest(KeyValuePair<float, SDateTime> deposit)
	{
		return this.GetDepositInterest(deposit.Key, deposit.Value);
	}

	public float GetDepositInterest(float amount, SDateTime date)
	{
		int monthsFlat = Utilities.GetMonthsFlat(date, SDateTime.Now());
		return (Mathf.Pow(1f + InsuranceAccount.MonthlyInterest, (float)monthsFlat) - 1f) * amount;
	}

	public float GetWithdrawCost(float amount)
	{
		float num = 0f;
		float num2 = this.Money - this.Deposits.SumSafe((KeyValuePair<float, SDateTime> x) => x.Key + this.GetDepositInterest(x));
		if (amount > num2)
		{
			float num3 = amount;
			for (int i = this.Deposits.Count - 1; i >= 0; i--)
			{
				KeyValuePair<float, SDateTime> keyValuePair = this.Deposits[i];
				num += this.GetDepositInterest(Mathf.Min(num3, keyValuePair.Key), keyValuePair.Value);
				num3 -= keyValuePair.Key;
				if (num3 <= 0f)
				{
					break;
				}
			}
		}
		return num;
	}

	public void Withdraw(float amount)
	{
		float num = this.Money - this.Deposits.SumSafe((KeyValuePair<float, SDateTime> x) => x.Key + this.GetDepositInterest(x));
		float num2 = 0f;
		if (amount > num)
		{
			float num3 = amount;
			for (int i = this.Deposits.Count - 1; i >= 0; i--)
			{
				KeyValuePair<float, SDateTime> keyValuePair = this.Deposits[i];
				num2 += this.GetDepositInterest(Mathf.Min(num3, keyValuePair.Key), keyValuePair.Value);
				num3 -= keyValuePair.Key;
				if (num3 < 0f)
				{
					this.Deposits[i] = new KeyValuePair<float, SDateTime>(-num3, keyValuePair.Value);
					break;
				}
				this.Deposits.RemoveAt(i);
			}
			GameSettings.Instance.MyCompany.AddToCashflow(-num2, Company.TransactionCategory.Interest);
			this.Money -= num2;
		}
		this.UpdateDeposits();
	}

	public void Deposit(float amount)
	{
		SDateTime sdateTime = SDateTime.Now();
		int index = this.Deposits.Count - 1;
		if (this.Deposits.Count > 0 && this.Deposits[index].Value.EqualsVerySimple(sdateTime))
		{
			this.Deposits[index] = new KeyValuePair<float, SDateTime>(this.Deposits[index].Key + amount, this.Deposits[index].Value);
		}
		else
		{
			this.Deposits.Add(new KeyValuePair<float, SDateTime>(amount, sdateTime));
		}
	}

	public void UpdateDeposits()
	{
		int num = 0;
		float num2 = 0f;
		int num3 = 0;
		SDateTime sdateTime = SDateTime.Now();
		int num4 = 0;
		for (int i = 0; i < this.Deposits.Count; i++)
		{
			KeyValuePair<float, SDateTime> keyValuePair = this.Deposits[i];
			if (num != keyValuePair.Value.RealYear)
			{
				if (num2 > 0f)
				{
					int num5 = num + InsuranceAccount.GetLockYears(num2);
					if (sdateTime.RealYear > num5 || (num5 == sdateTime.RealYear && sdateTime.Month >= num4))
					{
						int num6 = i - num3;
						this.Deposits.RemoveRange(num3, num6);
						i -= num6;
					}
				}
				num2 = 0f;
				num = keyValuePair.Value.RealYear;
				num3 = i;
			}
			num2 += keyValuePair.Key;
			num4 = keyValuePair.Value.Month;
		}
		if (num2 > 0f)
		{
			int num7 = num + InsuranceAccount.GetLockYears(num2);
			if (sdateTime.RealYear > num7 || (num7 == sdateTime.RealYear && sdateTime.Month >= num4))
			{
				this.Deposits.RemoveRange(num3, this.Deposits.Count - num3);
			}
		}
	}

	private static int GetLockYears(float amount)
	{
		return Mathf.Clamp(Mathf.FloorToInt(amount / 100000f), 1, 5);
	}

	public void GetDeposits(Text l, Text c, Text r)
	{
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilder2 = new StringBuilder();
		StringBuilder stringBuilder3 = new StringBuilder();
		stringBuilder.AppendLine("Amount".Loc());
		stringBuilder2.AppendLine("Accrued interest".Loc());
		stringBuilder3.AppendLine("DepositRelease".Loc());
		int num = -1;
		float num2 = 0f;
		float num3 = 0f;
		int month = 0;
		for (int i = 0; i < this.Deposits.Count; i++)
		{
			KeyValuePair<float, SDateTime> deposit = this.Deposits[i];
			if (num != deposit.Value.Year)
			{
				if (num2 > 0f)
				{
					int year = num + InsuranceAccount.GetLockYears(num2);
					SDateTime sdateTime = new SDateTime(0, month, year);
					stringBuilder.AppendLine(num2.Currency(true));
					stringBuilder2.AppendLine(num3.Currency(true));
					stringBuilder3.AppendLine(sdateTime.ToCompactString());
				}
				num2 = 0f;
				num3 = 0f;
				num = deposit.Value.Year;
			}
			num2 += deposit.Key;
			num3 += this.GetDepositInterest(deposit);
			month = deposit.Value.Month;
		}
		if (num2 > 0f)
		{
			int year2 = num + InsuranceAccount.GetLockYears(num2);
			SDateTime sdateTime2 = new SDateTime(0, month, year2);
			stringBuilder.AppendLine(num2.Currency(true));
			stringBuilder2.AppendLine(num3.Currency(true));
			stringBuilder3.AppendLine(sdateTime2.ToCompactString());
		}
		l.text = stringBuilder.ToString();
		c.text = stringBuilder2.ToString();
		r.text = stringBuilder3.ToString();
	}

	public int GetAgeGroup(float age)
	{
		int value = Mathf.FloorToInt((age - (float)Employee.Youngest) / (float)(Employee.RetirementAge - Employee.Youngest) * 3f);
		return Mathf.Clamp(value, 0, 2);
	}

	public void ChangeAmount(float change)
	{
		if (!change.IsValidFloat())
		{
			return;
		}
		GameSettings.Instance.MyCompany.MakeTransaction(-change, Company.TransactionCategory.NA, null);
		this.Money += change;
	}

	public static float MonthlyInterest = 0.00165158126f;

	public float Money;

	public List<KeyValuePair<float, SDateTime>> Deposits = new List<KeyValuePair<float, SDateTime>>();
}
