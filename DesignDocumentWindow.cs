﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DesignDocumentWindow : MonoBehaviour
{
	public SimulatedCompany Subsidiairy
	{
		get
		{
			if (!this.SubsidiaryCombo.gameObject.activeSelf)
			{
				return null;
			}
			return (SimulatedCompany)this.SubsidiaryCombo.SelectedItem;
		}
	}

	public SoftwareProduct SequelTo
	{
		get
		{
			return this._sequelTo;
		}
		set
		{
			this._sequelTo = value;
			this.IPButtonText.text = ((this._sequelTo != null) ? this._sequelTo.Name : "SelectIPButton".Loc());
			foreach (KeyValuePair<string, FeatureElement> keyValuePair in this.FeatureToggles)
			{
				keyValuePair.Value.UpdateIP(this._sequelTo);
			}
		}
	}

	public SoftwareType SelectedType
	{
		get
		{
			return GameSettings.Instance.SoftwareTypes[this.TypeCombo.SelectedItem.ToString()];
		}
	}

	public float Price
	{
		get
		{
			float b = 0f;
			try
			{
				b = (float)Convert.ToDouble(this.PriceText.text);
			}
			catch (Exception)
			{
			}
			return Mathf.Max(0f, b).FromCurrency();
		}
	}

	private void Update()
	{
		this.NeedsGrid.cellSize = new Vector2(this.NeedsRect.rect.width / 2f - 8f, this.NeedsGrid.cellSize.y);
	}

	public void SelectDefaultPrice()
	{
		string t = this.SelectedType.Name;
		List<SoftwareProduct> list = (from x in GameSettings.Instance.simulation.GetAllProducts()
		where x._type.Equals(t) && x._category.Equals(this.CategoryCombo.SelectedItemString) && Utilities.GetMonths(x.Release, SDateTime.Now()) < 60f && x.UnitSum > 0u
		select x).ToList<SoftwareProduct>();
		float x2;
		if (list.Count > 0)
		{
			float num = this.SelectedType.DevTime(this.GetFeatures(), this.CategoryCombo.SelectedItemString, null);
			float? num2 = list.AverageOutlier((SoftwareProduct x) => x.Price / x.DevTime, 0.5f);
			float? num3 = (num2 == null) ? null : new float?(num2.GetValueOrDefault() * num);
			if (num3 == null || num3.Value == 0f)
			{
				x2 = Utilities.GetBasePrice(t, this.CategoryCombo.SelectedItemString, num, 1f);
			}
			else
			{
				x2 = num3.Value;
			}
		}
		else
		{
			float devtime = this.SelectedType.DevTime(this.GetFeatures(), this.CategoryCombo.SelectedItemString, null);
			x2 = Utilities.GetBasePrice(t, this.CategoryCombo.SelectedItemString, devtime, 1f);
		}
		this.PriceText.text = x2.CurrencyMul().ToString("N");
		this.priceHasBeenEdited = false;
	}

	public void PriceEndEdit()
	{
		try
		{
			float num = Mathf.Max(0f, (float)Convert.ToDouble(this.PriceText.text));
			this.PriceText.text = num.ToString("N");
			this.priceHasBeenEdited = true;
		}
		catch (Exception)
		{
			this.SelectDefaultPrice();
		}
	}

	public void ChangeDevTeam()
	{
		HUD.Instance.TeamSelectWindow.Show(false, this.DevTeams, delegate(string[] t)
		{
			this.DevTeams.Clear();
			this.DevTeams.AddRange(t);
			this.UpdateDescription();
		}, null);
	}

	public void UpdateTeamText()
	{
		this.DevTeams = (from x in this.DevTeams
		where GameSettings.Instance.sActorManager.Teams.ContainsKey(x)
		select x).ToHashSet<string>();
		this.TeamText.text = Utilities.GetTeamDescription(this.DevTeams);
	}

	public string GetBalanceScore(float score)
	{
		if (score == 0f)
		{
			return "None";
		}
		int num = Mathf.Clamp(Mathf.FloorToInt((1f - Mathf.Pow(1f - score, 2f)) * (float)DesignDocumentWindow.BalanceNames.Length), 0, DesignDocumentWindow.BalanceNames.Length - 1);
		return DesignDocumentWindow.BalanceNames[num];
	}

	public string GetTimeString(float months, float actual)
	{
		string result;
		if (months < 12f)
		{
			result = "DevTime1".Loc();
		}
		else if (months >= 24f)
		{
			result = string.Format("DevTime3".Loc(), Mathf.FloorToInt(months / 12f));
		}
		else
		{
			result = "DevTime2".Loc();
		}
		return result;
	}

	public static string GetArtistString(float devart)
	{
		return ((int)(devart * 100f)).ToString() + "%";
	}

	private void Start()
	{
		this.Legend.Colors = (this.Pie.Colors = HUD.ThemeColors.ToList<Color>());
	}

	public void UpdatePieChart()
	{
		Dictionary<string, float> specializationMonths = this.SelectedType.GetSpecializationMonths(this.GetFeatures(), this.CategoryCombo.SelectedItemString, this.GetOSs(), null);
		this.Pie.Values = (from x in specializationMonths
		select x.Value).ToList<float>();
		this.Pie.UpdateCachedPie();
		this.Legend.Items.Clear();
		this.Legend.Items.AddRange(specializationMonths.Keys);
	}

	public void ToggleVisible()
	{
		if (this.Window.gameObject.activeSelf)
		{
			this.Window.Close();
		}
		else
		{
			this.Window.Show();
			this.UpdateOnShow();
			TutorialSystem.Instance.StartTutorial("Design document", false);
		}
	}

	public void ShowSequel(SoftwareProduct product)
	{
		while (product.HasSequel)
		{
			product = product.Sequel;
		}
		if (GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareWorkItem>().Any((SoftwareWorkItem x) => x.SequelTo != null && x.SequelTo.Value == product.ID))
		{
			return;
		}
		this.Window.Show();
		this.UpdateOnShow();
		this.TypeCombo.SelectedItem = product._type;
		this.CategoryCombo.SelectedItem = product._category;
		this.ProductName.text = GameSettings.Instance.simulation.GenerateProductSequalName(product.Name);
		this.SequelTo = product;
	}

	private void ClearFeatures()
	{
		this.FeaturePanel.Clear();
		this.FeatureScroll1.value = 1f;
		this.FeatureScroll2.value = 0f;
		this.FeatureToggles.Clear();
	}

	private string[] GetFeatures()
	{
		return (from x in this.FeatureToggles
		where x.Value.MainToggle.isOn
		select x.Key).ToArray<string>();
	}

	private void SelectFeatures(string[] features)
	{
		foreach (KeyValuePair<string, FeatureElement> keyValuePair in this.FeatureToggles)
		{
			keyValuePair.Value.MainToggle.isOn = features.Contains(keyValuePair.Key);
		}
	}

	private void FixFeatures()
	{
		if (this.FeatureToggles.Count == 0)
		{
			return;
		}
		if (!this.FixingFeatures)
		{
			this.FixingFeatures = true;
			string cat = this.CategoryCombo.SelectedItemString;
			SoftwareType selectedType = this.SelectedType;
			Dictionary<string, SoftwareProduct> needProducts = this.GetNeedProducts();
			string[] array = selectedType.ForceIssue(cat, needProducts, this.GetOSProducts());
			if (array != null)
			{
				string tooltipDescription = string.Format("SoftwareSoftwareDependencyWarning".Loc(), array[1].SWFeat(array[0]), array[0].LocSW());
				foreach (FeatureElement featureElement in this.FeatureToggles.Values)
				{
					featureElement.Tipper.TooltipDescription = tooltipDescription;
					featureElement.MainToggle.isOn = false;
					featureElement.MainToggle.interactable = false;
				}
			}
			else
			{
				foreach (FeatureElement featureElement2 in this.FeatureToggles.Values)
				{
					featureElement2.UpdateAvailability(this.FeatureToggles, this.GetOSProducts(), needProducts, cat);
				}
				foreach (Feature feature in from x in selectedType.Features.Values
				where x.IsCompatible(cat) && x.Forced
				select x)
				{
					FeatureElement featureElement3 = this.FeatureToggles[feature.Name];
					if (!featureElement3.MainToggle.isOn)
					{
						Feature feature2 = feature.InverseFromLookup(selectedType, cat);
						bool flag = true;
						while (feature2 != null)
						{
							FeatureElement featureElement4 = this.FeatureToggles[feature2.Name];
							if (featureElement4.MainToggle.isOn)
							{
								flag = false;
								break;
							}
							feature2 = feature2.InverseFromLookup(selectedType, cat);
						}
						if (flag)
						{
							featureElement3.MainToggle.isOn = true;
						}
					}
				}
				foreach (FeatureElement featureElement5 in this.FeatureToggles.Values)
				{
					featureElement5.UpdateAvailability(this.FeatureToggles, this.GetOSProducts(), needProducts, this.CategoryCombo.SelectedItemString);
				}
			}
			string[] needs = selectedType.GetNeeds(this.GetFeatures(), this.CategoryCombo.SelectedItemString);
			foreach (KeyValuePair<Button, GameObject> keyValuePair in this.NeedLabels)
			{
				if (needs.Contains(this.NeedsList[keyValuePair.Key].Key))
				{
					keyValuePair.Key.gameObject.SetActive(true);
					keyValuePair.Value.SetActive(true);
				}
				else
				{
					keyValuePair.Key.gameObject.SetActive(false);
					keyValuePair.Value.SetActive(false);
				}
			}
			this.FixingFeatures = false;
		}
	}

	public void UpdateTypeRelatedCombos(bool updateCat)
	{
		this._noSubUpdate = true;
		this.SubsidiaryCombo.Selected = 0;
		this._noSubUpdate = false;
		if (updateCat)
		{
			UnityEvent onSelectedChanged = this.CategoryCombo.OnSelectedChanged;
			this.CategoryCombo.OnSelectedChanged = null;
			this.CategoryCombo.Software = this.SelectedType.Name;
			Dictionary<string, string> dictionary = (from x in this.SelectedType.Categories
			where x.Value.IsUnlocked(TimeOfDay.Instance.Year)
			select x).ToDictionary((KeyValuePair<string, SoftwareCategory> x) => x.Key, (KeyValuePair<string, SoftwareCategory> x) => x.Value.Description);
			this.CategoryCombo.UpdateContent<string>(dictionary.Keys, dictionary.Values);
			this.CategoryCombo.Selected = 0;
			this.CategoryCombo.OnSelectedChanged = onSelectedChanged;
			this.HouseToggle.isOn = false;
		}
		this.HouseToggle.gameObject.SetActive(this.SelectedType.InHouse);
		if (updateCat)
		{
			foreach (KeyValuePair<Button, GameObject> keyValuePair in this.NeedLabels)
			{
				UnityEngine.Object.Destroy(keyValuePair.Key.gameObject);
				UnityEngine.Object.Destroy(keyValuePair.Value);
			}
			this.NeedsList.Clear();
			this.NeedLabels.Clear();
			string[] needs = this.SelectedType.GetNeeds(null);
			for (int i = 0; i < needs.Length; i++)
			{
				string text = needs[i];
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.LabelPrefab);
				string[] software = Localization.GetSoftware(text, null);
				gameObject.GetComponent<Text>().text = software[0];
				gameObject.transform.SetParent(this.NeedsSubPanel.transform, false);
				GameObject button = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
				button.GetComponentInChildren<Text>().text = "NeedNotChosen".Loc();
				button.transform.SetParent(this.NeedsSubPanel.transform, false);
				Button guiButton = button.GetComponent<Button>();
				this.NeedLabels.Add(guiButton, gameObject);
				this.NeedsList.Add(guiButton, new KeyValuePair<string, uint>(text, 0u));
				string localType = text;
				guiButton.onClick.AddListener(delegate
				{
					GameObject button = button;
					Text localText = button.GetComponentInChildren<Text>();
					this.NeedsList[guiButton] = new KeyValuePair<string, uint>(localType, 0u);
					localText.text = "NeedNotChosen".Loc();
					this.FixFeatures();
					this.UpdateDescription();
					this.UpdateOSList();
					ProductWindow productWindow = HUD.Instance.GetProductWindow("DesignDoc");
					productWindow.SetFilters(true, false, true);
					productWindow.Show(true, string.Format("ProductChooseGeneric".Loc(), localType), delegate(SoftwareProduct[] x)
					{
						if (x.Length > 0)
						{
							this.NeedsList[guiButton] = new KeyValuePair<string, uint>(localType, x[0].ID);
							localText.text = x[0].Name;
							this.FixFeatures();
							this.UpdateDescription();
							this.UpdateOSList();
						}
					}, false, false);
					HashSet<string> hashSet = new HashSet<string>();
					SoftwareType t = this.SelectedType;
					foreach (Feature feature in from x in this.GetFeatures()
					select t.Features[x])
					{
						feature.FindDependencies(localType, hashSet, GameSettings.Instance.SoftwareTypes, t);
					}
					foreach (Feature feature2 in from x in t.Features.Values
					where x.Forced && x.IsCompatible(this.CategoryCombo.SelectedItemString)
					select x)
					{
						feature2.FindDependencies(localType, hashSet, GameSettings.Instance.SoftwareTypes, t);
					}
					productWindow.Features = hashSet.ToArray<string>();
					productWindow.SetType(localType);
					if (localType.Equals(this.SelectedType.OSNeed) && this.SelectedType.OSSpecific && this.OSList.Items.Count > 0)
					{
						productWindow.OSs = (from x in this.OSList.Items
						select ((SoftwareProduct)x).ID).ToArray<uint>();
						productWindow.ApplyFilters();
					}
				});
			}
		}
		this.GenerateFeatures();
		this.UpdateDescription();
		this.SequelTo = null;
		if (updateCat)
		{
			this.OSPanel.SetActive(this.SelectedType.OSSpecific);
			this.OSList.Items.Clear();
		}
		if (!this.priceHasBeenEdited || updateCat)
		{
			this.SelectDefaultPrice();
		}
		List<SimulatedCompany> list = new List<SimulatedCompany>
		{
			null
		};
		foreach (uint id in GameSettings.Instance.MyCompany.Subsidiaries)
		{
			Company company = GameSettings.Instance.simulation.GetCompany(id);
			SimulatedCompany simulatedCompany = company as SimulatedCompany;
			if (simulatedCompany != null && simulatedCompany.Type.Types.Any((KeyValuePair<KeyValuePair<string, string>, float> x) => x.Key.Key.Equals(this.TypeCombo.SelectedItem) && (x.Key.Value == null || x.Key.Value.Equals(this.CategoryCombo.SelectedItem))))
			{
				list.Add(simulatedCompany);
			}
		}
		this.SubsidiaryCombo.UpdateContent<SimulatedCompany>(list);
		this.SubsidiaryCombo.Selected = 0;
		bool active = list.Count > 1;
		this.SubsidiaryCombo.gameObject.SetActive(active);
	}

	public void MinMaxTree(bool max)
	{
		foreach (FeatureElement featureElement in from x in this.FeatureToggles.Values
		where x.MainToggle.interactable
		select x)
		{
			featureElement.MainToggle.isOn = max;
		}
	}

	private void GenerateFeatures()
	{
		this.ClearFeatures();
		SoftwareType type = this.SelectedType;
		Dictionary<RectTransform, HashSet<RectTransform>> dictionary = new Dictionary<RectTransform, HashSet<RectTransform>>();
		Dictionary<string, RectTransform> dictionary2 = new Dictionary<string, RectTransform>();
		string swCat = this.CategoryCombo.SelectedItemString;
		foreach (Feature feature in from x in type.Features.Values
		where (x.Research == null || x.Researched) && x.IsCompatible(swCat)
		orderby (x.Research != null) ? 1 : 0
		select x)
		{
			GameObject gameObject;
			if (feature.Research != null)
			{
				gameObject = this.FeatureToggles[feature.Research].AddFeature(type, feature).gameObject;
			}
			else
			{
				gameObject = UnityEngine.Object.Instantiate<GameObject>(this.FeaturePrefab);
				RectTransform component = gameObject.GetComponent<RectTransform>();
				dictionary2[feature.Name] = component;
				dictionary[component] = new HashSet<RectTransform>();
			}
			FeatureElement component2 = gameObject.GetComponent<FeatureElement>();
			this.FeatureToggles[feature.Name] = component2;
			component2.Init(type, feature);
			component2.MainToggle.isOn = false;
			component2.MainToggle.onValueChanged.AddListener(delegate(bool x)
			{
				this.FixFeatures();
				this.UpdateDescription();
				this.UpdatePieChart();
				this.ServerCombo.GetComponent<Button>().interactable = (this.SelectedType.GetServerReq(this.GetFeatures()) > 0f);
				if (!this.priceHasBeenEdited)
				{
					this.SelectDefaultPrice();
				}
			});
			string[] feature2 = Localization.GetFeature(this.SelectedType, feature.Name);
			component2.Label.text = feature2[0];
		}
		foreach (KeyValuePair<string, RectTransform> keyValuePair in dictionary2)
		{
			Feature feature3 = type.Features[keyValuePair.Key];
			RectTransform key;
			if (feature3.From != null && dictionary2.TryGetValue(feature3.From, out key))
			{
				dictionary[key].Add(keyValuePair.Value);
			}
			foreach (KeyValuePair<string, string> keyValuePair2 in from x in feature3.Dependencies
			where x.Key.Equals(type.Name) && type.Features[x.Value].IsCompatible(swCat)
			select x)
			{
				RectTransform key2;
				if (dictionary2.TryGetValue(keyValuePair2.Value, out key2))
				{
					dictionary[key2].Add(keyValuePair.Value);
				}
			}
		}
		this.FeaturePanel.InitContent(dictionary);
		this.FixFeatures();
	}

	public void SubsidiaryChange()
	{
		if (this._noSubUpdate)
		{
			return;
		}
		Company c = this.Subsidiairy ?? GameSettings.Instance.MyCompany;
		if (this._sequelTo != null && !GameSettings.Instance.CanMakeSequel(this._sequelTo, c))
		{
			this.SequelTo = null;
		}
		if (this.Subsidiairy != null)
		{
			bool flag = false;
			foreach (object obj in this.OSList.Items.ToList<object>())
			{
				SoftwareProduct softwareProduct = obj as SoftwareProduct;
				if (softwareProduct != null && softwareProduct.IsMock)
				{
					this.OSList.Items.Remove(obj);
					flag = true;
				}
			}
			if (flag)
			{
				this.FixFeatures();
			}
		}
		this.ProjManButton.SetActive(this.Subsidiairy == null);
		this.UpdateDescription();
	}

	private void UpdateOSList()
	{
		if (this.SelectedType.OSNeed != null)
		{
			Dictionary<string, uint> needs = this.GetNeeds();
			if (needs != null && needs[this.SelectedType.OSNeed] > 0u)
			{
				List<SoftwareProduct> compatibleOS = GameSettings.Instance.simulation.GetCompatibleOS(needs[this.SelectedType.OSNeed], false);
				foreach (object obj in this.OSList.Items.ToList<object>())
				{
					if (!compatibleOS.Contains((SoftwareProduct)obj))
					{
						this.OSList.Items.Remove(obj);
					}
				}
			}
			this.FixFeatures();
			this.UpdateDescription();
		}
	}

	private float GetLicenseCost()
	{
		Company c = this.Subsidiairy ?? GameSettings.Instance.MyCompany;
		float num = (from x in this.NeedsList
		where x.Key != null && x.Key.gameObject.activeSelf && x.Value.Value > 0u
		select GameSettings.Instance.simulation.GetProduct(x.Value.Value, false) into x
		where x.DevCompany != c && !c.OwnsLicense(x)
		select x).Sum((SoftwareProduct x) => x.GetLicenseCost());
		float num2;
		if (this.SelectedType.OSSpecific)
		{
			num2 = (from x in this.OSList.Items
			select (SoftwareProduct)x into x
			where x.DevCompany != c && !c.OwnsLicense(x)
			select x).Sum((SoftwareProduct x) => x.GetLicenseCost());
		}
		else
		{
			num2 = 0f;
		}
		float num3 = num2;
		return num3 + num;
	}

	private void UpdateDescription()
	{
		this.UpdateTeamText();
		SoftwareType type = this.SelectedType;
		SDateTime time = SDateTime.Now();
		string[] features = this.GetFeatures();
		string[] array = (from x in type.GetBalance(features)
		select this.GetBalanceScore(x).Loc()).ToArray<string>();
		SoftwareProduct sequelTo = (this.SequelTo == null || this.SequelTo.Traded) ? null : this.SequelTo;
		float num = type.DevTime(features, this.CategoryCombo.SelectedItemString, sequelTo);
		float num2 = num;
		SoftwareCategory softwareCategory = type.Categories[this.CategoryCombo.SelectedItemString];
		uint[] oss = this.GetOSs();
		uint reach = type.GetReach(this.CategoryCombo.SelectedItemString, features, oss);
		float num3 = type.CodeArtRatio(features);
		float num4 = num2 * num3;
		if (type.OSSpecific)
		{
			int num5 = Mathf.Max(0, oss.Length - 1);
			num += (float)num5;
			num4 += (float)num5;
		}
		int[] optimalEmployeeCount = type.GetOptimalEmployeeCount(num, num3);
		if (this.DevTeams.Count == 0 || this.Subsidiairy != null)
		{
			num = GameData.ProjectDevTime(optimalEmployeeCount[0], optimalEmployeeCount[1], num, num3);
		}
		else
		{
			int num6 = 0;
			int num7 = 0;
			foreach (string key in this.DevTeams)
			{
				Team orNull = GameSettings.Instance.sActorManager.Teams.GetOrNull(key);
				if (orNull != null)
				{
					int[] array2 = orNull.CountDesignDev();
					num6 += array2[0];
					num7 += array2[1];
				}
			}
			if (num6 == 0 || num7 == 0)
			{
				num = GameData.ProjectDevTime(optimalEmployeeCount[0], optimalEmployeeCount[1], num, num3);
			}
			else
			{
				int designEmployees = num6 + (this.DevTeams.Count - 1) * 2;
				int devEmployees = num7 + (this.DevTeams.Count - 1) * 2;
				num = GameData.ProjectDevTime(designEmployees, devEmployees, num6, num7, num, num3);
			}
		}
		float num8 = GameSettings.Instance.simulation.GetFeatureScore(type.Name, this.CategoryCombo.SelectedItemString, time);
		float num9 = (num8 != 0f) ? (type.FeatureScore(features) / num8) : 1f;
		num9 = Mathf.Clamp01(num9 * num9);
		VarValueSheet descriptionSh = this.DescriptionSh;
		string[] array3 = new string[15];
		array3[0] = this.GetTimeString(num, num2);
		array3[1] = Mathf.Ceil((float)optimalEmployeeCount[0]).ToString();
		array3[2] = Mathf.Ceil((float)optimalEmployeeCount[1] * num3).ToString();
		array3[3] = Mathf.Ceil((float)optimalEmployeeCount[1] * (1f - num3)).ToString();
		array3[4] = num4.ToString("N2");
		array3[5] = (num2 * (1f - num3)).ToString("N2");
		array3[6] = this.GetLicenseCost().Currency(true);
		array3[7] = ((from x in features
		select type.Features[x] into x
		where x.PatentOwner > 0u && x.PatentOwner != GameSettings.Instance.MyCompany.ID
		select x).SumSafe((Feature x) => x.Royalty) * 100f).ToString("F") + "%";
		array3[8] = (type.GetServerReq(features) * reach * softwareCategory.Popularity * softwareCategory.Retention / num2).BandwidthFactor(time).Bandwidth();
		array3[9] = SoftwareType.GetQualityLabel(type.MaxQuality(this.GetNeeds(), features, this.CategoryCombo.SelectedItemString));
		array3[10] = ((int)(num9 * 100f)).ToString() + "%";
		array3[11] = array[0];
		array3[12] = array[1];
		array3[13] = array[2];
		array3[14] = reach.ToString("N0");
		descriptionSh.UpdateValues(array3);
	}

	private void UpdateOnShow()
	{
		this.DescriptionSh.SetData(new string[]
		{
			"ETA".Loc(),
			"Recommended designers".Loc(),
			"Recommended programmers".Loc(),
			"Recommended artists".Loc(),
			"EstUnits".Loc(new object[]
			{
				"Code".Loc()
			}),
			"EstUnits".Loc(new object[]
			{
				"Art".Loc()
			}),
			"License costs".Loc(),
			"Royalties".Loc(),
			"Appx bandwidth".Loc(),
			"Max quality".Loc(),
			"Expected interest".Loc(),
			"Innovation".Loc(),
			"Stability".Loc(),
			"Usability".Loc(),
			"Consumer reach".Loc()
		}, new string[0]);
		this.ProductName.text = this.DefaultName.Loc();
		Dictionary<string, string> dictionary = (from x in GameSettings.Instance.SoftwareTypes
		where !x.Value.OneClient && x.Value.IsUnlocked(TimeOfDay.Instance.Year)
		select x).ToDictionary((KeyValuePair<string, SoftwareType> x) => x.Key, (KeyValuePair<string, SoftwareType> x) => x.Value.Description);
		this.TypeCombo.UpdateContent<string>(dictionary.Keys, dictionary.Values);
		this.TypeCombo.UpdateSelection(0);
		this.TypeCombo.UpdateSelection(0);
		this.OSList.Items.Clear();
		this.DevTeams.Clear();
		this.DevTeams.AddRange(GameSettings.Instance.GetDefaultTeams("Design"));
		this.UpdateTeamText();
		this.UpdateTypeRelatedCombos(true);
		this.UpdatePieChart();
		this.HouseToggle.isOn = false;
		this.SequelTo = null;
		this.ServerCombo.UpdateContent<string>(GameSettings.Instance.GetServerNames());
		this.ServerCombo.Selected = 0;
		this.SCMCombo.UpdateContent<string>(new string[]
		{
			"None".Loc()
		}.Concat(GameSettings.Instance.GetServerNames()));
	}

	private bool CheckValid()
	{
		if (this.GetFeatures().Length == 0)
		{
			WindowManager.Instance.ShowMessageBox("SoftwareFeatureError".Loc(), false, DialogWindow.DialogType.Error);
			return false;
		}
		if (!(from x in GameSettings.Instance.simulation.GetAllProducts()
		select x.Name).Contains(this.ProductName.text))
		{
			if (!(from x in GameSettings.Instance.simulation.Companies.Values.SelectMany((SimulatedCompany x) => x.Releases)
			select x.Name).Contains(this.ProductName.text))
			{
				if (!(from x in GameSettings.Instance.simulation.Companies.Values.SelectMany((SimulatedCompany x) => x.ProjectQueue)
				select x.Name).Contains(this.ProductName.text) && !GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareWorkItem>().Any((SoftwareWorkItem x) => x.SoftwareName.Equals(this.ProductName.text)))
				{
					if (this.SelectedType.OSSpecific && this.OSList.Items.Count == 0)
					{
						WindowManager.Instance.ShowMessageBox("ProductOSError".Loc(), false, DialogWindow.DialogType.Error);
						return false;
					}
					using (IEnumerator<uint> enumerator = (from x in this.NeedsList
					where x.Key.gameObject.activeSelf
					select x.Value.Value).GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							if (enumerator.Current == 0u)
							{
								WindowManager.Instance.ShowMessageBox("ProductNeedError".Loc(), false, DialogWindow.DialogType.Error);
								return false;
							}
						}
					}
					return true;
				}
			}
		}
		WindowManager.Instance.ShowMessageBox("DesignProductNameError".Loc(), false, DialogWindow.DialogType.Error);
		return false;
	}

	public Dictionary<string, uint> GetNeeds()
	{
		return (from x in this.NeedsList
		where x.Key != null && x.Key.gameObject != null && x.Key.gameObject.activeSelf
		select x).ToDictionary((KeyValuePair<Button, KeyValuePair<string, uint>> x) => x.Value.Key, (KeyValuePair<Button, KeyValuePair<string, uint>> x) => x.Value.Value);
	}

	public Dictionary<string, SoftwareProduct> GetNeedProducts()
	{
		return (from x in this.NeedsList
		where x.Value.Value > 0u && x.Key != null && x.Key.gameObject != null && x.Key.gameObject.activeSelf
		select x).ToDictionary((KeyValuePair<Button, KeyValuePair<string, uint>> x) => x.Value.Key, (KeyValuePair<Button, KeyValuePair<string, uint>> x) => GameSettings.Instance.simulation.GetProduct(x.Value.Value, false));
	}

	private uint[] GetOSs()
	{
		return (from x in this.OSList.Items
		select ((SoftwareProduct)x).ID).ToArray<uint>();
	}

	private SoftwareProduct[] GetOSProducts()
	{
		return this.OSList.Items.OfType<SoftwareProduct>().ToArray<SoftwareProduct>();
	}

	private SoftwareWorkItem Create(float licenseCost, bool tut)
	{
		string server = (this.SCMCombo.Selected >= 1) ? this.SCMCombo.SelectedItemString : null;
		return DesignDocument.CreateWork(this.ProductName.text, this.SelectedType.Name, this.CategoryCombo.SelectedItemString, this.GetNeeds(), this.GetOSs(), this.Price, SDateTime.Now(), GameSettings.Instance.MyCompany, (this.SequelTo != null) ? new uint?(this.SequelTo.ID) : null, this.HouseToggle.isOn, licenseCost, this.GetFeatures(), null, this.ServerCombo.SelectedItemString, server, tut);
	}

	private string CheckCompetency()
	{
		if (this.Subsidiairy != null)
		{
			return null;
		}
		if (this.DevTeams.Count > 0)
		{
			List<string> list = this.GetFeatures().SelectNotNull((string x) => this.SelectedType.Features[x].GetCategory(true)).Distinct<string>().ToList<string>();
			for (int i = 0; i < list.Count; i++)
			{
				string text = list[i];
				bool flag = false;
				bool flag2 = false;
				foreach (Team team in this.DevTeams.SelectNotNull((string x) => GameSettings.Instance.sActorManager.Teams.GetOrNull(x)))
				{
					List<Actor> employeesDirect = team.GetEmployeesDirect();
					for (int j = 0; j < employeesDirect.Count; j++)
					{
						Actor actor = employeesDirect[j];
						if (actor.employee.IsRole(Employee.RoleBit.Designer))
						{
							flag = true;
							if (actor.employee.GetSpecialization(Employee.EmployeeRole.Designer, text, true) > 0.1f)
							{
								flag2 = true;
								break;
							}
						}
					}
					if (flag2)
					{
						break;
					}
				}
				if (!flag)
				{
					return "Design";
				}
				if (!flag2)
				{
					return text;
				}
			}
			return null;
		}
		return null;
	}

	private void CheckName(bool instaDevelop)
	{
		if (this.ProductName.text.Equals(this.DefaultName.Loc()))
		{
			WindowManager.Instance.ShowMessageBox("DesignProductNameHint".Loc(), false, DialogWindow.DialogType.Question, delegate
			{
				this.CheckInHouse(instaDevelop);
			}, null, null);
		}
		else
		{
			this.CheckInHouse(instaDevelop);
		}
	}

	private void CheckInHouse(bool instaDevelop)
	{
		SimulatedCompany subsidiairy = this.Subsidiairy;
		if (subsidiairy != null)
		{
			float licenseCost = this.GetLicenseCost();
			if (subsidiairy.CanMakeTransaction(-licenseCost))
			{
				foreach (SoftwareProduct softwareProduct in from x in this.NeedsList
				where x.Key.gameObject.activeSelf
				select GameSettings.Instance.simulation.GetProduct(x.Value.Value, false))
				{
					softwareProduct.TransferLicense(subsidiairy, SDateTime.Now());
				}
				if (this.SelectedType.OSSpecific)
				{
					foreach (SoftwareProduct softwareProduct2 in from x in this.OSList.Items
					select (SoftwareProduct)x)
					{
						softwareProduct2.TransferLicense(subsidiairy, SDateTime.Now());
					}
				}
				SoftwareType selectedType = this.SelectedType;
				string[] features = this.GetFeatures();
				float devTime = selectedType.DevTime(features, this.CategoryCombo.SelectedItemString, this.SequelTo);
				KeyValuePair<float, float> keyValuePair = subsidiairy.Premarket(devTime);
				subsidiairy.ProjectQueue.Add(new SimulatedCompany.ProductPrototype(this.ProductName.text, selectedType.Name, this.CategoryCombo.SelectedItemString, this.GetNeeds(), this.GetOSs(), subsidiairy.PickQuality(false, true), subsidiairy.PickQuality(true, true), subsidiairy.PickQuality(false, false), subsidiairy.PickQuality(true, false), this.Price, subsidiairy, this.HouseToggle.isOn, keyValuePair.Key, (this.SequelTo != null) ? new uint?(this.SequelTo.ID) : null, features, licenseCost + keyValuePair.Value));
				WindowManager.Instance.ShowMessageBox("SubsidiaryDelegationConfirm".Loc(new object[]
				{
					this.ProductName.text,
					subsidiairy.Name
				}), false, DialogWindow.DialogType.Information);
				this.ToggleVisible();
			}
			else
			{
				WindowManager.Instance.ShowMessageBox("ProductLicenseError".Loc(), false, DialogWindow.DialogType.Error);
			}
			return;
		}
		if (this.HouseToggle.isOn)
		{
			WindowManager.Instance.ShowMessageBox("InHouseWarning".Loc(), false, DialogWindow.DialogType.Question, delegate
			{
				this.CheckMockOS(instaDevelop);
			}, null, null);
		}
		else
		{
			this.CheckMockOS(instaDevelop);
		}
	}

	private void CheckMockOS(bool instaDevelop)
	{
		bool flag = (from x in this.GetOSs()
		select GameSettings.Instance.simulation.GetProduct(x, true)).Any((SoftwareProduct x) => x.IsMock);
		if (flag)
		{
			WindowManager.Instance.ShowMessageBox("OSMockConfirm".Loc(), false, DialogWindow.DialogType.Question, delegate
			{
				this.CheckSize(instaDevelop);
			}, null, null);
		}
		else
		{
			this.CheckSize(instaDevelop);
		}
	}

	private void CheckSize(bool instaDevelop)
	{
		if (this.DevTeams.Count > 0 && !this.AutoDev)
		{
			int num = 0;
			int num2 = 0;
			foreach (string key in this.DevTeams)
			{
				Team orNull = GameSettings.Instance.sActorManager.Teams.GetOrNull(key);
				if (orNull != null)
				{
					int[] array = orNull.CountDesignDev();
					num += array[0];
					num2 += array[1];
				}
			}
			SoftwareType selectedType = this.SelectedType;
			string[] features = this.GetFeatures();
			float artRatio = selectedType.CodeArtRatio(features);
			SoftwareProduct sequelTo = (this.SequelTo == null || this.SequelTo.Traded) ? null : this.SequelTo;
			float num3 = selectedType.DevTime(features, this.CategoryCombo.SelectedItemString, sequelTo);
			int[] optimalEmployeeCount = selectedType.GetOptimalEmployeeCount(num3, artRatio);
			float num4 = GameData.ProjectDevTime(num, num2, num3, artRatio);
			num3 = GameData.ProjectDevTime(optimalEmployeeCount[0], optimalEmployeeCount[1], num3, artRatio);
			if (num4 / num3 > 2f)
			{
				string msg = (num >= optimalEmployeeCount[0]) ? "DesignProductTeamSizeHintLarge".Loc() : "DesignProductTeamSizeHint".Loc();
				WindowManager.Instance.ShowMessageBox(msg, false, DialogWindow.DialogType.Question, delegate
				{
					this.AutoDevTest(instaDevelop);
				}, null, null);
			}
			else
			{
				if (num > optimalEmployeeCount[0])
				{
					HintController.Instance.Show(HintController.Hints.HintTeamSizeEffectiveness);
				}
				this.AutoDevTest(instaDevelop);
			}
		}
		else
		{
			this.AutoDevTest(instaDevelop);
		}
	}

	public void AutoDevTest(bool instaDevelop)
	{
		if (this.AutoDev)
		{
			List<AutoDevWorkItem> projs = GameSettings.Instance.MyCompany.WorkItems.OfType<AutoDevWorkItem>().ToList<AutoDevWorkItem>();
			if (projs.Count > 0)
			{
				SelectorController.Instance.selectWindow.Show("Project management".Loc(), from x in projs
				select x.Name, delegate(int i)
				{
					this.DevelopNow(projs[i], instaDevelop);
				}, false, true, true, false, null);
			}
			else
			{
				WindowManager.Instance.ShowMessageBox("DesignDocumentAutoDevWarning".Loc(), false, DialogWindow.DialogType.Error);
			}
		}
		else
		{
			this.DevelopNow(null, instaDevelop);
		}
	}

	public void DevelopClick(bool instaDevelop)
	{
		this.AutoDev = false;
		this.UpdateTeamText();
		if (this.CheckValid())
		{
			string text = this.CheckCompetency();
			if (text == null)
			{
				this.CheckName(instaDevelop);
			}
			else if (text.Equals("Design"))
			{
				WindowManager.Instance.ShowMessageBox("DesignProductAssignHint".Loc(new object[]
				{
					text.Loc()
				}), false, DialogWindow.DialogType.Question, delegate
				{
					this.CheckName(instaDevelop);
				}, null, null);
			}
			else
			{
				WindowManager.Instance.ShowMessageBox("DesignProductFeatureHint2".Loc(new object[]
				{
					text.Loc()
				}), false, DialogWindow.DialogType.Question, delegate
				{
					this.CheckName(instaDevelop);
				}, null, null);
			}
		}
	}

	public void AutoDevClick()
	{
		this.AutoDev = true;
		if (this.CheckValid())
		{
			this.CheckName(false);
		}
	}

	public void AddOS()
	{
		SoftwareType t = this.SelectedType;
		ProductWindow productWindow = HUD.Instance.GetProductWindow("DesignDoc");
		productWindow.Show(true, "ProductChooseOS".Loc(), delegate(SoftwareProduct[] xs)
		{
			if (xs.Length > 0)
			{
				foreach (SoftwareProduct softwareProduct in xs)
				{
					if (softwareProduct != null && !this.OSList.Items.Contains(softwareProduct))
					{
						this.OSList.Items.Add(softwareProduct);
					}
				}
				this.FixFeatures();
				this.UpdateDescription();
				this.UpdatePieChart();
			}
		}, true, true);
		productWindow.WithMock = (this.Subsidiairy == null);
		productWindow.SetFilters(true, false, t.OSLimit == null);
		HashSet<string> hashSet = new HashSet<string>();
		foreach (Feature feature in from x in this.GetFeatures()
		select t.Features[x])
		{
			feature.FindDependencies("Operating System", hashSet, GameSettings.Instance.SoftwareTypes, t);
		}
		foreach (Feature feature2 in from x in t.Features.Values
		where x.Forced && x.IsCompatible(this.CategoryCombo.SelectedItemString)
		select x)
		{
			feature2.FindDependencies("Operating System", hashSet, GameSettings.Instance.SoftwareTypes, t);
		}
		productWindow.Features = hashSet.ToArray<string>();
		productWindow.SetType("Operating System");
		if (t.OSLimit != null)
		{
			productWindow.SetCategory(t.OSLimit);
		}
		Dictionary<string, uint> needs = this.GetNeeds();
		if (this.SelectedType.OSNeed != null && needs != null && needs[this.SelectedType.OSNeed] > 0u)
		{
			productWindow.SetContent(GameSettings.Instance.simulation.GetCompatibleOS(needs[this.SelectedType.OSNeed], this.Subsidiairy == null));
		}
	}

	public void RemoveOS()
	{
		if (this.OSList.Selected.Count > 0)
		{
			List<int> source = this.OSList.Selected.ToList<int>();
			this.OSList.Selected.Clear();
			foreach (int num in from x in source
			orderby x descending
			select x)
			{
				if (num < this.OSList.Items.Count)
				{
					this.OSList.Items.RemoveAt(num);
				}
			}
			this.FixFeatures();
			this.UpdateDescription();
			this.UpdatePieChart();
		}
	}

	public void SelectIP()
	{
		ProductWindow productWindow = HUD.Instance.GetProductWindow("DesignDoc");
		productWindow.SetFilters(false, false, true);
		productWindow.Show(true, "ProductChooseSequel".Loc(), delegate(SoftwareProduct[] x)
		{
			this.SequelTo = ((x.Length != 0) ? x[0] : null);
		}, false, false);
		Company c = this.Subsidiairy ?? GameSettings.Instance.MyCompany;
		List<SoftwareProduct> content = (from x in c.Products
		where x._type.Equals(this.TypeCombo.SelectedItemString) && x._category.Equals(this.CategoryCombo.SelectedItemString) && GameSettings.Instance.CanMakeSequel(x, c)
		select x).ToList<SoftwareProduct>();
		productWindow.SetContent(content);
	}

	public void RandomName()
	{
		this.ProductName.text = ((this.SequelTo != null) ? GameSettings.Instance.simulation.GenerateProductSequalName(this.SequelTo.Name) : GameSettings.Instance.simulation.GenerateProductName(this.SelectedType, this.CategoryCombo.SelectedItemString));
	}

	public void DevelopNow(AutoDevWorkItem autoDev, bool instaDevelop)
	{
		if (autoDev == null && instaDevelop && this.SelectedType.Modded)
		{
			WindowManager.SpawnInputDialog("Quality/Bugs/Followers", "Debug", "1.0/100/1,000,000", delegate(string x)
			{
				string[] array = x.Split(new char[]
				{
					'/'
				});
				float num = Mathf.Sqrt((float)Convert.ToDecimal(array[0]));
				int bugs = Convert.ToInt32(array[1].Replace(",", string.Empty));
				uint followers = Convert.ToUInt32(array[2].Replace(",", string.Empty));
				SoftwareProduct softwareProduct3 = new SoftwareProduct(this.ProductName.text, this.SelectedType.Name, this.CategoryCombo.SelectedItemString, this.GetNeeds(), this.GetOSs(), num, num, num, num, this.Price, SDateTime.Now(), SDateTime.Now(), bugs, this.HouseToggle.isOn, GameSettings.Instance.MyCompany, (this.SequelTo != null) ? new uint?(this.SequelTo.ID) : null, GameSettings.Instance.simulation.GetID(), 0f, this.GetFeatures(), this.ServerCombo.SelectedItemString, followers, null);
				GameSettings.Instance.MyCompany.Products.Add(softwareProduct3);
				GameSettings.Instance.simulation.AddProduct(softwareProduct3, false);
				SupportWork supportWork = new SupportWork(softwareProduct3.ID, softwareProduct3.Name, -1);
				GameSettings.Instance.MyCompany.WorkItems.Add(supportWork);
				foreach (string key in GameSettings.Instance.GetDefaultTeams("Support"))
				{
					supportWork.AddDevTeam(GameSettings.Instance.sActorManager.Teams[key], false);
				}
			}, null);
			this.ToggleVisible();
			return;
		}
		float licenseCost = this.GetLicenseCost();
		if (GameSettings.Instance.MyCompany.CanMakeTransaction(-licenseCost))
		{
			foreach (SoftwareProduct softwareProduct in from x in this.NeedsList
			where x.Key.gameObject.activeSelf
			select GameSettings.Instance.simulation.GetProduct(x.Value.Value, false))
			{
				softwareProduct.TransferLicense(GameSettings.Instance.MyCompany, SDateTime.Now());
			}
			if (this.SelectedType.OSSpecific)
			{
				foreach (SoftwareProduct softwareProduct2 in from x in this.OSList.Items
				select (SoftwareProduct)x)
				{
					softwareProduct2.TransferLicense(GameSettings.Instance.MyCompany, SDateTime.Now());
				}
			}
			GameSettings.Instance.TeamDefaults["Design"] = this.DevTeams.ToHashSet<string>();
			SoftwareWorkItem softwareWorkItem = this.Create(licenseCost, autoDev == null);
			GameSettings.Instance.MyCompany.WorkItems.Add(softwareWorkItem);
			if (autoDev != null)
			{
				autoDev.AssignProject(softwareWorkItem);
			}
			else
			{
				foreach (Team team in this.DevTeams.SelectNotNull((string x) => GameSettings.Instance.sActorManager.Teams.GetOrNull(x)))
				{
					softwareWorkItem.AddDevTeam(team, false);
				}
			}
			this.ToggleVisible();
		}
		else
		{
			WindowManager.Instance.ShowMessageBox("ProductLicenseError".Loc(), false, DialogWindow.DialogType.Error);
		}
	}

	public GUIWindow Window;

	public InputField ProductName;

	public InputField PriceText;

	public GUICombobox TypeCombo;

	public GUICombobox ServerCombo;

	public GUICombobox SCMCombo;

	public GUICombobox CategoryCombo;

	public GUICombobox SubsidiaryCombo;

	public GUIPieChart Pie;

	public GUILegend Legend;

	public Button NameButton;

	public Button IPButton;

	public GameObject ProjManButton;

	public Toggle HouseToggle;

	public Text IPButtonText;

	public Text TeamText;

	public VarValueSheet DescriptionSh;

	public GUIListView OSList;

	public GridLayoutGroup NeedsGrid;

	public RectTransform NeedsRect;

	public Scrollbar FeatureScroll1;

	public Scrollbar FeatureScroll2;

	public GameObject ButtonPrefab;

	public GameObject LabelPrefab;

	public GameObject FeaturePrefab;

	[NonSerialized]
	public Dictionary<Button, KeyValuePair<string, uint>> NeedsList = new Dictionary<Button, KeyValuePair<string, uint>>();

	[NonSerialized]
	public Dictionary<string, FeatureElement> FeatureToggles = new Dictionary<string, FeatureElement>();

	[NonSerialized]
	public Dictionary<Button, GameObject> NeedLabels = new Dictionary<Button, GameObject>();

	public GameObject NeedsPanel;

	public GameObject NeedsSubPanel;

	public GameObject OSPanel;

	public DependencyLayoutGroup FeaturePanel;

	public string DefaultName = "StandardProductName";

	public static string[] BalanceNames = new string[]
	{
		"BalanceAmount1",
		"BalanceAmount2",
		"BalanceAmount3",
		"BalanceAmount4",
		"BalanceAmount5"
	};

	[NonSerialized]
	public HashSet<string> DevTeams = new HashSet<string>();

	[NonSerialized]
	private SoftwareProduct _sequelTo;

	[NonSerialized]
	private bool priceHasBeenEdited;

	private bool FixingFeatures;

	private bool _noSubUpdate;

	public bool AutoDev;
}
