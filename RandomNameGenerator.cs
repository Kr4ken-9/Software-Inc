﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class RandomNameGenerator
{
	public RandomNameGenerator(string start, Dictionary<string, string[]> groups, Dictionary<string, string[]> jumps, bool replace)
	{
		this.Start = start;
		this.Groups = groups;
		this.Chances = groups.ToDictionary((KeyValuePair<string, string[]> x) => x.Key, (KeyValuePair<string, string[]> x) => x.Value.SelectInPlace((string y) => new RandomNameGenerator.RandomWord(y)));
		this.Jumps = jumps;
		this.Replace = replace;
	}

	public RandomNameGenerator()
	{
	}

	public RandomNameGenerator Clone()
	{
		return new RandomNameGenerator(this.Start, this.Groups.ToDictionary((KeyValuePair<string, string[]> x) => x.Key, (KeyValuePair<string, string[]> x) => x.Value), this.Jumps.ToDictionary((KeyValuePair<string, string[]> x) => x.Key, (KeyValuePair<string, string[]> x) => x.Value), this.Replace);
	}

	public void MergeWith(RandomNameGenerator rng)
	{
		foreach (KeyValuePair<string, string[]> keyValuePair in rng.Jumps)
		{
			if (this.Jumps.ContainsKey(keyValuePair.Key))
			{
				this.Jumps[keyValuePair.Key] = this.Jumps[keyValuePair.Key].Concat(keyValuePair.Value).ToArray<string>();
			}
			else
			{
				this.Jumps.Add(keyValuePair.Key, keyValuePair.Value);
			}
		}
		foreach (KeyValuePair<string, string[]> keyValuePair2 in rng.Groups)
		{
			if (this.Groups.ContainsKey(keyValuePair2.Key))
			{
				this.Groups[keyValuePair2.Key] = this.Groups[keyValuePair2.Key].Concat(keyValuePair2.Value).ToArray<string>();
			}
			else
			{
				this.Groups.Add(keyValuePair2.Key, keyValuePair2.Value);
			}
			this.Chances[keyValuePair2.Key] = this.Groups[keyValuePair2.Key].SelectInPlace((string x) => new RandomNameGenerator.RandomWord(x));
		}
	}

	public static RandomNameGenerator Load(string[] lines)
	{
		Dictionary<string, string[]> dictionary = new Dictionary<string, string[]>();
		Dictionary<string, List<string>> dictionary2 = new Dictionary<string, List<string>>();
		string key = string.Empty;
		string text = null;
		bool replace = false;
		foreach (string text2 in lines)
		{
			if ("[REPLACE]".Equals(text2))
			{
				replace = true;
			}
			else if (text2.Length > 0 && text2[0] == '-')
			{
				string[] array = text2.Split(new char[]
				{
					'('
				});
				string text3 = array[0].Substring(1).Trim().ToUpper();
				string[] value = array[1].Substring(0, array[1].Length - 1).Split(new char[]
				{
					','
				}).SelectInPlace((string x) => x.Trim().ToUpper());
				dictionary2[text3] = new List<string>();
				dictionary[text3] = value;
				key = text3;
				if (text == null)
				{
					text = text3;
				}
			}
			else
			{
				dictionary2[key].Add(text2);
			}
		}
		return new RandomNameGenerator(text, dictionary2.ToDictionary((KeyValuePair<string, List<string>> x) => x.Key, (KeyValuePair<string, List<string>> y) => y.Value.ToArray()), dictionary, replace);
	}

	private string GetValue(string group)
	{
		RandomNameGenerator.RandomWord[] array;
		if (!this.Chances.TryGetValue(group, out array))
		{
			Debug.Log("Got invalid jump in name generator: " + group);
			return string.Empty;
		}
		if (array == null || array.Length == 0)
		{
			return string.Empty;
		}
		int num = (int)Mathf.Clamp(Utilities.RandomRange(0f, (float)array.Length * 0.1f), 0f, (float)(array.Length - 1));
		RandomNameGenerator.RandomWord randomWord = (from x in array
		orderby x.Chance descending, Utilities.RandomValue
		select x).ToArray<RandomNameGenerator.RandomWord>()[num];
		randomWord.Chance--;
		if (randomWord.Chance == 0)
		{
			randomWord.Chance = 100;
		}
		return randomWord.Word;
	}

	public string GenerateName()
	{
		int num = 0;
		StringBuilder stringBuilder = new StringBuilder();
		string text = this.Start;
		while (text != null && num < this.Groups.Count && !"STOP".Equals(text))
		{
			stringBuilder.Append(this.GetValue(text));
			string[] array;
			if (!this.Jumps.TryGetValue(text, out array) || array.Length == 0)
			{
				break;
			}
			text = array.GetRandom<string>();
			num++;
		}
		return stringBuilder.ToString();
	}

	private readonly string Start;

	private readonly Dictionary<string, string[]> Groups;

	private readonly Dictionary<string, string[]> Jumps;

	private Dictionary<string, RandomNameGenerator.RandomWord[]> Chances;

	public bool Replace;

	[Serializable]
	public class RandomWord
	{
		public RandomWord()
		{
		}

		public RandomWord(string word)
		{
			this.Word = word;
			this.Chance = 100;
		}

		public override string ToString()
		{
			return this.Word + ": " + this.Chance.ToString();
		}

		public string Word;

		public int Chance;
	}
}
