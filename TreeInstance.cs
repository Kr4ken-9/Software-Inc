﻿using System;
using UnityEngine;

[Serializable]
public class TreeInstance : IHasVector
{
	public TreeInstance()
	{
	}

	public TreeInstance(Vector3 pos, Quaternion rot, int idx)
	{
		this.Position = pos;
		this.Rotation = rot;
		this.Idx = idx;
		this.LeaveOffset = UnityEngine.Random.value;
	}

	public StaticTree TreeMesh
	{
		get
		{
			return (!(GameSettings.Instance == null)) ? GameSettings.Instance.CachedTrees[this.Idx % GameSettings.Instance.CachedTrees.Length] : null;
		}
	}

	public Bounds Bounds
	{
		get
		{
			Bounds bounds = this.TreeMesh.bounds;
			return new Bounds(bounds.center + this.Position, bounds.size);
		}
	}

	public Matrix4x4 Transform
	{
		get
		{
			return Matrix4x4.TRS(this.Position, this.Rotation, Vector3.one);
		}
	}

	public Vector2 GetPos()
	{
		return this.Position.ToVector2Z();
	}

	public SVector3 Position;

	public SVector3 Rotation;

	public int Idx;

	public float LeaveOffset;

	[NonSerialized]
	public TreeBatch BelongsTo;
}
