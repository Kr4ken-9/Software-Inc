﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ProductWindow : MonoBehaviour
{
	public void Init()
	{
		this.CanApplyFilters = true;
	}

	public void ShowProductDetails(SoftwareProduct product)
	{
		ProductDetailWindow productDetailWindow = WindowManager.FindWindowType<ProductDetailWindow>().FirstOrDefault((ProductDetailWindow x) => x.product == product);
		if (productDetailWindow != null)
		{
			WindowManager.Focus(productDetailWindow.Window);
			return;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ProductDetailWindow);
		gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		ProductDetailWindow component = gameObject.GetComponent<ProductDetailWindow>();
		component.Window.Modal = this.Window.Modal;
		component.Init(product);
	}

	public void Show(bool NotMe, string title, Action<SoftwareProduct[]> close, bool multi = false, bool OS = false)
	{
		this.WithMock = false;
		this.CategoryDisabled = false;
		this.Content = null;
		this.OSs = null;
		this.PortFilter = null;
		this.ProductList.MultiSelect = multi;
		this.ProductList.Selected.Clear();
		this.ProductList.ResetScroll();
		this.ResetFilters();
		this.OnCloseAction = close;
		this.ProductList["ProductCompany"].gameObject.SetActive(NotMe);
		this.ProductList["ProductCost"].gameObject.SetActive(!NotMe || this.OnCloseAction != null);
		this.ProductList["ProductLastMonth"].gameObject.SetActive(!NotMe);
		this.ProductList["ProductRefunds"].gameObject.SetActive(!NotMe);
		this.ProductList["ProductCopies"].gameObject.SetActive(!NotMe);
		this.ProductList["ProductStorage"].gameObject.SetActive(!NotMe);
		this.MarketButton.SetActive(!NotMe);
		this.PortButton.SetActive(!NotMe);
		this.CopyOrderButton.SetActive(!NotMe);
		this.PrintButton.SetActive(!NotMe);
		this.SupportButton.SetActive(!NotMe);
		this.SequelButton.SetActive(!NotMe);
		this._title = title;
		this.Window.NonLocTitle = this._title;
		if (this.OnCloseAction != null)
		{
			this.OKButton.gameObject.SetActive(true);
			this.CancelButton.gameObject.SetActive(true);
			this.Window.Modal = true;
		}
		else
		{
			this.OKButton.gameObject.SetActive(false);
			this.CancelButton.gameObject.SetActive(false);
			this.Window.Modal = false;
		}
		this.Window.Show(true, true);
	}

	public void Show(bool NotMe, string title, bool multi = false)
	{
		this.ProductList.Selected.Clear();
		this.ProductList.ResetScroll();
		this.Show(NotMe, title, null, multi, false);
	}

	public void SetCompany(string company)
	{
		this.CompanyFilter = company;
		this.ApplyFilters();
	}

	public void SetContent(IEnumerable<SoftwareProduct> items)
	{
		this.Content = items.ToList<SoftwareProduct>();
		this.ApplyFilters();
	}

	public void SetType(string type)
	{
		this.SWTypeFilter = type;
		this.ApplyFilters();
	}

	public void SetCategory(string category)
	{
		this.SWCategoryFilter = category;
		this.ApplyFilters();
	}

	public void SetFilters(bool company, bool type, bool category = true)
	{
		if (!type)
		{
			GUIColumn guicolumn = this.ProductList["ProductNeedType"];
			if (guicolumn.FilterActive)
			{
				guicolumn.ToggleFilter();
			}
		}
		if (!category || !type)
		{
			GUIColumn guicolumn2 = this.ProductList["ProductType"];
			if (guicolumn2.FilterActive)
			{
				guicolumn2.ToggleFilter();
			}
		}
		this.CategoryDisabled = !category;
	}

	public void ApplyFilters()
	{
		if (!this.CanApplyFilters)
		{
			return;
		}
		List<SoftwareProduct> source = this.Content ?? ((!this.WithMock) ? GameSettings.Instance.simulation.GetAllProducts().ToList<SoftwareProduct>() : GameSettings.Instance.simulation.GetProductsWithMock().ToList<SoftwareProduct>());
		bool ignoreType = this.SWTypeFilter == null;
		bool ignoreFeats = this.Features == null;
		bool ignoreCat = this.SWCategoryFilter == null;
		this.ProductList.Items = source.Where(delegate(SoftwareProduct x)
		{
			if (this.CompanyFilter != null && !x.DevCompany.Name.Equals(this.CompanyFilter))
			{
				return false;
			}
			if (!ignoreType && !x._type.Equals(this.SWTypeFilter))
			{
				return false;
			}
			if (!ignoreFeats)
			{
				for (int i = 0; i < this.Features.Length; i++)
				{
					if (!SoftwareType.DependencyMet(this.Features[i], x))
					{
						return false;
					}
				}
			}
			return (ignoreCat || x._category.Equals(this.SWCategoryFilter)) && (this.OSs == null || x.OSs == null || x.OSs.Any((uint z) => this.OSs.Contains(z))) && (this.PortFilter == null || !this.PortFilter.OSs.Contains(x.ID));
		}).Cast<object>().ToList<object>();
		this.Window.NonLocTitle = string.Concat(new object[]
		{
			this._title,
			"(",
			this.ProductList.Items.Count,
			")"
		});
	}

	public void ResetFilters()
	{
		this.Features = null;
		this.CompanyFilter = null;
		this.SWCategoryFilter = null;
		this.SWTypeFilter = null;
	}

	public void PressOK()
	{
		this.OnCloseAction(this.ProductList.GetSelected<SoftwareProduct>());
		this.Window.Close();
	}

	public void MarketClick()
	{
		SoftwareProduct softwareProduct = this.ProductList.GetSelected<SoftwareProduct>().FirstOrDefault<SoftwareProduct>();
		if (softwareProduct != null)
		{
			if (!softwareProduct.InHouse)
			{
				HUD.Instance.marketingWindow.Show(softwareProduct);
			}
			else
			{
				WindowManager.Instance.ShowMessageBox("InHouseMarketing".Loc(), false, DialogWindow.DialogType.Error);
			}
		}
	}

	public void DetailClick()
	{
		foreach (SoftwareProduct product in this.ProductList.GetSelected<SoftwareProduct>())
		{
			this.ShowProductDetails(product);
		}
	}

	public void TradeClick()
	{
		SoftwareProduct softwareProduct = this.ProductList.GetSelected<SoftwareProduct>().FirstOrDefault<SoftwareProduct>();
		if (softwareProduct != null && !softwareProduct.IsMock)
		{
			IPDeal deal = new IPDeal(softwareProduct);
			if (softwareProduct.DevCompany.Player)
			{
				Company buyer = GameSettings.Instance.simulation.FindBuyer(deal);
				if (buyer != null)
				{
					WindowManager.Instance.ShowMessageBox("IPDealActionSell".Loc(new object[]
					{
						buyer.Name,
						deal.Worth().Currency(true)
					}), true, DialogWindow.DialogType.Question, delegate
					{
						deal.Accept(buyer);
					}, null, null);
				}
				else
				{
					WindowManager.Instance.ShowMessageBox("IPDealActionFail".Loc(), false, DialogWindow.DialogType.Information);
				}
			}
			else
			{
				bool flag = false;
				if (softwareProduct.DevCompany.IsPlayerOwned())
				{
					flag = true;
				}
				else
				{
					HashSet<SoftwareProduct> hashSet = (from x in deal._products
					select GameSettings.Instance.simulation.GetProduct(x, true)).ToHashSet<SoftwareProduct>();
					for (int i = 0; i < softwareProduct.DevCompany.Products.Count; i++)
					{
						SoftwareProduct softwareProduct2 = softwareProduct.DevCompany.Products[i];
						if (!hashSet.Contains(softwareProduct2) && !softwareProduct2.Traded)
						{
							flag = true;
							break;
						}
					}
				}
				if (!flag)
				{
					WindowManager.Instance.ShowMessageBox("IPAIBadDeal".Loc(new object[]
					{
						softwareProduct.DevCompany.Name
					}), false, DialogWindow.DialogType.Information);
				}
				else if (GameSettings.Instance.MyCompany.CanMakeTransaction(-deal.Worth()))
				{
					if (deal.Worth() == 0f)
					{
						deal.Accept(GameSettings.Instance.MyCompany);
					}
					else
					{
						WindowManager.Instance.ShowMessageBox("IPDealActionBuy".Loc(new object[]
						{
							deal.Worth().Currency(true)
						}), true, DialogWindow.DialogType.Question, delegate
						{
							deal.Accept(GameSettings.Instance.MyCompany);
						}, null, null);
					}
				}
				else
				{
					WindowManager.Instance.ShowMessageBox("IPDealActionBuyFail".Loc(new object[]
					{
						softwareProduct.DevCompany.Name,
						deal.Worth().Currency(true)
					}), false, DialogWindow.DialogType.Information);
				}
			}
		}
	}

	public void PortClick()
	{
		SoftwareProduct softwareProduct = this.ProductList.GetSelected<SoftwareProduct>().FirstOrDefault<SoftwareProduct>();
		if (softwareProduct != null)
		{
			ProductWindow.StartPort(softwareProduct);
		}
	}

	public static void StartPort(SoftwareProduct p)
	{
		if (p.Type.OSSpecific && GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwarePort>().None((SoftwarePort z) => z.Product == p))
		{
			HUD.Instance.TeamSelectWindow.Show(GameSettings.Instance.GetDefaultTeams("Porting"), null, delegate(string[] ts, SimulatedCompany c)
			{
				ProductWindow productWindow = HUD.Instance.GetProductWindow("Port");
				productWindow.Show(true, "Port".Loc(), delegate(SoftwareProduct[] xs)
				{
					if (xs.Length > 0)
					{
						float num = xs.Sum((SoftwareProduct z) => z.GetLicenseCost(GameSettings.Instance.MyCompany));
						if (GameSettings.Instance.MyCompany.CanMakeTransaction(-num))
						{
							for (int i = 0; i < xs.Length; i++)
							{
								xs[i].TransferLicense(GameSettings.Instance.MyCompany, SDateTime.Now());
							}
							p.Loss += num;
							SoftwarePort softwarePort = new SoftwarePort(p, xs);
							GameSettings.Instance.MyCompany.WorkItems.Add(softwarePort);
							if (c != null)
							{
								softwarePort.CompanyWorker = c;
							}
							else
							{
								string[] ts;
								foreach (string key in ts)
								{
									softwarePort.AddDevTeam(GameSettings.Instance.sActorManager.Teams[key], false);
								}
							}
						}
						else
						{
							WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), false, DialogWindow.DialogType.Error);
						}
					}
				}, true, true);
				productWindow.WithMock = true;
				string oslimit = p.Type.OSLimit;
				productWindow.SetFilters(true, false, oslimit == null);
				productWindow.PortFilter = p;
				HashSet<string> hashSet = new HashSet<string>();
				SoftwareType t = p.Type;
				foreach (Feature feature in from z in p.Features
				select t.Features[z])
				{
					feature.FindDependencies("Operating System", hashSet, GameSettings.Instance.SoftwareTypes, t);
				}
				productWindow.Features = hashSet.ToArray<string>();
				productWindow.SetType("Operating System");
				if (oslimit != null)
				{
					productWindow.SetCategory(oslimit);
				}
				Dictionary<string, uint> needs = p.Needs;
				if (t.OSNeed != null && needs != null && needs[t.OSNeed] > 0u)
				{
					productWindow.SetContent(GameSettings.Instance.simulation.GetCompatibleOS(needs[t.OSNeed], false));
				}
			}, "Porting");
		}
		else if (!p.Type.OSSpecific)
		{
			WindowManager.Instance.ShowMessageBox("PortNonOSError".Loc(), false, DialogWindow.DialogType.Error);
		}
	}

	public void OrderCopies()
	{
		SoftwareProduct softwareProduct = this.ProductList.GetSelected<SoftwareProduct>().FirstOrDefault<SoftwareProduct>();
		if (softwareProduct != null)
		{
			HUD.Instance.copyOrderWindow.Show(softwareProduct);
		}
	}

	public void StartSupport()
	{
		SoftwareProduct p = this.ProductList.GetSelected<SoftwareProduct>().FirstOrDefault<SoftwareProduct>();
		if (p != null && p.Bugs > 0 && GameSettings.Instance.MyCompany.WorkItems.OfType<SupportWork>().None((SupportWork x) => x.TargetProduct == p))
		{
			HUD.Instance.TeamSelectWindow.Show(GameSettings.Instance.GetDefaultTeams("Support"), null, delegate(string[] ts, SimulatedCompany c)
			{
				SupportWork supportWork = new SupportWork(p.ID, p.Name, -1);
				if (!supportWork.Done)
				{
					GameSettings.Instance.MyCompany.WorkItems.Add(supportWork);
					if (c != null)
					{
						supportWork.CompanyWorker = c;
					}
					else
					{
						foreach (string key in ts)
						{
							supportWork.AddDevTeam(GameSettings.Instance.sActorManager.Teams[key], false);
						}
					}
				}
			}, "Support");
		}
	}

	public void MakeSequel()
	{
		SoftwareProduct softwareProduct = this.ProductList.GetSelected<SoftwareProduct>().FirstOrDefault<SoftwareProduct>();
		if (softwareProduct != null)
		{
			HUD.Instance.docWindow.ShowSequel(softwareProduct);
		}
	}

	public void CreatePrintJob()
	{
		SoftwareProduct p = this.ProductList.GetSelected<SoftwareProduct>().FirstOrDefault<SoftwareProduct>();
		if (p != null)
		{
			PrintJob orNull = GameSettings.Instance.PrintOrders.GetOrNull(p.ID);
			if (orNull == null)
			{
				if (GameSettings.Instance.ProductPrinters.Count == 0)
				{
					WindowManager.Instance.ShowMessageBox("NoPrintersWarning".Loc(new object[]
					{
						MarketSimulation.PhysicalCopyPrice.Currency(true)
					}), false, DialogWindow.DialogType.Question, delegate
					{
						PrintJob printJob2 = new PrintJob(p.ID);
						GameSettings.Instance.PrintOrders[p.ID] = printJob2;
						HUD.Instance.distributionWindow.Show(printJob2);
					}, null, null);
				}
				else
				{
					PrintJob printJob = new PrintJob(p.ID);
					GameSettings.Instance.PrintOrders[p.ID] = printJob;
					HUD.Instance.distributionWindow.Show(printJob);
				}
			}
			else
			{
				HUD.Instance.distributionWindow.Show(orNull);
			}
		}
	}

	public GUIWindow Window;

	public GameObject ProductDetailWindow;

	[NonSerialized]
	public string[] Features;

	public GUIListView ProductList;

	public Button OKButton;

	public Button CancelButton;

	[NonSerialized]
	public List<SoftwareProduct> Content;

	[NonSerialized]
	public uint[] OSs;

	[NonSerialized]
	private Action<SoftwareProduct[]> OnCloseAction;

	private string _title;

	[NonSerialized]
	public SoftwareProduct PortFilter;

	[NonSerialized]
	public bool CategoryDisabled;

	[NonSerialized]
	public bool WithMock;

	public GameObject MarketButton;

	public GameObject DetailButton;

	public GameObject TradeButton;

	public GameObject PortButton;

	public GameObject CopyOrderButton;

	public GameObject PrintButton;

	public GameObject SupportButton;

	public GameObject SequelButton;

	public GameObject CSVButton;

	private string SWTypeFilter;

	private string SWCategoryFilter;

	private string CompanyFilter;

	[NonSerialized]
	private bool CanApplyFilters;
}
