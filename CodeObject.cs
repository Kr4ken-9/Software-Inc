﻿using System;
using DG.Tweening;
using UnityEngine;

public class CodeObject : MonoBehaviour
{
	public void UpdateColor(string t)
	{
		int num = Mathf.Min(t.Length, this.Label.text.Length);
		this.Label.Letters = 0;
		for (int i = 0; i < num; i++)
		{
			if (t[i] != this.Label.text[i])
			{
				break;
			}
			this.Label.Letters = i + 1;
			this.Label.SetVerticesDirty();
		}
	}

	public void DestroyMe()
	{
		DOTween.To(() => this.Label.fontSize, delegate(int x)
		{
			this.Label.fontSize = x;
		}, 48, 1f);
		TweenSettingsExtensions.OnComplete<Tweener>(ShortcutExtensions46.DOFade(this.Label, 0f, 1f), delegate
		{
			UnityEngine.Object.Destroy(base.gameObject);
		});
	}

	public SplitColorText Label;

	public RectTransform Rect;
}
