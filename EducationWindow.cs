﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EducationWindow : MonoBehaviour
{
	public Employee.EmployeeRole? SelectedRole
	{
		get
		{
			for (int i = 0; i < this.RoleToggles.Length; i++)
			{
				if (this.RoleToggles[i].isOn)
				{
					return new Employee.EmployeeRole?(EducationWindow.Roles[i]);
				}
			}
			return null;
		}
	}

	public void UpdateMonths()
	{
		this.MonthText.text = string.Concat(new object[]
		{
			"Months".Loc(),
			"(",
			(int)this.Months.value,
			"):"
		});
		this.UpdateCost();
	}

	private float GetCost()
	{
		int num = this.Affected.Length;
		if (this.HRToggle.isOn)
		{
			num = this.Affected.Count((Actor x) => !x.employee.HR);
		}
		return (float)num * ((!this.HRToggle.isOn) ? (EducationWindow.EducationCost * this.Months.value) : 10000f);
	}

	public void UpdateCost()
	{
		Employee.EmployeeRole? selectedRole = this.SelectedRole;
		this.SpecCombo.interactable = (selectedRole != null);
		if (selectedRole != null)
		{
			this.SpecCombo.UpdateContent<string>(GameSettings.Instance.GetUnlockedSpecializations(selectedRole.Value));
			if (this.SpecCombo.Selected < 0)
			{
				this.SpecCombo.Selected = 0;
			}
		}
		else
		{
			this.SpecCombo.UpdateContent<string>(new string[0]);
		}
		this.CostText.text = this.GetCost().Currency(true);
	}

	public void SendEmployee(Actor emp, int months, Employee.EmployeeRole role, string spec, bool hr = false)
	{
		SDateTime sdateTime = SDateTime.Now() + new SDateTime(1, months, 0);
		sdateTime = new SDateTime(0, emp.SpawnTime, sdateTime.Day, sdateTime.Month, sdateTime.Year);
		if (this.HRToggle.isOn && !hr)
		{
			emp.HREd = true;
		}
		else
		{
			emp.CoursePoints = emp.employee.GetEducationFactor(role) * ((float)months / 16f);
			emp.CourseRole = role;
			emp.CourseSpec = spec;
		}
		if (emp.SpecialState == Actor.HomeState.Vacation)
		{
			sdateTime += new SDateTime(0, 1, 0);
		}
		emp.LastCourse = SDateTime.Now();
		emp.IgnoreOffSalary = (emp.SpecialState != Actor.HomeState.Vacation);
		GameSettings.Instance.sActorManager.AddToAwaiting(emp, sdateTime, true, true);
	}

	public void OnAccept()
	{
		this.Affected = (from x in this.Affected
		where x != null && (!this.HRToggle.isOn || !x.employee.HR)
		select x).ToArray<Actor>();
		float cost = this.GetCost();
		if (GameSettings.Instance.MyCompany.CanMakeTransaction(-cost))
		{
			int num = (!this.HRToggle.isOn) ? ((int)this.Months.value) : EducationWindow.HRMonths;
			GameSettings.Instance.MyCompany.MakeTransaction(-cost, Company.TransactionCategory.Education, null);
			foreach (Actor actor in this.Affected)
			{
				Actor emp = actor;
				int months = num;
				Employee.EmployeeRole? selectedRole = this.SelectedRole;
				this.SendEmployee(emp, months, (selectedRole == null) ? Employee.EmployeeRole.Programmer : selectedRole.Value, this.SpecCombo.SelectedItemString, false);
			}
			this.Window.Close();
		}
	}

	public void HRDevChange(int i)
	{
		if (this.HRToggle.isOn)
		{
			this.Months.value = (float)EducationWindow.HRMonths;
		}
		this.Months.interactable = !this.HRToggle.isOn;
		this.UpdateCost();
	}

	public void Show(IEnumerable<Actor> affect)
	{
		this.MonthText.text = string.Concat(new object[]
		{
			"Months".Loc(),
			"(",
			(int)this.Months.value,
			"):"
		});
		this.Affected = (from x in affect
		where !x.TakingCourses
		select x).ToArray<Actor>();
		if (this.Affected.Length == 0 && affect.Any<Actor>())
		{
			WindowManager.Instance.ShowMessageBox("EmployeeCourseBookError".Loc(), false, DialogWindow.DialogType.Error);
			return;
		}
		if (this.Affected.Length == 1)
		{
			HintController.Instance.Show(HintController.Hints.HintEmployeeEducation);
		}
		if (this.HRToggle.isOn)
		{
			this.HRToggle.isOn = false;
			this.Months.value = 1f;
			this.RoleToggles[0].isOn = true;
		}
		this.HRToggle.interactable = this.Affected.Any((Actor x) => !x.employee.HR);
		this.Window.Show();
		this.UpdateCost();
		TutorialSystem.Instance.StartTutorial("Education", false);
	}

	public static float EducationCost = 600f;

	public static int HRMonths = 6;

	public GUIWindow Window;

	private Actor[] Affected;

	public Toggle[] RoleToggles;

	public Toggle HRToggle;

	public GameObject MainPanel;

	public Text CostText;

	public Text MonthText;

	public Slider Months;

	public GUICombobox SpecCombo;

	public static Employee.EmployeeRole[] Roles = new Employee.EmployeeRole[]
	{
		Employee.EmployeeRole.Programmer,
		Employee.EmployeeRole.Designer,
		Employee.EmployeeRole.Artist
	};
}
