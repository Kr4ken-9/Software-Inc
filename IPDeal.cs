﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class IPDeal : Deal
{
	public IPDeal(SoftwareProduct product) : base(product.DevCompany, false)
	{
		IPDeal $this = this;
		while (product.SequelTo != null)
		{
			product = product.SequelTo;
		}
		List<uint> list = new List<uint>();
		while (product != null)
		{
			list.Add(product.ID);
			product = product.Sequel;
		}
		this._products = list.ToArray();
		Company client = base.Client;
		if (client != null && client.IsPlayerOwned())
		{
			this.worth = 0f;
		}
		else
		{
			SDateTime time = SDateTime.Now();
			bool player = client != null && client.Player;
			if (player && HUD.Instance.docWindow.SequelTo != null && this._products.Contains(HUD.Instance.docWindow.SequelTo.ID))
			{
				HUD.Instance.docWindow.SequelTo = null;
			}
			this.worth = this._products.Sum(delegate(uint x)
			{
				SoftwareProduct product2 = GameSettings.Instance.simulation.GetProduct(x, false);
				float num = (!player) ? 1f : (client.GetReputation(product2._type, product2._category) * (product2.RealQuality * product2.RealQuality));
				return $this.GetProductSaleSum(product2) + Mathf.Max((float)((!player) ? 10000 : 0), product2.GetQuality(time, false) * product2.DevTime * product2.Category.Popularity * num * 500000f);
			});
		}
	}

	public IPDeal(SoftwareProduct product, Company company, SDateTime date) : base(company, true)
	{
		IPDeal $this = this;
		while (product.SequelTo != null)
		{
			product = product.SequelTo;
		}
		List<uint> list = new List<uint>();
		while (product != null)
		{
			list.Add(product.ID);
			product = product.Sequel;
		}
		this._products = list.ToArray();
		SDateTime time = SDateTime.Now();
		this.worth = this._products.Sum(delegate(uint x)
		{
			SoftwareProduct product2 = GameSettings.Instance.simulation.GetProduct(x, false);
			return $this.GetProductSaleSum(product2) + product2.GetQuality(GameSettings.Instance.simulation.GetQuality(product2, time, false), time, false) * product2.DevTime * product2.Category.Popularity * 500000f * Utilities.RandomGauss(0.7f, 0.1f);
		});
		this.Date = date;
	}

	public IPDeal()
	{
	}

	public SoftwareProduct Product
	{
		get
		{
			return GameSettings.Instance.simulation.GetProduct(this._products[0], false);
		}
	}

	private float GetProductSaleSum(SoftwareProduct p)
	{
		return p.GetTime(SDateTime.Now(), 0.15f) * p.Sum;
	}

	public override string ReputationCategory()
	{
		return null;
	}

	public override void Accept(Company company)
	{
		base.Accept(company);
		string bill = this.Title();
		if (this.Request)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(this.worth, Company.TransactionCategory.Deals, bill);
			base.Bidder.MakeTransaction(-this.worth, Company.TransactionCategory.Deals, bill);
			foreach (SoftwareProduct softwareProduct in from x in this._products
			select GameSettings.Instance.simulation.GetProduct(x, false))
			{
				softwareProduct.Trade(base.Bidder);
			}
		}
		else
		{
			if (base.Client != null)
			{
				base.Client.MakeTransaction(this.worth, Company.TransactionCategory.Deals, bill);
			}
			SimulatedCompany simulatedCompany = base.Client as SimulatedCompany;
			if (simulatedCompany != null)
			{
				for (int i = 0; i < simulatedCompany.Releases.Count; i++)
				{
					SimulatedCompany.ProductPrototype productPrototype = simulatedCompany.Releases[i];
					if (productPrototype.SequelTo != null && this._products.Contains(productPrototype.SequelTo.Value))
					{
						productPrototype.RemoveProject();
						i--;
					}
				}
				for (int j = 0; j < simulatedCompany.ProjectQueue.Count; j++)
				{
					SimulatedCompany.ProductPrototype productPrototype2 = simulatedCompany.ProjectQueue[j];
					if (productPrototype2.SequelTo != null && this._products.Contains(productPrototype2.SequelTo.Value))
					{
						productPrototype2.RemoveProject();
						j--;
					}
				}
			}
			base.Company.MakeTransaction(-this.worth, Company.TransactionCategory.Deals, bill);
			foreach (SoftwareProduct softwareProduct2 in from x in this._products
			select GameSettings.Instance.simulation.GetProduct(x, false))
			{
				HUD.Instance.dealWindow.CancelProductDeals(softwareProduct2);
				softwareProduct2.Trade(company);
			}
		}
		HUD.Instance.ApplyProductWindowFilters();
	}

	public override float Worth()
	{
		return this.worth;
	}

	public override string Description()
	{
		return "IPDeal".Loc(new object[]
		{
			this.Product.Name
		});
	}

	public override string Title()
	{
		return "IntellectualPropertyAbbr";
	}

	public override float Payout()
	{
		return 0f;
	}

	public override void HandleUpdate()
	{
	}

	public override string[] GetDetailedDescriptionVars()
	{
		return new string[0];
	}

	public override string[] GetDetailedDescriptionValues()
	{
		return new string[0];
	}

	public override bool CancelOnAccept()
	{
		return true;
	}

	public override bool StillValid(bool active)
	{
		return base.StillValid(active) && Utilities.GetMonths(this.Date, SDateTime.Now()) < 1f;
	}

	public override float ReputationEffect(bool ending)
	{
		return 0f;
	}

	public readonly uint[] _products;

	private readonly float worth;

	public readonly SDateTime Date;
}
