﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public abstract class WorkItem
{
	public WorkItem(string name, ContractWork Contract, int siblingIndex = -1)
	{
		this._name = name;
		if (GameSettings.Instance != null)
		{
			this.ID = GameSettings.Instance.GetWorkItemID();
		}
		this.contract = Contract;
		this.SiblingIndex = siblingIndex;
		this.MakeWorkItem();
	}

	public WorkItem()
	{
	}

	public string Name
	{
		get
		{
			return this._name;
		}
		set
		{
			this._name = value;
			if (this.guiItem != null)
			{
				this.guiItem.Title.text = this.Name;
			}
		}
	}

	public WorkDeal ActiveDeal
	{
		get
		{
			return (this.deal != 0u) ? (HUD.Instance.dealWindow.AllDeals[this.deal] as WorkDeal) : null;
		}
		set
		{
			this.deal = ((value != null) ? value.ID : 0u);
		}
	}

	public SimulatedCompany CompanyWorker
	{
		get
		{
			return this._companyWorker;
		}
		set
		{
			if (this.CanUseCompany())
			{
				if (value != null)
				{
					this.ClearTeams();
				}
				this._companyWorker = value;
			}
		}
	}

	public bool Hidden
	{
		get
		{
			return this._hidden;
		}
		set
		{
			this._hidden = value;
			if (this.guiItem != null)
			{
				this.guiItem.UpdateActivation();
			}
		}
	}

	public bool Paused
	{
		get
		{
			return this._paused;
		}
		set
		{
			this._paused = value;
			this.PauseChange();
		}
	}

	private void FixWorkerSkill()
	{
		if (this.WorkerSkill == null)
		{
			this.WorkerSkill = new float[Employee.RoleCount];
		}
		else if (this.WorkerSkill.Length != Employee.RoleCount)
		{
			float[] workerSkill = this.WorkerSkill;
			this.WorkerSkill = new float[Employee.RoleCount];
			int num = 0;
			while (num < Employee.RoleCount && num < workerSkill.Length)
			{
				this.WorkerSkill[num] = workerSkill[num];
				num++;
			}
		}
	}

	protected void RecordSkill(Employee.EmployeeRole role, float value, float delta)
	{
		this.FixWorkerSkill();
		this.WorkerSkill[(int)role] = Mathf.Lerp(this.WorkerSkill[(int)role], value, Utilities.PerDay(1f + value * 8f, delta, true));
	}

	protected void RecordSkill(Employee.EmployeeRole role, Actor act, float delta)
	{
		this.RecordSkill(role, act.employee.GetSkill(role), delta);
	}

	public virtual void DevTeamChange()
	{
	}

	public virtual void PauseChange()
	{
	}

	public void AddDevTeam(Team team, bool force = false)
	{
		if (this is AutoDevWorkItem)
		{
			if (force)
			{
				this.DevTeams.Add(team.Name);
				this.DevTeamChange();
			}
			return;
		}
		this.CompanyWorker = null;
		this.DevTeams.Add(team.Name);
		if (!team.WorkItems.Contains(this))
		{
			team.WorkItems.Add(this);
		}
		this.DevTeamChange();
		team.CheckWorkRoleAssignment();
	}

	public void RemoveDevTeam(Team team)
	{
		if (this is AutoDevWorkItem)
		{
			return;
		}
		this.DevTeams.Remove(team.Name);
		team.WorkItems.RemoveAll((WorkItem x) => x == this);
		this.DevTeamChange();
	}

	public void SwitchDevTeam(Team team, Team newteam)
	{
		if (this.DevTeams.Contains(team.Name))
		{
			this.DevTeams.Remove(team.Name);
			team.WorkItems.RemoveAll((WorkItem x) => x == this);
			this.AddDevTeam(newteam, true);
		}
	}

	public List<Team> GetDevTeams()
	{
		return (from x in this.DevTeams
		select GameSettings.Instance.sActorManager.Teams[x]).ToList<Team>();
	}

	public void ClearTeams()
	{
		foreach (Team team in this.GetDevTeams())
		{
			this.RemoveDevTeam(team);
		}
		this.DevTeamChange();
	}

	public virtual void Kill(bool wasCancelled = false)
	{
		if (wasCancelled && !this._wasCancelled)
		{
			this._wasCancelled = true;
			this.Cancelled();
		}
		if (!this.Done)
		{
			if (this.deal > 0u)
			{
				HUD.Instance.dealWindow.CancelDeal(this.ActiveDeal, true);
			}
			GameSettings.Instance.MyCompany.WorkItems.Remove(this);
			this.guiItem.Remove();
			this.ClearTeams();
			this.Done = true;
		}
	}

	public virtual void AddCost(float cost)
	{
	}

	public virtual float GetMax()
	{
		return 0f;
	}

	public virtual Color BackColor
	{
		get
		{
			return Color.white;
		}
	}

	public virtual float GetWorkScore()
	{
		return 0f;
	}

	public void MakeWorkItem()
	{
		if (HUD.Instance != null)
		{
			if (this.guiItem == null)
			{
				this.guiItem = HUD.Instance.SpawnWorkItem(this, this.GUIWorkItemType());
			}
			this.guiItem.UpdateActivation();
		}
	}

	public virtual int GUIWorkItemType()
	{
		return 0;
	}

	protected virtual void Cancelled()
	{
	}

	public virtual WorkItem.HasWorkReturn HasWork(Actor actor)
	{
		return WorkItem.HasWorkReturn.True;
	}

	public virtual string GetIcon()
	{
		return "Cogs";
	}

	public abstract string GetWorkTypeName();

	public virtual float GetProgress()
	{
		return 0f;
	}

	public virtual string GetProgressLabel()
	{
		return string.Empty;
	}

	public virtual string Category()
	{
		return "N/A";
	}

	public virtual string CurrentStage()
	{
		return "N/A";
	}

	public virtual string GetTeam()
	{
		if (this.CompanyWorker != null)
		{
			return this.CompanyWorker.Name;
		}
		if (this.deal > 0u && !this.ActiveDeal.Incoming)
		{
			return this.ActiveDeal.Company.Name;
		}
		if (this.DevTeams.Count > 1)
		{
			return this.DevTeams.First<string>() + "+" + (this.DevTeams.Count - 1);
		}
		if (this.DevTeams.Count > 0)
		{
			return this.DevTeams.First<string>();
		}
		return null;
	}

	public virtual Color GetProgressColor()
	{
		return new Color(0.6156863f, 0.8392157f, 0.521568656f, 0.6784314f);
	}

	public override string ToString()
	{
		return this.Name;
	}

	public int GetEmployeeCount()
	{
		int num = 0;
		foreach (string key in this.DevTeams)
		{
			int count = GameSettings.Instance.sActorManager.Teams[key].Count;
			num += count;
		}
		return num;
	}

	public abstract float StressMultiplier();

	public virtual void InitWorkItem(GUIWorkItem gWork)
	{
	}

	public virtual string CollapseLabel()
	{
		return this.Category();
	}

	public abstract void DoWork(Actor actor, float effectivenss, float delta);

	public virtual float GetWorkBoost(Employee.EmployeeRole role, float currentSkill)
	{
		this.FixWorkerSkill();
		return 1f + Mathf.Max(0f, this.WorkerSkill[(int)role] - currentSkill);
	}

	public abstract Employee.EmployeeRole? GetBoostRole(Actor act);

	public virtual string HightlightButton()
	{
		return null;
	}

	public abstract int EmitType(Actor actor);

	public virtual bool CanUseCompany()
	{
		return false;
	}

	public virtual bool HasCompanyWork()
	{
		return false;
	}

	public virtual float CompanyWork(float delta)
	{
		return 0f;
	}

	public string _name;

	public readonly uint ID;

	public int Priority = 1;

	private float[] WorkerSkill = new float[Employee.RoleCount];

	protected uint deal;

	public int SiblingIndex = -1;

	public SHashSet<string> DevTeams = new SHashSet<string>();

	private SimulatedCompany _companyWorker;

	public bool Done;

	public bool AutoDev;

	public ContractWork contract;

	public bool Collapsed;

	public bool Enabled = true;

	private bool _hidden;

	private bool _wasCancelled;

	public bool _paused;

	[NonSerialized]
	public Vector2 MouseRel;

	[NonSerialized]
	public GUIWorkItem guiItem;

	public int MiniGameLives = 3;

	public enum HasWorkReturn
	{
		True,
		Ignore,
		Finished,
		NotApplicable,
		Waiting
	}
}
