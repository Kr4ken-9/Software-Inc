﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering;

public class MiniMapMaker : MonoBehaviour
{
	public GameObject CreateMap()
	{
		return this.CreateMap(this.MapDescFromGame(GameSettings.Instance.PlotRect()), true);
	}

	public MiniMapMaker.MapDescriptor MapDescFromRooms(BuildingPrefab r)
	{
		float xmin = r.Edges.Min((SVector3 x) => x.x);
		float ymin = r.Edges.Min((SVector3 x) => x.y);
		float xmax = r.Edges.Max((SVector3 x) => x.x);
		float ymax = r.Edges.Max((SVector3 x) => x.y);
		Rect rect = Rect.MinMaxRect(xmin, ymin, xmax, ymax);
		int floorOffset = r.Rooms.Min((BuildingPrefab.RoomObject x) => x.Floor);
		BuildingPrefab.RoomObject[] arr = (from x in r.Rooms
		where !x.Outdoor
		select x).ToArray<BuildingPrefab.RoomObject>();
		SVector3[][] rooms = arr.SelectInPlace((BuildingPrefab.RoomObject x) => x.Edges.SelectInPlace((int z) => r.Edges[z]));
		SVector3[] roomColors = arr.SelectInPlace((BuildingPrefab.RoomObject x) => x.Colors[1]);
		int[] roomFloors = arr.SelectInPlace((BuildingPrefab.RoomObject x) => x.Floor - floorOffset);
		BuildingPrefab.RoomObject[] array = (from x in r.Rooms
		where x.Outdoor
		select x).ToArray<BuildingPrefab.RoomObject>();
		SVector3[][] fences = array.SelectInPlace(delegate(BuildingPrefab.RoomObject x)
		{
			SVector3[] array3 = new SVector3[x.Edges.Length * 2];
			for (int l = 0; l < x.Edges.Length; l++)
			{
				int num5 = (l + 1) % x.Edges.Length;
				array3[l * 2] = r.Edges[x.Edges[l]];
				array3[l * 2 + 1] = r.Edges[x.Edges[num5]];
			}
			for (int m = 0; m < array3.Length; m++)
			{
				array3[m] = new SVector3(array3[m].x, (float)(x.Floor - floorOffset), array3[m].y, 0f);
			}
			return array3;
		});
		SVector3 gColor = TimeOfDay.Instance.GetGroundColor();
		SVector3[] fenceColors = array.SelectInPlace((BuildingPrefab.RoomObject x) => x.Colors[1]);
		SVector3[][] fenceFloor = array.SelectInPlace(delegate(BuildingPrefab.RoomObject x)
		{
			List<SVector3> list5 = (from z in x.Edges
			select new SVector3(r.Edges[z].x, (float)(x.Floor - floorOffset), r.Edges[z].y, 0f)).ToList<SVector3>();
			list5.Add((!x.Materials[2].Equals("None")) ? x.Colors[2] : gColor);
			return list5.ToArray();
		});
		float[] fenceHeight = Utilities.RepeatValue<float>(0.3f, array.Length);
		Dictionary<string, RoomSegment> dictionary = ObjectDatabase.Instance.RoomSegments.ToDictionary((GameObject x) => x.name, (GameObject x) => x.GetComponent<RoomSegment>());
		List<string> list = new List<string>();
		List<SVector3> list2 = new List<SVector3>();
		List<float> list3 = new List<float>();
		List<float> list4 = new List<float>();
		foreach (BuildingPrefab.RoomObject roomObject in r.Rooms)
		{
			foreach (BuildingPrefab.SegmentObject segmentObject in roomObject.Segments)
			{
				RoomSegment roomSegment = dictionary[segmentObject.Name];
				if (roomSegment.MiniMap)
				{
					Vector2 vector = segmentObject.Position.ToVector3().FlattenVector3();
					for (int k = 0; k < roomObject.Edges.Length; k++)
					{
						Vector2 vector2 = r.Edges[roomObject.Edges[k]].ToVector2();
						Vector2 vector3 = r.Edges[roomObject.Edges[(k + 1) % roomObject.Edges.Length]].ToVector2();
						Vector2? vector4 = Utilities.ProjectToLine(vector, vector2, vector3);
						if (vector4 != null && (vector4.Value - vector).sqrMagnitude < 0.05f)
						{
							list.Add(segmentObject.Name);
							list2.Add(new SVector3(segmentObject.Position.x, segmentObject.Position.y - (float)(floorOffset * 2), segmentObject.Position.z, 0f));
							Vector2 vector5 = vector3 - vector2;
							list3.Add(Quaternion.LookRotation(new Vector3(-vector5.y, 0f, vector5.x)).eulerAngles.y);
							list4.Add(segmentObject.Width);
							break;
						}
					}
				}
			}
		}
		SVector3[] treePos = new SVector3[0];
		SVector3[] array2 = new SVector3[0];
		float[] skraperHeights = new float[]
		{
			gColor.x,
			gColor.y,
			gColor.z
		};
		int num = (int)rect.x / 8;
		int num2 = (int)(rect.x + rect.width) / 8;
		int num3 = (int)rect.y / 8;
		int num4 = (int)(rect.y + rect.height) / 8;
		bool[,] roadMap = new bool[num2 - num, num4 - num3];
		return new MiniMapMaker.MapDescriptor(rooms, roomColors, roomFloors, fences, fenceFloor, fenceColors, array2, fenceHeight, treePos, array2, skraperHeights, roadMap, list.ToArray(), list2.ToArray(), list3.ToArray(), new SVector3(rect.x, rect.y, rect.width, rect.height))
		{
			SegmentWidth = list4.ToArray()
		};
	}

	public MiniMapMaker.MapDescriptor MapDescFromRooms(Room[] r)
	{
		float xmin = r.Min((Room x) => x.RoomBounds.xMin);
		float ymin = r.Min((Room x) => x.RoomBounds.yMin);
		float xmax = r.Max((Room x) => x.RoomBounds.xMax);
		float ymax = r.Max((Room x) => x.RoomBounds.yMax);
		Rect rect = Rect.MinMaxRect(xmin, ymin, xmax, ymax);
		int floorOffset = Mathf.Max(0, r.Min((Room x) => x.Floor));
		Room[] arr = (from x in r
		where !x.Outdoors
		select x).ToArray<Room>();
		SVector3[][] rooms = arr.SelectInPlace((Room x) => x.Edges.SelectInPlace((WallEdge z) => z.Pos));
		SVector3[] roomColors = arr.SelectInPlace((Room x) => x.OutsideColor);
		int[] roomFloors = arr.SelectInPlace((Room x) => x.Floor - floorOffset);
		Color gColor = TimeOfDay.Instance.GetGroundColor();
		Room[] arr2 = (from x in r
		where x.Outdoors
		select x).ToArray<Room>();
		SVector3[][] fences = arr2.SelectInPlace((Room x) => x.GetFenceSegments().SelectInPlace((Vector2 z) => new SVector3(z.x, (float)(x.Floor - floorOffset), z.y, 0f)));
		SVector3[] fenceColors = arr2.SelectInPlace((Room x) => x.OutsideColor);
		SVector3[][] fenceFloor = arr2.SelectInPlace(delegate(Room x)
		{
			List<SVector3> list = (from z in x.Edges
			select new SVector3(z.Pos.x, (float)(x.Floor - floorOffset), z.Pos.y, 0f)).ToList<SVector3>();
			list.Add((!x.FloorMat.Equals("None")) ? x.FloorColor : gColor);
			return list.ToArray();
		});
		float[] fenceHeight = arr2.SelectInPlace((Room x) => x.FenceHeight);
		List<RoomSegment> arr3 = r.SelectMany((Room x) => x.GetSegments(null)).Distinct<RoomSegment>().Where(delegate(RoomSegment x)
		{
			if (x.MiniMap)
			{
				WallEdge key = x.WallPosition.First<KeyValuePair<WallEdge, float>>().Key;
				WallEdge key2 = x.WallPosition.Last<KeyValuePair<WallEdge, float>>().Key;
				return key.IsAgainstOutdoors(key2) || !key.Links.ContainsValue(key2) || !key2.Links.ContainsValue(key);
			}
			return false;
		}).ToList<RoomSegment>();
		string[] segmentType = arr3.SelectInPlace((RoomSegment z) => z.name.Replace("(Clone)", string.Empty).Trim());
		SVector3[] segmentPos = arr3.SelectInPlace((RoomSegment z) => z.transform.position + Vector3.down * (float)floorOffset * 2f);
		float[] segmentRot = arr3.SelectInPlace((RoomSegment z) => z.transform.rotation.eulerAngles.y);
		float[] segmentWidth = arr3.SelectInPlace((RoomSegment z) => z.MiniWidth);
		SVector3[] treePos = new SVector3[0];
		SVector3[] array = new SVector3[0];
		float[] skraperHeights = new float[]
		{
			gColor.r,
			gColor.g,
			gColor.b
		};
		int num = (int)rect.x / 8;
		int num2 = (int)(rect.x + rect.width) / 8;
		int num3 = (int)rect.y / 8;
		int num4 = (int)(rect.y + rect.height) / 8;
		bool[,] roadMap = new bool[num2 - num, num4 - num3];
		return new MiniMapMaker.MapDescriptor(rooms, roomColors, roomFloors, fences, fenceFloor, fenceColors, array, fenceHeight, treePos, array, skraperHeights, roadMap, segmentType, segmentPos, segmentRot, new SVector3(rect.x, rect.y, rect.width, rect.height))
		{
			SegmentWidth = segmentWidth
		};
	}

	public MiniMapMaker.MapDescriptor MapDescFromGame(Rect plot)
	{
		Color gColor = TimeOfDay.Instance.GetGroundColor();
		plot = new Rect(Mathf.Floor(plot.x / 8f) * 8f, Mathf.Floor(plot.y / 8f) * 8f, Mathf.Max(4f, Mathf.Ceil((plot.x + plot.width) / 8f) * 8f - Mathf.Floor(plot.x / 8f) * 8f), Mathf.Max(4f, Mathf.Ceil((plot.y + plot.height) / 8f) * 8f - Mathf.Floor(plot.y / 8f) * 8f));
		Room[] arr = (from x in GameSettings.Instance.sRoomManager.Rooms
		where x.Floor >= 0 && !x.Outdoors
		select x).ToArray<Room>();
		SVector3[][] rooms = arr.SelectInPlace((Room x) => x.Edges.SelectInPlace((WallEdge z) => z.Pos));
		SVector3[] roomColors = arr.SelectInPlace((Room x) => x.OutsideColor);
		int[] roomFloors = arr.SelectInPlace((Room x) => x.Floor);
		Room[] arr2 = (from x in GameSettings.Instance.sRoomManager.Rooms
		where x.Outdoors
		select x).ToArray<Room>();
		SVector3[][] fences = arr2.SelectInPlace((Room x) => x.GetFenceSegments().SelectInPlace((Vector2 z) => new SVector3(z.x, (float)x.Floor, z.y, 0f)));
		SVector3[] fenceColors = arr2.SelectInPlace((Room x) => x.OutsideColor);
		SVector3[][] fenceFloor = arr2.SelectInPlace(delegate(Room x)
		{
			List<SVector3> list = (from z in x.Edges
			select new SVector3(z.Pos.x, (float)x.Floor, z.Pos.y, 0f)).ToList<SVector3>();
			list.Add((!x.FloorMat.Equals("None")) ? x.FloorColor : gColor);
			return list.ToArray();
		});
		float[] fenceHeight = arr2.SelectInPlace((Room x) => x.FenceHeight);
		List<RoomSegment> arr3 = (from x in GameSettings.Instance.sRoomManager.RoomSegments
		where x.MiniMap && x.WallPosition.Count > 1 && x.IsAgainstExterior
		select x).ToList<RoomSegment>();
		string[] segmentType = arr3.SelectInPlace((RoomSegment z) => z.name.Replace("(Clone)", string.Empty).Trim());
		SVector3[] segmentPos = arr3.SelectInPlace((RoomSegment z) => z.transform.position);
		float[] segmentRot = arr3.SelectInPlace((RoomSegment z) => z.transform.rotation.eulerAngles.y);
		float[] segmentWidth = arr3.SelectInPlace((RoomSegment z) => z.MiniWidth);
		SVector3[] treePos = (from x in GameSettings.Instance.Trees
		where x.Position.x >= plot.xMin && x.Position.x <= plot.xMax && x.Position.z >= plot.yMin && x.Position.z <= plot.yMax
		orderby UnityEngine.Random.value
		select x.Position).ToArray<SVector3>();
		RoadManager instance = RoadManager.Instance;
		SkraperGen[] arr4 = (from x in instance.Landmarks.OfType<SkraperGen>()
		where x.Blob.xMin >= plot.xMin && x.Blob.xMax <= plot.xMax && x.Blob.yMin >= plot.yMin && x.Blob.yMax <= plot.yMax
		select x).ToArray<SkraperGen>();
		SVector3[] skrapers = arr4.SelectInPlace((SkraperGen x) => new SVector3(x.Blob.x, x.Blob.y, x.Blob.width, x.Blob.height));
		float[] skraperHeights = arr4.SelectInPlace((SkraperGen x) => x.Height).ConcatArray(new float[]
		{
			gColor.r,
			gColor.g,
			gColor.b
		});
		SVector3[] houses = (from x in instance.Landmarks.OfType<BurbHouse>()
		where plot.ContainsEntirely(x.transform.position.FlattenVector3(), 0.1f)
		select x).Select(delegate(BurbHouse x)
		{
			Vector3 position = x.transform.position;
			float y = x.transform.rotation.eulerAngles.y;
			return new SVector3(position.x, position.y, position.z, y);
		}).ToArray<SVector3>();
		int num = Mathf.Clamp((int)plot.x / 8, 0, instance.RoadMap.GetLength(0));
		int num2 = Mathf.Clamp((int)(plot.x + plot.width) / 8, 0, instance.RoadMap.GetLength(0));
		int num3 = Mathf.Clamp((int)plot.y / 8, 0, instance.RoadMap.GetLength(1));
		int num4 = Mathf.Clamp((int)(plot.y + plot.height) / 8, 0, instance.RoadMap.GetLength(1));
		bool[,] array = new bool[num2 - num, num4 - num3];
		for (int i = num; i < num2; i++)
		{
			for (int j = num3; j < num4; j++)
			{
				array[i - num, j - num3] = (instance.RoadMap[i, j] > 0);
			}
		}
		return new MiniMapMaker.MapDescriptor(rooms, roomColors, roomFloors, fences, fenceFloor, fenceColors, houses, fenceHeight, treePos, skrapers, skraperHeights, array, segmentType, segmentPos, segmentRot, new SVector3(plot.x, plot.y, plot.width, plot.height))
		{
			SegmentWidth = segmentWidth
		};
	}

	public GameObject CreateMap(MiniMapMaker.MapDescriptor map, bool includeGround = true)
	{
		return this.CreateMap(map.Rooms, map.RoomColors, map.RoomFloors, map.Fences, map.FenceFloor, map.FenceColors, map.FenceHeight, map.TreePos, map.Skrapers, map.SkraperHeights, map.RoadMap, map.SegmentType, map.SegmentPos, map.SegmentRot, map.SegmentWidth, new Rect(map.Plot.x, map.Plot.y, map.Plot.z, map.Plot.w), true, includeGround);
	}

	public Mesh CreateBuildingMesh(MiniMapMaker.MapDescriptor desc, Vector2 voffset)
	{
		Vector3 offset = new Vector3(-voffset.x, -voffset.y, 0f);
		List<CombineInstance> list = new List<CombineInstance>();
		for (int i = 0; i < desc.Rooms.Length; i++)
		{
			list.Add(new CombineInstance
			{
				mesh = this.CreateRoom(desc.Rooms[i], desc.RoomFloors[i], desc.RoomColors[i], offset),
				transform = Matrix4x4.identity
			});
		}
		if (desc.Fences != null)
		{
			for (int j = 0; j < desc.Fences.Length; j++)
			{
				if (desc.Fences[j].Length > 0)
				{
					list.Add(new CombineInstance
					{
						mesh = this.CreateFence(desc.Fences[j], desc.FenceFloor[j], desc.FenceColors[j], offset, desc.FenceHeight[j]),
						transform = Matrix4x4.identity
					});
				}
			}
		}
		Dictionary<string, RoomSegment> dictionary = ObjectDatabase.Instance.RoomSegments.ToDictionary((GameObject x) => x.name, (GameObject x) => x.GetComponent<RoomSegment>());
		for (int k = 0; k < desc.SegmentType.Length; k++)
		{
			RoomSegment roomSegment = dictionary[desc.SegmentType[k]];
			CombineInstance item = default(CombineInstance);
			item.mesh = this.ColorMesh(this.Quad, roomSegment.MiniColor);
			Vector3 s = new Vector3(desc.SegmentWidth[k], roomSegment.MiniHeight, 0.1f);
			SVector3 svector = desc.SegmentPos[k];
			Quaternion quaternion = Quaternion.Euler(0f, desc.SegmentRot[k], 0f);
			Vector3 vector = quaternion * Vector3.back * 0.01f;
			item.transform = Matrix4x4.TRS(new Vector3(svector.x + offset.x + vector.x, svector.y + offset.z + roomSegment.MiniYOffset, svector.z + offset.y + vector.z), quaternion, s);
			list.Add(item);
		}
		Mesh mesh = new Mesh();
		mesh.CombineMeshes(list.ToArray());
		return mesh;
	}

	private Mesh CreateFence(SVector3[] edges, SVector3[] floorPlan, Color color, Vector3 offset, float height)
	{
		Mesh m = new Mesh();
		List<CombineInstance> list = new List<CombineInstance>();
		Mesh mesh = this.ColorMesh(this.cubeMesh, color);
		float y = edges.First<SVector3>().y;
		for (int i = 0; i < edges.Length; i += 2)
		{
			int num = i + 1;
			Vector3 vector = new Vector3(edges[i].x + offset.x, y * 2f + offset.z + height / 2f, edges[i].z + offset.y);
			Vector3 vector2 = new Vector3(edges[num].x + offset.x, y * 2f + offset.z + height / 2f, edges[num].z + offset.y);
			if (!(vector == vector2))
			{
				list.Add(new CombineInstance
				{
					mesh = mesh,
					transform = Matrix4x4.TRS((vector + vector2) * 0.5f, Quaternion.LookRotation(vector - vector2), new Vector3(0.05f, height, (vector - vector2).magnitude))
				});
			}
		}
		m.CombineMeshes(list.ToArray());
		Color fCol = floorPlan.Last<SVector3>().ToColor();
		Vector3[] array = (from x in floorPlan.Take(floorPlan.Length - 1)
		select new Vector3(x.x + offset.x, x.y * 2f + offset.z + 0.01f, x.z + offset.y)).ToArray<Vector3>();
		IEnumerable<Vector3> source = array;
		if (MiniMapMaker.<>f__mg$cache0 == null)
		{
			MiniMapMaker.<>f__mg$cache0 = new Func<Vector3, Vector2>(Utilities.FlattenVector3);
		}
		int[] source2 = new Triangulator(source.Select(MiniMapMaker.<>f__mg$cache0).ToArray<Vector2>()).Triangulate();
		Vector3[] vertices = m.vertices.ConcatArray(array);
		Vector3[] normals = m.normals.Concat(from x in array
		select Vector3.up).ToArray<Vector3>();
		Color[] colors = m.colors.Concat(from x in array
		select fCol).ToArray<Color>();
		int[] triangles = m.triangles.Concat(from x in source2
		select x + m.vertexCount).ToArray<int>();
		Vector2[] uv = m.uv.Concat(from x in array
		select Vector2.zero).ToArray<Vector2>();
		m.vertices = vertices;
		m.normals = normals;
		m.colors = colors;
		m.triangles = triangles;
		m.uv = uv;
		return m;
	}

	private Mesh CreateRoom(SVector3[] edges, int floor, Color color, Vector3 offset)
	{
		Mesh mesh = new Mesh();
		List<Vector3> vec1 = new List<Vector3>();
		List<Vector3> list = new List<Vector3>();
		List<Vector3> list2 = new List<Vector3>();
		for (int i = 0; i < edges.Length; i++)
		{
			int num = (i + 1) % edges.Length;
			Vector3 vector = new Vector3(edges[i].x + offset.x, (float)(floor * 2) + offset.z, edges[i].y + offset.y);
			Vector3 vector2 = new Vector3(edges[num].x + offset.x, (float)(floor * 2) + offset.z, edges[num].y + offset.y);
			Vector3 vector3 = vector2 - vector;
			Vector3 vector4 = new Vector3(vector3.z, 0f, -vector3.x);
			Vector3 normalized = vector4.normalized;
			vec1.Add(vector);
			vec1.Add(vector2);
			list.Add(new Vector3(vector.x, vector.y + 2f, vector.z));
			list.Add(new Vector3(vector2.x, vector2.y + 2f, vector2.z));
			list2.Add(normalized);
			list2.Add(normalized);
		}
		int[] source = new Triangulator((from x in edges
		select new Vector2(x.x, x.y)).ToArray<Vector2>()).Triangulate();
		List<int> list3 = new List<int>();
		list3.AddRange(from x in source
		select x + vec1.Count * 2);
		for (int j = 0; j < edges.Length * 2 - 1; j += 2)
		{
			list3.Add(j);
			list3.Add(edges.Length * 2 + j);
			list3.Add(edges.Length * 2 + j + 1);
			list3.Add(edges.Length * 2 + j + 1);
			list3.Add(j + 1);
			list3.Add(j);
		}
		Vector3[] array = vec1.Concat(list).Concat(from x in edges
		select new Vector3(x.x + offset.x, (float)(floor * 2 + 2) + offset.z, x.y + offset.y)).ToArray<Vector3>();
		mesh.vertices = array;
		mesh.normals = list2.Concat(list2).Concat(from x in edges
		select Vector3.up).ToArray<Vector3>();
		mesh.triangles = list3.ToArray();
		mesh.colors = Utilities.RepeatValue<Color>(color, array.Length);
		mesh.uv = Utilities.RepeatValue<Vector2>(Vector2.zero, array.Length);
		return mesh;
	}

	public GameObject CreateMap(SVector3[][] rooms, SVector3[] roomColors, int[] roomFloors, SVector3[][] fences, SVector3[][] fenceFloor, SVector3[] fenceColors, float[] fenceHeight, SVector3[] treePos, SVector3[] skrapers, float[] skraperHeights, bool[,] roadMap, string[] segmentType, SVector3[] segmentPos, float[] segmentRot, float[] segmentWidth, Rect plot, bool UnitSize, bool includeGround)
	{
		GameObject gameObject = this.CreateGameObject(null, "Minimap");
		Vector3 offset = new Vector3(-plot.x - plot.width / 2f, -plot.y - plot.height / 2f, (!includeGround) ? 0f : ((float)((!roomFloors.Any<int>()) ? 0 : (-(float)roomFloors.Max())) + 3.5f));
		List<CombineInstance> list = new List<CombineInstance>();
		for (int i = 0; i < rooms.Length; i++)
		{
			list.Add(new CombineInstance
			{
				mesh = this.CreateRoom(rooms[i], roomFloors[i], roomColors[i], offset),
				transform = Matrix4x4.identity
			});
		}
		if (fences != null)
		{
			for (int j = 0; j < fences.Length; j++)
			{
				if (fences[j].Length > 0)
				{
					list.Add(new CombineInstance
					{
						mesh = this.CreateFence(fences[j], fenceFloor[j], fenceColors[j], offset, fenceHeight[j]),
						transform = Matrix4x4.identity
					});
				}
			}
		}
		for (int k = 0; k < skrapers.Length; k++)
		{
			SVector3 svector = skrapers[k];
			CombineInstance item = default(CombineInstance);
			item.mesh = this.ColorMesh(this.cubeMesh, new Color(0.8f, 0.8f, 0.8f, 1f));
			Vector2 vector = new Vector2(svector.x + svector.z / 2f + offset.x, svector.y + svector.w / 2f + offset.y);
			item.transform = Matrix4x4.TRS(new Vector3(vector.x, skraperHeights[k] / 2f + offset.z, vector.y), Quaternion.identity, new Vector3(svector.z + 1f, skraperHeights[k], svector.w + 1f));
			list.Add(item);
		}
		if (fences != null)
		{
			for (int l = fences.Length; l < fenceColors.Length; l++)
			{
				SVector3 svector2 = fenceColors[l];
				list.Add(new CombineInstance
				{
					mesh = this.ColorMesh(this.House, new Color(0.8f, 0.5f, 0.4f, 1f)),
					transform = Matrix4x4.TRS(new Vector3(svector2.x + offset.x, svector2.y + offset.z, svector2.z + offset.y), Quaternion.Euler(0f, svector2.w, 0f), Vector3.one)
				});
			}
		}
		GameObject gameObject2 = this.CreateGameObject(list, "Building");
		gameObject2.transform.parent = gameObject.transform;
		if (includeGround)
		{
			list.Clear();
			CombineInstance item2 = default(CombineInstance);
			Color color = (skraperHeights.Length != skrapers.Length + 3) ? new Color32(90, 132, 69, byte.MaxValue) : new Color(skraperHeights[skraperHeights.Length - 3], skraperHeights[skraperHeights.Length - 2], skraperHeights[skraperHeights.Length - 1]);
			item2.mesh = this.ColorMesh(this.cubeMesh, color);
			item2.transform = Matrix4x4.TRS(new Vector3(0f, -2f + offset.z, 0f), Quaternion.identity, new Vector3(plot.width + 1f, 4f, plot.height + 1f));
			list.Add(item2);
			list.Add(new CombineInstance
			{
				mesh = this.ColorMesh(this.cubeMesh, new Color32(105, 93, 61, byte.MaxValue)),
				transform = Matrix4x4.TRS(new Vector3(0f, -6f + offset.z, 0f), Quaternion.identity, new Vector3(plot.width, 4f, plot.height))
			});
			GameObject gameObject3 = this.CreateGameObject(list, "Ground");
			gameObject3.transform.parent = gameObject.transform;
		}
		list.Clear();
		Dictionary<string, RoomSegment> dictionary = ObjectDatabase.Instance.RoomSegments.ToDictionary((GameObject x) => x.name, (GameObject x) => x.GetComponent<RoomSegment>());
		for (int m = 0; m < segmentType.Length; m++)
		{
			RoomSegment roomSegment = dictionary[segmentType[m]];
			CombineInstance item3 = default(CombineInstance);
			item3.mesh = this.ColorMesh(this.cubeMesh, roomSegment.MiniColor);
			Vector3 s = new Vector3((segmentWidth == null) ? roomSegment.MiniWidth : segmentWidth[m], roomSegment.MiniHeight, 0.1f);
			SVector3 svector3 = segmentPos[m];
			item3.transform = Matrix4x4.TRS(new Vector3(svector3.x + offset.x, svector3.y + offset.z + roomSegment.MiniYOffset, svector3.z + offset.y), Quaternion.Euler(0f, segmentRot[m], 0f), s);
			list.Add(item3);
		}
		GameObject gameObject4 = this.CreateGameObject(list, "Segments");
		gameObject4.transform.parent = gameObject.transform;
		if (includeGround)
		{
			Vector3 b = new Vector3(offset.x, offset.z, offset.y);
			list.Clear();
			int num = treePos.Length * 48 / 50000 + 1;
			for (int n = 0; n < treePos.Length; n += num)
			{
				SVector3 svector4 = treePos[n];
				list.Add(new CombineInstance
				{
					mesh = this.Tree,
					transform = Matrix4x4.TRS(svector4.ToVector3() + b, Quaternion.identity, Vector3.one)
				});
			}
			GameObject gameObject5 = this.CreateGameObject(list, "trees");
			gameObject5.transform.parent = gameObject.transform;
			list.Clear();
			int num2 = 8;
			for (int num3 = 0; num3 < roadMap.GetLength(0); num3++)
			{
				for (int num4 = 0; num4 < roadMap.GetLength(1); num4++)
				{
					if (roadMap[num3, num4])
					{
						list.Add(new CombineInstance
						{
							mesh = this.ColorMesh(this.Quad, Color.gray),
							transform = Matrix4x4.TRS(new Vector3((float)(num3 * num2 + num2 / 2) - plot.width / 2f, 0.01f + offset.z, (float)(num4 * num2 + num2 / 2) - plot.height / 2f), Quaternion.Euler(90f, 0f, 0f), Vector3.one * (float)num2)
						});
					}
				}
			}
			GameObject gameObject6 = this.CreateGameObject(list, "road");
			gameObject6.transform.parent = gameObject.transform;
		}
		if (UnitSize)
		{
			float num5 = (!roomFloors.Any<int>()) ? 0f : ((float)Mathf.Max(0, roomFloors.Max()));
			float num6;
			if (fences != null && fences.Any<SVector3[]>())
			{
				num6 = Mathf.Max(fences.Max((SVector3[] x) => (x.Length <= 0) ? 0f : x[0].y), num5);
			}
			else
			{
				num6 = num5;
			}
			num5 = num6;
			float num7 = num5 * 2f + 2f;
			float num8 = 1f / Mathf.Max(new float[]
			{
				plot.width,
				plot.height,
				num7 + ((!includeGround) ? 0f : 12f)
			});
			if (!includeGround)
			{
				gameObject.transform.position = gameObject.transform.position + Vector3.down * (num7 * num8 / 2f);
			}
			gameObject.transform.localScale = new Vector3(num8, num8, num8);
		}
		return gameObject;
	}

	private GameObject CreateGameObject(List<CombineInstance> meshes, string name)
	{
		GameObject gameObject = new GameObject(name);
		if (meshes != null)
		{
			MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
			meshRenderer.material = this.Mat;
			meshRenderer.lightProbeUsage = LightProbeUsage.Off;
			meshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
			MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
			meshFilter.mesh.CombineMeshes(meshes.ToArray());
		}
		return gameObject;
	}

	private Mesh ColorMesh(Mesh mesh, Color color)
	{
		return new Mesh
		{
			vertices = mesh.vertices,
			triangles = mesh.triangles,
			uv = mesh.uv,
			colors = mesh.vertices.SelectInPlace((Vector3 x) => color),
			normals = mesh.normals,
			tangents = mesh.tangents
		};
	}

	public Mesh cubeMesh;

	public Mesh Tree;

	public Mesh House;

	public Mesh Quad;

	public Material Mat;

	[CompilerGenerated]
	private static Func<Vector3, Vector2> <>f__mg$cache0;

	[Serializable]
	public struct MapDescriptor
	{
		public MapDescriptor(SVector3 plot)
		{
			this.Rooms = new SVector3[0][];
			this.RoomColors = new SVector3[0];
			this.TreePos = new SVector3[0];
			this.Skrapers = new SVector3[0];
			this.Fences = new SVector3[0][];
			this.FenceFloor = new SVector3[0][];
			this.FenceColors = new SVector3[0];
			this.FenceHeight = new float[0];
			this.RoomFloors = new int[0];
			this.SkraperHeights = new float[0];
			this.RoadMap = new bool[0, 0];
			this.SegmentType = new string[0];
			this.SegmentPos = new SVector3[0];
			this.SegmentRot = new float[0];
			this.Plot = plot;
			this.SegmentWidth = new float[0];
		}

		public MapDescriptor(SVector3[][] rooms, SVector3[] roomColors, int[] roomFloors, SVector3[][] fences, SVector3[][] fenceFloor, SVector3[] fenceColors, SVector3[] houses, float[] fenceHeight, SVector3[] treePos, SVector3[] skrapers, float[] skraperHeights, bool[,] roadMap, string[] segmentType, SVector3[] segmentPos, float[] segmentRot, SVector3 plot)
		{
			this.Rooms = rooms;
			this.RoomColors = roomColors;
			this.RoomFloors = roomFloors;
			this.Fences = fences;
			this.FenceFloor = fenceFloor;
			this.FenceColors = fenceColors.Concat(houses).ToArray<SVector3>();
			this.FenceHeight = fenceHeight;
			this.TreePos = treePos;
			this.Skrapers = skrapers;
			this.SkraperHeights = skraperHeights;
			this.RoadMap = roadMap;
			this.SegmentType = segmentType;
			this.SegmentPos = segmentPos;
			this.SegmentRot = segmentRot;
			this.SegmentWidth = null;
			this.Plot = plot;
		}

		public SVector3[][] Rooms;

		public SVector3[] RoomColors;

		public SVector3[] TreePos;

		public SVector3[] Skrapers;

		public SVector3[][] Fences;

		public SVector3[][] FenceFloor;

		public SVector3[] FenceColors;

		public float[] FenceHeight;

		public int[] RoomFloors;

		public float[] SkraperHeights;

		public bool[,] RoadMap;

		public string[] SegmentType;

		public SVector3[] SegmentPos;

		public float[] SegmentRot;

		public SVector3 Plot;

		public float[] SegmentWidth;
	}
}
