﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ReviewWindow : MonoBehaviour
{
	public void Show(ReviewWindow.ReviewData data, SoftwareAlpha target)
	{
		this.scrollRect.verticalNormalizedPosition = 1f;
		this.DateLabel.text = "Build date".Loc() + ": " + data.BuildDate.ToString();
		for (int i = 0; i < this._allContent.Count; i++)
		{
			GameObject obj = this._allContent[i];
			UnityEngine.Object.Destroy(obj);
		}
		this._allContent.Clear();
		this.AddText("Overall score".Loc(), true, this.DefaultColor, 24, true);
		Text scoreT = this.AddText(string.Empty, false, Color.clear, 18, false);
		this._finalScore = 0f;
		DOTween.To(() => this._finalScore, delegate(float x)
		{
			this._finalScore = x;
			scoreT.text = SmileyChart.ScoreString(this._finalScore) + " / 10";
		}, data.Score, 1f);
		this.AddText("Review accuracy".Loc(), true, this.DefaultColor, 24, true);
		Text accT = this.AddText(string.Empty, false, Color.clear, 18, false);
		this._finalAccuracy = 0f;
		DOTween.To(() => this._finalAccuracy, delegate(float x)
		{
			this._finalAccuracy = x;
			accT.text = Mathf.Min(9, Mathf.RoundToInt(this._finalAccuracy * 10f)) * 10 + "%";
		}, data.Accuracy, 1f);
		if (target != null)
		{
			this.AddText(string.Format("-{0} {1}\t\t+{2} {3}", new object[]
			{
				target.GetBugReviewFactor().ToPercent(),
				"Bugs".Loc().ToLower(),
				target.GetQualityFactor().ToPercent(),
				"Quality".Loc().ToLower()
			}), false, this.DefaultColor, -1, true);
		}
		foreach (KeyValuePair<string, Dictionary<string, List<SmileyChart.SmileyData>>> keyValuePair in from x in data.Data
		orderby x.Key
		select x)
		{
			this.AddText(keyValuePair.Key, true, this.DefaultColor, -1, false);
			foreach (KeyValuePair<string, List<SmileyChart.SmileyData>> keyValuePair2 in keyValuePair.Value.OrderBy((KeyValuePair<string, List<SmileyChart.SmileyData>> x) => x.Key))
			{
				this.AddText(keyValuePair2.Key, false, this.QualityGradient.Evaluate(keyValuePair2.Value.Average((SmileyChart.SmileyData x) => x.Score)), -1, false);
				this.AddChartLines(keyValuePair2.Value);
			}
		}
		this.Window.Show();
	}

	private Text AddText(string value, bool header, Color color, int fontSize = -1, bool center = false)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>((!header) ? this.TextPrefab : this.HeaderPrefab);
		gameObject.GetComponent<Image>().color = color;
		Text componentInChildren = gameObject.GetComponentInChildren<Text>();
		componentInChildren.text = value;
		if (fontSize > 0)
		{
			componentInChildren.fontSize = fontSize;
		}
		if (center)
		{
			componentInChildren.alignment = TextAnchor.MiddleCenter;
		}
		gameObject.transform.SetParent(this.ContentPanel.transform, false);
		this._allContent.Add(gameObject);
		return componentInChildren;
	}

	private void AddChartLines(List<SmileyChart.SmileyData> data)
	{
		int num = Mathf.CeilToInt((float)data.Count / (float)this.MaxPerLine);
		int num2 = Mathf.CeilToInt((float)data.Count / (float)num);
		for (int i = 0; i < num; i++)
		{
			this.AddChart(data.Skip(i * num2).Take(num2));
		}
	}

	private void AddChart(IEnumerable<SmileyChart.SmileyData> data)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.SmileyPrefab);
		SmileyChart component = gameObject.GetComponent<SmileyChart>();
		component.Scores = (from x in data
		orderby Utilities.RandomValue
		select x).ToList<SmileyChart.SmileyData>();
		gameObject.transform.SetParent(this.ContentPanel.transform, false);
		this._allContent.Add(gameObject);
	}

	public GUIWindow Window;

	public GameObject TextPrefab;

	public GameObject HeaderPrefab;

	public GameObject SmileyPrefab;

	public GameObject ContentPanel;

	public ScrollRect scrollRect;

	private List<GameObject> _allContent = new List<GameObject>();

	public int MaxPerLine = 20;

	public Text DateLabel;

	private float _finalScore;

	private float _finalAccuracy;

	public Color DefaultColor;

	public Gradient QualityGradient;

	[Serializable]
	public class ReviewData
	{
		public ReviewData()
		{
		}

		public ReviewData(Dictionary<string, Dictionary<string, List<SmileyChart.SmileyData>>> data, SDateTime buildDate, float score, float accuracy)
		{
			this.Data = data;
			this.BuildDate = buildDate;
			this.Score = score;
			this.Accuracy = accuracy;
		}

		public Dictionary<string, Dictionary<string, List<SmileyChart.SmileyData>>> Data;

		public SDateTime BuildDate;

		public float Score;

		public float Accuracy;
	}
}
