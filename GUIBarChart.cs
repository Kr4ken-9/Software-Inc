﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUIBarChart : Graphic, IPointerEnterHandler, IEventSystemHandler
{
	public void UpdateCachedBars()
	{
		this.DrawBarChart(this.CachedData);
		this.SetVerticesDirty();
	}

	protected override void OnPopulateMesh(VertexHelper h)
	{
		if (!this.Cached)
		{
			this.DrawBarChart(this.CachedData);
		}
		if (this.CachedData == null || this.CachedData.Count == 0)
		{
			h.Clear();
			return;
		}
		h.Clear();
		Utilities.VBOToHelper(this.CachedData, h);
	}

	private float[] FindMinMax(int count)
	{
		float num = 0f;
		float num2 = 1f;
		for (int i = 0; i < count; i++)
		{
			float num3 = 0f;
			float num4 = 0f;
			for (int j = 0; j < this.Values.Count; j++)
			{
				if (this.Values[j][i] > 0f)
				{
					num3 += this.Values[j][i];
				}
				else
				{
					num4 += this.Values[j][i];
				}
			}
			num = Mathf.Min(num, num4);
			num2 = Mathf.Max(num2, num3);
		}
		return new float[]
		{
			num,
			num2
		};
	}

	private void DrawBarChart(List<UIVertex> vbo)
	{
		Vector2 zero = Vector2.zero;
		Vector2 one = Vector2.one;
		zero.x -= base.rectTransform.pivot.x;
		zero.y -= base.rectTransform.pivot.y;
		one.x -= base.rectTransform.pivot.x;
		one.y -= base.rectTransform.pivot.y;
		zero.x *= base.rectTransform.rect.width;
		zero.y *= base.rectTransform.rect.height;
		one.x *= base.rectTransform.rect.width;
		one.y *= base.rectTransform.rect.height;
		vbo.Clear();
		UIVertex simpleVert = UIVertex.simpleVert;
		int num;
		if (this.Values.Count == 0)
		{
			num = 0;
		}
		else
		{
			num = this.Values.MinSafeInt((List<float> z) => z.Count, int.MaxValue, 0);
		}
		int num2 = num;
		float[] array = this.FindMinMax(num2);
		float num3 = array[0];
		float num4 = array[1];
		float num5 = 0.MapRange(num3, num4, zero.y, one.y, false);
		if (num3 == num4)
		{
			return;
		}
		int num6 = Mathf.RoundToInt(Mathf.Clamp((one.x - zero.x) / (float)num2 - this.spacing, 0f, this.spacing));
		float num7 = (one.x - zero.x + (float)num6) / (float)num2 - (float)num6;
		float num8 = zero.x;
		for (int i = 0; i < num2; i++)
		{
			float num9 = num5;
			float num10 = num5;
			for (int j = 0; j < this.Values.Count; j++)
			{
				Color color = (this.Colors.Count != 0) ? this.Colors[(j + this.ColorOffset) % this.Colors.Count] : this.color;
				Color c = (this.Values.Count != 1 || this.Values[j][i] >= 0f) ? color : this.Secondary;
				float num11 = this.Values[j][i].MapRange(num3, num4, zero.y, one.y, false) - num5;
				float num12 = (this.Values[j][i] >= 0f) ? num10 : num9;
				float num13 = num12 + num11;
				simpleVert.position = new Vector2(num8, num12);
				float t = (simpleVert.position.y - zero.y) / (one.y - zero.y);
				t = Mathf.Lerp(0.8f, 1f, t);
				simpleVert.color = this.MultCol(c, t);
				vbo.Add(simpleVert);
				simpleVert.position = new Vector2(num8, num13);
				t = (simpleVert.position.y - zero.y) / (one.y - zero.y);
				t = Mathf.Lerp(0.8f, 1f, t);
				simpleVert.color = this.MultCol(c, t);
				vbo.Add(simpleVert);
				simpleVert.position = new Vector2(num8 + num7, num13);
				t = (simpleVert.position.y - zero.y) / (one.y - zero.y);
				t = Mathf.Lerp(0.8f, 1f, t);
				simpleVert.color = this.MultCol(c, t);
				vbo.Add(simpleVert);
				simpleVert.position = new Vector2(num8 + num7, num12);
				t = (simpleVert.position.y - zero.y) / (one.y - zero.y);
				t = Mathf.Lerp(0.8f, 1f, t);
				simpleVert.color = this.MultCol(c, t);
				vbo.Add(simpleVert);
				if (this.Values[j][i] < 0f)
				{
					num9 = num13;
				}
				else
				{
					num10 = num13;
				}
			}
			num8 += num7 + (float)num6;
		}
		if (num4 > 0f && num3 < 0f)
		{
			GUILineChart.DrawLine(new Vector2(zero.x, num5), new Vector2(one.x, num5), this.BottomLineWidth, this.BottomLineColor, vbo);
		}
	}

	private Color MultCol(Color c, float t)
	{
		return new Color(c.r * t, c.g * t, c.b * t, c.a);
	}

	private void Update()
	{
		if (this._toolTip)
		{
			if (Tooltip.IsShowing)
			{
				if (this.Values.Count == 0)
				{
					Tooltip.Hide();
					this._toolTip = false;
					return;
				}
				Vector2 vector;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(base.rectTransform, Input.mousePosition, null, out vector);
				int num = this.Values.MinSafeInt((List<float> z) => z.Count, int.MaxValue, 0);
				int num2 = Mathf.Clamp(Mathf.FloorToInt((vector.x + base.rectTransform.pivot.x * base.rectTransform.rect.width) / base.rectTransform.rect.width * (float)num), 0, Mathf.Max(0, num - 1));
				float[] array = this.FindMinMax(num);
				float a = array[0];
				float b = array[1];
				float num3 = 0.MapRange(a, b, -base.rectTransform.rect.height * base.rectTransform.pivot.y, base.rectTransform.rect.height * base.rectTransform.pivot.y, false);
				float num4 = num3;
				float num5 = num3;
				float num6 = 0f;
				int i = 0;
				int num7 = -1;
				int num8 = -1;
				while (i < this.Values.Count)
				{
					if (num2 < this.Values[i].Count)
					{
						float num9 = this.Values[i][num2].MapRange(a, b, -base.rectTransform.rect.height * base.rectTransform.pivot.y, base.rectTransform.rect.height * base.rectTransform.pivot.y, false) - num3;
						float num10 = (this.Values[i][num2] >= 0f) ? num5 : num4;
						float num11 = num10 + num9;
						if (num10 > num11)
						{
							float num12 = num10;
							num10 = num11;
							num11 = num12;
						}
						if (vector.y >= num10 && vector.y <= num11)
						{
							num6 = (num10 + num11) / 2f;
							break;
						}
						if (vector.y < num10)
						{
							num6 = (num10 + num11) / 2f;
							num7 = i;
						}
						if (vector.y > num11)
						{
							num6 = (num10 + num11) / 2f;
							num8 = i;
						}
						if (this.Values[i][num2] < 0f)
						{
							num4 = num11;
						}
						else
						{
							num5 = num11;
						}
					}
					i++;
				}
				if (i >= this.Values.Count)
				{
					i = ((num8 <= -1) ? num7 : num8);
					if (i < 0 || i >= this.Values.Count)
					{
						Tooltip.Hide();
						this._toolTip = false;
						return;
					}
				}
				if (num2 >= this.Values[i].Count)
				{
					Tooltip.Hide();
					this._toolTip = false;
					return;
				}
				Vector2 vector2 = new Vector2((float)num2 * (base.rectTransform.rect.width / (float)num) - base.rectTransform.pivot.x * base.rectTransform.rect.width, num6 + 32f);
				Tooltip.UpdateToolTip(this.ToolTipFunc(num2, this.Values[i][num2], this.GetSum(num2)), null, new Vector2(base.rectTransform.position.x / Options.UISize + vector2.x, -((float)Screen.height - base.rectTransform.position.y) / Options.UISize + vector2.y));
			}
			else
			{
				this._toolTip = false;
			}
		}
	}

	private float GetSum(int idx)
	{
		float num = 0f;
		if (this.Values != null)
		{
			for (int i = 0; i < this.Values.Count; i++)
			{
				if (idx < this.Values[i].Count)
				{
					num += this.Values[i][idx];
				}
			}
		}
		return num;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (this.ToolTipFunc != null && this.Values != null && this.Values.Count > 0)
		{
			if (this.Values.Any((List<float> x) => x.Count > 0))
			{
				this._toolTip = true;
				Tooltip.SetToolTip("0", null, base.rectTransform);
			}
		}
	}

	protected override void OnRectTransformDimensionsChange()
	{
		base.OnRectTransformDimensionsChange();
		this.UpdateCachedBars();
	}

	[SerializeField]
	public List<List<float>> Values = new List<List<float>>
	{
		new List<float>
		{
			1f,
			2f,
			4f,
			-2f,
			-5f
		},
		new List<float>
		{
			2f,
			-2f,
			4f,
			-2f,
			3f
		}
	};

	public List<Color> Colors = new List<Color>(HUD.ThemeColors);

	public float spacing = 5f;

	public float BottomLineWidth = 2f;

	public Color BottomLineColor = Color.white;

	public List<UIVertex> CachedData = new List<UIVertex>();

	public bool Cached;

	public Color Secondary;

	public Func<int, float, float, string> ToolTipFunc;

	public int ColorOffset;

	private bool _toolTip;
}
