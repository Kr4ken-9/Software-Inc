﻿using System;
using UnityEngine;

[Serializable]
public struct SDateTime : IComparable<SDateTime>
{
	public SDateTime(int month, int year)
	{
		this = new SDateTime(0, 0, month, year);
	}

	public SDateTime(int day, int month, int year)
	{
		this = new SDateTime(0, 0, day, month, year);
	}

	public SDateTime(int baseYear)
	{
		this.Minute = 0;
		this.Hour = 0;
		this.Day = 0;
		this.Month = 0;
		this.Year = baseYear - SDateTime.BaseYear;
	}

	public SDateTime(int minute, int hour, int month, int year)
	{
		this = new SDateTime(minute, hour, 0, month, year);
	}

	public SDateTime(int minute, int hour, int day, int month, int year)
	{
		SDateTime.FixCounter(ref minute, ref hour, 60);
		SDateTime.FixCounter(ref hour, ref day, 24);
		SDateTime.FixCounter(ref day, ref month, GameSettings.DaysPerMonth);
		SDateTime.FixCounter(ref month, ref year, 12);
		this.Minute = minute;
		this.Hour = hour;
		this.Day = day;
		this.Month = month;
		this.Year = year;
	}

	public int RealYear
	{
		get
		{
			return this.Year + SDateTime.BaseYear;
		}
	}

	private static void FixCounter(ref int count, ref int postCount, int interval)
	{
		if (count >= interval)
		{
			postCount += count / interval;
			count %= interval;
		}
		else if (count < 0)
		{
			postCount -= -count / interval + 1;
			count = interval - -count % interval;
			if (count == interval)
			{
				postCount++;
				count = 0;
			}
		}
	}

	public int ToInt()
	{
		return this.Minute + (this.Hour + (this.Day + (this.Month + this.Year * 12) * GameSettings.DaysPerMonth) * 24) * 60;
	}

	public static SDateTime Lerp(SDateTime start, SDateTime end, float t)
	{
		return SDateTime.FromInt(Mathf.RoundToInt(Mathf.Lerp((float)start.ToInt(), (float)end.ToInt(), t)));
	}

	public static SDateTime FromInt(int d)
	{
		return SDateTime.FromInt(d, (!(GameSettings.Instance == null)) ? GameSettings.DaysPerMonth : GameData.DaysPerMonth);
	}

	public static SDateTime FromInt(int d, int dpm)
	{
		return new SDateTime(d % 60, d / 60 % 24, d / 1440 % dpm, d / (60 * dpm * 24) % 12, d / (60 * dpm * 24 * 12));
	}

	public static SDateTime operator -(SDateTime d1, SDateTime d2)
	{
		return SDateTime.FromInt(d1.ToInt() - d2.ToInt());
	}

	public static SDateTime operator -(SDateTime d1, int n)
	{
		return d1 - new SDateTime(n, 0);
	}

	public static SDateTime operator +(SDateTime d1, int n)
	{
		return d1 + new SDateTime(n, 0);
	}

	public static SDateTime operator +(SDateTime d1, float n)
	{
		if (Mathf.Approximately(n, 0f))
		{
			return d1;
		}
		bool flag = n < 0f;
		if (flag)
		{
			n = -n;
		}
		int num = Mathf.FloorToInt(n);
		n = (n - (float)num) * (float)GameSettings.DaysPerMonth;
		int num2 = Mathf.FloorToInt(n);
		n = (n - (float)num2) * 24f;
		int num3 = Mathf.FloorToInt(n);
		n = (n - (float)num3) * 60f;
		SDateTime d2 = new SDateTime(Mathf.RoundToInt(n), num3, num2, num, 0);
		return (!flag) ? (d1 + d2) : (d1 - d2);
	}

	public static bool operator <(SDateTime d1, SDateTime d2)
	{
		return d1.ToInt() < d2.ToInt();
	}

	public static bool operator >(SDateTime d1, SDateTime d2)
	{
		return d1.ToInt() > d2.ToInt();
	}

	public static SDateTime operator +(SDateTime d1, SDateTime d2)
	{
		return SDateTime.FromInt(d1.ToInt() + d2.ToInt());
	}

	public string ToString(int baseYear)
	{
		if (SDateTime.AMPM)
		{
			int num = this.Hour;
			string text = "AM";
			if (num > 11)
			{
				text = "PM";
				if (num > 12)
				{
					num -= 12;
				}
			}
			return string.Format("{0}:{1} {4}{5} {2} {3}", new object[]
			{
				((num != 0) ? num : 12).ToString(),
				this.Minute.ToString("D2"),
				SDateTime.Months[this.Month % 12].Loc(),
				this.Year + baseYear,
				text,
				(GameSettings.DaysPerMonth <= 1) ? string.Empty : string.Concat(new object[]
				{
					" Day ",
					this.Day + 1,
					"/",
					GameSettings.DaysPerMonth
				})
			});
		}
		return string.Format("{0}:{1}{4} {2} {3}", new object[]
		{
			this.Hour.ToString("D2"),
			this.Minute.ToString("D2"),
			SDateTime.Months[this.Month % 12].Loc(),
			this.Year + baseYear,
			(GameSettings.DaysPerMonth <= 1) ? string.Empty : string.Concat(new object[]
			{
				" Day ",
				this.Day + 1,
				"/",
				GameSettings.DaysPerMonth
			})
		});
	}

	public string ToTimeString(bool minutes = true)
	{
		return (!minutes) ? Utilities.HourToTime(this.Hour, SDateTime.AMPM) : Utilities.HourToTime(this.Hour, this.Minute, SDateTime.AMPM);
	}

	public string ToCompactString()
	{
		return this.ToCompactString(SDateTime.BaseYear);
	}

	public string ToCompactString(int baseYear)
	{
		return string.Format("{0} {1}", SDateTime.Months[this.Month % 12].Loc(), this.Year + baseYear);
	}

	public string ToExtraCompactString()
	{
		return this.ToExtraCompactString(SDateTime.BaseYear);
	}

	public string ToExtraCompactString(int baseYear)
	{
		string text = SDateTime.Months[this.Month % 12].Loc();
		return string.Format("{0}{1}", text.Substring(0, Mathf.Min(3, text.Length)).ToUpper(), (this.Year + baseYear).ToString().Substring(2, 2));
	}

	public string ToCompactString2()
	{
		return this.ToCompactString2(SDateTime.BaseYear);
	}

	public string ToCompactString2(int baseYear)
	{
		return string.Format("{2}{0} {1}", SDateTime.Months[this.Month % 12].Loc(), this.Year + baseYear, (GameSettings.DaysPerMonth <= 1) ? string.Empty : string.Concat(new object[]
		{
			"Day ",
			this.Day + 1,
			"/",
			GameSettings.DaysPerMonth,
			" "
		}));
	}

	public string ToVeryCompactString()
	{
		return this.ToVeryCompactString(SDateTime.BaseYear);
	}

	public string ToVeryCompactString(int baseYear)
	{
		return string.Format("{0} {1}", (SDateTime.Months[this.Month % 12] + "Abbr").Loc(), this.Year + baseYear);
	}

	public string ToQuarterString()
	{
		return string.Concat(new object[]
		{
			"Q",
			this.Month / 3 + 1,
			" ",
			this.RealYear
		});
	}

	public override string ToString()
	{
		return this.ToString(SDateTime.BaseYear);
	}

	public override int GetHashCode()
	{
		return this.ToInt();
	}

	public static bool operator ==(SDateTime d1, SDateTime d2)
	{
		return d1.Equals(d2);
	}

	public static bool operator !=(SDateTime d1, SDateTime d2)
	{
		return !d1.Equals(d2);
	}

	public bool Equals(SDateTime sd)
	{
		return sd.Year == this.Year && sd.Month == this.Month && sd.Day == this.Day && sd.Hour == this.Hour && sd.Minute == this.Minute;
	}

	public override bool Equals(object obj)
	{
		return obj is SDateTime && this.Equals((SDateTime)obj);
	}

	public bool EqualsVerySimple(SDateTime sd)
	{
		return sd.Year == this.Year && sd.Month == this.Month;
	}

	public bool Equals(SDateTime sd, bool simple)
	{
		if (simple)
		{
			return sd.Year == this.Year && sd.Month == this.Month && sd.Day == this.Day;
		}
		return this.Equals(sd);
	}

	public bool Equals(object obj, bool simple)
	{
		return obj is SDateTime && this.Equals((SDateTime)obj, simple);
	}

	public static SDateTime FromString(string input)
	{
		string[] array = input.Split(new char[]
		{
			'-'
		});
		int month = Convert.ToInt32(array[0]) - 1;
		int num = Convert.ToInt32(array[1]);
		return new SDateTime(month, num - SDateTime.BaseYear);
	}

	public int CompareTo(SDateTime other)
	{
		return this.ToInt().CompareTo(other.ToInt());
	}

	public static SDateTime Now()
	{
		return (!(TimeOfDay.Instance == null)) ? TimeOfDay.Instance.GetDate(false) : default(SDateTime);
	}

	public static SDateTime Min(params SDateTime[] times)
	{
		if (times.Length == 0)
		{
			throw new Exception("Cannot find minimum date when no dates are given");
		}
		SDateTime sdateTime = times[0];
		for (int i = 1; i < times.Length; i++)
		{
			if (times[i] < sdateTime)
			{
				sdateTime = times[i];
			}
		}
		return sdateTime;
	}

	public static SDateTime Max(params SDateTime[] times)
	{
		if (times.Length == 0)
		{
			throw new Exception("Cannot find maximum date when no dates are given");
		}
		SDateTime sdateTime = times[0];
		for (int i = 1; i < times.Length; i++)
		{
			if (times[i] > sdateTime)
			{
				sdateTime = times[i];
			}
		}
		return sdateTime;
	}

	public SDateTime Simplify()
	{
		return new SDateTime(0, 0, this.Day, this.Month, this.Year);
	}

	public SDateTime SimplifyMore()
	{
		return new SDateTime(0, 0, 0, this.Month, this.Year);
	}

	public static SDateTime NextMonth(int month)
	{
		SDateTime sdateTime = SDateTime.Now();
		if (month > sdateTime.Month)
		{
			return new SDateTime(0, 0, 0, month, sdateTime.Year);
		}
		return new SDateTime(0, 0, 0, month, sdateTime.Year + 1);
	}

	public static string[] Months = new string[]
	{
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	};

	public static int BaseYear = 1900;

	public static bool AMPM = false;

	public readonly int Minute;

	public readonly int Hour;

	public readonly int Month;

	public readonly int Year;

	public readonly int Day;
}
