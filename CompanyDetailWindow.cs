﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CompanyDetailWindow : MonoBehaviour
{
	public void Withdraw()
	{
		Company c = GameSettings.Instance.MyCompany;
		try
		{
			float num = ((float)Convert.ToDouble(this.WithdrawValue.text)).FromCurrency();
			num = Mathf.Clamp(num, 0f, this.company.Money);
			if (num > 0f)
			{
				if (num >= this.company.Money - 2f)
				{
					WindowManager.Instance.ShowMessageBox("SubsidiaryCloseWarning".Loc(new object[]
					{
						this.company.Name
					}), true, DialogWindow.DialogType.Warning, delegate
					{
						c.MakeTransaction(this.company.Money, Company.TransactionCategory.Intercompany, null);
						this.company.MakeTransaction(-this.company.Money, Company.TransactionCategory.Intercompany, null);
						this.company.MakeTransaction(-3f, Company.TransactionCategory.NA, null);
						this.Round = false;
						this.MoneySlider.value = 0f;
						this.Round = false;
						this.UpdateTexts();
					}, "Close subsidiary", null);
				}
				else
				{
					c.MakeTransaction(num, Company.TransactionCategory.Intercompany, null);
					this.company.MakeTransaction(-num, Company.TransactionCategory.Intercompany, null);
					this.Round = false;
					this.MoneySlider.value = 0f;
					this.Round = false;
					this.UpdateTexts();
				}
			}
		}
		catch (Exception)
		{
		}
	}

	public void Deposit()
	{
		Company myCompany = GameSettings.Instance.MyCompany;
		try
		{
			float num = ((float)Convert.ToDouble(this.DepositValue.text)).FromCurrency();
			num = Mathf.Clamp(num, 0f, myCompany.Money - 100f);
			if (num > 0f)
			{
				myCompany.MakeTransaction(-num, Company.TransactionCategory.Intercompany, null);
				this.company.MakeTransaction(num, Company.TransactionCategory.Intercompany, null);
				this.Round = false;
				this.MoneySlider.value = 0f;
				this.Round = false;
				this.UpdateTexts();
			}
		}
		catch (Exception)
		{
		}
	}

	public void UpdateTexts()
	{
		Company myCompany = GameSettings.Instance.MyCompany;
		float num = this.MoneySlider.value.MapRange(0f, 1f, 0f, Mathf.Max(0f, myCompany.Money - 100f), false);
		float num2 = this.MoneySlider.value.MapRange(0f, 1f, 0f, Mathf.Max(0f, this.company.Money), false);
		if (this.Round && this.MoneySlider.value != 1f)
		{
			float num3 = Mathf.Max(1f, Mathf.Pow(10f, Mathf.Floor(Mathf.Log10(myCompany.Money - 100f))) / 10f);
			num = Mathf.Clamp(Mathf.Round(num / num3) * num3, 0f, myCompany.Money - 100f);
			float num4 = Mathf.Max(1f, Mathf.Pow(10f, Mathf.Floor(Mathf.Log10(this.company.Money))) / 10f);
			num2 = Mathf.Clamp(Mathf.Round(num2 / num4) * num4, 0f, this.company.Money);
		}
		if (!num2.IsValidFloat())
		{
			num2 = 0f;
		}
		if (!num.IsValidFloat())
		{
			num = 0f;
		}
		this.DepositValue.text = num.CurrencyMul().ToString("N0");
		this.WithdrawValue.text = num2.CurrencyMul().ToString("N0");
		this.Round = true;
	}

	public void FixInputfields(int type)
	{
		if (type == 0)
		{
			try
			{
				float num = (float)Convert.ToDouble(this.DepositValue.text);
				num = num.FromCurrency();
				float num2 = GameSettings.Instance.MyCompany.Money - 100f;
				if (num2 > 0f)
				{
					num = Mathf.Clamp(num, 0f, num2);
					this.Round = false;
					this.MoneySlider.value = num / num2;
					this.Round = false;
					this.UpdateTexts();
				}
				else
				{
					this.Round = false;
					this.MoneySlider.value = 0f;
					this.Round = false;
					this.UpdateTexts();
				}
			}
			catch (Exception)
			{
				this.Round = false;
				this.MoneySlider.value = 0f;
				this.Round = false;
				this.UpdateTexts();
			}
		}
		if (type == 1)
		{
			try
			{
				float num3 = (float)Convert.ToDouble(this.WithdrawValue.text);
				num3 = num3.FromCurrency();
				float money = this.company.Money;
				if (money > 0f)
				{
					num3 = Mathf.Clamp(num3, 0f, money);
					this.Round = false;
					this.MoneySlider.value = num3 / money;
					this.Round = false;
					this.UpdateTexts();
				}
				else
				{
					this.Round = false;
					this.MoneySlider.value = 0f;
					this.Round = false;
					this.UpdateTexts();
				}
			}
			catch (Exception)
			{
				this.Round = false;
				this.MoneySlider.value = 0f;
				this.Round = false;
				this.UpdateTexts();
			}
		}
	}

	private void Start()
	{
		this._initializing = true;
		this.window.OnSizeChanged = new Action(this.chart.UpdateCachedPie);
		this.chart.Colors = HUD.ThemeColors.ToList<Color>();
		this.ShareList["StockSell"].gameObject.SetActive(this.company.Player);
		this.window.OnClose = delegate
		{
			this.company.OwnedStock.OnChange = null;
		};
		this.window.NonLocTitle = this.company.Name;
		this.ShareList.Items.AddRange(this.company.OwnedStock.Cast<object>());
		this.company.OwnedStock.OnChange = delegate
		{
			this.company.OwnedStock.Update<object>(this.ShareList.Items);
		};
		this.UpdateDistributionButton();
		this.PatentList.Items = (from x in this.company.Patents
		select GameSettings.Instance.SoftwareTypes[x.Key].Features[x.Value]).Cast<object>().ToList<object>();
		for (int i = 0; i < this.company.Stocks.Length; i++)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.StockButtonPrefab);
			StockButton component = gameObject.GetComponent<StockButton>();
			component.CompanyID = this.company.ID;
			component.Stock = i;
			gameObject.transform.SetParent(this.StockPanel, false);
			component.ActionButton.onClick.AddListener(delegate
			{
				this.UpdateChart();
				this.UpdateStocks();
			});
		}
		this.UpdateChart();
		this.UpdateStocks();
		TutorialSystem.Instance.StartTutorial("Stocks", false);
		SimulatedCompany simulatedCompany = this.company as SimulatedCompany;
		this.AutonomyToggle.isOn = (simulatedCompany != null && simulatedCompany.Autonomous);
		this._initializing = false;
	}

	public void ToggleAutonomy(bool value)
	{
		if (!this._initializing)
		{
			SimulatedCompany simulatedCompany = this.company as SimulatedCompany;
			if (simulatedCompany != null && this.company.OwnerCompany == GameSettings.Instance.MyCompany)
			{
				simulatedCompany.Autonomous = value;
			}
		}
	}

	public void UpdateStocks()
	{
		bool flag = this.company.IsPlayerOwned();
		this.StocksPanel.SetActive(!flag);
		this.PiePanel.SetActive(!flag);
		this.PatentSharePanel.SetActive(!flag);
		this.SubsidiaryPanel.SetActive(flag);
		this.Subsidiary2Panel.SetActive(flag);
		this.MoneySlider.value = 0f;
	}

	public void UpdateDistributionButton()
	{
		this.DistributionDealButton.gameObject.SetActive(this.company != GameSettings.Instance.MyCompany && !this.company.IsPlayerOwned() && GameSettings.Instance.Distribution != null);
		this.DistributionDealLabel.text = ((this.company.DistributionDeal == null) ? "DistributionOffer".Loc() : string.Format("DistributionOfferMade".Loc(), (this.company.DistributionDeal.Value * 100f).ToString("N2")));
	}

	public void MakeDistributionDeal()
	{
		float months = Utilities.GetMonths(this.company.LastDistributionOffer, SDateTime.Now());
		if (this.company.DistributionDeal != null)
		{
			WindowManager.Instance.ShowMessageBox(string.Format("CancelDistribution".Loc(), this.company.Name), false, DialogWindow.DialogType.Question, delegate
			{
				this.company.LastDistributionOffer = SDateTime.Now();
				this.company.DistributionDeal = null;
				this.UpdateDistributionButton();
				HUD.Instance.distributionWindow.UpdateDistributionDeals();
			}, "Cancel distribution deal", null);
		}
		else if (months < 1f)
		{
			WindowManager.Instance.ShowMessageBox("DistributionDealCooldown".Loc(), false, DialogWindow.DialogType.Information);
		}
		else
		{
			float num = GameSettings.Instance.simulation.GetAllCompanies().MaxSafe((Company x) => x.Products.SumSafe((SoftwareProduct z) => z.Sum), 0f);
			float num2 = this.company.Products.SumSafe((SoftwareProduct x) => x.Sum) / num;
			List<float> playerDigitalShare = GameSettings.Instance.simulation.PlayerDigitalShare;
			float num3 = Mathf.Lerp(0.25f - num2 * 0.25f, 1f, Mathf.Pow(playerDigitalShare[playerDigitalShare.Count - 1], 3f));
			float willing = (this.company.LastDistributionDeal == null || months >= 12f) ? (num3 * MarketSimulation.DistributionStandardCut) : Mathf.Min(MarketSimulation.DistributionStandardCut, this.company.LastDistributionDeal.Value + num3 * 0.05f);
			System.Random random = new System.Random(this.company.Name.GetHashCode());
			float num4 = 0.25f + (float)random.NextDouble() * 0.5f;
			WindowManager.SpawnInputDialog(string.Format("DistributionDealPrompt".Loc(), this.company.Name), "Distribution", (willing * num4 * 100f).ToString("N1"), delegate(string x)
			{
				try
				{
					float num5 = (float)Convert.ToDouble(x) / 100f;
					this.company.LastDistributionOffer = SDateTime.Now();
					if (num5 <= willing)
					{
						this.company.LastDistributionDeal = (this.company.DistributionDeal = new float?(num5));
						this.UpdateDistributionButton();
						HUD.Instance.distributionWindow.UpdateDistributionDeals();
					}
					else
					{
						WindowManager.Instance.ShowMessageBox(string.Format("DistributionDealDenied".Loc(), this.company.Name), false, DialogWindow.DialogType.Information);
					}
				}
				catch (Exception)
				{
					WindowManager.Instance.ShowMessageBox("InvalidAmount".Loc(), false, DialogWindow.DialogType.Error);
				}
			}, null);
		}
	}

	private void UpdateChart()
	{
		if (this.PiePanel.activeSelf)
		{
			this.chart.Values = (from x in this.company.Stocks
			where x != null
			group x by x.Owner into x
			select x.Sum((Stock z) => z.CurrentWorth)).ToList<float>();
			this.chart.UpdateCachedPie();
		}
	}

	private void Update()
	{
		if (this.company.Bankrupt)
		{
			this.window.Close();
		}
		if (this.company.IsPlayerOwned())
		{
			SimulatedCompany simulatedCompany = this.company as SimulatedCompany;
			if (simulatedCompany != null)
			{
				this.Projects.Items.SyncContent<SimulatedCompany.ProductPrototype>(simulatedCompany.Releases);
			}
		}
		Text companyInfo = this.CompanyInfo;
		string input = "CompanyDetailInfo";
		object[] array = new object[6];
		array[0] = this.company.GetMoneyWithInsurance(true).Currency(true);
		array[1] = (this.company.GetShare() * 100f).ToString("F0") + "%";
		array[2] = this.company.Founded.ToCompactString();
		array[3] = this.company.Products.Count;
		array[4] = this.company.Products.Count((SoftwareProduct x) => !x.Traded);
		array[5] = this.company.CompaniesBought;
		companyInfo.text = input.Loc(array);
	}

	public void ShowChart()
	{
		HUD.Instance.companyChart.Show(this.company);
	}

	public void ShowProducts()
	{
		ProductWindow productWindow = HUD.Instance.GetProductWindow("AllRelease");
		productWindow.Show(true, "CompanyReleases".Loc(new object[]
		{
			this.company.Name
		}), false);
		productWindow.SetFilters(false, true, true);
		productWindow.SetCompany(this.company.Name);
	}

	public GameObject StockButtonPrefab;

	public GUIWindow window;

	[NonSerialized]
	public Company company;

	public RectTransform StockPanel;

	public Text CompanyInfo;

	public GUIListView ShareList;

	public GUIListView PatentList;

	public GUIPieChart chart;

	public Button DistributionDealButton;

	public Text DistributionDealLabel;

	public Slider MoneySlider;

	public InputField DepositValue;

	public InputField WithdrawValue;

	public GameObject StocksPanel;

	public GameObject SubsidiaryPanel;

	public GameObject Subsidiary2Panel;

	public GameObject PiePanel;

	public GameObject PatentSharePanel;

	public Toggle AutonomyToggle;

	public GUIListView Projects;

	public bool Round = true;

	private bool _initializing;
}
