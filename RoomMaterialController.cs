﻿using System;
using System.Collections.Generic;
using System.IO;
using DevConsole;
using UnityEngine;

public class RoomMaterialController : MonoBehaviour
{
	public static int GetMaterialID(string mat)
	{
		if (RoomMaterialController.Instance != null)
		{
			RoomMaterialController.WallMaterial orNull = RoomMaterialController.Instance.AllMaterials.GetOrNull(mat);
			return (orNull == null) ? 0 : orNull.ID;
		}
		return 0;
	}

	public static void UpdateCutoffMat()
	{
		if (RoomMaterialController.Instance != null && GameSettings.Instance != null)
		{
			if (GameSettings.WallsDown == GameSettings.WallState.Back)
			{
				RoomMaterialController.Instance.MainCutMat.SetFloat("_Cutoff", (float)GameSettings.Instance.ActiveFloor * 2f + 1.8f);
				RoomMaterialController.Instance.MainCutMat.SetFloat("_CutoffTop", 1f);
			}
			else
			{
				RoomMaterialController.Instance.MainCutMat.SetFloat("_Cutoff", (float)GameSettings.Instance.ActiveFloor * 2f + 0.2f);
				RoomMaterialController.Instance.MainCutMat.SetFloat("_CutoffTop", 0f);
			}
		}
	}

	public void Init()
	{
		if (RoomMaterialController.Initialized)
		{
			return;
		}
		RoomMaterialController.Initialized = true;
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		this.AllMaterials = new Dictionary<string, RoomMaterialController.WallMaterial>();
		for (int i = 0; i < this.RoomMaterials.Length; i++)
		{
			RoomMaterialController.WallMaterial wallMaterial = this.RoomMaterials[i];
			this.AllMaterials[wallMaterial.Name] = wallMaterial;
		}
		for (int j = 0; j < this.MaterialPacks.Count; j++)
		{
			RoomMaterialPack roomMaterialPack = this.MaterialPacks[j];
			for (int k = 0; k < roomMaterialPack.Materials.Length; k++)
			{
				RoomMaterialController.WallMaterial wallMaterial2 = roomMaterialPack.Materials[k];
				this.AllMaterials[wallMaterial2.Name] = wallMaterial2;
			}
		}
		this.ColorController = new RoomMaterialController.ColorTexController(256, 256);
		int num = Mathf.NextPowerOfTwo(Mathf.CeilToInt(Mathf.Sqrt((float)this.AllMaterials.Count)) * 256);
		int num2 = SystemInfo.maxTextureSize;
		if (num > num2)
		{
			Debug.Log(string.Concat(new object[]
			{
				"Couldn't fit all room materials, max system texture size: ",
				num2,
				", needed: ",
				num
			}));
			LoadDebugger.AddError("Too many room materals to fit graphics card");
		}
		num2 = Mathf.Min(num2, num);
		int num3 = num2 / 256;
		num3 *= num3;
		this.BaseTex = new RenderTexture(num2, num2, 0, RenderTextureFormat.ARGB32);
		this.BumpTex = new RenderTexture(num2, num2, 0, RenderTextureFormat.ARGB32);
		this.ExtraTex = new RenderTexture(num2, num2, 0, RenderTextureFormat.ARGB32);
		this.BaseTex.autoGenerateMips = false;
		this.BumpTex.autoGenerateMips = false;
		this.ExtraTex.autoGenerateMips = false;
		this.BaseTex.useMipMap = SystemInfo.supportsInstancing;
		this.BumpTex.useMipMap = SystemInfo.supportsInstancing;
		this.ExtraTex.useMipMap = SystemInfo.supportsInstancing;
		this.TextureRendMat.SetVector("_Offset", new Vector4(0f, 0f, 1f, 1f));
		Graphics.Blit(this.DefaultBase, this.BaseTex, this.TextureRendMat);
		Graphics.Blit(this.DefaultBump, this.BumpTex, this.TextureRendMat);
		Graphics.Blit(this.DefaultExtra, this.ExtraTex, this.TextureRendMat);
		if (SystemInfo.supportsInstancing)
		{
			this.MainMat.SetTexture("_MainTex", this.BaseTex);
			this.MainMat.SetTexture("_BumpTex", this.BumpTex);
			this.MainMat.SetTexture("_ExtraTex", this.ExtraTex);
			this.MainCutMat.SetTexture("_MainTex", this.BaseTex);
			this.MainCutMat.SetTexture("_BumpTex", this.BumpTex);
			this.MainCutMat.SetTexture("_ExtraTex", this.ExtraTex);
		}
		this.MainMat.SetTexture("_ColorTex", this.ColorController.MainTex);
		this.MainCutMat.SetTexture("_ColorTex", this.ColorController.MainTex);
		this.TexCells = num2 / 256;
		this.MainMat.SetFloat("_TexSize", (float)this.TexCells);
		this.MainMat.SetFloat("_ColorSize", 256f);
		this.MainCutMat.SetFloat("_TexSize", (float)this.TexCells);
		this.MainCutMat.SetFloat("_ColorSize", 256f);
		this.MainMat.SetFloat("_Margin", 8f);
		this.MainCutMat.SetFloat("_Margin", 8f);
		this.TextureRendMat.SetFloat("_Margin", 0.03125f);
		int num4 = 0;
		bool flag = RoomMaterialController.ErrorsDuringLoad;
		foreach (RoomMaterialController.WallMaterial wallMaterial3 in this.AllMaterials.Values)
		{
			if (wallMaterial3.Load())
			{
				flag = true;
			}
			wallMaterial3.ID = num4;
			this.TextureRendMat.SetVector("_Offset", new Vector4((float)(num4 % this.TexCells), (float)(num4 / this.TexCells), (float)this.TexCells, (float)this.TexCells));
			if (wallMaterial3.Base != null)
			{
				Graphics.Blit(wallMaterial3.Base, this.BaseTex, this.TextureRendMat);
			}
			if (wallMaterial3.Bump != null)
			{
				Graphics.Blit(wallMaterial3.Bump, this.BumpTex, this.TextureRendMat);
			}
			if (wallMaterial3.Extra != null)
			{
				Graphics.Blit(wallMaterial3.Extra, this.ExtraTex, this.TextureRendMat);
			}
			if (wallMaterial3.AddSkirting)
			{
				Graphics.Blit(this.SkirtingBase, this.BaseTex, this.TextureRendMat);
				Graphics.Blit(this.SkirtingNormal, this.BumpTex, this.TextureRendMat);
				Graphics.Blit(this.SkirtingExtra, this.ExtraTex, this.TextureRendMat);
			}
			wallMaterial3.Unload();
			num4++;
			if (num4 >= num3)
			{
				break;
			}
		}
		if (SystemInfo.supportsInstancing)
		{
			this.BaseTex.GenerateMips();
			this.BumpTex.GenerateMips();
			this.ExtraTex.GenerateMips();
		}
		else
		{
			Texture2D texture2D = new Texture2D(this.BaseTex.width, this.BaseTex.height, TextureFormat.ARGB32, true);
			Texture2D texture2D2 = new Texture2D(this.BaseTex.width, this.BaseTex.height, TextureFormat.ARGB32, true);
			Texture2D texture2D3 = new Texture2D(this.BaseTex.width, this.BaseTex.height, TextureFormat.ARGB32, true);
			RenderTexture active = RenderTexture.active;
			RenderTexture.active = this.BaseTex;
			texture2D.ReadPixels(new Rect(0f, 0f, (float)texture2D.width, (float)texture2D.height), 0, 0, true);
			texture2D.Apply();
			RenderTexture.active = this.BumpTex;
			texture2D2.ReadPixels(new Rect(0f, 0f, (float)texture2D2.width, (float)texture2D2.height), 0, 0, true);
			texture2D2.Apply();
			RenderTexture.active = this.ExtraTex;
			texture2D3.ReadPixels(new Rect(0f, 0f, (float)texture2D3.width, (float)texture2D3.height), 0, 0, true);
			texture2D3.Apply();
			RenderTexture.active = active;
			this.MainMat.SetTexture("_MainTex", texture2D);
			this.MainMat.SetTexture("_BumpTex", texture2D2);
			this.MainMat.SetTexture("_ExtraTex", texture2D3);
			this.MainCutMat.SetTexture("_MainTex", texture2D);
			this.MainCutMat.SetTexture("_BumpTex", texture2D2);
			this.MainCutMat.SetTexture("_ExtraTex", texture2D3);
		}
		Debug.Log(string.Concat(new object[]
		{
			"material texture build time: ",
			Time.realtimeSinceStartup - realtimeSinceStartup,
			" for ",
			num4,
			" textures"
		}));
		if (flag)
		{
			LoadDebugger.AddError("There were errors while loading materials");
		}
		LoadDebugger.AddInfo("Finished loading room materials");
	}

	private void Awake()
	{
		if (RoomMaterialController.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		RoomMaterialController.Instance = this;
		string path = Path.Combine(Utilities.GetRoot(), "Materials");
		if (!Directory.Exists(path))
		{
			Directory.CreateDirectory(path);
		}
		else
		{
			foreach (string root in Directory.GetDirectories(path))
			{
				RoomMaterialPack roomMaterialPack = RoomMaterialPack.LoadPack(root, true, ref RoomMaterialController.ErrorsDuringLoad);
				if (roomMaterialPack != null)
				{
					this.MaterialPacks.Add(roomMaterialPack);
				}
			}
		}
	}

	private void FixedUpdate()
	{
		if (RoomMaterialController.Initialized)
		{
			this.ColorController.Update();
		}
	}

	public static int TakeColor()
	{
		if (RoomMaterialController.Instance != null)
		{
			return RoomMaterialController.Instance.ColorController.TakeColor();
		}
		throw new Exception("Tried to reserve color without controller");
	}

	public static Color GetColor(int id)
	{
		if (id == -1)
		{
			return new Color(0f, 0f, 0f, 0f);
		}
		if (RoomMaterialController.Instance != null)
		{
			return RoomMaterialController.Instance.ColorController.GetColor(id);
		}
		throw new Exception("Tried to read color without controller");
	}

	public static void FreeColor(int id)
	{
		if (id == -1)
		{
			return;
		}
		if (RoomMaterialController.Instance != null)
		{
			RoomMaterialController.Instance.ColorController.FreeColor(id);
			return;
		}
		throw new Exception("Tried to free color without controller");
	}

	public static void WriteColor(int id, Color col)
	{
		if (id == -1)
		{
			return;
		}
		if (RoomMaterialController.Instance != null)
		{
			RoomMaterialController.Instance.ColorController.WriteColor(id, col);
			return;
		}
		throw new Exception("Tried to write color without controller");
	}

	public static void Clear()
	{
		if (RoomMaterialController.Instance != null && RoomMaterialController.Instance.ColorController != null)
		{
			RoomMaterialController.Instance.ColorController.Clear();
			RoomMaterialController.Instance.GroundColorID = RoomMaterialController.Instance.ColorController.TakeColor();
			RoomMaterialController.Instance.BlackColorID = RoomMaterialController.Instance.ColorController.TakeColor();
			RoomMaterialController.Instance.ColorController.WriteColor(RoomMaterialController.Instance.BlackColorID, Color.black);
		}
	}

	private void CreateTestObject(int tex, int color, int size, Vector3 pos)
	{
		GameObject gameObject = new GameObject("TEST");
		gameObject.transform.position = pos;
		MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
		MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
		meshRenderer.material = this.MainMat;
		meshFilter.sharedMesh = new Mesh
		{
			vertices = new Vector3[]
			{
				new Vector3(0f, 0f, 0f),
				new Vector3((float)size, 0f, 0f),
				new Vector3((float)size, 0f, (float)size),
				new Vector3(0f, 0f, (float)size)
			},
			uv = new Vector2[]
			{
				new Vector2(0f, 0f),
				new Vector2((float)size, 0f),
				new Vector2((float)size, (float)size),
				new Vector2(0f, (float)size)
			},
			uv2 = new Vector2[]
			{
				new Vector2((float)color, (float)tex),
				new Vector2((float)color, (float)tex),
				new Vector2((float)color, (float)tex),
				new Vector2((float)color, (float)tex)
			},
			normals = new Vector3[]
			{
				Vector3.up,
				Vector3.up,
				Vector3.up,
				Vector3.up
			},
			triangles = new int[]
			{
				2,
				1,
				0,
				0,
				3,
				2
			}
		};
	}

	public static RoomMaterialController Instance;

	public RenderTexture BaseTex;

	public RenderTexture BumpTex;

	public RenderTexture ExtraTex;

	public Texture DefaultBase;

	public Texture DefaultBump;

	public Texture DefaultExtra;

	public Texture SkirtingBase;

	public Texture SkirtingNormal;

	public Texture SkirtingExtra;

	public RoomMaterialController.WallMaterial[] RoomMaterials;

	public Material TextureRendMat;

	public Material MainMat;

	public Material MainCutMat;

	public Material TestMat;

	[NonSerialized]
	public Dictionary<string, RoomMaterialController.WallMaterial> AllMaterials;

	public int TexCells;

	[NonSerialized]
	public int GroundColorID = -1;

	[NonSerialized]
	public int BlackColorID = -1;

	[NonSerialized]
	public List<RoomMaterialPack> MaterialPacks = new List<RoomMaterialPack>();

	[NonSerialized]
	public RoomMaterialController.ColorTexController ColorController;

	private static bool Initialized;

	public static bool ErrorsDuringLoad;

	[Serializable]
	public class WallMaterial
	{
		public WallMaterial()
		{
		}

		public WallMaterial(string name, string category, string baseTex, string bumpTex, string extraTex, bool skirting)
		{
			this._shouldLoad = true;
			this.Name = name;
			this.Category = category;
			this._baseTexFile = baseTex;
			this._bumpTexFile = bumpTex;
			this._extraTexFile = extraTex;
			this.AddSkirting = skirting;
		}

		public bool Load()
		{
			if (this._shouldLoad && !this._isLoaded)
			{
				if (this._isMipped)
				{
					if (this.Base != null)
					{
						UnityEngine.Object.Destroy(this.Base);
					}
					if (this.Extra != null)
					{
						UnityEngine.Object.Destroy(this.Extra);
					}
					if (this.Bump != null)
					{
						UnityEngine.Object.Destroy(this.Bump);
					}
					this._isMipped = false;
				}
				bool result = false;
				this.Base = this.LoadTexture(this._baseTexFile, "Base", ref result);
				this.Bump = this.LoadTexture(this._bumpTexFile, "Bump", ref result);
				this.Extra = this.LoadTexture(this._extraTexFile, "Extra", ref result);
				this._isLoaded = true;
				return result;
			}
			return false;
		}

		public void Unload()
		{
			if (this._shouldLoad && this._isLoaded)
			{
				if (this.Base != null)
				{
					RoomMaterialController.WallMaterial.Downscale(ref this.Base);
				}
				if (this.Extra != null)
				{
					RoomMaterialController.WallMaterial.Downscale(ref this.Extra);
				}
				if (this.Bump != null)
				{
					RoomMaterialController.WallMaterial.Downscale(ref this.Bump);
				}
				this._isLoaded = false;
				this._isMipped = true;
			}
		}

		private static void Downscale(ref Texture tex)
		{
			Texture2D texture2D = new Texture2D(128, 128, TextureFormat.ARGB32, false);
			Graphics.CopyTexture(tex, 0, 1, texture2D, 0, 0);
			UnityEngine.Object.Destroy(tex);
			tex = texture2D;
		}

		private Texture2D LoadTexture(string path, string type, ref bool errors)
		{
			if (path == null)
			{
				return null;
			}
			try
			{
				Texture2D texture2D = new Texture2D(256, 256, TextureFormat.ARGB32, true);
				texture2D.LoadImage(File.ReadAllBytes(path));
				if (texture2D.width != 256 || texture2D.height != 256)
				{
					throw new Exception(string.Concat(new object[]
					{
						type,
						" texture size is ",
						texture2D.width,
						"x",
						texture2D.height,
						", should be: 256 x 256"
					}));
				}
				return texture2D;
			}
			catch (Exception ex)
			{
				errors = true;
				Debug.LogException(new Exception("Error loading texture for material " + this.Name + ":\n" + ex.ToString()));
				if (Options.ConsoleOnError && !DevConsole.Console.isOpen)
				{
					DevConsole.Console.Open();
				}
			}
			return null;
		}

		[NonSerialized]
		public int ID;

		public string Name;

		public string Category;

		public Texture Base;

		public Texture Bump;

		public Texture Extra;

		public bool AddSkirting;

		[NonSerialized]
		public bool FromSteam;

		[NonSerialized]
		private string _baseTexFile;

		[NonSerialized]
		private string _bumpTexFile;

		[NonSerialized]
		private string _extraTexFile;

		[NonSerialized]
		private bool _shouldLoad;

		[NonSerialized]
		private bool _isLoaded;

		[NonSerialized]
		private bool _isMipped;
	}

	public class ColorTexController
	{
		public ColorTexController(int size, int chunkSize)
		{
			this.Size = size;
			this.ChunkSize = chunkSize;
			this.MainTex = new Texture2D(size, size, TextureFormat.ARGB32, false);
			this.MainTex.filterMode = FilterMode.Point;
			int num = this.Size / this.ChunkSize;
			this.Chunks = new RoomMaterialController.ColorTexController.ColorChunk[num, num];
			for (int i = 0; i < this.Chunks.GetLength(0); i++)
			{
				for (int j = 0; j < this.Chunks.GetLength(1); j++)
				{
					this.Chunks[i, j] = new RoomMaterialController.ColorTexController.ColorChunk(this.ChunkSize, i + j * this.Chunks.GetLength(0));
				}
			}
		}

		public void Update()
		{
			if (this.Dirty)
			{
				for (int i = 0; i < this.Chunks.GetLength(0); i++)
				{
					for (int j = 0; j < this.Chunks.GetLength(1); j++)
					{
						RoomMaterialController.ColorTexController.ColorChunk colorChunk = this.Chunks[i, j];
						if (colorChunk.Dirty)
						{
							this.MainTex.SetPixels(i * this.ChunkSize, j * this.ChunkSize, this.ChunkSize, this.ChunkSize, colorChunk.Chunk, 0);
							colorChunk.Dirty = false;
						}
					}
				}
				this.MainTex.Apply(false);
				this.Dirty = false;
			}
		}

		public int TakeColor()
		{
			for (int i = 0; i < this.Chunks.GetLength(0); i++)
			{
				for (int j = 0; j < this.Chunks.GetLength(1); j++)
				{
					RoomMaterialController.ColorTexController.ColorChunk colorChunk = this.Chunks[i, j];
					if (colorChunk.Free > 0)
					{
						return colorChunk.Num * this.Chunks.Length + colorChunk.GetFree();
					}
				}
			}
			throw new Exception("No more color chunks left!");
		}

		public void WriteColor(int id, Color color)
		{
			int num = this.ChunkSize * this.ChunkSize;
			int num2 = id / num;
			int num3 = num2 % this.Chunks.GetLength(0);
			int num4 = num2 / this.Chunks.GetLength(0);
			RoomMaterialController.ColorTexController.ColorChunk colorChunk = this.Chunks[num3, num4];
			colorChunk.Write(id - num2 * num, color);
			this.Dirty = true;
		}

		public void FreeColor(int id)
		{
			int num = this.ChunkSize * this.ChunkSize;
			int num2 = id / num;
			int num3 = num2 % this.Chunks.GetLength(0);
			int num4 = num2 / this.Chunks.GetLength(0);
			RoomMaterialController.ColorTexController.ColorChunk colorChunk = this.Chunks[num3, num4];
			colorChunk.FreeColor(id - num2 * num);
		}

		public Color GetColor(int id)
		{
			int num = this.ChunkSize * this.ChunkSize;
			int num2 = id / num;
			int num3 = num2 % this.Chunks.GetLength(0);
			int num4 = num2 / this.Chunks.GetLength(0);
			RoomMaterialController.ColorTexController.ColorChunk colorChunk = this.Chunks[num3, num4];
			return colorChunk.Chunk[id - num2 * num];
		}

		public Vector2 GetColorUV(int id)
		{
			int num = this.ChunkSize * this.ChunkSize;
			int num2 = id / num;
			int num3 = num2 % this.Chunks.GetLength(0);
			int num4 = num2 / this.Chunks.GetLength(0);
			num3 *= this.ChunkSize;
			num4 *= this.ChunkSize;
			int num5 = id - num2 * num;
			return new Vector2(((float)(num3 + num5 % this.ChunkSize) + 0.5f) / (float)this.Size, ((float)(num4 + num5 / this.ChunkSize) + 0.5f) / (float)this.Size);
		}

		public void Clear()
		{
			for (int i = 0; i < this.Chunks.GetLength(0); i++)
			{
				for (int j = 0; j < this.Chunks.GetLength(1); j++)
				{
					this.Chunks[i, j].Clear();
				}
			}
		}

		public Texture2D MainTex;

		public int Size;

		public int ChunkSize;

		public RoomMaterialController.ColorTexController.ColorChunk[,] Chunks;

		public bool Dirty;

		public class ColorChunk
		{
			public ColorChunk(int size, int num)
			{
				int num2 = size * size;
				this.Chunk = new Color[num2];
				this.Num = num;
				this.Clear();
			}

			public int GetFree()
			{
				for (int i = this.LastFree; i < this.Chunk.Length; i++)
				{
					if (this.Chunk[i].a < 0.5f)
					{
						this.LastFree = i + 1;
						this.Write(i, new Color(1f, 1f, 1f, 1f));
						this.Free--;
						return i;
					}
				}
				throw new Exception("Tried to get color from filled color chunk");
			}

			public void Write(int num, Color col)
			{
				this.Chunk[num] = col;
				this.Dirty = true;
			}

			public void FreeColor(int num)
			{
				this.LastFree = Mathf.Min(num, this.LastFree);
				this.Free++;
				this.Chunk[num] = new Color(0f, 0f, 0f, 0f);
			}

			public void Clear()
			{
				for (int i = 0; i < this.Chunk.Length; i++)
				{
					this.Chunk[i] = new Color(0f, 0f, 0f, 0f);
				}
				this.LastFree = 0;
				this.Free = this.Chunk.Length;
			}

			public Color[] Chunk;

			public int Num;

			public int Free;

			public int LastFree;

			public bool Dirty;
		}
	}
}
