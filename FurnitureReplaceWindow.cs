﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FurnitureReplaceWindow : MonoBehaviour
{
	public void Show(List<Furniture> furns)
	{
		this._furns.Clear();
		this._furns.AddRange(furns);
		foreach (KeyValuePair<Toggle, Furniture> keyValuePair in this._upgradeTo)
		{
			UnityEngine.Object.Destroy(keyValuePair.Key.gameObject);
		}
		this._upgradeTo.Clear();
		foreach (Furniture furniture in from x in (from x in this._furns
		select x.UpgradeTo).Distinct<string>().SelectMany((string x) => ObjectDatabase.Instance.GetUpgrades(x)).Distinct<Furniture>()
		where x != null && x.IsPlayerControlled()
		orderby x.Cost
		select x)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.BuildButtonPrototype);
			gameObject.transform.SetParent(this.ButtonPanel);
			Toggle component = gameObject.GetComponent<Toggle>();
			this._upgradeTo[component] = furniture;
			gameObject.GetComponentsInChildren<Image>()[2].sprite = furniture.Thumbnail;
			component.isOn = false;
			component.group = this.ButtonGroup;
			Furniture f1 = furniture;
			component.onValueChanged.AddListener(delegate(bool x)
			{
				if (x)
				{
					this.DetailPanel.SetFurniture(f1);
				}
			});
		}
		if (this._upgradeTo.Count > 0)
		{
			this._upgradeTo.First<KeyValuePair<Toggle, Furniture>>().Key.isOn = true;
			this.Window.Show();
		}
	}

	public void Accept()
	{
		Furniture value = this._upgradeTo.First((KeyValuePair<Toggle, Furniture> x) => x.Key.isOn).Value;
		if (!Cheats.UnlockFurn && !GameSettings.Instance.EditMode && TimeOfDay.Instance.Year + SDateTime.BaseYear < value.UnlockYear)
		{
			WindowManager.Instance.ShowMessageBox(string.Format("FurnitureUnlock".Loc(), value.UnlockYear), false, DialogWindow.DialogType.Error);
			return;
		}
		SelectorController.Instance.Highligt(false);
		SelectorController.Instance.Selected.Clear();
		bool flag = false;
		bool flag2 = false;
		List<UndoObject.UndoAction> list = new List<UndoObject.UndoAction>();
		for (int i = 0; i < this._furns.Count; i++)
		{
			Furniture furniture = this._furns[i];
			if (!(furniture == null) && !(furniture.gameObject == null))
			{
				if (furniture.WallFurn == value.WallFurn)
				{
					if (!string.IsNullOrEmpty(furniture.UpgradeTo) && value.UpgradeCompatible(furniture.UpgradeTo) && !furniture.name.Equals(value.name))
					{
						float cost = value.Cost;
						if (!GameSettings.Instance.MyCompany.CanMakeTransaction(-cost))
						{
							WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), false, DialogWindow.DialogType.Error);
							break;
						}
						if (this.ValidPlacement(furniture, value) && this.ValidUpgradeBoundary(furniture, value))
						{
							Furniture furniture2 = FurnitureReplaceWindow.UpgradeFurniture(furniture, value, this._furns, i, list);
							furniture2.gameObject.SetActive(true);
							SelectorController.Instance.Selected.Add(furniture2);
							GameSettings.Instance.MyCompany.MakeTransaction(-cost, Company.TransactionCategory.Construction, "Furniture");
							CostDisplay.Instance.Show(cost, furniture2.transform.position);
							CostDisplay.Instance.FloatAway();
							flag = true;
						}
						else
						{
							flag2 = true;
						}
					}
				}
			}
		}
		if (flag)
		{
			GameSettings.Instance.AddUndo(list.ToArray());
			UISoundFX.PlaySFX("PlaceFurniture", -1f, 0f);
			UISoundFX.PlaySFX("Kaching", -1f, 0f);
			SelectorController.Instance.DoPostSelectChecks();
			HUD.Instance.serverWindow.UpdateServerList();
		}
		if (flag2)
		{
			WindowManager.Instance.ShowMessageBox("FurnitureReplacementBlocked".Loc(), false, DialogWindow.DialogType.Information);
		}
		this.Window.Close();
	}

	private bool ValidPlacement(Furniture furn, Furniture upgrade)
	{
		return ((furn.Parent.Outdoors && upgrade.ValidOutdoors) || (!furn.Parent.Outdoors && upgrade.ValidIndoors)) && (upgrade.BasementValid || furn.Parent.Floor >= 0) && (!upgrade.WallFurn || !upgrade.OnlyExteriorWalls || !furn.GetSecondEdge().Links.ContainsValue(furn.FirstEdge));
	}

	private bool DoubleWallFurnCheck(Vector2 pos, Furniture upg, WallEdge e1, WallEdge e2)
	{
		float num = (e1.Pos - pos).magnitude - upg.WallWidth / 2f;
		if (num >= Room.WallOffset / 2f)
		{
			float magnitude = (e1.Pos - e2.Pos).magnitude;
			if (num + upg.WallWidth <= magnitude - Room.WallOffset / 2f)
			{
				return true;
			}
		}
		return false;
	}

	private bool ValidUpgradeBoundary(Furniture furn, Furniture upg)
	{
		Vector3 p = furn.OriginalPosition;
		if (furn.WallFurn)
		{
			Vector2 vector = furn.OriginalPosition.FlattenVector3();
			WallEdge firstEdge = furn.FirstEdge;
			WallEdge secondEdge = furn.GetSecondEdge();
			if (!firstEdge.ValidSegment(ref vector, upg.WallWidth, secondEdge, true, true, upg.ValidOnFence, upg.Height1, upg.Height2, false, 0f, 0f, false, furn))
			{
				return false;
			}
			if (!this.DoubleWallFurnCheck(vector, upg, firstEdge, secondEdge))
			{
				return false;
			}
			p = vector.ToVector3(p.y);
		}
		if (upg.BuildBoundary == null || upg.BuildBoundary.Length == 0)
		{
			return true;
		}
		List<Furniture> list = new List<Furniture>
		{
			furn
		};
		if (furn.IsSnapping)
		{
			list.Add(furn.SnappedTo.Parent);
		}
		foreach (SnapPoint snapPoint in from x in furn.SnapPoints
		where x.UsedBy != null
		select x)
		{
			list.Add(snapPoint.UsedBy);
		}
		return FurnitureBuilder.IsValid((from x in upg.BuildBoundary
		select Matrix4x4.TRS(p, this.transform.rotation, Vector3.one).MultiplyPoint(new Vector3(x.x, 0f, x.y)).FlattenVector3()).ToArray<Vector2>(), upg.Height1, upg.Height2, furn.Parent, upg.WallFurn, list.ToArray());
	}

	private static bool DeleteParent(Furniture furn, Furniture upg)
	{
		return furn.IsSnapping && !upg.IsSnapping && furn.SnappedTo.Parent.SnapPoints.Any((SnapPoint x) => x.UsedBy == furn);
	}

	public static Furniture UpgradeFurniture(Furniture furn, Furniture upgrade)
	{
		return FurnitureReplaceWindow.UpgradeFurniture(furn, upgrade, new List<Furniture>
		{
			furn
		}, 0, null);
	}

	private static Furniture UpgradeFurniture(Furniture furn, Furniture upgrade, List<Furniture> upgrades, int idx, List<UndoObject.UndoAction> undos)
	{
		Vector3 vector = furn.OriginalPosition;
		if (furn.WallFurn)
		{
			Vector2 v = vector.FlattenVector3();
			furn.FirstEdge.ValidSegment(ref v, upgrade.WallWidth, furn.GetSecondEdge(), true, true, upgrade.ValidOnFence, upgrade.Height1, upgrade.Height2, false, 0f, 0f, false, furn);
			vector = v.ToVector3(vector.y);
		}
		WallEdge wallEdge = (!furn.WallFurn) ? null : furn.FirstEdge;
		WallEdge wallEdge2 = (!furn.WallFurn) ? null : furn.GetSecondEdge();
		Quaternion rotation = furn.transform.rotation;
		float rotationOffset = furn.RotationOffset;
		Room parent = furn.Parent;
		SnapPoint snappedTo = furn.SnappedTo;
		Dictionary<int, Furniture> dictionary = (from sp in furn.SnapPoints
		where sp.UsedBy != null
		select sp).ToDictionary((SnapPoint sp) => sp.Id, (SnapPoint sp) => sp.UsedBy);
		for (int i = 0; i < furn.SnapPoints.Length; i++)
		{
			furn.SnapPoints[i].UsedBy = null;
		}
		TableScript component = furn.GetComponent<TableScript>();
		Server component2 = furn.GetComponent<Server>();
		Actor ownedBy = furn.OwnedBy;
		float num = upgrade.Cost;
		WriteDictionary oldFurn = null;
		List<UndoObject.UndoAction> list = (undos != null) ? new List<UndoObject.UndoAction>() : null;
		if (undos != null)
		{
			num -= furn.GetSellPrice();
			oldFurn = furn.SerializeThis(GameReader.LoadMode.Full);
		}
		UnityEngine.Object.Destroy(furn.gameObject);
		if (FurnitureReplaceWindow.DeleteParent(furn, upgrade))
		{
			upgrades.Remove(furn.SnappedTo.Parent);
			for (int j = idx + 1; j < upgrades.Count; j++)
			{
				Furniture upg = upgrades[j];
				if (furn.SnappedTo.Parent.SnapPoints.Any((SnapPoint y) => y.UsedBy == upg))
				{
					upgrades.Remove(upg);
					j--;
				}
			}
			if (undos != null)
			{
				undos.Add(new UndoObject.UndoAction(furn.SnappedTo.Parent, false));
				foreach (Furniture snap in furn.SnappedTo.Parent.IterateSnap(furn))
				{
					undos.Add(new UndoObject.UndoAction(snap, false));
				}
			}
			UnityEngine.Object.Destroy(furn.SnappedTo.Parent.gameObject);
		}
		Furniture furniture = UnityEngine.Object.Instantiate<Furniture>(upgrade);
		Furniture component3 = furniture.GetComponent<Furniture>();
		component3.RotationOffset = rotationOffset;
		if (snappedTo != null)
		{
			if (component3.IsSnapping)
			{
				component3.transform.position = (component3.OriginalPosition = vector);
				component3.transform.rotation = rotation;
				component3.SnappedTo = snappedTo;
				snappedTo.UsedBy = component3;
			}
			else
			{
				component3.transform.position = (component3.OriginalPosition = new Vector3(vector.x, (float)(furn.Parent.Floor * 2), vector.z));
				component3.transform.rotation = rotation;
			}
		}
		else if (component3.WallFurn)
		{
			float magnitude = (wallEdge.Pos - wallEdge2.Pos).magnitude;
			float magnitude2 = (vector.FlattenVector3() - wallEdge.Pos).magnitude;
			component3.transform.position = (component3.OriginalPosition = vector);
			component3.Init(wallEdge, wallEdge2, magnitude2 / magnitude, false);
		}
		else
		{
			component3.transform.position = (component3.OriginalPosition = new Vector3(vector.x, (float)(furn.Parent.Floor * 2), vector.z));
			component3.transform.rotation = rotation;
		}
		component3.Parent = parent;
		Server component4 = component3.GetComponent<Server>();
		if (component2 != null && component4 != null)
		{
			component4.WireTo(component2.Rep, true);
			component4.PreWired = true;
		}
		component3.OwnedBy = ownedBy;
		if (ownedBy != null)
		{
			ownedBy.Owns.Add(component3);
		}
		if (furn.InteractionPoints.Length == component3.InteractionPoints.Length)
		{
			for (int k = 0; k < furn.InteractionPoints.Length; k++)
			{
				if (furn.InteractionPoints[k].UsedBy != null && furn.InteractionPoints[k].Name.Equals(component3.InteractionPoints[k].Name))
				{
					Actor usedBy = furn.InteractionPoints[k].UsedBy;
					usedBy.UsingPoint = component3.InteractionPoints[k];
					component3.InteractionPoints[k].UsedBy = usedBy;
				}
			}
		}
		using (Dictionary<int, Furniture>.Enumerator enumerator2 = dictionary.GetEnumerator())
		{
			while (enumerator2.MoveNext())
			{
				KeyValuePair<int, Furniture> item = enumerator2.Current;
				SnapPoint snapPoint = component3.SnapPoints.FirstOrDefault((SnapPoint sp) => sp.Id == item.Key);
				if (snapPoint == null)
				{
					if (undos != null)
					{
						list.Add(new UndoObject.UndoAction(item.Value, false));
						foreach (Furniture snap2 in item.Value.IterateSnap(null))
						{
							undos.Add(new UndoObject.UndoAction(snap2, false));
						}
					}
					UnityEngine.Object.Destroy(item.Value.gameObject);
				}
				else
				{
					item.Value.SnappedTo = snapPoint;
					Furniture value = item.Value;
					Vector3 position = snapPoint.transform.position;
					item.Value.transform.position = position;
					value.OriginalPosition = position;
					item.Value.transform.rotation = Quaternion.Euler(0f, item.Value.RotationOffset, 0f) * snapPoint.transform.rotation;
					snapPoint.UsedBy = item.Value;
				}
			}
		}
		if (component != null)
		{
			parent.RecalculateTableGroups();
		}
		component3.UpdateFreeNavs(false);
		if (undos != null)
		{
			undos.Add(new UndoObject.UndoAction(oldFurn, component3, num));
			undos.AddRange(list);
		}
		return component3;
	}

	public void Cancel()
	{
		this.Window.Close();
	}

	public GUIWindow Window;

	public BuilderDetailPanel DetailPanel;

	public GameObject BuildButtonPrototype;

	public RectTransform ButtonPanel;

	public ToggleGroup ButtonGroup;

	private readonly Dictionary<Toggle, Furniture> _upgradeTo = new Dictionary<Toggle, Furniture>();

	private readonly List<Furniture> _furns = new List<Furniture>();
}
