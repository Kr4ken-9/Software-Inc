﻿using System;
using UnityEngine;

public class ListenerScript : MonoBehaviour
{
	private void Update()
	{
		if (CameraScript.Instance != null)
		{
			if (CameraScript.Instance.FlyMode)
			{
				base.transform.position = CameraScript.Instance.mainCam.transform.position;
				base.transform.rotation = CameraScript.Instance.mainCam.transform.rotation;
			}
			else
			{
				base.transform.position = new Vector3(base.transform.parent.position.x, base.transform.parent.position.y - CameraScript.Instance.mainCam.transform.localPosition.z / 8f, base.transform.parent.position.z);
				base.transform.localRotation = Quaternion.identity;
			}
		}
	}
}
