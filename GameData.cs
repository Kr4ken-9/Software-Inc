﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using DevConsole;
using UnityEngine;

public static class GameData
{
	static GameData()
	{
		Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
		Debug.Log("Software Inc. " + Versioning.VersionString);
		string text = Path.Combine(Utilities.GetRoot(), "Mods");
		if (!Directory.Exists(text))
		{
			Directory.CreateDirectory(text);
		}
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		GameData.LoadNames(text);
		GameData.LoadStaticData();
		GameData.LoadMods(text);
		GameData.LoadPrefabs();
		GameData.LoadCurrencies(text);
		GameData.LoadStyles();
		GameData.cachedCurrency = GameData.CurrencyRates["Standard"];
		GameData.CacheEffectivenessStats();
		Debug.Log("Data load time: " + (Time.realtimeSinceStartup - realtimeSinceStartup));
		LoadDebugger.AddInfo("Finished loading game data");
	}

	public static bool LoadAnyOnLoad
	{
		get
		{
			return GameData.LoadBuildingOnLoad || GameData.LoadCompanyOnLoad;
		}
		set
		{
			GameData.LoadCompanyOnLoad = value;
			GameData.LoadBuildingOnLoad = value;
		}
	}

	public static void InitRND()
	{
		GameData.RND = new System.Random(GameData.RandomString.GetHashCode());
	}

	public static int RNDRange(int min, int max)
	{
		return GameData.RND.Next(min, max);
	}

	public static float RNDRange(float min, float max)
	{
		return min + (float)GameData.RND.NextDouble() * (max - min);
	}

	public static float RNDValue
	{
		get
		{
			return (float)GameData.RND.NextDouble();
		}
	}

	private static void LoadStyles()
	{
		try
		{
			string path = Path.Combine(Utilities.GetRoot(), "Characters");
			if (Directory.Exists(path))
			{
				foreach (string path2 in Directory.GetFiles(path, "*.xml"))
				{
					XMLParser.XMLNode xmlnode = XMLParser.ParseXML(File.ReadAllText(path2));
					GameData.SavedStyles[Path.GetFileNameWithoutExtension(path2)] = new KeyValuePair<bool, ActorBodyItem.BodyItemObject[]>(xmlnode.Name.Equals("Female"), xmlnode.Children.SelectInPlace((XMLParser.XMLNode x) => new ActorBodyItem.BodyItemObject(x)));
				}
			}
		}
		catch (Exception ex)
		{
		}
	}

	public static void CreateStyle(string name, bool female, ActorBodyItem.BodyItemObject[] items)
	{
		try
		{
			string text = Path.Combine(Utilities.GetRoot(), "Characters");
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			XMLParser.XMLNode xmlnode = new XMLParser.XMLNode((!female) ? "Male" : "Female");
			xmlnode.Children.AddRange(from x in items
			select x.Serialize());
			File.WriteAllText(Path.Combine(text, name + ".xml"), XMLParser.ExportXML(xmlnode));
			GameData.SavedStyles[name] = new KeyValuePair<bool, ActorBodyItem.BodyItemObject[]>(female, items);
		}
		catch (Exception)
		{
		}
	}

	public static void DeleteStyle(string name)
	{
		try
		{
			string path = Path.Combine(Utilities.GetRoot(), "Characters");
			string path2 = Path.Combine(path, name + ".xml");
			if (File.Exists(path2))
			{
				File.Delete(path2);
				GameData.SavedStyles.Remove(name);
			}
		}
		catch (Exception)
		{
		}
	}

	public static IWorkshopItem LoadSteamPrefab(string path, string name)
	{
		string path2 = Path.Combine(path, "Building.xml");
		if (File.Exists(path2))
		{
			try
			{
				BuildingPrefab buildingPrefab = BuildingPrefab.FromXMLNode(XMLParser.ParseXML(File.ReadAllText(path2)));
				buildingPrefab.Root = path;
				buildingPrefab.ItemTitle = Path.GetFileName(path);
				GameData.Prefabs.Add(buildingPrefab);
				return buildingPrefab;
			}
			catch (Exception ex)
			{
				string text = "Error loading blueprint " + name + ":\n" + ex.ToString();
				Debug.Log(text);
				DevConsole.Console.LogError(text);
			}
		}
		return null;
	}

	public static void LoadPrefabs()
	{
		try
		{
			string path = Path.Combine(Utilities.GetRoot(), "Blueprints");
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			foreach (string text in Directory.GetDirectories(path))
			{
				string path2 = Path.Combine(text, "Building.xml");
				if (File.Exists(path2))
				{
					try
					{
						BuildingPrefab buildingPrefab = BuildingPrefab.FromXMLNode(XMLParser.ParseXML(File.ReadAllText(path2)));
						buildingPrefab.Root = text;
						buildingPrefab.ItemTitle = Path.GetFileName(text);
						GameData.Prefabs.Add(buildingPrefab);
					}
					catch (Exception ex)
					{
						GameData.FailedModList.Add(Path.GetFileName(text));
						string text2 = "Error loading blueprint " + Path.GetFileName(text) + ":\n" + ex.ToString();
						DevConsole.Console.LogError(text2);
					}
				}
			}
		}
		catch (Exception exception)
		{
			Debug.LogException(exception);
		}
	}

	public static void LoadGame(SaveGame file, byte[] companyData, bool backup, bool company, bool building, bool editMode)
	{
		GameData.LoadFile = file;
		GameData.CompanyData = companyData;
		GameData.LoadBuildingOnLoad = building;
		GameData.LoadCompanyOnLoad = company;
		GameData.LoadBackup = backup;
		GameData.EditMode = editMode;
		if (GUILoading.Instance != null)
		{
			GUILoading.SetState(true);
		}
	}

	public static void ExportSoftwareToLocalization()
	{
		XMLParser.XMLNode xmlnode = new XMLParser.XMLNode("Software");
		xmlnode.Children.Add(new XMLParser.XMLNode("CategoryDefault", "Default", null));
		foreach (KeyValuePair<string, SoftwareType> keyValuePair in GameData.SoftwareTypes)
		{
			XMLParser.XMLNode xmlnode2 = new XMLParser.XMLNode("Software");
			xmlnode2.Attributes["Name"] = keyValuePair.Key;
			xmlnode2.Children.Add(new XMLParser.XMLNode("Name", keyValuePair.Key, null));
			xmlnode2.Children.Add(new XMLParser.XMLNode("Description", keyValuePair.Value.Description, null));
			XMLParser.XMLNode xmlnode3 = new XMLParser.XMLNode("Features");
			xmlnode2.Children.Add(xmlnode3);
			foreach (KeyValuePair<string, Feature> keyValuePair2 in keyValuePair.Value.Features)
			{
				XMLParser.XMLNode xmlnode4 = new XMLParser.XMLNode("Feature");
				xmlnode4.Attributes["Name"] = keyValuePair2.Key;
				xmlnode4.Children.Add(new XMLParser.XMLNode("Name", keyValuePair2.Key, null));
				if (!string.IsNullOrEmpty(keyValuePair2.Value.Description))
				{
					xmlnode4.Children.Add(new XMLParser.XMLNode("Description", keyValuePair2.Value.Description, null));
				}
				xmlnode3.Children.Add(xmlnode4);
			}
			XMLParser.XMLNode xmlnode5 = new XMLParser.XMLNode("Categories");
			xmlnode2.Children.Add(xmlnode5);
			foreach (KeyValuePair<string, SoftwareCategory> keyValuePair3 in keyValuePair.Value.Categories)
			{
				if (!keyValuePair3.Key.Equals("Default"))
				{
					XMLParser.XMLNode xmlnode6 = new XMLParser.XMLNode("Category", keyValuePair3.Key, new Dictionary<string, string>
					{
						{
							"Name",
							keyValuePair3.Key
						}
					});
					xmlnode6.Children.Add(new XMLParser.XMLNode("Name", keyValuePair3.Key, null));
					if (!string.IsNullOrEmpty(keyValuePair3.Value.Description))
					{
						xmlnode6.Children.Add(new XMLParser.XMLNode("Description", keyValuePair3.Value.Description, null));
					}
					xmlnode5.Children.Add(xmlnode6);
				}
			}
			xmlnode.Children.Add(xmlnode2);
		}
		foreach (Feature feature in GameData.BaseFeatures)
		{
			XMLParser.XMLNode xmlnode7 = new XMLParser.XMLNode("Feature");
			xmlnode7.Attributes["Name"] = feature.Name;
			xmlnode7.Children.Add(new XMLParser.XMLNode("Name", feature.Name, null));
			if (!string.IsNullOrEmpty(feature.Description))
			{
				xmlnode7.Children.Add(new XMLParser.XMLNode("Description", feature.Description, null));
			}
			xmlnode.Children.Add(xmlnode7);
		}
		File.WriteAllText("Software.xml", XMLParser.ExportXML(xmlnode), Encoding.UTF8);
	}

	public static Currency GetCurrency(string name)
	{
		if (GameData.hasCachedCurrency && name.Equals(GameData.cachedCurrencyName))
		{
			return GameData.cachedCurrency;
		}
		Currency result;
		if (GameData.CurrencyRates.TryGetValue(name, out result))
		{
			GameData.cachedCurrencyName = name;
			GameData.cachedCurrency = result;
			GameData.hasCachedCurrency = true;
			return result;
		}
		return GameData.cachedCurrency;
	}

	public static string GenerateName(bool male)
	{
		string str = (!male) ? GameData.FemaleNames[Utilities.GaussRange(0f, 0, GameData.FemaleNames.Length - 1, 0.3f)] : GameData.MaleNames[Utilities.GaussRange(0f, 0, GameData.MaleNames.Length - 1, 0.3f)];
		return str + " " + GameData.LastNames[Utilities.GaussRange(0f, 0, GameData.LastNames.Length - 1, 0.3f)];
	}

	public static IEnumerable<SoftwareType> AllSoftwareTypes(ModPackage[] mods = null)
	{
		ModPackage[] array;
		if ((array = mods) == null)
		{
			array = (from x in GameData.ModPackages
			where x.Enabled
			select x).ToArray<ModPackage>();
		}
		mods = array;
		Dictionary<string, SoftwareType> dictionary = GameData.SoftwareTypes.ToDictionary((KeyValuePair<string, SoftwareType> x) => x.Key, (KeyValuePair<string, SoftwareType> x) => x.Value);
		List<string> list = mods.SelectMany((ModPackage x) => from z in x.SoftwareTypeOverrides
		where z.Value.Delete
		select z.Key).Distinct<string>().ToList<string>();
		list.RemoveAll((string x) => "Operating System".Equals(x));
		foreach (string key in list)
		{
			dictionary.Remove(key);
		}
		foreach (ModPackage modPackage in mods)
		{
			foreach (KeyValuePair<string, SoftwareType> keyValuePair in modPackage.SoftwareTypes)
			{
				if (!list.Contains(keyValuePair.Key))
				{
					dictionary[keyValuePair.Key] = keyValuePair.Value;
				}
			}
		}
		IEnumerable<IGrouping<string, SoftwareTypeOverride>> source = from x in mods.SelectMany((ModPackage x) => x.SoftwareTypeOverrides.Values)
		group x by x.Name;
		Func<IGrouping<string, SoftwareTypeOverride>, string> keySelector = (IGrouping<string, SoftwareTypeOverride> x) => x.Key;
		if (GameData.<>f__mg$cache0 == null)
		{
			GameData.<>f__mg$cache0 = new Func<IGrouping<string, SoftwareTypeOverride>, SoftwareTypeOverride[]>(Enumerable.ToArray<SoftwareTypeOverride>);
		}
		Dictionary<string, SoftwareTypeOverride[]> dictionary2 = source.ToDictionary(keySelector, GameData.<>f__mg$cache0);
		Feature[] array3;
		if (mods.Any((ModPackage x) => x.BaseFeatureOverride))
		{
			array3 = (from x in mods
			where x.BaseFeatureOverride
			select x).SelectMany((ModPackage x) => x.BaseFeatures).ToArray<Feature>();
		}
		else
		{
			array3 = GameData.BaseFeatures.Concat(mods.SelectMany((ModPackage x) => x.BaseFeatures)).ToArray<Feature>();
		}
		Feature[] @base = array3;
		foreach (KeyValuePair<string, SoftwareType> keyValuePair2 in dictionary.ToList<KeyValuePair<string, SoftwareType>>())
		{
			if (dictionary2.ContainsKey(keyValuePair2.Key))
			{
				dictionary[keyValuePair2.Key] = keyValuePair2.Value.Clone(@base, true, dictionary2[keyValuePair2.Key]);
			}
			else
			{
				dictionary[keyValuePair2.Key] = keyValuePair2.Value.Clone(@base, false, new SoftwareTypeOverride[0]);
			}
		}
		return dictionary.Values;
	}

	public static string[][] GetSpecialization(SoftwareType[] sw)
	{
		string[][] array = new string[3][];
		array[0] = (from x in sw.SelectMany((SoftwareType x) => from z in x.Features
		select z.Value.GetCategory(true)).Distinct<string>()
		where x != null
		select x).ToArray<string>();
		array[1] = (from x in sw.SelectMany((SoftwareType x) => from z in x.Features
		where z.Value.CodeArtRatio > 0f
		select z.Value.GetCategory(true)).Distinct<string>()
		where x != null
		select x).ToArray<string>();
		array[2] = (from x in sw.SelectMany((SoftwareType x) => from z in x.Features
		where z.Value.CodeArtRatio < 1f
		select z.Value.GetCategory(false)).Distinct<string>()
		where x != null
		select x).ToArray<string>();
		return array;
	}

	public static string[][] GetUnlockedSpecializations(int year)
	{
		string[][] array = new string[3][];
		SoftwareType[] sw = GameData.AllSoftwareTypes(null).ToArray<SoftwareType>();
		Dictionary<string, SoftwareType> sw2 = sw.ToDictionary((SoftwareType x) => x.Name);
		string[][] specialization = GameData.GetSpecialization(sw);
		array[2] = (from x in specialization[2]
		where (from z in sw
		where z.IsUnlocked(year)
		select z).Any((SoftwareType y) => y.Features.Any((KeyValuePair<string, Feature> z) => x.Equals(z.Value.GetCategory(false)) && z.Value.IsUnlocked(null, year, sw2)))
		select x).ToArray<string>();
		array[1] = (from x in specialization[1]
		where (from z in sw
		where z.IsUnlocked(year)
		select z).Any((SoftwareType y) => y.Features.Any((KeyValuePair<string, Feature> z) => x.Equals(z.Value.GetCategory(true)) && z.Value.IsUnlocked(null, year, sw2)))
		select x).ToArray<string>();
		array[0] = (from x in specialization[0]
		where (from z in sw
		where z.IsUnlocked(year)
		select z).Any((SoftwareType y) => y.Features.Any((KeyValuePair<string, Feature> z) => x.Equals(z.Value.GetCategory(true)) && z.Value.IsUnlocked(null, year, sw2)))
		select x).ToArray<string>();
		return array;
	}

	public static string[] GetUnlockedSpecializations(Employee.EmployeeRole role)
	{
		SoftwareType[] sw = GameData.AllSoftwareTypes(null).ToArray<SoftwareType>();
		Dictionary<string, SoftwareType> sw2 = sw.ToDictionary((SoftwareType x) => x.Name);
		string[][] specialization = GameData.GetSpecialization(sw);
		int year = GameData.ActiveYear;
		if (role == Employee.EmployeeRole.Artist)
		{
			return (from x in specialization[2]
			where (from z in sw
			where z.IsUnlocked(year)
			select z).Any((SoftwareType y) => y.Features.Any((KeyValuePair<string, Feature> z) => x.Equals(z.Value.GetCategory(false)) && z.Value.IsUnlocked(null, year, sw2)))
			select x).ToArray<string>();
		}
		if (role == Employee.EmployeeRole.Programmer)
		{
			return (from x in specialization[1]
			where (from z in sw
			where z.IsUnlocked(year)
			select z).Any((SoftwareType y) => y.Features.Any((KeyValuePair<string, Feature> z) => x.Equals(z.Value.GetCategory(true)) && z.Value.IsUnlocked(null, year, sw2)))
			select x).ToArray<string>();
		}
		return (from x in specialization[0]
		where (from z in sw
		where z.IsUnlocked(year)
		select z).Any((SoftwareType y) => y.Features.Any((KeyValuePair<string, Feature> z) => x.Equals(z.Value.GetCategory(true)) && z.Value.IsUnlocked(null, year, sw2)))
		select x).ToArray<string>();
	}

	public static IEnumerable<KeyValuePair<string, RandomNameGenerator>> AllNameGenerators(ModPackage[] mods = null)
	{
		ModPackage[] array;
		if ((array = mods) == null)
		{
			array = (from mod in GameData.ModPackages
			where mod.Enabled
			select mod).ToArray<ModPackage>();
		}
		mods = array;
		IEnumerable<KeyValuePair<string, RandomNameGenerator>> enumerable = GameData.NameGens.AsEnumerable<KeyValuePair<string, RandomNameGenerator>>();
		foreach (ModPackage modPackage in mods)
		{
			enumerable = enumerable.Concat(modPackage.NameGenerators);
		}
		return enumerable;
	}

	public static RandomNameGenerator GetStaticNameGenerator(string name)
	{
		return GameData.NameGens[name];
	}

	public static Dictionary<string, RandomNameGenerator> MergeGenerators(IEnumerable<KeyValuePair<string, RandomNameGenerator>> generators)
	{
		Dictionary<string, RandomNameGenerator> dictionary = new Dictionary<string, RandomNameGenerator>();
		IEnumerable<string> enumerable = (from x in generators
		select x.Key).Distinct<string>();
		using (IEnumerator<string> enumerator = enumerable.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				string key = enumerator.Current;
				RandomNameGenerator[] array = (from x in generators
				where x.Key.Equals(key)
				select x.Value).ToArray<RandomNameGenerator>();
				RandomNameGenerator randomNameGenerator = array.FirstOrDefault((RandomNameGenerator x) => x.Replace);
				if (randomNameGenerator != null)
				{
					dictionary.Add(key, randomNameGenerator);
				}
				else
				{
					RandomNameGenerator randomNameGenerator2 = array[0].Clone();
					for (int i = 1; i < array.Length; i++)
					{
						randomNameGenerator2.MergeWith(array[i]);
					}
					dictionary.Add(key, randomNameGenerator2);
				}
			}
		}
		return dictionary;
	}

	public static PersonalityGraph AllPersonalities()
	{
		List<ModPackage> list = (from x in GameData.ModPackages
		where x.Enabled && x.Personalities != null
		select x).ToList<ModPackage>();
		ModPackage modPackage = list.FirstOrDefault((ModPackage x) => x.Personalities.Replace && x.Personalities.PersonalityTraits.Count > 1);
		PersonalityGraph personalityGraph = (modPackage != null) ? modPackage.Personalities.Clone() : GameData.Personalities.Clone();
		for (int i = 0; i < list.Count; i++)
		{
			ModPackage modPackage2 = list[i];
			if (modPackage2 != modPackage)
			{
				personalityGraph.MergeWith(modPackage2.Personalities);
			}
		}
		return personalityGraph;
	}

	public static IEnumerable<CompanyType> AllCompanyTypes()
	{
		return GameData.AllCompanyTypes((from mod in GameData.ModPackages
		where mod.Enabled
		select mod).ToArray<ModPackage>());
	}

	public static IEnumerable<CompanyType> AllCompanyTypes(params ModPackage[] mods)
	{
		Dictionary<string, CompanyType> dictionary = GameData.CompanyTypes.ToDictionary((KeyValuePair<string, CompanyType> x) => x.Key, (KeyValuePair<string, CompanyType> x) => x.Value);
		HashSet<string> hashSet = new HashSet<string>();
		foreach (ModPackage modPackage in mods)
		{
			foreach (KeyValuePair<string, CompanyType> keyValuePair in modPackage.CompanyTypes)
			{
				dictionary[keyValuePair.Key] = keyValuePair.Value;
			}
			hashSet.AddRange(modPackage.DeleteCompanyTypes);
		}
		foreach (string key in hashSet)
		{
			dictionary.Remove(key);
		}
		return dictionary.Values;
	}

	public static IEnumerable<string> SoftwareTypeNames()
	{
		IEnumerable<string> enumerable = GameData.SoftwareTypes.Keys.AsEnumerable<string>();
		foreach (ModPackage modPackage in from mod in GameData.ModPackages
		where mod.Enabled
		select mod)
		{
			enumerable = enumerable.Concat(modPackage.SoftwareTypes.Keys);
		}
		return enumerable;
	}

	public static SoftwareType GetSoftwareType(string name)
	{
		SoftwareType result = null;
		foreach (ModPackage modPackage in from mod in GameData.ModPackages
		where mod.Enabled
		select mod)
		{
			if (modPackage.SoftwareTypes.TryGetValue(name, out result))
			{
				return result;
			}
		}
		if (GameData.SoftwareTypes.TryGetValue(name, out result))
		{
			return result;
		}
		throw new UnityException("Failed locating software type: " + name);
	}

	public static CompanyType GetCompanyType(string name)
	{
		CompanyType result = null;
		foreach (ModPackage modPackage in from mod in GameData.ModPackages
		where mod.Enabled
		select mod)
		{
			if (modPackage.CompanyTypes.TryGetValue(name, out result))
			{
				return result;
			}
		}
		if (GameData.CompanyTypes.TryGetValue(name, out result))
		{
			return result;
		}
		throw new UnityException("Failed locating company type: " + name);
	}

	public static EventCompany[] GetEvents(string name)
	{
		EventCompany[] result = null;
		if (GameData.Events.TryGetValue(name, out result))
		{
			return result;
		}
		throw new UnityException("Failed locating events: " + name);
	}

	public static RandomNameGenerator GetRandomNameGenerators(string name)
	{
		RandomNameGenerator result = null;
		foreach (ModPackage modPackage in from mod in GameData.ModPackages
		where mod.Enabled
		select mod)
		{
			if (modPackage.NameGenerators.TryGetValue(name, out result))
			{
				return result;
			}
		}
		if (GameData.NameGens.TryGetValue(name, out result))
		{
			return result;
		}
		throw new UnityException("Failed locating random name generator: " + name);
	}

	public static Dictionary<string, RandomNameGenerator> GetRandomNameGenerators()
	{
		Dictionary<string, RandomNameGenerator> dictionary = new Dictionary<string, RandomNameGenerator>(GameData.NameGens);
		foreach (ModPackage modPackage in from mod in GameData.ModPackages
		where mod.Enabled
		select mod)
		{
			foreach (KeyValuePair<string, RandomNameGenerator> keyValuePair in modPackage.NameGenerators)
			{
				dictionary[keyValuePair.Key] = keyValuePair.Value;
			}
		}
		return dictionary;
	}

	public static Dictionary<string, RandomNameGenerator> GetRandomNameGenerators(Dictionary<string, RandomNameGenerator> include)
	{
		Dictionary<string, RandomNameGenerator> dictionary = new Dictionary<string, RandomNameGenerator>(GameData.NameGens);
		foreach (KeyValuePair<string, RandomNameGenerator> keyValuePair in include)
		{
			dictionary[keyValuePair.Key] = keyValuePair.Value;
		}
		return dictionary;
	}

	private static void LoadNames(string modDir)
	{
		GameData.FemaleNames = GameData.LoadEmployeeNames(modDir, "femalefirstnames");
		GameData.MaleNames = GameData.LoadEmployeeNames(modDir, "malefirstnames");
		GameData.LastNames = GameData.LoadEmployeeNames(modDir, "lastnames");
	}

	private static string[] LoadEmployeeNames(string modDir, string assetName)
	{
		string[] result;
		try
		{
			string path = Path.Combine(modDir, assetName + ".txt");
			if (!File.Exists(path))
			{
				string contents = GameData.LoadFullTextAsset(assetName);
				File.WriteAllText(path, contents);
			}
			result = File.ReadAllLines(path);
		}
		catch (Exception)
		{
			result = GameData.LoadTextAsset(assetName);
		}
		return result;
	}

	public static void ReloadSoftware()
	{
		GameData.NameGens.Clear();
		GameData.SoftwareTypes.Clear();
		foreach (KeyValuePair<string, string[]> keyValuePair in GameData.LoadAllTextAssetsWithName("NameGenerators", true))
		{
			GameData.NameGens.Add(keyValuePair.Key, RandomNameGenerator.Load(keyValuePair.Value));
		}
		GameData.BaseFeatures = XMLParser.ParseXML(GameData.LoadFullTextAsset("SoftwareTypes/base")).GetNodes("Feature", false).SelectInPlace((XMLParser.XMLNode x) => new Feature(x, null, true));
		foreach (string text in GameData.LoadAllTextAssetsNoSplit("SoftwareTypes", "base"))
		{
			SoftwareType softwareType = new SoftwareType(XMLParser.ParseXML(text), false);
			GameData.SoftwareTypes.Add(softwareType.Name, softwareType);
		}
	}

	private static void LoadStaticData()
	{
		Debug.Log("Loading Random Name Generators");
		foreach (KeyValuePair<string, string[]> keyValuePair in GameData.LoadAllTextAssetsWithName("NameGenerators", true))
		{
			GameData.NameGens.Add(keyValuePair.Key, RandomNameGenerator.Load(keyValuePair.Value));
		}
		Debug.Log("Loading Sentence Generators");
		foreach (KeyValuePair<string, string[]> keyValuePair2 in GameData.LoadAllTextAssetsWithName("SentenceGenerator", true))
		{
			SentenceGenerator value = new SentenceGenerator(keyValuePair2.Value);
			GameData.SentenceGen[keyValuePair2.Key] = value;
		}
		Debug.Log("Loading mood effects");
		GameData.LoadMoodEffects();
		Debug.Log("Loading Software Types");
		GameData.BaseFeatures = XMLParser.ParseXML(GameData.LoadFullTextAsset("SoftwareTypes/base")).GetNodes("Feature", false).SelectInPlace((XMLParser.XMLNode x) => new Feature(x, null, true));
		foreach (string text in GameData.LoadAllTextAssetsNoSplit("SoftwareTypes", "base"))
		{
			SoftwareType softwareType = new SoftwareType(XMLParser.ParseXML(text), false);
			GameData.SoftwareTypes.Add(softwareType.Name, softwareType);
		}
		Debug.Log("Loading Company Types");
		foreach (string text2 in GameData.LoadAllTextAssetsNoSplit("CompanyTypes", string.Empty))
		{
			CompanyType companyType = new CompanyType(XMLParser.ParseXML(text2));
			GameData.CompanyTypes.Add(companyType.Name, companyType);
		}
		Debug.Log("Loading Companies");
		foreach (string text3 in GameData.LoadAllTextAssetsNoSplit("Companies", string.Empty))
		{
			EventCompany eventCompany = EventCompany.Load(XMLParser.ParseXML(text3));
			GameData.EventCompanies.Add(eventCompany.Name, eventCompany);
		}
		Debug.Log("Loading Events");
		foreach (string text4 in GameData.LoadAllTextAssetsNoSplit("Events", string.Empty))
		{
			KeyValuePair<string, EventCompany[]> keyValuePair3 = GameData.LoadEvents(XMLParser.ParseXML(text4), GameData.EventCompanies);
			GameData.Events.Add(keyValuePair3.Key, keyValuePair3.Value);
		}
		GameData.Personalities = new PersonalityGraph(XMLParser.ParseXML(GameData.LoadFullTextAsset("Personalities")));
	}

	public static void LoadMoodEffects()
	{
		GameData.MoodEffects = (from x in XMLParser.ParseXML(GameData.LoadFullTextAsset("MoodEffects")).Children
		select new MoodEffect(x)).ToDictionary((MoodEffect x) => x.Thought, (MoodEffect x) => x);
	}

	public static IWorkshopItem LoadSteamMod(string path, string name)
	{
		try
		{
			ModPackage modPackage = ModPackage.Load(path);
			GameData.ModPackages.Add(modPackage);
			return modPackage;
		}
		catch (Exception ex)
		{
			string text = "Error loading mod " + name + ":\n" + ex.ToString();
			Debug.Log(text);
			DevConsole.Console.LogError(text);
		}
		return null;
	}

	private static void LoadMods(string modFolder)
	{
		string[] directories = Directory.GetDirectories(modFolder);
		foreach (string text in directories)
		{
			try
			{
				GameData.ModPackages.Add(ModPackage.Load(text));
			}
			catch (Exception ex)
			{
				GameData.FailedModList.Add(Path.GetFileName(text));
				LoadDebugger.AddError("Failed loading mod: " + Path.GetFileName(text));
				if (DevConsole.Console.Singleton != null)
				{
					DevConsole.Console.LogError("Error loading mod " + Path.GetFileName(text) + ":\n" + ex.Message);
				}
			}
		}
	}

	public static KeyValuePair<string, EventCompany[]> LoadEvents(XMLParser.XMLNode node, Dictionary<string, EventCompany> companies)
	{
		return new KeyValuePair<string, EventCompany[]>(node.GetNodes("Name", true)[0].Value, node.GetNodes("Companies", true)[0].GetNodes("Company", true).SelectInPlace((XMLParser.XMLNode x) => companies[x.Value]));
	}

	private static void LoadCurrencies(string modFolder)
	{
		string text = null;
		try
		{
			string path = Path.Combine(modFolder, "Currencies.xml");
			if (!File.Exists(path))
			{
				string contents = GameData.LoadFullTextAsset("Currencies");
				File.WriteAllText(path, contents);
			}
			text = File.ReadAllText(path);
		}
		catch (Exception)
		{
			text = GameData.LoadFullTextAsset("Currencies");
		}
		XMLParser.XMLNode xmlnode = XMLParser.ParseXML(text);
		foreach (XMLParser.XMLNode xmlnode2 in xmlnode.GetNodes("Currency", true))
		{
			XMLParser.XMLNode node = xmlnode2.GetNode("Name", false);
			if (node != null)
			{
				GameData.CurrencyRates[node.Value] = new Currency(xmlnode2);
			}
		}
	}

	public static string[][] LoadAllTextAssets(string path)
	{
		TextAsset[] arr = Resources.LoadAll<TextAsset>(path);
		return arr.SelectInPlace((TextAsset x) => x.text.Split(new string[]
		{
			"\r\n",
			System.Environment.NewLine,
			"\n"
		}, StringSplitOptions.RemoveEmptyEntries));
	}

	public static string[] LoadAllTextAssetsNoSplit(string path, string exclude = "")
	{
		TextAsset[] source = Resources.LoadAll<TextAsset>(path);
		return (from x in source
		where !x.name.ToLower().Equals(exclude)
		select x.text).ToArray<string>();
	}

	public static Dictionary<string, string> LoadAllTextAssetsWithNameNoSplit(string path)
	{
		TextAsset[] source = Resources.LoadAll<TextAsset>(path);
		return source.ToDictionary((TextAsset x) => x.name, (TextAsset x) => x.text);
	}

	public static Dictionary<string, string[]> LoadAllTextAssetsWithName(string path, bool removeEmpty = true)
	{
		TextAsset[] source = Resources.LoadAll<TextAsset>(path);
		return source.ToDictionary((TextAsset x) => x.name, (TextAsset x) => x.text.Split(new string[]
		{
			"\r\n",
			System.Environment.NewLine,
			"\n"
		}, (!removeEmpty) ? StringSplitOptions.None : StringSplitOptions.RemoveEmptyEntries));
	}

	public static string LoadFullTextAsset(string assetName)
	{
		TextAsset textAsset = Resources.Load(assetName) as TextAsset;
		return textAsset.text;
	}

	public static string[] LoadTextAsset(string assetName)
	{
		return GameData.LoadFullTextAsset(assetName).Split(new string[]
		{
			"\r\n",
			System.Environment.NewLine,
			"\n"
		}, StringSplitOptions.RemoveEmptyEntries);
	}

	private static bool RecursiveCheckValid(Dictionary<string, SoftwareType> types, Feature feat, HashSet<Feature> visited, Feature start, List<string> path, bool first = true)
	{
		if (!first && feat == start)
		{
			path.Add(feat.Name);
			return false;
		}
		if (visited.Contains(feat))
		{
			return true;
		}
		visited.Add(feat);
		foreach (KeyValuePair<string, string> keyValuePair in feat.Dependencies)
		{
			if (!GameData.RecursiveCheckValid(types, types[keyValuePair.Key].Features[keyValuePair.Value], visited, start, path, false))
			{
				path.Add(feat.Name);
				return false;
			}
		}
		return true;
	}

	private static bool RecursiveFromCheckValid(SoftwareType sw, Feature feat, HashSet<Feature> visited, Feature start, List<string> path, bool first = true)
	{
		if (!first && start == feat)
		{
			path.Add(feat.Name);
			return false;
		}
		if (visited.Contains(feat))
		{
			return true;
		}
		visited.Add(feat);
		foreach (KeyValuePair<string, string> keyValuePair in feat.Dependencies)
		{
			if (keyValuePair.Key.Equals(sw.Name) && !GameData.RecursiveFromCheckValid(sw, sw.Features[keyValuePair.Value], visited, start, path, false))
			{
				path.Add(feat.Name);
				return false;
			}
		}
		if (feat.From != null && !GameData.RecursiveFromCheckValid(sw, sw.Features[feat.From], visited, start, path, false))
		{
			path.Add(feat.Name);
			return false;
		}
		return true;
	}

	public static string CheckForErrors(SoftwareType[] types)
	{
		string result;
		try
		{
			SoftwareType softwareType = types.First((SoftwareType x) => x.Name.Equals("Operating System"));
			foreach (SoftwareType softwareType2 in types)
			{
				if (!softwareType2.Features.Any((KeyValuePair<string, Feature> x) => x.Value.Forced))
				{
					return softwareType2.Name + " does not have any forced features. All software types should have at least one forced feature";
				}
				if (softwareType2.OSLimit != null && !softwareType.Categories.ContainsKey(softwareType2.OSLimit))
				{
					return softwareType2.Name + " has been limited to the operating system category: " + softwareType2.OSLimit + ", which does not exist";
				}
			}
			Dictionary<string, SoftwareType> dictionary = types.ToDictionary((SoftwareType x) => x.Name, (SoftwareType x) => x);
			HashSet<Feature> hashSet = new HashSet<Feature>();
			List<string> list = new List<string>();
			Dictionary<string, SoftwareType> types2 = types.ToDictionary((SoftwareType x) => x.Name, (SoftwareType x) => x);
			using (Dictionary<string, SoftwareType>.ValueCollection.Enumerator enumerator = dictionary.Values.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					SoftwareType sw = enumerator.Current;
					foreach (SoftwareCategory softwareCategory in sw.Categories.Values)
					{
						if (Mathf.Approximately(softwareCategory.TimeScale, 0f))
						{
							return string.Concat(new string[]
							{
								"Category: ",
								softwareCategory.Name,
								" in software ",
								sw.Name,
								" has zero timescale"
							});
						}
					}
					HashSet<string> hashSet2 = new HashSet<string>();
					foreach (Feature feature in sw.Features.Values)
					{
						if (Mathf.Approximately(feature.DevTime, 0f))
						{
							return string.Concat(new string[]
							{
								"Feature: ",
								feature.Name,
								" in software ",
								sw.Name,
								" has zero devtime"
							});
						}
						if (Mathf.Approximately(feature.Innovation, 0f) && Mathf.Approximately(feature.Stability, 0f) && Mathf.Approximately(feature.Usability, 0f))
						{
							return string.Concat(new string[]
							{
								"Feature: ",
								feature.Name,
								" in software ",
								sw.Name,
								" needs a non-zero value for innovation, stability or usability"
							});
						}
						if (feature.Research != null && feature.Unlock == null)
						{
							return string.Concat(new string[]
							{
								"Feature: ",
								feature.Name,
								" in software ",
								sw.Name,
								" is researchable but has no unlock date"
							});
						}
						if (feature.From != null)
						{
							if (!sw.Features.ContainsKey(feature.From))
							{
								return string.Concat(new string[]
								{
									"Feature from: ",
									feature.From,
									" for ",
									feature.Name,
									" in software ",
									sw.Name,
									" does not exist"
								});
							}
							Feature feature2 = sw.Features[feature.From];
							if (feature2.Research != null)
							{
								return string.Concat(new string[]
								{
									"Feature from: ",
									feature.From,
									" for ",
									feature.Name,
									" in software ",
									sw.Name,
									" is researchable and cannot be used as a parent"
								});
							}
							if (feature2.Software.Equals(sw.Name) && feature2.SoftwareCategories != null && feature2.SoftwareCategories.Count > 0)
							{
								HashSet<string> hashSet3 = sw.Categories.Keys.ToHashSet<string>();
								foreach (string item in feature2.SoftwareCategories.Keys)
								{
									hashSet3.Remove(item);
								}
								if (hashSet3.Count > 0)
								{
									bool flag = feature.SoftwareCategories != null;
									if (flag)
									{
										hashSet3 = feature.SoftwareCategories.Keys.ToHashSet<string>();
										foreach (string item2 in feature2.SoftwareCategories.Keys)
										{
											hashSet3.Remove(item2);
										}
										flag = (hashSet3.Count == 0);
									}
									if (!flag)
									{
										return string.Concat(new string[]
										{
											"Feature from: ",
											feature.From,
											" for ",
											feature.Name,
											" in software ",
											sw.Name,
											" has incompatible software category dependencies"
										});
									}
								}
							}
							if (hashSet2.Contains(feature.From))
							{
								return string.Concat(new string[]
								{
									"Two features can not inherit FROM the same feature: ",
									feature.From,
									" in software ",
									sw.Name,
									", use a feature dependency instead"
								});
							}
							hashSet2.Add(feature.From);
						}
						if (feature.SoftwareCategories != null)
						{
							string text = (from x in feature.SoftwareCategories
							where !sw.Categories.ContainsKey(x.Key)
							select x.Key).FirstOrDefault<string>();
							if (text != null)
							{
								return string.Concat(new string[]
								{
									"Feature: ",
									feature.Name,
									" in software ",
									sw.Name,
									" is dependant on non-existing software category ",
									text
								});
							}
						}
						foreach (KeyValuePair<string, string> keyValuePair in feature.Dependencies)
						{
							string str = sw.Name + " -> " + feature.Name;
							string str2 = keyValuePair.Key + " -> " + keyValuePair.Value;
							string str3 = "Found dependency from " + str + " to " + str2;
							if (keyValuePair.Key.Equals("Operating System") && !sw.Name.Equals("Operating System") && !sw.OSSpecific)
							{
								return str3 + ", but " + sw.Name + " is not operating system dependant";
							}
							if (!dictionary.ContainsKey(keyValuePair.Key))
							{
								return str3 + ", but " + keyValuePair.Key + " does not exist";
							}
							if (!dictionary[keyValuePair.Key].Features.ContainsKey(keyValuePair.Value))
							{
								return str3 + ", but " + keyValuePair.Value + " does not exist";
							}
							Feature feature3 = dictionary[keyValuePair.Key].Features[keyValuePair.Value];
							if (keyValuePair.Key.Equals(sw.Name) && feature3.Research != null)
							{
								return str3 + ", but a feature cannot depend on another feature that is researchable from the same software type";
							}
							if (feature3.Software.Equals(sw.Name) && feature3.SoftwareCategories != null && feature3.SoftwareCategories.Count > 0)
							{
								HashSet<string> hashSet4 = sw.Categories.Keys.ToHashSet<string>();
								foreach (string item3 in feature3.SoftwareCategories.Keys)
								{
									hashSet4.Remove(item3);
								}
								if (hashSet4.Count > 0)
								{
									bool flag2 = feature.SoftwareCategories != null;
									if (flag2)
									{
										hashSet4 = feature.SoftwareCategories.Keys.ToHashSet<string>();
										foreach (string item4 in feature3.SoftwareCategories.Keys)
										{
											hashSet4.Remove(item4);
										}
										flag2 = (hashSet4.Count == 0);
									}
									if (!flag2)
									{
										return str3 + ", but their software category dependencies do not match";
									}
								}
							}
						}
					}
				}
			}
			foreach (SoftwareType softwareType3 in types)
			{
				foreach (Feature feature4 in softwareType3.Features.Values)
				{
					hashSet.Clear();
					list.Clear();
					if (!GameData.RecursiveCheckValid(types2, feature4, hashSet, feature4, list, true))
					{
						return "Found recursive dependency tree in software " + softwareType3.Name + " with path: " + string.Join(" -> ", list.ToArray());
					}
					hashSet.Clear();
					list.Clear();
					if (!GameData.RecursiveFromCheckValid(softwareType3, feature4, hashSet, feature4, list, true))
					{
						return "Found recursive FROM dependency in software " + softwareType3.Name + " with path: " + string.Join(" -> ", list.ToArray());
					}
				}
			}
			result = null;
		}
		catch (Exception ex)
		{
			result = "Error checking for software type errors: " + ex.ToString();
		}
		return result;
	}

	public static float GetProjectEffectiveness(int employees, int devTime)
	{
		if (employees == 0 || devTime == 0)
		{
			return 1f;
		}
		if (employees - 1 < GameData.CachedEffectiveness.GetLength(1) && devTime - 1 < GameData.CachedEffectiveness.GetLength(0))
		{
			return GameData.CachedEffectiveness[devTime - 1, employees - 1];
		}
		return GameData.CalculateProjectEffectiveness(employees, devTime);
	}

	public static float ProjectDevTime(int designEmployees, int devEmployees, float devTime, float artRatio)
	{
		float projectEffectiveness = GameData.GetProjectEffectiveness(designEmployees, Mathf.Max(1, Mathf.RoundToInt(devTime * SoftwareType.DesignRatio * artRatio)));
		float projectEffectiveness2 = GameData.GetProjectEffectiveness(devEmployees, Mathf.Max(1, Mathf.RoundToInt(devTime * (1f - SoftwareType.DesignRatio))));
		return devTime * SoftwareType.DesignRatio / (projectEffectiveness * (float)designEmployees) + devTime * (1f - SoftwareType.DesignRatio) / (projectEffectiveness2 * (float)devEmployees);
	}

	public static float ProjectDevTime(int designEmployees, int devEmployees, int actualDesignEmployees, int actualDevEmployees, float devTime, float artRatio)
	{
		float projectEffectiveness = GameData.GetProjectEffectiveness(designEmployees, Mathf.Max(1, Mathf.RoundToInt(devTime * SoftwareType.DesignRatio * artRatio)));
		float projectEffectiveness2 = GameData.GetProjectEffectiveness(devEmployees, Mathf.Max(1, Mathf.RoundToInt(devTime * (1f - SoftwareType.DesignRatio))));
		return devTime * SoftwareType.DesignRatio / (projectEffectiveness * (float)actualDesignEmployees) + devTime * (1f - SoftwareType.DesignRatio) / (projectEffectiveness2 * (float)actualDevEmployees);
	}

	public static int GetOptimalEmployees(int devTime)
	{
		if (devTime == 0)
		{
			return 0;
		}
		if (devTime - 1 < GameData.OptimalEmployees.Length)
		{
			return GameData.OptimalEmployees[devTime - 1];
		}
		return GameData.CalculateOptimalEmployee(devTime);
	}

	private static void CacheEffectivenessStats()
	{
		GameData.OptimalEmployees = new int[GameData.CachedEffectiveness.GetLength(0)];
		for (int i = 0; i < GameData.CachedEffectiveness.GetLength(0); i++)
		{
			GameData.OptimalEmployees[i] = GameData.CalculateOptimalEmployee(i + 1);
			for (int j = 0; j < GameData.CachedEffectiveness.GetLength(1); j++)
			{
				GameData.CachedEffectiveness[i, j] = GameData.CalculateProjectEffectiveness(j + 1, i + 1);
			}
		}
	}

	private static int CalculateOptimalEmployee(int devTime)
	{
		return Mathf.RoundToInt((float)devTime / 4f + 1f);
	}

	private static float CalculateProjectEffectiveness(int employees, int devTime)
	{
		if (employees == 0)
		{
			return 0f;
		}
		return 1f / (float)employees * (1f + (1f - 1f / Mathf.Pow((float)employees / 20f + 0.95f, 100f / (float)devTime)) * Mathf.Pow((float)devTime, 0.15f));
	}

	public static List<string> FailedModList = new List<string>();

	public static string[] DifficultySettings = new string[]
	{
		"Easy",
		"Medium",
		"Hard"
	};

	private static string[] FemaleNames;

	private static string[] MaleNames;

	private static string[] LastNames;

	private static Dictionary<string, SoftwareType> SoftwareTypes = new Dictionary<string, SoftwareType>();

	private static Feature[] BaseFeatures;

	private static Dictionary<string, CompanyType> CompanyTypes = new Dictionary<string, CompanyType>();

	private static Dictionary<string, RandomNameGenerator> NameGens = new Dictionary<string, RandomNameGenerator>();

	public static Dictionary<string, Currency> CurrencyRates = new Dictionary<string, Currency>
	{
		{
			"Standard",
			new Currency("Standard", "$", string.Empty, 1f)
		}
	};

	public static List<ModPackage> ModPackages = new List<ModPackage>();

	private static Currency cachedCurrency;

	private static bool hasCachedCurrency = false;

	private static string cachedCurrencyName;

	public static SaveGame LoadFile;

	public static byte[] CompanyData;

	public static bool LoadBackup = false;

	public static string CompanyName = "MyCompany";

	public static bool LoadBuildingOnLoad = false;

	public static bool LoadCompanyOnLoad = false;

	public static bool EditMode = false;

	public static int StartingMoney = 10000000;

	[NonSerialized]
	private static Dictionary<string, EventCompany> EventCompanies = new Dictionary<string, EventCompany>();

	[NonSerialized]
	private static Dictionary<string, EventCompany[]> Events = new Dictionary<string, EventCompany[]>();

	public static Dictionary<string, SentenceGenerator> SentenceGen = new Dictionary<string, SentenceGenerator>();

	public static Dictionary<string, MoodEffect> MoodEffects;

	private static PersonalityGraph Personalities;

	public static int ActiveYear = 80;

	public static Actor founder;

	public static bool IntroDone = false;

	public static bool RuralBigPlots = true;

	public static int SelectedDifficulty = 1;

	public static int DaysPerMonth = 1;

	public static int LoanAmount = 0;

	public static int LoadYear = 0;

	public static GameData.EnvironmentType Environment = GameData.EnvironmentType.Town;

	public static GameData.ClimateType Climate = GameData.ClimateType.Temperate;

	public static List<BuildingPrefab> Prefabs = new List<BuildingPrefab>();

	public static Dictionary<string, KeyValuePair<bool, ActorBodyItem.BodyItemObject[]>> SavedStyles = new Dictionary<string, KeyValuePair<bool, ActorBodyItem.BodyItemObject[]>>();

	public static System.Random RND = new System.Random();

	public static string RandomString = "None";

	public static float[] EnvNoise = new float[]
	{
		0f,
		2f,
		4f
	};

	public static float[] EnvCars = new float[]
	{
		0.1f,
		0.2f,
		0.3f
	};

	private static float[,] CachedEffectiveness = new float[512, 64];

	private static int[] OptimalEmployees;

	[CompilerGenerated]
	private static Func<IGrouping<string, SoftwareTypeOverride>, SoftwareTypeOverride[]> <>f__mg$cache0;

	public enum EnvironmentType
	{
		Rural,
		Town,
		City
	}

	public enum ClimateType
	{
		Cold,
		Temperate,
		Warm
	}
}
