﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Steamworks;
using UnityEngine;

public class SaveGame : IComparable<SaveGame>, IWorkshopItem
{
	private SaveGame(string name, string path, bool readOnly, string cName, string version, SDateTime d1, DateTime d2, float money, int p, int e, int dpm, bool buildingOnly)
	{
		this.Name = name;
		this.CompanyName = cName;
		this.InGameTime = d1;
		this.RealTime = d2;
		this.Money = money;
		this.Products = p;
		this.Employees = e;
		this.GameVersion = version;
		this.DaysPerMonth = dpm;
		this.BuildingOnly = buildingOnly;
		this.Root = path;
		this.Readonly = readOnly;
		try
		{
			if (File.Exists(this.FilePath))
			{
				FileInfo fileInfo = new FileInfo(this.FilePath);
				this.FileSize = (float)fileInfo.Length / 1024f / 1024f;
			}
		}
		catch (Exception)
		{
		}
	}

	private SaveGame(string name, string path, bool readOnly, string cName, string version, SDateTime d1, DateTime d2, float money, int p, int e, int dpm, bool buildingOnly, MiniMapMaker.MapDescriptor map, bool broken)
	{
		this.Name = name;
		this.CompanyName = cName;
		this.InGameTime = d1;
		this.RealTime = d2;
		this.Money = money;
		this.Products = p;
		this.Employees = e;
		this.Map = map;
		this.Broken = broken;
		this.GameVersion = version;
		this.DaysPerMonth = dpm;
		this.BuildingOnly = buildingOnly;
		this.Root = path;
		this.Readonly = readOnly;
		try
		{
			if (File.Exists(this.FilePath))
			{
				FileInfo fileInfo = new FileInfo(this.FilePath);
				this.FileSize = (float)fileInfo.Length / 1024f / 1024f;
			}
		}
		catch (Exception)
		{
		}
	}

	public string UniqueName
	{
		get
		{
			return (!this.BuildingOnly) ? this.Name : ("Build" + this.Name);
		}
	}

	public string FilePath
	{
		get
		{
			return Path.Combine(this.Root, this.Name + ((!this.BuildingOnly) ? ".sav" : ".build"));
		}
	}

	public string ItemTitle
	{
		get
		{
			return this._steamTitle ?? this.Name;
		}
		set
		{
			this._steamTitle = value;
		}
	}

	public PublishedFileId_t? SteamID { get; set; }

	public bool SetTitle
	{
		get
		{
			return this._setTitle;
		}
		set
		{
			this._setTitle = value;
		}
	}

	public bool CanUpload
	{
		get
		{
			return this._canUpload && !this.Readonly;
		}
		set
		{
			this._canUpload = value;
		}
	}

	public string GetWorkshopType()
	{
		return "Building";
	}

	public string FolderPath()
	{
		return Path.GetFullPath(this.Root);
	}

	public string[] GetValidExts()
	{
		return new string[]
		{
			"png",
			"build",
			"txt"
		};
	}

	public string[] ExtraTags()
	{
		return new string[0];
	}

	public string GetThumbnail()
	{
		string text = Path.Combine(this.FolderPath(), "Thumbnail.png");
		return (!File.Exists(text)) ? null : text;
	}

	public float[] GetBuildMeta()
	{
		if (!this.BuildingOnly)
		{
			return null;
		}
		if (this.BuildingMeta == null)
		{
			try
			{
				byte[] bytes = Utilities.ReadData(this.FilePath, "BuildingMeta");
				this.BuildingMeta = Utilities.GetFloatsFromBytes(bytes);
			}
			catch (Exception)
			{
			}
		}
		return this.BuildingMeta;
	}

	public static SaveGame LoadGame(string path, bool build, bool canWrite)
	{
		string text = path;
		string fileNameWithoutExtension;
		if (build)
		{
			string text2 = Directory.GetFiles(path, "*.build").FirstOrDefault<string>();
			if (text2 == null)
			{
				return null;
			}
			text = text2;
			fileNameWithoutExtension = Path.GetFileNameWithoutExtension(text2);
			if (File.Exists(Path.Combine(path, "def.bin")))
			{
				canWrite = false;
			}
		}
		else
		{
			fileNameWithoutExtension = Path.GetFileNameWithoutExtension(path);
		}
		byte[] array = Utilities.ReadData(text, "Meta");
		if (array == null)
		{
			Debug.Log("Meta data empty for save file: " + text);
			return null;
		}
		string stringFromBytes = Utilities.GetStringFromBytes(array);
		ConfigFile configFile = ConfigFile.Load(stringFromBytes.Split(new string[]
		{
			"\r\n",
			Environment.NewLine,
			"\n"
		}, StringSplitOptions.RemoveEmptyEntries));
		string cName = configFile.Get("CompanyName", "Error");
		int dpm = configFile.Get("DaysPerMonth", "1").ConvertToIntDef(1);
		ConfigFile configFile2 = configFile;
		string key = "InGameTime";
		SDateTime sdateTime = new SDateTime(1, 0);
		SDateTime d = SDateTime.FromInt(configFile2.Get(key, sdateTime.ToInt().ToString()).ConvertToIntDef(0), dpm);
		DateTime now;
		if (!DateTime.TryParse(configFile.Get("RealTime", DateTime.Now.ToString()), out now))
		{
			now = DateTime.Now;
		}
		float money = configFile.Get("Money", "0").ConvertToFloatDef(0f);
		int p = configFile.Get("Products", "0").ConvertToIntDef(0);
		int e = configFile.Get("Employees", "0").ConvertToIntDef(0);
		string version = configFile.Get("GameVersion", Versioning.VersionString);
		bool buildingOnly = configFile.Get("BuildingOnly", "false").ConvertToBoolDef(false);
		MiniMapMaker.MapDescriptor map = new MiniMapMaker.MapDescriptor(new SVector3(0f, 0f, 10f, 10f));
		bool broken = false;
		try
		{
			using (MemoryStream memoryStream = new MemoryStream(Utilities.ReadData(text, "Map")))
			{
				BinaryFormatter binaryFormatter = new BinaryFormatter();
				map = (MiniMapMaker.MapDescriptor)binaryFormatter.Deserialize(memoryStream);
			}
		}
		catch (Exception)
		{
			broken = true;
		}
		return new SaveGame(fileNameWithoutExtension, (!build) ? SaveGameManager.SaveFolder : path, !canWrite, cName, version, d, now, money, p, e, dpm, buildingOnly, map, broken);
	}

	public bool SaveNow(bool successMsg, bool failMsg, GameReader.LoadMode mode)
	{
		SaveIcon.Show(this.Name);
		try
		{
			this.CompanyName = GameSettings.Instance.MyCompany.Name.Replace("[", string.Empty).Replace("]", string.Empty);
			this.InGameTime = SDateTime.Now();
			this.RealTime = DateTime.Now;
			this.Money = GameSettings.Instance.MyCompany.Money;
			this.Products = GameSettings.Instance.MyCompany.Products.Count;
			this.Employees = GameSettings.Instance.sActorManager.Actors.Count;
			this.GameVersion = Versioning.VersionString;
			this.BuildingOnly = (mode == GameReader.LoadMode.Building);
			this.BuildingMeta = null;
			if (this.BuildingOnly)
			{
				string path = this.FolderPath();
				if (!Directory.Exists(path))
				{
					Directory.CreateDirectory(path);
				}
			}
			string text = GameReader.SaveGame(this, mode, null);
			if (this.BuildingOnly && text == null)
			{
				try
				{
					GameObject obj = MinimapThumbnailMaker.Instance.MinimapMaker.CreateMap(this.Map, true);
					Texture2D texture2D = MinimapThumbnailMaker.Instance.RenderObject(obj, MinimapThumbnailMaker.ThumbSize.Big, null);
					File.WriteAllBytes(Path.Combine(this.FolderPath(), "Thumbnail.png"), texture2D.EncodeToPNG());
					UnityEngine.Object.Destroy(obj);
					UnityEngine.Object.Destroy(texture2D);
				}
				catch (Exception ex)
				{
				}
			}
			if (text != null && failMsg)
			{
				if (WindowManager.Instance != null)
				{
					WindowManager.SpawnDialog("SaveFailIOError".Loc(new object[]
					{
						text
					}), true, DialogWindow.DialogType.Error);
				}
				return false;
			}
			if (successMsg)
			{
				WindowManager.Instance.ShowMessageBox("SaveGameSuccess".Loc(), false, DialogWindow.DialogType.Information);
			}
			if (SaveGameManager.Instance != null)
			{
				SaveFileItem saveFileItem = SaveGameManager.Instance.FindSave(this);
				if (saveFileItem != null)
				{
					saveFileItem.Init(this);
					saveFileItem.DeInitTex();
				}
				SaveGameManager.Instance.ReorderList();
			}
			this.Legacy = false;
			this.Broken = false;
			try
			{
				if (File.Exists(this.FilePath))
				{
					FileInfo fileInfo = new FileInfo(this.FilePath);
					this.FileSize = (float)fileInfo.Length / 1024f / 1024f;
				}
			}
			catch (Exception)
			{
			}
		}
		catch (Exception ex2)
		{
			Exception e2 = ex2;
			Exception e = e2;
			Debug.LogException(e);
			if (failMsg)
			{
				WindowManager.Instance.ShowMessageBox("SaveGameFail2".Loc(), false, DialogWindow.DialogType.Error, delegate
				{
					FeedbackWindow.Instance.Show(FeedbackWindow.ReportTypes.Exception, false, true, new string[0]);
					FeedbackWindow.Instance.Exception = e.ToString();
				}, null, null);
			}
			return false;
		}
		return true;
	}

	public static SaveGame CreateSave(string name, GameReader.LoadMode mode, bool readOnly)
	{
		bool flag = mode == GameReader.LoadMode.Building;
		name = Utilities.CleanFileName(name);
		return new SaveGame(name, (!flag) ? SaveGameManager.SaveFolder : Path.Combine(SaveGameManager.BuildingFolder, name), readOnly, GameSettings.Instance.MyCompany.Name, Versioning.VersionString, SDateTime.Now(), DateTime.Now, GameSettings.Instance.MyCompany.Money, GameSettings.Instance.MyCompany.Products.Count, GameSettings.Instance.sActorManager.Actors.Count, GameSettings.DaysPerMonth, flag);
	}

	public static SaveGame SaveCurrentGame(string name, bool auto, GameReader.LoadMode mode)
	{
		SaveGame saveGame = SaveGame.CreateSave(name, mode, false);
		return (!saveGame.SaveNow(!auto, !auto, mode)) ? null : saveGame;
	}

	public bool IsOlder()
	{
		Versioning.Version version = Versioning.DisectVersionString(this.GameVersion);
		return version.GetTypeIndex() < Versioning.Type || version.Major < Versioning.Major || version.Minor < Versioning.Minor;
	}

	public int CompareTo(SaveGame y)
	{
		if (y == null)
		{
			return -1;
		}
		if (y == this)
		{
			return 0;
		}
		if (this.BuildingOnly != y.BuildingOnly)
		{
			if (this.BuildingOnly)
			{
				return -1;
			}
			return 1;
		}
		else
		{
			if (!this.BuildingOnly)
			{
				return y.RealTime.CompareTo(this.RealTime);
			}
			float[] buildMeta = this.GetBuildMeta();
			float[] buildMeta2 = y.GetBuildMeta();
			if (buildMeta[0] != buildMeta2[0])
			{
				return buildMeta[0].CompareTo(buildMeta2[0]);
			}
			return buildMeta[2].CompareTo(buildMeta2[2]);
		}
	}

	public override string ToString()
	{
		return this.Name;
	}

	public readonly string Name;

	public bool Legacy;

	public bool Broken;

	public string CompanyName;

	public string Root;

	public SDateTime InGameTime;

	public DateTime RealTime;

	public float Money;

	public int Products;

	public int Employees;

	public int DaysPerMonth;

	public MiniMapMaker.MapDescriptor Map;

	public string GameVersion;

	public float FileSize;

	public bool BuildingOnly;

	private float[] BuildingMeta;

	private string _steamTitle;

	public bool Readonly;

	private bool _canUpload = true;

	private bool _setTitle = true;

	public enum BuildingMetaIndex
	{
		RentMode,
		Area,
		Cost
	}
}
