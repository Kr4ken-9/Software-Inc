﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class StaticTree : MonoBehaviour
{
	public List<Vector3> Verts
	{
		get
		{
			this.Init();
			return this._verts;
		}
	}

	public List<Vector3> Norms
	{
		get
		{
			this.Init();
			return this._norms;
		}
	}

	public List<Vector4> Tans
	{
		get
		{
			this.Init();
			return this._tans;
		}
	}

	public List<int> Tris
	{
		get
		{
			this.Init();
			return this._tris;
		}
	}

	private void Init()
	{
		if (!this._Initialized)
		{
			this._Initialized = true;
			Mesh sharedMesh = this.Leaves.sharedMesh;
			this._verts = new List<Vector3>();
			this._norms = new List<Vector3>();
			this._tans = new List<Vector4>();
			this._tris = new List<int>();
			sharedMesh.GetVertices(this._verts);
			sharedMesh.GetNormals(this._norms);
			sharedMesh.GetTangents(this._tans);
			sharedMesh.GetTriangles(this._tris, 0);
		}
	}

	public Bounds bounds
	{
		get
		{
			if (this.Leaves == null)
			{
				return this.Trunk.sharedMesh.bounds;
			}
			Bounds bounds = this.Trunk.sharedMesh.bounds;
			Bounds bounds2 = this.Leaves.sharedMesh.bounds;
			Vector3 vector = Utilities.MinVector(bounds.min, bounds2.min);
			Vector3 vector2 = Utilities.MinVector(bounds.max, bounds2.max);
			return new Bounds((vector + vector2) * 0.5f, vector2 - vector);
		}
	}

	public MeshFilter Trunk;

	public MeshFilter Leaves;

	[NonSerialized]
	private bool _Initialized;

	private List<Vector3> _verts;

	private List<Vector3> _norms;

	private List<Vector4> _tans;

	private List<int> _tris;
}
