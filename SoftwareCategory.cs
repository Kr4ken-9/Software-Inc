﻿using System;
using UnityEngine;

[Serializable]
public class SoftwareCategory
{
	public SoftwareCategory(SoftwareCategory c, string defaultGen)
	{
		this.Name = c.Name;
		this.Description = c.Description;
		this.Retention = c.Retention;
		this.TimeScale = c.TimeScale;
		this.Popularity = c.Popularity;
		this.Iterative = c.Iterative;
		this._nameGen = defaultGen;
		this.Unlock = c.Unlock;
	}

	public SoftwareCategory(SoftwareCategory c)
	{
		this.Name = c.Name;
		this.Description = c.Description;
		this.Retention = c.Retention;
		this.TimeScale = c.TimeScale;
		this.Popularity = c.Popularity;
		this.Iterative = c.Iterative;
		this.IdealPrice = c.IdealPrice;
		this._nameGen = c._nameGen;
		this.Unlock = c.Unlock;
	}

	public SoftwareCategory(XMLParser.XMLNode node, string defaultGen)
	{
		this.Name = node.GetAttribute("Name");
		this.Description = node.GetNode("Description", true).Value;
		this.Retention = Mathf.Abs(node.GetNodeValue<float>("Retention"));
		this.TimeScale = Mathf.Abs(node.GetNodeValue<float>("TimeScale"));
		this.Popularity = Mathf.Abs(node.GetNodeValue<float>("Popularity"));
		this.Iterative = Mathf.Abs(node.GetNodeValue<float>("Iterative"));
		this.Unlock = node.GetNodeValueOptional<int>("Unlock");
		this.IdealPrice = node.GetNodeValueOptional<float>("IdealPrice");
		this._nameGen = node.GetNodeValue("NameGenerator", defaultGen);
	}

	public SoftwareCategory(string name, string description, float retention, float timescale, float popularity, float iterative, float? idealPrice, string nameGen)
	{
		this.Name = name;
		this.Description = description;
		this.Retention = retention;
		this.TimeScale = timescale;
		this.Popularity = popularity;
		this.Iterative = iterative;
		this.IdealPrice = idealPrice;
		this._nameGen = nameGen;
	}

	public SoftwareCategory()
	{
	}

	public RandomNameGenerator GetNameGen(GameSettings gs)
	{
		return gs.RNG[this._nameGen];
	}

	public string GetNameGenName()
	{
		return this._nameGen;
	}

	public bool IsUnlocked(int year)
	{
		return this.Unlock == null || year >= this.Unlock.Value - SDateTime.BaseYear;
	}

	public static SoftwareCategory GetDefault(float popularity, float retention, float iterative, string nameGen)
	{
		return new SoftwareCategory("Default", string.Empty, retention, 1f, popularity, iterative, null, nameGen);
	}

	public readonly string Name;

	public readonly string Description;

	public readonly float Retention;

	public readonly float TimeScale;

	public readonly float Iterative;

	public float Popularity;

	private string _nameGen;

	public int? Unlock;

	public readonly float? IdealPrice;
}
