﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FrameDistributor : MonoBehaviour
{
	private void FixedUpdate()
	{
		if (this.PauseWithGame && GameSettings.GameSpeed == 0f)
		{
			return;
		}
		float delta = Time.deltaTime;
		if (this.RunEverything)
		{
			for (int i = 0; i < this.Objects.Count; i++)
			{
				if (this.RunSecond)
				{
					this.Objects[i].UpdateNow2(delta);
				}
				else
				{
					this.Objects[i].UpdateNow(delta);
				}
			}
		}
		else if (this.Threaded)
		{
			int max = Mathf.CeilToInt((float)this.Objects.Count / (float)this.MaxObjectsPerFrame);
			if (this.threads.Count < max)
			{
				for (int j = this.threads.Count; j < max; j++)
				{
					int lI = j;
					this.threads.Add(new Thread(delegate
					{
						int num2 = lI * max;
						while (num2 < lI * max + max && num2 < this.Objects.Count)
						{
							if (this.RunSecond)
							{
								this.Objects[num2].UpdateNow2(delta);
							}
							else
							{
								this.Objects[num2].UpdateNow(delta);
							}
							num2++;
						}
					}));
				}
			}
			for (int k = 0; k < max; k++)
			{
				this.threads[k].Start();
			}
			for (int l = 0; l < max; l++)
			{
				this.threads[l].Join();
			}
		}
		else
		{
			int num = (!this.EverythingPerSecond) ? this.MaxObjectsPerFrame : Mathf.CeilToInt((float)this.Objects.Count * delta);
			for (int m = 0; m < this.Objects.Count; m++)
			{
				this.Objects[m].RefreshTime += delta;
				if (m >= this.LastItem && m < this.LastItem + num)
				{
					if (!this.Objects[m].NeedUpdate(!this.RunSecond))
					{
						this.Objects[m].RefreshTime = 0f;
						num++;
					}
					else
					{
						if (this.RunSecond)
						{
							this.Objects[m].UpdateNow2(this.Objects[m].RefreshTime);
						}
						else
						{
							this.Objects[m].UpdateNow(this.Objects[m].RefreshTime);
						}
						this.Objects[m].RefreshTime = 0f;
					}
				}
			}
			this.LastItem += num;
			if (this.LastItem >= this.Objects.Count)
			{
				this.LastItem = 0;
			}
		}
	}

	public void RegisterObject(IDistributee obj)
	{
		this.Objects.Add(obj);
	}

	public void UnregisterObject(IDistributee obj)
	{
		this.Objects.Remove(obj);
	}

	[NonSerialized]
	private List<IDistributee> Objects = new List<IDistributee>();

	[NonSerialized]
	private int LastItem;

	public int MaxObjectsPerFrame = 50;

	public bool RunEverything;

	public bool RunSecond;

	public bool Threaded;

	public bool PauseWithGame;

	public bool EverythingPerSecond;

	[NonSerialized]
	private List<Thread> threads = new List<Thread>();
}
