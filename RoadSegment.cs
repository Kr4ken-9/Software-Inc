﻿using System;
using UnityEngine;

public class RoadSegment : MonoBehaviour
{
	public void Awake()
	{
		MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();
		materialPropertyBlock.SetFloat("_offX", this.Tx);
		materialPropertyBlock.SetFloat("_offY", this.Ty);
		this.render.SetPropertyBlock(materialPropertyBlock);
	}

	public void Init(int rot)
	{
		int num = rot / 90;
		for (int i = 0; i < num; i++)
		{
			RoadNode northOut = this.NorthOut;
			RoadNode northIn = this.NorthIn;
			RoadNode southOut = this.SouthOut;
			RoadNode southIn = this.SouthIn;
			RoadNode eastOut = this.EastOut;
			RoadNode eastIn = this.EastIn;
			RoadNode westOut = this.WestOut;
			RoadNode westIn = this.WestIn;
			this.NorthOut = westOut;
			this.NorthIn = westIn;
			this.SouthOut = eastOut;
			this.SouthIn = eastIn;
			this.EastOut = northOut;
			this.EastIn = northIn;
			this.WestOut = southOut;
			this.WestIn = southIn;
		}
		if (this.NorthOut != null)
		{
			this.NorthOut.Init(true);
		}
		if (this.NorthIn != null)
		{
			this.NorthIn.Init(true);
		}
		if (this.SouthOut != null)
		{
			this.SouthOut.Init(false);
		}
		if (this.SouthIn != null)
		{
			this.SouthIn.Init(false);
		}
		if (this.EastOut != null)
		{
			this.EastOut.Init(true);
		}
		if (this.EastIn != null)
		{
			this.EastIn.Init(false);
		}
		if (this.WestOut != null)
		{
			this.WestOut.Init(false);
		}
		if (this.WestIn != null)
		{
			this.WestIn.Init(true);
		}
	}

	public void Optimize()
	{
		int num = 0;
		RoadNode a = null;
		RoadNode b = null;
		RoadNode a2 = null;
		RoadNode b2 = null;
		RoadSegment roadSegment = null;
		RoadSegment roadSegment2 = null;
		this.SetRoad(this.NorthIn, this.NorthOut, 1, 0, ref num, ref a, ref b, ref a2, ref b2, ref roadSegment, ref roadSegment2);
		this.SetRoad(this.SouthIn, this.SouthOut, -1, 0, ref num, ref a, ref b, ref a2, ref b2, ref roadSegment, ref roadSegment2);
		this.SetRoad(this.EastIn, this.EastOut, 0, -1, ref num, ref a, ref b, ref a2, ref b2, ref roadSegment, ref roadSegment2);
		this.SetRoad(this.WestIn, this.WestOut, 0, 1, ref num, ref a, ref b, ref a2, ref b2, ref roadSegment, ref roadSegment2);
		if (num == 2)
		{
			this.SkipConnection(a, b2, roadSegment, roadSegment2);
			this.SkipConnection(a2, b, roadSegment2, roadSegment);
		}
	}

	private void SkipConnection(RoadNode a, RoadNode b, RoadSegment inny, RoadSegment outy)
	{
		RoadNode roadNode = this.FindOppositeNode(a, inny);
		RoadNode roadNode2 = this.FindOppositeNode(b, outy);
		roadNode.self.GetConnections().Remove(a.self);
		roadNode.self.AddConnection(roadNode2.self);
		b.self.GetConnections().Remove(roadNode2.self);
	}

	private RoadNode FindOppositeNode(RoadNode node, RoadSegment seg)
	{
		if (node == this.NorthIn)
		{
			return seg.SouthOut;
		}
		if (node == this.NorthOut)
		{
			return seg.SouthIn;
		}
		if (node == this.SouthIn)
		{
			return seg.NorthOut;
		}
		if (node == this.SouthOut)
		{
			return seg.NorthIn;
		}
		if (node == this.EastIn)
		{
			return seg.WestOut;
		}
		if (node == this.EastOut)
		{
			return seg.WestIn;
		}
		if (node == this.WestIn)
		{
			return seg.EastOut;
		}
		if (node == this.WestOut)
		{
			return seg.EastIn;
		}
		return null;
	}

	private void SetRoad(RoadNode inR, RoadNode outR, int xx, int yy, ref int c, ref RoadNode a, ref RoadNode aE, ref RoadNode b, ref RoadNode bE, ref RoadSegment aS, ref RoadSegment bS)
	{
		if (inR != null && outR.self.GetConnections().Count > 0)
		{
			RoadSegment segment = RoadManager.Instance.GetSegment(this.x + xx, this.y + yy);
			if (segment != null)
			{
				if (c == 0)
				{
					a = inR;
					aE = outR;
					aS = segment;
				}
				else
				{
					b = inR;
					bE = outR;
					bS = segment;
				}
				c++;
			}
		}
	}

	public void RemoveConnections()
	{
		RoadSegment segment = RoadManager.Instance.GetSegment(this.x + 1, this.y);
		if (segment != null && segment.SouthOut != null && this.NorthIn != null)
		{
			segment.SouthOut.self.RemoveConnection(this.NorthIn.self);
		}
		RoadSegment segment2 = RoadManager.Instance.GetSegment(this.x - 1, this.y);
		if (segment2 != null && segment2.NorthOut != null && this.SouthIn != null)
		{
			segment2.NorthOut.self.RemoveConnection(this.SouthIn.self);
		}
		RoadSegment segment3 = RoadManager.Instance.GetSegment(this.x, this.y - 1);
		if (segment3 != null && segment3.WestOut != null && this.EastIn != null)
		{
			segment3.WestOut.self.RemoveConnection(this.EastIn.self);
		}
		RoadSegment segment4 = RoadManager.Instance.GetSegment(this.x, this.y + 1);
		if (segment4 != null && segment4.EastOut != null && this.WestIn != null)
		{
			segment4.EastOut.self.RemoveConnection(this.WestIn.self);
		}
	}

	public RoadNode EastIn;

	public RoadNode EastOut;

	public RoadNode NorthIn;

	public RoadNode NorthOut;

	public RoadNode WestIn;

	public RoadNode WestOut;

	public RoadNode SouthIn;

	public RoadNode SouthOut;

	public int x;

	public int y;

	public float Tx;

	public float Ty;

	public Renderer render;

	public RoadNode[] Parking = new RoadNode[0];
}
