﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class CompanyWorksheet : MonoBehaviour
{
	private int Granularity
	{
		get
		{
			return CompanyChart.Granularities[Mathf.Max(0, this.GranularityCombo.Selected)];
		}
	}

	public void OnToggle(int panel)
	{
		this.SheetPanel.SetActive(panel == 0);
		this.GraphPanel.SetActive(panel == 1);
	}

	public void UpdateBills()
	{
		object selectedItem = this.BillCombo.SelectedItem;
		if (selectedItem != null)
		{
			Company.TransactionCategory transactionCategory = (Company.TransactionCategory)selectedItem;
			KeyValuePair<string, float>[] source = (from x in GameSettings.Instance.BillsCurrent[transactionCategory]
			orderby x.Key
			select x).ToArray<KeyValuePair<string, float>>();
			bool loc = !this.NoLoc.Contains(transactionCategory);
			this.Bills.SetData((from x in source
			select (!loc) ? x.Key : x.Key.Loc()).ToArray<string>(), (from x in source
			select x.Value.Currency(true)).ToArray<string>());
		}
		else
		{
			this.Bills.SetData(new string[0], new string[0]);
		}
	}

	private void Init()
	{
		this.Worksheet.Width = 5;
		this.Worksheet.Height = this.Categories.Count + this.Categories.Sum((KeyValuePair<string, string[]> x) => x.Value.Length) + 2;
		this.Worksheet.Initialize();
		this.UpdateHeaders();
		this.Worksheet.MarkRow(0, Color.white);
		int num = 1;
		for (int i = 0; i < this.Categories.Count; i++)
		{
			KeyValuePair<string, string[]> keyValuePair = this.Categories[i];
			this.Worksheet[0, num].text = ("Sheet" + keyValuePair.Key).LocDef(keyValuePair.Key.Loc());
			this.Worksheet.MarkRow(num, new Color32(163, 221, byte.MaxValue, byte.MaxValue));
			num++;
			for (int j = 0; j < keyValuePair.Value.Length; j++)
			{
				string text = keyValuePair.Value[j];
				this.Worksheet[0, num].text = ("Sheet" + text).LocDef(text.Loc());
				if (this.Clickables.ContainsKey(text))
				{
					this.Worksheet[0, num].text = "<color=#4444FF>" + this.Worksheet[0, num].text + "</color>";
					EventTrigger eventTrigger = this.Worksheet[0, num].gameObject.AddComponent<EventTrigger>();
					EventTrigger.TriggerEvent triggerEvent = new EventTrigger.TriggerEvent();
					Company.TransactionCategory key = this.Clickables[text];
					triggerEvent.AddListener(delegate(BaseEventData x)
					{
						if (this.BillCombo.Items.Contains(key))
						{
							this.BillCombo.SelectedItem = key;
						}
					});
					EventTrigger.Entry item = new EventTrigger.Entry
					{
						eventID = EventTriggerType.PointerClick,
						callback = triggerEvent
					};
					eventTrigger.triggers.Add(item);
				}
				this.Worksheet.IndentCell(0, num, 8f);
				num++;
			}
		}
		this.Worksheet[0, num].text = "NetProfit".Loc();
		this.Worksheet.MarkRow(num, new Color32(239, 228, 138, byte.MaxValue));
		this.OnToggle(0);
		this.UpdateBills();
	}

	public void ChangeGranularity()
	{
		if (GameSettings.Instance == null)
		{
			return;
		}
		int granularity = this.Granularity;
		this.offset = this.offset * this._lastGranularity / granularity;
		this._lastGranularity = granularity;
		this.UpdateSheet(false);
	}

	private void UpdateHeaders()
	{
		int granularity = this.Granularity;
		SDateTime d = SDateTime.Now();
		int n = d.Month % granularity;
		d -= n;
		for (int i = 0; i < 4; i++)
		{
			int n2 = (i - 3 + this.offset) * granularity;
			if (granularity == 12)
			{
				this.Worksheet[i + 1, 0].text = (d + n2).RealYear.ToString();
			}
			else if (granularity == 3)
			{
				this.Worksheet[i + 1, 0].text = (d + n2).ToQuarterString();
			}
			else
			{
				this.Worksheet[i + 1, 0].text = (d + n2).ToVeryCompactString();
			}
		}
	}

	public void ChangeOffset(int change)
	{
		if (change == 0)
		{
			if (this.offset < 0)
			{
				this.offset = 0;
				this.UpdateSheet(false);
			}
		}
		else if (change == -1)
		{
			int granularity = this.Granularity;
			int num = GameSettings.Instance.MyCompany.Founded.Month % granularity;
			int num2 = (Utilities.GetMonthsFlat(GameSettings.Instance.MyCompany.Founded, SDateTime.Now()) + num) / granularity;
			int num3 = Mathf.Min(0, -num2 + 3);
			if (this.offset > num3)
			{
				this.offset--;
				this.UpdateSheet(false);
			}
		}
		else if (change == 1 && this.offset < 0)
		{
			this.offset++;
			this.UpdateSheet(false);
		}
	}

	public void UpdateSheet(bool onlyThisMonth)
	{
		if (this.init)
		{
			return;
		}
		Company myCompany = GameSettings.Instance.MyCompany;
		int granularity = this.Granularity;
		int num = (SDateTime.Now().Month + 1) % granularity;
		int actual = this.offset * granularity;
		for (int i = 0; i < 4; i++)
		{
			Dictionary<string, float> use = new Dictionary<string, float>();
			for (int j = 0; j < granularity; j++)
			{
				Dictionary<string, float> dictionary;
				if (i == 0 && this.offset == 0 && j == 0)
				{
					dictionary = myCompany.tempCashflow;
				}
				else
				{
					dictionary = myCompany.Cashflow.ToDictionary((KeyValuePair<string, List<float>> x) => x.Key, (KeyValuePair<string, List<float>> x) => (x.Value.Count <= -actual - 1) ? 0f : x.Value[x.Value.Count + actual]);
				}
				Dictionary<string, float> dictionary2 = dictionary;
				foreach (KeyValuePair<string, float> keyValuePair in dictionary2)
				{
					use.AddUp(keyValuePair.Key, keyValuePair.Value);
				}
				actual--;
				if (num > 0)
				{
					num--;
					if (num == 0)
					{
						break;
					}
				}
			}
			int num2 = 1;
			for (int k = 0; k < this.Categories.Count; k++)
			{
				KeyValuePair<string, string[]> keyValuePair2 = this.Categories[k];
				this.SetCellValue(4 - i, num2, (from x in keyValuePair2.Value
				select (!use.ContainsKey(x)) ? 0f : use[x]).Sum(), false);
				num2++;
				for (int l = 0; l < keyValuePair2.Value.Length; l++)
				{
					string key = keyValuePair2.Value[l];
					this.SetCellValue(4 - i, num2, (!use.ContainsKey(key)) ? 0f : use[key], false);
					num2++;
				}
			}
			this.SetCellValue(4 - i, num2, (from x in use
			where !x.Key.Equals("Balance")
			select x).Sum((KeyValuePair<string, float> x) => x.Value), true);
			if (onlyThisMonth && i == 0)
			{
				break;
			}
		}
		if (!onlyThisMonth)
		{
			this.BillCombo.UpdateContent<Company.TransactionCategory>(from x in GameSettings.Instance.BillsCurrent.Keys
			orderby (int)x
			select x);
			this.UpdateBills();
			this.UpdateHeaders();
		}
	}

	private void SetCellValue(int i, int j, float val, bool green = false)
	{
		this.Worksheet[i, j].text = val.Currency(true);
		if (green)
		{
			this.Worksheet[i, j].color = ((val >= -1f) ? ((val <= 1f) ? Color.black : new Color32(51, 204, 51, byte.MaxValue)) : new Color32(239, 60, 57, byte.MaxValue));
		}
		else
		{
			this.Worksheet[i, j].color = ((val >= -1f) ? Color.black : new Color32(239, 60, 57, byte.MaxValue));
		}
	}

	public void Show()
	{
		HUD.Instance.BankruptcyWarning.SetActive(false);
		if (this.init)
		{
			this.Init();
			this.init = false;
		}
		this.UpdateSheet(false);
		base.GetComponent<CompanyChart>().SetCompany(GameSettings.Instance.MyCompany);
		this.Window.Toggle(false);
	}

	public GUIWindow Window;

	public GUIWorkSheet Worksheet;

	public GUICombobox BillCombo;

	public GUICombobox GranularityCombo;

	private bool init = true;

	public int offset;

	private int _lastGranularity = 1;

	[NonSerialized]
	public List<KeyValuePair<string, string[]>> Categories = new List<KeyValuePair<string, string[]>>
	{
		new KeyValuePair<string, string[]>("HR", new string[]
		{
			"Salaries",
			"Staff",
			"Hire",
			"Education",
			"Benefits"
		}),
		new KeyValuePair<string, string[]>("Maintenance", new string[]
		{
			"Construction",
			"Repairs",
			"Bills"
		}),
		new KeyValuePair<string, string[]>("Software", new string[]
		{
			"Sales",
			"Licenses",
			"Marketing",
			"Contracts",
			"Distribution",
			"Royalties"
		}),
		new KeyValuePair<string, string[]>("Misc", new string[]
		{
			"Stocks",
			"Dividends",
			"Loan",
			"Interest",
			"Deals"
		})
	};

	[NonSerialized]
	public Dictionary<string, Company.TransactionCategory> Clickables = new Dictionary<string, Company.TransactionCategory>
	{
		{
			"Bills",
			Company.TransactionCategory.Bills
		},
		{
			"Benefits",
			Company.TransactionCategory.Benefits
		},
		{
			"Construction",
			Company.TransactionCategory.Construction
		},
		{
			"Distribution",
			Company.TransactionCategory.Distribution
		},
		{
			"Staff",
			Company.TransactionCategory.Staff
		},
		{
			"Deals",
			Company.TransactionCategory.Deals
		},
		{
			"Repairs",
			Company.TransactionCategory.Repairs
		},
		{
			"Royalties",
			Company.TransactionCategory.Royalties
		}
	};

	[NonSerialized]
	public HashSet<Company.TransactionCategory> NoLoc = new HashSet<Company.TransactionCategory>
	{
		Company.TransactionCategory.Royalties
	};

	public VarValueSheet Bills;

	public GameObject SheetPanel;

	public GameObject GraphPanel;
}
