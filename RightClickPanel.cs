﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class RightClickPanel : MonoBehaviour
{
	public void KillRingTween()
	{
		if (this.ActiveRingTween != null)
		{
			TweenExtensions.Kill(this.ActiveRingTween, false);
			this.ActiveRingTween = null;
		}
	}

	public void Activate(string[] cats)
	{
		if (this.Buttons.Count == 0)
		{
			return;
		}
		UISoundFX.PlaySFX("ContextMenu", -1f, 0f);
		this.CenterRing.gameObject.SetActive(true);
		this.KillRingTween();
		this.CenterRing.GetComponent<Image>().color = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, 191);
		List<RightClickButton> list;
		if (cats.Length > 1)
		{
			list = new List<RightClickButton>();
			list.AddRange(from x in this.Buttons
			where (x.Value & SelectorController.ACTCAT.NULL) == SelectorController.ACTCAT.NULL
			select x.Key);
			for (int i = 0; i < cats.Length; i++)
			{
				string text = cats[i];
				SelectorController.ACTCAT localItem = (SelectorController.ACTCAT)(1 << Array.IndexOf<string>(SelectorController.Categories, text));
				RightClickButton rightClickButton = this.CreateButton(text, SelectorController.CategoryIcons[text], delegate
				{
					this.SetCategory(localItem);
				}, SelectorController.ContextButtonGroup.Group);
				this.Buttons[rightClickButton] = SelectorController.ACTCAT.NULL;
				list.Add(rightClickButton);
			}
		}
		else
		{
			list = this.Buttons.Keys.ToList<RightClickButton>();
		}
		list = (from x in list
		orderby (int)x.Order
		select x).ToList<RightClickButton>();
		for (int j = 0; j < list.Count; j++)
		{
			list[j].gameObject.SetActive(true);
			list[j].DegWidth = 360f / (float)list.Count;
			list[j].Pos = ((float)(-(float)j) + 0.5f) * (360f / (float)list.Count);
			list[j].Init();
		}
	}

	public void Activate(Vector2 pos)
	{
		if (this.Buttons.Count == 0)
		{
			return;
		}
		base.GetComponent<RectTransform>().anchoredPosition = new Vector2(Mathf.Clamp(pos.x, 128f, (float)(Screen.width - 128)), Mathf.Clamp(-((float)Screen.height - pos.y), (float)(-(float)Screen.height + 128), -128f));
		this.CenterRing.sizeDelta = new Vector2(0f, 0f);
		TweenSettingsExtensions.SetEase<Tweener>(ShortcutExtensions46.DOSizeDelta(this.CenterRing, new Vector2(126f, 126f), 0.6f, true), 30);
		this.Activate(this.CategoryFromSelected().ToArray<string>());
	}

	private IEnumerable<string> CategoryFromSelected()
	{
		if (SelectorController.Instance.Selected.OfType<Actor>().Any((Actor x) => x.AItype == AI<Actor>.AIType.Employee))
		{
			yield return "Employee";
		}
		if (SelectorController.Instance.Selected.OfType<Actor>().Any((Actor x) => x.AItype == AI<Actor>.AIType.Cleaning || x.AItype == AI<Actor>.AIType.IT || x.AItype == AI<Actor>.AIType.Janitor || x.AItype == AI<Actor>.AIType.Receptionist || x.AItype == AI<Actor>.AIType.Courier))
		{
			yield return "Staff";
		}
		if (SelectorController.Instance.Selected.OfType<Room>().Any<Room>())
		{
			yield return "Room";
		}
		if (SelectorController.Instance.Selected.OfType<RoomSegment>().Any<RoomSegment>())
		{
			yield return "Segment";
		}
		if (SelectorController.Instance.Selected.OfType<Furniture>().Any<Furniture>())
		{
			yield return "Furniture";
		}
		if (SelectorController.Instance.Selected.OfType<RoadNode>().Any<RoadNode>())
		{
			yield return "Parking";
		}
		yield break;
	}

	private IEnumerable<KeyValuePair<string, SelectorController.ACTCAT>> CatToString(SelectorController.ACTCAT[] cats)
	{
		bool[] present = new bool[Enum.GetValues(typeof(SelectorController.ACTCAT)).Length];
		foreach (SelectorController.ACTCAT actcat in cats)
		{
			for (int k = 0; k < present.Length; k++)
			{
				if ((actcat & k + SelectorController.ACTCAT.NULL) > (SelectorController.ACTCAT)0)
				{
					present[k] = true;
				}
			}
		}
		for (int i = 1; i < present.Length; i++)
		{
			if (present[i])
			{
				yield return new KeyValuePair<string, SelectorController.ACTCAT>(SelectorController.Categories[i], (SelectorController.ACTCAT)(1 << i));
			}
		}
		yield break;
	}

	private string CatToString(SelectorController.ACTCAT cat)
	{
		switch (cat)
		{
		case SelectorController.ACTCAT.NULL:
			return null;
		case SelectorController.ACTCAT.FURN:
			return "Furniture";
		default:
			if (cat == SelectorController.ACTCAT.STAFF)
			{
				return "Staff";
			}
			if (cat != SelectorController.ACTCAT.SEG)
			{
				return null;
			}
			return "Segment";
		case SelectorController.ACTCAT.ROOM:
			return "Room";
		case SelectorController.ACTCAT.EMP:
			return "Employee";
		}
	}

	private void SetCategory(SelectorController.ACTCAT cat)
	{
		List<RightClickButton> list = new List<RightClickButton>();
		foreach (KeyValuePair<RightClickButton, SelectorController.ACTCAT> keyValuePair in this.Buttons)
		{
			if ((keyValuePair.Value & cat) == cat)
			{
				list.Add(keyValuePair.Key);
			}
			else
			{
				UnityEngine.Object.Destroy(keyValuePair.Key.gameObject);
			}
		}
		this.Buttons.Clear();
		foreach (RightClickButton key in list)
		{
			this.Buttons[key] = cat;
		}
		this.Activate(new string[]
		{
			this.CatToString(cat)
		});
		this.DisableClose = true;
	}

	private RightClickButton CreateButton(string description, string icon, Action action, SelectorController.ContextButtonGroup order)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
		gameObject.name = description;
		RightClickButton component = gameObject.GetComponent<RightClickButton>();
		component.MainPanel = this;
		component.Order = order;
		component.Description = description.Loc();
		component.Icon.sprite = IconManager.GetIcon(icon);
		component.OnClick = delegate
		{
			try
			{
				action();
			}
			catch (Exception ex)
			{
				throw new Exception("Got error when performing action " + description + ":\n" + ex.ToString());
			}
		};
		gameObject.transform.SetParent(base.transform, false);
		return component;
	}

	public void AddButton(string desciption, string icon, Action action, SelectorController.ACTCAT category, SelectorController.ContextButtonGroup order)
	{
		if (this.CenterRing.gameObject.activeSelf)
		{
			this.Deactivate();
		}
		this.Buttons.Add(this.CreateButton(desciption, icon, action, order), category);
	}

	public void Deactivate()
	{
		if (this.CenterRing == null || this.CenterRing.gameObject == null)
		{
			return;
		}
		this.Description.text = string.Empty;
		this.CenterRing.gameObject.SetActive(false);
		foreach (KeyValuePair<RightClickButton, SelectorController.ACTCAT> keyValuePair in this.Buttons)
		{
			UnityEngine.Object.Destroy(keyValuePair.Key.gameObject);
		}
		this.Buttons.Clear();
	}

	private void LateUpdate()
	{
		if (Input.GetMouseButtonUp(0))
		{
			if (this.DisableClose)
			{
				this.DisableClose = false;
			}
			else
			{
				this.Deactivate();
			}
		}
	}

	public GameObject ButtonPrefab;

	public RectTransform CenterRing;

	public Text Description;

	[NonSerialized]
	public Dictionary<RightClickButton, SelectorController.ACTCAT> Buttons = new Dictionary<RightClickButton, SelectorController.ACTCAT>();

	[NonSerialized]
	public Tweener ActiveRingTween;

	private bool DisableClose;
}
