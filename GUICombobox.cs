﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GUICombobox : MonoBehaviour
{
	public List<object> Items
	{
		get
		{
			return this._items;
		}
		set
		{
			this.UpdateContent<object>(value);
		}
	}

	public bool interactable
	{
		get
		{
			return base.GetComponent<Button>().interactable;
		}
		set
		{
			base.GetComponent<Button>().interactable = value;
		}
	}

	public int Selected
	{
		get
		{
			return this._selected;
		}
		set
		{
			this._selected = value;
			if (this.Items == null || this.Selected < 0 || this.Selected >= this.Items.Count)
			{
				this.text.text = this.NoSelectionName.Loc();
			}
			else if (this.Items[this.Selected] == null)
			{
				this.text.text = this.NoSelectionName.Loc();
				this._selected = -1;
			}
			else if (this.FeatureComb)
			{
				string text = this.Items[this.Selected].ToString();
				if ("Any".Equals(text))
				{
					this.text.text = "Any".Loc();
				}
				else if ("Nothing selected".Equals(text))
				{
					this.text.text = "Nothing selected".Loc();
				}
				else
				{
					string[] feature = Localization.GetFeature(this.Software, text);
					this.text.text = feature[0];
				}
			}
			else if (this.SoftwareTypes)
			{
				string value2 = this.Items[this.Selected].ToString();
				if ("All".Equals(value2))
				{
					this.text.text = "All".Loc();
				}
				else if (this.SWCategory)
				{
					this.text.text = this.SelectedItem.ToString().LocSWC(this.Software);
				}
				else
				{
					SoftwareType sw = GameSettings.Instance.SoftwareTypes[this.Items[this.Selected].ToString()];
					string[] software = Localization.GetSoftware(sw);
					this.text.text = software[0];
				}
			}
			else
			{
				this.text.text = ((!this.LocalizeContent) ? this.Items[this.Selected].ToString() : this.Items[this.Selected].ToString().LocTry());
			}
			if (this.OnSelectedChanged != null && this.Items != null && this.Items.Count > 0)
			{
				this.OnSelectedChanged.Invoke();
			}
		}
	}

	public bool IsShown
	{
		get
		{
			return this.DropDownPanel.activeSelf;
		}
	}

	public object SelectedItem
	{
		get
		{
			return (this.Items == null || this.Selected <= -1 || this.Selected >= this.Items.Count) ? null : this.Items[this.Selected];
		}
		set
		{
			this.Selected = this.Items.IndexOf(value);
		}
	}

	public string SelectedItemString
	{
		get
		{
			return (this.Items == null || this.Selected <= -1 || this.Selected >= this.Items.Count) ? null : ((this.Items[this.Selected] == null) ? null : this.Items[this.Selected].ToString());
		}
	}

	public void OnSelectedChangedRemoveAllListeners()
	{
		if (this.OnSelectedChanged == null)
		{
			this.OnSelectedChanged = new UnityEvent();
		}
		else
		{
			this.OnSelectedChanged.RemoveAllListeners();
		}
	}

	private void Awake()
	{
		if (this.Items == null)
		{
			this.Items = new List<object>();
		}
	}

	public void UpdateContent<T>(IEnumerable<T> newListt)
	{
		float num = 64f;
		List<object> items = newListt.Cast<object>().ToList<object>();
		object selectedItem = this.SelectedItem;
		this._items = items;
		for (int i = this._items.Count; i < this.Elements.Count; i++)
		{
			this.Elements[i].SetActive(false);
		}
		for (int k = 0; k < this.Items.Count; k++)
		{
			string[] array = new string[]
			{
				"Error",
				"Error"
			};
			if (this.SoftwareTypes && "All".Equals(this.Items[k].ToString()))
			{
				array = new string[]
				{
					"All".Loc(),
					"N/A"
				};
			}
			else if (this.FeatureComb && "Any".Equals(this.Items[k].ToString()))
			{
				array = new string[]
				{
					"Any".Loc(),
					"N/A"
				};
			}
			else
			{
				if (this.SoftwareTypes)
				{
					if (this.SWCategory)
					{
						array = new string[]
						{
							this.Items[k].ToString().LocSWC(this.Software)
						};
					}
					else
					{
						array = Localization.GetSoftware(this.Items[k].ToString(), "Error");
					}
				}
				if (this.FeatureComb)
				{
					array = Localization.GetFeature(this.Software, this.Items[k].ToString());
				}
			}
			string elementLabel;
			if (k < this.Elements.Count)
			{
				GameObject gameObject = this.Elements[k];
				gameObject.SetActive(true);
				elementLabel = this.GetElementLabel(this.Items[k], array[0]);
				gameObject.GetComponentsInChildren<Text>(true)[0].text = elementLabel;
			}
			else
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
				this.Elements.Add(gameObject2);
				elementLabel = this.GetElementLabel(this.Items[k], array[0]);
				gameObject2.GetComponentInChildren<Text>().text = elementLabel;
				int j = k;
				gameObject2.GetComponentInChildren<Button>().onClick.AddListener(delegate
				{
					this.UpdateSelection(j);
				});
				gameObject2.transform.SetParent(this.DropDownContent.transform, false);
			}
			num = Mathf.Max(num, (float)elementLabel.Length * 10f);
		}
		int num2 = this.Items.IndexOf(selectedItem);
		if (num2 >= 0)
		{
			this._selected = num2;
		}
		else
		{
			this.Selected = 0;
		}
		this.UpdateDropdownSize(num);
	}

	private string GetElementLabel(object element, string swLoc)
	{
		if (element == null)
		{
			return (!this.LocalizeNullName) ? this.NullName : this.NullName.Loc();
		}
		return (!this.SoftwareTypes && !this.FeatureComb) ? ((!this.LocalizeContent) ? element.ToString() : element.ToString().LocTry()) : swLoc;
	}

	public void UpdateContent<T>(IEnumerable<T> newListt, IEnumerable<string> toolTips)
	{
		float num = 64f;
		List<object> items = newListt.Cast<object>().ToList<object>();
		string[] array = toolTips.ToArray<string>();
		object selectedItem = this.SelectedItem;
		this._items = items;
		this.Elements.ForEach(delegate(GameObject x)
		{
			UnityEngine.Object.Destroy(x.gameObject);
		});
		this.Elements.Clear();
		for (int i = 0; i < this.Items.Count; i++)
		{
			string[] array2 = new string[]
			{
				"Error",
				"Error"
			};
			if (this.SoftwareTypes)
			{
				if (this.SWCategory)
				{
					array2 = Localization.GetSoftwareCat(this.Software, this.Items[i].ToString(), array[i]);
				}
				else
				{
					array2 = Localization.GetSoftware(this.Items[i].ToString(), array[i]);
				}
			}
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
			this.Elements.Add(gameObject);
			string elementLabel = this.GetElementLabel(this.Items[i], array2[0]);
			num = Mathf.Max(num, (float)elementLabel.Length * 10f);
			gameObject.GetComponentInChildren<Text>().text = elementLabel;
			int j = i;
			gameObject.GetComponentInChildren<Button>().onClick.AddListener(delegate
			{
				this.UpdateSelection(j);
			});
			gameObject.GetComponent<GUIToolTipper>().TooltipDescription = ((!this.SoftwareTypes) ? array[i] : array2[1]);
			gameObject.transform.SetParent(this.DropDownContent.transform, false);
		}
		int num2 = this.Items.IndexOf(selectedItem);
		if (num2 >= 0)
		{
			this._selected = num2;
		}
		else
		{
			this.Selected = 0;
		}
		this.UpdateDropdownSize(num);
	}

	private void UpdateDropdownSize(float width)
	{
		this.PanelRect.sizeDelta = new Vector2(width + 18f, this.PanelRect.sizeDelta.y);
	}

	private void OnDestroy()
	{
		UnityEngine.Object.Destroy(this.DropDownPanel);
	}

	public void ToggleDropDown()
	{
		this.PanelRect.sizeDelta = new Vector2(this.PanelRect.sizeDelta.x, (float)(((this.MaxItems != 0) ? Mathf.Min(this.Items.Count, this.MaxItems) : this.Items.Count) * 28 + 4));
		RectTransform component = this.DropDownContent.GetComponent<RectTransform>();
		ScrollRect component2 = this.DropDownPanel.GetComponent<ScrollRect>();
		component.offsetMax = new Vector2((float)((this.Items.Count <= this.MaxItems) ? 0 : -16), 0f);
		component2.verticalScrollbar = ((this.Items.Count <= this.MaxItems) ? null : this.scrollBar.GetComponent<Scrollbar>());
		this.scrollBar.SetActive(this.Items.Count > this.MaxItems);
		this.DropDownPanel.transform.SetParent(WindowManager.Instance.Canvas.transform);
		this.comboPanelParent = WindowManager.Instance.Canvas.GetComponent<CanvasScaler>();
		this.EnableDropdownPanel(!this.DropDownPanel.activeSelf);
		this.JustOpened = this.DropDownPanel.activeSelf;
	}

	public void EnableDropdownPanel(bool enable)
	{
		if (enable)
		{
			GUICombobox.OpenCombo = this;
		}
		else if (GUICombobox.OpenCombo == this)
		{
			GUICombobox.OpenCombo = null;
		}
		this.DropDownPanel.SetActive(enable);
	}

	public void UpdateSelection(int idx)
	{
		this.Selected = ((idx >= this.Items.Count) ? -1 : idx);
		this.EnableDropdownPanel(false);
	}

	public void UpdateSelectionSupressEvents(int idx)
	{
		this._selected = idx;
		this.text.text = ((this.Items == null || this.Selected >= this.Items.Count) ? string.Empty : this.Items[this.Selected].ToString());
	}

	public void DisableClose()
	{
		this.JustOpened = true;
	}

	private void OnDisable()
	{
		if (this.DropDownPanel != null)
		{
			this.EnableDropdownPanel(false);
		}
	}

	private void Update()
	{
		float num = (!(this.comboPanelParent != null)) ? 1f : this.comboPanelParent.scaleFactor;
		this.DropDownPanel.transform.position = new Vector3(base.transform.position.x + this.rectTransform.rect.width * num / 2f + (this.PanelRect.rect.width * num - this.rectTransform.rect.width * num), base.transform.position.y - this.rectTransform.rect.height * num / 2f, base.transform.position.z);
		if (!this.JustOpened)
		{
			if (Input.GetMouseButtonUp(0))
			{
				this.EnableDropdownPanel(false);
			}
		}
		else
		{
			this.JustOpened = false;
		}
	}

	public static GUICombobox OpenCombo;

	public GameObject DropDownPanel;

	public GameObject DropDownContent;

	public GameObject scrollBar;

	public GameObject ButtonPrefab;

	public Text text;

	public int MaxItems;

	public bool LocalizeContent;

	public bool SoftwareTypes;

	public bool SWCategory;

	public bool FeatureComb;

	public string Software;

	public string NullName = "None";

	public bool LocalizeNullName = true;

	public string NoSelectionName = "Nothing selected";

	private List<object> _items;

	private bool JustOpened;

	public UnityEvent OnSelectedChanged;

	private List<GameObject> Elements = new List<GameObject>();

	private int _selected;

	public RectTransform PanelRect;

	private CanvasScaler comboPanelParent;

	public RectTransform rectTransform;
}
