﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveFileItem : MonoBehaviour, IComparable<SaveFileItem>
{
	public void Init(SaveGame save)
	{
		this.Save = save;
		this.MainLabel.text = this.Save.ItemTitle;
		this.SteamIcon.SetActive(this.Save.SteamID != null);
		this.DeleteIcon.SetActive(!this.Save.Readonly);
		if (this.Save.BuildingOnly)
		{
			float[] buildMeta = this.Save.GetBuildMeta();
			if (buildMeta == null || buildMeta.Length < 4)
			{
				this.SubLabel1.text = "Error".Loc();
			}
			else if (buildMeta[0] == 0f)
			{
				this.SubLabel1.text = string.Format("{0}\n{4}: {1}\n{5}: {3}\n{6}: {2}", new object[]
				{
					"For rent".Loc(),
					buildMeta[1].ToString("N0") + " m2",
					buildMeta[2].Currency(true),
					buildMeta[3].ToString("N0") + " m2",
					"Leased area".Loc(),
					"Building area".Loc(),
					"Cost".Loc()
				});
			}
			else
			{
				this.SubLabel1.text = string.Format("{0}\n{4}: {1}\n{5}: {3}\n{6}: {2}", new object[]
				{
					"For sale".Loc(),
					buildMeta[1].ToString("N0") + " m2",
					buildMeta[2].Currency(true),
					buildMeta[3].ToString("N0") + " m2",
					"Building area".Loc(),
					"Plot area".Loc(),
					"Cost".Loc()
				});
			}
			this.SubLabel2.text = this.Save.FileSize.ByteSize(false);
		}
		else
		{
			this.SubLabel1.text = string.Format("{0}\n{4}: {1}\n{5}: {2}\n{6}: {3}", new object[]
			{
				this.Save.RealTime.ToString("dd MMM yyyy HH:mm"),
				this.Save.CompanyName,
				this.Save.Money.Currency(true),
				this.Save.Products,
				"Company".Loc(),
				"Money".Loc(),
				"Products".Loc()
			});
			this.SubLabel2.text = string.Format("{0}\n{4}: {1}\n{5}: {2}\n{6}: {3}", new object[]
			{
				this.Save.FileSize.ByteSize(false),
				this.Save.InGameTime.ToCompactString(),
				this.Save.Employees,
				this.Save.DaysPerMonth,
				"Date".Loc(),
				"Employees".Loc(),
				"DaysPerMonth".Loc()
			});
		}
		if (this.TextureInit)
		{
			this.InitTexture((Texture2D)this.Thumbnail.texture);
		}
	}

	public void Delete()
	{
		DialogWindow diag = WindowManager.SpawnDialog();
		diag.Show("DeleteSaveConf".Loc(), false, DialogWindow.DialogType.Warning, new KeyValuePair<string, Action>[]
		{
			new KeyValuePair<string, Action>("Yes", delegate
			{
				SaveGameManager.Instance.DeleteSave(this.Save, true);
				diag.Window.Close();
			}),
			new KeyValuePair<string, Action>("No", delegate
			{
				diag.Window.Close();
			})
		});
	}

	public void InitTexture(Texture2D tex)
	{
		if (this.Save.Broken)
		{
			return;
		}
		if (!this.Save.BuildingOnly && Versioning.DisectVersionString(this.Save.GameVersion).Major < SaveGameManager.MinimumSupportedSaveAlpha)
		{
			return;
		}
		this.MainLabel.text = this.Save.ItemTitle;
		if (!this.TextureInit)
		{
			GameObject obj = MinimapThumbnailMaker.Instance.MinimapMaker.CreateMap(this.Save.Map, true);
			MinimapThumbnailMaker.Instance.RenderObject(obj, MinimapThumbnailMaker.ThumbSize.Small, tex);
			UnityEngine.Object.Destroy(obj);
			this.Thumbnail.texture = tex;
			this.TextureInit = true;
		}
	}

	public Texture2D DeInitTex()
	{
		if (this.TextureInit)
		{
			this.TextureInit = false;
			return (Texture2D)this.Thumbnail.texture;
		}
		return null;
	}

	public int CompareTo(SaveFileItem other)
	{
		if (other == null)
		{
			return -1;
		}
		if (this.Save == null)
		{
			return (other.Save != null) ? 1 : 0;
		}
		return this.Save.CompareTo(other.Save);
	}

	public RawImage Thumbnail;

	public bool TextureInit;

	public Text MainLabel;

	public Text SubLabel1;

	public Text SubLabel2;

	[NonSerialized]
	public SaveGame Save;

	public Toggle Toggle;

	public RectTransform rect;

	public GameObject SteamIcon;

	public GameObject DeleteIcon;
}
