﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TeamWindow : MonoBehaviour
{
	public void UpdateMinSpec()
	{
		this.MinSpecLabel.text = (this.MinSpec.value / 100f).ToPercent();
		if (!this._selectionChanging)
		{
			Team[] selected = this.TeamList.GetSelected<Team>();
			float specializationMinCap = this.MinSpec.value / 100f;
			for (int i = 0; i < selected.Length; i++)
			{
				selected[i].SpecializationMinCap = specializationMinCap;
			}
		}
	}

	public void UpdateVacation()
	{
		if (!this._selectionChanging)
		{
			foreach (Team team in this.TeamList.GetSelected<Team>())
			{
				team.VacationMonth = this.MonthCombo.Selected;
				team.RescheduleVacations();
			}
		}
	}

	public void UpdateVacationRange()
	{
		if (!this._selectionChanging)
		{
			foreach (Team team in this.TeamList.GetSelected<Team>())
			{
				team.VacationSpread = this.RangeCombo.Selected;
				team.RescheduleVacations();
			}
		}
	}

	public void Disband()
	{
		Team[] sel = this.TeamList.GetSelected<Team>();
		if (sel.Length > 0)
		{
			WindowManager instance = WindowManager.Instance;
			string text = "TeamDisbandConfirmation";
			object[] array = new object[1];
			array[0] = Newspaper.MakeList(sel.SelectInPlace((Team x) => x.Name));
			instance.ShowMessageBox(text.Loc(array), true, DialogWindow.DialogType.Warning, delegate
			{
				for (int i = 0; i < sel.Length; i++)
				{
					Team team = sel[i];
					GameSettings.Instance.sActorManager.RemoveTeam(team.Name);
				}
				this.TeamList.ClearSelected();
				this.TeamList.Items = GameSettings.Instance.sActorManager.Teams.Values.Cast<object>().ToList<object>();
			}, "Disband team", null);
		}
	}

	public void Rename()
	{
		Team[] selected = this.TeamList.GetSelected<Team>();
		if (selected.Length == 1)
		{
			Team x = selected[0];
			WindowManager.SpawnInputDialog("Rename".Loc(), "Rename".Loc(), x.Name, delegate(string y)
			{
				GameSettings.Instance.sActorManager.RenameTeam(x.Name, y);
			}, null);
		}
	}

	public void ShowEmployees()
	{
		Team[] selected = this.TeamList.GetSelected<Team>();
		if (selected.Length > 0)
		{
			HUD.Instance.employeeWindow.Show(selected.ToHashSet<Team>());
		}
	}

	private void Start()
	{
		this.MonthCombo.UpdateContent<string>(SDateTime.Months);
		this.RangeCombo.UpdateContent<string>(from x in Enumerable.Range(1, 12)
		select "MonthPostfix".LocPlural(x));
		this.TeamList.Items = GameSettings.Instance.sActorManager.Teams.Values.Cast<object>().ToList<object>();
		this.TeamList.OnSelectChange = delegate(bool d)
		{
			Team[] selected = this.TeamList.GetSelected<Team>();
			this.chart.SetContent(this.TeamList.GetSelected<Team>().SelectMany((Team x) => from z in x.GetEmployees()
			select z.employee).ToArray<Employee>());
			this.chart.MinSkillTeam = ((selected.Length != 1) ? null : selected[0]);
			this._selectionChanging = true;
			this.UpdateArrDepTime();
			Slider minSpec = this.MinSpec;
			float value;
			if (selected.Length == 0)
			{
				value = 0f;
			}
			else
			{
				value = selected.Average((Team x) => x.SpecializationMinCap) * 100f;
			}
			minSpec.value = value;
			this.MinSpec.interactable = (selected.Length > 0);
			this.RangeCombo.interactable = (selected.Length > 0);
			this.CrunchToggle.interactable = (selected.Length > 0);
			this.MonthCombo.interactable = (selected.Length > 0);
			if (selected.Length > 0)
			{
				this.MonthCombo.Selected = selected.Mode((Team x) => x.VacationMonth, 0);
				this.RangeCombo.Selected = selected.Mode((Team x) => x.VacationSpread, 0);
				this.CrunchToggle.isOn = this.TeamList.GetSelected<Team>().Mode((Team x) => x.CrunchMode, false);
			}
			this._selectionChanging = false;
		};
	}

	public void ToggleCrunch()
	{
		if (!this._selectionChanging)
		{
			Team[] selected = this.TeamList.GetSelected<Team>();
			for (int i = 0; i < selected.Length; i++)
			{
				selected[i].CrunchMode = this.CrunchToggle.isOn;
			}
		}
	}

	public void AddTeam()
	{
		if (this.CreateTeam(this.input.text))
		{
			this.input.text = "Team name".Loc();
		}
	}

	public bool CreateTeam(string teamName)
	{
		if (!teamName.Trim().IsEmpty())
		{
			if (!GameSettings.Instance.sActorManager.Teams.Any((KeyValuePair<string, Team> x) => x.Key.Equals(teamName)))
			{
				GameSettings.Instance.sActorManager.Teams.Add(teamName, new Team(teamName));
				this.TeamList.Items = GameSettings.Instance.sActorManager.Teams.Values.Cast<object>().ToList<object>();
				return true;
			}
			WindowManager.Instance.ShowMessageBox("TeamNameError".Loc(), false, DialogWindow.DialogType.Error);
		}
		else
		{
			WindowManager.Instance.ShowMessageBox("TeamEmptyNameError".Loc(), false, DialogWindow.DialogType.Error);
		}
		return false;
	}

	private void UpdateArrDepTime()
	{
		Team[] selected = this.TeamList.GetSelected<Team>();
		int hour = 8;
		int hour2 = 16;
		if (selected.Length > 0)
		{
			hour = selected.Mode((Team x) => x.WorkStart, 0);
			hour2 = selected.Mode((Team x) => x.WorkEnd, 0);
		}
		this.ArrTime.text = Utilities.HourToTime(hour, SDateTime.AMPM);
		this.DepTime.text = Utilities.HourToTime(hour2, SDateTime.AMPM);
	}

	public void OpenHR()
	{
		Team[] selected = this.TeamList.GetSelected<Team>();
		if (selected.Length > 0)
		{
			this.autoWindow.Show(selected);
		}
	}

	public void ApplyTimes()
	{
		if (!this._selectionChanging)
		{
			Team[] selected = this.TeamList.GetSelected<Team>();
			if (selected.Length > 0)
			{
				int num = this.ArrTime.text.TimeToHour(8);
				int num2 = this.DepTime.text.TimeToHour(16);
				if (num == num2)
				{
					num2 = (num2 + 1) % 24;
				}
				foreach (Team team in selected)
				{
					team.WorkStart = num;
					team.WorkEnd = num2;
				}
				this.UpdateArrDepTime();
			}
		}
	}

	public void ManageRoles()
	{
		Team[] selected = this.TeamList.GetSelected<Team>();
		if (selected.Length > 0)
		{
			HUD.Instance.roleGrid.Show(selected.SelectMany((Team x) => x.GetEmployeesDirect()));
		}
	}

	public GUIWindow Window;

	public AutomationWindow autoWindow;

	public GUIListView TeamList;

	public InputField input;

	public InputField ArrTime;

	public InputField DepTime;

	public SpecializationChart chart;

	public GUICombobox MonthCombo;

	public GUICombobox RangeCombo;

	public Toggle CrunchToggle;

	public Slider MinSpec;

	public Text MinSpecLabel;

	private bool _selectionChanging;
}
