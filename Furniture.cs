﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DG.Tweening;
using UnityEngine;

public class Furniture : WallSnap, IRoomConnector, IDistributee
{
	public static void UpdateEdgeDetection()
	{
		if (GameSettings.Instance == null)
		{
			return;
		}
		foreach (Furniture furniture in GameSettings.Instance.sRoomManager.AllFurniture)
		{
			if (furniture != null)
			{
				furniture.RefreshEdgeDetection();
			}
		}
	}

	public Actor Reserved
	{
		get
		{
			return this._reserved;
		}
		set
		{
			if (value != this._reserved)
			{
				if (this._reserved != null)
				{
					this._reserved.ReservedFurniture.Remove(this);
				}
				this._reserved = value;
				if (this._reserved != null)
				{
					this._reserved.ReservedFurniture.Add(this);
				}
			}
		}
	}

	public bool IsOn
	{
		get
		{
			return this._isOn;
		}
		set
		{
			if (this.isTemporary)
			{
				return;
			}
			if (this.HasUpg && this.upg.Broken)
			{
				value = false;
			}
			if ((!this.AlwaysOn || value || (this.HasUpg && this.upg.Broken)) && this._isOn != value)
			{
				SDateTime dateLocked = TimeOfDay.GetDateLocked();
				if (!this.ManualUsageCalculation && !this.PlacedInEditMode && (!GameSettings.Instance.RentMode || this.InRentMode) && this._isOn)
				{
					float months = Mathf.Clamp01(Utilities.GetMonths(this.LastActivity, dateLocked));
					GameSettings.Instance.ElectricityBill += this.GetWattPrice(months, this.Wattage);
					GameSettings.Instance.Waterbill += this.GetWaterPrice(months, this.Water);
				}
				this.LastActivity = dateLocked;
				this._isOn = value;
				if (!this.ManualUsageCalculation)
				{
					this.LastWaterUse = ((!this._isOn) ? 0f : this.Water);
					this.LastWattUse = ((!this._isOn) ? 0f : this.Wattage);
				}
				if (this.HeatCoolPotential != 0f)
				{
					this.Parent.MakeTemperatureDirty(true);
				}
				for (int i = 0; i < this.SnapPoints.Length; i++)
				{
					if (this.SnapPoints[i].UsedBy != null)
					{
						this.SnapPoints[i].UsedBy.ParentPowerToggled();
					}
				}
				if (this.Noisiness > 0f)
				{
					this.Parent.RefreshNoise();
				}
				base.SendMessage("PowerToggled", this._isOn, SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	public Actor OwnedBy
	{
		get
		{
			return this._ownedBy;
		}
		set
		{
			this._ownedBy = value;
			if (this.OwnedBy != null)
			{
				if (this.Reserved != null && this.OwnedBy != this.Reserved)
				{
					this.Reserved = null;
				}
				for (int i = 0; i < this.InteractionPoints.Length; i++)
				{
					InteractionPoint interactionPoint = this.InteractionPoints[i];
					if (interactionPoint.UsedBy != null && interactionPoint.UsedBy != this._ownedBy)
					{
						interactionPoint.UsedBy.AIScript.currentNode = interactionPoint.UsedBy.AIScript.BehaviorNodes["Loiter"];
						interactionPoint.UsedBy.UsingPoint = null;
					}
				}
				if (this.OwnedBy.isActiveAndEnabled && this.OwnedBy.UsingPoint != null && this.OwnedBy.UsingPoint.Parent != this && this.OwnedBy.UsingPoint.Parent.Type.Equals(this.Type) && this.OwnedBy.UsingPoint.Parent.OwnedBy != this.OwnedBy)
				{
					if (this.OwnedBy.UsingPoint.Parent.Reserved == this.OwnedBy)
					{
						this.OwnedBy.UsingPoint.Parent.Reserved = null;
					}
					this.OwnedBy.UsingPoint = null;
					this.OwnedBy.AIScript.currentNode = this.OwnedBy.AIScript.BehaviorNodes["Loiter"];
				}
			}
			if (this.Type.Equals("Chair"))
			{
				Furniture computer = this.GetComputer();
				if (computer != null && computer.OwnedBy != this.OwnedBy)
				{
					if (computer.OwnedBy != null)
					{
						computer.OwnedBy.Owns.Remove(computer);
					}
					computer.OwnedBy = this._ownedBy;
					if (this._ownedBy != null)
					{
						this._ownedBy.Owns.Add(computer);
					}
				}
			}
			else if (this.Type.Equals("Computer") && this.ComputerChair != null && this.ComputerChair.OwnedBy != this.OwnedBy)
			{
				if (this.ComputerChair.OwnedBy != null)
				{
					this.ComputerChair.OwnedBy.Owns.Remove(this.ComputerChair);
				}
				this.ComputerChair.OwnedBy = this._ownedBy;
				if (this._ownedBy != null)
				{
					this._ownedBy.Owns.Add(this.ComputerChair);
				}
			}
			if (this._ownedBy != null && this.Parent != null && !this.Parent.AllowedInRoom(this._ownedBy))
			{
				HUD.Instance.AddPopupMessage("FurnitureInLimitRoomWarning".Loc(new object[]
				{
					Localization.GetFurniture(base.name, null)[0]
				}), "Info", PopupManager.PopUpAction.GotoFurn, this.DID, PopupManager.NotificationSound.Issue, 0.5f, PopupManager.PopupIDs.None, 1);
			}
			this.CheckAssignMat();
		}
	}

	public void SetOwnedByDeserializing(Actor act)
	{
		this._ownedBy = act;
	}

	public override string[] GetActions()
	{
		return this.actions;
	}

	public override string[] GetExtendedIconInfo()
	{
		if (this.Type.Equals("Server"))
		{
			return new string[]
			{
				"Server",
				"Chart",
				"Lightning",
				"Wires",
				"MoreSoftware"
			};
		}
		return new string[]
		{
			"Furniture"
		};
	}

	public override Color[] GetExtendedColorInfo()
	{
		if (this.Type.Equals("Server"))
		{
			Server component = base.GetComponent<Server>();
			return new Color[]
			{
				base.GetColorStat((!(component.Rep != null)) ? 1f : (component.Rep.Available * 2f)),
				base.GetColorStat(1f),
				base.GetColorStat(1f),
				base.GetColorStat(1f)
			};
		}
		return null;
	}

	public void TurnMonth()
	{
		if (!this.ManualUsageCalculation && !this.PlacedInEditMode && (!GameSettings.Instance.RentMode || this.InRentMode) && this.IsOn && (this.Water > 0f || this.Wattage > 0f))
		{
			float months = Mathf.Clamp01(Utilities.GetMonths(this.LastActivity, SDateTime.Now()));
			GameSettings.Instance.ElectricityBill += this.GetWattPrice(months, this.Wattage);
			GameSettings.Instance.Waterbill += this.GetWaterPrice(months, this.Water);
			this.LastActivity = SDateTime.Now();
		}
	}

	public void CalculateManualUsage(float months)
	{
		GameSettings.Instance.ElectricityBill += this.GetWattPrice(months, this.Wattage);
		GameSettings.Instance.Waterbill += this.GetWaterPrice(months, this.Water);
		float num = months * 24f * (float)GameSettings.DaysPerMonth;
		this.LastWaterUse = this.Water * num;
		this.LastWattUse = this.Wattage * num;
	}

	public void ParentPowerToggled()
	{
		if (this.OnWithParent && this.SnappedTo != null)
		{
			this.IsOn = this.SnappedTo.Parent.IsOn;
		}
	}

	public override string[] GetExtendedInfo()
	{
		if (this.Type.Equals("Server"))
		{
			Server component = base.GetComponent<Server>();
			if (component.Rep != null)
			{
				return new string[]
				{
					component.Rep.ServerName,
					((1f - component.Rep.Available) * 100f).ToString("0") + "%",
					component.Rep.PowerSum.Bandwidth(),
					(component.Rep.Children.Count + 1).ToString(),
					component.Rep.Items.Count.ToString()
				};
			}
		}
		string[] furniture = Localization.GetFurniture(base.name, null);
		return new string[]
		{
			furniture[0]
		};
	}

	public override string[] GetExtendedTooltipInfo()
	{
		if (this.Type.Equals("Server"))
		{
			return new string[]
			{
				"ServerLoad".Loc(),
				"Bandwidth".Loc(),
				"Server count".Loc(),
				"Processes".Loc()
			};
		}
		return null;
	}

	public bool Broken()
	{
		return this.HasUpg && this.upg.Broken;
	}

	public override string GetInfo()
	{
		if (this.Type.Equals("Server"))
		{
			return this.upg.GetDescription();
		}
		if (base.gameObject == null)
		{
			return string.Empty;
		}
		if (this.Type.Equals("Elevator") || this.Type.Equals("Stairs"))
		{
			return string.Empty;
		}
		StringBuilder stringBuilder = new StringBuilder();
		if (this.HasUpg)
		{
			stringBuilder.AppendLine(this.upg.GetDescription());
		}
		if (this.Wattage > 0f)
		{
			stringBuilder.AppendLine((!this.IsOn) ? "Currently off".Loc() : "Currently on".Loc());
		}
		if (this.InteractionPoints.Length > 0)
		{
			stringBuilder.AppendLine("Currently used by".Loc() + ": " + this.UsedByInfo());
		}
		if (this.CanAssign)
		{
			stringBuilder.AppendLine("Owned by".Loc() + ": " + ((!(this.OwnedBy == null)) ? this.OwnedBy.employee.FullName : "Nobody".Loc()));
		}
		if (this.Type.Equals("Chair"))
		{
			stringBuilder.AppendLine("Comfort".Loc() + ": " + (this.GetComfort() * 100f).ToString("F") + "%");
		}
		if (this.Type.Equals("Computer") && this.InteractionPoints[0].UsedBy != null)
		{
			stringBuilder.AppendLine("Noise level".Loc() + ": " + this.FinalNoise.ToDB());
		}
		if (this.TemperatureController)
		{
			stringBuilder.AppendLine(string.Concat(new object[]
			{
				"Capacity".Loc(),
				": ",
				this.TempControllerArea.ToString("0.##"),
				"/",
				this.HeatCoolArea,
				" m2"
			}));
		}
		if (this.Type.Equals("ProductPrinter") && this.IsOn)
		{
			ProductPrinter component = base.GetComponent<ProductPrinter>();
			stringBuilder.AppendLine("NextPrintCountdown".Loc(new object[]
			{
				Mathf.RoundToInt(component.NextPrint / component.ActalPrintSpeed() * 60f)
			}));
		}
		if (this.Type.Equals("Pallet"))
		{
			ProductPallet component2 = base.GetComponent<ProductPallet>();
			Dictionary<uint, uint> dictionary = new Dictionary<uint, uint>();
			for (int i = 0; i < component2.Orders.Length; i++)
			{
				ProductPrintOrder productPrintOrder = component2.Orders[i];
				if (productPrintOrder != null)
				{
					for (int j = 0; j < productPrintOrder.Copies.Length; j++)
					{
						dictionary.AddUp(productPrintOrder.ProductIDs[j], productPrintOrder.Copies[j]);
					}
				}
			}
			foreach (KeyValuePair<uint, uint> keyValuePair in dictionary)
			{
				string text = ProductPrintOrder.NameFromID(keyValuePair.Key);
				if (text != null)
				{
					stringBuilder.AppendLine(text + " x " + keyValuePair.Value);
				}
			}
		}
		if (this.MaxQueue > 0)
		{
			int num = 0;
			for (int k = 0; k < this.InteractionPoints.Length; k++)
			{
				InteractionPoint interactionPoint = this.InteractionPoints[k];
				num += Mathf.Max(0, interactionPoint.CurrentQueue.Count - 1);
			}
			stringBuilder.AppendLine("FurnitureQueue".Loc(new object[]
			{
				num
			}));
		}
		return stringBuilder.ToString();
	}

	public override IEnumerable<Selectable> GetRelated()
	{
		if (base.gameObject != null)
		{
			if (this.Type.Equals("Server"))
			{
				Server serv = base.GetComponent<Server>();
				if (serv == null)
				{
					yield break;
				}
				if (serv.Rep != null && serv.Rep.furn != null)
				{
					yield return serv.Rep.furn;
					foreach (Server item in from x in serv.Rep.Children
					where x != null && x.gameObject != null && x.furn != null
					select x)
					{
						yield return item.furn;
					}
				}
			}
			else if (this.Table != null)
			{
				foreach (TableScript item2 in from x in this.Table.GetChildren()
				where x != null && x.gameObject != null && x.FurnComp != null
				select x)
				{
					yield return item2.FurnComp;
				}
			}
			else if (this.RoomTemperature.Count > 0)
			{
				foreach (Room item3 in from x in this.RoomTemperature
				where x != null && x.gameObject != null
				select x)
				{
					yield return item3;
				}
			}
			else if (this.OwnedBy != null)
			{
				yield return this.OwnedBy;
			}
		}
		yield break;
	}

	public bool IsConnecter
	{
		get
		{
			return "Elevator".Equals(this.Type) || "Stairs".Equals(this.Type);
		}
		set
		{
		}
	}

	public PathNode<Vector3> pathNode
	{
		get
		{
			return this._pathNode;
		}
		set
		{
			this._pathNode = value;
		}
	}

	public Furniture GetConnectedElevator(bool above)
	{
		Room roomFromPoint = GameSettings.Instance.sRoomManager.GetRoomFromPoint(base.transform.position + Vector3.up * ((!above) ? -2.5f : 2.5f));
		if (roomFromPoint != null)
		{
			Furniture furniture = roomFromPoint.GetFurniture("Elevator").FirstOrDefault((Furniture x) => x.OriginalPosition + ((!above) ? (Vector3.up * 2f) : (Vector3.down * 2f)) == this.OriginalPosition);
			if (furniture != null)
			{
				return furniture;
			}
		}
		return null;
	}

	public Vector3 GetOffsetPos(Room room, bool inverse = false)
	{
		if (this.Type.Equals("Elevator") && room != null)
		{
			if (inverse)
			{
				if (room == this.Parent)
				{
					return base.transform.position;
				}
				return base.transform.position + base.transform.rotation * Vector3.forward;
			}
			else
			{
				if (room == this.Parent)
				{
					return base.transform.position + base.transform.rotation * Vector3.forward;
				}
				Furniture connectedElevator = this.GetConnectedElevator(true);
				if (connectedElevator != null && room == connectedElevator.Parent)
				{
					return connectedElevator.transform.position;
				}
				connectedElevator = this.GetConnectedElevator(false);
				if (connectedElevator != null && room == connectedElevator.Parent)
				{
					return connectedElevator.transform.position;
				}
			}
		}
		if (this.OffsetPoints.Length > 0)
		{
			return (!(room == this.Parent)) ? this.OffsetPoints[(!inverse) ? 1 : 0].transform.position : this.OffsetPoints[(!inverse) ? 0 : 1].transform.position;
		}
		return base.transform.position;
	}

	private string UsedByInfo()
	{
		HashSet<string> hashSet = new HashSet<string>();
		foreach (InteractionPoint interactionPoint in this.InteractionPoints)
		{
			if (interactionPoint.UsedBy != null)
			{
				hashSet.Add(interactionPoint.UsedBy.employee.FullName);
			}
		}
		return Newspaper.MakeList(hashSet.ToArray<string>());
	}

	public override string Description()
	{
		return "Pieces of furniture";
	}

	public void InteractStart()
	{
		if (this.InteractAnimation)
		{
			Animation componentInChildren = base.GetComponentInChildren<Animation>();
			componentInChildren["InteractStart"].speed = GameSettings.GameSpeed;
			componentInChildren.Play("InteractStart");
		}
		if (this.InteractStartClip != null)
		{
			this.UpdateAudioState();
			this.AudioSrc.PlayOneShot(this.InteractStartClip);
		}
	}

	public bool CheckTwoFloorValid()
	{
		if (this.Parent != null && this.ExtraParent != null)
		{
			Furniture[] ignore = (from x in this.SnapPoints
			select x.UsedBy).Concate(this).ToArray<Furniture>();
			if (FurnitureBuilder.IsValid(this.FinalBoundary, this.Height1, this.Height2, this.Parent, this.WallFurn, ignore) && FurnitureBuilder.IsValid(this.FinalBoundary, this.Height1, this.Height2, this.ExtraParent, this.WallFurn, ignore))
			{
				return true;
			}
		}
		return false;
	}

	public bool UpdateParent(bool checkValid = true, bool tableScript = true)
	{
		this.RefreshEdgeDetection();
		if (this.Parent != null)
		{
			this.Parent.RefreshNoise();
		}
		if (this.WallFurn)
		{
			return true;
		}
		int floor = Mathf.FloorToInt(base.transform.position.y / 2f + 0.5f);
		if (tableScript && this.Table != null && this.Parent != null)
		{
			this.Parent.RemoveTable(this.Table);
		}
		if (this.TwoFloors && this.ExtraParent != null)
		{
			this.ExtraParent.RemoveFurniture(this);
			this.ExtraParent.DirtyNavMesh = true;
			this.ExtraParent.DirtyFloorMesh = true;
			this.ExtraParent = null;
		}
		Vector2 p = new Vector2(base.transform.position.x, base.transform.position.z);
		if (this.Parent != null)
		{
			floor = this.Parent.Floor;
			if (!this.Parent.IsInside(p))
			{
				if (this.TwoFloors)
				{
					this.Parent.DirtyRoofMesh = true;
				}
				this.Parent.RemoveFurniture(this);
				if ("Elevator".Equals(this.Type))
				{
					this.ClearElevatorConnections();
				}
				if (this.IsConnecter)
				{
					this.pathNode.GetConnections().Clear();
					this.Parent.DirtyPathNodes = true;
				}
				this.Parent = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Floor == floor && x.IsInside(p));
				this.CheckNoiseRefresh();
			}
		}
		else
		{
			if ("Elevator".Equals(this.Type))
			{
				this.ClearElevatorConnections();
			}
			if (this.IsConnecter)
			{
				this.pathNode.GetConnections().Clear();
			}
			this.Parent = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.Floor == floor && x.IsInside(p));
		}
		if (this.Parent == null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return false;
		}
		if (this.TwoFloors)
		{
			this.ExtraParent = FurnitureBuilder.GetBestRoom(floor + 1, p, this, null);
			if (!FurnitureBuilder.IsValid(this.FinalBoundary, this.Height1, this.Height2, this.ExtraParent, this.WallFurn, (from x in this.SnapPoints
			select x.UsedBy).Concate(this).ToArray<Furniture>()))
			{
				this.ExtraParent = null;
			}
			if (this.ExtraParent != null)
			{
				this.ExtraParent.AddFurniture(this);
				this.ExtraParent.DirtyNavMesh = true;
				this.ExtraParent.DirtyPathNodes = true;
				this.ExtraParent.DirtyFloorMesh = true;
			}
		}
		if (this.TwoFloors && this.ExtraParent == null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return false;
		}
		if (checkValid)
		{
			if (!this.IsSnapping || FurnitureBuilder.IsValid(this.FinalBoundary, this.Height1, this.Height2, this.Parent, this.WallFurn, new Furniture[]
			{
				this,
				this.SnappedTo.Parent
			}))
			{
				if (this.IsSnapping)
				{
					goto IL_410;
				}
				if (FurnitureBuilder.IsValid(this.FinalBoundary, this.Height1, this.Height2, this.Parent, this.WallFurn, (from x in this.SnapPoints
				select x.UsedBy).Concate(this).ToArray<Furniture>()))
				{
					goto IL_410;
				}
			}
			UnityEngine.Object.Destroy(base.gameObject);
			return false;
		}
		IL_410:
		if (this.TwoFloors)
		{
			this.Parent.DirtyRoofMesh = true;
		}
		this.Parent.AddFurniture(this);
		if (tableScript && this.Table != null)
		{
			this.Table.Init();
		}
		LampScript component = base.GetComponent<LampScript>();
		if (component != null)
		{
			component.CalcEdge(this.Parent.RoomBounds, base.transform.position);
		}
		this.Parent.DirtyNavMesh = true;
		this.CalcEdge();
		if (this.IsConnecter)
		{
			this.Parent.DirtyPathNodes = true;
		}
		if (this.Type.Equals("Elevator"))
		{
			this.UpdateElevatorConnections(null);
		}
		this.UpdateBoundsMesh();
		this.CleanFloor();
		this.Parent.RecalculateStateVariables(false);
		this.Parent.RefreshNoise();
		return true;
	}

	public void InteractEnd()
	{
		if (this.InteractAnimation)
		{
			Animation componentInChildren = base.GetComponentInChildren<Animation>();
			componentInChildren["InteractEnd"].speed = GameSettings.GameSpeed;
			componentInChildren.Play("InteractEnd");
		}
		if (this.InteractEndClip != null)
		{
			if (!this.IsOn && this.AudioSrc.isPlaying)
			{
				this.AudioSrc.Stop();
			}
			this.UpdateAudioState();
			this.AudioSrc.PlayOneShot(this.InteractEndClip);
		}
	}

	private void Awake()
	{
		this.HasUpg = (this.upg != null);
		if (this.InteractionPoints == null)
		{
			this.InteractionPoints = new InteractionPoint[0];
		}
		for (int i = 0; i < this.InteractionPoints.Length; i++)
		{
			this.InteractionPoints[i].Id = i;
		}
		if (this.SnapPoints == null)
		{
			this.SnapPoints = new SnapPoint[0];
		}
		for (int j = 0; j < this.SnapPoints.Length; j++)
		{
			this.SnapPoints[j].Id = j;
		}
		if (this.HoldablePoints == null)
		{
			this.HoldablePoints = new Transform[0];
		}
		for (int k = 0; k < this.HoldablePoints.Length; k++)
		{
			this.Holdables[this.HoldablePoints[k]] = null;
		}
	}

	public bool TestAvailable(string action)
	{
		for (int i = 0; i < this.InteractionPoints.Length; i++)
		{
			InteractionPoint interactionPoint = this.InteractionPoints[i];
			if (interactionPoint.UsedBy == null && interactionPoint.Name.Equals(action) && interactionPoint.Usable())
			{
				return true;
			}
		}
		return false;
	}

	public bool TestAvailable(bool usingCheck = true, string action = null)
	{
		bool result = false;
		for (int i = 0; i < this.InteractionPoints.Length; i++)
		{
			InteractionPoint interactionPoint = this.InteractionPoints[i];
			if ((action == null && interactionPoint.Usable()) || (action != null && action.Equals(interactionPoint.Name) && interactionPoint.Usable()))
			{
				result = true;
				if (usingCheck && interactionPoint.UsedBy != null)
				{
					return false;
				}
			}
		}
		return result;
	}

	private void Start()
	{
		if (this.TreeLeaves != null && GameSettings.Instance != null)
		{
			this.TreeLeaves.sharedMaterial = GameSettings.Instance.LeaveMat;
		}
		if (this.GenerateBoundaryOnStart)
		{
			this.GenerateBoundary();
			return;
		}
		if (!this.isTemporary)
		{
			if (this.Deserialized)
			{
				TimeProbe.BeginTime("Furniture init time:");
			}
			if (this.RandomSFX)
			{
				this._randomSFXTimer = UnityEngine.Random.Range(this.RandomSFXMin, this.RandomSFXMax);
			}
			if (this.TemperatureController)
			{
				this.TempWatt = this.Wattage;
				this.TempWater = this.Water;
				this.TempBreak = ((!this.HasUpg) ? 12f : this.upg.TimeToAtrophy);
			}
			if (this.Parent == null)
			{
				UnityEngine.Object.Destroy(base.gameObject);
				return;
			}
			this.Parent.AddFurniture(this);
			this.Parent.RefreshNoise();
			this.UpdateBoundaryPoints();
			base.name = base.name.Replace("(Clone)", string.Empty);
			this.Parent.RecalculateStateVariables(false);
			GameSettings.Instance.sRoomManager.AllFurniture.Add(this);
			if (this.IsConnecter)
			{
				this._pathNode = new PathNode<Vector3>(base.transform.position + Vector3.up * 0.5f, this);
			}
			if (this.Type.Equals("Elevator"))
			{
				this.UpdateElevatorConnections(null);
			}
			if (this.IsConnecter || (this.NavBoundary != null && this.NavBoundary.Length > 0))
			{
				this.Parent.DirtyPathNodes = true;
			}
			this.IsOn = this.AlwaysOn;
			if (!this.Deserialized && this.Table != null)
			{
				this.Table.Init();
			}
			LampScript component = base.GetComponent<LampScript>();
			if (component != null)
			{
				component.CalcEdge(this.Parent.RoomBounds, base.transform.position);
			}
			if (!this.Deserialized)
			{
				this.CleanFloor();
			}
			this.CalcEdge();
			this.UpdateBoundsMesh();
			if (this.Height1 < Actor.HumanHeight && this.FinalNav != null && this.FinalNav.Length > 0)
			{
				this.Parent.DirtyNavMesh = true;
			}
			this.CheckNoiseRefresh();
		}
		base.InitWritable();
		this.Children = (from x in base.GetComponentsInChildren<Renderer>(true)
		where !"HideUnaffected".Equals(x.tag) && x.GetComponent<ParticleSystem>() == null && x.GetComponent<Holdable>() == null
		select x).ToArray<Renderer>();
		this.AudioSrc = base.GetComponent<AudioSource>();
		if (this.AudioSrc != null)
		{
			this.HasAudioSource = true;
			this.MaxAudioDistance = this.AudioSrc.maxDistance * this.AudioSrc.maxDistance;
		}
		else
		{
			this.HasAudioSource = false;
		}
		if (!this.isTemporary && (this.HasUpg || this.HasAudioSource || this.HasConnectionBeam || this.DespawnHoldables))
		{
			GameSettings.Instance.FurnitureUpdateHandler.RegisterObject(this);
		}
		if ("Computer".Equals(this.Type) && GameSettings.Instance != null)
		{
			GameSettings.Instance.ComputerNoiseUpdateHandler.RegisterObject(this);
		}
		this._hasClip = (this.AudioSrc != null && this.AudioSrc.clip != null);
		List<string> list = new List<string>(this.actions);
		this.CanAssign = (this.CanAssign && this.InteractionPoints.Length > 0);
		if (this.CanAssign && !this.isTemporary)
		{
			list.Add("Unpair");
			HintController.Instance.Show(HintController.Hints.HintEmployeeAssign);
		}
		if (!this.Type.Equals("Elevator"))
		{
			list.Add("Move");
		}
		else if (!this.isTemporary)
		{
			HintController.Instance.Show(HintController.Hints.HintElevatorBeam);
		}
		if (ObjectDatabase.Instance.GetUpgrades(this.UpgradeTo).Count > 0)
		{
			list.Add("ReplaceFurn");
		}
		if (this.ColorPrimaryEnabled)
		{
			list.Add("Furniture color");
			list.Add("Default Style");
			list.Add("ResetDefaultStyle");
		}
		if (this.Type.Equals("Lamp") && !this.isTemporary)
		{
			HintController.Instance.Show(HintController.Hints.LampTestHint);
		}
		if (this.Type.Equals("Server"))
		{
			list.Add("ConnectServers");
		}
		list.Add("Duplicate");
		this.actions = list.ToArray();
		if (!this.DisableInitColor && !this.Deserialized && GameSettings.Instance != null)
		{
			this._colorPrimary = GameSettings.Instance.GetDefaultColor(this.DefaultColorGroup + "Primary", this.ColorPrimaryDefault);
			this._colorSecondary = GameSettings.Instance.GetDefaultColor(this.DefaultColorGroup + "Secondary", this.ColorSecondaryDefault);
			this._colorTertiary = GameSettings.Instance.GetDefaultColor(this.DefaultColorGroup + "Tertiary", this.ColorTertiaryDefault);
			if (this.Colorable.Count > 0)
			{
				this.UpdateMaterials();
			}
		}
		if (this.isTemporary)
		{
			this.ForceTemporary();
		}
		if (this.ForceColorSecondary)
		{
			this.ColorSecondary = this.ColorSecondaryDefault;
		}
		if (this.ForceColorTertiary)
		{
			this.ColorTertiary = this.ColorTertiaryDefault;
		}
		if (!this.isTemporary && (this.Type.Equals("Radiator") || this.Type.Equals("Ventilation")))
		{
			GameSettings.Instance.sRoomManager.TemperatureControlDirty = true;
		}
		if (!this.Deserialized)
		{
			Vector3 position = base.transform.position;
			if (this.WallFurn)
			{
				base.transform.position = base.transform.position + base.transform.rotation * Vector3.forward * 0.2f;
			}
			else
			{
				base.transform.position = base.transform.position + Vector3.up * 0.2f;
			}
			if (GameSettings.Instance != null && GameSettings.Instance.EditMode)
			{
				this.PlacedInEditMode = true;
			}
			ShortcutExtensions.DOMove(base.transform, position, 0.2f, false);
		}
		this.RefreshEdgeDetection();
		if (this.Deserialized)
		{
			TimeProbe.EndTime("Furniture init time:");
		}
		this.CheckAssignMat();
	}

	public override void PostDeserialize()
	{
		if (this.SerializedQueue != null)
		{
			for (int i = 0; i < this.SerializedQueue.Length; i++)
			{
				uint[] source = this.SerializedQueue[i];
				foreach (Actor actor in from x in source.Skip(1)
				select (Actor)base.GetDeserializedObject(x))
				{
					if (actor != null)
					{
						actor.InQueue[this.Type] = this.InteractionPoints[i];
						this.InteractionPoints[i].CurrentQueue.Add(actor);
					}
				}
			}
		}
		Server component = base.GetComponent<Server>();
		if (component != null)
		{
			component.PostDeserialize();
		}
		TableScript component2 = base.GetComponent<TableScript>();
		if (component2 != null)
		{
			component2.PostDeserialize();
		}
		ReceptionDesk component3 = base.GetComponent<ReceptionDesk>();
		if (component3 != null && component3.QueueSave != null)
		{
			component3.Queue = (from x in component3.QueueSave
			select base.GetDeserializedObject(x) as Actor into x
			where x != null
			select x).ToList<Actor>();
		}
	}

	public void ForceTemporary()
	{
		this.ColorPrimary = this.ColorPrimaryDefault;
		this.ColorSecondary = this.ColorSecondaryDefault;
		this.ColorTertiary = this.ColorTertiaryDefault;
		this.IsOn = true;
	}

	private void UpdateElevatorConnections(Furniture from = null)
	{
		this.ClearElevatorConnections();
		Furniture connectedElevator = this.GetConnectedElevator(true);
		if (connectedElevator != null && connectedElevator.pathNode != null)
		{
			if (connectedElevator != from)
			{
				connectedElevator.UpdateElevatorConnections(this);
			}
			this.pathNode.AddConnection(connectedElevator.pathNode);
		}
		connectedElevator = this.GetConnectedElevator(false);
		if (connectedElevator != null && connectedElevator.pathNode != null)
		{
			if (connectedElevator != from)
			{
				connectedElevator.UpdateElevatorConnections(this);
			}
			this.pathNode.AddConnection(connectedElevator.pathNode);
		}
		if (this.HasConnectionBeam)
		{
			this.ConnectionBeam.UpdateBeam(this, true);
		}
	}

	public bool UsableForTableGroup()
	{
		for (int i = 0; i < this.SnapPoints.Length; i++)
		{
			SnapPoint snapPoint = this.SnapPoints[i];
			if (snapPoint.Name.Equals("OnTable") && snapPoint.UsedBy != null && snapPoint.UsedBy.DisableTableGrouping)
			{
				return false;
			}
		}
		return true;
	}

	private void UpdateAudioState()
	{
		this.AudioSrc.outputAudioMixerGroup = ((!(object.ReferenceEquals(GameSettings.Instance.sRoomManager.CameraRoom, this.Parent) ^ this.ReverseLowPass)) ? AudioManager.InGameHighPass : AudioManager.InGameNormal);
	}

	public void UpdateNow(float delta)
	{
		if (this.isTemporary)
		{
			return;
		}
		if (this.HasConnectionBeam)
		{
			bool flag = base.IsSelected && HUD.Instance.BuildMode;
			this.ConnectionBeam.gameObject.SetActive(flag);
			if (flag)
			{
				this.ConnectionBeam.UpdateBeam(this, false);
			}
		}
		if (this.HasUpg)
		{
			this.upg.UpdateMe();
		}
		if (this.DespawnHoldables)
		{
			bool flag2 = false;
			for (int i = 0; i < this.HoldablePoints.Length; i++)
			{
				Transform key = this.HoldablePoints[i];
				Holdable holdable = this.Holdables[key];
				if (!(holdable != null))
				{
					break;
				}
				if (Utilities.GetHours(holdable.Spawned, SDateTime.Now()) > this.DespawnHour)
				{
					UnityEngine.Object.Destroy(holdable.gameObject);
					this.Holdables[key] = null;
					flag2 = true;
				}
			}
			if (flag2)
			{
				this.UpdateHoldableStatus();
				if (this.HasHoldables > 0)
				{
					int num = this.Holdables.Count;
					for (int j = 0; j < num; j++)
					{
						if (this.Holdables[this.HoldablePoints[j]] == null)
						{
							for (int k = num - 1; k > j; k--)
							{
								Holdable holdable2 = this.Holdables[this.HoldablePoints[k]];
								if (holdable2 != null)
								{
									holdable2.transform.SetParent(this.HoldablePoints[j]);
									holdable2.transform.localPosition = Vector3.zero;
									holdable2.transform.localRotation = Quaternion.identity;
									this.Holdables[this.HoldablePoints[j]] = holdable2;
									this.Holdables[this.HoldablePoints[k]] = null;
									num--;
									break;
								}
								num--;
							}
						}
					}
				}
			}
		}
		if (this.HoldablePoints.Length > 0 && this.Parent != null)
		{
			for (int l = 0; l < this.HoldablePoints.Length; l++)
			{
				Holdable holdable3 = this.Holdables[this.HoldablePoints[l]];
				if (holdable3 != null)
				{
					for (int m = 0; m < holdable3.Renderers.Length; m++)
					{
						holdable3.Renderers[m].enabled = (GameSettings.Instance.ActiveFloor == this.Parent.Floor);
					}
				}
			}
		}
		if (this.HasAudioSource)
		{
			if (this.RandomSFX && GameSettings.Instance.sRoomManager.CameraRoom == this.Parent && GameSettings.GameSpeed > 0f && this.IsOn)
			{
				this.UpdateAudioState();
				this._randomSFXTimer -= delta;
				if (this._randomSFXTimer <= 0f)
				{
					this._randomSFXTimer = UnityEngine.Random.Range(this.RandomSFXMin, this.RandomSFXMax);
					if ((base.transform.position - CameraScript.Instance.Listener.transform.position).sqrMagnitude <= this.MaxAudioDistance)
					{
						this.AudioSrc.PlayOneShot(this.SFXFiles.GetRandom<AudioClip>());
					}
				}
			}
			if (this.AudioSrc.isPlaying)
			{
				this.UpdateAudioState();
			}
			if (this._hasClip)
			{
				if (GameSettings.GameSpeed > 0f && this.IsOn && (this.Parent.Floor == GameSettings.Instance.ActiveFloor || (this.Parent.Floor <= GameSettings.Instance.ActiveFloor && this.ReverseLowPass && object.ReferenceEquals(GameSettings.Instance.sRoomManager.CameraRoom, GameSettings.Instance.sRoomManager.Outside))))
				{
					if (!this.AudioSrc.isPlaying)
					{
						if (AudioManager.MaxChannels / 2 > AudioManager.FurniturePlaying && (base.transform.position - CameraScript.Instance.Listener.transform.position).sqrMagnitude <= this.MaxAudioDistance)
						{
							this.AudioSrc.Play();
							AudioManager.FurniturePlaying++;
						}
					}
					else if ((base.transform.position - CameraScript.Instance.Listener.transform.position).sqrMagnitude > this.MaxAudioDistance)
					{
						this.AudioSrc.Stop();
						AudioManager.FurniturePlaying--;
					}
				}
				else if (this.AudioSrc.isPlaying)
				{
					this.AudioSrc.Stop();
					AudioManager.FurniturePlaying--;
				}
			}
		}
	}

	public void UpdateNow2(float delta)
	{
		this.ActorNoise = Furniture.RecalculateNoise(this.OriginalPosition.FlattenVector3(), true, this.Parent, this, null, false, false);
		this.RefreshFinalNoiseValue();
	}

	public bool NeedUpdate(bool firstFunction)
	{
		return firstFunction || this.InteractionPoints[0].UsedBy != null;
	}

	public void RefreshEdgeDetection()
	{
		if (this.Parent == null || GameSettings.Instance == null || this.Children == null || this.Children.Length == 0 || CameraScript.Instance == null)
		{
			return;
		}
		if (this.TwoFloors)
		{
			this.UpperFloorFrame.enabled = (CameraScript.Instance.FlyMode || this.Parent.Floor + 1 == GameSettings.Instance.ActiveFloor);
		}
		bool flag = this.Height1 > 1.6f;
		bool flag2 = CameraScript.Instance.FlyMode || ((!flag || !GameSettings.Instance.HideCeilingFurniture) && (!this.WallFurn || this.CheckWallDown()) && (this.Parent.Floor == GameSettings.Instance.ActiveFloor || (this.TwoFloors && this.Parent.Floor + 1 == GameSettings.Instance.ActiveFloor) || (GameSettings.Instance.ActiveFloor > -1 && this.Parent.Floor > -1 && this.Parent.Floor < GameSettings.Instance.ActiveFloor && (this.PokesThroughWall || this.IsHighlight || this.IsSecondary || this.Parent.Outdoors || this.EdgeDetection()))));
		flag2 &= (!GameSettings.Instance.RentMode || GameSettings.Instance.EditMode || this.Parent.PlayerOwned || !this.Parent.Rentable);
		if (this.Children[0].enabled != flag2)
		{
			for (int i = 0; i < this.Children.Length; i++)
			{
				this.Children[i].enabled = flag2;
			}
		}
	}

	private bool CheckWallDown()
	{
		if (GameSettings.WallsDown == GameSettings.WallState.Low || GameSettings.WallsDown == GameSettings.WallState.LowNoSeg)
		{
			return false;
		}
		if (GameSettings.WallsDown == GameSettings.WallState.Back)
		{
			float f = Quaternion.Angle(base.transform.rotation, CameraScript.Instance.mainCam.transform.rotation);
			return Mathf.Abs(f) > 90f;
		}
		return true;
	}

	private void OnDestroy()
	{
		this.Reserved = null;
		if (GameSettings.IsQuitting)
		{
			return;
		}
		if (this.isTemporary)
		{
			return;
		}
		if (this.OwnedBy != null)
		{
			this.OwnedBy.Owns.Remove(this);
		}
		if (SelectorController.Instance != null && SelectorController.Instance.Selected.Contains(this))
		{
			SelectorController.Instance.Selected.Remove(this);
			SelectorController.Instance.DoPostSelectChecks();
		}
		if (GameSettings.Instance == null)
		{
			return;
		}
		if (this.AudioSrc != null && this.AudioSrc.isPlaying)
		{
			AudioManager.FurniturePlaying--;
		}
		GameSettings.Instance.FurnitureUpdateHandler.UnregisterObject(this);
		if ("Computer".Equals(this.Type))
		{
			GameSettings.Instance.ComputerNoiseUpdateHandler.UnregisterObject(this);
		}
		GameSettings.Instance.sActorManager.Actors.ForEach(delegate(Actor x)
		{
			x.Owns.Remove(this);
		});
		GameSettings.Instance.sActorManager.Staff.ForEach(delegate(Actor x)
		{
			x.Owns.Remove(this);
		});
		GameSettings.Instance.sRoomManager.AllFurniture.Remove(this);
		if ("Radiator".Equals(this.Type) || "Ventilation".Equals(this.Type))
		{
			GameSettings.Instance.sRoomManager.TemperatureControlDirty = true;
		}
		this.SymbolicDestroy();
		MaterialBank.Instance.RemoveFurnitureMat(this, this.ColorPrimary, this.ColorSecondary, this.ColorTertiary);
		foreach (SnapPoint snapPoint in this.SnapPoints)
		{
			if (snapPoint.UsedBy != null)
			{
				UnityEngine.Object.Destroy(snapPoint.UsedBy.gameObject);
			}
		}
		if (!this.Undo)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(this.GetSellPrice(), Company.TransactionCategory.Construction, "Recycle");
		}
	}

	public float GetSellPrice()
	{
		return this.Cost / 2f * (this.HasUpg ? this.upg.Quality : 1f);
	}

	public void RefreshNoiseReduction()
	{
	}

	private void ClearElevatorConnections()
	{
		if (this.pathNode != null)
		{
			foreach (PathNode<Vector3> pathNode in this.pathNode.GetConnections().ToList<PathNode<Vector3>>())
			{
				if (pathNode.Tag is IRoomConnector)
				{
					IRoomConnector roomConnector = (IRoomConnector)pathNode.Tag;
					if (roomConnector.pathNode != null)
					{
						roomConnector.pathNode.RemoveConnection(this.pathNode);
					}
					this.pathNode.RemoveConnection(pathNode);
				}
			}
		}
	}

	public void CheckNoiseRefresh()
	{
	}

	public void SymbolicDestroy()
	{
		base.ClearConnections();
		for (int i = 0; i < this.InteractionPoints.Length; i++)
		{
			this.InteractionPoints[i].ClearQueue();
		}
		if (HUD.Instance != null)
		{
			HUD.Instance.UnreachableFuniture.Remove(this);
			HUD.Instance.BlockedDoorways.Remove(this);
			HUD.Instance.NoChairPC.Remove(this);
		}
		foreach (Room room in from x in GameSettings.Instance.sRoomManager.Rooms
		where x != null && x.gameObject != null
		select x)
		{
			if (room.RemoveFurniture(this))
			{
				room.RefreshNoise();
			}
		}
		if (this.Parent != null)
		{
			this.CheckNoiseRefresh();
			if (this.Table != null)
			{
				this.Parent.RemoveTable(this.Table);
			}
			if (this.IsConnecter && this.pathNode != null)
			{
				if (this.Type.Equals("Elevator"))
				{
					this.ClearElevatorConnections();
				}
				this.pathNode.GetConnections().Clear();
			}
			if (this.Height1 < Actor.HumanHeight && this.FinalNav != null && this.FinalNav.Length > 0)
			{
				this.Parent.DirtyNavMesh = true;
			}
			this.Parent.DirtyPathNodes = true;
			this.Parent.RecalculateStateVariables(false);
			if (this.TwoFloors)
			{
				this.Parent.DirtyRoofMesh = true;
			}
		}
		else if (this.IsConnecter && this.pathNode != null)
		{
			if (this.Type.Equals("Elevator"))
			{
				this.ClearElevatorConnections();
			}
			this.pathNode.GetConnections().Clear();
		}
		if (this.TwoFloors && this.ExtraParent != null)
		{
			this.ExtraParent.RemoveFurniture(this);
			this.ExtraParent.DirtyNavMesh = true;
			this.ExtraParent.DirtyPathNodes = true;
			this.ExtraParent.DirtyFloorMesh = true;
			this.ExtraParent = null;
		}
		if (this.InteractionPoints != null)
		{
			InteractionPoint[] interactionPoints = this.InteractionPoints;
			for (int j = 0; j < interactionPoints.Length; j++)
			{
				InteractionPoint ip = interactionPoints[j];
				if (ip != null && ip.UsedBy != null)
				{
					ip.UsedBy.ResetState();
					Furniture computer = this.GetComputer();
					if (ip.UsedBy != null && (ip.UsedBy.UsingPoint == ip || (computer != null && computer.InteractionPoints.Any((InteractionPoint x) => x.UsedBy == ip.UsedBy))))
					{
						ip.UsedBy.UsingPoint = null;
					}
				}
			}
		}
		if (this.SnappedTo != null && this.SnappedTo.UsedBy == this)
		{
			this.SnappedTo.UsedBy = null;
			if (this.SnapsTo.Equals("OnTable") && this.SnappedTo.Parent != null && this.SnappedTo.Parent.Parent != null)
			{
				this.SnappedTo.Parent.Parent.RecalculateTableGroups();
			}
		}
	}

	private bool EdgeDetection()
	{
		if (CameraScript.Instance.FlyMode)
		{
			return true;
		}
		if (Options.OpaqueGlass)
		{
			return false;
		}
		if (this.AtEdge > 9f)
		{
			return false;
		}
		float value = (CameraScript.Instance.transform.rotation.eulerAngles.x - 8f) / 50f;
		return this.AtEdge < Mathf.Lerp(9f, 1f, Mathf.Clamp01(value));
	}

	public void CalcEdge()
	{
		if (this.Parent == null)
		{
			return;
		}
		this.AtEdge = float.MaxValue;
		Vector2 vector = new Vector2(base.transform.position.x, base.transform.position.z);
		List<WallEdge> edges = this.Parent.Edges;
		if (edges == null)
		{
			return;
		}
		for (int i = 0; i < edges.Count; i++)
		{
			int index = (i + 1) % edges.Count;
			if (edges[i].HasWindows(edges[index]))
			{
				Vector2? vector2 = Utilities.ProjectToLine(vector, edges[i].Pos, edges[index].Pos);
				if (vector2 != null)
				{
					float sqrMagnitude = (vector2.Value - vector).sqrMagnitude;
					if (sqrMagnitude < this.AtEdge)
					{
						this.AtEdge = sqrMagnitude;
					}
				}
			}
		}
	}

	public void CleanFloor()
	{
		if (this.Height1 < Actor.HumanHeight && this.FinalNav != null && this.FinalNav.Length > 0)
		{
			List<Room.Dirt> list = this.Parent.QueryDirtQuad(this.FinalNav.GetBounds().Expand(0.5f, 0.5f)).ToList<Room.Dirt>();
			for (int i = 0; i < list.Count; i++)
			{
				Room.Dirt dirt = list[i];
				if (Utilities.IsInside(dirt.Pos, this.FinalNav))
				{
					this.Parent.RemoveDirt(dirt.Index);
				}
			}
			this.Parent.UpdateDirtScore(false);
		}
	}

	public void NotifySnapPoint(SnapPoint point, Furniture furn, bool hasSnapped)
	{
		furn.UpdateComputerChair(hasSnapped);
		if (this.Type.Equals("Computer") && point.Name.Equals("PCAddon"))
		{
			if (this.ComputerTransform != null)
			{
				this.ComputerTransform.localPosition = ((!hasSnapped) ? this.OriginalOffset : this.PCAddonOffset);
				this.ComputerTransform.localRotation = Quaternion.Euler((!hasSnapped) ? this.OriginalRotation : this.PCAddonRotation);
			}
			for (int i = 0; i < this.RoleBuffs.Length; i++)
			{
				this.RoleBuffs[i] = 0f;
			}
			foreach (SnapPoint snapPoint in from x in this.SnapPoints
			where x.UsedBy != null && x.Name.Equals("PCAddon")
			select x)
			{
				for (int j = 0; j < Mathf.Min(this.RoleBuffs.Length, snapPoint.UsedBy.RoleBuffs.Length); j++)
				{
					this.RoleBuffs[j] += snapPoint.UsedBy.RoleBuffs[j];
				}
			}
		}
	}

	public float GetEffectivenessValue(Employee.EmployeeRole role)
	{
		if (this.upg.Quality < 0.25f)
		{
			HUD.Instance.AddPopupMessage("ComputerWarning".Loc(), "Info", PopupManager.PopUpAction.GotoFurn, this.DID, PopupManager.NotificationSound.Issue, 0.75f, PopupManager.PopupIDs.PCWarning, 1);
		}
		return Mathf.Clamp01(this.upg.Quality * 2f) * this.ComputerPower * (1f + this.RoleBuffs[(int)role]);
	}

	public int GetSnappingDepth()
	{
		int num = 0;
		SnapPoint snappedTo = this.SnappedTo;
		while (snappedTo != null)
		{
			num++;
			snappedTo = snappedTo.Parent.SnappedTo;
		}
		return num;
	}

	public void LogLoadError(string error)
	{
		if (GameSettings.Instance.FurnitureErrorOccured)
		{
			Debug.Log(error);
		}
		else
		{
			Debug.LogException(new Exception(error));
		}
	}

	public Vector2 GetMeanPos()
	{
		return this.GetMeanPos(base.transform.localToWorldMatrix);
	}

	public Vector2 GetMeanPos(Matrix4x4 trans)
	{
		List<Vector2> list = this.CalculateBoundary();
		if (list != null && list.Count > 0)
		{
			float num = 0f;
			float num2 = 0f;
			for (int i = 0; i < list.Count; i++)
			{
				Vector3 vector = trans.MultiplyPoint(list[i].ToVector3(0f));
				num += vector.x;
				num2 += vector.z;
			}
			return new Vector2(num / (float)list.Count, num2 / (float)list.Count);
		}
		return this.OriginalPosition.FlattenVector3();
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		base.transform.position = (SVector3)dictionary["Pos"];
		this.OriginalPosition = base.transform.position;
		base.transform.rotation = (SVector3)dictionary["Rot"];
		this.RotationOffset = dictionary.Get<float>("RotationOffset", 0f);
		this.Parent = GameSettings.Instance.sRoomManager.GetRoomFromPoint(base.transform.position);
		this.PlacedInEditMode = dictionary.Get<bool>("PlacedInEditMode", false);
		uint num = dictionary.Get<uint>("Parent", 0u);
		if (this.Parent == null || this.Parent.DID != num)
		{
			Vector2 meanPos = this.GetMeanPos();
			this.Parent = GameSettings.Instance.sRoomManager.GetRoomFromPoint(meanPos.ToVector3(this.OriginalPosition.y));
		}
		if ((this.Parent == null || this.Parent == GameSettings.Instance.sRoomManager.Outside) && !this.WallFurn && !this.IsSnapping)
		{
			if (this.Parent == GameSettings.Instance.sRoomManager.Outside)
			{
				this.LogLoadError("Parent was outside for furniture: " + base.name);
			}
			else
			{
				this.LogLoadError("Parent was null for furniture: " + base.name);
			}
			this.Parent = null;
			this.isTemporary = true;
			base.gameObject.SetActive(false);
			UnityEngine.Object.Destroy(base.gameObject);
			return null;
		}
		if (this.IsSnapping)
		{
			uint pSnapID = dictionary.Get<uint>("SnapPoint", 0u);
			Furniture furniture = base.GetDeserializedObject(pSnapID) as Furniture;
			if (furniture == null && pSnapID > 0u)
			{
				furniture = GameSettings.Instance.sRoomManager.AllFurniture.FirstOrDefault((Furniture x) => x.DID == pSnapID);
			}
			if (!(furniture != null))
			{
				this.LogLoadError("Snap was null for furniture: " + base.name);
				this.Parent = null;
				this.isTemporary = true;
				base.gameObject.SetActive(false);
				UnityEngine.Object.Destroy(base.gameObject);
				return null;
			}
			SnapPoint snapPoint = furniture.SnapPoints[dictionary.Get<int>("SnapId", 0)];
			this.SnappedTo = snapPoint;
			Vector3 realPos = this.SnappedTo.GetRealPos();
			base.transform.position = realPos;
			this.OriginalPosition = realPos;
			snapPoint.UsedBy = this;
			this.Parent = furniture.Parent;
		}
		if (this.WallFurn)
		{
			base.DeserializeSnap(dictionary);
			if (this.FirstEdge == null)
			{
				this.LogLoadError("First wallEdge was null for furniture: " + base.name);
				this.Parent = null;
				this.isTemporary = true;
				base.gameObject.SetActive(false);
				UnityEngine.Object.Destroy(base.gameObject);
				return null;
			}
			WallEdge otherEdge = this.WallPosition.Keys.FirstOrDefault((WallEdge x) => x != this.FirstEdge);
			if (otherEdge == null)
			{
				this.LogLoadError("Other wallEdge was null for furniture: " + base.name);
				this.Parent = null;
				this.isTemporary = true;
				base.gameObject.SetActive(false);
				UnityEngine.Object.Destroy(base.gameObject);
				return null;
			}
			this.Parent = (from x in this.FirstEdge.Links
			where x.Value == otherEdge
			select x.Key).FirstOrDefault<Room>();
			if (this.Parent == null)
			{
				this.LogLoadError("Parent was null for furniture: " + base.name);
				this.isTemporary = true;
				base.gameObject.SetActive(false);
				UnityEngine.Object.Destroy(base.gameObject);
				return null;
			}
		}
		if (this.Parent == null)
		{
			this.LogLoadError("Parent was null on double check for furniture: " + base.name);
			this.isTemporary = true;
			base.gameObject.SetActive(false);
			UnityEngine.Object.Destroy(base.gameObject);
			return null;
		}
		this.Parent.AddFurniture(this);
		Upgradable component = base.GetComponent<Upgradable>();
		if (component != null)
		{
			component.Deserialize(dictionary);
		}
		this.IsOn = (bool)dictionary["isOn"];
		this.ColorPrimary = dictionary.Get<SVector3>("ColP", this.ColorPrimaryDefault).ToColor();
		this.ColorSecondary = dictionary.Get<SVector3>("ColS", this.ColorSecondaryDefault).ToColor();
		this.ColorTertiary = dictionary.Get<SVector3>("ColT", this.ColorTertiaryDefault).ToColor();
		Server component2 = base.GetComponent<Server>();
		if (component2 != null)
		{
			WriteDictionary writeDictionary = dictionary.Get<WriteDictionary>("Server", null);
			if (writeDictionary != null)
			{
				component2.DeserializeThis(writeDictionary);
			}
		}
		this.CalcEdge();
		ReceptionDesk component3 = base.GetComponent<ReceptionDesk>();
		if (component3 != null)
		{
			component3.QueueSave = dictionary.Get<uint[]>("ReceptionQueue", new uint[0]);
		}
		this.SerializedQueue = dictionary.Get<uint[][]>("CurrentQueue2", new uint[0][]);
		if (this.Table != null)
		{
			this.Table.DeserializeState(dictionary);
		}
		this.LastActivity = dictionary.Get<SDateTime>("LastActivity", new SDateTime(0, 0));
		uint pId = dictionary.Get<uint>("ExtraParent", 0u);
		this.ExtraParent = (base.GetDeserializedObject(pId) as Room);
		if (this.TwoFloors && this.ExtraParent == null)
		{
			if (pId > 0u)
			{
				this.ExtraParent = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == pId);
			}
			else
			{
				this.ExtraParent = FurnitureBuilder.GetBestRoom(this.Parent.Floor + 1, this.OriginalPosition.FlattenVector3(), this, null);
			}
			if (this.ExtraParent == null)
			{
				this.LogLoadError("Extra parent was null for furniture: " + base.name);
				this.ExtraParent = null;
				this.isTemporary = true;
				base.gameObject.SetActive(false);
				UnityEngine.Object.Destroy(base.gameObject);
				return null;
			}
		}
		if (this.ExtraParent != null)
		{
			this.ExtraParent.AddFurniture(this);
		}
		string[] array = dictionary.Get<string[]>("Holdables", new string[0]);
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i] != null)
			{
				this.PlaceHoldable(ItemDispenser.Instance.Dispense(array[i]).GetComponent<Holdable>());
			}
		}
		this.UpdateHoldableStatus();
		Writeable component4 = base.GetComponent<ProductPrinter>();
		if (component4 != null)
		{
			component4.DeserializeThis(dictionary);
		}
		component4 = base.GetComponent<ProductPallet>();
		if (component4 != null)
		{
			component4.DeserializeThis(dictionary);
		}
		return this;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["Pos"] = this.OriginalPosition;
		dictionary["Rot"] = base.transform.rotation;
		dictionary["Type"] = base.name;
		dictionary["Parent"] = ((!(this.Parent != null)) ? 0u : this.Parent.DID);
		dictionary["RotationOffset"] = this.RotationOffset;
		dictionary["PlacedInEditMode"] = this.PlacedInEditMode;
		if (this.IsSnapping && this.SnappedTo != null)
		{
			Furniture component = this.SnappedTo.Parent.GetComponent<Furniture>();
			if (component != null)
			{
				dictionary["SnapPoint"] = component.DID;
				dictionary["SnapId"] = this.SnappedTo.Id;
			}
		}
		if (this.WallFurn)
		{
			base.SerializeSnap(dictionary);
		}
		dictionary["isOn"] = this._isOn;
		dictionary["ColP"] = this.ColorPrimary;
		dictionary["ColS"] = this.ColorSecondary;
		dictionary["ColT"] = this.ColorTertiary;
		Upgradable component2 = base.GetComponent<Upgradable>();
		if (component2 != null)
		{
			component2.Serialize(dictionary);
		}
		if (this.Table != null)
		{
			this.Table.SerializeState(dictionary);
		}
		Server component3 = base.GetComponent<Server>();
		if (component3 != null)
		{
			dictionary["Server"] = component3.SerializeThis(mode);
		}
		if (mode == GameReader.LoadMode.Full)
		{
			dictionary["CurrentQueue2"] = this.InteractionPoints.SelectInPlace((InteractionPoint x) => (from z in x.CurrentQueue
			where z != null
			select z.DID).ToArray<uint>());
			string key = "Holdables";
			object value;
			if (this.Holdables != null)
			{
				value = (from x in this.Holdables.Values
				select (!(x == null)) ? x.name : null).ToArray<string>();
			}
			else
			{
				value = new string[0];
			}
			dictionary[key] = value;
			ReceptionDesk component4 = base.GetComponent<ReceptionDesk>();
			if (component4 != null)
			{
				dictionary["ReceptionQueue"] = (from x in component4.Queue
				where x != null
				select x.DID).ToArray<uint>();
			}
			Writeable component5 = base.GetComponent<ProductPrinter>();
			if (component5 != null)
			{
				component5.SerializeThis(dictionary, mode);
			}
			component5 = base.GetComponent<ProductPallet>();
			if (component5 != null)
			{
				component5.SerializeThis(dictionary, mode);
			}
		}
		dictionary["LastActivity"] = this.LastActivity;
		dictionary["ExtraParent"] = ((!(this.ExtraParent == null)) ? this.ExtraParent.DID : 0u);
	}

	public override string WriteName()
	{
		return "Furniture";
	}

	public void UpdateComputerChair(bool hasSnapped)
	{
		if (HUD.Instance == null)
		{
			return;
		}
		if (this.SnappedTo != null)
		{
			if ("OnTable".Equals(this.SnappedTo.Name) && hasSnapped)
			{
				if (this.NeedsChair)
				{
					Furniture computerChair = this.ComputerChair;
					this.ComputerChair = null;
					if (computerChair != null && computerChair.OwnedBy == this.OwnedBy)
					{
						if (this.OwnedBy != null)
						{
							this.OwnedBy.Owns.Remove(computerChair);
						}
						computerChair.OwnedBy = null;
					}
					foreach (SnapPoint snapPoint in this.SnappedTo.Links)
					{
						Furniture usedBy = snapPoint.UsedBy;
						if (usedBy != null && "Chair".Equals(usedBy.Type))
						{
							float num = Quaternion.LookRotation(usedBy.transform.position - base.transform.position).eulerAngles.y;
							num = Mathf.DeltaAngle(base.transform.rotation.eulerAngles.y, num);
							if (Mathf.Abs(num) < 5f)
							{
								this.ComputerChair = usedBy;
								if (this.ComputerChair != null && this.ComputerChair.OwnedBy != this.OwnedBy)
								{
									if (this.ComputerChair.OwnedBy != null)
									{
										this.ComputerChair.OwnedBy.Owns.Remove(this.ComputerChair);
									}
									this.ComputerChair.OwnedBy = this.OwnedBy;
									if (this.OwnedBy != null)
									{
										this.OwnedBy.Owns.Add(this.ComputerChair);
									}
								}
								for (int i = 0; i < this.InteractionPoints.Length; i++)
								{
									InteractionPoint interactionPoint = this.InteractionPoints[i];
									if (interactionPoint.Name.Equals("Use") || interactionPoint.Name.Equals("Repair"))
									{
										InteractionPoint interactionPoint2 = this.ComputerChair.GetInteractionPoint("Use", true);
										if (interactionPoint2 != null)
										{
											interactionPoint.transform.position = interactionPoint2.transform.position.ReplaceY(interactionPoint.transform.position.y);
											interactionPoint.transform.rotation = interactionPoint2.transform.rotation;
										}
									}
								}
								HUD.Instance.NoChairPC.Remove(this);
								return;
							}
						}
					}
					HUD.Instance.NoChairPC.Add(this);
				}
			}
			else if ("AtTable".Equals(this.SnappedTo.Name))
			{
				for (int j = 0; j < this.SnappedTo.Parent.SnapPoints.Length; j++)
				{
					SnapPoint snapPoint2 = this.SnappedTo.Parent.SnapPoints[j];
					if ("OnTable".Equals(snapPoint2.Name) && snapPoint2.UsedBy != null)
					{
						snapPoint2.UsedBy.UpdateComputerChair(true);
					}
				}
			}
		}
	}

	public bool UpgradeCompatible(string group)
	{
		return (!string.IsNullOrEmpty(this.UpgradeFrom) && this.UpgradeFrom.Equals(group)) || (!string.IsNullOrEmpty(this.UpgradeTo) && this.UpgradeTo.Equals(group));
	}

	public Furniture GetComputer()
	{
		if (this.SnappedTo == null || !this.Type.Equals("Chair"))
		{
			return null;
		}
		foreach (SnapPoint snapPoint in this.SnappedTo.Links)
		{
			Furniture usedBy = snapPoint.UsedBy;
			if (usedBy != null && usedBy.Type.Equals("Computer") && usedBy.ComputerChair == this)
			{
				return usedBy;
			}
		}
		return null;
	}

	public float GetComfort()
	{
		if (this.Type.Equals("Computer"))
		{
			return (!(this.ComputerChair != null)) ? 1f : this.ComputerChair.GetComfort();
		}
		return (!this.HasUpg) ? this.Comfort : (Furniture.ComfortDegration.Evaluate(1f - this.upg.Quality) * this.Comfort);
	}

	public void UpdateBoundsMesh()
	{
		if (this.WallFurn)
		{
			return;
		}
		Vector2[] expanded = this.Parent.GetExpanded(Room.WallOffset / 2f);
		Vector2 a = Vector2.zero;
		Vector2 one = Vector2.one;
		base.transform.position = this.OriginalPosition;
		base.transform.localScale = Vector3.one;
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		for (int i = 0; i < this.MeshBoundary.Length; i++)
		{
			num = Mathf.Min(num, this.MeshBoundary[i].x);
			num2 = Mathf.Max(num2, this.MeshBoundary[i].x);
			num3 = Mathf.Min(num3, this.MeshBoundary[i].y);
			num4 = Mathf.Max(num4, this.MeshBoundary[i].y);
		}
		Vector2 vector = new Vector2(1f / (num2 - num), 1f / (num4 - num3));
		Vector2 p = this.OriginalPosition.FlattenVector3();
		float num5 = Room.WallOffset * Room.WallOffset + 0.01f;
		foreach (Vector2 vector2 in this.MeshBoundary)
		{
			Vector3 vector3 = base.transform.localToWorldMatrix.inverse.MultiplyVector(new Vector3(a.x, 0f, a.y));
			Vector3 v = new Vector3(vector2.x * one.x + vector3.x, 0f, vector2.y * one.y + vector3.z);
			Vector3 vector4 = base.transform.localToWorldMatrix.MultiplyPoint(v);
			Vector2 vector5 = new Vector2(vector4.x, vector4.z);
			if (!Utilities.IsInside(vector5, expanded))
			{
				Vector2 vector6 = Vector2.zero;
				float num6 = float.MaxValue;
				Vector2 vector7 = Vector2.zero;
				float num7 = float.MaxValue;
				for (int k = 0; k < expanded.Length; k++)
				{
					int num8 = (k + 1) % expanded.Length;
					if (Utilities.isLeft(expanded[k], expanded[num8], p) > 0)
					{
						if (Utilities.isLeft(expanded[(k != 0) ? (k - 1) : (expanded.Length - 1)], expanded[k], p) > 0)
						{
							float magnitude = (this.Parent.Edges[num8].Pos - vector5).magnitude;
							if (magnitude < 0.01f)
							{
								vector7 = expanded[k] - this.Parent.Edges[num8].Pos;
								num7 = vector7.magnitude;
								break;
							}
						}
						Vector2? vector8 = Utilities.ProjectToLine(vector5, expanded[k], expanded[num8]);
						if (vector8 != null)
						{
							Vector2 vector9 = vector8.Value - vector5;
							float sqrMagnitude = vector9.sqrMagnitude;
							if (sqrMagnitude < num7 && sqrMagnitude < num5)
							{
								num7 = sqrMagnitude;
								vector7 = vector9;
							}
						}
						else
						{
							Vector2 vector10 = expanded[k] - vector5;
							float sqrMagnitude2 = vector10.sqrMagnitude;
							if (sqrMagnitude2 < num6 && sqrMagnitude2 < num5)
							{
								num6 = sqrMagnitude2;
								vector6 = vector10;
							}
						}
					}
				}
				if (vector7 == Vector2.zero && vector6 != Vector2.zero)
				{
					vector7 = vector6;
				}
				if (vector7 != Vector2.zero)
				{
					a += vector7 * 0.5f;
					Vector3 vector11 = base.transform.localToWorldMatrix.inverse.MultiplyVector(new Vector3(vector7.x, 0f, vector7.y));
					one = new Vector2(one.x - Mathf.Abs(vector11.x) * vector.x, one.y - Mathf.Abs(vector11.z) * vector.y);
				}
			}
		}
		base.transform.position = base.transform.position + new Vector3(a.x, 0f, a.y);
		base.transform.localScale = new Vector3(Mathf.Abs(one.x), 1f, Mathf.Abs(one.y));
	}

	public bool CanUse(Actor actor, string action)
	{
		bool flag = true;
		if (actor.AItype == AI<Actor>.AIType.Employee)
		{
			if (!this.Parent.CompatibleWithTeam(actor.GetTeam()))
			{
				return false;
			}
			if (this.Parent.ForceRole >= 0 && (Employee.RoleToMask[this.Parent.ForceRole] & actor.GetRole()) == Employee.RoleBit.None)
			{
				return false;
			}
			flag = ((this.Reserved == null || this.Reserved == actor) && (this.OwnedBy == null || this.OwnedBy == actor));
		}
		if (flag)
		{
			flag = false;
			for (int i = 0; i < this.InteractionPoints.Length; i++)
			{
				InteractionPoint interactionPoint = this.InteractionPoints[i];
				if ((interactionPoint.UsedBy == null || interactionPoint.UsedBy == actor) && interactionPoint.Name.Equals(action) && interactionPoint.Usable())
				{
					flag = true;
					break;
				}
			}
		}
		return flag && (actor.AItype != AI<Actor>.AIType.Employee || !this.HasUpg || !this.upg.Broken) && (!this.Type.Equals("Computer") || (!(this.ComputerChair == null) && this.ComputerChair.CanUse(actor, action)));
	}

	public InteractionPoint GetInteractionPoint(string action, bool force = false)
	{
		InteractionPoint result = null;
		for (int i = 0; i < this.InteractionPoints.Length; i++)
		{
			InteractionPoint interactionPoint = this.InteractionPoints[i];
			if (interactionPoint.Name.Equals(action) && (force || interactionPoint.Usable()))
			{
				result = interactionPoint;
				break;
			}
		}
		return result;
	}

	public InteractionPoint GetQueueableInteractionPoint(Actor act, string action)
	{
		if (this.MaxQueue == 0)
		{
			return null;
		}
		InteractionPoint result = null;
		for (int i = 0; i < this.InteractionPoints.Length; i++)
		{
			InteractionPoint interactionPoint = this.InteractionPoints[i];
			if (interactionPoint.Name.Equals(action) && interactionPoint.Usable() && interactionPoint.CanQueue(act, action))
			{
				result = interactionPoint;
				break;
			}
		}
		return result;
	}

	public InteractionPoint GetInteractionPoint(Actor actor, string action)
	{
		if (actor.AItype == AI<Actor>.AIType.Employee)
		{
			if (GameSettings.Instance.RentMode && this.Parent.Rentable && !this.Parent.PlayerOwned)
			{
				return null;
			}
			if (!this.Parent.CompatibleWithTeam(actor.GetTeam()))
			{
				return null;
			}
			if (this.Parent.ForceRole >= 0 && (Employee.RoleToMask[this.Parent.ForceRole] & actor.GetRole()) == Employee.RoleBit.None)
			{
				return null;
			}
			if (action.Equals("Use") && ((this.Reserved != null && this.Reserved != actor) || (this.OwnedBy != null && this.OwnedBy != actor)))
			{
				return null;
			}
			if (this.HasUpg && this.upg.Broken)
			{
				return null;
			}
		}
		if (this.Type.Equals("Tray"))
		{
			if (action.Equals("Serve"))
			{
				if (!this.CanPlaceHoldable())
				{
					return null;
				}
			}
			else if (this.HasHoldables == 0)
			{
				return null;
			}
		}
		else if (this.Type.Equals("Computer") && (action.Equals("Use") || action.Equals("Repair")) && (this.ComputerChair == null || (!action.Equals("Repair") && !this.ComputerChair.CanUse(actor, "Use"))))
		{
			return null;
		}
		InteractionPoint result = null;
		for (int i = 0; i < this.InteractionPoints.Length; i++)
		{
			InteractionPoint interactionPoint = this.InteractionPoints[i];
			if ((interactionPoint.UsedBy == null || interactionPoint.UsedBy == actor) && interactionPoint.Name.Equals(action) && interactionPoint.Usable() && (this.MaxQueue == 0 || interactionPoint.CurrentQueue.Count == 0 || interactionPoint.CurrentQueue[0] == actor))
			{
				result = interactionPoint;
				break;
			}
		}
		return result;
	}

	public string DefaultColorGroup
	{
		get
		{
			return (!string.IsNullOrEmpty(this._defaultColorGroup)) ? this._defaultColorGroup : base.name;
		}
	}

	public Color ColorPrimary
	{
		get
		{
			return this._colorPrimary;
		}
		set
		{
			if (!this.ColorPrimaryEnabled)
			{
				this._colorPrimary = value;
			}
			else
			{
				MaterialBank.Instance.RemoveFurnitureMat(this, this._colorPrimary, this.ColorSecondary, this.ColorTertiary);
				this._colorPrimary = value;
				if (this.LightPrimary && this.ColorableLights != null)
				{
					this.ColorableLights.ForEach(delegate(Light x)
					{
						x.color = this._colorPrimary;
					});
				}
				this.UpdateMaterials();
			}
		}
	}

	public void TurnOffColor(bool off2, bool off3)
	{
		if (this.TurnOffSecondaryColor != off2 || this.TurnOffTertiaryColor != off3)
		{
			MaterialBank.Instance.RemoveFurnitureMat(this, this._colorPrimary, this.ColorSecondary, this.ColorTertiary);
			this.TurnOffSecondaryColor = off2;
			this.TurnOffTertiaryColor = off3;
			this.UpdateMaterials();
		}
	}

	public Color ColorSecondary
	{
		get
		{
			return (!this.TurnOffSecondaryColor) ? this._colorSecondary : Color.black;
		}
		set
		{
			if (!this.ColorSecondaryEnabled)
			{
				this._colorSecondary = value;
			}
			else
			{
				MaterialBank.Instance.RemoveFurnitureMat(this, this._colorPrimary, this.ColorSecondary, this.ColorTertiary);
				this._colorSecondary = value;
				this.UpdateMaterials();
			}
		}
	}

	public Color ColorTertiary
	{
		get
		{
			return (!this.TurnOffTertiaryColor) ? this._colorTertiary : Color.black;
		}
		set
		{
			if (!this.ColorTertiaryEnabled)
			{
				this._colorTertiary = value;
			}
			else
			{
				MaterialBank.Instance.RemoveFurnitureMat(this, this._colorPrimary, this.ColorSecondary, this.ColorTertiary);
				this._colorTertiary = value;
				if (!this.LightPrimary)
				{
					this.ColorableLights.ForEach(delegate(Light x)
					{
						x.color = this._colorTertiary;
					});
				}
				this.UpdateMaterials();
			}
		}
	}

	public void ImprintPosition()
	{
		Vector3 position = base.transform.position;
		base.transform.position = this.OriginalPosition;
		for (int i = 0; i < this.InteractionPoints.Length; i++)
		{
			this.InteractionPoints[i].pos = this.InteractionPoints[i].transform.position;
		}
		base.transform.position = position;
		for (int j = 0; j < this.SnapPoints.Length; j++)
		{
			this.SnapPoints[j].pos = this.SnapPoints[j].GetRealPos();
		}
	}

	public void UpdateFreeNavs(bool threaded = false)
	{
		bool flag = true;
		Vector3 position = Vector3.zero;
		if (!threaded)
		{
			position = base.transform.position;
			base.transform.position = this.OriginalPosition;
		}
		foreach (IGrouping<string, InteractionPoint> grouping in from x in this.InteractionPoints
		group x by x.Name)
		{
			int num = 0;
			int num2 = 1;
			foreach (InteractionPoint interactionPoint in grouping)
			{
				interactionPoint.UpdateFreeNav(threaded);
				if (!interactionPoint.NeedsReachCheck || interactionPoint.Usable())
				{
					num++;
					num2 = Mathf.Max(num2, interactionPoint.MinimumNeeded);
				}
			}
			if (num < num2)
			{
				flag = false;
			}
		}
		if (!threaded)
		{
			base.transform.position = position;
		}
		for (int i = 0; i < this.InteractionPoints.Length; i++)
		{
			this.InteractionPoints[i].UpdateDefer();
		}
		object unreachableFuniture = HUD.Instance.UnreachableFuniture;
		lock (unreachableFuniture)
		{
			if (flag)
			{
				HUD.Instance.UnreachableFuniture.Remove(this);
			}
			else
			{
				HUD.Instance.UnreachableFuniture.Add(this);
			}
		}
		for (int j = 0; j < this.SnapPoints.Length; j++)
		{
			if (this.SnapPoints[j].UsedBy == null)
			{
				this.SnapPoints[j].UpdateValid(threaded);
			}
		}
	}

	public void CheckAssignMat()
	{
		if (GameSettings.Instance == null)
		{
			return;
		}
		if (GameSettings.Instance.AssignOverlay && this.CanAssign && this.UseStandardMat)
		{
			this.ForceMaterial((!(this.OwnedBy != null)) ? MaterialBank.Instance.DefaultWhite : MaterialBank.Instance.DefaultRed, true);
		}
		else if (this._forceMat)
		{
			this.ForceMaterial(null, false);
		}
	}

	private void ForceMaterial(MaterialPropertyBlock block, bool force)
	{
		if (force)
		{
			this._forceMat = true;
			MaterialBank.Instance.RemoveFurnitureMat(this, this._colorPrimary, this.ColorSecondary, this.ColorTertiary);
			for (int i = 0; i < this.Colorable.Count; i++)
			{
				this.Colorable[i].SetPropertyBlock(block);
			}
		}
		else
		{
			this._forceMat = false;
			this.UpdateMaterials();
		}
	}

	public void UpdateMaterials()
	{
		if (this._forceMat)
		{
			return;
		}
		if (!this.UseStandardMat)
		{
			Material[] array = MaterialBank.Instance.FurnitureMat(this, this._colorPrimary, this.ColorSecondary, this.ColorTertiary);
			if (array == null || array.Length == 0)
			{
				Debug.LogError("Failed getting new materials for furniture " + base.name);
				return;
			}
			for (int i = 0; i < this.Colorable.Count; i++)
			{
				if (i >= array.Length)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Tried getting missing material index ",
						i,
						" for furniture ",
						base.name
					}));
					return;
				}
				Material[] array2 = new Material[Mathf.Max(1, this.Colorable[i].sharedMaterials.Length)];
				array2[0] = array[i];
				for (int j = 1; j < array2.Length; j++)
				{
					array2[j] = this.Colorable[i].sharedMaterials[j];
				}
				this.Colorable[i].sharedMaterials = array2;
			}
		}
		else
		{
			MaterialPropertyBlock materialPropertyBlock = MaterialBank.Instance.FurnitureInstanceMat(this, this._colorPrimary, this.ColorSecondary, this.ColorTertiary);
			if (materialPropertyBlock == null)
			{
				Debug.LogError("Failed getting new materials for furniture " + base.name);
				return;
			}
			for (int k = 0; k < this.Colorable.Count; k++)
			{
				this.Colorable[k].SetPropertyBlock(materialPropertyBlock);
			}
		}
		for (int l = 0; l < this.ColorableLights.Count; l++)
		{
			this.ColorableLights[l].color = ((!this.LightPrimary) ? this.ColorTertiary : this.ColorPrimary);
		}
	}

	public override bool ValidSnap(bool clone, HashSet<Room> destroy = null, bool keep = false)
	{
		WallEdge secondEdge = this.WallPosition.Keys.First((WallEdge x) => x != this.FirstEdge);
		if (!clone && this.FirstEdge.IsFence(secondEdge))
		{
			float height = this.FirstEdge.GetHeight(secondEdge);
			if (this.Height2 > height)
			{
				return false;
			}
		}
		Room key = this.FirstEdge.Links.FirstOrDefault((KeyValuePair<Room, WallEdge> x) => x.Value == secondEdge).Key;
		if (key == null)
		{
			return false;
		}
		if (!clone && this.OnlyExteriorWalls && this.FirstEdge.Links.ContainsValue(secondEdge) && secondEdge.Links.ContainsValue(this.FirstEdge))
		{
			Room key2 = secondEdge.Links.First((KeyValuePair<Room, WallEdge> x) => x.Value == this.FirstEdge).Key;
			if (this.PokesThroughWall || (!key.Outdoors && !key2.Outdoors))
			{
				return false;
			}
		}
		return true;
	}

	public override bool EdgeChanged(WallEdge[] previous, bool clone)
	{
		this.RefreshEdgeDetection();
		WallEdge secondEdge = this.WallPosition.Keys.First((WallEdge x) => x != this.FirstEdge);
		Room key = this.FirstEdge.Links.FirstOrDefault((KeyValuePair<Room, WallEdge> x) => x.Value == secondEdge).Key;
		if (!this.ValidSnap(clone, null, false))
		{
			base.DestroyMe();
			return false;
		}
		if (key != this.Parent)
		{
			if (this.Parent != null)
			{
				this.Parent.RemoveFurniture(this);
				this.Parent.RefreshNoise();
			}
			this.Parent = key;
			if (this.Parent != null)
			{
				this.Parent.AddFurniture(this);
				this.Parent.RefreshNoise();
			}
		}
		return true;
	}

	public void UpdateBoundaryPoints()
	{
		Matrix4x4 mat = Matrix4x4.TRS(this.OriginalPosition, base.transform.rotation, Vector3.one);
		this.FinalBoundary = this.BuildBoundary.SelectInPlace((Vector2 x) => mat.MultiplyPoint(x.ToVector3(0f)).FlattenVector3());
		this.FinalNav = this.NavBoundary.SelectInPlace((Vector2 x) => mat.MultiplyPoint(x.ToVector3(0f)).FlattenVector3());
	}

	public List<Vector2> CalculateBoundary()
	{
		Vector3 position = base.transform.position;
		Quaternion rotation = base.transform.rotation;
		Vector3 localScale = base.transform.localScale;
		base.transform.position = Vector3.zero;
		base.transform.rotation = Quaternion.identity;
		base.transform.localScale = Vector3.one;
		MeshFilter[] componentsInChildren = base.GetComponentsInChildren<MeshFilter>(true);
		List<Vector2> list = new List<Vector2>();
		List<Vector3> list2 = new List<Vector3>();
		foreach (MeshFilter meshFilter in from x in componentsInChildren
		where !"HideUnaffected".Equals(x.tag)
		select x)
		{
			Matrix4x4 mat = meshFilter.transform.localToWorldMatrix;
			meshFilter.sharedMesh.GetVertices(list2);
			list.AddRange(list2.Select((Vector3 x) => mat.MultiplyPoint(x).FlattenVector3()));
			list2.Clear();
		}
		list = Utilities.ComputeConvexHull(list);
		if (Utilities.Clockwise(list))
		{
			list.Reverse();
		}
		for (int i = 0; i < list.Count; i++)
		{
			int index = (i + 1) % list.Count;
			if (list[i].Dist(list[index]) < 0.01f)
			{
				list.RemoveAt(i);
				i--;
			}
			else
			{
				int index2 = (i != 0) ? (i - 1) : (list.Count - 1);
				Vector2 normalized = (list[i] - list[index2]).normalized;
				Vector2 normalized2 = (list[index] - list[i]).normalized;
				if (Mathf.Abs(Mathf.Acos(Vector2.Dot(normalized, normalized2))) < 0.05235988f)
				{
					list.RemoveAt(i);
					i--;
				}
			}
		}
		base.transform.position = position;
		base.transform.rotation = rotation;
		base.transform.localScale = localScale;
		return list;
	}

	public void GenerateBoundary()
	{
		this.BuildBoundary = this.CalculateBoundary().ToArray();
	}

	private void OnDrawGizmosSelected()
	{
		if (this.FinalBoundary != null && this.FinalBoundary.Length > 1)
		{
			for (int i = 0; i < this.FinalBoundary.Length; i++)
			{
				Gizmos.color = Color.Lerp(Color.white, Color.black, (float)i / (float)(this.FinalBoundary.Length - 1));
				int num = (i + 1) % this.FinalBoundary.Length;
				Gizmos.DrawLine(new Vector3(this.FinalBoundary[i].x, this.Height1, this.FinalBoundary[i].y), new Vector3(this.FinalBoundary[i].x, this.Height2, this.FinalBoundary[i].y));
				Gizmos.DrawLine(new Vector3(this.FinalBoundary[i].x, this.Height1, this.FinalBoundary[i].y), new Vector3(this.FinalBoundary[num].x, this.Height1, this.FinalBoundary[num].y));
				Gizmos.DrawLine(new Vector3(this.FinalBoundary[i].x, this.Height2, this.FinalBoundary[i].y), new Vector3(this.FinalBoundary[num].x, this.Height2, this.FinalBoundary[num].y));
				Gizmos.DrawSphere(new Vector3(this.FinalBoundary[i].x, this.Height1, this.FinalBoundary[i].y), 0.02f);
			}
			Gizmos.color = Color.white;
		}
		else if (this.BuildBoundary != null && this.BuildBoundary.Length > 1)
		{
			for (int j = 0; j < this.BuildBoundary.Length; j++)
			{
				Gizmos.color = Color.Lerp(Color.red, Color.black, (float)j / (float)(this.BuildBoundary.Length - 1));
				int num2 = (j + 1) % this.BuildBoundary.Length;
				Gizmos.DrawLine(base.transform.position + new Vector3(this.BuildBoundary[j].x, this.Height1, this.BuildBoundary[j].y), base.transform.position + new Vector3(this.BuildBoundary[j].x, this.Height2, this.BuildBoundary[j].y));
				Gizmos.DrawLine(base.transform.position + new Vector3(this.BuildBoundary[j].x, this.Height1, this.BuildBoundary[j].y), base.transform.position + new Vector3(this.BuildBoundary[num2].x, this.Height1, this.BuildBoundary[num2].y));
				Gizmos.DrawLine(base.transform.position + new Vector3(this.BuildBoundary[j].x, this.Height2, this.BuildBoundary[j].y), base.transform.position + new Vector3(this.BuildBoundary[num2].x, this.Height2, this.BuildBoundary[num2].y));
				Gizmos.DrawSphere(base.transform.position + new Vector3(this.BuildBoundary[j].x, this.Height1, this.BuildBoundary[j].y), 0.02f);
			}
			if (this.NavBoundary != null && this.NavBoundary.Length > 1)
			{
				for (int k = 0; k < this.NavBoundary.Length; k++)
				{
					Gizmos.color = Color.Lerp(Color.cyan, Color.black, (float)k / (float)(this.NavBoundary.Length - 1));
					int num3 = (k + 1) % this.NavBoundary.Length;
					Gizmos.DrawLine(base.transform.position + new Vector3(this.NavBoundary[k].x, 0.01f, this.NavBoundary[k].y), base.transform.position + new Vector3(this.NavBoundary[num3].x, 0.01f, this.NavBoundary[num3].y));
					Gizmos.DrawSphere(base.transform.position + new Vector3(this.NavBoundary[k].x, 0.01f, this.NavBoundary[k].y), 0.02f);
				}
			}
			Gizmos.color = Color.white;
		}
		if (this.ComputerChair != null)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(base.transform.position, this.ComputerChair.transform.position);
			Gizmos.color = Color.white;
		}
	}

	public void OnDrawGizmos()
	{
		if (this.IsConnecter && this.pathNode != null)
		{
			foreach (PathNode<Vector3> pathNode in this.pathNode.GetConnections())
			{
				Gizmos.color = Color.red;
				Gizmos.DrawLine(this.pathNode.Point, pathNode.Point);
			}
		}
		if (this.HasAudioSource)
		{
			Gizmos.color = ((!this.AudioSrc.isPlaying) ? Color.red : ((!(this.AudioSrc.outputAudioMixerGroup == AudioManager.InGameHighPass)) ? Color.green : Color.blue));
			Gizmos.DrawSphere(base.transform.position + Vector3.up * 1.5f, 0.5f);
		}
		Gizmos.color = Color.white;
	}

	public Transform ObjectTransform
	{
		get
		{
			return base.transform;
		}
	}

	public float RefreshTime
	{
		get
		{
			return this.rTime;
		}
		set
		{
			this.rTime = value;
		}
	}

	public bool IsValid
	{
		get
		{
			return this != null && base.gameObject != null;
		}
	}

	public override int GetFloor()
	{
		return (!(this.Parent != null)) ? base.GetFloor() : this.Parent.Floor;
	}

	public void UpdateBlocked()
	{
		HUD.Instance.BlockedDoorways.Remove(this);
		if (this.IsConnecter)
		{
			Vector3 offsetPos = this.GetOffsetPos(this.Parent, false);
			if (this.Parent.GetNodeAt(offsetPos.FlattenVector3()) == null)
			{
				HUD.Instance.BlockedDoorways.Add(this);
			}
			if (this.TwoFloors && this.ExtraParent != null)
			{
				offsetPos = this.GetOffsetPos(this.Parent, true);
				if (this.ExtraParent.GetNodeAt(offsetPos.FlattenVector3()) == null)
				{
					HUD.Instance.BlockedDoorways.Add(this);
				}
			}
		}
	}

	public Transform[] IntermediatePoints(Room from)
	{
		if (from != this.Parent && this.InterPointsReversed != null && this.InterPointsReversed.Length > 0)
		{
			return this.InterPointsReversed;
		}
		return this.InterPoints;
	}

	public float GetWattPrice(float months, float watt)
	{
		return (GameSettings.Instance.RentMode && !this.InRentMode) ? 0f : (months * watt * 5f);
	}

	public float GetWaterPrice(float months, float water)
	{
		return (GameSettings.Instance.RentMode && !this.InRentMode) ? 0f : (months * water * 5f);
	}

	public override bool IsSelectionRestricted()
	{
		return (!GameSettings.Instance.EditMode && this.PlacedInEditMode) || (!GameSettings.Instance.EditMode && GameSettings.Instance.RentMode && (!this.InRentMode || !this.Parent.Rentable || !this.Parent.PlayerOwned));
	}

	public override bool IsSelectableInView()
	{
		return (this.Parent.Outdoors || this.GetFloor() == GameSettings.Instance.ActiveFloor) && (this.Height1 <= 1.6f || !GameSettings.Instance.HideCeilingFurniture) && (!this.WallFurn || this.CheckWallDown());
	}

	public bool IsNull
	{
		get
		{
			return this == null || base.gameObject == null;
		}
	}

	public KeyValuePair<CombineInstance[], int> FixCombine()
	{
		CombineInstance[] array = new CombineInstance[this.Colorable.Count];
		int num = 0;
		for (int i = 0; i < this.Colorable.Count; i++)
		{
			CombineInstance combineInstance = new CombineInstance
			{
				mesh = this.FixMesh(this.Colorable[i].GetComponent<MeshFilter>().mesh),
				transform = this.Colorable[i].localToWorldMatrix
			};
			array[i] = combineInstance;
			num += combineInstance.mesh.vertexCount;
		}
		return new KeyValuePair<CombineInstance[], int>(array, num);
	}

	private Mesh FixMesh(Mesh m)
	{
		Mesh mesh = new Mesh();
		Vector2[] uv = m.uv;
		mesh.vertices = m.vertices;
		mesh.normals = m.normals;
		mesh.uv = uv;
		mesh.tangents = m.tangents;
		Color[] array = new Color[uv.Length];
		for (int i = 0; i < array.Length; i++)
		{
			Vector2 vector = uv[i];
			if (vector.x < 0.5f)
			{
				if (vector.y < 0.5f)
				{
					array[i] = this.ColorTertiary.Alpha(1f - vector.y / 0.5f);
				}
				else
				{
					array[i] = this.ColorPrimary.Alpha((1f - vector.y) / 0.5f);
				}
			}
			else if (vector.y < 0.5f)
			{
				float a = (vector.x - 0.5f) / 0.5f;
				float num = vector.y / 0.5f;
				array[i] = new Color(num, num, num, a);
			}
			else
			{
				array[i] = this.ColorSecondary.Alpha((1f - vector.y) / 0.5f);
			}
		}
		mesh.colors = array;
		mesh.triangles = m.triangles;
		return mesh;
	}

	public override bool SingleMat()
	{
		return false;
	}

	public void MeshCombine(bool enable)
	{
	}

	public bool CanPlaceHoldable()
	{
		foreach (KeyValuePair<Transform, Holdable> keyValuePair in this.Holdables)
		{
			if (keyValuePair.Value == null)
			{
				return true;
			}
		}
		return false;
	}

	public void UpdateHoldableStatus()
	{
		this.HasHoldables = this.Holdables.Values.Count((Holdable x) => x != null);
	}

	public void PlaceHoldable(Holdable holdable)
	{
		foreach (KeyValuePair<Transform, Holdable> keyValuePair in this.Holdables.ToList<KeyValuePair<Transform, Holdable>>())
		{
			if (keyValuePair.Value == null)
			{
				holdable.transform.SetParent(keyValuePair.Key);
				holdable.transform.localPosition = Vector3.zero;
				holdable.transform.localRotation = Quaternion.identity;
				this.Holdables[keyValuePair.Key] = holdable;
				break;
			}
		}
		this.UpdateHoldableStatus();
	}

	public Holdable TakeHoldable()
	{
		Holdable result = null;
		foreach (KeyValuePair<Transform, Holdable> keyValuePair in this.Holdables.ToList<KeyValuePair<Transform, Holdable>>().ReverseEnum<KeyValuePair<Transform, Holdable>>())
		{
			if (keyValuePair.Value != null)
			{
				result = keyValuePair.Value;
				this.Holdables[keyValuePair.Key] = null;
				break;
			}
		}
		this.UpdateHoldableStatus();
		return result;
	}

	public static float CalculateNoise(float noise, float distance)
	{
		if (distance >= noise)
		{
			return 0f;
		}
		float num = Mathf.Min(1f, distance / noise);
		num = Mathf.Pow(1f - num, 0.25f);
		return num * Mathf.Clamp01(noise / 8f);
	}

	private static List<Vector2> GetNoiseReduction(Vector2 pos, Room r, List<Furniture> cubicles)
	{
		Furniture.cSegments.Clear();
		if (cubicles != null)
		{
			for (int i = 0; i < cubicles.Count; i++)
			{
				Furniture.AddSegment(pos, cubicles[i]);
			}
		}
		else
		{
			HashList<Furniture> furniture = r.GetFurniture("Cubicle");
			int count = furniture.Count;
			for (int j = 0; j < count; j++)
			{
				Furniture furn = furniture[j];
				Furniture.AddSegment(pos, furn);
			}
		}
		return Furniture.cSegments;
	}

	private static void AddSegment(Vector2 pos, Furniture furn)
	{
		float num = pos.x - furn.OriginalPosition.x;
		float num2 = pos.y - furn.OriginalPosition.z;
		if (num > -2f && num < 2f && num2 > -2f && num2 < 2f)
		{
			Vector3 vector = furn.transform.right * 0.5f;
			Furniture.cSegments.Add(new Vector2(furn.OriginalPosition.x + vector.x, furn.OriginalPosition.z + vector.z));
			Furniture.cSegments.Add(new Vector2(furn.OriginalPosition.x - vector.x, furn.OriginalPosition.z - vector.z));
		}
	}

	private static float CalculateNoiseReduction(Vector2 a, Vector2 b, List<Vector2> cubes)
	{
		if (cubes.Count == 0)
		{
			return 1f;
		}
		int i = 0;
		while (i < cubes.Count)
		{
			Vector2 p = cubes[i];
			Vector2 p2 = cubes[i + 1];
			if (a.x < p.x)
			{
				if (a.x >= p2.x || b.x >= p.x || b.x >= p2.x)
				{
					goto IL_BE;
				}
			}
			else if (p.x >= b.x || p2.x >= a.x || p2.x >= b.x)
			{
				goto IL_BE;
			}
			IL_166:
			i += 2;
			continue;
			IL_BE:
			if (a.y < p.y)
			{
				if (a.y < p2.y && b.y < p.y && b.y < p2.y)
				{
					goto IL_166;
				}
			}
			else if (p.y < b.y && p2.y < a.y && p2.y < b.y)
			{
				goto IL_166;
			}
			if (Utilities.FasterLineSegmentIntersection(a, b, p, p2))
			{
				return 0.6f;
			}
			goto IL_166;
		}
		return 1f;
	}

	private static void AddNoiseValue(float val)
	{
		if (val > 0f)
		{
			Furniture.NoiseValues.AddOrReplace(Furniture.NoiseValueCount, val);
			Furniture.NoiseValueCount++;
			Furniture.NoiseValueMax = Mathf.Max(val, Furniture.NoiseValueMax);
		}
	}

	private static float CalculateFinalNoiseValue()
	{
		if (Furniture.NoiseValueCount == 0 || Furniture.NoiseValueMax == 0f)
		{
			return 0f;
		}
		float num = 0f;
		for (int i = 0; i < Furniture.NoiseValueCount; i++)
		{
			num += Furniture.NoiseValues[i] / Furniture.NoiseValueMax * Furniture.NoiseValues[i];
		}
		return num;
	}

	public static float RecalculateNoise(Vector2 p, bool actor, Room parent, Furniture self, List<Furniture> cubicles = null, bool both = false, bool forceNoise = false)
	{
		List<Vector2> noiseReduction = Furniture.GetNoiseReduction(p, parent, cubicles);
		Furniture.NoiseValueCount = 0;
		Furniture.NoiseValueMax = 0f;
		if (actor || both)
		{
			for (int i = 0; i < parent.Occupants.Count; i++)
			{
				Actor actor2 = parent.Occupants[i];
				if ((self == null || actor2 != self.InteractionPoints[0].UsedBy) && actor2.Noisiness > 0f)
				{
					Vector2 b = actor2.transform.position.FlattenVector3();
					float num = actor2.Noisiness * Furniture.CalculateNoiseReduction(p, b, noiseReduction);
					if (Mathf.Abs(actor2.transform.position.x - p.x) < num && Mathf.Abs(actor2.transform.position.z - p.y) < num)
					{
						Furniture.AddNoiseValue(Furniture.CalculateNoise(num, (p - b).magnitude));
					}
				}
			}
		}
		if (!actor || both)
		{
			Furniture.GetNoiseFromRoom(parent, p, noiseReduction, self, false, forceNoise);
			Furniture.NoiseVisit.Clear();
			for (int j = 0; j < parent.Edges.Count; j++)
			{
				Room room = parent.Edges[(j + 1) % parent.Edges.Count].GetRoom(parent.Edges[j]);
				if (room != null && !Furniture.NoiseVisit.Contains(room))
				{
					Furniture.GetNoiseFromRoom(room, p, noiseReduction, self, true, forceNoise);
					Furniture.NoiseVisit.Add(room);
				}
			}
			if (parent.Floor >= 0)
			{
				float num2 = Room.GetOutdoorNoise(new Vector3(0f, (float)(parent.Floor * 2), 0f));
				if (num2 > 0f)
				{
					float num3 = 100f;
					float num4 = 0.05f;
					for (int k = 0; k < parent.Edges.Count; k++)
					{
						WallEdge wallEdge = parent.Edges[k];
						WallEdge wallEdge2 = parent.Edges[(k + 1) % parent.Edges.Count];
						Room room2 = wallEdge2.GetRoom(wallEdge);
						if (room2 == null || room2.Outdoors)
						{
							Vector2? vector = Utilities.ProjectToLine(p, wallEdge.Pos, wallEdge2.Pos);
							if (vector != null)
							{
								float magnitude = (p - vector.Value).magnitude;
								if (magnitude < num2)
								{
									float num5 = Furniture.WallIsolation;
									float magnitude2 = (wallEdge.Pos - vector.Value).magnitude;
									HashSet<WallSnap> orNull = wallEdge.Children.GetOrNull(wallEdge2);
									if (orNull != null)
									{
										foreach (RoomSegment roomSegment in orNull.OfType<RoomSegment>())
										{
											float num6 = roomSegment.WallPosition[wallEdge];
											float num7 = num6 + roomSegment.WallWidth / 2f;
											num6 -= roomSegment.WallWidth / 2f;
											if (magnitude2 > num6 && magnitude2 < num7)
											{
												num5 = roomSegment.NoiseFactor * (roomSegment.Height2 - roomSegment.Height1) / 2f;
												break;
											}
										}
									}
									num5 *= Furniture.CalculateNoiseReduction(p, vector.Value, noiseReduction);
									if (magnitude / num5 < num3 / num4)
									{
										num3 = magnitude;
										num4 = num5;
									}
								}
							}
						}
					}
					num2 *= num4;
					Furniture.AddNoiseValue(Furniture.CalculateNoise(num2, num3));
				}
			}
		}
		return Furniture.CalculateFinalNoiseValue();
	}

	private static void GetNoiseFromRoom(Room r, Vector2 p, List<Vector2> noiseReduction, Furniture self, bool neighbor, bool forceNoise)
	{
		List<Furniture> furnitures = r.GetFurnitures();
		int count = furnitures.Count;
		for (int i = 0; i < count; i++)
		{
			Furniture furniture = furnitures[i];
			if (furniture.Noisiness > 0f && (forceNoise || furniture.IsOn) && furniture != self && (furniture.SnappedTo == null || furniture.SnappedTo.Parent != self) && Mathf.Abs(furniture.OriginalPosition.x - p.x) < furniture.Noisiness && Mathf.Abs(furniture.OriginalPosition.z - p.y) < furniture.Noisiness)
			{
				Vector2 vector = furniture.OriginalPosition.FlattenVector3();
				float num = furniture.Noisiness * Furniture.CalculateNoiseReduction(p, vector, noiseReduction);
				if (neighbor)
				{
					num *= Furniture.GetWallFactor(p, vector, r);
				}
				Furniture.AddNoiseValue(Furniture.CalculateNoise(num, (p - vector).magnitude));
			}
		}
	}

	private static float GetWallFactor(Vector2 p, Vector2 p2, Room r)
	{
		for (int i = 0; i < r.Edges.Count; i++)
		{
			WallEdge wallEdge = r.Edges[i];
			WallEdge wallEdge2 = r.Edges[(i + 1) % r.Edges.Count];
			Vector2? lineIntersection = Utilities.GetLineIntersection(p, p2, wallEdge.Pos, wallEdge2.Pos);
			if (lineIntersection != null)
			{
				HashSet<WallSnap> orNull = wallEdge.Children.GetOrNull(wallEdge2);
				if (orNull != null)
				{
					float magnitude = (wallEdge.Pos - lineIntersection.Value).magnitude;
					foreach (RoomSegment roomSegment in orNull.OfType<RoomSegment>())
					{
						float num = roomSegment.WallPosition[wallEdge];
						float num2 = num + roomSegment.WallWidth / 2f;
						num -= roomSegment.WallWidth / 2f;
						if (magnitude > num && magnitude < num2)
						{
							return roomSegment.NoiseFactor * (roomSegment.Height2 - roomSegment.Height1) / 2f;
						}
					}
				}
				return Furniture.WallIsolation;
			}
		}
		return 0f;
	}

	public static float CombineNoises(float environmentNoise, float actorNoise)
	{
		float num = Mathf.Max(environmentNoise, actorNoise);
		if (num > 0f)
		{
			float num2 = environmentNoise / num * environmentNoise;
			float num3 = actorNoise / num * actorNoise;
			return Mathf.Clamp01(num2 + num3);
		}
		return 0f;
	}

	public void RefreshFinalNoiseValue()
	{
		this.FinalNoise = Furniture.CombineNoises(this.EnvironmentNoise, this.ActorNoise);
	}

	public IEnumerable<Furniture> IterateSnap(Furniture stopAt = null)
	{
		foreach (SnapPoint snapPoint in this.SnapPoints)
		{
			if (snapPoint.UsedBy != null)
			{
				if (!(snapPoint.UsedBy == stopAt))
				{
					yield return snapPoint.UsedBy;
					foreach (Furniture furniture in snapPoint.UsedBy.IterateSnap(null))
					{
						yield return furniture;
					}
				}
			}
		}
		yield break;
	}

	public bool IsPlayerControlled()
	{
		return GameSettings.Instance == null || GameSettings.Instance.EditMode || !GameSettings.Instance.RentMode || this.InRentMode;
	}

	[Header("Placement and pathfinding")]
	public Vector2[] BuildBoundary;

	public Vector2[] NavBoundary;

	[NonSerialized]
	public Vector2[] FinalNav;

	public Vector2[] FinalBoundary;

	public bool OnXEdge;

	public bool OnYEdge;

	public bool GenerateBoundaryOnStart;

	public bool CanRotate = true;

	public float Height1;

	public float Height2;

	public bool IgnoreBoundary;

	public bool BasementValid = true;

	public bool OnlyExteriorWalls;

	public bool WallFurn;

	[Header("Snapping")]
	public SnapPoint[] SnapPoints;

	public SnapPoint SnappedTo;

	public bool IsSnapping;

	public string SnapsTo;

	[Header("Ingame Options")]
	public bool CanAssign = true;

	public bool ReverseLowPass;

	public bool ValidIndoors = true;

	public bool ValidOutdoors = true;

	public bool ValidOnFence = true;

	public bool NeedsChair;

	public bool ManualUsageCalculation;

	public Sprite Thumbnail;

	public bool InteractAnimation;

	public int MaxQueue;

	public AudioClip InteractStartClip;

	public AudioClip InteractEndClip;

	public string UpgradeFrom;

	public string UpgradeTo;

	public bool InDemo = true;

	public bool ForceAccessible;

	public bool ITFix;

	public int UnlockYear;

	public bool TemperatureController;

	public bool AlwaysOn;

	public bool OnWithParent;

	public Transform ComputerTransform;

	public Vector3 OriginalOffset;

	public Vector3 PCAddonOffset;

	public Vector3 OriginalRotation;

	public Vector3 PCAddonRotation;

	public bool PokesThroughWall;

	public bool CanLean = true;

	public bool UseStandardMat;

	[NonSerialized]
	public bool DisableCombine;

	public bool TwoFloors;

	public Transform[] OffsetPoints;

	public Transform[] InterPoints;

	public Transform[] InterPointsReversed;

	public Renderer UpperFloorFrame;

	public Transform[] HoldablePoints;

	[NonSerialized]
	public Dictionary<Transform, Holdable> Holdables = new Dictionary<Transform, Holdable>();

	public bool DespawnHoldables;

	public float DespawnHour = 24f;

	public bool RandomSFX;

	public AudioClip[] SFXFiles;

	public float RandomSFXMin;

	public float RandomSFXMax;

	private float _randomSFXTimer;

	public bool DisableTableGrouping;

	public bool InRentMode = true;

	[Header("Ingame attributes")]
	public string Type;

	public string[] Category;

	public string FunctionCategory;

	public float Cost = 5f;

	[TextArea(3, 10)]
	public string ButtonDescription;

	public float ComputerPower = 1f;

	public float HeatCoolPotential;

	public float HeatCoolArea;

	public bool EqualizeTemperature = true;

	public float[] RoleBuffs = new float[5];

	public float Lighting;

	public float Coffee;

	public float Wait;

	public float[] AuraValues;

	public float Wattage;

	public float Water;

	public float Noisiness;

	public float Comfort;

	public float Environment = 1f;

	public bool HasConnectionBeam;

	public ElevatorHighlight ConnectionBeam;

	[Header("Do not touch")]
	[NonSerialized]
	public int HasHoldables;

	[NonSerialized]
	public string FileName;

	public bool PlacedInEditMode;

	public Vector2[] MeshBoundary;

	public float RotationOffset;

	[NonSerialized]
	public float TempControllerArea;

	public Vector3 OriginalPosition;

	[NonSerialized]
	private uint[][] SerializedQueue;

	private bool _isOn;

	private bool _hasClip;

	public AudioSource AudioSrc;

	[NonSerialized]
	private bool HasAudioSource;

	public bool isTemporary;

	[NonSerialized]
	public float TempWatt = 1f;

	[NonSerialized]
	public float TempWater = 1f;

	[NonSerialized]
	public float TempBreak = 1f;

	public Upgradable upg;

	[NonSerialized]
	public bool HasUpg;

	private float AtEdge;

	[NonSerialized]
	public bool DisableInitColor;

	public int PathFailCount;

	public InteractionPoint[] InteractionPoints;

	public Room Parent;

	private Actor _reserved;

	public float EnvironmentNoise;

	public float ActorNoise;

	public float FinalNoise;

	public Furniture ComputerChair;

	public bool Undo;

	public float LastWattUse;

	public float LastWaterUse;

	[NonSerialized]
	private SDateTime LastActivity;

	private float MaxAudioDistance;

	[NonSerialized]
	public Room ExtraParent;

	public TableScript Table;

	public static int AuraCount = 3;

	private Actor _ownedBy;

	private Renderer[] Children;

	[NonSerialized]
	public string[] actions = new string[]
	{
		"Sell",
		"Types in Room"
	};

	[NonSerialized]
	public HashSet<Room> RoomTemperature = new HashSet<Room>();

	private PathNode<Vector3> _pathNode;

	private static readonly FloatInterpolator ComfortDegration = new FloatInterpolator(1f, true, new float[]
	{
		1f,
		0.9999f,
		0.9984f,
		0.9919f,
		0.9744f,
		0.9375f,
		0.8704f,
		0.7599f,
		0.5904f,
		0.3439f,
		0.185493752f
	});

	[Header("Style")]
	public string _defaultColorGroup;

	public string PrimaryColorName = "Primary";

	public string SecondaryColorName = "Secondary";

	public string TertiaryColorName = "Tertiary";

	public bool ColorPrimaryEnabled;

	public bool ColorSecondaryEnabled;

	public bool ColorTertiaryEnabled;

	public bool ForceColorSecondary;

	public bool ForceColorTertiary;

	private Color _colorPrimary;

	private Color _colorSecondary;

	private Color _colorTertiary;

	[NonSerialized]
	public bool TurnOffSecondaryColor;

	[NonSerialized]
	public bool TurnOffTertiaryColor;

	public Color ColorPrimaryDefault;

	public Color ColorSecondaryDefault;

	public Color ColorTertiaryDefault;

	public List<Renderer> Colorable;

	public List<Light> ColorableLights;

	public bool FullColorMaterial;

	public bool LightPrimary = true;

	public Renderer TreeLeaves;

	private bool _forceMat;

	[NonSerialized]
	private float rTime;

	private static readonly List<Vector2> cSegments = new List<Vector2>();

	private static HashSet<Room> NoiseVisit = new HashSet<Room>();

	private static List<float> NoiseValues = new List<float>();

	private static int NoiseValueCount = 0;

	public static float NoiseValueMax = 0f;

	private static float WallIsolation = 0.3f;

	public enum AuraTypes
	{
		Effectiveness,
		Skill,
		Mood
	}
}
