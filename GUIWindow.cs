﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GUIWindow : MonoBehaviour
{
	public bool CanSize
	{
		get
		{
			return this.SizeButton != null && this.SizeButton.activeSelf;
		}
	}

	public bool IsCollapsed
	{
		get
		{
			return this.Collapsed;
		}
	}

	public bool Shown
	{
		get
		{
			return base.gameObject.activeSelf;
		}
	}

	public string Title
	{
		get
		{
			return this._title;
		}
		set
		{
			if (this._title != value)
			{
				this._title = value;
				this.TitleText.text = this._title.Loc();
			}
		}
	}

	public string NonLocTitle
	{
		get
		{
			return this._title;
		}
		set
		{
			this._title = value;
			this.TitleText.text = this._title;
		}
	}

	public void ToggleCollapse()
	{
		if (this.Collapsed)
		{
			if (this.CanSize && this.reactiveSizeButton)
			{
				this.SizeButton.SetActive(true);
			}
			float num = this.TitleText.preferredWidth + 64f;
			this.rectTransform.anchoredPosition = new Vector2(this.rectTransform.anchoredPosition.x - (this.LastWidth - num), this.rectTransform.anchoredPosition.y);
			this.rectTransform.sizeDelta = new Vector2(this.LastWidth, this.LastHeight);
			this.MainPanel.SetActive(true);
		}
		else
		{
			if (this.CanSize)
			{
				this.SizeButton.SetActive(false);
				this.reactiveSizeButton = true;
			}
			this.LastWidth = this.rectTransform.sizeDelta.x;
			this.LastHeight = this.rectTransform.sizeDelta.y;
			float num2 = this.TitleText.preferredWidth + 64f;
			this.rectTransform.anchoredPosition = new Vector2(this.rectTransform.anchoredPosition.x + (this.LastWidth - num2), this.rectTransform.anchoredPosition.y);
			this.rectTransform.sizeDelta = new Vector2(num2, 23f);
			this.MainPanel.SetActive(false);
		}
		this.Collapsed = !this.Collapsed;
	}

	private void Start()
	{
		this.Title = (this.Title ?? this.InitialTitle);
		this.rectTransform = base.GetComponent<RectTransform>();
		if (this.StartHidden)
		{
			base.gameObject.SetActive(false);
		}
		else
		{
			WindowManager.RegisterWindow(this);
		}
	}

	private void Update()
	{
		if (this.Dragging)
		{
			base.transform.position = new Vector3(Mathf.Clamp(Input.mousePosition.x - this.InitMouseOffset.x, -this.rectTransform.sizeDelta.x + 64f, (float)(Screen.width - 64)), Mathf.Clamp(Input.mousePosition.y - this.InitMouseOffset.y, this.rectTransform.sizeDelta.y, (float)Screen.height), base.transform.position.z);
			if (Input.GetMouseButtonUp(0))
			{
				this.Dragging = false;
				if (this.SavePosition)
				{
					Options.AddWindowSize(this.WindowSizeID, new SVector3(this.rectTransform.anchoredPosition.x, this.rectTransform.anchoredPosition.y, (!this.Collapsed) ? this.rectTransform.sizeDelta.x : this.LastWidth, (!this.Collapsed) ? this.rectTransform.sizeDelta.y : this.LastHeight));
				}
			}
		}
		else if (this.Sizing)
		{
			this.rectTransform.sizeDelta = new Vector2(Mathf.Clamp(Input.mousePosition.x / Options.UISize - base.transform.position.x + this.InitMouseOffset.x, this.MinSize.x, (float)Screen.width), Mathf.Clamp(((float)Screen.height - Input.mousePosition.y) / Options.UISize + base.transform.position.y + this.InitMouseOffset.y, this.MinSize.y, (float)Screen.height));
			if (Input.GetMouseButtonUp(0))
			{
				this.Sizing = false;
				if (this.OnSizeChanged != null)
				{
					this.OnSizeChanged();
				}
				Options.AddWindowSize(this.WindowSizeID, new SVector3(this.rectTransform.anchoredPosition.x, this.rectTransform.anchoredPosition.y, (!this.Collapsed) ? this.rectTransform.sizeDelta.x : this.LastWidth, (!this.Collapsed) ? this.rectTransform.sizeDelta.y : this.LastHeight));
			}
		}
	}

	public void BeginDrag()
	{
		this.InitMouseOffset = Input.mousePosition - base.transform.position;
		this.Dragging = true;
	}

	public void BeginSize()
	{
		this.InitMouseOffset = new Vector2(base.transform.position.x + this.rectTransform.sizeDelta.x - Input.mousePosition.x / Options.UISize, -(base.transform.position.y - this.rectTransform.sizeDelta.y) - ((float)Screen.height - Input.mousePosition.y) / Options.UISize);
		this.Sizing = true;
	}

	private void OnDestroy()
	{
		WindowManager.DeregisterWindow(this);
	}

	public void Toggle(bool forceRegister = false)
	{
		if (this.Shown)
		{
			if (this.Collapsed)
			{
				this.ToggleCollapse();
			}
			else
			{
				this.Close();
			}
		}
		else
		{
			this.Show(forceRegister, true);
		}
	}

	public void Show()
	{
		this.Show(false, true);
	}

	public void Show(bool forceRegister, bool resetCollapse = true)
	{
		if (base.gameObject.activeSelf && !forceRegister)
		{
			WindowManager.Focus(this);
		}
		else
		{
			WindowManager.RegisterWindow(this);
			base.gameObject.SetActive(true);
		}
		if (resetCollapse && this.Collapsed)
		{
			this.ToggleCollapse();
		}
		this.HasBeenShown = true;
	}

	public void Close()
	{
		if (this.OnClose != null)
		{
			this.OnClose();
		}
		if (this.OnlyHide)
		{
			WindowManager.DeregisterWindow(this);
			base.gameObject.SetActive(false);
		}
		else
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void Focus()
	{
		WindowManager.Focus(this);
	}

	public bool OnlyHide;

	public bool StartHidden;

	public bool Modal;

	public bool AlwaysOnTop;

	public bool HasBeenShown;

	public bool HideBlockPanel;

	public Vector2 MinSize = new Vector2(128f, 128f);

	public Text TitleText;

	public string InitialTitle = "New window";

	public Action OnClose;

	public GameObject MainPanel;

	public GameObject SizeButton;

	public string WindowSizeID;

	public bool SavePosition = true;

	private string _title;

	private Vector2 InitMouseOffset;

	private bool Dragging;

	private bool Sizing;

	private bool Collapsed;

	private bool reactiveSizeButton = true;

	private float LastHeight;

	private float LastWidth;

	public RectTransform rectTransform;

	public Action OnSizeChanged;
}
