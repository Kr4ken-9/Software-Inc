﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ReputationItem : MonoBehaviour
{
	public void SetRep(float pct)
	{
		if (pct < 0.5f)
		{
			this.Icon.color = Color.Lerp(new Color32(111, 111, 111, byte.MaxValue), new Color32(49, 130, 143, byte.MaxValue), pct * 2f);
		}
		else
		{
			this.Icon.color = Color.Lerp(new Color32(49, 130, 143, byte.MaxValue), new Color32(70, 143, 67, byte.MaxValue), (pct - 0.5f) * 2f);
		}
		this.Icon.sprite = this.Icons[Mathf.Min(this.Icons.Length - 1, Mathf.FloorToInt((float)this.Icons.Length * pct))];
	}

	public Text Label;

	public Image Icon;

	public Sprite[] Icons;
}
