﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ThumbCam : MonoBehaviour
{
	private void Awake()
	{
		this.ThumbnailCam.targetTexture = this.Target;
	}

	public void GenerateBoundary()
	{
		if (this.ActiveObject != null && this.IsFurn)
		{
			Furniture component = this.ActiveObject.GetComponent<Furniture>();
			component.GenerateBoundary();
			Vector3[] minMax = this.GetMinMax(this.ActiveObject);
			component.Height1 = Mathf.Max(0f, minMax[0].y);
			component.Height2 = Mathf.Min(2f, minMax[1].y);
		}
	}

	public void SaveToPrefab()
	{
	}

	private void Start()
	{
		this.RotX = this.PivotTransform.rotation.eulerAngles.x;
		this.RotY = this.PivotTransform.rotation.eulerAngles.y;
		foreach (Furniture furniture in from x in this.BuildObjects.GetAllFurniture()
		select x.GetComponent<Furniture>())
		{
			Furniture localItem = furniture;
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
			gameObject.GetComponentInChildren<Text>().text = furniture.name;
			gameObject.GetComponentsInChildren<Image>()[1].sprite = furniture.Thumbnail;
			gameObject.GetComponentsInChildren<Image>()[2].enabled = (furniture.BuildBoundary == null || furniture.BuildBoundary.Length == 0 || furniture.NavBoundary == null || furniture.NavBoundary.Length == 0);
			gameObject.GetComponent<Button>().onClick.AddListener(delegate
			{
				if (this.ActiveObject != null)
				{
					UnityEngine.Object.Destroy(this.ActiveObject);
				}
				this.IsFurn = true;
				this.ActiveFurniturePrefab = localItem;
				GameObject gameObject3 = UnityEngine.Object.Instantiate<GameObject>(localItem.gameObject);
				gameObject3.GetComponent<Furniture>().isTemporary = true;
				this.WallCube.SetActive(localItem.WallFurn);
				this.SideWall1.SetActive(false);
				this.SideWall2.SetActive(false);
				this.UpperWall.SetActive(false);
				if (localItem.WallFurn)
				{
					this.GroundPlane.transform.position = new Vector3(0f, 0f, -0.1f);
				}
				else
				{
					this.GroundPlane.transform.position = new Vector3((!localItem.OnXEdge) ? 0.5f : 0f, 0f, (!localItem.OnYEdge) ? 0.5f : 0f);
				}
				gameObject3.name = gameObject3.name.Replace("(Clone)", string.Empty).Trim();
				this.ActiveObject = gameObject3;
				Vector3[] minMax = this.GetMinMax(gameObject3);
				this.PivotTransform.transform.position = (minMax[0] + minMax[1]) * 0.5f;
			});
			gameObject.transform.SetParent(this.ButtonPanel.transform);
		}
		foreach (RoomSegment roomSegment in from x in this.BuildObjects.RoomSegments
		select x.GetComponent<RoomSegment>())
		{
			RoomSegment localItem = roomSegment;
			GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
			gameObject2.GetComponentInChildren<Text>().text = roomSegment.name;
			gameObject2.GetComponentsInChildren<Image>()[0].color = Color.green;
			gameObject2.GetComponentsInChildren<Image>()[1].sprite = roomSegment.Thumbnail;
			gameObject2.GetComponentsInChildren<Image>()[2].enabled = false;
			gameObject2.GetComponent<Button>().onClick.AddListener(delegate
			{
				if (this.ActiveObject != null)
				{
					UnityEngine.Object.Destroy(this.ActiveObject);
				}
				this.IsFurn = false;
				this.ActiveSegmentPrefab = localItem;
				GameObject gameObject3 = UnityEngine.Object.Instantiate<GameObject>(localItem.gameObject);
				gameObject3.GetComponent<RoomSegment>().IsTemporary = true;
				this.WallCube.SetActive(false);
				this.SideWall1.SetActive(true);
				this.SideWall2.SetActive(true);
				this.UpperWall.SetActive(true);
				this.UpperWall.transform.localScale = new Vector3(localItem.WallWidth, 0.2f, 1f);
				this.SideWall1.transform.position = new Vector3(localItem.WallWidth / 2f + 0.5f, 1f, 0f);
				this.SideWall2.transform.position = new Vector3(-localItem.WallWidth / 2f - 0.5f, 1f, 0f);
				this.GroundPlane.transform.position = new Vector3(localItem.WallWidth / 2f, 0f, 0f);
				gameObject3.name = gameObject3.name.Replace("(Clone)", string.Empty).Trim();
				this.ActiveObject = gameObject3;
				Vector3[] minMax = this.GetMinMax(gameObject3);
				this.PivotTransform.transform.position = (minMax[0] + minMax[1]) * 0.5f;
			});
			gameObject2.transform.SetParent(this.ButtonPanel.transform);
		}
	}

	public void CopyBoundary()
	{
		if (this.ActiveObject != null && this.IsFurn)
		{
			Furniture component = this.ActiveObject.GetComponent<Furniture>();
			component.NavBoundary = component.BuildBoundary.ToArray<Vector2>();
		}
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			this.lastMouse = Input.mousePosition;
		}
		if (!EventSystem.current.IsPointerOverGameObject())
		{
			if (Input.GetMouseButton(0))
			{
				Vector3 vector = Input.mousePosition - this.lastMouse;
				this.RotX -= vector.y;
				this.RotY += vector.x;
				this.PivotTransform.rotation = Quaternion.Euler(this.RotX, this.RotY, 0f);
			}
			this.MainCamTransform.localPosition = new Vector3(0f, 0f, this.MainCamTransform.localPosition.z + Input.mouseScrollDelta.y);
		}
		this.lastMouse = Input.mousePosition;
	}

	public Vector3[] GetMinMax(GameObject obj)
	{
		Vector3 vector = Vector3.one * 500f;
		Vector3 vector2 = Vector3.one * -500f;
		MeshFilter[] componentsInChildren = obj.GetComponentsInChildren<MeshFilter>();
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			MeshFilter meshFilter = componentsInChildren[i];
			Renderer component = meshFilter.GetComponent<Renderer>();
			if (!(component == null) && component.enabled)
			{
				MeshFilter localItem = meshFilter;
				foreach (Vector3 v in from x in meshFilter.mesh.vertices
				select localItem.transform.localToWorldMatrix.MultiplyPoint(x))
				{
					vector = Utilities.MinVector(vector, v);
					vector2 = Utilities.MaxVector(vector2, v);
				}
			}
		}
		return new Vector3[]
		{
			vector,
			vector2
		};
	}

	public void Preview()
	{
		if (this.ActiveObject != null)
		{
			this.ActivateSegmentWall(false);
			bool activeSelf = this.WallCube.activeSelf;
			bool activeSelf2 = this.UpperWall.activeSelf;
			this.SideWall1.SetActive(false);
			this.SideWall2.SetActive(false);
			this.UpperWall.SetActive(false);
			this.WallCube.SetActive(false);
			this.GroundPlane.SetActive(false);
			this.ArrowPlane.SetActive(false);
			Vector3[] minMax = this.GetMinMax(this.ActiveObject);
			this.ThumbnailPivot.transform.position = (minMax[0] + minMax[1]) * 0.5f;
			this.ThumbnailCam.orthographicSize = (minMax[0] - minMax[1]).magnitude / 2f;
			this.ThumbnailCam.Render();
			this.GroundPlane.SetActive(true);
			this.ArrowPlane.SetActive(true);
			this.WallCube.SetActive(activeSelf);
			this.ActivateSegmentWall(true);
			this.SideWall1.SetActive(activeSelf2);
			this.SideWall2.SetActive(activeSelf2);
			this.UpperWall.SetActive(activeSelf2);
		}
	}

	private void ActivateSegmentWall(bool active)
	{
		if (this.ActiveObject != null && !this.IsFurn)
		{
			foreach (MeshFilter meshFilter in this.ActiveObject.GetComponent<RoomSegment>().GetAllWallMeshes())
			{
				meshFilter.gameObject.SetActive(active);
			}
		}
	}

	public void TakePicture()
	{
	}

	public Camera ThumbnailCam;

	public RenderTexture Target;

	public Material BlitMat;

	public Vector3 lastMouse;

	public Transform PivotTransform;

	public Transform ThumbnailPivot;

	public Transform MainCamTransform;

	public float RotX;

	public float RotY;

	public GameObject ButtonPanel;

	public GameObject ButtonPrefab;

	public ObjectDatabase BuildObjects;

	public bool IsFurn = true;

	public GameObject ActiveObject;

	public GameObject GroundPlane;

	public GameObject ArrowPlane;

	public GameObject WallCube;

	public Furniture ActiveFurniturePrefab;

	public RoomSegment ActiveSegmentPrefab;

	public GameObject SideWall1;

	public GameObject SideWall2;

	public GameObject UpperWall;
}
