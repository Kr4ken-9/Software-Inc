﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using UnityEngine;

[Serializable]
[StructLayout(LayoutKind.Sequential)]
public class SVector3
{
	public SVector3()
	{
	}

	public SVector3(float X, float Y, float Z, float W = 0f)
	{
		this.x = X;
		this.y = Y;
		this.z = Z;
		this.w = W;
	}

	public SVector3(string txt)
	{
		float[] array = txt.Split(new char[]
		{
			','
		}).SelectInPlace((string k) => (float)Convert.ToDouble(k));
		this.x = array[0];
		this.y = array[1];
		this.z = array[2];
		this.w = array[3];
	}

	protected bool Equals(SVector3 other)
	{
		return this.x.Equals(other.x) && this.y.Equals(other.y) && this.z.Equals(other.z) && this.w.Equals(other.w);
	}

	public override bool Equals(object obj)
	{
		return !object.ReferenceEquals(null, obj) && (object.ReferenceEquals(this, obj) || (obj.GetType() == base.GetType() && this.Equals((SVector3)obj)));
	}

	public override int GetHashCode()
	{
		int num = this.x.GetHashCode();
		num = (num * 397 ^ this.y.GetHashCode());
		num = (num * 397 ^ this.z.GetHashCode());
		return num * 397 ^ this.w.GetHashCode();
	}

	public static implicit operator SVector3(Rect v)
	{
		return new SVector3(v.x, v.y, v.width, v.height);
	}

	public static implicit operator SVector3(Vector3 v)
	{
		return new SVector3(v.x, v.y, v.z, 0f);
	}

	public static implicit operator SVector3(Vector4 v)
	{
		return new SVector3(v.x, v.y, v.z, v.w);
	}

	public static implicit operator SVector3(Quaternion q)
	{
		return new SVector3(q.x, q.y, q.z, q.w);
	}

	public static implicit operator SVector3(Color c)
	{
		return new SVector3(c.r, c.g, c.b, c.a);
	}

	public static implicit operator SVector3(Vector2 c)
	{
		return new SVector3(c.x, c.y, 0f, 0f);
	}

	public static implicit operator Rect(SVector3 c)
	{
		return new Rect(c.x, c.y, c.z, c.w);
	}

	public static implicit operator Vector2(SVector3 c)
	{
		return new Vector2(c.x, c.y);
	}

	public static implicit operator Vector3(SVector3 v)
	{
		return new Vector3(v.x, v.y, v.z);
	}

	public static implicit operator Vector4(SVector3 v)
	{
		return new Vector4(v.x, v.y, v.z, v.w);
	}

	public static implicit operator Quaternion(SVector3 q)
	{
		return new Quaternion(q.x, q.y, q.z, q.w);
	}

	public static implicit operator Color(SVector3 q)
	{
		return new Color(q.x, q.y, q.z, q.w);
	}

	public Quaternion ToQuaternion()
	{
		return new Quaternion(this.x, this.y, this.z, this.w);
	}

	public Color ToColor()
	{
		return new Color(this.x, this.y, this.z, this.w);
	}

	public Rect ToRect()
	{
		return new Rect(this.x, this.y, this.w, this.z);
	}

	public Vector2 ToVector2()
	{
		return new Vector2(this.x, this.y);
	}

	public Vector2 ToVector2Z()
	{
		return new Vector2(this.x, this.z);
	}

	public Vector3 ToVector3()
	{
		return new Vector3(this.x, this.y, this.z);
	}

	public Vector4 ToVector4()
	{
		return new Vector4(this.x, this.y, this.z, this.w);
	}

	public string Serialize()
	{
		CultureInfo invariantCulture = CultureInfo.InvariantCulture;
		return string.Format("{0},{1},{2},{3}", new object[]
		{
			this.x.ToString(invariantCulture),
			this.y.ToString(invariantCulture),
			this.z.ToString(invariantCulture),
			this.w.ToString(invariantCulture)
		});
	}

	public static SVector3 Deserialize(string input, bool throwEx = false)
	{
		CultureInfo invariantCulture = CultureInfo.InvariantCulture;
		string[] array = input.Split(new char[]
		{
			','
		});
		float[] array2 = new float[4];
		for (int i = 0; i < Mathf.Min(array.Length, 4); i++)
		{
			if (throwEx)
			{
				array2[i] = (float)Convert.ToDouble(array[i], invariantCulture);
			}
			else
			{
				try
				{
					array2[i] = (float)Convert.ToDouble(array[i], invariantCulture);
				}
				catch (Exception)
				{
					break;
				}
			}
		}
		return new SVector3(array2[0], array2[1], array2[2], array2[3]);
	}

	public override string ToString()
	{
		return string.Concat(new object[]
		{
			this.x,
			",",
			this.y,
			",",
			this.z,
			",",
			this.w
		});
	}

	public float x;

	public float y;

	public float z;

	public float w;
}
