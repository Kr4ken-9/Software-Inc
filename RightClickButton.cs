﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class RightClickButton : MonoBehaviour
{
	public void Init()
	{
		if (this.Order == SelectorController.ContextButtonGroup.Group)
		{
			this.Icon.color = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, 0);
			ShortcutExtensions46.DOColor(this.Icon, new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue), 0.2f);
			this.MainColor = new Color32(150, 150, 150, 191);
		}
		else
		{
			this.Icon.color = new Color32(49, 49, 49, 0);
			ShortcutExtensions46.DOColor(this.Icon, new Color32(49, 49, 49, byte.MaxValue), 0.2f);
			this.MainColor = RightClickButton.ButtonColors[(int)(this.Order % (SelectorController.ContextButtonGroup)RightClickButton.ButtonColors.Length)].Alpha(0.7490196f);
		}
		Image component = base.GetComponent<Image>();
		component.fillAmount = 0f;
		ShortcutExtensions46.DOFillAmount(component, (this.DegWidth - 1f) / 360f, 0.2f);
		component.color = this.MainColor;
		this.r = base.GetComponent<RectTransform>();
		this.r.rotation = Quaternion.identity;
		ShortcutExtensions.DORotate(this.r, new Vector3(0f, 0f, this.Pos), 0.2f, 0);
		if (this.Order == SelectorController.ContextButtonGroup.Group)
		{
			this.r.localScale = new Vector3(1.05f, 1.05f, 1f);
		}
		this.Icon.GetComponent<RectTransform>().anchoredPosition = Quaternion.Euler(0f, 0f, -this.DegWidth / 2f) * new Vector2(0f, 96f);
		this.Icon.transform.rotation = Quaternion.Euler(0f, 0f, -this.Pos);
	}

	private void Update()
	{
		Vector2 a = new Vector2(this.MainPanel.transform.position.x, this.MainPanel.transform.position.y);
		Vector2 b = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		float magnitude = (a - b).magnitude;
		float num = Mathf.DeltaAngle(Mathf.Atan2(b.y - a.y, b.x - a.x) * 57.29578f, this.Pos + 90f);
		if (magnitude > 64f && magnitude < 128f && num > 0f && num < this.DegWidth)
		{
			if (!this.Highlight)
			{
				ShortcutExtensions46.DOColor(base.GetComponent<Image>(), new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, 191), 0.5f);
				ShortcutExtensions.DOScale(base.GetComponent<RectTransform>(), new Vector3(1.1f, 1.1f, 1f), 0.5f);
				this.MainPanel.Description.text = this.Description;
				this.MainPanel.KillRingTween();
				this.MainPanel.ActiveRingTween = ShortcutExtensions46.DOColor(this.MainPanel.CenterRing.GetComponent<Image>(), this.MainColor + new Color(0.25f, 0.25f, 0.25f, 0f), 0.5f);
				this.Highlight = true;
				UISoundFX.PlaySFX("HighlightTick", -1f, 0f);
			}
		}
		else if (this.Highlight)
		{
			ShortcutExtensions46.DOColor(base.GetComponent<Image>(), this.MainColor, 0.5f);
			ShortcutExtensions.DOScale(base.GetComponent<RectTransform>(), (this.Order != SelectorController.ContextButtonGroup.Group) ? new Vector3(1f, 1f, 1f) : new Vector3(1.05f, 1.05f, 1f), 0.5f);
			if (this.MainPanel.Description.text == this.Description)
			{
				this.MainPanel.Description.text = string.Empty;
				this.MainPanel.KillRingTween();
				this.MainPanel.ActiveRingTween = ShortcutExtensions46.DOColor(this.MainPanel.CenterRing.GetComponent<Image>(), new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, 191), 0.5f);
			}
			this.Highlight = false;
		}
		if (this.Highlight && Input.GetMouseButtonUp(0) && this.OnClick != null)
		{
			UISoundFX.PlaySFX("ButtonClick", -1f, 0f);
			this.OnClick();
		}
	}

	public float Pos;

	public float DegWidth;

	public string Description;

	public RightClickPanel MainPanel;

	public Image Icon;

	public Action OnClick;

	private RectTransform r;

	public SelectorController.ContextButtonGroup Order = SelectorController.ContextButtonGroup.Manage;

	public Color MainColor;

	public static Color[] ButtonColors = new Color[]
	{
		new Color32(220, 123, 142, 191),
		new Color32(196, 158, 238, 191),
		new Color32(126, 161, 204, 191),
		new Color32(225, 177, 133, 191),
		new Color32(152, 207, 142, 191),
		new Color32(119, 210, 213, 191)
	};

	private bool Highlight;
}
