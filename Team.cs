﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Team
{
	public Team(string name)
	{
		this.Name = name;
	}

	public Actor Leader
	{
		get
		{
			return this._leader;
		}
		set
		{
			if (this._leader != value && this.MeetingTable != null)
			{
				this.MeetingTable.ReserveTables(false, false);
				this.MeetingTable = null;
				this.Meeting.Clear();
			}
			this._leader = value;
		}
	}

	public bool HREnabled
	{
		get
		{
			return this.Leader != null && this.Leader.employee.HR;
		}
	}

	public float Compatibility
	{
		get
		{
			if (this.Employees.Count == 0 || GameSettings.Instance == null)
			{
				return -1f;
			}
			if (this.compatibility == -1f)
			{
				this.CalculateCompatibility();
			}
			float num = this.compatibility;
			if (this.Leader != null)
			{
				num *= this.Leader.employee.Leadership.MapRange(-1f, 1f, 0.5f, 2f, false);
				if (num < 1f)
				{
					num = Mathf.Lerp(num, 1f, this.Leader.employee.GetSkill(Employee.EmployeeRole.Lead));
				}
			}
			return num;
		}
	}

	public int Count
	{
		get
		{
			return this.Employees.Count;
		}
	}

	public override string ToString()
	{
		return this.Name;
	}

	public int GetNextVacation(Actor emp)
	{
		if (this.VacationSpread == 0)
		{
			return this.VacationMonth;
		}
		int num = this.Employees.IndexOf(emp);
		return (this.VacationMonth + num % (this.VacationSpread + 1)) % 12;
	}

	public void CalculateCompatibility()
	{
		this.Employees.RemoveAll((Actor x) => x == null);
		if (this.Employees.Count == 0)
		{
			this.compatibility = -1f;
			return;
		}
		if (this.Employees.Count == 1)
		{
			this.Employees[0].TeamCompatibility = 1f;
			this.compatibility = 1f;
			return;
		}
		float[] array = new float[this.Employees.Count];
		for (int i = 0; i < this.Employees.Count; i++)
		{
			for (int j = i + 1; j < this.Employees.Count; j++)
			{
				float num = this.Employees[i].employee.Compatibility(this.Employees[j].employee);
				array[i] += num;
				array[j] += num;
			}
		}
		float num2 = 0f;
		for (int k = 0; k < this.Employees.Count; k++)
		{
			this.Employees[k].TeamCompatibility = array[k] / (float)(this.Employees.Count - 1);
			num2 += this.Employees[k].TeamCompatibility;
		}
		this.compatibility = num2 / (float)this.Employees.Count;
		if (this.Compatibility < 0.1f && HUD.Instance != null)
		{
			HUD.Instance.AddPopupMessage("LowCompatWarning".Loc(new object[]
			{
				this.Name
			}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Issue, 0.25f, PopupManager.PopupIDs.Teamcompat, 1);
		}
	}

	public void AddEmployee(Actor actor)
	{
		if (this.Employees.Contains(actor))
		{
			return;
		}
		if (actor.employee.IsRole(Employee.RoleBit.Lead))
		{
			if (this.Leader == null)
			{
				this.Leader = actor;
			}
			else
			{
				actor.employee.ChangeToNaturalRole(false);
			}
		}
		this.Employees.Add(actor);
		this.CalculateCompatibility();
		foreach (SoftwareWorkItem softwareWorkItem in this.WorkItems.OfType<SoftwareWorkItem>())
		{
			softwareWorkItem.UpdateWorking();
		}
	}

	public void RemoveEmployee(Actor actor)
	{
		if (actor == this.Leader)
		{
			this.Leader = null;
		}
		actor.TeamCompatibility = -1f;
		this.Employees.Remove(actor);
		this.CalculateCompatibility();
		foreach (SoftwareWorkItem softwareWorkItem in this.WorkItems.OfType<SoftwareWorkItem>())
		{
			softwareWorkItem.UpdateWorking();
		}
	}

	public bool HasEmployee(Actor actor)
	{
		return this.Employees.Contains(actor);
	}

	public Actor[] GetEmployees()
	{
		return this.Employees.ToArray();
	}

	public List<Actor> GetEmployeesDirect()
	{
		return this.Employees;
	}

	public void Deserialize(WriteDictionary dictionary)
	{
		this.Name = (string)dictionary["Name"];
		uint[] source = (uint[])dictionary["Employees"];
		this.VacationMonth = dictionary.Get<int>("VacationMonth", 6);
		this.VacationSpread = dictionary.Get<int>("VacationSpread", 1);
		this.Employees = (from x in source.Distinct<uint>()
		select (Actor)Writeable.STGetDeserializedObject(x) into x
		where x != null && x.gameObject != null
		select x).ToList<Actor>();
		this.Employees.ForEach(delegate(Actor x)
		{
			x.Team = this.Name;
		});
		this.Leader = (Actor)Writeable.STGetDeserializedObject(dictionary.Get<uint>("Leader", 0u));
		this.Talking = (Actor)Writeable.STGetDeserializedObject(dictionary.Get<uint>("Talking", 0u));
		this.WorkStart = (int)dictionary["WorkStart"];
		this.WorkEnd = (int)dictionary["WorkEnd"];
		this.CrunchMode = dictionary.Get<bool>("CrunchMode", false);
		object obj = Writeable.STGetDeserializedObject(dictionary.Get<uint>("MeetingTable", 0u));
		if (obj != null)
		{
			this.MeetingTable = ((Furniture)obj).GetComponent<TableScript>();
		}
		source = (uint[])dictionary["Meeting"];
		this.Meeting = (from x in source
		select (Actor)Writeable.STGetDeserializedObject(x)).ToList<Actor>();
		uint[] witems = dictionary.Get<uint[]>("workItems", new uint[0]);
		foreach (WorkItem item in from x in GameSettings.Instance.MyCompany.WorkItems
		where witems.Contains(x.ID)
		select x)
		{
			if (!this.WorkItems.Contains(item))
			{
				this.WorkItems.Add(item);
			}
		}
		GameSettings.Instance.sRoomManager.Rooms.ForEach(delegate(Room x)
		{
			if (x.TempTeam != null && x.TempTeam.Contains(this.Name))
			{
				x.AddTeam(this);
			}
		});
		this.HR = dictionary.Get<HRManagement>("HR", new HRManagement());
		this.CalculateCompatibility();
	}

	public float GetCompatibility(Employee employee)
	{
		float num = 0f;
		for (int i = 0; i < this.Employees.Count; i++)
		{
			num += this.Employees[i].employee.Compatibility(employee);
		}
		return num / (float)this.Employees.Count;
	}

	public WriteDictionary Serialize()
	{
		WriteDictionary writeDictionary = new WriteDictionary("Team");
		writeDictionary["Name"] = this.Name;
		writeDictionary["Employees"] = (from x in this.Employees
		where x != null && x.gameObject != null
		select x.DID).ToArray<uint>();
		writeDictionary["Leader"] = ((!(this.Leader != null)) ? 0u : this.Leader.DID);
		writeDictionary["WorkStart"] = this.WorkStart;
		writeDictionary["WorkEnd"] = this.WorkEnd;
		writeDictionary["MeetingTable"] = ((!(this.MeetingTable != null)) ? 0u : this.MeetingTable.FurnComp.DID);
		writeDictionary["Meeting"] = (from x in this.Meeting
		where x != null && x.gameObject != null
		select x.DID).ToArray<uint>();
		writeDictionary["Talking"] = ((!(this.Talking != null)) ? 0u : this.Talking.DID);
		WriteDictionary writeDictionary2 = writeDictionary;
		string key = "workItems";
		object value;
		if (this.WorkItems != null)
		{
			value = (from x in this.WorkItems
			select x.ID).ToArray<uint>();
		}
		else
		{
			value = null;
		}
		writeDictionary2[key] = value;
		writeDictionary["HR"] = this.HR;
		writeDictionary["VacationMonth"] = this.VacationMonth;
		writeDictionary["VacationSpread"] = this.VacationSpread;
		writeDictionary["CrunchMode"] = this.CrunchMode;
		return writeDictionary;
	}

	public static string GetCompatDesc(float compat)
	{
		float num = Mathf.Clamp(compat, 0f, 2f) / 2f;
		int num2 = Mathf.CeilToInt(num * 6f) + 1;
		return ("TeamCompat" + num2).Loc();
	}

	public int[] CountRoles(int[] r = null)
	{
		r = (r ?? new int[Employee.RoleCount]);
		for (int i = 0; i < this.Employees.Count; i++)
		{
			Actor actor = this.Employees[i];
			if (actor.employee.IsRole(Employee.RoleBit.Lead))
			{
				r[0]++;
			}
			else
			{
				r[(int)actor.employee.HiredFor]++;
			}
		}
		return r;
	}

	public void CheckWorkRoleAssignment()
	{
	}

	public int[] CountDesignDev()
	{
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < this.Employees.Count; i++)
		{
			Actor actor = this.Employees[i];
			if (actor.employee.IsRole(Employee.RoleBit.Designer))
			{
				num++;
			}
			if (actor.employee.IsRole(Employee.RoleBit.Programmer | Employee.RoleBit.Artist))
			{
				num2++;
			}
		}
		return new int[]
		{
			num,
			num2
		};
	}

	public void RescheduleVacations()
	{
		foreach (Actor actor in this.Employees)
		{
			actor.ScheduleVacation(false);
		}
	}

	public string Name;

	public int VacationMonth = 6;

	public int VacationSpread = 1;

	private List<Actor> Employees = new List<Actor>();

	private Actor _leader;

	public int WorkStart = 8;

	public int WorkEnd = 16;

	public TableScript MeetingTable;

	public List<Actor> Meeting = new List<Actor>();

	public Actor Talking;

	public List<WorkItem> WorkItems = new List<WorkItem>();

	private float compatibility = -1f;

	public HRManagement HR = new HRManagement();

	public bool CrunchMode;

	public float SpecializationMinCap;
}
