﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class SentenceGenerator
{
	public SentenceGenerator(string[] input)
	{
		int num = 0;
		SentenceGenerator.Node node = new SentenceGenerator.Node();
		List<string> list = new List<string>();
		for (int i = 0; i < input.Length; i++)
		{
			string text = input[i];
			if (num != 0)
			{
				if (num == 1)
				{
					if (string.IsNullOrEmpty(text.Trim()) || text[0] == '-' || text[0] == '>')
					{
						node.Sentences = list.ToArray();
						list.Clear();
						i--;
						num = 0;
					}
					else
					{
						list.Add(text);
					}
				}
			}
			else if (text.Length > 0 && text[0] == '-')
			{
				node = new SentenceGenerator.Node();
				int num2 = text.IndexOf('(');
				int num3 = text.IndexOf(')');
				int num4 = text.IndexOf('[');
				int num5 = text.IndexOf(']');
				string key = text.Substring(1, num2 - 1);
				this.Nodes[key] = node;
				if (num2 == num3 - 1)
				{
					node.Children = new string[0];
				}
				else
				{
					node.Children = text.Substring(num2 + 1, num3 - num2 - 1).Split(new char[]
					{
						','
					});
				}
				if (num4 == num5 - 1 || num4 < 0)
				{
					node.HasReq = false;
				}
				else
				{
					float[] array = (from x in text.Substring(num4 + 1, num5 - num4 - 1).Split(new char[]
					{
						','
					})
					select (float)Convert.ToDouble(x)).ToArray<float>();
					node.HasReq = true;
					node.LowerBound = array[0];
					node.Index = (int)array[1];
					node.UpperBound = array[2];
				}
				num = 1;
			}
		}
		if (node.Sentences == null)
		{
			node.Sentences = list.ToArray();
		}
	}

	public string GenerateSentence(SentenceGenerator.Node node, params float[] values)
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (node.Sentences.Length > 0)
		{
			stringBuilder.Append(node.Sentences.GetRandom<string>() + " ");
		}
		foreach (SentenceGenerator.Node node2 in from x in node.Children
		select this.Nodes[x])
		{
			if (!node2.HasReq || (values[node2.Index] >= node2.LowerBound && values[node2.Index] < node2.UpperBound))
			{
				stringBuilder.Append(this.GenerateSentence(node2, values));
			}
		}
		return stringBuilder.ToString();
	}

	public string GenerateSentence(string FromNode, params float[] values)
	{
		return this.GenerateSentence(this.Nodes[FromNode], values);
	}

	private Dictionary<string, SentenceGenerator.Node> Nodes = new Dictionary<string, SentenceGenerator.Node>();

	public class Node
	{
		public string[] Sentences;

		public int Index;

		public float LowerBound;

		public float UpperBound;

		public string[] Children;

		public bool HasReq;
	}
}
