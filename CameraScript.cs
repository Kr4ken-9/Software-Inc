﻿using System;
using DevConsole;
using SSAA;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CinematicEffects;
using UnityStandardAssets.ImageEffects;

public class CameraScript : MonoBehaviour
{
	public static float ScreenUpscale
	{
		get
		{
			return (float)Options.SSAA / 10f;
		}
	}

	private void Start()
	{
		this.mainCam = Camera.main;
		AudioSource[] components = base.GetComponents<AudioSource>();
		this.WinterWind = components[0];
		this.HighWind = components[1];
		this.BirdSound = components[2];
		this.CricketSound = components[3];
		this.PipeSound = components[4];
		if (CameraScript.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		CameraScript.Instance = this;
		CameraScript.ApplyOptions();
		CameraScript.ApplySpeeds();
		this.lastPos = Input.mousePosition;
		this.UpdatePostFX();
		this.lastBird = 5f;
		this.DataColors = this.mainCam.GetComponents<ColorCorrectionLookup>()[2];
	}

	public static void ApplySpeeds()
	{
		if (CameraScript.Instance != null)
		{
			CameraScript.Instance.ScrollSpeed = Options.ScrollSpeed;
			CameraScript.Instance.ZoomSpeed = Options.ZoomSpeed;
			CameraScript.Instance.RotationSpeed = Options.RotationSpeed;
		}
	}

	public static void GotoPos(Vector2 pos)
	{
		if (CameraScript.Instance != null)
		{
			CameraScript.Instance.TargetPos = pos;
			CameraScript.Instance.GotoTarget = true;
		}
	}

	public static void ApplyOptions()
	{
		if (CameraScript.Instance != null)
		{
			CameraScript.Instance.SSAAObj.enabled = (Options.SSAA > 10);
			internal_SSAA.ChangeScale((float)Options.SSAA / 10f);
			CameraScript.Instance.SSR.enabled = Options.SSR;
			CameraScript.Instance.SSAO.enabled = Options.AmbientOcclusion;
			CameraScript.Instance.Bloom.enabled = Options.Bloom;
			CameraScript.Instance.TiltScript.enabled = Options.TiltShift;
			CameraScript.Instance.AntiAlias.enabled = Options.FXAA;
			CameraScript.Instance.SMAA.enabled = Options.SMAA;
			CameraScript.Instance.UpdatePostFX();
			CameraScript.Instance.ColorCorrection.redChannel.MoveKey(1, new Keyframe(0.5f, Options.Gamma, 1f, 1f));
			CameraScript.Instance.ColorCorrection.greenChannel.MoveKey(1, new Keyframe(0.5f, Options.Gamma, 1f, 1f));
			CameraScript.Instance.ColorCorrection.blueChannel.MoveKey(1, new Keyframe(0.5f, Options.Gamma, 1f, 1f));
			CameraScript.Instance.ColorCorrection.UpdateParameters();
		}
		else if (MainMenuController.Instance != null)
		{
			MainMenuController.Instance.ApplyOptions();
		}
	}

	private void OnDestroy()
	{
		CameraScript.Instance = null;
	}

	private void Update()
	{
		this.FloorLabel.text = GameSettings.Instance.ActiveFloor.ToString();
		this.wasDragging = false;
		if (this.isDragging && InputController.GetKeyUp(InputController.Keys.DragCamera, true))
		{
			this.isDragging = false;
			this.Momentum = (base.transform.position - this.LastPosMomentum).FlattenVector3() * (-this.DragMomentum / this.mainCam.transform.localPosition.z);
			this.wasDragging = ((this.LastPos - base.transform.position).magnitude > 0.2f);
			if (!this.wasDragging)
			{
				this.Momentum = Vector2.zero;
			}
		}
		this.DataColors.enabled = DataOverlay.HasActive;
		if (this.DoneSaving)
		{
			this.SaveIndicator.enabled = false;
			this.DoneSaving = false;
		}
		float num = (GameSettings.GameSpeed != 0f) ? 1f : 0f;
		this.lastBird -= Time.deltaTime;
		if (this.lastBird <= 0f)
		{
			this.birdPlay = 1 - this.birdPlay;
			this.lastBird = UnityEngine.Random.Range(2f, 10f);
		}
		float t = Time.deltaTime * (float)((GameSettings.GameSpeed != 0f) ? 2 : 10);
		this.HighWind.volume = Mathf.Lerp(this.HighWind.volume, (GameSettings.Instance.ActiveFloor != -1) ? (this.HighSfx.Evaluate(-this.mainCam.transform.localPosition.z / 600f) * num) : 0f, t);
		float snowTemp = TimeOfDay.Instance.GetSnowTemp(0f);
		float num2 = (GameSettings.Instance.ActiveFloor != -1) ? (this.LowSfx.Evaluate(-this.mainCam.transform.localPosition.z / 600f) * num) : 0f;
		this.WinterWind.volume = Mathf.Lerp(this.WinterWind.volume, num2 * snowTemp, t);
		float num3 = this.DaySfx.Evaluate(((float)TimeOfDay.Instance.Hour + TimeOfDay.Instance.Minute / 60f) / 24f);
		this.BirdSound.volume = ((GameSettings.Instance.CliType != GameData.ClimateType.Warm) ? Mathf.Lerp(this.BirdSound.volume, (float)this.birdPlay * num2 * (1f - snowTemp) * 0.5f * num3, t) : 0f);
		this.CricketSound.volume = Mathf.Lerp(this.CricketSound.volume, num2 * (1f - snowTemp) * 0.5f * (1f - num3), t);
		this.PipeSound.volume = Mathf.Lerp(this.PipeSound.volume, (GameSettings.Instance.ActiveFloor != -1) ? 0f : 0.75f, t);
		if (!GameSettings.FreezeGame || this.FlyMode)
		{
			if (InputController.GetKeyUp(InputController.Keys.ToggleCamera, false) || (GameSettings.FreezeGame && this.FlyMode))
			{
				this.FlyMode = !this.FlyMode;
				if (this.FlyMode)
				{
					this.Momentum = Vector2.zero;
					HUD.Instance.BuildMode = false;
					BuildController.Instance.ClearBuild(false, false, false, false);
					this.LastFloor = GameSettings.Instance.ActiveFloor;
					GameSettings.Instance.ActiveFloor = 100;
					GameSettings.Instance.sRoomManager.ChangeFloor();
					this.FOV = this.mainCam.fieldOfView;
					this.TiltScript.blurArea = 0f;
					this.SSAO.Intensity = 2f;
					this.SSAO.Radius = 1.2f;
					this.FOV = 40f;
				}
				else
				{
					GameSettings.Instance.ActiveFloor = this.LastFloor;
					GameSettings.Instance.sRoomManager.ChangeFloor();
				}
				Furniture.UpdateEdgeDetection();
				this.Fog.heightFog = !this.FlyMode;
				this.Fog.startDistance = (float)((!this.FlyMode) ? 0 : 10);
				this.mainCam.fieldOfView = 15f;
				this.mainCam.nearClipPlane = ((!this.FlyMode) ? 2f : 0.5f);
				this.mainCam.farClipPlane = ((!this.FlyMode) ? 700f : (125f * CameraScript.FlyCamDistance));
				this.SSAO.CutoffDistance = ((!this.FlyMode) ? 500f : (100f * CameraScript.FlyCamDistance));
				this.mainCam.transform.localPosition = new Vector3(0f, 0f, this.mainCam.transform.localPosition.z);
				this.mainCam.transform.localRotation = Quaternion.identity;
				this.Target = this.mainCam.transform.rotation;
				Cursor.lockState = ((!this.FlyMode) ? Options.CursorLock() : CursorLockMode.Locked);
				Cursor.visible = !this.FlyMode;
			}
			if (this.FlyMode)
			{
				this.FlyCam();
			}
			else
			{
				Cursor.lockState = ((!this.FlyMode) ? Options.CursorLock() : CursorLockMode.Locked);
				Cursor.visible = !this.FlyMode;
				this.StandardCam();
			}
			this.FloorHelper.gameObject.SetActive(GameSettings.Instance.ActiveFloor > 0 && HUD.Instance.BuildMode);
			if (this.FloorHelper.gameObject.activeSelf)
			{
				this.FloorHelper.localScale = new Vector3(this.FloorHelper.localScale.x, (float)GameSettings.Instance.ActiveFloor, this.FloorHelper.localScale.z);
				this.FloorHelper.position = new Vector3(base.transform.position.x, (float)GameSettings.Instance.ActiveFloor + 0.02f, base.transform.position.z);
				this.FloorHelper.rotation = Quaternion.identity;
			}
		}
	}

	private void OnFileSaved(int id, string filepath)
	{
		this.DoneSaving = true;
	}

	public void FlyCam()
	{
		Vector3 vector = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0f);
		this.Target *= Quaternion.Euler(-vector.y * Time.deltaTime * this.RotationSpeed * 10f, vector.x * Time.deltaTime * this.RotationSpeed * 10f, 0f);
		this.Target = Quaternion.Euler(this.Target.eulerAngles.x, this.Target.eulerAngles.y, 0f);
		this.mainCam.transform.rotation = Quaternion.Lerp(this.mainCam.transform.rotation, this.Target, 0.25f);
		Vector3 a = Vector3.zero;
		if (this.FlyLockFloor)
		{
			if (InputController.GetKey(InputController.Keys.MoveUp, true))
			{
				a += this.mainCam.transform.forward;
			}
			if (InputController.GetKey(InputController.Keys.MoveLeft, true))
			{
				a -= this.mainCam.transform.right;
			}
			if (InputController.GetKey(InputController.Keys.MoveDown, true))
			{
				a -= this.mainCam.transform.forward;
			}
			if (InputController.GetKey(InputController.Keys.MoveRight, true))
			{
				a += this.mainCam.transform.right;
			}
			if (InputController.GetKeyDown(InputController.Keys.FloorUp, false))
			{
				this.FlyFloor++;
			}
			if (InputController.GetKeyDown(InputController.Keys.FloorDown, false))
			{
				this.FlyFloor--;
			}
			a = new Vector3(a.x, 0f, a.z);
			bool key = Input.GetKey(KeyCode.LeftShift);
			this.mainCam.transform.position = this.mainCam.transform.position + a.normalized * Time.deltaTime * this.ScrollSpeed / (float)((!key) ? 8 : 2);
			this.mainCam.transform.position = new Vector3(this.mainCam.transform.position.x, Mathf.Lerp(this.mainCam.transform.position.y, (float)this.FlyFloor * 2f - 0.7f, Time.deltaTime * 10f), this.mainCam.transform.position.z);
		}
		else
		{
			if (InputController.GetKey(InputController.Keys.MoveUp, true))
			{
				a += Vector3.forward;
			}
			if (InputController.GetKey(InputController.Keys.MoveLeft, true))
			{
				a += Vector3.left;
			}
			if (InputController.GetKey(InputController.Keys.MoveDown, true))
			{
				a += Vector3.back;
			}
			if (InputController.GetKey(InputController.Keys.MoveRight, true))
			{
				a += Vector3.right;
			}
			if (InputController.GetKey(InputController.Keys.FloorUp, true))
			{
				a += Vector3.up;
			}
			if (InputController.GetKey(InputController.Keys.FloorDown, true))
			{
				a += Vector3.down;
			}
			bool key2 = Input.GetKey(KeyCode.LeftShift);
			this.mainCam.transform.position = this.mainCam.transform.position + this.mainCam.transform.rotation * a.normalized * Time.deltaTime * this.ScrollSpeed / (float)((!key2) ? 8 : 2);
		}
		if (Input.GetKeyDown(KeyCode.LeftControl))
		{
			this.FlyFloor = Mathf.FloorToInt((this.mainCam.transform.position.y + 1f) / 2f);
			this.FlyLockFloor = !this.FlyLockFloor;
		}
		this.lastPos = Input.mousePosition;
		this.FOV = Mathf.Clamp(this.FOV - Input.GetAxis("Mouse ScrollWheel") * this.ZoomSpeed * 0.5f, 10f, 120f);
		this.mainCam.fieldOfView = Mathf.Lerp(this.mainCam.fieldOfView, this.FOV, 0.1f);
		this.mainCam.transform.position = new Vector3(Mathf.Clamp(this.mainCam.transform.position.x, 0f, 256f), Mathf.Clamp(this.mainCam.transform.position.y, -2f, 128f), Mathf.Clamp(this.mainCam.transform.position.z, 0f, 256f));
	}

	public void StandardCam()
	{
		if (HUD.Instance.BuildMode && InputController.GetKeyDown(InputController.Keys.TopDown, false))
		{
			this.TopDown = !this.TopDown;
			Furniture.UpdateEdgeDetection();
		}
		float num = this.ScrollSpeed * this.mainCam.orthographicSize / 20f * Time.deltaTime;
		Vector3 a = Vector3.zero;
		if (InputController.GetKey(InputController.Keys.MoveUp, false))
		{
			this.GotoTarget = false;
			float f = base.transform.rotation.eulerAngles.y / 180f * 3.14159274f;
			a = new Vector3(a.x + Mathf.Sin(f) * num, a.y, a.z + Mathf.Cos(f) * num);
		}
		if (InputController.GetKey(InputController.Keys.MoveDown, false))
		{
			this.GotoTarget = false;
			float f2 = base.transform.rotation.eulerAngles.y / 180f * 3.14159274f;
			a = new Vector3(a.x - Mathf.Sin(f2) * num, a.y, a.z - Mathf.Cos(f2) * num);
		}
		if (InputController.GetKey(InputController.Keys.MoveLeft, false))
		{
			this.GotoTarget = false;
			float f3 = (base.transform.rotation.eulerAngles.y - 90f) / 180f * 3.14159274f;
			a = new Vector3(a.x + Mathf.Sin(f3) * num, a.y, a.z + Mathf.Cos(f3) * num);
		}
		if (InputController.GetKey(InputController.Keys.MoveRight, false))
		{
			this.GotoTarget = false;
			float f4 = (base.transform.rotation.eulerAngles.y + 90f) / 180f * 3.14159274f;
			a = new Vector3(a.x + Mathf.Sin(f4) * num, a.y, a.z + Mathf.Cos(f4) * num);
		}
		if (this.GotoTarget)
		{
			base.transform.position = new Vector3(Mathf.Lerp(base.transform.position.x, this.TargetPos.x, Time.deltaTime * 8f), base.transform.position.y, Mathf.Lerp(base.transform.position.z, this.TargetPos.y, Time.deltaTime * 8f));
		}
		if (Options.EdgeScroll)
		{
			this.EdgeScroll(ref a, num);
		}
		a += this.Momentum.ToVector3(0f);
		this.Momentum = Vector2.Lerp(this.Momentum, Vector2.zero, Time.deltaTime * this.DragSlowdown);
		if (BuildController.Instance.CanChangeFloor())
		{
			if (InputController.GetKeyDown(InputController.Keys.FloorDown, false))
			{
				int activeFloor = GameSettings.Instance.ActiveFloor;
				GameSettings.Instance.ActiveFloor = Mathf.Max(-1, GameSettings.Instance.ActiveFloor - 1);
				if ((BuildController.Instance.IsBuildingRoom() && BuildController.Instance.FenceMode) || RoadBuildCube.Instance.gameObject.activeSelf)
				{
					GameSettings.Instance.ActiveFloor = Mathf.Max(0, GameSettings.Instance.ActiveFloor);
				}
				this.UpdateFloorSound(activeFloor);
				Furniture.UpdateEdgeDetection();
				GameSettings.Instance.sRoomManager.ChangeFloor();
			}
			if (InputController.GetKeyDown(InputController.Keys.FloorUp, false))
			{
				int activeFloor2 = GameSettings.Instance.ActiveFloor;
				GameSettings.Instance.ActiveFloor = Mathf.Min(GameSettings.MaxFloor, GameSettings.Instance.ActiveFloor + 1);
				this.UpdateFloorSound(activeFloor2);
				Furniture.UpdateEdgeDetection();
				GameSettings.Instance.sRoomManager.ChangeFloor();
			}
		}
		if (GameSettings.Instance.ActiveFloor == -1)
		{
			this.UndergroundMesh.SetActive(true);
			this.GroundMesh.SetActive(false);
		}
		else
		{
			this.UndergroundMesh.SetActive(false);
			this.GroundMesh.SetActive(true);
		}
		float num2 = Mathf.Abs(this.mainCam.transform.localPosition.z) / 125f;
		base.transform.position = new Vector3(Mathf.Clamp(base.transform.position.x + a.x * num2, 0f, 256f), Mathf.Lerp(base.transform.position.y, (float)(GameSettings.Instance.ActiveFloor * 2), 0.1f), Mathf.Clamp(base.transform.position.z + a.z * num2, 0f, 256f));
		if (InputController.GetKey(InputController.Keys.RotateCamera, false))
		{
			Vector3 vector = Input.mousePosition - this.lastPos;
			base.transform.rotation = Quaternion.Euler((!this.TopDown) ? Mathf.Clamp(base.transform.rotation.eulerAngles.x - vector.y * 0.01f * this.RotationSpeed, 8f, 65f) : base.transform.rotation.eulerAngles.x, base.transform.rotation.eulerAngles.y + vector.x * 0.01f * this.RotationSpeed, base.transform.rotation.eulerAngles.z);
			Furniture.UpdateEdgeDetection();
		}
		int num3 = ((!InputController.GetKey(InputController.Keys.TurnLeft, false)) ? 0 : 1) - ((!InputController.GetKey(InputController.Keys.TurnRight, false)) ? 0 : 1);
		int num4 = ((!InputController.GetKey(InputController.Keys.TurnDown, false)) ? 0 : 1) - ((!InputController.GetKey(InputController.Keys.TurnUp, false)) ? 0 : 1);
		if (num3 != 0 || num4 != 0)
		{
			Furniture.UpdateEdgeDetection();
		}
		num3 *= 5;
		num4 *= 5;
		base.transform.rotation = Quaternion.Euler((!this.TopDown) ? Mathf.Clamp(base.transform.rotation.eulerAngles.x - (float)num4 * Time.deltaTime * this.RotationSpeed, 8f, 65f) : base.transform.rotation.eulerAngles.x, base.transform.rotation.eulerAngles.y + (float)num3 * Time.deltaTime * this.RotationSpeed, base.transform.rotation.eulerAngles.z);
		if (this.TopDown)
		{
			base.transform.rotation = Quaternion.Euler(Mathf.Lerp(base.transform.rotation.eulerAngles.x, 90f, Time.deltaTime * 10f), base.transform.rotation.eulerAngles.y, 0f);
		}
		float num5 = ((!InputController.GetKey(InputController.Keys.ZoomIn, false)) ? 0f : Time.deltaTime) - ((!InputController.GetKey(InputController.Keys.ZoomOut, false)) ? 0f : Time.deltaTime);
		num5 *= 5f;
		float num6 = (!DevConsole.Console.isOpen && !EnvironmentEditor.DisableScroll()) ? Input.GetAxis("Mouse ScrollWheel") : 0f;
		float num7 = (num5 + ((!GUICheck.OverGUI) ? num6 : 0f)) * this.ZoomSpeed * num2;
		this.mainCam.transform.localPosition = new Vector3(this.mainCam.transform.localPosition.x, this.mainCam.transform.localPosition.y, Mathf.Clamp(this.mainCam.transform.localPosition.z + num7, -430f, -15f));
		if (!Mathf.Approximately(num7, 0f))
		{
			this.UpdatePostFX();
		}
		this.lastPos = Input.mousePosition;
		this.mainCam.fieldOfView = 15f;
		if (this.isDragging)
		{
			Ray ray = this.mainCam.ScreenPointToRay(Input.mousePosition);
			Plane plane = new Plane(Vector3.up, Vector3.up * (float)(GameSettings.Instance.ActiveFloor * 2));
			float distance = 0f;
			plane.Raycast(ray, out distance);
			Vector3 point = ray.GetPoint(distance);
			Vector3 vector2 = this.lastDragPos - point;
			this.LastPosMomentum = base.transform.position;
			base.transform.position = new Vector3(Mathf.Clamp(base.transform.position.x + vector2.x, 0f, 256f), base.transform.position.y, Mathf.Clamp(base.transform.position.z + vector2.z, 0f, 256f));
		}
		if (!BuildController.Instance.IsActive() && !GUICheck.OverGUI && InputController.GetKeyDown(InputController.Keys.DragCamera, false))
		{
			this.isDragging = true;
			this.Momentum = Vector2.zero;
			Ray ray2 = this.mainCam.ScreenPointToRay(Input.mousePosition);
			Plane plane2 = new Plane(Vector3.up, Vector3.up * (float)(GameSettings.Instance.ActiveFloor * 2));
			float distance2 = 0f;
			plane2.Raycast(ray2, out distance2);
			this.lastDragPos = ray2.GetPoint(distance2);
			this.LastPos = base.transform.position;
		}
	}

	public void UpdatePostFX()
	{
		float num = this.mainCam.transform.localPosition.z;
		num = Mathf.Clamp01((-num - 15f) / 185f);
		this.TiltScript.blurArea = this.TiltArea.Evaluate(num) * CameraScript.ScreenUpscale;
		this.SSAO.Intensity = this.AmbIntensity.Evaluate(num);
		this.SSAO.Radius = this.AmbRadius.Evaluate(num);
	}

	public void EdgeScroll(ref Vector3 movement, float TScrollSpeed)
	{
		float num = base.transform.rotation.eulerAngles.y / 180f * 3.14159274f;
		float num2 = ((Input.mousePosition.x < (float)(Screen.width - 2)) ? 0f : 1f) - ((Input.mousePosition.x > 2f) ? 0f : 1f);
		num2 = Mathf.Clamp(num2, -1f, 1f);
		float num3 = ((Input.mousePosition.y < (float)(Screen.height - 2)) ? 0f : 1f) - ((Input.mousePosition.y > 2f) ? 0f : 1f);
		num3 = Mathf.Clamp(num3, -1f, 1f);
		if (num3 != 0f || num2 != 0f)
		{
			this.GotoTarget = false;
		}
		movement = new Vector3(movement.x + Mathf.Sin(num) * TScrollSpeed * num3 + Mathf.Sin(num + 1.57079637f) * TScrollSpeed * num2, movement.y, movement.z + Mathf.Cos(num) * TScrollSpeed * num3 + Mathf.Cos(num + 1.57079637f) * TScrollSpeed * num2);
	}

	public void ChangeFloor(int amount)
	{
		if (BuildController.Instance.CanChangeFloor())
		{
			int activeFloor = GameSettings.Instance.ActiveFloor;
			GameSettings.Instance.ActiveFloor = Mathf.Clamp(GameSettings.Instance.ActiveFloor + amount, (!BuildController.Instance.IsBuildingRoom() || !BuildController.Instance.FenceMode) ? -1 : 0, GameSettings.MaxFloor);
			this.UpdateFloorSound(activeFloor);
			Furniture.UpdateEdgeDetection();
			GameSettings.Instance.sRoomManager.ChangeFloor();
		}
	}

	public void UpdateFloorSound(int last)
	{
		if (last != GameSettings.Instance.ActiveFloor)
		{
			float pitch = 1f + Mathf.Clamp((float)GameSettings.Instance.ActiveFloor / 50f, -0.5f, 0.5f);
			UISoundFX.PlaySFX((last >= GameSettings.Instance.ActiveFloor) ? "FloorDown" : "FloorUp", pitch, 0f);
		}
	}

	public void Deserialize(WriteDictionary input)
	{
		this.FlyMode = (bool)input["FlyMode"];
		GameSettings.Instance.ActiveFloor = (int)input["ActiveFloor"];
		base.transform.position = ((SVector3)input["Position"]).ToVector3();
		base.transform.rotation = ((SVector3)input["Rotation"]).ToQuaternion();
		this.mainCam.transform.position = ((SVector3)input["CamPosition"]).ToVector3();
		this.mainCam.transform.rotation = ((SVector3)input["CamRotation"]).ToQuaternion();
		this.UpdatePostFX();
	}

	public WriteDictionary Serialize()
	{
		WriteDictionary writeDictionary = new WriteDictionary("Camera");
		writeDictionary["FlyMode"] = this.FlyMode;
		writeDictionary["ActiveFloor"] = GameSettings.Instance.ActiveFloor;
		writeDictionary["Position"] = base.transform.position;
		writeDictionary["Rotation"] = base.transform.rotation;
		writeDictionary["CamPosition"] = this.mainCam.transform.position;
		writeDictionary["CamRotation"] = this.mainCam.transform.rotation;
		return writeDictionary;
	}

	public static float FlyCamDistance = 1f;

	private Vector2 TargetPos;

	private bool GotoTarget;

	private Vector3 lastPos;

	private Quaternion Target;

	public float ScrollSpeed = 20f;

	public float ZoomSpeed = 500f;

	public float RotationSpeed = 10f;

	public float DragMomentum = 10f;

	public float DragSlowdown = 1f;

	public bool FlyMode;

	public float FOV;

	public AntiAliasing SMAA;

	public SuperSampling_SSAA SSAAObj;

	public SSAOPro SSAO;

	public TiltShift TiltScript;

	public AnimationCurve TiltArea;

	public AnimationCurve AmbIntensity;

	public AnimationCurve AmbRadius;

	public AnimationCurve LowSfx;

	public AnimationCurve HighSfx;

	public AnimationCurve DaySfx;

	public Antialiasing AntiAlias;

	public BloomOptimized Bloom;

	public GlobalFog Fog;

	public ScreenSpaceReflection SSR;

	public static CameraScript Instance;

	private AudioSource WinterWind;

	private AudioSource HighWind;

	private AudioSource BirdSound;

	private AudioSource CricketSound;

	private AudioSource PipeSound;

	private int birdPlay = 1;

	private float lastBird;

	private int LastFloor;

	private Vector2 Momentum = Vector2.zero;

	[NonSerialized]
	public Camera mainCam;

	public GameObject GroundMesh;

	public GameObject UndergroundMesh;

	public RawImage SaveIndicator;

	private bool DoneSaving;

	public bool TopDown = true;

	public bool FlyLockFloor;

	public int FlyFloor;

	private ColorCorrectionLookup DataColors;

	public AudioListener Listener;

	private Vector3 lastDragPos;

	[NonSerialized]
	private bool isDragging;

	[NonSerialized]
	public bool wasDragging;

	public Vector3 LastPos;

	public Vector3 LastPosMomentum;

	public Transform FloorHelper;

	public ColorCorrectionCurves ColorCorrection;

	public Text FloorLabel;
}
