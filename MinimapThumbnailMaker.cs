﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MinimapThumbnailMaker : MonoBehaviour
{
	private void Awake()
	{
		if (MinimapThumbnailMaker.Instance != null)
		{
			MinimapThumbnailMaker.Instance.light.intensity = this.light.intensity;
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		this.BigTex = new RenderTexture(512, 512, 16)
		{
			antiAliasing = 1,
			autoGenerateMips = false,
			filterMode = FilterMode.Point
		};
		this.SmallTex = new RenderTexture(256, 256, 16)
		{
			antiAliasing = 1,
			autoGenerateMips = false,
			filterMode = FilterMode.Point
		};
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		MinimapThumbnailMaker.Instance = this;
	}

	private void OnDestroy()
	{
		if (MinimapThumbnailMaker.Instance == this)
		{
			MinimapThumbnailMaker.Instance = null;
		}
	}

	public Color Blend(int x, int y, int size, int w, int h, Color[] cs)
	{
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		int num5 = 0;
		for (int i = x - size / 2; i < x + size / 2; i++)
		{
			for (int j = y - size / 2; j < y + size / 2; j++)
			{
				if (i >= 0 && i < w && j >= 0 && j < h)
				{
					Color color = cs[i + j * w];
					num += color.a;
					num2 += color.r;
					num3 += color.g;
					num4 += color.b;
					num5++;
				}
			}
		}
		return new Color(num2 / num, num3 / num, num4 / num, num / (float)num5);
	}

	public void RenderMap(GameData.ClimateType cli, GameData.EnvironmentType env, Texture2D tex)
	{
		this.MapRoot.SetActive(true);
		this.City.SetActive(env == GameData.EnvironmentType.City);
		this.Town.SetActive(env == GameData.EnvironmentType.Town);
		this.Rural.SetActive(env == GameData.EnvironmentType.Rural);
		this.SmallDensTemperate.SetActive(env != GameData.EnvironmentType.Rural && cli == GameData.ClimateType.Temperate);
		this.SmallDensWarm.SetActive(env != GameData.EnvironmentType.Rural && cli == GameData.ClimateType.Warm);
		this.SmallDensCold.SetActive(env != GameData.EnvironmentType.Rural && cli == GameData.ClimateType.Cold);
		this.HighDensTemperate.SetActive(env == GameData.EnvironmentType.Rural && cli == GameData.ClimateType.Temperate);
		this.HighDensWarm.SetActive(env == GameData.EnvironmentType.Rural && cli == GameData.ClimateType.Warm);
		this.HighDensCold.SetActive(env == GameData.EnvironmentType.Rural && cli == GameData.ClimateType.Cold);
		this.Snow.SetActive(cli == GameData.ClimateType.Cold);
		this.Grass.SetActive(cli == GameData.ClimateType.Temperate);
		this.Desert.SetActive(cli == GameData.ClimateType.Warm);
		this.RenderObject(this.MapRoot.gameObject, MinimapThumbnailMaker.ThumbSize.Big, tex);
		this.MapRoot.SetActive(false);
	}

	public Texture2D RenderObject(GameObject obj, MinimapThumbnailMaker.ThumbSize size, Texture2D finalTex = null)
	{
		this.light.enabled = true;
		RenderTexture active = RenderTexture.active;
		Camera cam = this.Cam;
		RenderTexture renderTexture = (size != MinimapThumbnailMaker.ThumbSize.Big) ? this.SmallTex : this.BigTex;
		RenderTexture.active = renderTexture;
		cam.targetTexture = renderTexture;
		Dictionary<Renderer, KeyValuePair<int, bool>> dictionary = obj.GetComponentsInChildren<Renderer>().ToDictionary((Renderer x) => x, (Renderer x) => new KeyValuePair<int, bool>(x.gameObject.layer, x.enabled));
		foreach (KeyValuePair<Renderer, KeyValuePair<int, bool>> keyValuePair in dictionary)
		{
			keyValuePair.Key.gameObject.layer = 9;
			keyValuePair.Key.enabled = true;
		}
		this.Cam.Render();
		this.Cam.Render();
		foreach (KeyValuePair<Renderer, KeyValuePair<int, bool>> keyValuePair2 in dictionary)
		{
			keyValuePair2.Key.gameObject.layer = keyValuePair2.Value.Key;
			keyValuePair2.Key.enabled = keyValuePair2.Value.Value;
		}
		int num = (size != MinimapThumbnailMaker.ThumbSize.Big) ? 128 : 256;
		RenderTexture renderTexture2 = new RenderTexture(num, num, 16, RenderTextureFormat.ARGB32);
		this.BlitMat.SetFloat("_inputSize", (float)(num * 2));
		Graphics.Blit(this.Cam.targetTexture, renderTexture2, this.BlitMat);
		if (finalTex == null)
		{
			finalTex = new Texture2D(num, num, TextureFormat.ARGB32, false);
		}
		RenderTexture.active = renderTexture2;
		finalTex.ReadPixels(new Rect(0f, 0f, (float)num, (float)num), 0, 0);
		finalTex.Apply();
		RenderTexture.active = active;
		UnityEngine.Object.Destroy(renderTexture2);
		this.light.enabled = false;
		return finalTex;
	}

	public Camera Cam;

	public static MinimapThumbnailMaker Instance;

	public MiniMapMaker MinimapMaker;

	public Material BlitMat;

	public Light light;

	private RenderTexture BigTex;

	private RenderTexture SmallTex;

	public GameObject MapRoot;

	public GameObject City;

	public GameObject Town;

	public GameObject Rural;

	public GameObject SmallDensTemperate;

	public GameObject SmallDensWarm;

	public GameObject SmallDensCold;

	public GameObject HighDensTemperate;

	public GameObject HighDensWarm;

	public GameObject HighDensCold;

	public GameObject Snow;

	public GameObject Grass;

	public GameObject Desert;

	public enum ThumbSize
	{
		Big,
		Small
	}
}
