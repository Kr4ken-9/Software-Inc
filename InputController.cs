﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

public static class InputController
{
	static InputController()
	{
		KeyCode[] array = new KeyCode[44];
		RuntimeHelpers.InitializeArray(array, fieldof(<PrivateImplementationDetails>.$field-42BA45EB3CCBACE4D385916514844EDE5584C3B2).FieldHandle);
		InputController.KeyBindings = array;
		InputController.KeyModifiers = new InputController.Modifiers[]
		{
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.SHIFT,
			InputController.Modifiers.NONE,
			InputController.Modifiers.CTRL,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.NONE,
			InputController.Modifiers.CTRL,
			InputController.Modifiers.CTRL,
			InputController.Modifiers.CTRL,
			InputController.Modifiers.NONE,
			InputController.Modifiers.SHIFT,
			InputController.Modifiers.CTRL,
			InputController.Modifiers.NONE
		};
		KeyCode[] array2 = new KeyCode[44];
		RuntimeHelpers.InitializeArray(array2, fieldof(<PrivateImplementationDetails>.$field-05C7CD3FD6EABB9ED4846FE9B2A9F6A161352F27).FieldHandle);
		InputController.AltKeyBindings = array2;
		InputController.AltKeyModifiers = new InputController.Modifiers[44];
		InputController.InputEnabled = true;
		InputController.OrgKeys = new KeyCode[InputController.KeyBindings.Length];
		InputController.OrgAltKeys = new KeyCode[InputController.AltKeyBindings.Length];
		InputController.OrgMods = new InputController.Modifiers[InputController.KeyModifiers.Length];
		InputController.OrdAltMods = new InputController.Modifiers[InputController.AltKeyModifiers.Length];
		for (int i = 0; i < InputController.KeyBindings.Length; i++)
		{
			InputController.OrgKeys[i] = InputController.KeyBindings[i];
			InputController.OrgAltKeys[i] = InputController.AltKeyBindings[i];
			InputController.OrgMods[i] = InputController.KeyModifiers[i];
			InputController.OrdAltMods[i] = InputController.AltKeyModifiers[i];
		}
	}

	public static int KeyCount
	{
		get
		{
			return InputController.KeyBindings.Length;
		}
	}

	public static void Reset()
	{
		for (int i = 0; i < InputController.KeyBindings.Length; i++)
		{
			InputController.KeyBindings[i] = InputController.OrgKeys[i];
			InputController.AltKeyBindings[i] = InputController.OrgAltKeys[i];
			InputController.KeyModifiers[i] = InputController.OrgMods[i];
			InputController.AltKeyModifiers[i] = InputController.OrdAltMods[i];
		}
	}

	public static string GetPrettyKeyName(KeyCode code)
	{
		string text = string.Empty;
		if (InputController.PrettyKeyNames.TryGetValue(code, out text))
		{
			return text.LocTry();
		}
		int num = (int)code;
		if (num >= 48 && num <= 57)
		{
			return (num - 47).ToString();
		}
		text = code.ToString();
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < text.Length; i++)
		{
			char c = text[i];
			if (char.IsUpper(c) && i > 0)
			{
				stringBuilder.Append(" " + char.ToLower(c));
			}
			else if (i > 0 && char.IsNumber(c) && !char.IsNumber(text[i - 1]))
			{
				stringBuilder.Append(" " + c);
			}
			else
			{
				stringBuilder.Append(c);
			}
		}
		string text2 = stringBuilder.ToString();
		if (text2.Length > 1)
		{
			return ("KeyCode" + text2).LocDef(text2);
		}
		return text2;
	}

	public static void BindKey(InputController.Keys key, KeyCode keyCode, bool alter, bool GUI, bool ctrl, bool alt, bool shift, bool cmd)
	{
		InputController.BindKey(key, keyCode, alter, GUI, InputController.ModifierFromBool(ctrl, alt, shift, cmd));
	}

	public static void BindKey(InputController.Keys key, KeyCode keyCode, bool alter, bool GUI, InputController.Modifiers mod)
	{
		if (keyCode == KeyCode.Escape)
		{
			if (alter)
			{
				InputController.AltKeyBindings[(int)key] = keyCode;
				InputController.AltKeyModifiers[(int)key] = InputController.Modifiers.NONE;
			}
			else
			{
				InputController.KeyBindings[(int)key] = keyCode;
				InputController.KeyModifiers[(int)key] = InputController.Modifiers.NONE;
			}
			return;
		}
		if (keyCode == KeyCode.LeftControl || keyCode == KeyCode.RightControl || keyCode == KeyCode.LeftShift || keyCode == KeyCode.RightShift || keyCode == KeyCode.LeftAlt || keyCode == KeyCode.RightAlt || keyCode == KeyCode.LeftCommand || keyCode == KeyCode.RightCommand)
		{
			return;
		}
		if (InputController.CompareKeys(key, false, keyCode, mod))
		{
			return;
		}
		if (!InputController.AllowDoubleBind(key))
		{
			for (int i = 0; i < InputController.KeyBindings.Length; i++)
			{
				if (i != (int)key)
				{
					InputController.Keys key2 = (InputController.Keys)i;
					if (!InputController.AllowDoubleBind(key2))
					{
						if (InputController.CompareKeys(key2, false, keyCode, mod))
						{
							if (GUI)
							{
								InputController.DoRebind(i, false, (int)key, alter, keyCode, mod);
								return;
							}
							InputController.KeyBindings[i] = ((!alter) ? InputController.KeyBindings[(int)key] : InputController.AltKeyBindings[(int)key]);
							InputController.BindModifer(key, false, mod);
						}
						if (InputController.CompareKeys(key2, true, keyCode, mod))
						{
							if (GUI)
							{
								InputController.DoRebind(i, true, (int)key, alter, keyCode, mod);
								return;
							}
							InputController.AltKeyBindings[i] = ((!alter) ? InputController.KeyBindings[(int)key] : InputController.AltKeyBindings[(int)key]);
							InputController.BindModifer(key, true, mod);
						}
					}
				}
			}
		}
		if (alter)
		{
			InputController.AltKeyBindings[(int)key] = keyCode;
			InputController.BindModifer(key, true, mod);
		}
		else
		{
			InputController.KeyBindings[(int)key] = keyCode;
			InputController.BindModifer(key, false, mod);
		}
	}

	public static InputController.Modifiers ModifierFromBool(bool ctrl, bool alt, bool shift, bool cmd)
	{
		InputController.Modifiers modifiers = InputController.Modifiers.NONE;
		if (ctrl)
		{
			modifiers |= InputController.Modifiers.CTRL;
		}
		if (alt)
		{
			modifiers |= InputController.Modifiers.ALT;
		}
		if (shift)
		{
			modifiers |= InputController.Modifiers.SHIFT;
		}
		if (cmd)
		{
			modifiers |= InputController.Modifiers.CMD;
		}
		return modifiers;
	}

	private static void BindModifer(InputController.Keys key, bool alter, InputController.Modifiers bind)
	{
		if (alter)
		{
			InputController.AltKeyModifiers[(int)key] = bind;
		}
		else
		{
			InputController.KeyModifiers[(int)key] = bind;
		}
	}

	public static bool CompareKeys(InputController.Keys key1, InputController.Keys key2, bool alt1, bool alt2)
	{
		KeyCode keyCode = (!alt1) ? InputController.KeyBindings[(int)key1] : InputController.AltKeyBindings[(int)key1];
		KeyCode keyCode2 = (!alt2) ? InputController.KeyBindings[(int)key2] : InputController.AltKeyBindings[(int)key2];
		InputController.Modifiers modifiers = (!alt1) ? InputController.KeyModifiers[(int)key1] : InputController.AltKeyModifiers[(int)key1];
		InputController.Modifiers modifiers2 = (!alt2) ? InputController.KeyModifiers[(int)key2] : InputController.AltKeyModifiers[(int)key2];
		return keyCode == keyCode2 && modifiers == modifiers2;
	}

	public static bool CompareKeys(InputController.Keys key, bool alt, KeyCode k2, InputController.Modifiers m2)
	{
		KeyCode keyCode = (!alt) ? InputController.KeyBindings[(int)key] : InputController.AltKeyBindings[(int)key];
		InputController.Modifiers modifiers = (!alt) ? InputController.KeyModifiers[(int)key] : InputController.AltKeyModifiers[(int)key];
		return keyCode == k2 && modifiers == m2;
	}

	public static void DoRebind(int orgBind, bool orgAlt, int newBind, bool newAlt, KeyCode code, InputController.Modifiers mod)
	{
		DialogWindow diag = WindowManager.SpawnDialog();
		diag.Show(string.Format("KeyBindingWarning".Loc(), code.ToString(), InputController.GetLocKey(orgBind)), false, DialogWindow.DialogType.Warning, new KeyValuePair<string, Action>[]
		{
			new KeyValuePair<string, Action>("Yes", delegate
			{
				if (orgAlt)
				{
					InputController.AltKeyBindings[orgBind] = ((!newAlt) ? InputController.KeyBindings[newBind] : InputController.AltKeyBindings[newBind]);
					InputController.AltKeyModifiers[orgBind] = ((!newAlt) ? InputController.KeyModifiers[newBind] : InputController.AltKeyModifiers[newBind]);
				}
				else
				{
					InputController.KeyBindings[orgBind] = ((!newAlt) ? InputController.KeyBindings[newBind] : InputController.AltKeyBindings[newBind]);
					InputController.KeyModifiers[orgBind] = ((!newAlt) ? InputController.KeyModifiers[newBind] : InputController.AltKeyModifiers[newBind]);
				}
				if (newAlt)
				{
					InputController.AltKeyBindings[newBind] = code;
					InputController.AltKeyModifiers[newBind] = mod;
				}
				else
				{
					InputController.KeyBindings[newBind] = code;
					InputController.KeyModifiers[newBind] = mod;
				}
				diag.Window.Close();
				Options.SaveToFile();
				foreach (InputButton inputButton in OptionsWindow.Instance.InputButtons)
				{
					inputButton.UpdateText();
				}
			}),
			new KeyValuePair<string, Action>("No", delegate
			{
				diag.Window.Close();
			})
		});
	}

	public static string GetLocKey(int key)
	{
		return InputController.KeyNames[key].Loc();
	}

	public static InputController.Keys GetKeyNum(string name)
	{
		InputController.Keys result;
		try
		{
			InputController.Keys keys = (InputController.Keys)Enum.Parse(typeof(InputController.Keys), name);
			result = keys;
		}
		catch (Exception)
		{
			result = InputController.Keys.NewConsole;
		}
		return result;
	}

	public static string GetKeyBindString(InputController.Keys key, bool alt)
	{
		KeyCode binding = InputController.GetBinding(key, alt);
		return (binding != KeyCode.Escape) ? InputController.GetPrettyKeyName(binding) : "None".Loc();
	}

	public static string GetFullKeyBindString(InputController.Keys key, bool alt)
	{
		KeyCode binding = InputController.GetBinding(key, alt);
		if (binding == KeyCode.Escape)
		{
			return "None".Loc();
		}
		InputController.Modifiers modifiers = (!alt) ? InputController.KeyModifiers[(int)key] : InputController.AltKeyModifiers[(int)key];
		List<string> list = new List<string>();
		if ((modifiers & InputController.Modifiers.CTRL) > InputController.Modifiers.NONE)
		{
			list.Add("Ctrl");
		}
		if ((modifiers & InputController.Modifiers.SHIFT) > InputController.Modifiers.NONE)
		{
			list.Add("Shift");
		}
		if ((modifiers & InputController.Modifiers.ALT) > InputController.Modifiers.NONE)
		{
			list.Add("Alt");
		}
		if ((modifiers & InputController.Modifiers.CMD) > InputController.Modifiers.NONE)
		{
			list.Add("Cmd");
		}
		list.Add(InputController.GetPrettyKeyName(binding));
		return string.Join("+", list.ToArray());
	}

	public static bool[] GetMods(InputController.Keys key, bool alt)
	{
		InputController.Modifiers modifiers = (!alt) ? InputController.KeyModifiers[(int)key] : InputController.AltKeyModifiers[(int)key];
		return new bool[]
		{
			(modifiers & InputController.Modifiers.CTRL) != InputController.Modifiers.NONE,
			(modifiers & InputController.Modifiers.ALT) != InputController.Modifiers.NONE,
			(modifiers & InputController.Modifiers.SHIFT) != InputController.Modifiers.NONE,
			(modifiers & InputController.Modifiers.CMD) != InputController.Modifiers.NONE
		};
	}

	public static KeyCode GetBinding(InputController.Keys key, bool alt)
	{
		return (!alt) ? InputController.KeyBindings[(int)key] : InputController.AltKeyBindings[(int)key];
	}

	public static bool GetKeyDown(InputController.Keys key, bool force = false)
	{
		bool ctrl = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
		bool alt = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
		bool shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
		bool cmd = Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand);
		KeyCode binding = InputController.GetBinding(key, false);
		KeyCode binding2 = InputController.GetBinding(key, true);
		return (InputController.InputEnabled || force) && ((binding != KeyCode.Escape && Input.GetKeyDown(binding) && InputController.CheckModifier(key, false, ctrl, alt, shift, cmd)) || (binding2 != KeyCode.Escape && Input.GetKeyDown(binding2) && InputController.CheckModifier(key, true, ctrl, alt, shift, cmd)));
	}

	public static bool EventKey(InputController.Keys key, Event e, bool force = false)
	{
		bool ctrl = (e.modifiers & EventModifiers.Control) != EventModifiers.None;
		bool alt = (e.modifiers & EventModifiers.Alt) != EventModifiers.None;
		bool shift = (e.modifiers & EventModifiers.Shift) != EventModifiers.None;
		bool cmd = (e.modifiers & EventModifiers.Command) != EventModifiers.None;
		KeyCode binding = InputController.GetBinding(key, false);
		KeyCode binding2 = InputController.GetBinding(key, true);
		return (InputController.InputEnabled || force) && ((binding != KeyCode.Escape && binding == e.keyCode && InputController.CheckModifier(key, false, ctrl, alt, shift, cmd)) || (binding2 != KeyCode.Escape && binding2 == e.keyCode && InputController.CheckModifier(key, true, ctrl, alt, shift, cmd)));
	}

	public static bool IsBound(InputController.Keys key)
	{
		KeyCode binding = InputController.GetBinding(key, false);
		KeyCode binding2 = InputController.GetBinding(key, true);
		return binding != KeyCode.Escape || binding2 != KeyCode.Escape;
	}

	public static bool CheckModifier(InputController.Keys key, bool alter)
	{
		bool flag = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
		bool flag2 = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
		bool flag3 = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
		bool flag4 = Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand);
		InputController.Modifiers modifiers = (!alter) ? InputController.KeyModifiers[(int)key] : InputController.AltKeyModifiers[(int)key];
		return (modifiers & InputController.Modifiers.CTRL) != InputController.Modifiers.NONE == flag && (modifiers & InputController.Modifiers.ALT) != InputController.Modifiers.NONE == flag2 && (modifiers & InputController.Modifiers.SHIFT) != InputController.Modifiers.NONE == flag3 && (modifiers & InputController.Modifiers.CMD) != InputController.Modifiers.NONE == flag4;
	}

	private static bool CheckModifier(InputController.Keys key, bool alter, bool ctrl, bool alt, bool shift, bool cmd)
	{
		InputController.Modifiers modifiers = (!alter) ? InputController.KeyModifiers[(int)key] : InputController.AltKeyModifiers[(int)key];
		return (modifiers & InputController.Modifiers.CTRL) != InputController.Modifiers.NONE == ctrl && (modifiers & InputController.Modifiers.ALT) != InputController.Modifiers.NONE == alt && (modifiers & InputController.Modifiers.SHIFT) != InputController.Modifiers.NONE == shift && (modifiers & InputController.Modifiers.CMD) != InputController.Modifiers.NONE == cmd;
	}

	public static bool GetKey(InputController.Keys key, bool ignoreModifers = false)
	{
		bool ctrl = ignoreModifers || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
		bool alt = ignoreModifers || Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
		bool shift = ignoreModifers || Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
		bool cmd = ignoreModifers || Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand);
		KeyCode binding = InputController.GetBinding(key, false);
		KeyCode binding2 = InputController.GetBinding(key, true);
		return InputController.InputEnabled && ((binding != KeyCode.Escape && Input.GetKey(binding) && (ignoreModifers || InputController.CheckModifier(key, false, ctrl, alt, shift, cmd))) || (binding2 != KeyCode.Escape && Input.GetKey(binding2) && (ignoreModifers || InputController.CheckModifier(key, true, ctrl, alt, shift, cmd))));
	}

	public static bool GetKeyUp(InputController.Keys key, bool ignoreModifers = false)
	{
		bool ctrl = ignoreModifers || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
		bool alt = ignoreModifers || Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
		bool shift = ignoreModifers || Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
		bool cmd = ignoreModifers || Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand);
		KeyCode binding = InputController.GetBinding(key, false);
		KeyCode binding2 = InputController.GetBinding(key, true);
		return InputController.InputEnabled && ((binding != KeyCode.Escape && Input.GetKeyUp(binding) && (ignoreModifers || InputController.CheckModifier(key, false, ctrl, alt, shift, cmd))) || (binding2 != KeyCode.Escape && Input.GetKeyUp(binding2) && (ignoreModifers || InputController.CheckModifier(key, true, ctrl, alt, shift, cmd))));
	}

	public static List<string> ToConfig(bool alt)
	{
		List<string> list = new List<string>();
		for (int i = 0; i < InputController.KeyBindings.Length; i++)
		{
			List<string> list2 = list;
			string[] array = new string[5];
			int num = 0;
			InputController.Keys keys = (InputController.Keys)i;
			array[num] = keys.ToString();
			array[1] = "=";
			array[2] = ((!alt) ? InputController.KeyBindings[i].ToString() : InputController.AltKeyBindings[i].ToString());
			array[3] = ",";
			int num2 = 4;
			string text;
			if (alt)
			{
				int num3 = (int)InputController.AltKeyModifiers[i];
				text = num3.ToString();
			}
			else
			{
				int num4 = (int)InputController.KeyModifiers[i];
				text = num4.ToString();
			}
			array[num2] = text;
			list2.Add(string.Concat(array));
		}
		return list;
	}

	public static bool AllowDoubleBind(InputController.Keys key)
	{
		return key == InputController.Keys.DragCamera;
	}

	public static void LoadConfig(List<string> config, List<string> altConfig)
	{
		if (config != null)
		{
			foreach (string text in config)
			{
				try
				{
					string[] array = text.Split(new char[]
					{
						'='
					});
					if (array[1].Contains(","))
					{
						string[] array2 = array[1].Split(new char[]
						{
							','
						});
						object obj = Enum.Parse(typeof(InputController.Keys), array[0]);
						object obj2 = Enum.Parse(typeof(KeyCode), array2[0]);
						int mod = Convert.ToInt32(array2[1]);
						InputController.BindKey((InputController.Keys)obj, (KeyCode)obj2, false, false, (InputController.Modifiers)mod);
					}
					else
					{
						object obj3 = Enum.Parse(typeof(InputController.Keys), array[0]);
						object obj4 = Enum.Parse(typeof(KeyCode), array[1]);
						InputController.BindKey((InputController.Keys)obj3, (KeyCode)obj4, false, false, InputController.Modifiers.NONE);
					}
				}
				catch (Exception)
				{
					Debug.Log("Couldn't bind key for " + text);
				}
			}
		}
		if (altConfig != null)
		{
			foreach (string text2 in altConfig)
			{
				try
				{
					string[] array3 = text2.Split(new char[]
					{
						'='
					});
					if (array3[1].Contains(","))
					{
						string[] array4 = array3[1].Split(new char[]
						{
							','
						});
						object obj5 = Enum.Parse(typeof(InputController.Keys), array3[0]);
						object obj6 = Enum.Parse(typeof(KeyCode), array4[0]);
						int mod2 = Convert.ToInt32(array4[1]);
						InputController.BindKey((InputController.Keys)obj5, (KeyCode)obj6, true, false, (InputController.Modifiers)mod2);
					}
					else
					{
						object obj7 = Enum.Parse(typeof(InputController.Keys), array3[0]);
						object obj8 = Enum.Parse(typeof(KeyCode), array3[1]);
						InputController.BindKey((InputController.Keys)obj7, (KeyCode)obj8, true, false, InputController.Modifiers.NONE);
					}
				}
				catch (Exception)
				{
					Debug.Log("Couldn't bind alt key for " + text2);
				}
			}
		}
	}

	public static Dictionary<KeyCode, string> PrettyKeyNames = new Dictionary<KeyCode, string>
	{
		{
			KeyCode.Mouse0,
			"Left mouse"
		},
		{
			KeyCode.Mouse1,
			"Right mouse"
		},
		{
			KeyCode.Mouse2,
			"Middle mouse"
		}
	};

	public static readonly string[] KeyNames = new string[]
	{
		"Toggle ceiling fixture",
		"Toggle walls",
		"Move camera forward",
		"Move camera backward",
		"Move camera left",
		"Move camera right",
		"Rotate camera up",
		"Rotate camera down",
		"Rotate camera left",
		"Rotate camera right",
		"Zoom in",
		"Zoom out",
		"Move up one floor",
		"Move down one floor",
		"Pause game",
		"Normal game speed",
		"Faster game speed",
		"Fastest game speed",
		"Skip day",
		"Toggle camera mode",
		"Hide HUD",
		"Go to employee",
		"Destroy selected",
		"Toggle Build Mode",
		"Close all windows",
		"Turn furniture clockwise",
		"Turn furniture counter clockwise",
		"Context menu",
		"Rotate camera",
		"Selection",
		"Select multiple",
		"Topdown view",
		"Sweep selection",
		"Drag camera",
		"Open console",
		"Anchor grid",
		"Reset grid",
		"Save game",
		"Mirror room horizontally",
		"Mirror room vertically",
		"Toggle audio overlay",
		"Close active window",
		"Undo",
		"Align to nearest wall"
	};

	private static KeyCode[] OrgKeys;

	private static KeyCode[] OrgAltKeys;

	private static InputController.Modifiers[] OrgMods;

	private static InputController.Modifiers[] OrdAltMods;

	private static KeyCode[] KeyBindings;

	private static InputController.Modifiers[] KeyModifiers;

	private static KeyCode[] AltKeyBindings;

	private static InputController.Modifiers[] AltKeyModifiers;

	public static bool InputEnabled;

	public enum Keys
	{
		NormalSelection = 29,
		MultipleSelect,
		SweepSelect = 32,
		RotateCamera = 28,
		DragCamera = 33,
		ContextMenu = 27,
		MoveUp = 2,
		MoveDown,
		MoveLeft,
		MoveRight,
		TurnUp,
		TurnDown,
		TurnLeft,
		TurnRight,
		AlignNearest = 43,
		ZoomIn = 10,
		ZoomOut,
		FloorUp,
		FloorDown,
		TopDown = 31,
		Pause = 14,
		Speed1,
		Speed2,
		Speed3,
		SkipDay,
		SaveGame = 37,
		Undo = 42,
		Destroy = 22,
		ToggleBuildMode,
		AnchorGrid = 35,
		ResetGrid,
		FurnitureClock = 25,
		FurnitureAntiClock,
		MirrorRoomHor = 38,
		MirrorRoomVert,
		CloseActiveWindow = 41,
		CloseAllWindows = 24,
		ToggleLights = 0,
		ToggleWalls,
		ToggleAudioOverlay = 40,
		GotoEmp = 21,
		ToggleCamera = 19,
		HideHUD,
		NewConsole = 34
	}

	[Flags]
	public enum Modifiers
	{
		NONE = 0,
		CTRL = 1,
		ALT = 2,
		SHIFT = 4,
		CMD = 8
	}
}
