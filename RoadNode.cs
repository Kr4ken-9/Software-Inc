﻿using System;
using UnityEngine;

public class RoadNode : Selectable
{
	public void Init(bool left)
	{
		this.Left = left;
		if (!this.init)
		{
			this.init = true;
			this.self = new PathNode<Vector2>(new Vector2(base.transform.position.x, base.transform.position.z), this);
			this.self.Weight = this.Weight;
			for (int i = 0; i < this.Connected.Length; i++)
			{
				RoadNode roadNode = this.Connected[i];
				roadNode.Init(left);
				this.self.AddConnection(roadNode.self);
			}
			if (this.Parking)
			{
				float num = Mathf.Abs(Mathf.Cos(base.transform.rotation.eulerAngles.y * 0.0174532924f));
				Vector2 vector = new Vector2(2f * num + 0.9f * (1f - num), 0.9f * num + 2f * (1f - num));
				this.NavMesh = new Vector2[]
				{
					new Vector2(base.transform.position.x - vector.x, base.transform.position.z - vector.y),
					new Vector2(base.transform.position.x + vector.x, base.transform.position.z - vector.y),
					new Vector2(base.transform.position.x + vector.x, base.transform.position.z + vector.y),
					new Vector2(base.transform.position.x - vector.x, base.transform.position.z + vector.y)
				};
				RoadManager.Instance.RegisterParking(this);
			}
		}
	}

	public Vector2 GetPos()
	{
		RoadSegment component = base.transform.parent.GetComponent<RoadSegment>();
		if (component != null)
		{
			return new Vector2((float)component.x, (float)component.y);
		}
		return new Vector2(0f, 0f);
	}

	private void OnDestroy()
	{
		if (GameSettings.IsQuitting)
		{
			return;
		}
		if (HUD.Instance != null)
		{
			HUD.Instance.UnreachableParking.Remove(this);
		}
		if (this.Parking)
		{
			RoadManager.Instance.DeregisterParking(this);
		}
	}

	private void OnDrawGizmosSelected()
	{
		if (this.self != null)
		{
			Gizmos.color = ((!this.Left) ? Color.green : Color.blue);
			foreach (PathNode<Vector2> pathNode in this.self.GetConnections())
			{
				try
				{
					Gizmos.DrawLine(base.transform.position, ((RoadNode)pathNode.Tag).transform.position);
				}
				catch (Exception)
				{
					Gizmos.DrawSphere(base.transform.position, 1f);
				}
			}
		}
		else
		{
			Gizmos.color = Color.red;
			foreach (RoadNode roadNode in this.Connected)
			{
				try
				{
					Gizmos.DrawLine(base.transform.position, roadNode.transform.position);
				}
				catch (Exception)
				{
					Gizmos.DrawSphere(base.transform.position, 1f);
				}
			}
		}
		Gizmos.color = ((!this.Parking) ? Color.white : ((!this.Taken) ? Color.green : Color.red));
		Gizmos.DrawSphere(base.transform.position, (!this.Parking) ? 0.1f : 0.3f);
		Gizmos.color = Color.white;
	}

	public override int GetFloor()
	{
		return 0;
	}

	public override string[] GetActions()
	{
		return new string[]
		{
			"AssignParking",
			"SelectNearParking",
			"SelectParkedPeople"
		};
	}

	public override bool IsSelectionRestricted()
	{
		return !this.Parking || GameSettings.Instance.RentMode || !GameSettings.Instance.PlayerOwnedPoint(base.transform.position.FlattenVector3());
	}

	public override bool IsSelectableInView()
	{
		return true;
	}

	public override bool SingleMat()
	{
		return true;
	}

	public override string[] GetExtendedIconInfo()
	{
		return new string[]
		{
			"Road"
		};
	}

	public override string[] GetExtendedInfo()
	{
		return new string[]
		{
			this.Assign.ToString().Loc()
		};
	}

	public override string GetInfo()
	{
		return ((!this.Taken) ? "Free" : "Taken").Loc();
	}

	public static string[] ParkingAssignStrings = new string[]
	{
		"Anyone",
		"Employees",
		"Staff",
		"Guests",
		"Deliveries",
		"Cooks"
	};

	public bool Parking;

	public bool Taken;

	public bool Left;

	public float Weight = 1f;

	public int ID;

	public PathNode<Vector2> self;

	private bool init;

	public RoadNode[] Connected;

	[NonSerialized]
	public Vector2[] NavMesh;

	public RoadNode.ParkingAssign Assign;

	public enum ParkingAssign
	{
		Anyone,
		Employees,
		Staff,
		Guests,
		Deliveries,
		Cooks
	}
}
