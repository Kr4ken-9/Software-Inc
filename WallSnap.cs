﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WallSnap : Selectable
{
	public void Init(WallEdge edge1, WallEdge edge2, float pos, bool clone = false)
	{
		WallEdge[] previous = this.WallPosition.Keys.ToArray<WallEdge>();
		this.ClearConnections();
		this.FirstEdge = edge1;
		Vector2 a = edge2.Pos - edge1.Pos;
		float magnitude = a.magnitude;
		this.WallPosition[edge1] = pos * magnitude;
		this.WallPosition[edge2] = (1f - pos) * magnitude;
		edge1.AddSegment(edge2, this);
		edge2.AddSegment(edge1, this);
		Vector2 vector = edge1.Pos + a * pos;
		base.transform.position = new Vector3(vector.x, base.transform.position.y, vector.y);
		base.transform.rotation = Quaternion.LookRotation(new Vector3(-a.y, 0f, a.x));
		IEnumerable<Room> enumerable = (from x in (from x in edge1.Links
		where x.Value == edge2
		select x).Concat(from x in edge2.Links
		where x.Value == edge1
		select x)
		select x.Key).Distinct<Room>();
		foreach (Room room in enumerable)
		{
			room.DirtyOuterMesh = true;
			room.DirtyInnerMesh = true;
		}
		this.EdgeChanged(previous, clone);
	}

	public virtual bool EdgeChanged(WallEdge[] previous, bool clone)
	{
		return true;
	}

	public virtual bool ValidSnap(bool clone, HashSet<Room> destroy = null, bool keep = false)
	{
		return true;
	}

	public void DestroyMe()
	{
		this.BeenDestroyed = true;
		UnityEngine.Object.Destroy(base.gameObject);
	}

	protected void DeserializeSnap(WriteDictionary dictionary)
	{
		if (dictionary.Contains("WallSnapRoom"))
		{
			bool flag = dictionary.Get<bool>("Reversed", false);
			if (dictionary.Contains("WallSnapForcePos"))
			{
				uint roomID = (uint)dictionary["WallSnapRoom"];
				Vector2 vector = dictionary.Get<SVector3>("WallSnapForcePos", new SVector3(0f, 0f, 0f, 0f)).ToVector2();
				Room room = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == roomID);
				if (room != null && room.Edges != null)
				{
					for (int i = 0; i < room.Edges.Count; i++)
					{
						WallEdge wallEdge = room.Edges[i];
						WallEdge wallEdge2 = room.Edges[(i + 1) % room.Edges.Count];
						Vector2? vector2 = Utilities.ProjectToLine(vector, wallEdge.Pos, wallEdge2.Pos);
						if (vector2 != null && (vector2.Value - vector).magnitude < 0.1f)
						{
							if (flag)
							{
								WallEdge wallEdge3 = wallEdge;
								wallEdge = wallEdge2;
								wallEdge2 = wallEdge3;
							}
							base.transform.position = new Vector3(0f, (float)(room.Floor * 2), 0f);
							this.Init(wallEdge, wallEdge2, (vector - wallEdge.Pos).magnitude / (wallEdge.Pos - wallEdge2.Pos).magnitude, this.DeserializeClone);
							this.DeserializeClone = false;
						}
					}
				}
				else
				{
					UnityEngine.Object.Destroy(base.gameObject);
				}
			}
			else
			{
				uint roomID = (uint)dictionary["WallSnapRoom"];
				int num = (int)dictionary["WallSnapIndex"];
				float num2 = (float)dictionary["WallSnapPos"];
				Room room2 = base.GetDeserializedObject(roomID) as Room;
				if (room2 == null && roomID > 0u)
				{
					room2 = GameSettings.Instance.sRoomManager.Rooms.FirstOrDefault((Room x) => x.DID == roomID);
				}
				if (room2 == null)
				{
					UnityEngine.Object.Destroy(base.gameObject);
				}
				else if (num < 0 || num > room2.Edges.Count - 1)
				{
					UnityEngine.Object.Destroy(base.gameObject);
				}
				else
				{
					WallEdge wallEdge4 = room2.Edges[num];
					WallEdge wallEdge5 = wallEdge4.Links[room2];
					base.transform.position = new Vector3(0f, (float)(room2.Floor * 2), 0f);
					float num3 = num2 / (wallEdge4.Pos - wallEdge5.Pos).magnitude;
					if (flag)
					{
						WallEdge wallEdge6 = wallEdge4;
						wallEdge4 = wallEdge5;
						wallEdge5 = wallEdge6;
						num3 = 1f - num3;
					}
					this.Init(wallEdge4, wallEdge5, num3, this.DeserializeClone);
					this.DeserializeClone = false;
				}
			}
		}
		else
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void PrepareTempSerialization()
	{
		if (this.WallPosition.Count > 0)
		{
			this.TempSnapReversed = false;
			WallEdge e1 = this.FirstEdge;
			WallEdge e2 = this.WallPosition.Keys.First((WallEdge x) => x != e1);
			Room key = e1.Links.FirstOrDefault((KeyValuePair<Room, WallEdge> x) => x.Value == e2).Key;
			if (key == null)
			{
				WallEdge e = e2;
				e2 = e1;
				e1 = e;
				key = e1.Links.FirstOrDefault((KeyValuePair<Room, WallEdge> x) => x.Value == e2).Key;
				this.TempSnapReversed = true;
			}
			if (key == null)
			{
				Debug.LogException(new Exception("Failed saving a wall snapping object: " + base.name + " due to linking error"), this);
			}
			else
			{
				this.TempSnapRoom = key.DID;
			}
		}
	}

	protected void SerializeSnap(WriteDictionary dictionary)
	{
		if (this.TempSnapRoom > 0u)
		{
			dictionary["WallSnapRoom"] = this.TempSnapRoom;
			dictionary["WallSnapForcePos"] = base.transform.position.FlattenVector3();
			dictionary["Reversed"] = this.TempSnapReversed;
			this.TempSnapRoom = 0u;
		}
		else if (this.WallPosition.Count > 0)
		{
			WallEdge e1 = this.FirstEdge;
			WallEdge e2 = this.WallPosition.Keys.First((WallEdge x) => x != e1);
			Room key = e1.Links.FirstOrDefault((KeyValuePair<Room, WallEdge> x) => x.Value == e2).Key;
			bool flag = false;
			if (key == null)
			{
				WallEdge e = e2;
				e2 = e1;
				e1 = e;
				key = e1.Links.FirstOrDefault((KeyValuePair<Room, WallEdge> x) => x.Value == e2).Key;
				flag = true;
			}
			if (key == null)
			{
				Debug.LogException(new Exception("Failed saving a wall snapping object: " + base.name + " due to linking error"), this);
			}
			else
			{
				int num = key.Edges.IndexOf(e1);
				dictionary["WallSnapRoom"] = key.DID;
				dictionary["WallSnapIndex"] = num;
				dictionary["WallSnapPos"] = this.WallPosition[e1];
				dictionary["Reversed"] = flag;
			}
		}
	}

	public WallEdge GetSecondEdge()
	{
		if (this.WallPosition != null)
		{
			foreach (KeyValuePair<WallEdge, float> keyValuePair in this.WallPosition)
			{
				if (keyValuePair.Key != this.FirstEdge)
				{
					return keyValuePair.Key;
				}
			}
		}
		return null;
	}

	public bool GetAgainstExterior()
	{
		WallEdge secondEdge = this.GetSecondEdge();
		return secondEdge != null && (this.FirstEdge.IsAgainstOutdoors(secondEdge) || !secondEdge.Links.ContainsValue(this.FirstEdge) || !this.FirstEdge.Links.ContainsValue(secondEdge));
	}

	public Room GetPrimaryRoom()
	{
		WallEdge otherEdge = this.WallPosition.Keys.First((WallEdge x) => x != this.FirstEdge);
		return (from x in this.FirstEdge.Links
		where x.Value == otherEdge
		select x.Key).FirstOrDefault<Room>();
	}

	protected void ClearConnections()
	{
		foreach (KeyValuePair<WallEdge, float> keyValuePair in this.WallPosition)
		{
			foreach (KeyValuePair<WallEdge, HashSet<WallSnap>> keyValuePair2 in keyValuePair.Key.Children)
			{
				keyValuePair2.Value.Remove(this);
			}
		}
		this.WallPosition.Clear();
	}

	public float WallWidth;

	public float YOffset;

	[NonSerialized]
	public Dictionary<WallEdge, float> WallPosition = new Dictionary<WallEdge, float>();

	public WallEdge FirstEdge;

	[NonSerialized]
	public bool BeenDestroyed;

	[NonSerialized]
	public bool DeserializeClone;

	private uint TempSnapRoom;

	private bool TempSnapReversed;
}
