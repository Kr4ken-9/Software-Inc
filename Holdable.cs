﻿using System;
using UnityEngine;

public class Holdable : MonoBehaviour
{
	private void Start()
	{
		this.Spawned = SDateTime.Now();
	}

	public void DecoupleFromParent()
	{
		if (this.Parent != null)
		{
			this.Parent.DecoupleHoldable(this);
		}
	}

	public void DestroyMe()
	{
		if (this != null && base.gameObject != null)
		{
			this.DecoupleFromParent();
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void OnDestroy()
	{
		if (GameSettings.IsQuitting)
		{
			return;
		}
		this.DecoupleFromParent();
	}

	public Vector3 OffsetTranslation;

	public Vector3 OffsetRotation;

	public TableScript Parent;

	public bool HoldStraight = true;

	public Renderer[] Renderers;

	public SDateTime Spawned;

	public bool DestroyOnDespawn;
}
