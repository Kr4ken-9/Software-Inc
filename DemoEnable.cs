﻿using System;
using UnityEngine;

public class DemoEnable : MonoBehaviour
{
	private void Start()
	{
		base.GetComponent<Renderer>().enabled = false;
		base.enabled = false;
	}

	private void Update()
	{
	}
}
