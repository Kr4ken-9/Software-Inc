﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MarketingWindow : MonoBehaviour
{
	public void UpdateTeamLabel()
	{
		this.TeamLabel.text = ((this.CompanyWorker == null) ? Utilities.GetTeamDescription(this.Teams) : this.CompanyWorker.Name);
	}

	public void EnableTeams(bool enable)
	{
		foreach (GameObject gameObject in this.TeamThings)
		{
			gameObject.SetActive(enable);
		}
		this.ContentPanel.offsetMin = new Vector2(this.ContentPanel.offsetMin.x, (float)((!enable) ? 55 : 88));
	}

	public void Show(SoftwareWorkItem sw)
	{
		DatePicker datePicker = this.datePicker;
		SDateTime? releaseDate = sw.ReleaseDate;
		datePicker.CurrentDate = ((releaseDate == null) ? (SDateTime.Now() + sw.DevTime) : releaseDate.Value);
		this.TargetWork = sw;
		this.TargetProduct = null;
		bool[] array = new bool[4];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = true;
		}
		array[0] = true;
		array[2] = (sw is SoftwareAlpha && !GameSettings.Instance.PressBuildQueue.Contains((SoftwareAlpha)sw));
		foreach (MarketingPlan marketingPlan in GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>())
		{
			if (marketingPlan.TargetItem == sw)
			{
				array[1] &= (marketingPlan.Type != MarketingPlan.TaskType.PressRelease);
				array[3] &= (marketingPlan.Type != MarketingPlan.TaskType.Hype);
			}
		}
		this.Toggles[4].gameObject.SetActive(false);
		bool flag = false;
		int num = 0;
		for (int j = array.Length - 1; j > -1; j--)
		{
			if (array[j] && (j != 0 || sw.ReleaseDate == null))
			{
				num = j;
			}
			flag |= array[j];
			this.Toggles[j].gameObject.SetActive(array[j]);
		}
		if (flag)
		{
			this.Window.NonLocTitle = string.Format("MarketingWindowTitle".Loc(), sw.SoftwareName);
			for (int k = 0; k < array.Length; k++)
			{
				if (num != k)
				{
					this.Toggles[k].isOn = false;
				}
			}
			this.Toggles[num].isOn = true;
			this.Window.Show();
			this.Toggle(true);
		}
		else
		{
			WindowManager.Instance.ShowMessageBox("NoMarketingOptions".Loc(), false, DialogWindow.DialogType.Error);
		}
	}

	public void Show(SoftwareProduct product)
	{
		if (GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>().Any((MarketingPlan x) => x.Type == MarketingPlan.TaskType.PostMarket && x.TargetProd == product))
		{
			WindowManager.Instance.ShowMessageBox("NoMarketingOptions".Loc(), false, DialogWindow.DialogType.Error);
			return;
		}
		this.TargetWork = null;
		this.TargetProduct = product;
		this.Window.NonLocTitle = string.Format("MarketingWindowTitle".Loc(), product.Name);
		for (int i = 0; i < 4; i++)
		{
			this.Toggles[i].isOn = false;
			this.Toggles[i].gameObject.SetActive(false);
		}
		this.Toggles[4].gameObject.SetActive(true);
		this.Toggles[4].isOn = true;
		this.Window.Show();
		this.Toggle(true);
		TutorialSystem.Instance.StartTutorial("Marketing", false);
	}

	public void Reset()
	{
		this.PressPanel.SetActive(false);
		this.ReleasePanel.SetActive(false);
		this.MarketPanel.SetActive(false);
		this.MarketBudget.text = "0";
		this.UpdatePressReleaseCost();
		for (int i = 0; i < this.PressOptions.Length; i++)
		{
			this.PressOptions[i].isOn = false;
		}
	}

	public void Toggle(bool val)
	{
		if (val)
		{
			int num = -1;
			for (int i = 0; i < this.Toggles.Length; i++)
			{
				if (this.Toggles[i].isOn && this.Toggles[i].gameObject.activeSelf)
				{
					num = i;
					break;
				}
			}
			if (num == -1)
			{
				return;
			}
			this.SelectedOption = (MarketingWindow.MarketingOption)num;
			this.Reset();
			this.DescriptionText.text = MarketingWindow.Descs[num].Loc();
			switch (this.SelectedOption)
			{
			case MarketingWindow.MarketingOption.AnnounceRelease:
				this.ReleasePanel.SetActive(true);
				this.EnableTeams(false);
				break;
			case MarketingWindow.MarketingOption.PressRelease:
				this.Teams.Clear();
				this.Teams.AddRange(GameSettings.Instance.GetDefaultTeams("PressRelease"));
				this.CompanyWorker = null;
				this.UpdateTeamLabel();
				this.EnableTeams(true);
				this.PressPanel.SetActive(true);
				break;
			case MarketingWindow.MarketingOption.PressBuild:
				this.EnableTeams(false);
				break;
			case MarketingWindow.MarketingOption.Hype:
				this.Teams.Clear();
				this.Teams.AddRange(GameSettings.Instance.GetDefaultTeams("Hype"));
				this.CompanyWorker = null;
				this.UpdateTeamLabel();
				this.EnableTeams(true);
				break;
			case MarketingWindow.MarketingOption.Market:
				this.Teams.Clear();
				this.Teams.AddRange(GameSettings.Instance.GetDefaultTeams("Marketing"));
				this.CompanyWorker = null;
				this.UpdateTeamLabel();
				this.EnableTeams(true);
				this.MarketPanel.SetActive(true);
				break;
			}
		}
	}

	public float GetPressReleaseCost()
	{
		float num = 0f;
		for (int i = 0; i < this.PressOptions.Length; i++)
		{
			if (this.PressOptions[i].isOn)
			{
				num += this.PressOptionCost[i];
			}
		}
		return num;
	}

	public void PickTeams()
	{
		HUD.Instance.TeamSelectWindow.Show(this.Teams, this.CompanyWorker, delegate(string[] ts, SimulatedCompany c)
		{
			this.Teams.Clear();
			if (c != null)
			{
				this.CompanyWorker = c;
			}
			else
			{
				this.CompanyWorker = null;
				this.Teams.AddRange(ts);
			}
			this.UpdateTeamLabel();
		}, null);
	}

	public float GetPressReleaseEffect()
	{
		float num = 1f;
		float num2 = this.PressOptionEffect.Sum() + 1f;
		for (int i = 0; i < this.PressOptions.Length; i++)
		{
			if (this.PressOptions[i].isOn)
			{
				num += this.PressOptionEffect[i];
			}
		}
		return num / num2;
	}

	public void UpdatePressReleaseCost()
	{
		this.PressCost.text = this.GetPressReleaseCost().Currency(true);
	}

	private void ChangeReleaseDate(SDateTime before, SDateTime now)
	{
		if (now > before)
		{
			float months = Utilities.GetMonths(before, now);
			float months2 = Utilities.GetMonths(SDateTime.Now(), now);
			if (months2 < 24f)
			{
				float num = Mathf.Clamp(Mathf.Pow(Mathf.Max(0f, 0.9f - (months2 - months / 8f) / 24f), 12f / months), 0f, 0.8f);
				this.TargetWork.Followers *= 1f - num;
				this.TargetWork.FollowerChange -= this.TargetWork.Followers * num;
			}
		}
	}

	public void Begin()
	{
		bool flag = true;
		MarketingWindow.MarketingOption selectedOption = this.SelectedOption;
		switch (selectedOption)
		{
		case MarketingWindow.MarketingOption.AnnounceRelease:
		{
			int month = this.datePicker.CurrentDate.Month;
			int year = this.datePicker.CurrentDate.Year;
			SDateTime rDate = new SDateTime(0, month, year);
			SDateTime now = SDateTime.Now();
			if (rDate.Year < now.Year || (rDate.Year == now.Year && rDate.Month <= now.Month))
			{
				WindowManager.Instance.ShowMessageBox("ReleaseDateError".Loc(), false, DialogWindow.DialogType.Error);
				flag = false;
			}
			else if (this.TargetWork.ReleaseDate != null)
			{
				if (!this.TargetWork.ReleaseDate.Value.EqualsVerySimple(rDate))
				{
					WindowManager.Instance.ShowMessageBox("ReleaseDateChangeWarning".Loc(new object[]
					{
						Utilities.DateDiff(now, rDate)
					}), true, DialogWindow.DialogType.Question, delegate
					{
						this.ChangeReleaseDate(this.TargetWork.ReleaseDate.Value, rDate);
						this.TargetWork.ReleaseDate = new SDateTime?(rDate);
					}, null, null);
				}
			}
			else
			{
				WindowManager.Instance.ShowMessageBox("ReleaseDateConfirmation".Loc(new object[]
				{
					rDate.ToCompactString(),
					Utilities.DateDiff(now, rDate)
				}), true, DialogWindow.DialogType.Question, delegate
				{
					this.TargetWork.ReleaseDate = new SDateTime?(rDate);
				}, "Set release date", null);
			}
			break;
		}
		case MarketingWindow.MarketingOption.PressRelease:
			if (!this.TargetWork.Done)
			{
				this.PressRelease();
			}
			break;
		case MarketingWindow.MarketingOption.PressBuild:
			if (!this.TargetWork.Done)
			{
				if (this.TargetWork.ReleaseDate != null)
				{
					this.PressBuild();
				}
				else
				{
					WindowManager.Instance.ShowMessageBox("MarketingReleaseDateWarning".Loc(new object[]
					{
						"Press build".Loc().ToLower()
					}), true, DialogWindow.DialogType.Question, delegate
					{
						this.PressBuild();
					}, "Marketing without release date", null);
				}
			}
			break;
		case MarketingWindow.MarketingOption.Hype:
			if (!this.TargetWork.Done)
			{
				if (this.CompanyWorker == null)
				{
					GameSettings.Instance.TeamDefaults["Hype"] = this.Teams.ToHashSet<string>();
				}
				MarketingPlan item = new MarketingPlan(this.TargetWork, MarketingPlan.TaskType.Hype, 0f, 0f, (!(this.TargetWork.guiItem == null)) ? (this.TargetWork.guiItem.transform.GetSiblingIndex() + 1) : -1);
				this.AssignTeams(item);
				GameSettings.Instance.MyCompany.WorkItems.Add(item);
			}
			break;
		case MarketingWindow.MarketingOption.Market:
		{
			if (this.CompanyWorker == null)
			{
				GameSettings.Instance.TeamDefaults["Marketing"] = this.Teams.ToHashSet<string>();
			}
			float num = 0f;
			try
			{
				num = (float)Convert.ToDouble(this.MarketBudget.text);
			}
			catch (Exception)
			{
			}
			num = num.FromCurrency();
			MarketingPlan item2 = new MarketingPlan(num, this.TargetProduct.ID, this.TargetProduct.Name);
			this.AssignTeams(item2);
			GameSettings.Instance.MyCompany.WorkItems.Add(item2);
			break;
		}
		}
		if (flag)
		{
			this.Window.Close();
		}
	}

	private void PressRelease()
	{
		if (this.CompanyWorker == null)
		{
			GameSettings.Instance.TeamDefaults["PressRelease"] = this.Teams.ToHashSet<string>();
		}
		MarketingPlan item = new MarketingPlan(this.TargetWork, MarketingPlan.TaskType.PressRelease, this.GetPressReleaseCost(), this.GetPressReleaseEffect(), (!(this.TargetWork.guiItem == null)) ? (this.TargetWork.guiItem.transform.GetSiblingIndex() + 1) : -1);
		this.AssignTeams(item);
		GameSettings.Instance.MyCompany.WorkItems.Add(item);
	}

	private void AssignTeams(WorkItem item)
	{
		if (this.CompanyWorker != null)
		{
			item.CompanyWorker = this.CompanyWorker;
		}
		else
		{
			foreach (string key in this.Teams)
			{
				Team team;
				if (GameSettings.Instance.sActorManager.Teams.TryGetValue(key, out team))
				{
					item.AddDevTeam(team, false);
				}
			}
		}
	}

	private void PressBuild()
	{
		GameSettings.Instance.PressBuildQueue.Add(this.TargetWork as SoftwareAlpha);
		HUD.Instance.AddPopupMessage("PressBuildConfirmation".Loc(new object[]
		{
			this.TargetWork.SoftwareName
		}), "Info", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Neutral, 0f, PopupManager.PopupIDs.None, 6);
	}

	public GUIWindow Window;

	public Toggle[] Toggles;

	public GameObject ReleasePanel;

	public GameObject PressPanel;

	public GameObject MarketPanel;

	public Text DescriptionText;

	public Text PressCost;

	public Text TeamLabel;

	[NonSerialized]
	public HashSet<string> Teams = new HashSet<string>();

	[NonSerialized]
	public SimulatedCompany CompanyWorker;

	public InputField MarketBudget;

	public Toggle[] PressOptions;

	[NonSerialized]
	public SoftwareProduct TargetProduct;

	[NonSerialized]
	public SoftwareWorkItem TargetWork;

	public GameObject[] TeamThings;

	public RectTransform ContentPanel;

	public DatePicker datePicker;

	public float[] PressOptionCost;

	public float[] PressOptionEffect;

	private static string[] Descs = new string[]
	{
		"MarketingDescAnnounce",
		"MarketingDescRelease",
		"MarketingDescBuild",
		"MarketingDescHype",
		"MarketingDescPost"
	};

	public MarketingWindow.MarketingOption SelectedOption;

	public static float ChoicesSum = 3.75f;

	public static float CostPerUnit = 100f;

	public enum MarketingOption
	{
		AnnounceRelease,
		PressRelease,
		PressBuild,
		Hype,
		Market
	}
}
