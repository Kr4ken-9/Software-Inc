﻿using System;
using System.Linq;
using UnityEngine;

public class CompanyWindow : MonoBehaviour
{
	public CompanyDetailWindow GetCompanyDetailWindow(Company company)
	{
		return WindowManager.FindWindowType<CompanyDetailWindow>().FirstOrDefault((CompanyDetailWindow x) => x.company == company);
	}

	public void ToggleCompanyDetails(Company company)
	{
		CompanyDetailWindow companyDetailWindow = WindowManager.FindWindowType<CompanyDetailWindow>().FirstOrDefault((CompanyDetailWindow x) => x.company == company);
		if (companyDetailWindow != null)
		{
			companyDetailWindow.window.Close();
			return;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.CompanyDetailWindow);
		gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		CompanyDetailWindow component = gameObject.GetComponent<CompanyDetailWindow>();
		component.company = company;
	}

	public void ShowCompanyDetails(Company company)
	{
		CompanyDetailWindow companyDetailWindow = WindowManager.FindWindowType<CompanyDetailWindow>().FirstOrDefault((CompanyDetailWindow x) => x.company == company);
		if (companyDetailWindow != null)
		{
			WindowManager.Focus(companyDetailWindow.window);
			return;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.CompanyDetailWindow);
		gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		CompanyDetailWindow component = gameObject.GetComponent<CompanyDetailWindow>();
		component.company = company;
	}

	public void FocusCompany(Company company)
	{
		this.Bind();
		int num = this.CompanyList.ActualItems.IndexOf(company);
		if (num >= 0)
		{
			this.CompanyList.Select(num);
			this.CompanyList.KeepIdxInView(num);
			this.Window.Show();
		}
		else
		{
			WindowManager.Instance.ShowMessageBox("CompanyMissingWarning".Loc(), false, DialogWindow.DialogType.Error);
		}
	}

	private void Start()
	{
		this.Bind();
	}

	public void Bind()
	{
		this.CompanyList.Items = GameSettings.Instance.simulation.GetAllCompanies().Cast<object>().ToList<object>();
		GameSettings.Instance.simulation.BindCompanyUpdate(this.CompanyList.Items);
	}

	public GUIWindow Window;

	public GUIListView CompanyList;

	public GameObject CompanyDetailWindow;
}
