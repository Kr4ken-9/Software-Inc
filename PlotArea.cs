﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class PlotArea
{
	public PlotArea()
	{
	}

	public PlotArea(Color color, params PlotArea.PlotPoint[] points)
	{
		this.Init(points);
		this.PlotColor = color;
	}

	public PlotArea(params PlotArea.PlotPoint[] points)
	{
		this.Init(points);
		this.PlotColor = PlotArea.GetRandomColor();
	}

	public PlotArea(IList<PlotArea.PlotPoint> points)
	{
		this.Init(points);
		this.PlotColor = PlotArea.GetRandomColor();
	}

	public Vector2[] Polygon
	{
		get
		{
			if (this._polygon == null)
			{
				this.InitPolygon();
			}
			return this._polygon;
		}
	}

	public Vector2[] PolygonEx
	{
		get
		{
			if (this._polygon == null)
			{
				this.InitPolygon();
			}
			return this._polygonEx;
		}
	}

	public bool PlayerOwned
	{
		get
		{
			return this._playerOwned;
		}
		set
		{
			this._playerOwned = value;
			if (this.PlotObject != null)
			{
				this.PlotObject.UpdatePlayerOwned();
			}
		}
	}

	public float Price
	{
		get
		{
			return this.PricePerSqm * this.Area + this.AddonCost;
		}
		set
		{
			this.PricePerSqm = value / this.Area;
		}
	}

	private void InitPolygon()
	{
		this._polygon = new Vector2[this.Links.Count];
		this._polygonEx = new Vector2[this.Links.Count];
		for (int i = 0; i < this.Links.Count; i++)
		{
			PlotArea.PlotPoint point = this.Links[i].Point;
			this._polygon[i] = new Vector2(point.X, point.Y);
		}
		for (int j = 0; j < this._polygon.Length; j++)
		{
			Vector2 first = this._polygon[(j != 0) ? (j - 1) : (this._polygon.Length - 1)];
			Vector2 second = this._polygon[j];
			Vector2 third = this._polygon[(j + 1) % this._polygon.Length];
			this._polygonEx[j] = Utilities.GetOffset(first, second, third, -PlotArea.InsideFuzzy, true);
		}
		this.Center = Utilities.GetPolygonCentroid(this._polygon).ToVector3(0f);
		this.Area = Utilities.PolygonArea(this.Polygon);
	}

	private void Init(IList<PlotArea.PlotPoint> points)
	{
		this.Links = new List<PlotArea.PointLink>(points.Count);
		this.MinX = float.MaxValue;
		this.MinY = float.MaxValue;
		this.MaxX = float.MinValue;
		this.MaxY = float.MinValue;
		for (int i = 0; i < points.Count; i++)
		{
			this.Links.Add(new PlotArea.PointLink(points[i]));
			this.MinX = Mathf.Min(this.MinX, points[i].X);
			this.MinY = Mathf.Min(this.MinY, points[i].Y);
			this.MaxX = Mathf.Max(this.MaxX, points[i].X);
			this.MaxY = Mathf.Max(this.MaxY, points[i].Y);
		}
		this.MinX -= PlotArea.InsideFuzzy;
		this.MinY -= PlotArea.InsideFuzzy;
		this.MaxX += PlotArea.InsideFuzzy;
		this.MaxY += PlotArea.InsideFuzzy;
		for (int j = 0; j < this.Links.Count; j++)
		{
			this.Links[j].Previous = this.Links[(j != 0) ? (j - 1) : (this.Links.Count - 1)];
			this.Links[j].Next = this.Links[(j + 1) % this.Links.Count];
		}
		this.Points = this.Links.ToDictionary((PlotArea.PointLink x) => x.Point, (PlotArea.PointLink x) => x);
		this.InitPolygon();
		this.PricePerSqm = PlotArea.AreaPrice + (0.5f - Utilities.RandomValue) * PlotArea.PriceRange;
	}

	public bool IsInside(Vector2 p)
	{
		return p.x >= this.MinX && p.x <= this.MaxX && p.y >= this.MinY && p.y <= this.MaxY && Utilities.IsInside(p, this._polygonEx);
	}

	public static List<PlotArea> PlotsFromRects(List<Rect> areas)
	{
		List<PlotArea> list = new List<PlotArea>();
		for (int i = 0; i < areas.Count; i++)
		{
			Rect rect = areas[i];
			list.Add(new PlotArea(new PlotArea.PlotPoint[]
			{
				new PlotArea.PlotPoint(rect.xMax, rect.yMin),
				new PlotArea.PlotPoint(rect.xMax, rect.yMax),
				new PlotArea.PlotPoint(rect.xMin, rect.yMax),
				new PlotArea.PlotPoint(rect.xMin, rect.yMin)
			}));
		}
		return list;
	}

	public static List<PlotArea> PlotsFromRects(List<KeyValuePair<Rect, float>> areas)
	{
		List<PlotArea> list = new List<PlotArea>();
		for (int i = 0; i < areas.Count; i++)
		{
			Rect key = areas[i].Key;
			list.Add(new PlotArea(new PlotArea.PlotPoint[]
			{
				new PlotArea.PlotPoint(key.xMax, key.yMin),
				new PlotArea.PlotPoint(key.xMax, key.yMax),
				new PlotArea.PlotPoint(key.xMin, key.yMax),
				new PlotArea.PlotPoint(key.xMin, key.yMin)
			})
			{
				AddonCost = areas[i].Value
			});
		}
		return list;
	}

	public static List<PlotArea> DividePlots(List<PlotArea> plots, float maxLen)
	{
		List<PlotArea> list = new List<PlotArea>();
		for (int i = 0; i < plots.Count; i++)
		{
			PlotArea.DividePlotSub(plots[i], list, maxLen * 2f);
		}
		return list;
	}

	private static void DividePlotSub(PlotArea plot, List<PlotArea> output, float maxLen)
	{
		if (plot.Points.Count == 4)
		{
			Vector2 a = plot.Polygon[1] - plot.Polygon[0];
			float magnitude = a.magnitude;
			Vector2 a2 = plot.Polygon[3] - plot.Polygon[0];
			float magnitude2 = a2.magnitude;
			if (magnitude > magnitude2)
			{
				if (magnitude > maxLen)
				{
					Vector2 a3 = plot.Polygon[2] - plot.Polygon[3];
					float magnitude3 = a3.magnitude;
					if ((magnitude3 / magnitude).IsBetween(0.75f, 1.25f))
					{
						PlotArea.PlotPoint plotPoint = new PlotArea.PlotPoint(plot.Polygon[0] + a * GameData.RNDRange(0.4f, 0.6f));
						PlotArea.PlotPoint plotPoint2 = new PlotArea.PlotPoint(plot.Polygon[3] + a3 * GameData.RNDRange(0.4f, 0.6f));
						PlotArea.DividePlotSub(new PlotArea(new PlotArea.PlotPoint[]
						{
							plot.Links[0].Point,
							plotPoint,
							plotPoint2,
							plot.Links[3].Point
						}), output, maxLen);
						PlotArea.DividePlotSub(new PlotArea(new PlotArea.PlotPoint[]
						{
							plotPoint,
							plot.Links[1].Point,
							plot.Links[2].Point,
							plotPoint2
						}), output, maxLen);
						return;
					}
				}
			}
			else if (magnitude2 > maxLen)
			{
				Vector2 a4 = plot.Polygon[2] - plot.Polygon[1];
				float magnitude4 = a4.magnitude;
				if ((magnitude4 / magnitude2).IsBetween(0.75f, 1.25f))
				{
					PlotArea.PlotPoint plotPoint3 = new PlotArea.PlotPoint(plot.Polygon[0] + a2 * GameData.RNDRange(0.4f, 0.6f));
					PlotArea.PlotPoint plotPoint4 = new PlotArea.PlotPoint(plot.Polygon[1] + a4 * GameData.RNDRange(0.4f, 0.6f));
					PlotArea.DividePlotSub(new PlotArea(new PlotArea.PlotPoint[]
					{
						plot.Links[0].Point,
						plot.Links[1].Point,
						plotPoint4,
						plotPoint3
					}), output, maxLen);
					PlotArea.DividePlotSub(new PlotArea(new PlotArea.PlotPoint[]
					{
						plotPoint3,
						plotPoint4,
						plot.Links[2].Point,
						plot.Links[3].Point
					}), output, maxLen);
					return;
				}
			}
		}
		output.Add(plot);
	}

	public GameObject CreateObject(GameObject prefab)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefab);
		PlotObject component = gameObject.GetComponent<PlotObject>();
		component.EdgeMesh.sharedMesh = this.GenerateBorderMesh(1f);
		component.PlotMesh.sharedMesh = this.GenerateInnerMesh();
		component.Renderer.material.color = this.PlotColor.ToColor().Alpha(0.5f);
		this.PlotObject = component;
		this.PlotObject.Plot = this;
		this.PlotObject.UpdatePlayerOwned();
		return gameObject;
	}

	private static PlotArea Merge(PlotArea p1, PlotArea p2)
	{
		if (p1 == p2)
		{
			return null;
		}
		int num = -1;
		int num2 = -1;
		int i = 0;
		while (i < p1.Links.Count)
		{
			PlotArea.PointLink pointLink = p1.Links[i];
			PlotArea.PointLink pointLink2;
			if (p2.Points.TryGetValue(pointLink.Point, out pointLink2) && pointLink.Point == pointLink2.Point && pointLink.Previous.Point != pointLink2.Next.Point)
			{
				if (num != -1)
				{
					return null;
				}
				num = i;
				num2 = p2.Links.IndexOf(pointLink2);
				break;
			}
			else
			{
				i++;
			}
		}
		if (num == -1)
		{
			return null;
		}
		int num3 = -1;
		for (int j = 0; j < p1.Links.Count; j++)
		{
			PlotArea.PointLink pointLink3 = p1.Links[(num + j) % p1.Links.Count];
			int num4 = num2 - j;
			PlotArea.PointLink pointLink4 = p2.Links[(num4 >= 0) ? num4 : (p2.Links.Count + num4)];
			if (pointLink3.Next.Point != pointLink4.Previous.Point)
			{
				num3 = j;
				break;
			}
		}
		if (num3 < 1)
		{
			return null;
		}
		List<PlotArea.PlotPoint> list = new List<PlotArea.PlotPoint>();
		int num5;
		for (num5 = (num + num3) % p1.Links.Count; num5 != num; num5 = (num5 + 1) % p1.Links.Count)
		{
			list.Add(p1.Links[num5].Point);
		}
		num5 = num2;
		int num6 = num2 - num3;
		if (num6 < 0)
		{
			num6 = p2.Links.Count + num6;
		}
		while (num5 != num6)
		{
			list.Add(p2.Links[num5].Point);
			num5 = (num5 + 1) % p2.Links.Count;
		}
		if (list.Distinct<PlotArea.PlotPoint>().Count<PlotArea.PlotPoint>() == list.Count)
		{
			return new PlotArea(p1.PlotColor, list.ToArray())
			{
				MergeCount = p1.MergeCount + p2.MergeCount + 1
			};
		}
		return null;
	}

	public Mesh GenerateBorderMesh(float width)
	{
		Vector2[] array = new Vector2[this.Polygon.Length];
		for (int i = 0; i < this.Polygon.Length; i++)
		{
			int num = (i != 0) ? (i - 1) : (this.Polygon.Length - 1);
			int num2 = (i + 1) % this.Polygon.Length;
			array[i] = Utilities.GetOffset(this.Polygon[num], this.Polygon[i], this.Polygon[num2], width, true);
		}
		int[] array2 = new int[this.Polygon.Length * 6];
		for (int j = 0; j < this.Polygon.Length; j++)
		{
			int num3 = (j + 1) % this.Polygon.Length;
			array2[j * 6] = j + this.Polygon.Length;
			array2[j * 6 + 1] = num3;
			array2[j * 6 + 2] = j;
			array2[j * 6 + 3] = j + this.Polygon.Length;
			array2[j * 6 + 4] = num3 + this.Polygon.Length;
			array2[j * 6 + 5] = num3;
		}
		Mesh mesh = new Mesh();
		Vector3[] array3 = (from x in this.Polygon
		select x.ToVector3(0f)).Concat(from x in array
		select x.ToVector3(0f)).ToArray<Vector3>();
		mesh.vertices = array3;
		mesh.normals = (from x in array3
		select Vector3.up).ToArray<Vector3>();
		mesh.triangles = array2;
		return mesh;
	}

	public Mesh GenerateInnerMesh()
	{
		Triangulator triangulator = new Triangulator(this.Polygon);
		int[] triangles = triangulator.Triangulate();
		Mesh mesh = new Mesh();
		mesh.vertices = (from x in this.Polygon
		select x.ToVector3(0f)).ToArray<Vector3>();
		mesh.normals = (from x in this.Polygon
		select Vector3.up).ToArray<Vector3>();
		mesh.uv = this.Polygon;
		mesh.triangles = triangles;
		return mesh;
	}

	public static List<PlotArea> GenerateRandom(bool bigPlots)
	{
		List<PlotArea.PlotPoint> list = new List<PlotArea.PlotPoint>();
		List<PlotArea> list2 = new List<PlotArea>();
		int size = 16;
		float num = 8f;
		float dist = 16f;
		float num2 = (!bigPlots) ? (dist * 0.6f) : (dist * 0.4f);
		float num3 = (!bigPlots) ? 0.4f : 0f;
		float num4 = (float)size * dist;
		num4 = Mathf.Sqrt(num4 * num4 + num4 * num4);
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				bool flag = j > 0 && j < size - 1;
				bool flag2 = i > 0 && i < size - 1;
				if (j <= 1 && i >= size / 2 - 1 && i < size / 2 + 1)
				{
					flag2 = false;
					flag = false;
				}
				float num5 = num + (float)j * dist;
				float num6 = num + (float)i * dist;
				float num7 = 0f;
				float num8 = (float)size / 2f * dist;
				num7 -= num5;
				num8 -= num6;
				float num9 = Mathf.Sqrt(num7 * num7 + num8 * num8);
				float num10 = (1f - num9 / num4) * dist * num3;
				num7 /= num9;
				num8 /= num9;
				num7 *= num10;
				num8 *= num10;
				list.Add(new PlotArea.PlotPoint(num5 + ((!flag) ? 0f : ((GameData.RNDValue - 0.5f) * num2 + num7)), num6 + ((!flag2) ? 0f : ((GameData.RNDValue - 0.5f) * num2 + num8))));
			}
		}
		for (int k = 0; k < size - 1; k++)
		{
			for (int l = 0; l < size - 1; l++)
			{
				list2.Add(new PlotArea(new PlotArea.PlotPoint[]
				{
					list[l + k * size],
					list[l + k * size + 1],
					list[l + (k + 1) * size + 1],
					list[l + (k + 1) * size]
				}));
			}
		}
		int index = (size / 2 - 1) * (size - 2) + size / 2 - 1;
		PlotArea plotArea = new PlotArea(new PlotArea.PlotPoint[]
		{
			new PlotArea.PlotPoint(9f, 120f),
			new PlotArea.PlotPoint(24f, 120f),
			new PlotArea.PlotPoint(24f, 136f),
			new PlotArea.PlotPoint(9f, 136f)
		});
		list2[index] = plotArea;
		if (bigPlots)
		{
			list2.Remove(plotArea);
			List<PlotArea> list3 = new List<PlotArea>(list2);
			list2.Clear();
			while (list3.Count > 0)
			{
				PlotArea me = list3[0];
				list3.RemoveAt(0);
				List<PlotArea> list4 = (from x in list3
				where x.Points.Any((KeyValuePair<PlotArea.PlotPoint, PlotArea.PointLink> y) => me.Points.ContainsKey(y.Key))
				select x).ToList<PlotArea>();
				for (int m = 0; m < list4.Count; m++)
				{
					PlotArea plotArea2 = PlotArea.Merge(me, list4[m]);
					if (plotArea2 != null)
					{
						me = plotArea2;
						list3.Remove(list4[m]);
					}
					else
					{
						list2.Add(list4[m]);
					}
				}
				me.MergeCount = 0;
				list2.Add(me);
			}
		}
		list2 = (from x in list2
		orderby PlotArea.DistanceSqrt(x.Links[0].Point, 0f, (float)size / 2f * dist) descending
		select x).ToList<PlotArea>();
		int num11 = (!bigPlots) ? 200 : 50;
		int num12 = (!bigPlots) ? int.MaxValue : 3;
		List<PlotArea> list5 = new List<PlotArea>();
		for (int n = 0; n < num11; n++)
		{
			float num13 = GameData.RNDValue;
			num13 = num13 * num13 * num13 * num13;
			int index2 = Mathf.Min((int)Mathf.Round(num13 * (float)list2.Count), list2.Count - 1);
			PlotArea p1 = list2[index2];
			if (p1 == plotArea)
			{
				n--;
			}
			else
			{
				PlotArea.PointLink p = p1.Links[GameData.RNDRange(0, p1.Links.Count)];
				PlotArea plotArea3 = list2.FirstOrDefault((PlotArea x) => x.Points.ContainsKey(p.Point) && x != p1);
				if (plotArea3 != null && plotArea3 != plotArea)
				{
					PlotArea plotArea4 = PlotArea.Merge(p1, plotArea3);
					if (plotArea4 != null)
					{
						list2.Remove(p1);
						list2.Remove(plotArea3);
						if (plotArea4.MergeCount >= num12)
						{
							list5.Add(plotArea4);
						}
						else
						{
							list2.Insert(list2.Count - list2.Count / 3, plotArea4);
						}
					}
				}
			}
		}
		list2.AddRange(list5);
		list2.Remove(plotArea);
		list2.Insert(0, plotArea);
		return list2;
	}

	private static Color GetRandomColor()
	{
		Vector3 vector = Utilities.HSVToRGB((float)UnityEngine.Random.Range(0, 360), 0.75f, 1f);
		return new Color(vector.x, vector.y, vector.z);
	}

	private static float DistanceSqrt(PlotArea.PlotPoint p1, float x, float y)
	{
		float num = x - p1.X;
		float num2 = y - p1.Y;
		return num * num + num2 * num2;
	}

	public static float AreaPrice = 350f;

	public static float PriceRange = 100f;

	public static float InsideFuzzy = 0.01f;

	public static float StartPlotPrice = 50000f;

	public List<PlotArea.PointLink> Links;

	[NonSerialized]
	private Vector2[] _polygon;

	[NonSerialized]
	private Vector2[] _polygonEx;

	public float Area;

	public float PricePerSqm;

	public float AddonCost;

	[NonSerialized]
	public int MergeCount;

	public float Monthly;

	public int MonthsLeft;

	public float MinX;

	public float MaxX;

	public float MinY;

	public float MaxY;

	public Dictionary<PlotArea.PlotPoint, PlotArea.PointLink> Points;

	public SVector3 PlotColor;

	public SVector3 Center;

	private bool _playerOwned;

	[NonSerialized]
	public PlotObject PlotObject;

	[Serializable]
	public class PlotPoint
	{
		public PlotPoint()
		{
		}

		public PlotPoint(float x, float y)
		{
			this.X = x;
			this.Y = y;
		}

		public PlotPoint(Vector2 p)
		{
			this.X = p.x;
			this.Y = p.y;
		}

		public override string ToString()
		{
			return this.X + ", " + this.Y;
		}

		public float X;

		public float Y;
	}

	[Serializable]
	public class PointLink
	{
		public PointLink()
		{
		}

		public PointLink(PlotArea.PlotPoint point)
		{
			this.Point = point;
		}

		public override string ToString()
		{
			return this.Point.ToString();
		}

		public PlotArea.PlotPoint Point;

		public PlotArea.PointLink Previous;

		public PlotArea.PointLink Next;
	}
}
