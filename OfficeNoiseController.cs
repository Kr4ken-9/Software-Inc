﻿using System;
using UnityEngine;

public class OfficeNoiseController : MonoBehaviour
{
	private void Start()
	{
		this._sources = base.GetComponentsInChildren<AudioSource>();
	}

	private void Update()
	{
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		Room cameraRoom = GameSettings.Instance.sRoomManager.CameraRoom;
		float num5 = 30f;
		float num6 = 5f;
		float num7 = 12f;
		float num8 = 3f;
		float num9 = 2f;
		float num10 = 15f;
		Vector3 point;
		if (CameraScript.Instance.FlyMode)
		{
			int num11 = Mathf.FloorToInt(CameraScript.Instance.mainCam.transform.position.y / 2f);
			point = new Vector3(CameraScript.Instance.mainCam.transform.position.x, (float)(num11 * 2), CameraScript.Instance.mainCam.transform.position.z);
		}
		else
		{
			Ray ray = HUD.Instance.MainCamera.ScreenPointToRay(new Vector3((float)(Screen.width / 2), (float)(Screen.height / 2), 0f));
			Plane plane = new Plane(Vector3.up, Vector3.up * (float)GameSettings.Instance.ActiveFloor * 2f);
			float distance = 0f;
			plane.Raycast(ray, out distance);
			point = ray.GetPoint(distance);
		}
		float num12 = 1f;
		if (GameSettings.GameSpeed > 0f && cameraRoom != null && !cameraRoom.Dummy)
		{
			num12 = 1f - (Mathf.Clamp(CameraScript.Instance.mainCam.transform.position.y - (float)(cameraRoom.Floor * 2), num6, num5) - num6) / (num5 - num6);
			float num13 = 0f;
			for (int i = 0; i < cameraRoom.Occupants.Count; i++)
			{
				Actor actor = cameraRoom.Occupants[i];
				if (actor.AItype == AI<Actor>.AIType.Employee && !actor.AIScript.currentNode.Name.Equals("Loiter"))
				{
					num13 += 1f - (Mathf.Clamp((actor.transform.position - point).magnitude, num8, num7) - num8) / (num7 - num8);
				}
			}
			if (num13 >= num10)
			{
				num13 = 1f;
			}
			else if (num13 <= num9)
			{
				num13 = 0f;
			}
			else
			{
				num13 = num13.MapRange(num9, num10, 0f, 1f, false);
			}
			num = this.Curves[0].Evaluate(num13);
			num2 = this.Curves[1].Evaluate(num13);
			num3 = this.Curves[2].Evaluate(num13);
			num4 = this.Curves[3].Evaluate(num13);
		}
		int num14 = 10;
		this._sources[0].volume = Mathf.Lerp(this._sources[0].volume, num * num12, Time.deltaTime * (float)num14);
		this._sources[1].volume = Mathf.Lerp(this._sources[1].volume, num2 * num12, Time.deltaTime * (float)num14);
		this._sources[2].volume = Mathf.Lerp(this._sources[2].volume, num3 * num12, Time.deltaTime * (float)num14);
		this._sources[3].volume = Mathf.Lerp(this._sources[3].volume, num4 * num12, Time.deltaTime * (float)num14);
	}

	private AudioSource[] _sources;

	public AnimationCurve[] Curves;
}
