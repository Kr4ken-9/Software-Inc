﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class ProductPrintOrder
{
	public ProductPrintOrder()
	{
		this.TotalCopies = 0u;
		this.ProductIDs = new uint[0];
		this.Copies = new uint[0];
	}

	public ProductPrintOrder(IList<uint> id, IList<uint> copies)
	{
		int num = Mathf.Min(id.Count, copies.Count);
		this.ProductIDs = new uint[num];
		this.Copies = new uint[num];
		for (int i = 0; i < num; i++)
		{
			this.ProductIDs[i] = id[i];
			this.Copies[i] = copies[i];
			this.TotalCopies += copies[i];
		}
	}

	public ProductPrintOrder(Dictionary<uint, uint> orders)
	{
		this.ProductIDs = new uint[orders.Count];
		this.Copies = new uint[orders.Count];
		int num = 0;
		foreach (KeyValuePair<uint, uint> keyValuePair in orders)
		{
			this.ProductIDs[num] = keyValuePair.Key;
			this.Copies[num] = keyValuePair.Value;
			this.TotalCopies += keyValuePair.Value;
			num++;
		}
	}

	public static IStockable StockableFromID(uint id)
	{
		return GameSettings.Instance.simulation.GetStockable(id);
	}

	public static string NameFromID(uint id)
	{
		IStockable stockable = GameSettings.Instance.simulation.GetStockable(id);
		if (stockable != null)
		{
			return stockable.GetName();
		}
		return null;
	}

	public void Apply()
	{
		this.RemoveFromStorage();
		for (int i = 0; i < this.ProductIDs.Length; i++)
		{
			uint id = this.ProductIDs[i];
			uint num = this.Copies[i];
			SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(id, false);
			if (product != null)
			{
				product.PhysicalCopies += num;
			}
			else
			{
				SoftwareAlpha softwareAlpha = GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareAlpha>().FirstOrDefault((SoftwareAlpha x) => x.SWID != null && x.SWID.Value == id);
				if (softwareAlpha != null)
				{
					softwareAlpha.PhysicalCopies += num;
				}
				else
				{
					PrintJob orNull = GameSettings.Instance.PrintOrders.GetOrNull(id);
					foreach (SimulatedCompany simulatedCompany in GameSettings.Instance.simulation.Companies.Values)
					{
						SimulatedCompany.ProductPrototype productPrototype = simulatedCompany.Releases.FirstOrDefault((SimulatedCompany.ProductPrototype x) => x.SWID != null && x.SWID == id);
						if (productPrototype != null)
						{
							productPrototype.PhysicalCopies += num;
							Deal deal;
							if (orNull != null && orNull.DealID != null && HUD.Instance.dealWindow.AllDeals.TryGetValue(orNull.DealID.Value, out deal))
							{
								PrintDeal printDeal = deal as PrintDeal;
								if (printDeal != null)
								{
									printDeal.CurrentAmount += num;
									Company company = deal.Company;
									Company client = deal.Client;
									if (company != null && client != null)
									{
										float num2 = deal.Worth() * num;
										IStockable stockable = orNull.GetStockable();
										if (stockable != null)
										{
											stockable.AddLoss(num2);
										}
										string bill = printDeal.Title();
										company.MakeTransaction(num2, Company.TransactionCategory.Deals, bill);
										client.MakeTransaction(-num2, Company.TransactionCategory.Distribution, "Copy order");
									}
									if (printDeal.CurrentAmount > printDeal.Goal)
									{
										printDeal.FinalizeDeal();
									}
								}
							}
							break;
						}
					}
				}
			}
		}
	}

	public void RemoveFromStorage()
	{
		for (int i = 0; i < this.ProductIDs.Length; i++)
		{
			uint id = this.ProductIDs[i];
			uint amount = this.Copies[i];
			GameSettings.Instance.SetPrintsToStorage(id, amount, false);
		}
	}

	public void AddToStorage()
	{
		for (int i = 0; i < this.ProductIDs.Length; i++)
		{
			uint id = this.ProductIDs[i];
			uint amount = this.Copies[i];
			GameSettings.Instance.SetPrintsToStorage(id, amount, true);
		}
	}

	public static ProductPrintOrder Merge(params ProductPrintOrder[] orders)
	{
		Dictionary<uint, uint> dictionary = new Dictionary<uint, uint>();
		foreach (ProductPrintOrder productPrintOrder in orders)
		{
			if (productPrintOrder != null)
			{
				for (int j = 0; j < productPrintOrder.ProductIDs.Length; j++)
				{
					uint key = productPrintOrder.ProductIDs[j];
					uint num = productPrintOrder.Copies[j];
					if (!dictionary.ContainsKey(key))
					{
						dictionary[key] = num;
					}
					else
					{
						dictionary[key] += num;
					}
				}
			}
		}
		return new ProductPrintOrder(dictionary);
	}

	public uint TotalCopies;

	public uint[] ProductIDs;

	public uint[] Copies;
}
