﻿using System;
using System.Linq;

public class PlayerDistribution : IServerItem
{
	public bool CancelOnUnload()
	{
		return false;
	}

	public float GetLoadRequirement()
	{
		return (this.ItemSales * 0.1f).BandwidthFactor(SDateTime.Now());
	}

	public void HandleLoad(float load)
	{
		this.TimeToCancel += 1f - load;
		if (this.TimeToCancel >= 1f)
		{
			this.TimeToCancel -= 1f;
			Company company = GameSettings.Instance.simulation.GetAllCompanies().FirstOrDefault((Company x) => x.DistributionDeal != null);
			if (company != null)
			{
				company.DistributionDeal = null;
				HUD.Instance.AddPopupMessage("DistributionFailCancel".Loc(new object[]
				{
					company.Name
				}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.None, 1);
				HUD.Instance.distributionWindow.UpdateDistributionDeals();
			}
		}
		if (load < 1f)
		{
			HUD.Instance.AddPopupMessage("ServerLoadWarning".Loc(), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.ServerProblems, 1);
		}
		this.LastLoad = load;
	}

	public string GetDescription()
	{
		return "Digital distribution".Loc();
	}

	public void SerializeServer(string name)
	{
		this.ServerName = name;
	}

	public float ItemSales;

	public string ServerName;

	public float LastLoad = 1f;

	public float TimeToCancel;
}
