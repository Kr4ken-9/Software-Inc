﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CinematicEffects;

public static class Options
{
	static Options()
	{
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		string[] commandLineArgs = Environment.GetCommandLineArgs();
		for (int i = 0; i < commandLineArgs.Length; i++)
		{
			string text = commandLineArgs[i].ToLower();
			if (text.Equals("-disableresolution"))
			{
				Options.DisableResolutionChange = true;
			}
			else if (text.Equals("-resizepanel") && i + 1 < commandLineArgs.Length)
			{
				string[] array = commandLineArgs[i + 1].Split(new char[]
				{
					','
				});
				if (array.Length == 2)
				{
					try
					{
						double num = Convert.ToDouble(array[0]);
						double num2 = Convert.ToDouble(array[1]);
						Options.MainPanelOffset = new Vector2?(new Vector2((float)num, (float)num2));
					}
					catch (Exception)
					{
					}
				}
				i++;
			}
		}
		Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
		ConfigFile configFile = new ConfigFile();
		try
		{
			string settingsFile = Options.SettingsFile;
			configFile = ((!File.Exists(settingsFile)) ? configFile : ConfigFile.Load(Utilities.ReadOnlyReadAllText(settingsFile)));
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
			Options.ShowFileLoadError();
		}
		Options._currency = configFile.GetOrDefault("Currency", Options._currency.ToString());
		Options._language = configFile.GetOrDefault("Language", Options._language.ToString());
		Options._tutorial = configFile.GetOrDefault("Tutorial", Options._tutorial.ToString()).ConvertToBoolDef(Options._tutorial);
		Options.AMPM = configFile.GetOrDefault("AMPM", Options.AMPM.ToString()).ConvertToBoolDef(Options.AMPM);
		Options._consoleOnError = configFile.GetOrDefault("ConsoleOnError", Options._consoleOnError.ToString()).ConvertToBoolDef(Options._consoleOnError);
		Options._celsius = configFile.GetOrDefault("Celsius", Options._celsius.ToString()).ConvertToBoolDef(Options._celsius);
		Options._autoSkip = configFile.GetOrDefault("AutoSkip", Options._autoSkip.ToString()).ConvertToBoolDef(Options._autoSkip);
		Options._autoSave = configFile.GetOrDefault("AutoSave", Options._autoSave.ToString()).ConvertToBoolDef(Options._autoSave);
		Options._backup = configFile.GetOrDefault("Backup", Options._backup.ToString()).ConvertToBoolDef(Options._backup);
		Options._moreShadow = configFile.GetOrDefault("MoreShadow", Options._moreShadow.ToString()).ConvertToBoolDef(Options._moreShadow);
		Options._opaqueGlass = configFile.GetOrDefault("OpaqueGlass", Options._opaqueGlass.ToString()).ConvertToBoolDef(Options._opaqueGlass);
		Options._edgeScroll = configFile.GetOrDefault("EdgeScroll", Options._edgeScroll.ToString()).ConvertToBoolDef(Options._edgeScroll);
		Options._askMarketing = configFile.GetOrDefault("AskMarketing", Options._askMarketing.ToString()).ConvertToBoolDef(Options._askMarketing);
		Options._askPrinting = configFile.GetOrDefault("AskPrinting", Options._askPrinting.ToString()).ConvertToBoolDef(Options._askPrinting);
		Options._askReporting = configFile.GetOrDefault("AskReporting", Options._askReporting.ToString()).ConvertToBoolDef(Options._askReporting);
		Options._hints = configFile.GetOrDefault("Hints", Options._hints.ToString()).ConvertToBoolDef(Options._hints);
		Options._currencyShortForm = configFile.GetOrDefault("CurrencyShortForm", Options._currencyShortForm.ToString()).ConvertToBoolDef(Options._currencyShortForm);
		Options._colorBrightnessSlider = configFile.GetOrDefault("ColorBrightnessSlider", Options._colorBrightnessSlider.ToString()).ConvertToBoolDef(Options._colorBrightnessSlider);
		Options._ambientOcclusion = configFile.GetOrDefault("AmbientOcclusion", Options._ambientOcclusion.ToString()).ConvertToBoolDef(Options._ambientOcclusion);
		Options._ssaa = Mathf.Clamp(configFile.GetOrDefault("SSAA", Options._ssaa.ToString()).ConvertToIntDef(Options._ssaa), 10, 20);
		Options._fxaa = configFile.GetOrDefault("FXAA", Options._fxaa.ToString()).ConvertToBoolDef(Options._fxaa);
		Options._smaa = configFile.GetOrDefault("SMAA", Options._smaa.ToString()).ConvertToBoolDef(Options._smaa);
		Options._ssr = configFile.GetOrDefault("SSR", Options._ssr.ToString()).ConvertToBoolDef(Options._ssr);
		Options._tiltShift = configFile.GetOrDefault("TiltShift", Options._tiltShift.ToString()).ConvertToBoolDef(Options._tiltShift);
		Options._frosted = configFile.GetOrDefault("Frosted", Options._frosted.ToString()).ConvertToBoolDef(Options._frosted);
		Options._bloom = configFile.GetOrDefault("Bloom", Options._bloom.ToString()).ConvertToBoolDef(Options._bloom);
		Options._gamma = configFile.GetOrDefault("Gamma", Options._gamma.ToString()).ConvertToFloatDef(Options._gamma);
		Options._scrollSpeed = configFile.GetOrDefault("ScrollSpeed", Options._scrollSpeed.ToString()).ConvertToFloatDef(Options._scrollSpeed);
		Options._zoomSpeed = configFile.GetOrDefault("ZoomSpeed", Options._zoomSpeed.ToString()).ConvertToFloatDef(Options._zoomSpeed);
		Options._rotationSpeed = configFile.GetOrDefault("RotationSpeed", Options._rotationSpeed.ToString()).ConvertToFloatDef(Options._rotationSpeed);
		Options._uiSize = configFile.GetOrDefault("UISize", Options._uiSize.ToString()).ConvertToFloatDef(Options._uiSize);
		List<string> list = configFile["IgnoreQuestions"];
		if (list != null)
		{
			Options.IgnoreQuestions.AddRange(list);
		}
		Options._lastFullscreen = configFile.GetOrDefault("Fullscreen", "False").ConvertToBoolDef(false);
		Options.Shadows = configFile.GetOrDefault("Shadows", "True").ConvertToBoolDef(true);
		Options.ShadowQuality = configFile.GetOrDefault("ShadowQuality", "3").ConvertToIntDef(3);
		Options.TargetFrameRate = configFile.GetOrDefault("TargetFrameRate", "60").ConvertToIntDef(60);
		List<string> list2 = configFile["Resolution"];
		if (list2 != null && list2.Count > 2)
		{
			Options.TResolution = Options.FindRes(list2[0].ConvertToIntDef(1024), list2[1].ConvertToIntDef(768), list2[2].ConvertToIntDef(60));
		}
		else
		{
			Options.TResolution = Options.FindRes(1024, 768, 60);
		}
		Options.RunInBackground = configFile.GetOrDefault("RunInBackground", "False").ConvertToBoolDef(false);
		Options.VSync = configFile.GetOrDefault("VSync", "1").ConvertToIntDef(1);
		InputController.LoadConfig(configFile["Keys"], configFile["AltKeys"]);
		AudioManager.LoadConfig(configFile["AudioLevels"]);
		Options.Loaded = true;
		Options._lastFullscreen = Options.Fullscreen;
		Options._lastResolution = Options.TResolution;
		Options.LoadSizes();
		Options.LoadWidths();
		Options.SaveToFile();
		Options.InitHints();
		Debug.Log("Player preferences load time: " + (Time.realtimeSinceStartup - realtimeSinceStartup));
		LoadDebugger.AddInfo("Finshed loading player preferences");
	}

	public static string SettingsFile
	{
		get
		{
			return Path.Combine(Utilities.GetRoot(), "settings.txt");
		}
	}

	private static string WindowSizeFile
	{
		get
		{
			return Path.Combine(Utilities.GetRoot(), "WindowSizes.txt");
		}
	}

	private static string ColumnWidthFile
	{
		get
		{
			return Path.Combine(Utilities.GetRoot(), "ColumnWidths.txt");
		}
	}

	private static string HintFile
	{
		get
		{
			return Path.Combine(Utilities.GetRoot(), "Hints.bin");
		}
	}

	public static string Currency
	{
		get
		{
			return Options._currency;
		}
		set
		{
			if (!Options._currency.Equals(value))
			{
				Options._currency = value;
				Options.SaveToFile();
			}
		}
	}

	public static string Language
	{
		get
		{
			return Options._language;
		}
		set
		{
			if (!Localization.CurrentTranslation.Equals(value))
			{
				Localization.CurrentTranslation = value;
				Options._language = Localization.CurrentTranslation;
				Options.SaveToFile();
			}
		}
	}

	public static float UISize
	{
		get
		{
			return Options._uiSize;
		}
		set
		{
			if (!Options._uiSize.Equals(value))
			{
				Options._uiSize = Mathf.Clamp(Mathf.Round(value * 10f) / 10f, 1f, 2f);
				if (WindowManager.Instance != null)
				{
					WindowManager.Instance.Canvas.GetComponent<CanvasScaler>().scaleFactor = Options._uiSize;
				}
				Options.SaveToFile();
			}
		}
	}

	public static bool AMPM
	{
		get
		{
			return SDateTime.AMPM;
		}
		set
		{
			if (SDateTime.AMPM != value)
			{
				SDateTime.AMPM = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool Celsius
	{
		get
		{
			return Options._celsius;
		}
		set
		{
			if (Options._celsius != value)
			{
				Options._celsius = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool Tutorial
	{
		get
		{
			return Options._tutorial;
		}
		set
		{
			if (Options._tutorial != value)
			{
				Options._tutorial = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool AutoSkip
	{
		get
		{
			return Options._autoSkip;
		}
		set
		{
			if (Options._autoSkip != value)
			{
				Options._autoSkip = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool AutoSave
	{
		get
		{
			return Options._autoSave;
		}
		set
		{
			if (Options._autoSave != value)
			{
				Options._autoSave = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool Backup
	{
		get
		{
			return Options._backup;
		}
		set
		{
			if (Options._backup != value)
			{
				Options._backup = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool MoreShadow
	{
		get
		{
			return Options._moreShadow;
		}
		set
		{
			if (Options._moreShadow != value)
			{
				Options._moreShadow = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool OpaqueGlass
	{
		get
		{
			return Options._opaqueGlass;
		}
		set
		{
			if (Options._opaqueGlass != value)
			{
				Options._opaqueGlass = value;
				GameSettings.GlassOpaqueChange();
				Options.SaveToFile();
			}
		}
	}

	public static bool Frosted
	{
		get
		{
			return Options._frosted;
		}
		set
		{
			if (Options._frosted != value)
			{
				Options._frosted = value;
				CensorCamera.Refresh();
				Options.SaveToFile();
			}
		}
	}

	public static bool AmbientOcclusion
	{
		get
		{
			return Options._ambientOcclusion;
		}
		set
		{
			if (Options._ambientOcclusion != value)
			{
				Options._ambientOcclusion = value;
				CameraScript.ApplyOptions();
				Options.SaveToFile();
			}
		}
	}

	public static int SSAA
	{
		get
		{
			return Options._ssaa;
		}
		set
		{
			if (Options._ssaa != value)
			{
				Options._ssaa = Mathf.Clamp(value, 10, 20);
				CameraScript.ApplyOptions();
				Options.SaveToFile();
			}
		}
	}

	public static bool ConsoleOnError
	{
		get
		{
			return InputController.IsBound(InputController.Keys.NewConsole) && Options._consoleOnError;
		}
		set
		{
			if (Options._consoleOnError != value)
			{
				Options._consoleOnError = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool SSR
	{
		get
		{
			return Options._ssr;
		}
		set
		{
			if (Options._ssr != value)
			{
				Options._ssr = value;
				CameraScript.ApplyOptions();
				Options.SaveToFile();
			}
		}
	}

	public static bool TiltShift
	{
		get
		{
			return Options._tiltShift;
		}
		set
		{
			if (Options._tiltShift != value)
			{
				Options._tiltShift = value;
				CameraScript.ApplyOptions();
				Options.SaveToFile();
			}
		}
	}

	public static bool Bloom
	{
		get
		{
			return Options._bloom;
		}
		set
		{
			if (Options._bloom != value)
			{
				Options._bloom = value;
				CameraScript.ApplyOptions();
				Options.SaveToFile();
			}
		}
	}

	public static bool AskMarketing
	{
		get
		{
			return Options._askMarketing;
		}
		set
		{
			if (Options._askMarketing != value)
			{
				Options._askMarketing = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool AskPrinting
	{
		get
		{
			return Options._askPrinting;
		}
		set
		{
			if (Options._askPrinting != value)
			{
				Options._askPrinting = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool AskReporting
	{
		get
		{
			return Options._askReporting;
		}
		set
		{
			if (Options._askReporting != value)
			{
				Options._askReporting = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool HintsEnabled
	{
		get
		{
			return Options._hints;
		}
		set
		{
			if (Options._hints != value)
			{
				Options._hints = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool CurrencyShortForm
	{
		get
		{
			return Options._currencyShortForm;
		}
		set
		{
			if (Options._currencyShortForm != value)
			{
				Options._currencyShortForm = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool ColorBrightnessSlider
	{
		get
		{
			return Options._colorBrightnessSlider;
		}
		set
		{
			if (Options._colorBrightnessSlider != value)
			{
				Options._colorBrightnessSlider = value;
				Options.SaveToFile();
			}
		}
	}

	public static float Gamma
	{
		get
		{
			return Options._gamma;
		}
		set
		{
			if (Options._gamma != value)
			{
				Options._gamma = value;
				CameraScript.ApplyOptions();
				Options.SaveToFile();
			}
		}
	}

	public static bool FXAA
	{
		get
		{
			return Options._fxaa;
		}
		set
		{
			if (Options._fxaa != value)
			{
				Options._fxaa = value;
				CameraScript.ApplyOptions();
				Options.SaveToFile();
			}
		}
	}

	public static bool SMAA
	{
		get
		{
			return Options._smaa;
		}
		set
		{
			if (Options._smaa != value)
			{
				Options._smaa = (Options.CheckSMAASupport() && value);
				CameraScript.ApplyOptions();
				Options.SaveToFile();
			}
		}
	}

	public static float ScrollSpeed
	{
		get
		{
			return Options._scrollSpeed;
		}
		set
		{
			if (!Options._scrollSpeed.Equals(value))
			{
				Options._scrollSpeed = value;
				CameraScript.ApplySpeeds();
				Options.SaveToFile();
			}
		}
	}

	public static float ZoomSpeed
	{
		get
		{
			return Options._zoomSpeed;
		}
		set
		{
			if (!Options._zoomSpeed.Equals(value))
			{
				Options._zoomSpeed = value;
				CameraScript.ApplySpeeds();
				Options.SaveToFile();
			}
		}
	}

	public static float RotationSpeed
	{
		get
		{
			return Options._rotationSpeed;
		}
		set
		{
			if (!Options._rotationSpeed.Equals(value))
			{
				Options._rotationSpeed = value;
				CameraScript.ApplySpeeds();
				Options.SaveToFile();
			}
		}
	}

	public static bool EdgeScroll
	{
		get
		{
			return Options._edgeScroll;
		}
		set
		{
			if (Options._edgeScroll != value)
			{
				Options._edgeScroll = value;
				Cursor.lockState = Options.CursorLock();
				Options.SaveToFile();
			}
		}
	}

	public static bool RunInBackground
	{
		get
		{
			return Application.runInBackground;
		}
		set
		{
			if (Application.runInBackground != value)
			{
				Application.runInBackground = value;
				Options.SaveToFile();
			}
		}
	}

	public static Resolution TResolution
	{
		get
		{
			return Options._lastResolution;
		}
		set
		{
			Options.UpdateResolution(value, Options._lastFullscreen);
			Options.SaveToFile();
		}
	}

	public static bool Fullscreen
	{
		get
		{
			return Options._lastFullscreen;
		}
		set
		{
			if (Screen.fullScreen != value)
			{
				Options.UpdateResolution(Options._lastResolution, value);
				Options.SaveToFile();
			}
		}
	}

	public static int VSync
	{
		get
		{
			return QualitySettings.vSyncCount;
		}
		set
		{
			if (QualitySettings.vSyncCount != value)
			{
				QualitySettings.vSyncCount = value;
				Options.SaveToFile();
			}
		}
	}

	public static bool Shadows
	{
		get
		{
			return QualitySettings.shadowDistance > 0f;
		}
		set
		{
			if (Options.Shadows != value)
			{
				QualitySettings.shadowDistance = (float)((!value) ? 0 : 500);
				Options.SaveToFile();
			}
		}
	}

	public static int ShadowQuality
	{
		get
		{
			return (int)QualitySettings.shadowResolution;
		}
		set
		{
			if (QualitySettings.shadowResolution != (ShadowResolution)value)
			{
				QualitySettings.shadowResolution = (ShadowResolution)value;
				Options.SaveToFile();
			}
		}
	}

	public static int TargetFrameRate
	{
		get
		{
			return Application.targetFrameRate;
		}
		set
		{
			if (Application.targetFrameRate != value)
			{
				Application.targetFrameRate = value;
				Options.SaveToFile();
			}
		}
	}

	private static void ShowFileLoadError()
	{
		if (!Options.FailedToLoad && WindowManager.Instance != null)
		{
			WindowManager.SpawnDialog("OptionFileError".Loc(), false, DialogWindow.DialogType.Error);
			Options.FailedToLoad = true;
		}
	}

	public static CursorLockMode CursorLock()
	{
		return (!Options.EdgeScroll) ? CursorLockMode.None : CursorLockMode.Confined;
	}

	public static bool CheckSMAASupport()
	{
		Shader shader = Shader.Find("Hidden/Subpixel Morphological Anti-aliasing");
		return ImageEffectHelper.BasicSupport(shader);
	}

	private static void InitHints()
	{
		Options.Hints = new bool[Enum.GetValues(typeof(HintController.Hints)).Length];
		bool flag = false;
		try
		{
			if (File.Exists(Options.HintFile))
			{
				byte[] array = File.ReadAllBytes(Options.HintFile);
				for (int i = 0; i < Options.Hints.Length; i++)
				{
					if (i < array.Length)
					{
						Options.Hints[i] = (array[i] != 0);
					}
					else
					{
						Options.Hints[i] = true;
					}
				}
				flag = true;
			}
		}
		catch (Exception)
		{
		}
		if (!flag)
		{
			for (int j = 0; j < Options.Hints.Length; j++)
			{
				Options.Hints[j] = true;
			}
		}
	}

	public static void UpdateHint(int hint, bool value)
	{
		Options.Hints[hint] = value;
		try
		{
			byte[] array = new byte[Options.Hints.Length];
			for (int i = 0; i < Options.Hints.Length; i++)
			{
				array[i] = ((!Options.Hints[i]) ? 0 : 1);
			}
			File.WriteAllBytes(Options.HintFile, array);
		}
		catch (Exception)
		{
		}
	}

	public static void ResetHints()
	{
		for (int i = 1; i < Options.Hints.Length; i++)
		{
			Options.Hints[i] = true;
		}
		Options.UpdateHint(0, true);
	}

	public static bool HintEnabled(int hint)
	{
		return Options.Hints[hint];
	}

	public static Resolution FindRes(int w, int h, int r)
	{
		Resolution result = Screen.currentResolution;
		foreach (Resolution resolution in Screen.resolutions)
		{
			if (resolution.width == w && resolution.height == h)
			{
				if (resolution.refreshRate == r)
				{
					return resolution;
				}
				result = resolution;
			}
		}
		return result;
	}

	public static void UpdateResolution(Resolution r, bool fullscreen)
	{
		if (r.width < 1024 || r.height < 720)
		{
			r = Screen.resolutions.First((Resolution x) => x.width >= 1024 && x.height >= 720);
		}
		if (!Options.DisableResolutionChange)
		{
			Options._lastFullscreen = fullscreen;
			Options._lastResolution = r;
			Screen.SetResolution(r.width, r.height, fullscreen, r.refreshRate);
		}
	}

	public static void AddWindowSize(string id, SVector3 size)
	{
		if (!string.IsNullOrEmpty(id))
		{
			Options.WindowSizes[id] = size;
			Options.SaveSizes();
		}
	}

	private static void LoadSizes()
	{
		if (File.Exists(Options.WindowSizeFile))
		{
			try
			{
				ConfigFile configFile = ConfigFile.Load(File.ReadAllLines(Options.WindowSizeFile));
				foreach (KeyValuePair<string, List<string>> keyValuePair in configFile.Values)
				{
					if (keyValuePair.Value.Count > 0)
					{
						try
						{
							SVector3 svector = SVector3.Deserialize(keyValuePair.Value[0], true);
							if (svector.x != 0f || svector.y != 0f || svector.z != 0f || svector.w != 0f)
							{
								Options.WindowSizes[keyValuePair.Key] = svector;
							}
						}
						catch (Exception)
						{
						}
					}
				}
			}
			catch (Exception)
			{
			}
		}
	}

	private static void SaveSizes()
	{
		try
		{
			ConfigFile configFile = new ConfigFile();
			foreach (KeyValuePair<string, SVector3> keyValuePair in Options.WindowSizes)
			{
				configFile.Add(keyValuePair.Key, keyValuePair.Value.Serialize());
			}
			File.WriteAllText(Options.WindowSizeFile, configFile.Serialize());
		}
		catch (Exception)
		{
		}
	}

	public static void ResetSizes()
	{
		Options.WindowSizes.Clear();
		try
		{
			if (File.Exists(Options.WindowSizeFile))
			{
				File.Delete(Options.WindowSizeFile);
			}
		}
		catch (Exception)
		{
		}
	}

	public static SVector3 GetWindowSize(string id)
	{
		return (Options.WindowSizes != null) ? Options.WindowSizes.GetOrNull(id) : null;
	}

	public static void AddColumnWidth(string id, float width)
	{
		Options.ColumnWidths[id] = width;
		Options.SaveWidths();
	}

	public static bool GetColumnWidth(string id, out float result)
	{
		return Options.ColumnWidths.TryGetValue(id, out result);
	}

	private static void LoadWidths()
	{
		if (File.Exists(Options.ColumnWidthFile))
		{
			try
			{
				ConfigFile configFile = ConfigFile.Load(File.ReadAllLines(Options.ColumnWidthFile));
				foreach (KeyValuePair<string, List<string>> keyValuePair in configFile.Values)
				{
					if (keyValuePair.Value.Count > 0)
					{
						try
						{
							float num = (float)Convert.ToDouble(keyValuePair.Value[0]);
							if (num > 0f)
							{
								Options.ColumnWidths[keyValuePair.Key] = num;
							}
						}
						catch (Exception)
						{
						}
					}
				}
			}
			catch (Exception)
			{
			}
		}
	}

	private static void SaveWidths()
	{
		try
		{
			ConfigFile configFile = new ConfigFile();
			foreach (KeyValuePair<string, float> keyValuePair in Options.ColumnWidths)
			{
				configFile.Add(keyValuePair.Key, keyValuePair.Value.ToString());
			}
			File.WriteAllText(Options.ColumnWidthFile, configFile.Serialize());
		}
		catch (Exception)
		{
		}
	}

	public static void ResetWidths()
	{
		Options.ColumnWidths.Clear();
		try
		{
			if (File.Exists(Options.ColumnWidthFile))
			{
				File.Delete(Options.ColumnWidthFile);
			}
		}
		catch (Exception)
		{
		}
	}

	public static void UpdateResolution()
	{
		if (!Options.DisableResolutionChange)
		{
			Resolution lastResolution = Options._lastResolution;
			bool lastFullscreen = Options._lastFullscreen;
			Screen.SetResolution(lastResolution.width, lastResolution.height, lastFullscreen, lastResolution.refreshRate);
		}
	}

	public static void SaveToFile()
	{
		if (Options.Loaded)
		{
			ConfigFile configFile = new ConfigFile();
			configFile.Add("Currency", Options.Currency);
			configFile.Add("Language", Options.Language);
			configFile.Add("Tutorial", Options.Tutorial.ToString());
			configFile.Add("AMPM", Options.AMPM.ToString());
			configFile.Add("Celsius", Options.Celsius.ToString());
			configFile.Add("AutoSkip", Options.AutoSkip.ToString());
			configFile.Add("AutoSave", Options.AutoSave.ToString());
			configFile.Add("Backup", Options.Backup.ToString());
			configFile.Add("EdgeScroll", Options.EdgeScroll.ToString());
			configFile.Add("AmbientOcclusion", Options.AmbientOcclusion.ToString());
			configFile.Add("ConsoleOnError", Options.ConsoleOnError.ToString());
			configFile.Add("SSAA", Options.SSAA.ToString());
			configFile.Add("FXAA", Options.FXAA.ToString());
			configFile.Add("SMAA", Options.SMAA.ToString());
			configFile.Add("SSR", Options.SSR.ToString());
			configFile.Add("TiltShift", Options.TiltShift.ToString());
			configFile.Add("Frosted", Options.Frosted.ToString());
			configFile.Add("Bloom", Options.Bloom.ToString());
			configFile.Add("Gamma", Options.Gamma.ToString());
			configFile.Add("ScrollSpeed", Options.ScrollSpeed.ToString());
			configFile.Add("ZoomSpeed", Options.ZoomSpeed.ToString());
			configFile.Add("UISize", Options.UISize.ToString());
			configFile.Add("RotationSpeed", Options.RotationSpeed.ToString());
			configFile.Add("RunInBackground", Options.RunInBackground.ToString());
			configFile.Add("Fullscreen", Options._lastFullscreen.ToString());
			configFile.Add("Resolution", Options._lastResolution.width.ToString());
			configFile.Add("Resolution", Options._lastResolution.height.ToString());
			configFile.Add("Resolution", Options._lastResolution.refreshRate.ToString());
			configFile.Add("VSync", Options.VSync.ToString());
			configFile.Add("TargetFrameRate", Options.TargetFrameRate.ToString());
			configFile.Add("Shadows", Options.Shadows.ToString());
			configFile.Add("ShadowQuality", Options.ShadowQuality.ToString());
			configFile.Add("MoreShadow", Options.MoreShadow.ToString());
			configFile.Add("OpaqueGlass", Options.OpaqueGlass.ToString());
			configFile.Add("AskMarketing", Options.AskMarketing.ToString());
			configFile.Add("AskPrinting", Options.AskPrinting.ToString());
			configFile.Add("AskReporting", Options.AskReporting.ToString());
			configFile.Add("Hints", Options.HintsEnabled.ToString());
			configFile.Add("CurrencyShortForm", Options.CurrencyShortForm.ToString());
			configFile.Add("ColorBrightnessSlider", Options.ColorBrightnessSlider.ToString());
			configFile.AddRange("Keys", InputController.ToConfig(false));
			configFile.AddRange("AltKeys", InputController.ToConfig(true));
			configFile.AddRange("AudioLevels", AudioManager.ToConfig());
			configFile.AddRange("IgnoreQuestions", Options.IgnoreQuestions);
			try
			{
				File.WriteAllText(Options.SettingsFile, configFile.Serialize());
			}
			catch (Exception)
			{
				Options.ShowFileLoadError();
			}
		}
	}

	private static bool DisableResolutionChange = false;

	public static Vector2? MainPanelOffset = null;

	private static bool FailedToLoad = false;

	private static bool Loaded = false;

	private static string _currency = "Standard";

	private static bool _tutorial = true;

	private static bool _celsius = true;

	private static bool _autoSkip = false;

	private static bool _autoSave = true;

	private static bool _backup = true;

	private static bool _edgeScroll = false;

	private static bool _ambientOcclusion = true;

	private static int _ssaa = 10;

	private static bool _ssr = false;

	private static bool _fxaa = true;

	private static bool _smaa = false;

	private static bool _tiltShift = true;

	private static bool _bloom = true;

	private static bool _moreShadow = false;

	private static bool _opaqueGlass = false;

	private static bool _frosted = false;

	private static float _scrollSpeed = 100f;

	private static float _zoomSpeed = 50f;

	private static float _rotationSpeed = 10f;

	private static bool _lastFullscreen;

	private static Resolution _lastResolution;

	private static bool _askMarketing = true;

	private static bool _askPrinting = true;

	private static bool _askReporting = true;

	private static bool _hints = true;

	private static bool _currencyShortForm = false;

	private static bool _consoleOnError = true;

	private static string _language = "English";

	private static float _uiSize = 1f;

	private static float _gamma = 0.5f;

	private static bool _colorBrightnessSlider = true;

	public static HashSet<string> IgnoreQuestions = new HashSet<string>();

	private static Dictionary<string, SVector3> WindowSizes = new Dictionary<string, SVector3>();

	private static Dictionary<string, float> ColumnWidths = new Dictionary<string, float>();

	private static bool[] Hints;
}
