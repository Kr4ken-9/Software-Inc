﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUIWorkItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IEventSystemHandler
{
	public void Remove()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void ChangePriority(int val)
	{
		this.work.Priority = Mathf.Clamp(this.work.Priority + val, 1, 10);
		this.PriorityLabel.text = this.work.Priority.ToString();
	}

	private void Start()
	{
		if (this.work != null)
		{
			this.Init(this.work);
			this.PriorityLabel.text = this.work.Priority.ToString();
		}
		else
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public GameObject AddActionButton(string label, Action OnClick)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ActionButtonPrefab);
		gameObject.GetComponent<Button>().onClick.AddListener(delegate
		{
			OnClick();
		});
		gameObject.GetComponentInChildren<Text>().text = label.Loc();
		gameObject.transform.SetParent(this.ButtonPanel, false);
		gameObject.name = label + "Button";
		this.Buttons[label] = gameObject.GetComponent<Image>();
		return gameObject;
	}

	private void Init(WorkItem item)
	{
		this.work = item;
		base.name = this.work.GetWorkTypeName() + ((this.work.contract == null) ? string.Empty : "Contract");
		this.UpdatePauseButton();
		this.Title.text = this.work.Name;
		this.UpdateCollapse();
		SoftwareWorkItem workItem = item as SoftwareWorkItem;
		if (workItem != null)
		{
			this.AddBonusButton("Info", "Project details", delegate
			{
				GUIWorkItem.SpawnDevInfoWindow(workItem, workItem is DesignDocument);
			});
			if (this.work.contract == null && this.work.ActiveDeal == null)
			{
				this.AddBonusButton("Cogs", "Change software name", delegate
				{
					WindowManager.SpawnInputDialog("Change software name".Loc(), "Name".Loc(), workItem.SoftwareName, delegate(string newName)
					{
						if (!newName.Equals(workItem.SoftwareName))
						{
							if (!(from x in GameSettings.Instance.simulation.GetAllProducts()
							select x.Name).Contains(newName))
							{
								if (!(from x in GameSettings.Instance.simulation.Companies.Values.SelectMany((SimulatedCompany x) => x.Releases)
								select x.Name).Contains(newName))
								{
									if (!(from x in GameSettings.Instance.simulation.Companies.Values.SelectMany((SimulatedCompany x) => x.ProjectQueue)
									select x.Name).Contains(newName) && !GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareWorkItem>().Any((SoftwareWorkItem x) => x.SoftwareName.Equals(newName)))
									{
										this.work.Name = newName;
										workItem.SoftwareName = newName;
										MarketingPlan marketingPlan = GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>().FirstOrDefault((MarketingPlan x) => x.TargetItem == this.work);
										if (marketingPlan != null)
										{
											marketingPlan.CorrectName();
											return;
										}
										return;
									}
								}
							}
							WindowManager.Instance.ShowMessageBox("DesignProductNameError".Loc(), false, DialogWindow.DialogType.Error);
						}
					}, null);
				});
			}
		}
		if (item is SoftwareAlpha)
		{
			SoftwareAlpha lItem = item as SoftwareAlpha;
			this.AddBonusButton("Server", "Server".Loc(), delegate
			{
				IServerHost[] serv = GameSettings.Instance.GetAllServers();
				if (serv.Length > 0)
				{
					SelectorController.Instance.selectWindow.Show("Server", (from x in serv
					select x.ServerName).ToArray<string>(), delegate(int i)
					{
						if (i == -1)
						{
							GameSettings.Instance.DeregisterServerItem(lItem);
							lItem.Server2 = null;
						}
						else
						{
							GameSettings.Instance.RegisterWithServer(serv[i].ServerName, lItem, true);
							lItem.Server2 = serv[i].ServerName;
						}
					}, true, true, true, false, null);
				}
			});
			this.AddBonusButton("Smiley", "Past peer reviews".Loc(), delegate
			{
				if (lItem.PastReviews.Count > 0)
				{
					SelectorController.Instance.selectWindow.Show("Past peer reviews", from x in lItem.PastReviews
					select x.BuildDate.ToCompactString2(), delegate(int i)
					{
						HUD.Instance.reviewWindow.Show(lItem.PastReviews[i], null);
					}, false, true, true, false, null);
				}
			});
		}
		if (item is MarketingPlan)
		{
			MarketingPlan mark = (MarketingPlan)item;
			if (mark.Type == MarketingPlan.TaskType.PostMarket)
			{
				this.AddBonusButton("Money", "Budget".Loc(), delegate
				{
					WindowManager.SpawnInputDialog("Budget".Loc(), "Budget".Loc(), mark.MaxBudget.Currency(false), delegate(string x)
					{
						try
						{
							float x2 = (float)Convert.ToDouble(x);
							mark.MaxBudget = x2.FromCurrency();
						}
						catch (Exception)
						{
						}
					}, null);
				});
			}
		}
		this.MainIcon.sprite = IconManager.GetIcon(item.GetIcon());
		this.InitButtons();
		this.TypeBar.color = item.BackColor;
		this.PriorityLabel.text = this.work.Priority.ToString();
		this.work.InitWorkItem(this);
	}

	public static void SpawnDevInfoWindow(SoftwareWorkItem work, bool design)
	{
		SoftwareType type = work.Type;
		SDateTime time = SDateTime.Now();
		string[] features = work.Features;
		SoftwareProduct softwareProduct = (work.SequelTo == null) ? null : GameSettings.Instance.simulation.GetProduct(work.SequelTo.Value, false);
		SoftwareCategory softwareCategory = type.Categories[work._category];
		uint reach = type.GetReach(work._category, work.Features, work.OSs);
		softwareProduct = ((softwareProduct == null || !softwareProduct.Traded) ? softwareProduct : null);
		float num = type.DevTime(features, work._category, softwareProduct);
		float num2 = num;
		float codeArtRatio = work.CodeArtRatio;
		float num3 = num2 * codeArtRatio;
		if (type.OSSpecific && work.OSs != null)
		{
			int num4 = Mathf.Max(0, work.OSs.Length - 1);
			num += (float)num4;
			num3 += (float)num4;
		}
		int[] optimalEmployeeCount = type.GetOptimalEmployeeCount(num, codeArtRatio);
		int num5 = 0;
		float num6 = 0f;
		float num7 = 0f;
		foreach (Actor actor in work.DevTeams.SelectMany((string x) => GameSettings.Instance.sActorManager.Teams[x].GetEmployeesDirect()))
		{
			if (design)
			{
				if (actor.employee.IsRole(Employee.RoleBit.Designer))
				{
					num5++;
				}
			}
			else
			{
				bool flag = actor.employee.IsRole(Employee.RoleBit.Programmer);
				bool flag2 = actor.employee.IsRole(Employee.RoleBit.Artist);
				if (flag2 && flag)
				{
					num7 += 0.5f;
					num6 += 0.5f;
				}
				else if (flag2)
				{
					num7 += 1f;
				}
				else if (flag)
				{
					num6 += 1f;
				}
			}
		}
		num7 = Mathf.Ceil(num7);
		num6 = Mathf.Ceil(num6);
		List<string> list = new List<string>();
		List<string> list2 = new List<string>();
		list.Add("Type".Loc());
		list2.Add(((!work._category.Equals("Default")) ? (work._category.LocSWC(work._type) + " ") : string.Empty) + work._type.LocSW());
		list.Add("Started development".Loc());
		list2.Add(work.DevStart.ToCompactString2());
		if (design)
		{
			list.Add("Recommended designers".Loc());
			list2.Add(num5 + "/" + Mathf.Ceil((float)optimalEmployeeCount[0]).ToString());
		}
		else
		{
			list.Add("Recommended programmers".Loc());
			list.Add("Recommended artists".Loc());
			list2.Add(num6 + "/" + Mathf.Ceil((float)optimalEmployeeCount[1] * codeArtRatio).ToString());
			list2.Add(num7 + "/" + Mathf.Ceil((float)optimalEmployeeCount[1] * (1f - codeArtRatio)).ToString());
		}
		if (work.contract != null)
		{
			if ((design && !(work as DesignDocument).AllZero()) || (!design && (work.CodeArtRatio > 0f || (work as SoftwareAlpha).ArtProgress > 0f)))
			{
				list.Add("Deadline".Loc());
				list2.Add((work.DevStart + new SDateTime(work.contract.Months, 0)).ToCompactString2());
			}
			list.Add("Code units".Loc());
			list.Add("Art units".Loc());
			list.Add("Required quality".Loc());
			list.Add("Payout".Loc());
			list.Add("Penalty".Loc());
			list.Add("Cost per bug".Loc());
			list2.Add(work.contract.CodeUnits.ToString("N2"));
			list2.Add(work.contract.ArtUnits.ToString("N2"));
			list2.Add(SoftwareType.GetQualityLabel(work.contract.Quality));
			list2.Add(work.contract.Done.CurrencyInt(true));
			list2.Add(work.contract.Penalty.CurrencyInt(true));
			list2.Add(work.contract.PerBug.Currency(true));
		}
		else
		{
			list.Add("EstUnits".Loc(new object[]
			{
				"Code".Loc()
			}));
			list.Add("EstUnits".Loc(new object[]
			{
				"Art".Loc()
			}));
			list2.Add(num3.ToString("N2"));
			list2.Add((num2 * (1f - codeArtRatio)).ToString("N2"));
			WorkDeal activeDeal = work.ActiveDeal;
			if (activeDeal != null)
			{
				list.Add("Deadline".Loc());
				list2.Add(activeDeal.GetEndDate());
			}
			else
			{
				list.Add("Release date".Loc());
				list.Add("Consumer reach".Loc());
				list.Add("Royalties".Loc());
				list.Add("Appx bandwidth".Loc());
				list2.Add((work.ReleaseDate == null) ? "None".Loc() : work.ReleaseDate.Value.ToCompactString2());
				list2.Add(reach.ToString("N0"));
				list2.Add(((from x in features
				select type.Features[x] into x
				where x.PatentOwner > 0u && x.PatentOwner != GameSettings.Instance.MyCompany.ID
				select x).SumSafe((Feature x) => x.Royalty) * 100f).ToString("F") + "%");
				list2.Add((type.GetServerReq(features) * reach * softwareCategory.Popularity * softwareCategory.Retention / num2).BandwidthFactor(time).Bandwidth());
			}
		}
		foreach (KeyValuePair<string, uint> keyValuePair in work.Needs)
		{
			list.Add(keyValuePair.Key.LocSW());
			list2.Add(GameSettings.Instance.simulation.GetProduct(keyValuePair.Value, false).Name);
		}
		if (type.OSSpecific && work.OSs != null)
		{
			list.Add("Operating systems".Loc());
			for (int i = 0; i < work.OSs.Length - 1; i++)
			{
				list.Add(string.Empty);
			}
			for (int j = 0; j < work.OSs.Length; j++)
			{
				list2.Add(GameSettings.Instance.simulation.GetProduct(work.OSs[j], true).Name);
			}
		}
		list.Add("SCM".Loc());
		list2.Add(work.Server2 ?? "None".Loc());
		WindowManager.SpawnTableInfoWindow("Project details".Loc(), work.SoftwareName, list.ToArray(), list2.ToArray(), "WorkInfo" + work.SoftwareName);
	}

	private void InitButtons()
	{
		if (this.work is DesignDocument)
		{
			this.AddActionButton("Assign", delegate
			{
				this.Assign(null, null);
			});
			SoftwareWorkItem swItem = (SoftwareWorkItem)this.work;
			this.AddActionButton("Develop", delegate
			{
				if (swItem.ActiveDeal == null && swItem.GetProgress() < ((swItem.contract != null) ? (swItem.contract.Quality / 2f) : 0.25f))
				{
					WindowManager.Instance.ShowMessageBox("DesignSafetyCheck".Loc(), true, DialogWindow.DialogType.Question, delegate
					{
						swItem.PromoteAction();
					}, "Promote from design", null);
				}
				else
				{
					swItem.PromoteAction();
				}
			});
			if (this.work.contract == null && this.work.ActiveDeal == null && !swItem.InHouse)
			{
				this.AddActionButton("Market", delegate
				{
					HUD.Instance.marketingWindow.Show(swItem);
				});
			}
			this.AddActionButton("Cancel", delegate
			{
				WindowManager.Instance.ShowMessageBox("WorkItemCancelConf".Loc(new object[]
				{
					this.work.Name
				}), true, DialogWindow.DialogType.Warning, delegate
				{
					this.work.Kill(true);
				}, "Cancel work", null);
			});
		}
		else if (this.work is SoftwareAlpha)
		{
			SoftwareAlpha swItem = (SoftwareAlpha)this.work;
			this.AddActionButton("Assign", delegate
			{
				this.Assign(null, delegate
				{
					swItem.CheckCompetency();
				});
			});
			bool flag = this.work.contract == null && this.work.ActiveDeal == null;
			bool flag2 = flag && !swItem.InHouse;
			if (!flag2)
			{
				this.PromoteText = this.AddActionButton("Promote", delegate
				{
					swItem.PromoteAction();
				}).GetComponentInChildren<Text>();
			}
			if (flag2)
			{
				this.AddActionButton("Market", delegate
				{
					HUD.Instance.marketingWindow.Show(swItem);
				});
			}
			this.AddActionButton("Review", delegate
			{
				if (swItem.InDelay || swItem.InBeta)
				{
					WindowManager.Instance.ShowMessageBox("ReviewError".Loc(), false, DialogWindow.DialogType.Error);
					return;
				}
				DialogWindow msg = WindowManager.SpawnDialog();
				Dictionary<string, Action> dictionary = new Dictionary<string, Action>();
				dictionary["Team"] = delegate
				{
					HUD.Instance.TeamSelectWindow.Show(false, GameSettings.Instance.GetDefaultTeams("Review"), delegate(string[] t)
					{
						ReviewWork reviewWork2 = new ReviewWork(swItem, null, false);
						GameSettings.Instance.MyCompany.WorkItems.Add(reviewWork2);
						foreach (string key in t)
						{
							reviewWork2.AddDevTeam(GameSettings.Instance.sActorManager.Teams[key], false);
						}
					}, "Review");
					msg.Window.Close();
				};
				dictionary["Outsource"] = delegate
				{
					ReviewWork item = new ReviewWork(swItem, GameSettings.Instance.RNG["ContractCompany"].GenerateName(), true);
					GameSettings.Instance.MyCompany.WorkItems.Add(item);
					msg.Window.Close();
				};
				if (swItem.ActiveDeal != null || swItem.contract != null)
				{
					dictionary["Client"] = delegate
					{
						ReviewWork item = new ReviewWork(swItem, (swItem.ActiveDeal == null) ? swItem.contract.Company : swItem.ActiveDeal.CompanyName, false);
						GameSettings.Instance.MyCompany.WorkItems.Add(item);
						msg.Window.Close();
					};
				}
				dictionary["Cancel"] = delegate
				{
					msg.Window.Close();
				};
				msg.Show("ReviewMessage".Loc(new object[]
				{
					ReviewWork.StandardCost.Currency(true)
				}), false, DialogWindow.DialogType.Question, dictionary.ToArray<KeyValuePair<string, Action>>());
			});
			if (flag)
			{
				this.AddActionButton("Print", delegate
				{
					if (swItem.contract != null || swItem.ActiveDeal != null)
					{
						WindowManager.Instance.ShowMessageBox("PlayerIPPrintError".Loc(), false, DialogWindow.DialogType.Error);
					}
					else if (swItem.InBeta)
					{
						uint id = swItem.ForceID();
						if (!GameSettings.Instance.PrintOrders.ContainsKey(id))
						{
							if (GameSettings.Instance.ProductPrinters.Count == 0)
							{
								WindowManager.Instance.ShowMessageBox("NoPrintersWarning".Loc(new object[]
								{
									MarketSimulation.PhysicalCopyPrice.Currency(true)
								}), false, DialogWindow.DialogType.Question, delegate
								{
									PrintJob printJob2 = new PrintJob(id);
									GameSettings.Instance.PrintOrders[id] = printJob2;
									HUD.Instance.distributionWindow.Show(printJob2);
								}, null, null);
							}
							else
							{
								PrintJob printJob = new PrintJob(id);
								GameSettings.Instance.PrintOrders[id] = printJob;
								HUD.Instance.distributionWindow.Show(printJob);
							}
						}
					}
					else
					{
						WindowManager.Instance.ShowMessageBox("BetaPrintError".Loc(), false, DialogWindow.DialogType.Error);
					}
				});
			}
			if (flag2)
			{
				this.PromoteText = this.AddActionButton("Promote", new Action(swItem.UserPromote)).GetComponentInChildren<Text>();
			}
			this.AddActionButton("Cancel", delegate
			{
				WindowManager.Instance.ShowMessageBox("WorkItemCancelConf".Loc(new object[]
				{
					this.work.Name
				}), true, DialogWindow.DialogType.Warning, delegate
				{
					this.work.Kill(true);
				}, "Cancel work", null);
			});
		}
		else if (this.work is SupportWork)
		{
			this.AddActionButton("Assign", delegate
			{
				this.Assign("Support", null);
			});
			this.AddActionButton("CancelSupport", new Action(((SupportWork)this.work).CancelSupport));
		}
		else if (this.work is MarketingPlan)
		{
			MarketingPlan mp = (MarketingPlan)this.work;
			this.AddActionButton("Assign", delegate
			{
				this.Assign(null, null);
			});
			if (mp.Type == MarketingPlan.TaskType.PressRelease)
			{
				this.AddActionButton("Release", delegate
				{
					if (mp.TargetItem.ReleaseDate != null)
					{
						mp.StopMarketing();
					}
					else
					{
						WindowManager.Instance.ShowMessageBox("MarketingReleaseDateWarning".Loc(new object[]
						{
							"Press release".Loc().ToLower()
						}), true, DialogWindow.DialogType.Question, delegate
						{
							mp.StopMarketing();
						}, "Marketing without release date", null);
					}
				});
				this.AddActionButton("Cancel", delegate
				{
					mp.Kill(false);
				});
			}
			else
			{
				this.AddActionButton("MarketingStop", new Action(mp.StopMarketing));
			}
		}
		else if (this.work is AutoDevWorkItem)
		{
			AutoDevWorkItem ad = (AutoDevWorkItem)this.work;
			this.AddActionButton("Assign", delegate
			{
				List<Actor> leaders = (from x in GameSettings.Instance.sActorManager.Actors
				where x.employee.IsRole(Employee.RoleBit.Lead)
				select x).ToList<Actor>();
				if (leaders.Count > 0)
				{
					SelectorController.Instance.selectWindow.Show("Pick a leader", from x in leaders
					select x.employee.FullName, delegate(int x)
					{
						ad.Leader = ((x != -1) ? leaders[x] : null);
					}, false, true, true, false, null);
				}
				else
				{
					WindowManager.Instance.ShowMessageBox("AutoDevNoLeaders".Loc(), false, DialogWindow.DialogType.Error);
				}
			});
			this.AddActionButton("Options", new Action(ad.OpenOptions));
			this.AddActionButton("Cancel", delegate
			{
				WindowManager.Instance.ShowMessageBox("WorkItemCancelConf".Loc(new object[]
				{
					this.work.Name
				}), true, DialogWindow.DialogType.Warning, delegate
				{
					this.work.Kill(false);
				}, "Cancel work", null);
			});
		}
		else if (this.work is ResearchWork)
		{
			this.AddActionButton("Assign", delegate
			{
				this.Assign(null, null);
			});
			this.AddActionButton("Patent", new Action(((ResearchWork)this.work).PatentNow));
			this.AddActionButton("Cancel", delegate
			{
				WindowManager.Instance.ShowMessageBox("ResearchCancelWarning".Loc(), true, DialogWindow.DialogType.Warning, delegate
				{
					this.work.Kill(true);
				}, null, null);
			});
		}
		else if (this.work is SoftwarePort)
		{
			this.AddActionButton("Assign", delegate
			{
				this.Assign(null, null);
			});
			this.AddActionButton("Skip", delegate
			{
				(this.work as SoftwarePort).Skip();
			});
			this.AddActionButton("Cancel", delegate
			{
				this.work.Kill(true);
			});
		}
		else
		{
			if (!(this.work is ReviewWork))
			{
				throw new Exception("Couldn't find buttons to add to work item");
			}
			ReviewWork reviewWork = (ReviewWork)this.work;
			if (reviewWork.ReviewCompany == null)
			{
				this.AddActionButton("Assign", delegate
				{
					this.Assign(null, null);
				});
			}
			this.AddActionButton("End", new Action(reviewWork.EndReview));
		}
	}

	public void InitPos()
	{
		if (this.work.SiblingIndex > -1)
		{
			base.transform.SetSiblingIndex(this.work.SiblingIndex);
		}
		else
		{
			this.work.SiblingIndex = base.transform.GetSiblingIndex();
		}
	}

	public void AddBonusButton(string icon, string tip, UnityAction onClick)
	{
		this.AddBonusButton(icon, tip, null, onClick);
	}

	public void AddBonusButton(string icon, string tip, string desc, UnityAction onClick)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.BonusButtonPrefab);
		gameObject.GetComponent<Image>().sprite = IconManager.GetIcon(icon);
		GUIToolTipper component = gameObject.GetComponent<GUIToolTipper>();
		component.ToolTipValue = tip;
		component.TooltipDescription = desc;
		gameObject.GetComponent<Button>().onClick.AddListener(onClick);
		gameObject.transform.SetParent(this.BonusButtonPanel.transform, false);
	}

	public void Collapse()
	{
		HintController.Instance.Show(HintController.Hints.HintWorkItemActions);
		this.work.Collapsed = !this.work.Collapsed;
		this.UpdateCollapse();
		SelectorController.CanClick = false;
	}

	public void UpdateActivation()
	{
		if (this != null && base.gameObject != null && HUD.Instance != null && HUD.Instance.WorkToToggle != null && this.work != null)
		{
			bool flag = HUD.Instance.GetWorkTypeToggled(this.work.GetWorkTypeName()) && !this.work.Hidden;
			if (SelectorController.Instance != null && SelectorController.Instance.SelectedTeams != null && flag)
			{
				flag = false;
				foreach (string item in this.work.DevTeams)
				{
					if (SelectorController.Instance.SelectedTeams.Contains(item))
					{
						flag = true;
						break;
					}
				}
			}
			base.gameObject.SetActive(flag);
		}
	}

	public void Expand()
	{
		if (this.work.Collapsed)
		{
			this.work.Collapsed = false;
			this.UpdateCollapse();
		}
	}

	public void Assign(string saveCat = null, Action a = null)
	{
		if (this.work.CanUseCompany())
		{
			HUD.Instance.TeamSelectWindow.Show(this.work.DevTeams, this.work.CompanyWorker, delegate(string[] t, SimulatedCompany c)
			{
				if (c != null)
				{
					this.work.CompanyWorker = c;
				}
				else
				{
					foreach (string text in this.work.DevTeams.ToArray<string>())
					{
						if (!t.Contains(text))
						{
							this.work.RemoveDevTeam(GameSettings.Instance.sActorManager.Teams[text]);
						}
					}
					for (int j = 0; j < t.Length; j++)
					{
						if (!this.work.DevTeams.Contains(t[j]))
						{
							this.work.AddDevTeam(GameSettings.Instance.sActorManager.Teams[t[j]], false);
						}
					}
				}
				if (a != null)
				{
					a();
				}
			}, saveCat);
		}
		else
		{
			HUD.Instance.TeamSelectWindow.Show(false, this.work.DevTeams, delegate(string[] t)
			{
				foreach (string text in this.work.DevTeams.ToArray<string>())
				{
					if (!t.Contains(text))
					{
						this.work.RemoveDevTeam(GameSettings.Instance.sActorManager.Teams[text]);
					}
				}
				for (int j = 0; j < t.Length; j++)
				{
					if (!this.work.DevTeams.Contains(t[j]))
					{
						this.work.AddDevTeam(GameSettings.Instance.sActorManager.Teams[t[j]], false);
					}
				}
				if (a != null)
				{
					a();
				}
			}, saveCat);
		}
	}

	private void UpdateCollapse()
	{
		if (this.work.Collapsed)
		{
			this.WorkLayout.minHeight = 43f;
			base.GetComponent<Image>().sprite = this.SprCollapse;
			this.ButtonPanel.gameObject.SetActive(false);
		}
		else
		{
			this.WorkLayout.minHeight = 128f;
			base.GetComponent<Image>().sprite = this.SprExpand;
			this.ButtonPanel.gameObject.SetActive(true);
		}
	}

	private void OnEnable()
	{
		if (HUD.Instance != null && HUD.Instance.MainWorkItemPanel != null)
		{
			NoDragScrollRect component = HUD.Instance.MainWorkItemPanel.GetComponent<NoDragScrollRect>();
			component.OnChange(component.normalizedPosition);
		}
	}

	private void OnDisable()
	{
		if (HUD.Instance != null && HUD.Instance.MainWorkItemPanel != null)
		{
			NoDragScrollRect component = HUD.Instance.MainWorkItemPanel.GetComponent<NoDragScrollRect>();
			component.OnChange(component.normalizedPosition);
		}
	}

	private void SetIndicator(float value)
	{
		if (value > 0f && value < 1f)
		{
			this.MaxIndicator.gameObject.SetActive(true);
			this.MaxIndicator.anchoredPosition = new Vector2(256f * value, 0f);
		}
		else
		{
			this.MaxIndicator.gameObject.SetActive(false);
		}
	}

	private void Update()
	{
		string team = this.work.GetTeam();
		this.ButtonPanel.sizeDelta = new Vector2(this.ButtonPanel.sizeDelta.x, Mathf.Lerp(this.ButtonPanel.sizeDelta.y, this.ButtonPanelHeight, Time.deltaTime * 10f));
		if (this.work.Collapsed)
		{
			this.Description.text = this.work.CollapseLabel();
		}
		else
		{
			this.State.text = this.work.CurrentStage();
			this.Team.text = (team ?? "Unassigned".Loc());
			this.TeamImg.sprite = ((this.work.CompanyWorker == null) ? this.PeopleTeam : this.CompanyTeam);
			this.Description.text = this.work.Category();
			this.Progress.text = this.work.GetProgressLabel();
		}
		float num = this.work.GetProgress();
		if (!num.IsValidFloat())
		{
			num = 0f;
		}
		if (this.work is DesignDocument)
		{
			num = 1f - Mathf.Abs(num * 2f - 1f);
		}
		if (num > 0f)
		{
			this.ProgressBar.gameObject.SetActive(true);
			this.ProgressBar.sizeDelta = new Vector2(-(1f - num) * 256f, this.ProgressBar.sizeDelta.y);
		}
		else
		{
			this.ProgressBar.gameObject.SetActive(false);
		}
		string text = this.work.HightlightButton();
		if (this.ActiveButton != null && text == null)
		{
			this.ActiveButton.color = Color.white;
			this.ActiveButton = null;
			this.ButtonPanelHeight = 0f;
		}
		else if (this.ActiveButton == null && text != null)
		{
			this.ActiveButton = this.Buttons.GetOrNull(text);
		}
		if (this.ActiveButton != null)
		{
			this.ButtonPanelHeight = 43f;
			this.ActiveButton.color = ((Time.realtimeSinceStartup * 2f % 2f >= 1f) ? Color.red : Color.white);
		}
		this.ProgImg.color = this.work.GetProgressColor();
		this.SetIndicator(this.work.GetMax());
		this.UnassignWarning.SetActive(team == null);
		if (this.work is SoftwareAlpha)
		{
			SoftwareAlpha softwareAlpha = this.work as SoftwareAlpha;
			this.PromoteText.text = ((!softwareAlpha.InBeta) ? ((!softwareAlpha.InDelay) ? "Promote" : "Beta") : "Release").Loc();
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (this.work is DesignDocument || this.work is ResearchWork)
		{
			SpecializationProgress.Instance.Show(this);
		}
		this.ButtonPanelHeight = 43f;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		SpecializationProgress.Instance.Hide();
		this.ButtonPanelHeight = 0f;
	}

	public void StartDrag()
	{
		HintController.Instance.Show(HintController.Hints.HintWorkItemActions);
		HUD.Instance.DraggingWork = this;
		base.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		if (HUD.Instance != null && HUD.Instance.DraggingWork == this)
		{
			HUD.Instance.DraggingWork = null;
		}
	}

	public void PauseWork()
	{
		this.work.Paused = !this.work.Paused;
		this.UpdatePauseButton();
	}

	public void UpdatePauseButton()
	{
		this.PauseButton.sprite = ((!this.work.Paused) ? this.Play : this.Pause);
		this.PauseWarning.gameObject.SetActive(this.work.Paused);
	}

	public LayoutElement WorkLayout;

	public GameObject ActionButtonPrefab;

	public Text Title;

	public Text Description;

	public Text State;

	public Text Progress;

	public Text Team;

	public Text PriorityLabel;

	public WorkItem work;

	public Image TypeBar;

	public Image ProgImg;

	public Image TeamImg;

	public RectTransform MaxIndicator;

	public RectTransform ButtonPanel;

	public RectTransform ProgressBar;

	public Sprite SprExpand;

	public Sprite SprCollapse;

	public Sprite Play;

	public Sprite Pause;

	public Sprite Deal;

	public Sprite PeopleTeam;

	public Sprite CompanyTeam;

	public Image MainIcon;

	public Image StateIcon;

	public float ButtonPanelHeight;

	public GameObject UnassignWarning;

	public GameObject PauseWarning;

	public GameObject BonusButtonPanel;

	public GameObject BonusButtonPrefab;

	public GameObject[] PriorityStuff;

	public Text PromoteText;

	public Image PauseButton;

	public Image ActiveButton;

	[NonSerialized]
	private Dictionary<string, Image> Buttons = new Dictionary<string, Image>();
}
