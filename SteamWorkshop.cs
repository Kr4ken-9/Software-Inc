﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using DevConsole;
using Steamworks;
using UnityEngine;

public class SteamWorkshop : MonoBehaviour
{
	private void Start()
	{
		if (SteamWorkshop.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		if (!SteamManager.Initialized)
		{
			SteamWorkshop.DoneLoading = !SteamManager.Initialized;
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
		this.LoadWorkshopCache();
		SteamWorkshop.Instance = this;
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		if (SteamManager.Initialized)
		{
			this._UGCQuery = CallResult<SteamUGCQueryCompleted_t>.Create(new CallResult<SteamUGCQueryCompleted_t>.APIDispatchDelegate(this.OnUGCquery));
			this._createItemResult = CallResult<CreateItemResult_t>.Create(new CallResult<CreateItemResult_t>.APIDispatchDelegate(this.CreateItemResult));
			this._submitItemResult = CallResult<SubmitItemUpdateResult_t>.Create(new CallResult<SubmitItemUpdateResult_t>.APIDispatchDelegate(this.SubmitItemResult));
			SteamWorkshop.UserID = SteamUser.GetSteamID().m_SteamID;
			List<PublishedFileId_t> list = new List<PublishedFileId_t>();
			this.CheckItems(GameData.ModPackages.OfType<IWorkshopItem>(), list);
			this.CheckItems(Localization.Translations.Values.OfType<IWorkshopItem>(), list);
			this.CheckItems(GameData.Prefabs.OfType<IWorkshopItem>(), list);
			this.CheckItems(RoomMaterialController.Instance.MaterialPacks.OfType<IWorkshopItem>(), list);
			this.CheckItems(SaveGameManager.WorkshoppableGames.OfType<IWorkshopItem>(), list);
			uint num = SteamUGC.GetNumSubscribedItems();
			PublishedFileId_t[] array = new PublishedFileId_t[num];
			num = SteamUGC.GetSubscribedItems(array, num);
			int num2 = 0;
			while ((long)num2 < (long)((ulong)num))
			{
				ulong num3;
				string path;
				uint num4;
				if (SteamUGC.GetItemInstallInfo(array[num2], out num3, out path, 512u, out num4))
				{
					string modType = SteamWorkshop.GetModType(path);
					IWorkshopItem workshopItem = SteamWorkshop.LoadMod(modType, path, "Steam ID: " + Path.GetFileName(path), true);
					if (workshopItem != null)
					{
						this.UpdateItemStatus(workshopItem, array[num2]);
						list.Add(array[num2]);
					}
				}
				num2++;
			}
			if (list.Count > 0)
			{
				SteamWorkshop.RunningQuery = true;
				UGCQueryHandle_t handle = SteamUGC.CreateQueryUGCDetailsRequest(list.ToArray(), (uint)list.Count);
				SteamAPICall_t hAPICall = SteamUGC.SendQueryUGCRequest(handle);
				this._UGCQuery.Set(hAPICall, null);
			}
			if (ModWindow.Instance != null)
			{
				ModWindow.Instance.Refresh();
			}
			LoadDebugger.AddInfo("Finished loading Steam Workshop");
		}
		SteamWorkshop.DoneLoading = true;
	}

	public static void RecheckItems(IEnumerable<IWorkshopItem> items)
	{
		if (SteamWorkshop.Instance != null)
		{
			List<PublishedFileId_t> list = new List<PublishedFileId_t>();
			SteamWorkshop.Instance.CheckItems(items, list);
			if (list.Count > 0)
			{
				if (SteamWorkshop.RunningQuery)
				{
					SteamWorkshop.QueryQueue.Enqueue(list);
				}
				else
				{
					SteamWorkshop.Instance._UGCQuery = CallResult<SteamUGCQueryCompleted_t>.Create(new CallResult<SteamUGCQueryCompleted_t>.APIDispatchDelegate(SteamWorkshop.Instance.OnUGCquery));
					SteamWorkshop.RunningQuery = true;
					UGCQueryHandle_t handle = SteamUGC.CreateQueryUGCDetailsRequest(list.ToArray(), (uint)list.Count);
					SteamAPICall_t hAPICall = SteamUGC.SendQueryUGCRequest(handle);
					SteamWorkshop.Instance._UGCQuery.Set(hAPICall, null);
				}
			}
		}
	}

	private void LoadWorkshopCache()
	{
		try
		{
			if (File.Exists("WorkshopCache.txt"))
			{
				ConfigFile configFile = ConfigFile.Load(File.ReadAllLines("WorkshopCache.txt"));
				foreach (KeyValuePair<string, List<string>> keyValuePair in configFile.Values)
				{
					if (keyValuePair.Value.Count != 0)
					{
						string text = keyValuePair.Value[0];
						IWorkshopItem workshopItem = SteamWorkshop.LoadMod(SteamWorkshop.GetModType(text), text, keyValuePair.Key, false);
						if (workshopItem != null)
						{
							workshopItem.ItemTitle = keyValuePair.Key;
							workshopItem.CanUpload = false;
							this.PreviousItems[keyValuePair.Key] = text;
						}
						else
						{
							this.FailedLoads.Add(SteamWorkshop.NormalizePath(text));
							GameData.FailedModList.Add(keyValuePair.Key);
						}
					}
				}
			}
		}
		catch (Exception)
		{
		}
	}

	private void RefreshWorkshopCache()
	{
		try
		{
			string value = SteamWorkshop.NormalizePath(Path.GetFullPath("./"));
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			foreach (KeyValuePair<string, string> keyValuePair in this.PreviousItems)
			{
				if (Directory.Exists(keyValuePair.Value))
				{
					dictionary[keyValuePair.Key] = keyValuePair.Value;
				}
			}
			foreach (KeyValuePair<PublishedFileId_t, IWorkshopItem> keyValuePair2 in SteamWorkshop.WorkshopItems)
			{
				if (!SteamWorkshop.NormalizePath(keyValuePair2.Value.FolderPath()).StartsWith(value))
				{
					dictionary[keyValuePair2.Value.ItemTitle] = keyValuePair2.Value.FolderPath();
				}
			}
			if (dictionary.Count > 0)
			{
				ConfigFile configFile = new ConfigFile();
				foreach (KeyValuePair<string, string> keyValuePair3 in dictionary)
				{
					configFile.Add(keyValuePair3.Key, keyValuePair3.Value);
				}
				File.WriteAllText("WorkshopCache.txt", configFile.Serialize());
			}
		}
		catch (Exception)
		{
		}
	}

	private void CheckItems(IEnumerable<IWorkshopItem> items, List<PublishedFileId_t> pubIds)
	{
		foreach (IWorkshopItem item in items)
		{
			PublishedFileId_t? publishedFileId_t = SteamWorkshop.CheckModID(item);
			if (publishedFileId_t != null)
			{
				this.UpdateItemStatus(item, publishedFileId_t.Value);
				pubIds.Add(publishedFileId_t.Value);
			}
		}
	}

	private void Update()
	{
		if (MainMenuController.Instance != null)
		{
			if (this.CurrentUpdate != null)
			{
				MainMenuController.Instance.pBarGO.SetActive(true);
				ulong num;
				ulong num2;
				switch (SteamUGC.GetItemUpdateProgress(this.CurrentUpdate.Value, out num, out num2))
				{
				case EItemUpdateStatus.k_EItemUpdateStatusInvalid:
					WindowManager.SpawnDialog("FailedSteamUpload".Loc(new object[]
					{
						"Update status invalid"
					}), true, DialogWindow.DialogType.Error);
					this.WaitPanel(false);
					this.CurrentUpdate = null;
					this.UploadingItem = null;
					break;
				case EItemUpdateStatus.k_EItemUpdateStatusPreparingConfig:
					MainMenuController.Instance.pBarText.text = "Preparing".Loc();
					break;
				case EItemUpdateStatus.k_EItemUpdateStatusPreparingContent:
					MainMenuController.Instance.pBarText.text = "Preparing".Loc();
					break;
				case EItemUpdateStatus.k_EItemUpdateStatusUploadingContent:
					MainMenuController.Instance.pBarText.text = "Uploading".Loc();
					break;
				case EItemUpdateStatus.k_EItemUpdateStatusUploadingPreviewFile:
					MainMenuController.Instance.pBarText.text = "Uploading".Loc();
					break;
				case EItemUpdateStatus.k_EItemUpdateStatusCommittingChanges:
					MainMenuController.Instance.pBarText.text = "Committing".Loc();
					break;
				default:
					MainMenuController.Instance.pBarText.text = string.Empty;
					break;
				}
				MainMenuController.Instance.pBar.Value = ((num2 <= 0UL) ? 0f : (num / num2));
			}
			else
			{
				MainMenuController.Instance.pBarGO.SetActive(false);
			}
		}
	}

	public void UpdateItemStatus(IWorkshopItem item, PublishedFileId_t id)
	{
		item.CanUpload = false;
		item.SetTitle = false;
		item.SteamID = new PublishedFileId_t?(id);
		SteamWorkshop.WorkshopItems[id] = item;
	}

	public static void PrepareMod(IWorkshopItem item)
	{
		string path = Path.Combine(item.FolderPath(), "TypeInfo.txt");
		if (File.Exists(path))
		{
			File.Delete(path);
		}
		File.WriteAllText(path, item.GetWorkshopType());
		path = Path.Combine(item.FolderPath(), "pubID.txt");
		if (File.Exists(path))
		{
			File.Delete(path);
		}
	}

	public static string CheckValid(string[] ext, string path)
	{
		if (Directory.Exists(path))
		{
			foreach (string path2 in Directory.GetFiles(path))
			{
				string text = Path.GetExtension(path2).ToLower();
				bool flag = false;
				for (int j = 0; j < ext.Length; j++)
				{
					if (text.EndsWith(ext[j]))
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					return "SteamUploadFileType".Loc(new object[]
					{
						text
					});
				}
			}
			foreach (string path3 in Directory.GetDirectories(path))
			{
				string text2 = SteamWorkshop.CheckValid(ext, path3);
				if (text2 != null)
				{
					return text2;
				}
			}
			return null;
		}
		return "SteamUploadDirectoryError".Loc(new object[]
		{
			path
		});
	}

	public static string GetModType(string path)
	{
		string path2 = Path.Combine(path, "TypeInfo.txt");
		if (File.Exists(path2))
		{
			return File.ReadAllText(path2).Trim();
		}
		return null;
	}

	public static PublishedFileId_t? CheckModID(IWorkshopItem item)
	{
		string path = Path.Combine(item.FolderPath(), "pubID.txt");
		if (File.Exists(path))
		{
			try
			{
				ulong value = Convert.ToUInt64(File.ReadAllText(path).Trim());
				return new PublishedFileId_t?(new PublishedFileId_t(value));
			}
			catch (Exception)
			{
			}
		}
		return null;
	}

	public static void WriteModID(IWorkshopItem item)
	{
		string path = Path.Combine(item.FolderPath(), "pubID.txt");
		if (File.Exists(path))
		{
			File.Delete(path);
		}
		if (item.SteamID != null)
		{
			File.WriteAllText(path, item.SteamID.Value.ToString());
		}
	}

	public static IWorkshopItem LoadMod(string type, string path, string name, bool withDummy)
	{
		string p = SteamWorkshop.NormalizePath(path);
		if (SteamWorkshop.Instance != null && SteamWorkshop.Instance.FailedLoads.Contains(p))
		{
			return null;
		}
		DummyWorkshopItem dummyWorkshopItem = (!withDummy) ? null : new DummyWorkshopItem(type, path, name, null);
		if ("Mod".Equals(type))
		{
			ModPackage modPackage = GameData.ModPackages.FirstOrDefault((ModPackage x) => SteamWorkshop.NormalizePath(x.Root).Equals(p));
			if (modPackage == null)
			{
				return GameData.LoadSteamMod(path, name) ?? dummyWorkshopItem;
			}
			return modPackage;
		}
		else if ("Localization".Equals(type))
		{
			Localization.Translation translation = Localization.Translations.Values.FirstOrDefault((Localization.Translation x) => SteamWorkshop.NormalizePath(x.Root).Equals(p));
			if (translation == null)
			{
				return Localization.LoadSteamLanguage(path, name) ?? dummyWorkshopItem;
			}
			return translation;
		}
		else if ("Blueprint".Equals(type))
		{
			BuildingPrefab buildingPrefab = GameData.Prefabs.FirstOrDefault((BuildingPrefab x) => SteamWorkshop.NormalizePath(x.Root).Equals(p));
			if (buildingPrefab == null)
			{
				return GameData.LoadSteamPrefab(path, name) ?? dummyWorkshopItem;
			}
			return buildingPrefab;
		}
		else if ("Furniture".Equals(type))
		{
			FurnitureMod furnitureMod = FurnitureLoader.LoadedFurniture.FirstOrDefault((FurnitureMod x) => SteamWorkshop.NormalizePath(x.Root).Equals(p));
			if (furnitureMod == null)
			{
				bool flag = false;
				return FurnitureLoader.LoadFurnitureMod(path, ref flag, name, false) ?? dummyWorkshopItem;
			}
			return furnitureMod;
		}
		else if ("Material".Equals(type))
		{
			FurnitureMod furnitureMod2 = FurnitureLoader.LoadedFurniture.FirstOrDefault((FurnitureMod x) => SteamWorkshop.NormalizePath(x.Root).Equals(p));
			if (furnitureMod2 != null)
			{
				return furnitureMod2;
			}
			RoomMaterialPack roomMaterialPack = RoomMaterialPack.LoadPack(path, false, ref RoomMaterialController.ErrorsDuringLoad);
			if (roomMaterialPack != null)
			{
				RoomMaterialController.Instance.MaterialPacks.Add(roomMaterialPack);
				return roomMaterialPack;
			}
			return dummyWorkshopItem;
		}
		else
		{
			if (!"Building".Equals(type))
			{
				return null;
			}
			SaveGame saveGame = SaveGameManager.WorkshoppableGames.FirstOrDefault((SaveGame x) => SteamWorkshop.NormalizePath(x.Root).Equals(p));
			if (saveGame != null)
			{
				return saveGame;
			}
			if (Directory.Exists(path))
			{
				SaveGame saveGame2 = SaveGameManager.LoadGameMeta(path, true, false);
				if (saveGame2 != null)
				{
					SaveGameManager.AddSave(saveGame2);
				}
				return saveGame2;
			}
			return null;
		}
	}

	public static string NormalizePath(string path)
	{
		return Path.GetFullPath(path).TrimEnd(new char[]
		{
			Path.DirectorySeparatorChar,
			Path.AltDirectorySeparatorChar
		}).ToUpperInvariant();
	}

	public void UploadMod(IWorkshopItem mod)
	{
		if (mod.CanUpload && this.UploadingItem == null)
		{
			this.UploadingItem = mod;
			if (this.UploadingItem.SteamID != null)
			{
				WindowManager.SpawnInputDialog("SteamWorkshopNotePrompt".Loc(), "Change notes".Loc(), string.Empty, delegate(string x)
				{
					SteamWorkshop.PrepareMod(this.UploadingItem);
					SteamWorkshop.ChangeNotes = x;
					this.WaitPanel(true);
					this.UploadContent();
				}, delegate
				{
					this.UploadingItem = null;
				});
			}
			else
			{
				DialogWindow diag = WindowManager.SpawnDialog();
				diag.Show("SteamUploadConfirmation".Loc(), false, DialogWindow.DialogType.Question, new KeyValuePair<string, Action>[]
				{
					new KeyValuePair<string, Action>("Yes", delegate
					{
						this.WaitPanel(true);
						SteamAPICall_t hAPICall = SteamUGC.CreateItem((AppId_t)362620u, EWorkshopFileType.k_EWorkshopFileTypeFirst);
						this._createItemResult.Set(hAPICall, null);
						diag.Window.Close();
					}),
					new KeyValuePair<string, Action>("No", delegate
					{
						this.UploadingItem = null;
						diag.Window.Close();
					})
				});
			}
		}
	}

	private void UploadContent()
	{
		if (this.UploadingItem.CanUpload && this.UploadingItem != null && this.UploadingItem.SteamID != null)
		{
			Debug.Log("Starting Steam workshop item upload for item: " + this.UploadingItem.SteamID.Value);
			UGCUpdateHandle_t ugcupdateHandle_t = SteamUGC.StartItemUpdate((AppId_t)362620u, this.UploadingItem.SteamID.Value);
			this.CurrentUpdate = new UGCUpdateHandle_t?(ugcupdateHandle_t);
			if (this.UploadingItem.SetTitle)
			{
				Debug.Log("Setting title: " + this.UploadingItem.ItemTitle);
				SteamUGC.SetItemTitle(ugcupdateHandle_t, this.UploadingItem.ItemTitle);
			}
			string fullPath = Path.GetFullPath(this.UploadingItem.FolderPath());
			Debug.Log("Setting content folder: " + fullPath);
			SteamUGC.SetItemContent(ugcupdateHandle_t, fullPath);
			List<string> list = new List<string>();
			list.Add(this.UploadingItem.GetWorkshopType());
			list.AddRange(this.UploadingItem.ExtraTags());
			Debug.Log("Setting tags: " + string.Join(", ", list.ToArray()));
			SteamUGC.SetItemTags(ugcupdateHandle_t, list);
			string thumbnail = this.UploadingItem.GetThumbnail();
			if (thumbnail != null)
			{
				Debug.Log("Setting thumbnail " + thumbnail);
				SteamUGC.SetItemPreview(ugcupdateHandle_t, thumbnail);
			}
			Debug.Log("Submitting to Steam and registering callback");
			SteamAPICall_t steamAPICall_t = SteamUGC.SubmitItemUpdate(ugcupdateHandle_t, SteamWorkshop.ChangeNotes);
			if (steamAPICall_t == SteamAPICall_t.Invalid)
			{
				Debug.Log("Workshop update handle invalid");
				WindowManager.SpawnDialog("FailedSteamUpload".Loc(new object[]
				{
					"Update handle invalid"
				}), true, DialogWindow.DialogType.Error);
				this.WaitPanel(false);
				this.CurrentUpdate = null;
				this.UploadingItem = null;
			}
			else
			{
				this._submitItemResult.Set(steamAPICall_t, null);
			}
		}
		else
		{
			Debug.Log("Not authorized to upload this Workshop item");
			WindowManager.SpawnDialog("FailedSteamUpload".Loc(new object[]
			{
				"Not authorized to upload this item"
			}), true, DialogWindow.DialogType.Error);
			this.WaitPanel(false);
			this.CurrentUpdate = null;
			this.UploadingItem = null;
		}
	}

	private void WaitPanel(bool show)
	{
		if (MainMenuController.Instance != null)
		{
			MainMenuController.Instance.WaitPanel.SetActive(show);
		}
	}

	private void SubmitItemResult(SubmitItemUpdateResult_t result, bool failure)
	{
		if (this.UploadingItem == null)
		{
			Debug.Log("Steam work item upload callback received for null");
			WindowManager.SpawnDialog("FailedSteamUpload".Loc(new object[]
			{
				result.m_eResult.ToString()
			}), true, DialogWindow.DialogType.Error);
			return;
		}
		Debug.Log("Steam work item upload callback received for " + this.UploadingItem.SteamID.Value);
		this.CurrentUpdate = null;
		this.WaitPanel(false);
		if (failure || result.m_eResult != EResult.k_EResultOK)
		{
			WindowManager.SpawnDialog("FailedSteamUpload".Loc(new object[]
			{
				result.m_eResult.ToString()
			}), true, DialogWindow.DialogType.Error);
		}
		else
		{
			SteamWorkshop.WriteModID(this.UploadingItem);
			SteamWorkshop.WorkshopItems[this.UploadingItem.SteamID.Value] = this.UploadingItem;
			WindowManager.SpawnDialog("SuccessSteamUpload".Loc(), true, DialogWindow.DialogType.Information);
			SteamFriends.ActivateGameOverlayToWebPage("steam://url/CommunityFilePage/" + this.UploadingItem.SteamID.Value.m_PublishedFileId.ToString());
		}
		this.UploadingItem = null;
	}

	private void CreateItemResult(CreateItemResult_t result, bool failure)
	{
		if (!failure && result.m_eResult == EResult.k_EResultOK)
		{
			if (result.m_bUserNeedsToAcceptWorkshopLegalAgreement)
			{
				SteamFriends.ActivateGameOverlayToWebPage("steam://url/CommunityFilePage/" + result.m_nPublishedFileId.m_PublishedFileId.ToString());
			}
			this.UploadingItem.SteamID = new PublishedFileId_t?(result.m_nPublishedFileId);
			SteamWorkshop.PrepareMod(this.UploadingItem);
			SteamWorkshop.WriteModID(this.UploadingItem);
			SteamWorkshop.WorkshopItems[result.m_nPublishedFileId] = this.UploadingItem;
			this.UploadContent();
		}
		else
		{
			this.WaitPanel(false);
			WindowManager.SpawnDialog("FailedSteamUpload".Loc(new object[]
			{
				result.m_eResult.ToString()
			}), true, DialogWindow.DialogType.Error);
		}
	}

	private void OnUGCquery(SteamUGCQueryCompleted_t result, bool failure)
	{
		if (!failure && result.m_eResult == EResult.k_EResultOK)
		{
			for (uint num = 0u; num < result.m_unNumResultsReturned; num += 1u)
			{
				SteamUGCDetails_t result2;
				SteamUGC.GetQueryUGCResult(result.m_handle, num, out result2);
				this.OnGameUGCRequest(result2);
			}
			this.RefreshWorkshopCache();
		}
		else
		{
			string text = "Failed running Steam query with error code " + result.m_eResult.ToString();
			Debug.Log(text);
			DevConsole.Console.LogError(text);
		}
		if (SteamWorkshop.QueryQueue.Count > 0)
		{
			this._UGCQuery = CallResult<SteamUGCQueryCompleted_t>.Create(new CallResult<SteamUGCQueryCompleted_t>.APIDispatchDelegate(this.OnUGCquery));
			List<PublishedFileId_t> list = SteamWorkshop.QueryQueue.Dequeue();
			UGCQueryHandle_t handle = SteamUGC.CreateQueryUGCDetailsRequest(list.ToArray(), (uint)list.Count);
			SteamAPICall_t hAPICall = SteamUGC.SendQueryUGCRequest(handle);
			this._UGCQuery.Set(hAPICall, null);
		}
		else
		{
			SteamWorkshop.RunningQuery = false;
		}
	}

	private void OnGameUGCRequest(SteamUGCDetails_t result)
	{
		IWorkshopItem workshopItem;
		if (SteamWorkshop.WorkshopItems.TryGetValue(result.m_nPublishedFileId, out workshopItem))
		{
			if (result.m_eResult == EResult.k_EResultFileNotFound)
			{
				workshopItem.CanUpload = true;
				workshopItem.SetTitle = true;
				workshopItem.SteamID = null;
				SteamWorkshop.WriteModID(workshopItem);
				SteamWorkshop.WorkshopItems.Remove(result.m_nPublishedFileId);
			}
			else if (workshopItem is DummyWorkshopItem)
			{
				workshopItem.ItemTitle = result.m_rgchTitle;
				GameData.FailedModList.Add(workshopItem.ItemTitle);
			}
			else
			{
				workshopItem.ItemTitle = result.m_rgchTitle;
				if (SteamWorkshop.UserID == result.m_ulSteamIDOwner)
				{
					workshopItem.CanUpload = true;
				}
				if (ModWindow.Instance != null)
				{
					ModWindow.Instance.Refresh();
				}
				if (LanguageCombo.Instance != null)
				{
					LanguageCombo.Instance.Refresh();
				}
			}
		}
	}

	public static ulong UserID;

	protected CallResult<SteamUGCQueryCompleted_t> _UGCQuery;

	protected CallResult<CreateItemResult_t> _createItemResult;

	protected CallResult<SubmitItemUpdateResult_t> _submitItemResult;

	public static SteamWorkshop Instance;

	public static Dictionary<PublishedFileId_t, IWorkshopItem> WorkshopItems = new Dictionary<PublishedFileId_t, IWorkshopItem>();

	public static bool RunningQuery = false;

	public static bool DoneLoading = false;

	public static Queue<List<PublishedFileId_t>> QueryQueue = new Queue<List<PublishedFileId_t>>();

	public static string ChangeNotes = string.Empty;

	private Dictionary<string, string> PreviousItems = new Dictionary<string, string>();

	private HashSet<string> FailedLoads = new HashSet<string>();

	private UGCUpdateHandle_t? CurrentUpdate;

	private IWorkshopItem UploadingItem;
}
