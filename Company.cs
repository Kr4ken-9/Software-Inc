﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Company
{
	public Company(string name, int startingMoney, SDateTime time, bool eventComp = false)
	{
		this.Name = name;
		this._money = (float)startingMoney;
		this.Founded = time;
		foreach (string key in Enum.GetNames(typeof(Company.TransactionCategory)))
		{
			this.Cashflow.Add(key, new List<float>());
		}
		this.Cashflow.Remove("NA");
		this.Cashflow.Add("Balance", new List<float>());
		this.ID = ((!eventComp) ? GameSettings.Instance.simulation.GetCompanyID() : 0u);
		this.Stocks = ((!eventComp) ? new Stock[GameSettings.Instance.StockCount] : new Stock[1]);
		this.Stocks[0] = new Stock(this.ID, this.ID, this._money, 1f);
		this.OwnedStock.Add(this.Stocks[0]);
	}

	public Company()
	{
	}

	public Company OwnerCompany
	{
		get
		{
			return (this._ownerCompany != 0u) ? GameSettings.Instance.simulation.GetCompany(this._ownerCompany) : null;
		}
		set
		{
			if (this._ownerCompany != 0u)
			{
				this.OwnerCompany.Subsidiaries.Remove(this.ID);
			}
			this._ownerCompany = ((value != null) ? value.ID : 0u);
			if (this._ownerCompany != 0u)
			{
				this.OwnerCompany.Subsidiaries.Add(this.ID);
			}
		}
	}

	public float CompanyStockPrice
	{
		get
		{
			return Mathf.Max(Company.StockDefaultPrice, Mathf.Ceil(this.GetMoneyWithInsurance(true) / (float)(GameSettings.Instance.StockCount - 1) / 10000f) * 10000f);
		}
	}

	public float Money
	{
		get
		{
			return this._money;
		}
	}

	public uint Fans
	{
		get
		{
			return this._fans;
		}
	}

	public float BusinessReputation
	{
		get
		{
			return this._businessReputation;
		}
	}

	public void ChangeBusinessRep(float percentStars, string category, float max = 1f)
	{
		if (percentStars == 0f || float.IsNaN(percentStars) || float.IsInfinity(percentStars))
		{
			return;
		}
		percentStars = ((percentStars <= 0f) ? Mathf.Max(-max, percentStars) : Mathf.Min(max, percentStars));
		if (this.Player && category != null)
		{
			Company.RepEffectItem repEffectItem;
			if (this.RepEffects.TryGetValue(category, out repEffectItem))
			{
				repEffectItem.ChangeValue(percentStars);
			}
			else
			{
				this.RepEffects[category] = new Company.RepEffectItem(percentStars);
			}
		}
		this._businessReputation = Mathf.Clamp01(this._businessReputation + percentStars / (float)Company.BusinessRepStars);
	}

	public string GetDistributionDealString()
	{
		return (this.DistributionDeal == null) ? "None".Loc() : ((this.DistributionDeal.Value * 100f).ToString("N2") + "%");
	}

	public float DiscreteRep
	{
		get
		{
			return (!Mathf.Approximately(this._businessReputation, 1f)) ? (Mathf.Floor(this._businessReputation * (float)Company.BusinessRepStars) / (float)Company.BusinessRepStars) : 1f;
		}
	}

	public float GetMoneyWithInsurance(bool withStocks = true)
	{
		float num = this.Money + this.Subsidiaries.SumSafe((uint x) => GameSettings.Instance.simulation.GetCompany(x).GetMoneyWithInsurance(false));
		float num2;
		if (withStocks)
		{
			num2 = (from x in this.OwnedStock
			where x.Target != this.ID
			select x).Sum((Stock x) => x.Percentage * x.TargetCompany.GetMoneyWithInsurance(false));
		}
		else
		{
			num2 = 0f;
		}
		float num3 = num + num2;
		if (this.Player)
		{
			num3 += GameSettings.Instance.Insurance.Money - GameSettings.Instance.Loans.SumSafe((KeyValuePair<int, float> x) => x.Value * (float)x.Key);
			if (!GameSettings.Instance.RentMode)
			{
				num3 += GameSettings.Instance.PlayerPlots.SumSafe((PlotArea x) => x.Price - x.Monthly * (float)x.MonthsLeft);
			}
		}
		return Mathf.Max(0f, num3);
	}

	public Dictionary<string, Dictionary<string, float>> GetSoftwareRep()
	{
		return this._softwareRep;
	}

	public bool IsPlayerOwned()
	{
		return this._ownerCompany == GameSettings.Instance.MyCompany.ID;
	}

	public bool IsSubsidiary()
	{
		return this._ownerCompany > 0u;
	}

	public void MakeSubsidiary(Company owner)
	{
		this.OwnerCompany = owner;
		int num = 0;
		foreach (KeyValuePair<string, string> key in this.Patents.ToList<KeyValuePair<string, string>>())
		{
			key.GetFeature().TransferPatent(owner);
			num++;
		}
		int stocks = this.TransferStock(owner, false);
		this.GenerateTakeOverMessage(stocks, 0, num);
		if (owner.Player)
		{
			TutorialSystem.Instance.StartTutorial("Subsidiary", false);
			HUD.Instance.dealWindow.CancelCompanyDeals(this);
			this.DistributionDeal = null;
			HUD.Instance.distributionWindow.UpdateDistributionDeals();
		}
	}

	public float GetSumStock()
	{
		float num = 0f;
		for (int i = 0; i < this.Stocks.Length; i++)
		{
			if (this.Stocks[i] != null)
			{
				num += this.Stocks[i].Percentage;
			}
		}
		return num;
	}

	public int GetFreeStock()
	{
		for (int i = 1; i < this.Stocks.Length; i++)
		{
			if (this.Stocks[i] == null)
			{
				return i;
			}
		}
		return -1;
	}

	public float GetShare()
	{
		return (from x in this.Stocks
		where x != null && x.Owner == this.ID
		select x).Sum((Stock x) => x.RealPercentage);
	}

	public void InfiniteMoney()
	{
		this._money = float.PositiveInfinity;
	}

	public float GetReputation(string software, string category)
	{
		Dictionary<string, float> dictionary;
		float result;
		if (this._softwareRep.TryGetValue(software, out dictionary) && dictionary.TryGetValue(category, out result))
		{
			return result;
		}
		return 0f;
	}

	public void AddFans(int amount, string software, string category)
	{
		if (amount == 0)
		{
			return;
		}
		if (amount < 0)
		{
			int num = amount;
			if (software != null)
			{
				num = Mathf.RoundToInt((float)amount * Mathf.Max(0.01f, this.GetReputation(software, category)));
			}
			if (num < 0)
			{
				if ((long)(-(long)num) > (long)((ulong)this._fans))
				{
					this._fans = 0u;
				}
				else
				{
					this._fans -= (uint)(-(uint)num);
				}
			}
		}
		else
		{
			checked
			{
				try
				{
					this._fans += (uint)amount;
				}
				catch (OverflowException)
				{
					this._fans = MarketSimulation.Population;
				}
				if (this._fans > MarketSimulation.Population)
				{
					this._fans = MarketSimulation.Population;
				}
			}
		}
		if (software == null)
		{
			return;
		}
		float popularity = GameSettings.Instance.SoftwareTypes[software].Categories[category].Popularity;
		float num2 = (float)amount / Company.FanThresh;
		Dictionary<string, float> dictionary;
		if (!this._softwareRep.TryGetValue(software, out dictionary))
		{
			dictionary = new Dictionary<string, float>();
			this._softwareRep[software] = dictionary;
		}
		float num3 = (from x in dictionary
		where !x.Key.Equals(category)
		select x.Value).MaxOrDefault(0f) * 0.1f;
		num3 += 1f + this._fans / MarketSimulation.Population / 2f;
		if (GameSettings.Instance.MyCompany == this)
		{
			num3 += GameSettings.Instance.Difficulty.MapRange(0f, 2f, 0.25f, 0f, false);
		}
		num2 = 1f + num2 * num3 * (1f / popularity);
		float num4;
		if (dictionary.TryGetValue(category, out num4) && num4 > 0.001f)
		{
			dictionary[category] = Mathf.Clamp01(num4 * num2);
		}
		else
		{
			dictionary[category] = Mathf.Clamp01(num2 * (0.001f / popularity));
		}
	}

	public void TradeStock(Company company, Stock stock, float offer)
	{
		Company ownerCompany = stock.OwnerCompany;
		if (company == ownerCompany)
		{
			return;
		}
		if (company.Player || ownerCompany.Player)
		{
			GameSettings.Instance.ResetUndo();
		}
		float currentWorth = stock.CurrentWorth;
		ownerCompany.MakeTransaction(offer, Company.TransactionCategory.Stocks, null);
		company.MakeTransaction(-offer, Company.TransactionCategory.Stocks, null);
		float moneyWithInsurance = stock.TargetCompany.GetMoneyWithInsurance(true);
		stock.Percentage = ((moneyWithInsurance != 0f) ? (currentWorth / moneyWithInsurance) : 0.01f);
		stock.InitialPrice = currentWorth;
		ownerCompany.OwnedStock.Remove(stock);
		stock.Owner = company.ID;
		company.OwnedStock.Add(stock);
	}

	public void BuyOut(Company company, bool broke)
	{
		if (this.IsSubsidiary())
		{
			SHashSet<WorkItem> workItems = this.OwnerCompany.WorkItems;
			foreach (WorkItem workItem in workItems)
			{
				if (workItem.CompanyWorker == this)
				{
					workItem.CompanyWorker = null;
				}
			}
			this.OwnerCompany.Subsidiaries.Remove(this.ID);
		}
		if (company == null)
		{
			this.Products.ForEach(delegate(SoftwareProduct x)
			{
				x.CancelHostingServices();
			});
		}
		if (this.DistributionDeal != null)
		{
			this.DistributionDeal = null;
			HUD.Instance.distributionWindow.UpdateDistributionDeals();
		}
		HUD.Instance.dealWindow.CancelCompanyDeals(this);
		if (!broke && this.Stocks[0].CurrentWorth > 0f)
		{
			company.MakeTransaction(-this.Stocks[0].CurrentWorth, Company.TransactionCategory.Stocks, null);
		}
		int num = 0;
		foreach (KeyValuePair<string, string> key in this.Patents.ToList<KeyValuePair<string, string>>())
		{
			key.GetFeature().TransferPatent(company);
			num++;
		}
		int products = 0;
		if (company != null)
		{
			if (!this.IsPlayerOwned())
			{
				Newspaper.GenerateStockBuyout(this, new Company[]
				{
					company
				}, this.Money);
			}
			if (company.Player)
			{
				products = this.Products.Count;
			}
			this.Products.ToList<SoftwareProduct>().ForEach(delegate(SoftwareProduct x)
			{
				x.Trade(company);
			});
			company.CompaniesBought++;
		}
		foreach (Company company2 in (from x in this.Stocks
		where x != null && x.Owner != this.ID
		select x.OwnerCompany).Distinct<Company>())
		{
			company2.OwnedStock.RemoveAll((Stock x) => x.Target == this.ID);
		}
		int stocks = this.TransferStock(company, true);
		if (company != null && company.Player)
		{
			this.GenerateTakeOverMessage(stocks, products, num);
		}
		GameSettings.Instance.simulation.RemoveCompany(this);
		this.Bankrupt = true;
	}

	private void GenerateTakeOverMessage(int stocks, int products, int patents)
	{
		if (stocks > 0 || products > 0 || patents > 0)
		{
			List<string> list = new List<string>();
			if (stocks > 0)
			{
				list.Add("BoughtStockInfo".Loc(new object[]
				{
					stocks
				}));
			}
			if (products > 0)
			{
				list.Add("BoughtProductInfo".Loc(new object[]
				{
					products
				}));
			}
			if (patents > 0)
			{
				list.Add("BoughtOutPatentInfo".Loc(new object[]
				{
					patents
				}));
			}
			HUD.Instance.AddPopupMessage("BoughtOutInfo".Loc(new object[]
			{
				Newspaper.MakeList(list),
				this.Name
			}), "Info", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Good, 0.25f, PopupManager.PopupIDs.None, 1);
		}
	}

	private int TransferStock(Company company, bool buyOut)
	{
		int num = 0;
		if (company != null && !company.Bankrupt && GameSettings.Instance.simulation.GetCompany(company.ID) != null)
		{
			foreach (Stock stock in this.OwnedStock.ToList<Stock>())
			{
				if (stock.Target != this.ID)
				{
					if (company.Player)
					{
						num++;
					}
					this.TradeStock(company, stock, 0f);
				}
			}
		}
		else
		{
			foreach (Company company2 in (from x in this.OwnedStock
			select x.TargetCompany).Distinct<Company>())
			{
				if (buyOut || company2 != this)
				{
					for (int i = 0; i < company2.Stocks.Length; i++)
					{
						if (company2.Stocks[i] != null && company2.Stocks[i].Owner == this.ID)
						{
							company2.Stocks[i] = null;
						}
					}
				}
			}
		}
		this.OwnedStock.Clear();
		for (int j = (!buyOut) ? 1 : 0; j < this.Stocks.Length; j++)
		{
			if (this.Stocks[j] != null)
			{
				this.Stocks[j].OwnerCompany.OwnedStock.Remove(this.Stocks[j]);
				this.Stocks[j] = null;
			}
		}
		return num;
	}

	public void BuyStock(Company company, int i, float offer)
	{
		if (i == 0)
		{
			this.BuyOut(company, false);
		}
		else if (this.Stocks[i] == null)
		{
			float companyStockPrice = this.CompanyStockPrice;
			float num = (company == this) ? (this.GetMoneyWithInsurance(true) - companyStockPrice) : (this.GetMoneyWithInsurance(true) + companyStockPrice);
			this.Stocks[i] = new Stock(company.ID, this.ID, companyStockPrice, (num != 0f) ? (companyStockPrice / num) : 0.01f);
			company.OwnedStock.Add(this.Stocks[i]);
			if (company != this)
			{
				this.MakeTransaction(offer, Company.TransactionCategory.Stocks, null);
			}
			company.MakeTransaction(-offer, Company.TransactionCategory.Stocks, null);
		}
	}

	public bool OwnsLicense(SoftwareProduct product)
	{
		Company ownerCompany = this.OwnerCompany;
		return this.Licenses.Contains(product.ID) || this.Products.Contains(product) || (ownerCompany != null && product.DevCompany == ownerCompany) || this.Subsidiaries.Contains(product.DevCompany.ID);
	}

	public void AddLicense(SoftwareProduct product)
	{
		if (!this.OwnsLicense(product))
		{
			this.Licenses.Add(product.ID);
		}
	}

	public bool CanMakeTransaction(float amount)
	{
		return !this.Bankrupt && !float.IsNaN(amount) && !float.IsInfinity(amount) && (amount >= 0f || this._money + amount >= 0f);
	}

	public void AddToCashflow(float amount, Company.TransactionCategory category)
	{
		if (category == Company.TransactionCategory.NA)
		{
			return;
		}
		string text = EnumStringer<Company.TransactionCategory>.ToString(category);
		if (!this.tempCashflow.ContainsKey(text))
		{
			this.tempCashflow[text] = 0f;
		}
		Dictionary<string, float> dictionary;
		string key;
		(dictionary = this.tempCashflow)[key = text] = dictionary[key] + amount;
	}

	public void EndDay()
	{
		foreach (string text in this.Cashflow.Keys)
		{
			if (text.Equals("Balance"))
			{
				if (this.Player)
				{
					this.Cashflow[text].Add(this.Money + GameSettings.Instance.Insurance.Money);
				}
				else
				{
					this.Cashflow[text].Add(this.Money);
				}
			}
			else
			{
				this.Cashflow[text].Add(this.tempCashflow.GetOrDefault(text, 0f));
			}
		}
		this.tempCashflow.Clear();
	}

	public void MakeTransaction(float amount, Company.TransactionCategory category, string bill = null)
	{
		if (amount == 0f)
		{
			return;
		}
		if (float.IsNaN(amount))
		{
			Debug.LogException(new UnityException("Tried to deduct NaN from company " + this.Name + " in category " + EnumStringer<Company.TransactionCategory>.ToString(category)));
			return;
		}
		if (float.IsInfinity(amount))
		{
			Debug.LogException(new UnityException("Tried to deduct infinity from company " + this.Name + " in category " + EnumStringer<Company.TransactionCategory>.ToString(category)));
			return;
		}
		if (!this.Player && !this.Bankrupt && !this.CanMakeTransaction(amount))
		{
			this.Bankrupt = true;
			if (this.IsPlayerOwned())
			{
				HUD.Instance.AddPopupMessage("SubsidiaryBankruptWarning".Loc(new object[]
				{
					this.Name
				}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.None, 1);
				this.BuyOut(this.OwnerCompany, true);
			}
			else
			{
				Stock stock = (from x in this.Stocks
				where x != null && x.Owner != this.ID && GameSettings.Instance.simulation.GetCompany(x.Owner) != null
				select x).MaxInstance((Stock x) => x.Percentage);
				this.BuyOut((stock != null) ? stock.OwnerCompany : null, true);
			}
		}
		this._money += amount;
		this.AddToCashflow(amount, category);
		if (this.Player)
		{
			if (HUD.Instance != null)
			{
				HUD.Instance.financeWindow.UpdateSheet(true);
			}
			if (bill != null)
			{
				Dictionary<string, float> dictionary;
				if (!GameSettings.Instance.BillsNext.TryGetValue(category, out dictionary))
				{
					dictionary = new Dictionary<string, float>();
					GameSettings.Instance.BillsNext[category] = dictionary;
				}
				float num = 0f;
				if (dictionary.TryGetValue(bill, out num))
				{
					float num2 = num + amount;
					if (num2 == 0f)
					{
						dictionary.Remove(bill);
					}
					else
					{
						dictionary[bill] = num2;
					}
				}
				else
				{
					dictionary[bill] = amount;
				}
			}
		}
	}

	public bool CanBuyOut(Company cmp)
	{
		if (this == cmp)
		{
			return false;
		}
		for (int i = 1; i < this.Stocks.Length; i++)
		{
			if (this.Stocks[i] == null || this.Stocks[i].Owner != cmp.ID)
			{
				return false;
			}
		}
		return true;
	}

	public override string ToString()
	{
		return this.Name;
	}

	public void SimulateFanLoss(SDateTime time)
	{
		float num = this._softwareRep.SumSafe((KeyValuePair<string, Dictionary<string, float>> x) => x.Value.SumSafe((KeyValuePair<string, float> y) => y.Value));
		if (num == 0f)
		{
			return;
		}
		foreach (KeyValuePair<string, Dictionary<string, float>> keyValuePair in this._softwareRep)
		{
			foreach (KeyValuePair<string, float> keyValuePair2 in keyValuePair.Value.ToArray<KeyValuePair<string, float>>())
			{
				float retention = GameSettings.Instance.SoftwareTypes[keyValuePair.Key].Categories[keyValuePair2.Key].Retention;
				bool flag = false;
				SDateTime release = new SDateTime(0, 0, 0);
				for (int j = 0; j < this.Products.Count; j++)
				{
					SoftwareProduct softwareProduct = this.Products[j];
					if (softwareProduct._type.Equals(keyValuePair.Key) && softwareProduct._category.Equals(keyValuePair2.Key) && Utilities.GetMonths(softwareProduct.Release, time) <= 240f * retention)
					{
						flag = true;
						if (softwareProduct.Release > release)
						{
							release = softwareProduct.Release;
						}
					}
				}
				if (flag && Utilities.GetMonths(release, time) > 120f * retention)
				{
					float num2 = 0.1f * (keyValuePair2.Value / num);
					if (num2 > 0.01f)
					{
						HUD.Instance.AddPopupMessage(string.Format("SoftwareSupportWaning".Loc(), keyValuePair.Key.LocSW(), keyValuePair2.Key.LocSWC(keyValuePair.Key)), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Issue, 0.75f, PopupManager.PopupIDs.FanWarning, 1);
					}
					num2 /= (float)GameSettings.DaysPerMonth;
					this.AddFans(-Mathf.FloorToInt(Company.FanThresh * num2), keyValuePair.Key, keyValuePair2.Key);
				}
			}
		}
	}

	public void PayDividends()
	{
		float num = (from x in this.tempCashflow
		where !x.Key.Equals("Balance") && !x.Key.Equals("Dividends")
		select x).SumSafe((KeyValuePair<string, float> x) => x.Value);
		if (num > 0f)
		{
			float sumStock = this.GetSumStock();
			for (int i = 1; i < this.Stocks.Length; i++)
			{
				Stock stock = this.Stocks[i];
				if (stock != null && stock.Owner != this.ID)
				{
					float num2 = num * (stock.Percentage / sumStock) / 24f;
					stock.OwnerCompany.MakeTransaction(num2, Company.TransactionCategory.Dividends, null);
					this.MakeTransaction(-num2, Company.TransactionCategory.Dividends, null);
				}
			}
		}
	}

	public void CancelAllWorkFor(SoftwareProduct product)
	{
		foreach (WorkItem workItem in this.WorkItems.ToList<WorkItem>())
		{
			MarketingPlan marketingPlan = workItem as MarketingPlan;
			if (marketingPlan != null && marketingPlan.Type == MarketingPlan.TaskType.PostMarket && marketingPlan.TargetProd == product)
			{
				marketingPlan.Kill(false);
			}
			else
			{
				SupportWork supportWork = workItem as SupportWork;
				if (supportWork != null && supportWork.TargetProduct == product)
				{
					supportWork.Kill(false);
				}
				else
				{
					SoftwarePort softwarePort = workItem as SoftwarePort;
					if (softwarePort != null && softwarePort.Product == product)
					{
						softwarePort.Kill(false);
					}
					SoftwareWorkItem softwareWorkItem = workItem as SoftwareWorkItem;
					if (softwareWorkItem != null && softwareWorkItem.SequelTo != null && softwareWorkItem.SequelTo.Value == product.ID)
					{
						softwareWorkItem.Kill(false);
					}
				}
			}
		}
	}

	public static int BusinessRepStars = 6;

	public static float StockDefaultPrice = 50000f;

	public readonly uint ID;

	public readonly string Name;

	public readonly SDateTime Founded;

	private List<uint> Licenses = new List<uint>();

	public float LicenseIncome;

	public int CompaniesBought;

	private float _money;

	public Stock[] Stocks;

	public EventList<Stock> OwnedStock = new EventList<Stock>();

	public List<KeyValuePair<string, string>> Patents = new List<KeyValuePair<string, string>>();

	public float? DistributionDeal;

	public float? LastDistributionDeal;

	public SDateTime LastDistributionOffer = new SDateTime(0, 0);

	public SHashSet<uint> Subsidiaries = new SHashSet<uint>();

	public Dictionary<string, Company.RepEffectItem> RepEffects;

	private uint _ownerCompany;

	protected uint _fans;

	protected Dictionary<string, Dictionary<string, float>> _softwareRep = new Dictionary<string, Dictionary<string, float>>();

	protected float _businessReputation = 1f / (float)Company.BusinessRepStars;

	public SHashSet<WorkItem> WorkItems = new SHashSet<WorkItem>();

	public List<SoftwareProduct> Products = new List<SoftwareProduct>();

	public bool Bankrupt;

	public bool Player;

	public Dictionary<string, List<float>> Cashflow = new Dictionary<string, List<float>>();

	public Dictionary<string, float> tempCashflow = new Dictionary<string, float>();

	public static float FanThresh = 30000f;

	[Serializable]
	public class RepEffectItem
	{
		public RepEffectItem()
		{
		}

		public RepEffectItem(float value)
		{
			if (value > 0f)
			{
				this.Positive = value;
			}
			else
			{
				this.Negative = -value;
			}
			this.NegativeLast = (this.PositiveLast = SDateTime.Now());
		}

		public float GetValue(bool positive)
		{
			if (positive)
			{
				float num = 1f - Mathf.Clamp01(Utilities.GetHours(this.PositiveLast, SDateTime.Now()) / 24f);
				return this.Positive * num;
			}
			float num2 = 1f - Mathf.Clamp01(Utilities.GetHours(this.NegativeLast, SDateTime.Now()) / 24f);
			return this.Negative * num2;
		}

		public bool IsRelevant()
		{
			if (this.Positive <= 0f && this.Negative <= 0f)
			{
				return false;
			}
			float hours = Utilities.GetHours(this.PositiveLast, SDateTime.Now());
			float hours2 = Utilities.GetHours(this.NegativeLast, SDateTime.Now());
			return hours <= 24f || hours2 <= 24f;
		}

		public void ChangeValue(float value)
		{
			if (value > 0f)
			{
				this.ChangePositive(value);
			}
			else
			{
				this.ChangeNegative(-value);
			}
		}

		public void ChangePositive(float newValue)
		{
			SDateTime sdateTime = SDateTime.Now();
			float num = 1f - Mathf.Clamp01(Utilities.GetHours(this.PositiveLast, sdateTime) / 24f);
			this.Positive = this.Positive * num + newValue;
			this.PositiveLast = sdateTime;
		}

		public void ChangeNegative(float newValue)
		{
			SDateTime sdateTime = SDateTime.Now();
			float num = 1f - Mathf.Clamp01(Utilities.GetHours(this.NegativeLast, sdateTime) / 24f);
			this.Negative = this.Negative * num + newValue;
			this.NegativeLast = sdateTime;
		}

		public float Positive;

		public float Negative;

		public SDateTime PositiveLast;

		public SDateTime NegativeLast;
	}

	public enum TransactionCategory
	{
		Salaries,
		Contracts,
		Sales,
		Bills,
		Hire,
		Construction,
		Repairs,
		Marketing,
		Licenses,
		Staff,
		Stocks,
		Education,
		Loan,
		Interest,
		Distribution,
		Deals,
		Royalties,
		Dividends,
		NA,
		Benefits,
		Intercompany
	}
}
