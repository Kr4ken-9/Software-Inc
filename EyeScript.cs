﻿using System;
using UnityEngine;

public class EyeScript : MonoBehaviour
{
	private void SetEyesScale(float s, int[] indices)
	{
		if (this.Face != null)
		{
			this.Face.SetInt("_EyeNum", indices[Mathf.RoundToInt(s * (float)(indices.Length - 1))]);
		}
	}

	private void Update()
	{
		if (GameSettings.GameSpeed == 0f)
		{
			return;
		}
		if (this.sleep)
		{
			if (this.Face != null)
			{
				this.Face.SetInt("_EyeNum", this.SleepIndex);
			}
			return;
		}
		if (this.Happy && this.Energy > 0.25f)
		{
			if (this.Face != null)
			{
				this.Face.SetInt("_EyeNum", this.HappyIndex);
			}
			return;
		}
		float s = (this.blink <= 1f) ? this.blink : (2f - this.blink);
		this.SetEyesScale(s, (this.Energy >= 0.25f) ? this.BlinkIndices : this.TiredBlinkIndices);
		this.NextBlink -= Time.deltaTime;
		if (this.NextBlink <= 0f)
		{
			this.blinkNow = true;
			this.NextBlink = UnityEngine.Random.Range(1f, 6f);
		}
		if (this.blinkNow)
		{
			this.blink = Mathf.Min(this.blink + Time.deltaTime * 10f, 2f);
			if (this.blink == 2f)
			{
				this.blink = 0f;
				this.blinkNow = false;
			}
		}
	}

	public float Energy = 1f;

	public int[] BlinkIndices;

	public int[] TiredBlinkIndices;

	public int SleepIndex;

	public int HappyIndex;

	public float blink;

	public bool Happy;

	public bool blinkNow;

	public bool sleep;

	public float NextBlink;

	public float heightMin;

	public float heightMax;

	public float widthMin;

	public float widthMax;

	[NonSerialized]
	public Material Face;
}
