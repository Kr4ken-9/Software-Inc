﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class InteractionPoint : MonoBehaviour
{
	public Actor UsedBy
	{
		get
		{
			return this._usedBy;
		}
		set
		{
			this._usedBy = value;
			if (this.Parent.Type.Equals("Computer") && this.Parent.ComputerChair != null)
			{
				this.Parent.ComputerChair.GetInteractionPoint("Use", true).UsedBy = value;
			}
			if (this.Child != null && this.Child.UsedBy != this.UsedBy)
			{
				this.Child.UsedBy = this.UsedBy;
			}
			if (this.Parent.Table != null)
			{
				this.Parent.Table.UpdateStatus();
			}
			else if (this.Parent.SnappedTo != null && this.Parent.SnappedTo.Parent.Table != null)
			{
				this.Parent.SnappedTo.Parent.Table.UpdateStatus();
			}
		}
	}

	public Vector2 Point
	{
		get
		{
			return (!this.ActiveDefer.IsReferenceNull()) ? this.ActiveDefer.Point : new Vector2(base.transform.position.x, base.transform.position.z);
		}
	}

	public float Rotation
	{
		get
		{
			return (!this.ActiveDefer.IsReferenceNull()) ? this.ActiveDefer.Rotation : base.transform.rotation.eulerAngles.y;
		}
	}

	public void UpdateFreeNav(bool threaded)
	{
		if (this.Parent != null && this.Parent.Parent != null)
		{
			this.FreeNav = (this.Parent.Parent.GetNodeAt(((!threaded) ? base.transform.position : this.pos).FlattenVector3()) != null);
		}
		else
		{
			this.FreeNav = false;
		}
	}

	public void UpdateDefer()
	{
		this.ActiveDefer = null;
		if (!this.FreeNav && !this.DeferChild.IsReferenceNull())
		{
			InteractionPoint deferChild = this.DeferChild;
			while (deferChild != null && deferChild != this && !deferChild.FreeNav)
			{
				deferChild = deferChild.DeferChild;
			}
			this.ActiveDefer = ((!(deferChild != this)) ? null : deferChild);
		}
	}

	public bool Usable()
	{
		return !(this.Parent == null) && !(this.Parent.Parent == null) && (this.Parent.isTemporary || this.FreeNav || !this.ActiveDefer.IsReferenceNull());
	}

	private void OnDrawGizmos()
	{
		if (GameSettings.Instance == null || this.Parent.Parent.Floor == GameSettings.Instance.ActiveFloor)
		{
			Gizmos.color = ((!(this.Parent == null)) ? ((!this.Usable()) ? Color.red : Color.green) : Color.white);
			Gizmos.DrawSphere(base.transform.position, 0.05f);
			Gizmos.color = Color.white;
			if (this.Child != null)
			{
				Gizmos.DrawLine(base.transform.position, this.Child.transform.position);
			}
		}
	}

	public bool CanQueue(Actor act, string action)
	{
		if (this.CurrentQueue.Count >= this.Parent.MaxQueue || act.AItype != AI<Actor>.AIType.Employee)
		{
			return false;
		}
		if (!this.Parent.Parent.CompatibleWithTeam(act.GetTeam()))
		{
			return false;
		}
		if (this.Parent.Parent.ForceRole >= 0 && (Employee.RoleToMask[this.Parent.Parent.ForceRole] & act.GetRole()) == Employee.RoleBit.None)
		{
			return false;
		}
		if (action.Equals("Use") && ((this.Parent.Reserved != null && this.Parent.Reserved != act) || (this.Parent.OwnedBy != null && this.Parent.OwnedBy != act)))
		{
			return false;
		}
		if (this.Parent.HasUpg && this.Parent.upg.Broken)
		{
			return false;
		}
		if (this.Parent.Type.Equals("Tray"))
		{
			if (action.Equals("Serve"))
			{
				if (!this.Parent.CanPlaceHoldable())
				{
					return false;
				}
			}
			else if (this.Parent.HasHoldables == 0)
			{
				return false;
			}
		}
		return true;
	}

	public void RemoveFromQueue(Actor act)
	{
		for (int i = 0; i < this.CurrentQueue.Count; i++)
		{
			if (this.CurrentQueue[i] == act)
			{
				this.CurrentQueue.RemoveAt(i);
				break;
			}
		}
	}

	public bool IsInQueue(Actor act)
	{
		for (int i = 0; i < this.CurrentQueue.Count; i++)
		{
			if (this.CurrentQueue[i] == act)
			{
				return true;
			}
		}
		return false;
	}

	public void ClearQueue()
	{
		this.CurrentQueue.ForEach(delegate(Actor x)
		{
			x.InQueue.Remove(this.Parent.Type);
		});
		this.CurrentQueue.Clear();
	}

	public bool IsUp(Actor act)
	{
		return this.CurrentQueue[0] == act;
	}

	public void AddToQueue(Actor act)
	{
		if (!this.IsInQueue(act))
		{
			this.CurrentQueue.Add(act);
		}
	}

	public int QueueLength
	{
		get
		{
			return this.CurrentQueue.Count;
		}
	}

	public string Name;

	private Actor _usedBy;

	public Furniture Parent;

	public InteractionPoint Child;

	public InteractionPoint DeferChild;

	public InteractionPoint ActiveDefer;

	public int Id;

	public bool NeedsReachCheck = true;

	public Actor.AnimationStates Animation;

	public int subAnimation;

	private bool FreeNav;

	public int MinimumNeeded = 1;

	[NonSerialized]
	public List<Actor> CurrentQueue = new List<Actor>();

	[NonSerialized]
	public Vector3 pos;
}
