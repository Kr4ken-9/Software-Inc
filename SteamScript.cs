﻿using System;
using Steamworks;
using UnityEngine;

public class SteamScript : MonoBehaviour
{
	private void Start()
	{
		if (SteamManager.Initialized)
		{
			this.m_GameOverlayActivated = Callback<GameOverlayActivated_t>.Create(new Callback<GameOverlayActivated_t>.DispatchDelegate(this.OnGameOverlayActivated));
		}
	}

	private void OnGameOverlayActivated(GameOverlayActivated_t pCallback)
	{
		if (pCallback.m_bActive != 0)
		{
			GameSettings.ForcePause = true;
			GameSettings.FreezeGame = true;
		}
		else
		{
			GameSettings.ForcePause = false;
		}
	}

	private void OnDestroy()
	{
		if (this.m_GameOverlayActivated != null)
		{
			this.m_GameOverlayActivated.Unregister();
		}
	}

	private void Update()
	{
	}

	protected Callback<GameOverlayActivated_t> m_GameOverlayActivated;
}
