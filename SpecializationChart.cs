﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SpecializationChart : MonoBehaviour
{
	private void Start()
	{
		this.ContentRect = this.ContentPanel.GetComponent<RectTransform>();
	}

	public void SetContent(Employee[] emps)
	{
		foreach (KeyValuePair<int, Dictionary<string, GUIProgressBar>> keyValuePair in this.Bars)
		{
			foreach (KeyValuePair<string, GUIProgressBar> keyValuePair2 in keyValuePair.Value)
			{
				UnityEngine.Object.Destroy(keyValuePair2.Value.gameObject);
			}
		}
		this.labels.ForEach(delegate(GameObject x)
		{
			UnityEngine.Object.Destroy(x);
		});
		this.labels.Clear();
		this.Bars.Clear();
		this.Employees = emps;
		if (this.Employees.Length > 0)
		{
			this.Bars[2] = new Dictionary<string, GUIProgressBar>();
			this.Bars[1] = new Dictionary<string, GUIProgressBar>();
			this.Bars[3] = new Dictionary<string, GUIProgressBar>();
			foreach (string text in new string[]
			{
				"Base skill"
			}.Concat(GameSettings.Instance.GetUnlockedSpecializations(Employee.EmployeeRole.Lead)))
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.TextPrefab);
				gameObject.GetComponent<Text>().text = text.LocTry();
				gameObject.transform.SetParent(this.ContentPanel.transform, false);
				this.labels.Add(gameObject);
				int num = 0;
				foreach (KeyValuePair<int, Dictionary<string, GUIProgressBar>> keyValuePair3 in this.Bars)
				{
					Employee.EmployeeRole key = (Employee.EmployeeRole)keyValuePair3.Key;
					GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.ProgressBarPrefab);
					gameObject2.transform.SetParent(this.ContentPanel.transform, false);
					GUIProgressBar component = gameObject2.GetComponent<GUIProgressBar>();
					component.EndExt = (component.EndColor = (component.StartColor = SpecializationChart.SkillColors[num]));
					keyValuePair3.Value[text] = component;
					if (!"Base skill".Equals(text) && !GameSettings.Instance.GetUnlockedSpecializations(key).Contains(text))
					{
						component.color = new Color(1f, 1f, 1f, 0.1f);
						component.Value = 0f;
					}
					num++;
				}
			}
		}
		this.Update();
	}

	private void Update()
	{
		if (this.AutoSize)
		{
			if (this.ContentRect == null)
			{
				this.ContentRect = this.ContentPanel.GetComponent<RectTransform>();
			}
			this.GridPanel.cellSize = new Vector2(this.ContentRect.rect.width / 4f - 2f, 24f);
		}
		bool flag = this.CompareTeam != null && this.CompareTeam.GetEmployeesDirect().Count > 0;
		int num = 0;
		foreach (KeyValuePair<int, Dictionary<string, GUIProgressBar>> keyValuePair in this.Bars)
		{
			foreach (KeyValuePair<string, GUIProgressBar> keyValuePair2 in keyValuePair.Value)
			{
				Employee.EmployeeRole role = (Employee.EmployeeRole)keyValuePair.Key;
				string spec = keyValuePair2.Key;
				bool flag2 = false;
				float num2 = 0f;
				float value = 0f;
				if (spec == "Base skill")
				{
					flag2 = true;
					if (flag)
					{
						keyValuePair2.Value.AltValue = new float?(this.CompareTeam.GetEmployeesDirect().Average((Actor x) => x.employee.GetSkill(role)));
					}
					else
					{
						keyValuePair2.Value.AltValue = null;
					}
				}
				else if (flag)
				{
					keyValuePair2.Value.AltValue = new float?(this.CompareTeam.GetEmployeesDirect().Average((Actor x) => this.GetSpec(x.employee, role, spec)));
				}
				else
				{
					keyValuePair2.Value.AltValue = null;
				}
				this.GetAverageAndMax(this.Employees, role, spec, flag2, out num2, out value);
				keyValuePair2.Value.Value = value;
				if (!flag2 && role != Employee.EmployeeRole.Designer)
				{
					GUIProgressBar value2 = keyValuePair2.Value;
					value2.EndExt = (value2.EndColor = (value2.StartColor = ((this.MinSkillTeam == null || num2 >= this.MinSkillTeam.SpecializationMinCap) ? SpecializationChart.SkillColors[num] : SpecializationChart.SkillColors[3])));
				}
			}
			num++;
		}
	}

	private void GetAverageAndMax(Employee[] emps, Employee.EmployeeRole role, string spec, bool baseSkill, out float maxVal, out float avgVal)
	{
		float num = 0f;
		float num2 = 0f;
		int num3 = 0;
		foreach (Employee employee in emps)
		{
			float num4 = (!baseSkill) ? this.GetSpec(employee, role, spec) : employee.GetSkill(role);
			num = Mathf.Max(num, num4);
			num2 += num4;
			num3++;
		}
		maxVal = num;
		avgVal = num2 / (float)num3;
	}

	private float GetSpec(Employee emp, Employee.EmployeeRole role, string spec)
	{
		Actor myActor = emp.MyActor;
		if (myActor == null)
		{
			return emp.GetSpecialization(role, spec, false);
		}
		if (myActor.isActiveAndEnabled || myActor.CoursePoints <= 0f || myActor.CourseRole != role || !myActor.CourseSpec.Equals(spec))
		{
			return myActor.employee.GetSpecialization(role, spec, false);
		}
		SDateTime? arriveTime = GameSettings.Instance.sActorManager.GetArriveTime(myActor);
		if (arriveTime != null)
		{
			float num = Utilities.GetMonths(myActor.LastCourse, SDateTime.Now()) / Utilities.GetMonths(myActor.LastCourse, arriveTime.Value);
			return Mathf.Clamp01(myActor.employee.GetSpecialization(role, spec, false) + num * myActor.CoursePoints);
		}
		return Mathf.Clamp01(myActor.employee.GetSpecialization(role, spec, false) + myActor.CoursePoints);
	}

	public static Color[] SkillColors = new Color[]
	{
		new Color32(133, 162, 219, byte.MaxValue),
		new Color32(161, 219, 133, byte.MaxValue),
		new Color32(219, 133, 133, byte.MaxValue),
		new Color32(50, 50, 50, byte.MaxValue)
	};

	public GameObject ContentPanel;

	private RectTransform ContentRect;

	public GridLayoutGroup GridPanel;

	public GameObject TextPrefab;

	public GameObject ProgressBarPrefab;

	private Dictionary<int, Dictionary<string, GUIProgressBar>> Bars = new Dictionary<int, Dictionary<string, GUIProgressBar>>();

	private List<GameObject> labels = new List<GameObject>();

	public bool AutoSize = true;

	[NonSerialized]
	public Employee[] Employees = new Employee[0];

	[NonSerialized]
	public Team CompareTeam;

	[NonSerialized]
	public Team MinSkillTeam;
}
