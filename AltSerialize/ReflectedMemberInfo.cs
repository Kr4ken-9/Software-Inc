﻿using System;
using System.Reflection;

namespace AltSerialize
{
	internal class ReflectedMemberInfo
	{
		public ReflectedMemberInfo(FieldInfo field)
		{
			if (field == null)
			{
				throw new AltSerializeException("Could not create meta data information for serialization.");
			}
			if (field.FieldType == null)
			{
				throw new AltSerializeException("The field '" + field.Name + "' has no Type information.");
			}
			this._isFieldInfo = true;
			this._fieldInfo = field;
		}

		public ReflectedMemberInfo(PropertyInfo propinfo)
		{
			if (propinfo == null)
			{
				throw new AltSerializeException("Could not create meta data information for serialization.");
			}
			if (propinfo.PropertyType == null)
			{
				throw new AltSerializeException("The property '" + propinfo.Name + "' has no Type information.");
			}
			this._propertyInfo = propinfo;
		}

		public string Name
		{
			get
			{
				if (this._isFieldInfo)
				{
					return this._fieldInfo.Name;
				}
				return this._propertyInfo.Name;
			}
		}

		public Type FieldType
		{
			get
			{
				return this.GetFieldOrPropertyType();
			}
		}

		public object GetValue(object obj)
		{
			if (this._isFieldInfo)
			{
				return this._fieldInfo.GetValue(obj);
			}
			return this._propertyInfo.GetValue(obj, null);
		}

		public void SetValue(object obj, object newValue)
		{
			if (this._isFieldInfo)
			{
				this._fieldInfo.SetValue(obj, newValue);
			}
			else
			{
				this._propertyInfo.SetValue(obj, newValue, null);
			}
		}

		public Type GetFieldOrPropertyType()
		{
			Type result;
			try
			{
				if (this._isFieldInfo)
				{
					result = this._fieldInfo.FieldType;
				}
				else
				{
					result = this._propertyInfo.PropertyType;
				}
			}
			catch (Exception ex)
			{
				if (this._isFieldInfo)
				{
					if (this._fieldInfo == null)
					{
						throw new AltSerializeException("The field information for a reflected member was null.");
					}
					throw new AltSerializeException("Failed to retrieve the field type for the field named '" + this._fieldInfo.Name + "': " + ex.Message);
				}
				else
				{
					if (this._propertyInfo == null)
					{
						throw new AltSerializeException("The property information for a reflected member was null!");
					}
					throw new AltSerializeException("Failed to retrieve the property type for the property named '" + this._propertyInfo.Name + "': " + ex.Message);
				}
			}
			return result;
		}

		private FieldInfo _fieldInfo;

		private PropertyInfo _propertyInfo;

		private bool _isFieldInfo;
	}
}
