﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;

namespace AltSerialize
{
	internal static class DynamicSerializerFactory
	{
		public static MethodInfo[] SerializeMethods
		{
			get
			{
				if (DynamicSerializerFactory._serializeMethods == null)
				{
					DynamicSerializerFactory.Methods();
				}
				return DynamicSerializerFactory._serializeMethods;
			}
		}

		public static MethodInfo[] DeserializeMethods
		{
			get
			{
				if (DynamicSerializerFactory._deserializeMethods == null)
				{
					DynamicSerializerFactory.Methods();
				}
				return DynamicSerializerFactory._deserializeMethods;
			}
		}

		private static MethodInfo GetDeserializeMethod(Type type)
		{
			for (int i = 0; i < DynamicSerializerFactory.streamTypes.Length; i++)
			{
				if (type == DynamicSerializerFactory.streamTypes[i])
				{
					return DynamicSerializerFactory.DeserializeMethods[i];
				}
			}
			return null;
		}

		private static MethodInfo GetSerializerMethod(Type type)
		{
			for (int i = 0; i < DynamicSerializerFactory.streamTypes.Length; i++)
			{
				if (type == DynamicSerializerFactory.streamTypes[i])
				{
					return DynamicSerializerFactory.SerializeMethods[i];
				}
			}
			return null;
		}

		private static void Methods()
		{
			Type typeFromHandle = typeof(AltSerializer);
			DynamicSerializerFactory._serializeMethods = new MethodInfo[DynamicSerializerFactory.streamTypes.Length];
			DynamicSerializerFactory._deserializeMethods = new MethodInfo[DynamicSerializerFactory.streamTypes.Length];
			for (int i = 0; i < DynamicSerializerFactory.streamTypes.Length; i++)
			{
				DynamicSerializerFactory._serializeMethods[i] = typeFromHandle.GetMethod("Write", new Type[]
				{
					DynamicSerializerFactory.streamTypes[i]
				});
				if (DynamicSerializerFactory._serializeMethods[i] == null)
				{
					throw new Exception("No write method for type '" + DynamicSerializerFactory.streamTypes[i].Name + "'.");
				}
				DynamicSerializerFactory._deserializeMethods[i] = typeFromHandle.GetMethod("Read" + DynamicSerializerFactory.streamTypes[i].Name);
				if (DynamicSerializerFactory._deserializeMethods[i] == null)
				{
					throw new Exception("No read method for type '" + DynamicSerializerFactory.streamTypes[i].Name + "'");
				}
			}
		}

		public static DynamicSerializer GenerateSerializer(Type objectType)
		{
			AppDomain domain = Thread.GetDomain();
			AssemblyBuilder assemblyBuilder = domain.DefineDynamicAssembly(new AssemblyName
			{
				Name = "DynamicSerializer",
				Version = new Version(1, 0, 0, 0)
			}, AssemblyBuilderAccess.Run);
			ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("DynamicSerializerModule");
			TypeAttributes attr = TypeAttributes.Public | TypeAttributes.Sealed;
			string name = "ser_" + objectType.Name;
			TypeBuilder typeBuilder = moduleBuilder.DefineType(name, attr, typeof(DynamicSerializer));
			Type[] parameterTypes = new Type[]
			{
				typeof(object),
				typeof(AltSerializer)
			};
			Type[] parameterTypes2 = new Type[]
			{
				typeof(AltSerializer),
				typeof(int)
			};
			MethodBuilder methodBuilder = typeBuilder.DefineMethod("Serialize", MethodAttributes.FamANDAssem | MethodAttributes.Family | MethodAttributes.Virtual, CallingConventions.HasThis, null, parameterTypes);
			DynamicSerializerFactory.GenerateSerializeMethod(methodBuilder.GetILGenerator(), objectType);
			MethodBuilder methodBuilder2 = typeBuilder.DefineMethod("Deserialize", MethodAttributes.FamANDAssem | MethodAttributes.Family | MethodAttributes.Virtual, CallingConventions.HasThis, typeof(object), parameterTypes2);
			DynamicSerializerFactory.GenerateDeserializeMethod(methodBuilder2.GetILGenerator(), objectType);
			Type type = typeBuilder.CreateType();
			return (DynamicSerializer)Activator.CreateInstance(type);
		}

		private static void GenerateSerializeMethod(ILGenerator methodIL, Type objectType)
		{
			methodIL.DeclareLocal(objectType);
			methodIL.Emit(OpCodes.Ldarg_1);
			methodIL.Emit(OpCodes.Castclass, objectType);
			methodIL.Emit(OpCodes.Stloc_0);
			PropertyInfo[] properties = objectType.GetProperties();
			foreach (PropertyInfo propertyInfo in properties)
			{
				if (propertyInfo.GetCustomAttributes(typeof(DoNotSerializeAttribute), true).Length == 0)
				{
					if (propertyInfo.CanRead && propertyInfo.CanWrite)
					{
						MethodInfo method = typeof(Type).GetMethod("GetTypeFromHandle");
						MethodInfo method2 = typeof(AltSerializer).GetMethod("Serialize", new Type[]
						{
							typeof(object),
							typeof(Type)
						});
						MethodInfo getMethod = propertyInfo.GetGetMethod();
						MethodInfo serializerMethod = DynamicSerializerFactory.GetSerializerMethod(propertyInfo.PropertyType);
						if (serializerMethod != null)
						{
							methodIL.Emit(OpCodes.Ldarg_2);
							methodIL.Emit(OpCodes.Ldloc_0);
							methodIL.Emit(OpCodes.Callvirt, getMethod);
							methodIL.Emit(OpCodes.Callvirt, serializerMethod);
						}
						else if (propertyInfo.PropertyType.IsValueType)
						{
							methodIL.Emit(OpCodes.Ldarg_2);
							methodIL.Emit(OpCodes.Ldloc_0);
							methodIL.Emit(OpCodes.Callvirt, getMethod);
							methodIL.Emit(OpCodes.Box, propertyInfo.PropertyType);
							methodIL.Emit(OpCodes.Ldtoken, propertyInfo.PropertyType);
							methodIL.Emit(OpCodes.Call, method);
							methodIL.Emit(OpCodes.Callvirt, method2);
						}
						else
						{
							methodIL.Emit(OpCodes.Ldarg_2);
							methodIL.Emit(OpCodes.Ldloc_0);
							methodIL.Emit(OpCodes.Callvirt, getMethod);
							methodIL.Emit(OpCodes.Ldtoken, propertyInfo.PropertyType);
							methodIL.Emit(OpCodes.Call, method);
							methodIL.Emit(OpCodes.Callvirt, method2);
						}
					}
				}
			}
			methodIL.Emit(OpCodes.Ret);
		}

		private static void GenerateDeserializeMethod(ILGenerator methodIL, Type objectType)
		{
			ConstructorInfo constructor = objectType.GetConstructor(new Type[0]);
			methodIL.DeclareLocal(objectType);
			methodIL.DeclareLocal(typeof(object));
			methodIL.Emit(OpCodes.Nop);
			methodIL.Emit(OpCodes.Newobj, constructor);
			methodIL.Emit(OpCodes.Stloc_0);
			Label label = methodIL.DefineLabel();
			methodIL.Emit(OpCodes.Ldarg_2);
			methodIL.Emit(OpCodes.Ldc_I4, 0);
			methodIL.Emit(OpCodes.Ceq);
			methodIL.Emit(OpCodes.Brtrue_S, label);
			methodIL.Emit(OpCodes.Ldarg_1);
			methodIL.Emit(OpCodes.Ldloc_0);
			methodIL.Emit(OpCodes.Ldarg_2);
			MethodInfo method = typeof(AltSerializer).GetMethod("SetCachedObjectID", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			methodIL.Emit(OpCodes.Callvirt, method);
			methodIL.MarkLabel(label);
			PropertyInfo[] properties = objectType.GetProperties();
			foreach (PropertyInfo propertyInfo in properties)
			{
				if (propertyInfo.GetCustomAttributes(typeof(DoNotSerializeAttribute), true).Length == 0)
				{
					if (propertyInfo.CanRead && propertyInfo.CanWrite)
					{
						MethodInfo method2 = typeof(Type).GetMethod("GetTypeFromHandle");
						MethodInfo method3 = typeof(AltSerializer).GetMethod("Deserialize", new Type[]
						{
							typeof(Type)
						});
						MethodInfo setMethod = propertyInfo.GetSetMethod();
						MethodInfo deserializeMethod = DynamicSerializerFactory.GetDeserializeMethod(propertyInfo.PropertyType);
						if (deserializeMethod != null)
						{
							methodIL.Emit(OpCodes.Ldloc_0);
							methodIL.Emit(OpCodes.Ldarg_1);
							methodIL.Emit(OpCodes.Callvirt, deserializeMethod);
							methodIL.Emit(OpCodes.Callvirt, setMethod);
						}
						else if (propertyInfo.PropertyType.IsValueType)
						{
							methodIL.Emit(OpCodes.Ldloc_0);
							methodIL.Emit(OpCodes.Ldarg_1);
							methodIL.Emit(OpCodes.Ldtoken, propertyInfo.PropertyType);
							methodIL.Emit(OpCodes.Call, method2);
							methodIL.Emit(OpCodes.Callvirt, method3);
							methodIL.Emit(OpCodes.Unbox_Any, propertyInfo.PropertyType);
							methodIL.Emit(OpCodes.Callvirt, setMethod);
						}
						else
						{
							methodIL.Emit(OpCodes.Ldloc_0);
							methodIL.Emit(OpCodes.Ldarg_1);
							methodIL.Emit(OpCodes.Ldtoken, propertyInfo.PropertyType);
							methodIL.Emit(OpCodes.Call, method2);
							methodIL.Emit(OpCodes.Callvirt, method3);
							methodIL.Emit(OpCodes.Castclass, propertyInfo.PropertyType);
							methodIL.Emit(OpCodes.Callvirt, setMethod);
						}
					}
				}
			}
			methodIL.Emit(OpCodes.Ldloc_0);
			methodIL.Emit(OpCodes.Stloc_1);
			Label label2 = methodIL.DefineLabel();
			methodIL.Emit(OpCodes.Br_S, label2);
			methodIL.MarkLabel(label2);
			methodIL.Emit(OpCodes.Ldloc_1);
			methodIL.Emit(OpCodes.Ret);
		}

		private static Type[] streamTypes = new Type[]
		{
			typeof(int),
			typeof(uint),
			typeof(short),
			typeof(ushort),
			typeof(long),
			typeof(ulong),
			typeof(DateTime),
			typeof(TimeSpan),
			typeof(float),
			typeof(double),
			typeof(decimal),
			typeof(Guid),
			typeof(string)
		};

		private static MethodInfo[] _serializeMethods;

		private static MethodInfo[] _deserializeMethods;
	}
}
