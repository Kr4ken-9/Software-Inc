﻿using System;
using System.Text;

namespace AltSerialize
{
	public static class Serializer
	{
		public static SerializeFlags DefaultSerializeFlags
		{
			get
			{
				return Serializer._serializer.DefaultSerializeFlags;
			}
			set
			{
				Serializer._serializer.DefaultSerializeFlags = value;
			}
		}

		public static Encoding DefaultEncoding
		{
			get
			{
				return Serializer._serializer.DefaultEncoding;
			}
			set
			{
				Serializer._serializer.DefaultEncoding = value;
			}
		}

		public static byte[] Serialize(object anObject)
		{
			return Serializer._serializer.Serialize(anObject);
		}

		public static byte[] Serialize(object anObject, Type objectType)
		{
			return Serializer._serializer.Serialize(anObject, objectType);
		}

		public static object Deserialize(byte[] bytes, Type objectType)
		{
			return Serializer._serializer.Deserialize(bytes, objectType);
		}

		public static object Deserialize(byte[] bytes)
		{
			return Serializer._serializer.Deserialize(bytes);
		}

		public static void CacheObject(object cachedObject)
		{
			Serializer._serializer.CacheObject(cachedObject);
		}

		private static ByteSerializer _serializer = new ByteSerializer();
	}
}
