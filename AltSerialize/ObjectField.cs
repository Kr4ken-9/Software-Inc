﻿using System;
using System.Reflection;

namespace AltSerialize
{
	internal class ObjectField
	{
		public Type FieldType
		{
			get
			{
				return this.FieldInfo.FieldType;
			}
		}

		public FieldInfo FieldInfo
		{
			get
			{
				return this._fieldInfo;
			}
			set
			{
				this._fieldInfo = value;
			}
		}

		private FieldInfo _fieldInfo;
	}
}
