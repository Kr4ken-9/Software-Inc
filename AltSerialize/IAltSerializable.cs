﻿using System;

namespace AltSerialize
{
	public interface IAltSerializable
	{
		void Serialize(AltSerializer serializer);

		void Deserialize(AltSerializer deserializer);
	}
}
