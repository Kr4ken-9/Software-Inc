﻿using System;

namespace AltSerialize
{
	[Flags]
	internal enum SerializedObjectFlags : byte
	{
		None = 0,
		IsNull = 1,
		SetCache = 2,
		CachedItem = 4,
		PropertyName = 8,
		FieldName = 16,
		Type = 32,
		SystemType = 64,
		Array = 128,
		Invalid = 255
	}
}
