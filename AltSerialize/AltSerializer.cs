﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using UnityEngine;

namespace AltSerialize
{
	public class AltSerializer : IDisposable
	{
		static AltSerializer()
		{
			AltSerializer.AddTypes();
		}

		public AltSerializer() : this(new MemoryStream())
		{
		}

		public AltSerializer(byte[] bytes) : this(new MemoryStream(bytes))
		{
		}

		public AltSerializer(Stream stream)
		{
			this.InitStaticCache();
			this.Stream = stream;
			this.arrayBuffer = new byte[this.BlockSize];
		}

		private static void AddType(Type objectType, int hashId)
		{
			AltSerializer._hashIntType[hashId] = objectType;
			AltSerializer._hashTypeInt[objectType] = hashId;
		}

		private static void AddTypes()
		{
			AltSerializer.AddType(typeof(int), 0);
			AltSerializer.AddType(typeof(uint), 1);
			AltSerializer.AddType(typeof(short), 2);
			AltSerializer.AddType(typeof(ushort), 3);
			AltSerializer.AddType(typeof(byte), 4);
			AltSerializer.AddType(typeof(sbyte), 5);
			AltSerializer.AddType(typeof(long), 6);
			AltSerializer.AddType(typeof(ulong), 7);
			AltSerializer.AddType(typeof(float), 8);
			AltSerializer.AddType(typeof(double), 9);
			AltSerializer.AddType(typeof(decimal), 10);
			AltSerializer.AddType(typeof(int?), 20);
			AltSerializer.AddType(typeof(uint?), 21);
			AltSerializer.AddType(typeof(short?), 22);
			AltSerializer.AddType(typeof(ushort?), 23);
			AltSerializer.AddType(typeof(byte?), 24);
			AltSerializer.AddType(typeof(sbyte?), 25);
			AltSerializer.AddType(typeof(long?), 26);
			AltSerializer.AddType(typeof(ulong?), 27);
			AltSerializer.AddType(typeof(float?), 28);
			AltSerializer.AddType(typeof(double?), 29);
			AltSerializer.AddType(typeof(decimal?), 30);
			AltSerializer.AddType(typeof(char), 31);
			AltSerializer.AddType(typeof(char?), 32);
			AltSerializer.AddType(typeof(bool), 33);
			AltSerializer.AddType(typeof(bool?), 34);
			AltSerializer.AddType(typeof(int[]), 40);
			AltSerializer.AddType(typeof(uint[]), 41);
			AltSerializer.AddType(typeof(short[]), 42);
			AltSerializer.AddType(typeof(ushort[]), 43);
			AltSerializer.AddType(typeof(byte[]), 44);
			AltSerializer.AddType(typeof(sbyte[]), 45);
			AltSerializer.AddType(typeof(long[]), 46);
			AltSerializer.AddType(typeof(ulong[]), 47);
			AltSerializer.AddType(typeof(float[]), 48);
			AltSerializer.AddType(typeof(double[]), 49);
			AltSerializer.AddType(typeof(decimal[]), 50);
			AltSerializer.AddType(typeof(char[]), 51);
			AltSerializer.AddType(typeof(bool[]), 52);
			AltSerializer.AddType(typeof(TimeSpan), 100);
			AltSerializer.AddType(typeof(DateTime), 101);
			AltSerializer.AddType(typeof(Guid), 102);
			AltSerializer.AddType(typeof(TimeSpan?), 103);
			AltSerializer.AddType(typeof(DateTime?), 104);
			AltSerializer.AddType(typeof(Guid?), 105);
			AltSerializer.AddType(typeof(string), 106);
			AltSerializer.AddType(typeof(DateTime[]), 110);
			AltSerializer.AddType(typeof(TimeSpan[]), 111);
			AltSerializer.AddType(typeof(Guid[]), 112);
			AltSerializer.AddType(typeof(string[]), 113);
			AltSerializer.AddType(typeof(object), 250);
			AltSerializer.AddType(typeof(object[]), 251);
			AltSerializer.AddType(typeof(Type), 252);
			AltSerializer.AddType(typeof(Type[]), 253);
		}

		internal static Dictionary<Type, ObjectMetaData> MetaDataHash
		{
			get
			{
				return AltSerializer._metaDataHash;
			}
		}

		private static void GetAllFieldsOfType(Type type, HashSet<FieldInfo> fieldList)
		{
			if (type == null || type == typeof(object) || type == typeof(ValueType))
			{
				return;
			}
			FieldInfo[] fields = type.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			for (int i = 0; i < fields.Length; i++)
			{
				fieldList.Add(fields[i]);
			}
			AltSerializer.GetAllFieldsOfType(type.BaseType, fieldList);
		}

		internal static void InsertSortedMetaData(List<ReflectedMemberInfo> list, ReflectedMemberInfo minfo)
		{
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i].Name.CompareTo(minfo.Name) > 0)
				{
					list.Insert(i, minfo);
					return;
				}
			}
			list.Add(minfo);
		}

		internal static byte[] GetBytes(object obj, Type objectType)
		{
			if (objectType == typeof(int))
			{
				return BitConverter.GetBytes((int)obj);
			}
			if (objectType == typeof(bool))
			{
				return new byte[]
				{
					(!(bool)obj) ? 0 : 1
				};
			}
			if (objectType == typeof(byte))
			{
				return new byte[]
				{
					(byte)obj
				};
			}
			if (objectType == typeof(sbyte))
			{
				return new byte[]
				{
					(byte)((sbyte)obj)
				};
			}
			if (objectType == typeof(short))
			{
				return BitConverter.GetBytes((short)obj);
			}
			if (objectType == typeof(ushort))
			{
				return BitConverter.GetBytes((ushort)obj);
			}
			if (objectType == typeof(uint))
			{
				return BitConverter.GetBytes((uint)obj);
			}
			if (objectType == typeof(long))
			{
				return BitConverter.GetBytes((long)obj);
			}
			if (objectType == typeof(ulong))
			{
				return BitConverter.GetBytes((ulong)obj);
			}
			if (objectType == typeof(float))
			{
				return BitConverter.GetBytes((float)obj);
			}
			if (objectType == typeof(double))
			{
				return BitConverter.GetBytes((double)obj);
			}
			if (objectType == typeof(char))
			{
				return BitConverter.GetBytes((char)obj);
			}
			if (objectType == typeof(IntPtr))
			{
				throw new AltSerializeException("IntPtr type is not supported.");
			}
			if (objectType == typeof(UIntPtr))
			{
				throw new AltSerializeException("UIntPtr type is not supported.");
			}
			throw new AltSerializeException("Could not retrieve bytes from the object type " + objectType.FullName + ".");
		}

		internal static object ReadBytes(byte[] bytes, Type objectType)
		{
			if (objectType == typeof(bool))
			{
				return bytes[0] == 1;
			}
			if (objectType == typeof(byte))
			{
				return bytes[0];
			}
			if (objectType == typeof(sbyte))
			{
				return (sbyte)bytes[0];
			}
			if (objectType == typeof(short))
			{
				return BitConverter.ToInt16(bytes, 0);
			}
			if (objectType == typeof(ushort))
			{
				return BitConverter.ToUInt16(bytes, 0);
			}
			if (objectType == typeof(int))
			{
				return BitConverter.ToInt32(bytes, 0);
			}
			if (objectType == typeof(uint))
			{
				return BitConverter.ToUInt32(bytes, 0);
			}
			if (objectType == typeof(long))
			{
				return BitConverter.ToInt64(bytes, 0);
			}
			if (objectType == typeof(ulong))
			{
				return BitConverter.ToUInt64(bytes, 0);
			}
			if (objectType == typeof(float))
			{
				return BitConverter.ToSingle(bytes, 0);
			}
			if (objectType == typeof(double))
			{
				return BitConverter.ToDouble(bytes, 0);
			}
			if (objectType == typeof(char))
			{
				return BitConverter.ToChar(bytes, 0);
			}
			if (objectType == typeof(IntPtr))
			{
				throw new AltSerializeException("IntPtr type is not supported.");
			}
			throw new AltSerializeException("Could not retrieve bytes from the object type " + objectType.FullName + ".");
		}

		public bool SerializeProperties
		{
			get
			{
				return this._serializeProperties;
			}
			set
			{
				this._serializeProperties = value;
			}
		}

		public Encoding Encoding
		{
			get
			{
				return this._encoding;
			}
			set
			{
				this._encoding = value;
			}
		}

		public Stream Stream
		{
			get
			{
				return this._stream;
			}
			set
			{
				this._stream = value;
			}
		}

		public bool CacheEnabled
		{
			get
			{
				return this._cacheEnabled;
			}
			set
			{
				this._cacheEnabled = value;
			}
		}

		public bool SerializePropertyNames
		{
			get
			{
				return this._serializePropertyNames;
			}
			set
			{
				this._serializePropertyNames = value;
			}
		}

		internal SerializerCache Cache
		{
			get
			{
				return this._cache;
			}
		}

		internal ObjectMetaData GetMetaData(Type type)
		{
			if (type == null)
			{
				throw new AltSerializeException("The serializer could not get meta data for the type.");
			}
			if (AltSerializer.MetaDataHash.ContainsKey(type))
			{
				return AltSerializer.MetaDataHash[type];
			}
			if (type.GetCustomAttributes(typeof(CompiledSerializerAttribute), true).Length != 0)
			{
				ObjectMetaData objectMetaData = new ObjectMetaData(this);
				objectMetaData.ObjectType = type;
				objectMetaData.DynamicSerializer = DynamicSerializerFactory.GenerateSerializer(type);
				AltSerializer.MetaDataHash[type] = objectMetaData;
				return objectMetaData;
			}
			if (type.GetInterface(typeof(IAltSerializable).Name) != null)
			{
				ObjectMetaData objectMetaData2 = new ObjectMetaData(this);
				objectMetaData2.ObjectType = type;
				objectMetaData2.IsIAltSerializable = true;
				AltSerializer.MetaDataHash[type] = objectMetaData2;
				return objectMetaData2;
			}
			List<ReflectedMemberInfo> list = new List<ReflectedMemberInfo>();
			PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
			foreach (PropertyInfo propertyInfo in properties)
			{
				if (propertyInfo.GetCustomAttributes(typeof(DoNotSerializeAttribute), true).Length <= 0)
				{
					if (propertyInfo.GetIndexParameters().Length <= 0)
					{
						if (propertyInfo.CanRead && propertyInfo.CanWrite)
						{
							AltSerializer.InsertSortedMetaData(list, new ReflectedMemberInfo(propertyInfo));
						}
					}
				}
			}
			List<ReflectedMemberInfo> list2 = new List<ReflectedMemberInfo>();
			HashSet<FieldInfo> hashSet = new HashSet<FieldInfo>();
			AltSerializer.GetAllFieldsOfType(type, hashSet);
			foreach (FieldInfo fieldInfo in hashSet)
			{
				if (!fieldInfo.IsNotSerialized)
				{
					AltSerializer.InsertSortedMetaData(list2, new ReflectedMemberInfo(fieldInfo));
				}
			}
			ObjectMetaData objectMetaData3 = new ObjectMetaData(this);
			objectMetaData3.ObjectType = type;
			objectMetaData3.Fields = list2.ToArray();
			objectMetaData3.Properties = list.ToArray();
			if (type.IsGenericType)
			{
				objectMetaData3.GenericTypeDefinition = type.GetGenericTypeDefinition();
				objectMetaData3.GenericParameters = type.GetGenericArguments();
			}
			if (type.GetInterface(typeof(ISerializable).Name) != null)
			{
				objectMetaData3.IsISerializable = true;
				objectMetaData3.Extra = type.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
				{
					typeof(SerializationInfo),
					typeof(StreamingContext)
				}, null);
			}
			Type @interface = type.GetInterface("System.Collections.IList");
			if (@interface != null)
			{
				objectMetaData3.ImplementsIList = true;
			}
			@interface = type.GetInterface("System.Collections.IDictionary");
			if (@interface != null)
			{
				objectMetaData3.ImplementsIDictionary = true;
			}
			if (objectMetaData3.GenericTypeDefinition == typeof(List<>))
			{
				objectMetaData3.IsGenericList = true;
				objectMetaData3.ImplementsIList = false;
				objectMetaData3.Extra = type.GetField("_items", BindingFlags.Instance | BindingFlags.NonPublic);
				objectMetaData3.SizeField = type.GetField("_size", BindingFlags.Instance | BindingFlags.NonPublic);
				objectMetaData3.SerializeMethod = type.GetMethod("ToArray");
			}
			AltSerializer.MetaDataHash[type] = objectMetaData3;
			return objectMetaData3;
		}

		public void SetCachedObjectID(object obj, int cacheID)
		{
			this.Cache.SetCachedObjectId(obj, cacheID);
		}

		private void WriteSerializationFlags(SerializedObjectFlags flags)
		{
			this.Stream.WriteByte((byte)flags);
		}

		private SerializedObjectFlags ReadSerializationFlags()
		{
			return (SerializedObjectFlags)this.Stream.ReadByte();
		}

		private void WriteType(Type objectType)
		{
			int num;
			if (AltSerializer._hashTypeInt.TryGetValue(objectType, out num))
			{
				this.WriteUInt24(0);
				this.Stream.WriteByte((byte)num);
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				AltSerializer.GetTypeName(objectType, stringBuilder);
				byte[] bytes = Encoding.ASCII.GetBytes(stringBuilder.ToString());
				this.WriteUInt24(bytes.Length);
				this.Stream.Write(bytes, 0, bytes.Length);
			}
		}

		public static void GetTypeName(Type type, StringBuilder sb)
		{
			Type type2 = type;
			while (type2.IsArray)
			{
				type2 = type2.GetElementType();
			}
			if (type2.IsNested)
			{
				Type declaringType = type2.DeclaringType;
				if (string.IsNullOrEmpty(declaringType.Namespace))
				{
					sb.Append(declaringType.Name + "+" + type.Name);
				}
				else
				{
					sb.Append(string.Concat(new string[]
					{
						declaringType.Namespace,
						".",
						declaringType.Name,
						"+",
						type.Name
					}));
				}
			}
			else if (string.IsNullOrEmpty(type.Namespace))
			{
				sb.Append(type.Name);
			}
			else
			{
				sb.Append(type.Namespace + "." + type.Name);
			}
			if (type2.IsGenericType)
			{
				Type[] genericArguments = type2.GetGenericArguments();
				sb.Append("[");
				for (int i = 0; i < genericArguments.Length; i++)
				{
					Type type3 = genericArguments[i];
					sb.Append("[");
					AltSerializer.GetTypeName(type3, sb);
					sb.Append("]");
					if (i < genericArguments.Length - 1)
					{
						sb.Append(",");
					}
				}
				sb.Append("]");
			}
			sb.Append(", " + type.Assembly.GetName().Name);
		}

		private Type ReadType()
		{
			int num = this.ReadUInt24();
			if (num == 0)
			{
				num = this.Stream.ReadByte();
				return AltSerializer._hashIntType[num];
			}
			byte[] array = new byte[num];
			this.Stream.Read(array, 0, num);
			string @string = Encoding.ASCII.GetString(array);
			Type type;
			if (AltSerializer._types.TryGetValue(@string, out type))
			{
				return type;
			}
			type = Type.GetType(@string);
			if (type == null)
			{
				throw new AltSerializeException("Unable to GetType object type '" + @string + "'");
			}
			AltSerializer._types[@string] = type;
			return type;
		}

		public void Reset()
		{
			if (this.Stream != null)
			{
				this.Stream.Position = 0L;
			}
			this.Cache.Clear();
		}

		public void CacheObject(object cachedObject)
		{
			this.Cache.CacheObject(cachedObject, true);
		}

		private void CacheType(Type objType)
		{
			this.GetMetaData(objType);
			this.CacheObject(objType);
		}

		private void InitStaticCache()
		{
			Type[] array = new Type[]
			{
				typeof(List<int>),
				typeof(List<uint>),
				typeof(List<byte>),
				typeof(List<sbyte>),
				typeof(List<short>),
				typeof(List<ushort>),
				typeof(List<long>),
				typeof(List<ulong>),
				typeof(List<DateTime>),
				typeof(List<TimeSpan>),
				typeof(List<decimal>),
				typeof(List<float>),
				typeof(List<double>),
				typeof(List<Guid>)
			};
			foreach (Type objType in array)
			{
				this.CacheType(objType);
			}
		}

		private void WriteUInt24(int value)
		{
			this.Write((byte)(value & 255));
			this.Write((byte)(value >> 8 & 255));
			this.Write((byte)(value >> 16 & 255));
		}

		public void Write(byte val)
		{
			this.Stream.WriteByte(val);
		}

		public void Write(sbyte val)
		{
			this.Stream.WriteByte((byte)val);
		}

		public void Write(byte[] bytes, int offset, int count)
		{
			this.Stream.Write(bytes, offset, count);
		}

		public void Write(byte[] bytes)
		{
			this.Stream.Write(bytes, 0, bytes.Length);
		}

		public void Write(int value)
		{
			this.Stream.Write(BitConverter.GetBytes(value), 0, 4);
		}

		public void Write(uint value)
		{
			this.Stream.Write(BitConverter.GetBytes(value), 0, 4);
		}

		public void Write(string str)
		{
			if (str == null)
			{
				this.WriteUInt24(16777215);
				return;
			}
			int byteCount = this.Encoding.GetByteCount(str);
			this.WriteUInt24(byteCount);
			if (byteCount > this.BlockSize)
			{
				byte[] array = new byte[byteCount];
				this.Encoding.GetBytes(str, 0, str.Length, array, 0);
				this.Stream.Write(array, 0, byteCount);
			}
			else
			{
				this.Encoding.GetBytes(str, 0, str.Length, this.arrayBuffer, 0);
				this.Stream.Write(this.arrayBuffer, 0, byteCount);
			}
		}

		public void Write(short value)
		{
			this.Stream.WriteByte((byte)(value & 255));
			this.Stream.WriteByte((byte)(value >> 8 & 255));
		}

		public void Write(ushort value)
		{
			this.Stream.WriteByte((byte)(value & 255));
			this.Stream.WriteByte((byte)(value >> 8 & 255));
		}

		public void Write(long value)
		{
			this.Stream.Write(BitConverter.GetBytes(value), 0, 8);
		}

		public void Write(ulong value)
		{
			this.Stream.Write(BitConverter.GetBytes(value), 0, 8);
		}

		public void Write(DateTime value)
		{
			long value2 = value.ToBinary();
			this.Write(value2);
		}

		public void Write(TimeSpan value)
		{
			long ticks = value.Ticks;
			this.Write(ticks);
		}

		public void Write(Guid value)
		{
			this.Write(value.ToByteArray());
		}

		public void Write(decimal value)
		{
			int[] bits = decimal.GetBits(value);
			this.Write(bits[0]);
			this.Write(bits[1]);
			this.Write(bits[2]);
			this.Write(bits[3]);
		}

		public void Write(double value)
		{
			this.Stream.Write(BitConverter.GetBytes(value), 0, 8);
		}

		public void Write(float value)
		{
			this.Stream.Write(BitConverter.GetBytes(value), 0, 4);
		}

		public void Write(char value)
		{
			this.Stream.Write(BitConverter.GetBytes(value), 0, 2);
		}

		public void WriteCultureInfo(CultureInfo info)
		{
			this.SerializeValueType(info.LCID, typeof(int));
		}

		private int ReadUInt24()
		{
			int num = this.ReadByte();
			num += this.ReadByte() << 8;
			return num + (this.ReadByte() << 16);
		}

		public int ReadInt32()
		{
			this.Stream.Read(this.arrayBuffer, 0, 4);
			return BitConverter.ToInt32(this.arrayBuffer, 0);
		}

		public uint ReadUInt32()
		{
			this.Stream.Read(this.arrayBuffer, 0, 4);
			return BitConverter.ToUInt32(this.arrayBuffer, 0);
		}

		public int ReadByte()
		{
			return this.Stream.ReadByte();
		}

		public sbyte ReadSByte()
		{
			return (sbyte)this.Stream.ReadByte();
		}

		public byte[] ReadBytes(int count)
		{
			byte[] array = new byte[count];
			this.ReadBytes(array, 0, count);
			return array;
		}

		public void ReadBytes(byte[] bytes, int offset, int count)
		{
			this.Stream.Read(bytes, offset, count);
		}

		public string ReadString()
		{
			int num = this.ReadUInt24();
			if (num == 16777215)
			{
				return null;
			}
			byte[] array;
			if (num > this.BlockSize)
			{
				array = new byte[num];
			}
			else
			{
				array = this.arrayBuffer;
			}
			this.Stream.Read(array, 0, num);
			return this.Encoding.GetString(array, 0, num);
		}

		public short ReadInt16()
		{
			this.Stream.Read(this.arrayBuffer, 0, 2);
			return (short)((int)this.arrayBuffer[0] + ((int)this.arrayBuffer[1] << 8));
		}

		public ushort ReadUInt16()
		{
			this.Stream.Read(this.arrayBuffer, 0, 2);
			return (ushort)((int)this.arrayBuffer[0] + ((int)this.arrayBuffer[1] << 8));
		}

		public long ReadInt64()
		{
			this.Stream.Read(this.arrayBuffer, 0, 8);
			return BitConverter.ToInt64(this.arrayBuffer, 0);
		}

		public ulong ReadUInt64()
		{
			this.Stream.Read(this.arrayBuffer, 0, 8);
			return BitConverter.ToUInt64(this.arrayBuffer, 0);
		}

		public DateTime ReadDateTime()
		{
			long dateData = this.ReadInt64();
			return DateTime.FromBinary(dateData);
		}

		public TimeSpan ReadTimeSpan()
		{
			long ticks = this.ReadInt64();
			return new TimeSpan(ticks);
		}

		public Guid ReadGuid()
		{
			this.Stream.Read(this.guidArray, 0, 16);
			return new Guid(this.guidArray);
		}

		public char ReadChar()
		{
			this.Stream.Read(this.arrayBuffer, 0, 2);
			return BitConverter.ToChar(this.arrayBuffer, 0);
		}

		public decimal ReadDecimal()
		{
			return new decimal(new int[]
			{
				this.ReadInt32(),
				this.ReadInt32(),
				this.ReadInt32(),
				this.ReadInt32()
			});
		}

		public double ReadDouble()
		{
			this.Stream.Read(this.arrayBuffer, 0, 8);
			return BitConverter.ToDouble(this.arrayBuffer, 0);
		}

		public float ReadSingle()
		{
			this.Stream.Read(this.arrayBuffer, 0, 4);
			return BitConverter.ToSingle(this.arrayBuffer, 0);
		}

		public CultureInfo ReadCultureInfo()
		{
			int culture = (int)this.DeserializeValueType(typeof(int));
			return CultureInfo.GetCultureInfo(culture);
		}

		public void Dispose()
		{
			this.Dispose(true);
		}

		protected virtual void Dispose(bool disposeAll)
		{
			if (this._stream != null)
			{
				this._stream.Dispose();
			}
			this.Cache.Dispose();
			GC.SuppressFinalize(this);
		}

		private object DeserializeValueType(Type objectType)
		{
			if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				bool flag = (bool)this.Deserialize();
				if (flag)
				{
					Type type = objectType.GetGenericArguments()[0];
					ConstructorInfo constructor = objectType.GetConstructor(new Type[]
					{
						type
					});
					return constructor.Invoke(new object[]
					{
						this.Deserialize()
					});
				}
				return null;
			}
			else
			{
				if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(KeyValuePair<, >))
				{
					Type[] array = new Type[]
					{
						(Type)this.Deserialize(),
						(Type)this.Deserialize()
					};
					object obj = this.Deserialize();
					object obj2 = this.Deserialize();
					Type type2 = typeof(KeyValuePair<, >).MakeGenericType(array);
					ConstructorInfo constructor2 = type2.GetConstructor(array);
					return constructor2.Invoke(new object[]
					{
						obj,
						obj2
					});
				}
				if (objectType.IsPrimitive)
				{
					int num = Marshal.SizeOf(objectType);
					if (objectType == typeof(char))
					{
						num = 2;
					}
					if (objectType == typeof(bool))
					{
						num = 1;
					}
					byte[] array2 = new byte[num];
					this.Stream.Read(array2, 0, num);
					return AltSerializer.ReadBytes(array2, objectType);
				}
				if (objectType == typeof(DateTime))
				{
					long dateData = this.ReadInt64();
					return DateTime.FromBinary(dateData);
				}
				if (objectType == typeof(TimeSpan))
				{
					long value = this.ReadInt64();
					return TimeSpan.FromTicks(value);
				}
				if (objectType == typeof(decimal))
				{
					return new decimal(new int[]
					{
						this.ReadInt32(),
						this.ReadInt32(),
						this.ReadInt32(),
						this.ReadInt32()
					});
				}
				if (objectType == typeof(Guid))
				{
					byte[] array3 = new byte[16];
					this.Stream.Read(array3, 0, 16);
					return new Guid(array3);
				}
				if (objectType.IsEnum)
				{
					Type underlyingType = Enum.GetUnderlyingType(objectType);
					object value2 = this.Deserialize(underlyingType);
					return Enum.ToObject(objectType, value2);
				}
				if (objectType.IsValueType && !objectType.IsPrimitive && (objectType.Namespace == null || !objectType.Namespace.StartsWith("System")))
				{
					return this.DeserializeComplexType(objectType, -1);
				}
				int num2 = Marshal.SizeOf(objectType);
				IntPtr intPtr = Marshal.AllocHGlobal(num2);
				byte[] source = this.ReadBytes(num2);
				Marshal.Copy(source, 0, intPtr, num2);
				object result = Marshal.PtrToStructure(intPtr, objectType);
				Marshal.FreeHGlobal(intPtr);
				return result;
			}
		}

		private object DeserializeByteArray()
		{
			int num = this.ReadInt32();
			byte[] array = new byte[num];
			this.ReadBytes(array, 0, num);
			return array;
		}

		private object DeserializeValueTypeArray(Type baseType)
		{
			int num = Marshal.SizeOf(baseType);
			if (baseType == typeof(char))
			{
				num = 2;
			}
			else if (baseType == typeof(bool))
			{
				num = 1;
			}
			int num2 = this.ReadInt32();
			int i = 0;
			Array array = Array.CreateInstance(baseType, num2);
			int num3 = num2 * num;
			while (i < num3)
			{
				int num4 = this.BlockSize;
				if (i + num4 > num3)
				{
					num4 = num3 - i;
				}
				this.Stream.Read(this.arrayBuffer, 0, num4);
				Buffer.BlockCopy(this.arrayBuffer, 0, array, i, num4);
				i += num4;
			}
			return array;
		}

		private object DeserializeArray(Type objectType, int cacheID)
		{
			int num = this.ReadByte();
			Type elementType = objectType.GetElementType();
			if (!elementType.IsPrimitive || num != 1)
			{
				int[] array = new int[num];
				int num2 = 1;
				for (int i = 0; i < num; i++)
				{
					array[i] = this.ReadInt32();
					num2 *= array[i];
				}
				Array array2 = Array.CreateInstance(elementType, array);
				if (cacheID > 0)
				{
					this.Cache.SetCachedObjectId(array2, cacheID);
				}
				int[] array3 = new int[num];
				for (int j = 0; j < num2; j++)
				{
					object value = this.DeserializeElement(elementType);
					try
					{
						array2.SetValue(value, array3);
					}
					catch (InvalidCastException message)
					{
						Debug.Log(message);
						throw;
					}
					array3[array3.Length - 1]++;
					for (int k = array3.Length - 1; k >= 0; k--)
					{
						if (array3[k] >= array[k] && k > 0)
						{
							array3[k] = 0;
							array3[k - 1]++;
						}
					}
				}
				return array2;
			}
			if (elementType == typeof(byte))
			{
				return this.DeserializeByteArray();
			}
			return this.DeserializeValueTypeArray(elementType);
		}

		private object DeserializeElement(Type elementType)
		{
			if (elementType.IsPrimitive)
			{
				return this.DeserializeValueType(elementType);
			}
			if ((elementType.IsClass && !elementType.IsSealed) || elementType.IsAbstract || elementType.IsInterface)
			{
				return this.Deserialize();
			}
			return this.Deserialize(elementType);
		}

		private object DeserializeList(Type objectType, int cacheID, ObjectMetaData metaData)
		{
			int num = this.ReadInt32();
			IList list = Activator.CreateInstance(objectType) as IList;
			if (cacheID > 0)
			{
				this.Cache.SetCachedObjectId(list, cacheID);
			}
			Type elementType = typeof(object);
			if (metaData.GenericParameters != null && metaData.GenericParameters.Length > 0)
			{
				elementType = metaData.GenericParameters[0];
			}
			for (int i = 0; i < num; i++)
			{
				object value = this.DeserializeElement(elementType);
				list.Add(value);
			}
			return list;
		}

		private object DeserializeDictionary(Type objectType, int cacheID)
		{
			Type[] genericArguments = objectType.GetGenericArguments();
			int num = (int)this.Deserialize(typeof(int));
			IDictionary dictionary = Activator.CreateInstance(objectType) as IDictionary;
			if (cacheID > 0)
			{
				this.Cache.SetCachedObjectId(dictionary, cacheID);
			}
			Type elementType = typeof(object);
			Type elementType2 = typeof(object);
			if (genericArguments.Length > 0)
			{
				elementType = genericArguments[0];
				elementType2 = genericArguments[1];
			}
			for (int i = 0; i < num; i++)
			{
				object key = this.DeserializeElement(elementType);
				object value = this.DeserializeElement(elementType2);
				dictionary[key] = value;
			}
			return dictionary;
		}

		private object DeserializeComplexType(Type objectType, int cacheID)
		{
			ObjectMetaData metaData = this.GetMetaData(objectType);
			if (metaData.DynamicSerializer != null)
			{
				return metaData.DynamicSerializer.Deserialize(this, cacheID);
			}
			if (metaData.IsIAltSerializable)
			{
				object obj = Activator.CreateInstance(objectType);
				if (cacheID > 0)
				{
					this.Cache.SetCachedObjectId(obj, cacheID);
				}
				((IAltSerializable)obj).Deserialize(this);
				return obj;
			}
			if (metaData.ImplementsIDictionary)
			{
				return this.DeserializeDictionary(objectType, cacheID);
			}
			if (metaData.ImplementsIList)
			{
				return this.DeserializeList(objectType, cacheID, metaData);
			}
			if (metaData.IsISerializable)
			{
				SerializationInfo serializationInfo = new SerializationInfo(objectType, new AltFormatter());
				StreamingContext streamingContext = new StreamingContext(StreamingContextStates.All);
				Dictionary<string, object> dictionary = (Dictionary<string, object>)this.Deserialize(typeof(Dictionary<string, object>));
				foreach (KeyValuePair<string, object> keyValuePair in dictionary)
				{
					serializationInfo.AddValue(keyValuePair.Key, keyValuePair.Value);
				}
				ConstructorInfo constructorInfo = (ConstructorInfo)metaData.Extra;
				return constructorInfo.Invoke(new object[]
				{
					serializationInfo,
					streamingContext
				});
			}
			if (metaData.IsGenericList)
			{
				FieldInfo fieldInfo = (FieldInfo)metaData.Extra;
				Type fieldType = fieldInfo.FieldType;
				object obj2 = this.DeserializeArray(fieldType, 0);
				object obj3 = Activator.CreateInstance(objectType, new object[]
				{
					obj2
				});
				if (cacheID > 0)
				{
					this.Cache.SetCachedObjectId(obj3, cacheID);
				}
				return obj3;
			}
			object obj4 = Activator.CreateInstance(objectType);
			if (cacheID > 0)
			{
				this.Cache.SetCachedObjectId(obj4, cacheID);
			}
			if (this.SerializePropertyNames)
			{
				int num = this.ReadInt32();
				for (int i = 0; i < num; i++)
				{
					string text = this.ReadString();
					ReflectedMemberInfo reflectedMemberInfo = metaData.FindMemberInfoByName(text);
					if (reflectedMemberInfo == null)
					{
						foreach (FieldInfo fieldInfo2 in metaData.ObjectType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
						{
							object[] customAttributes = fieldInfo2.GetCustomAttributes(typeof(NameRedirection), false);
							if (customAttributes.Length > 0)
							{
								NameRedirection nameRedirection = customAttributes[0] as NameRedirection;
								if (nameRedirection != null && nameRedirection.OldNames.Contains(text))
								{
									object value = this.DeserializeElement(fieldInfo2.FieldType);
									fieldInfo2.SetValue(obj4, value);
									break;
								}
							}
						}
						throw new AltSerializeException(string.Concat(new string[]
						{
							"Unable to find the property '",
							text,
							"' in object type '",
							objectType.FullName,
							"'."
						}));
					}
					object newValue = this.DeserializeElement(reflectedMemberInfo.FieldType);
					reflectedMemberInfo.SetValue(obj4, newValue);
				}
			}
			else
			{
				foreach (ReflectedMemberInfo reflectedMemberInfo2 in metaData.Values)
				{
					if (this.SerializeProperties || reflectedMemberInfo2.FieldType.IsSerializable)
					{
						object newValue2 = this.DeserializeElement(reflectedMemberInfo2.FieldType);
						reflectedMemberInfo2.SetValue(obj4, newValue2);
					}
				}
			}
			MethodInfo methodInfo = objectType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).FirstOrDefault((MethodInfo x) => x.GetCustomAttributes(typeof(OnDeserializedAttribute), true).Length > 0);
			if (methodInfo != null)
			{
				methodInfo.Invoke(obj4, this._streamContext);
			}
			return obj4;
		}

		public object Deserialize()
		{
			return this.Deserialize(null);
		}

		public object Deserialize(Type objectType)
		{
			int num = 0;
			SerializedObjectFlags serializedObjectFlags = this.ReadSerializationFlags();
			if (serializedObjectFlags == SerializedObjectFlags.IsNull)
			{
				return null;
			}
			if ((byte)(serializedObjectFlags & SerializedObjectFlags.CachedItem) != 0)
			{
				num = this.ReadInt32();
				return this.Cache.GetCachedObject(num);
			}
			if ((byte)(serializedObjectFlags & SerializedObjectFlags.SetCache) != 0)
			{
				num = this.ReadInt32();
			}
			if ((byte)(serializedObjectFlags & SerializedObjectFlags.SystemType) != 0)
			{
				int key = this.Stream.ReadByte();
				if (!AltSerializer._hashIntType.TryGetValue(key, out objectType))
				{
					throw new AltSerializeException("Unknown data type encountered in stream.");
				}
			}
			if ((byte)(serializedObjectFlags & SerializedObjectFlags.Type) != 0)
			{
				objectType = (Type)this.Deserialize(typeof(Type));
			}
			if (objectType == null)
			{
				throw new AltSerializeException("Object type was null, probably corrupt file");
			}
			if (objectType.IsValueType)
			{
				return this.DeserializeValueType(objectType);
			}
			object obj;
			if (objectType.IsArray)
			{
				obj = this.DeserializeArray(objectType, num);
				num = 0;
			}
			else if (objectType == typeof(string))
			{
				obj = this.ReadString();
			}
			else if (objectType == typeof(Type))
			{
				obj = this.ReadType();
			}
			else if (objectType == typeof(CultureInfo))
			{
				obj = this.ReadCultureInfo();
			}
			else
			{
				obj = this.DeserializeComplexType(objectType, num);
				num = 0;
			}
			if (num > 0)
			{
				this.Cache.SetCachedObjectId(obj, num);
			}
			return obj;
		}

		private void SerializeValueType(object obj, Type objectType)
		{
			if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				bool flag = (bool)objectType.GetProperty("HasValue").GetValue(obj, null);
				this.Serialize(flag);
				if (flag)
				{
					this.Serialize(objectType.GetProperty("Value").GetValue(obj, null));
				}
			}
			else if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(KeyValuePair<, >))
			{
				Type[] genericArguments = objectType.GetGenericArguments();
				this.Serialize(genericArguments[0]);
				this.Serialize(genericArguments[1]);
				this.Serialize(objectType.GetProperty("Key").GetValue(obj, null));
				this.Serialize(objectType.GetProperty("Value").GetValue(obj, null));
			}
			else if (objectType.IsPrimitive)
			{
				byte[] bytes = AltSerializer.GetBytes(obj, objectType);
				this.Stream.Write(bytes, 0, bytes.Length);
			}
			else if (objectType == typeof(DateTime))
			{
				this.Write(((DateTime)obj).ToBinary());
			}
			else if (objectType == typeof(TimeSpan))
			{
				this.Write(((TimeSpan)obj).Ticks);
			}
			else if (objectType == typeof(Guid))
			{
				byte[] buffer = ((Guid)obj).ToByteArray();
				this.Stream.Write(buffer, 0, 16);
			}
			else if (objectType.IsEnum)
			{
				Type underlyingType = Enum.GetUnderlyingType(objectType);
				object obj2 = Convert.ChangeType(obj, underlyingType);
				this.Serialize(obj2, underlyingType);
			}
			else if (objectType == typeof(decimal))
			{
				int[] bits = decimal.GetBits((decimal)obj);
				this.Write(bits[0]);
				this.Write(bits[1]);
				this.Write(bits[2]);
				this.Write(bits[3]);
			}
			else if (objectType.IsValueType && !objectType.IsPrimitive && (objectType.Namespace == null || !objectType.Namespace.StartsWith("System")))
			{
				this.SerializeComplexType(obj, objectType);
			}
			else
			{
				int num = Marshal.SizeOf(objectType);
				byte[] array = new byte[num];
				IntPtr intPtr = Marshal.AllocHGlobal(num);
				Marshal.StructureToPtr(obj, intPtr, false);
				Marshal.Copy(intPtr, array, 0, num);
				this.Write(array, 0, array.Length);
				Marshal.FreeHGlobal(intPtr);
			}
		}

		private void SerializeValueTypeArray(Array array, Type baseType, int count)
		{
			int num = Marshal.SizeOf(baseType);
			if (baseType == typeof(bool))
			{
				num = 1;
			}
			else if (baseType == typeof(char))
			{
				num = 2;
			}
			this.Write(count);
			int num2 = count * num;
			int num3;
			for (int i = 0; i < num2; i += num3)
			{
				num3 = this.BlockSize;
				if (i + this.BlockSize > num2)
				{
					num3 = num2 - i;
				}
				Buffer.BlockCopy(array, i, this.arrayBuffer, 0, num3);
				this.Stream.Write(this.arrayBuffer, 0, num3);
			}
		}

		private void SerializeByteArray(object array, int count)
		{
			byte[] bytes = (byte[])array;
			this.Write(count);
			this.Write(bytes, 0, count);
		}

		private void SerializeArray(object obj, Type objectType, int count)
		{
			Array array = (Array)obj;
			if (objectType == null)
			{
				objectType = obj.GetType().GetElementType();
			}
			this.Write((byte)array.Rank);
			if (count < 0)
			{
				count = array.Length;
			}
			Type type = objectType;
			if (array.Rank != 1 || !type.IsPrimitive)
			{
				int[] array2 = new int[array.Rank];
				if (array.Rank == 1)
				{
					this.Write(count);
				}
				else
				{
					for (int i = 0; i < array.Rank; i++)
					{
						array2[i] = array.GetLength(i);
						this.Write(array2[i]);
					}
				}
				int[] array3 = new int[array.Rank];
				for (int j = 0; j < count; j++)
				{
					object value = array.GetValue(array3);
					this.SerializeElement(value, type);
					array3[array3.Length - 1]++;
					for (int k = array3.Length - 1; k >= 0; k--)
					{
						if (array3[k] >= array2[k] && k > 0)
						{
							array3[k] = 0;
							array3[k - 1]++;
						}
					}
				}
				return;
			}
			if (type == typeof(byte))
			{
				this.SerializeByteArray(obj, count);
				return;
			}
			this.SerializeValueTypeArray((Array)obj, type, count);
		}

		private void SerializeElement(object obj, Type elementType)
		{
			if (elementType.IsPrimitive)
			{
				this.SerializeValueType(obj, elementType);
			}
			else if ((elementType.IsClass && !elementType.IsSealed) || elementType.IsAbstract || elementType.IsInterface)
			{
				this.Serialize(obj);
			}
			else
			{
				this.Serialize(obj, elementType);
			}
		}

		private void SerializeList(object genericObject, Type objectType, ObjectMetaData metaData)
		{
			IList list = genericObject as IList;
			if (list == null)
			{
				throw new AltSerializeException("The object type " + objectType.FullName + " does not implement IList.");
			}
			IEnumerator enumerator = list.GetEnumerator();
			this.Write(list.Count);
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				this.SerializeElement(obj, objectType);
			}
		}

		private void SerializeDictionary(object genericObject, Type objectType)
		{
			Type[] genericArguments = objectType.GetGenericArguments();
			IDictionary dictionary = genericObject as IDictionary;
			if (dictionary == null)
			{
				throw new AltSerializeException("The object type " + objectType.FullName + " does not implement IDictionary.");
			}
			IDictionaryEnumerator enumerator = dictionary.GetEnumerator();
			Type elementType = typeof(object);
			Type elementType2 = typeof(object);
			if (genericArguments.Length > 0)
			{
				elementType = genericArguments[0];
				elementType2 = genericArguments[1];
			}
			this.Serialize(dictionary.Count, typeof(int));
			while (enumerator.MoveNext())
			{
				this.SerializeElement(enumerator.Key, elementType);
				this.SerializeElement(enumerator.Value, elementType2);
			}
		}

		private void SerializeComplexType(object obj, Type objectType)
		{
			ObjectMetaData metaData = this.GetMetaData(objectType);
			if (metaData.DynamicSerializer != null)
			{
				metaData.DynamicSerializer.Serialize(obj, this);
				return;
			}
			if (metaData.IsIAltSerializable)
			{
				((IAltSerializable)obj).Serialize(this);
				return;
			}
			if (metaData.ImplementsIDictionary)
			{
				this.SerializeDictionary(obj, objectType);
				return;
			}
			if (metaData.ImplementsIList)
			{
				this.SerializeList(obj, objectType, metaData);
				return;
			}
			if (metaData.IsISerializable)
			{
				SerializationInfo serializationInfo = new SerializationInfo(objectType, new AltFormatter());
				StreamingContext context = new StreamingContext(StreamingContextStates.All);
				((ISerializable)obj).GetObjectData(serializationInfo, context);
				SerializationInfoEnumerator enumerator = serializationInfo.GetEnumerator();
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				while (enumerator.MoveNext())
				{
					dictionary[enumerator.Name] = enumerator.Value;
				}
				this.Serialize(dictionary, typeof(Dictionary<string, object>));
				return;
			}
			if (metaData.IsGenericList)
			{
				FieldInfo fieldInfo = (FieldInfo)metaData.Extra;
				object value = fieldInfo.GetValue(obj);
				int count = ((ICollection)obj).Count;
				this.SerializeArray(value, metaData.GenericParameters[0], count);
				return;
			}
			if (this.SerializePropertyNames)
			{
				this.Write(metaData.Values.Length);
			}
			foreach (ReflectedMemberInfo reflectedMemberInfo in metaData.Values)
			{
				object value2 = reflectedMemberInfo.GetValue(obj);
				if (this.SerializePropertyNames)
				{
					this.Write(reflectedMemberInfo.Name);
				}
				if (this.SerializeProperties || reflectedMemberInfo.FieldType.IsSerializable)
				{
					this.SerializeElement(value2, reflectedMemberInfo.FieldType);
				}
			}
		}

		public void Serialize(object obj)
		{
			this.Serialize(obj, null);
		}

		public void Serialize(object obj, Type objectType)
		{
			if (obj == null)
			{
				this.WriteSerializationFlags(SerializedObjectFlags.IsNull);
				return;
			}
			SerializedObjectFlags serializedObjectFlags = SerializedObjectFlags.None;
			int num = 0;
			bool flag = true;
			if (objectType == null)
			{
				objectType = obj.GetType();
				if (obj.GetType().BaseType == typeof(Type))
				{
					serializedObjectFlags = SerializedObjectFlags.SystemType;
					objectType = typeof(Type);
				}
				else if (objectType == typeof(string))
				{
					serializedObjectFlags = SerializedObjectFlags.SystemType;
				}
				else if (AltSerializer._hashTypeInt.ContainsKey(objectType))
				{
					serializedObjectFlags = SerializedObjectFlags.SystemType;
					flag = false;
				}
				else
				{
					serializedObjectFlags = SerializedObjectFlags.Type;
				}
			}
			if (this.CacheEnabled && flag && !objectType.IsEnum && !objectType.IsValueType)
			{
				serializedObjectFlags |= SerializedObjectFlags.SetCache;
				num = this.Cache.GetObjectCacheID(obj, objectType);
				if (num != 0)
				{
					this.WriteSerializationFlags(SerializedObjectFlags.CachedItem);
					this.Write(num);
					return;
				}
				num = this.Cache.CacheObject(obj, false);
			}
			this.WriteSerializationFlags(serializedObjectFlags);
			if ((byte)(serializedObjectFlags & SerializedObjectFlags.SetCache) != 0)
			{
				this.Write(num);
			}
			if ((byte)(serializedObjectFlags & SerializedObjectFlags.SystemType) != 0)
			{
				this.Write((byte)AltSerializer._hashTypeInt[objectType]);
			}
			if ((byte)(serializedObjectFlags & SerializedObjectFlags.Type) != 0)
			{
				this.Serialize(objectType, typeof(Type));
			}
			if (objectType.IsValueType)
			{
				this.SerializeValueType(obj, objectType);
			}
			else if (objectType.IsArray)
			{
				this.SerializeArray(obj, null, -1);
			}
			else if (objectType == typeof(string))
			{
				this.Write((string)obj);
			}
			else if (objectType == typeof(Type))
			{
				this.WriteType(obj as Type);
			}
			else if (objectType == typeof(CultureInfo))
			{
				this.WriteCultureInfo(obj as CultureInfo);
			}
			else
			{
				this.SerializeComplexType(obj, objectType);
			}
		}

		private static Dictionary<Type, int> _hashTypeInt = new Dictionary<Type, int>();

		private static Dictionary<int, Type> _hashIntType = new Dictionary<int, Type>();

		private static Dictionary<string, Type> _types = new Dictionary<string, Type>();

		private static Dictionary<Type, ObjectMetaData> _metaDataHash = new Dictionary<Type, ObjectMetaData>();

		private bool _serializeProperties = true;

		private Encoding _encoding = Encoding.Unicode;

		private Stream _stream;

		private bool _cacheEnabled = true;

		private bool _serializePropertyNames;

		private SerializerCache _cache = new SerializerCache();

		private byte[] arrayBuffer;

		private int BlockSize = 16384;

		private byte[] guidArray = new byte[16];

		private readonly object[] _streamContext = new object[]
		{
			default(StreamingContext)
		};
	}
}
