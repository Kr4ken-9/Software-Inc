﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AltSerialize
{
	internal class SerializerCache : IDisposable
	{
		public SerializerCache()
		{
			this._objList.Add(0);
		}

		public void Clear()
		{
			this.Clear(false);
		}

		public void Clear(bool clearPermanant)
		{
			this._newUniqueID = 1;
			for (int i = this._staticID; i < this._objList.Count; i++)
			{
				if (this._objList[i] != null)
				{
					this._hashByObject.Remove(this._objList[i]);
				}
			}
			this._objList.RemoveRange(this._staticID, this._objList.Count - this._staticID);
			this._newUniqueID = this._staticID;
		}

		public int GetObjectCacheID(object obj, Type objectType)
		{
			int result;
			if (this._hashByObject.TryGetValue(obj, out result))
			{
				return result;
			}
			return 0;
		}

		public int CacheObject(object obj, bool permanant)
		{
			if (permanant && this._staticID != this._newUniqueID)
			{
				throw new Exception("Unable to cache item.");
			}
			int newUniqueID = this._newUniqueID;
			this._newUniqueID++;
			this._objList.Insert(newUniqueID, obj);
			try
			{
				this._hashByObject[obj] = newUniqueID;
			}
			catch (Exception ex)
			{
				Debug.LogException(new Exception(string.Concat(new string[]
				{
					"Object: ",
					obj.GetType().ToString(),
					" = ",
					obj.ToString(),
					"\n",
					ex.ToString()
				})));
				throw ex;
			}
			if (permanant)
			{
				this._staticID++;
			}
			return newUniqueID;
		}

		public object GetCachedObject(int uniqueId)
		{
			if (uniqueId < this._objList.Count)
			{
				return this._objList[uniqueId];
			}
			return null;
		}

		public void SetCachedObjectId(object obj, int uniqueId)
		{
			while (this._objList.Count <= uniqueId)
			{
				this._objList.Add(null);
			}
			this._objList[uniqueId] = obj;
		}

		public void Dispose()
		{
			this.Dispose(true);
		}

		public virtual void Dispose(bool disposeAll)
		{
			GC.SuppressFinalize(this);
		}

		private Dictionary<object, int> _hashByObject = new Dictionary<object, int>();

		private List<object> _objList = new List<object>();

		private int _staticID = 1;

		private int _newUniqueID = 1;

		internal class SubHash
		{
			public SubHash()
			{
			}

			public SubHash(object storedObject, int id)
			{
				this.StoredObject = storedObject;
				this.ObjectID = id;
			}

			public object StoredObject
			{
				get
				{
					return this._storedObject;
				}
				set
				{
					this._storedObject = value;
				}
			}

			public int ObjectID
			{
				get
				{
					return this._objectId;
				}
				set
				{
					this._objectId = value;
				}
			}

			public override int GetHashCode()
			{
				return this.StoredObject.GetHashCode();
			}

			private object _storedObject;

			private int _objectId;
		}
	}
}
