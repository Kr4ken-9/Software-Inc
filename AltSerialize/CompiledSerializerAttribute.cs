﻿using System;

namespace AltSerialize
{
	[AttributeUsage(AttributeTargets.Class)]
	public class CompiledSerializerAttribute : Attribute
	{
	}
}
