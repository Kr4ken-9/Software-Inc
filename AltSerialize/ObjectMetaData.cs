﻿using System;
using System.Reflection;

namespace AltSerialize
{
	internal class ObjectMetaData
	{
		public ObjectMetaData(AltSerializer owner)
		{
			this.Owner = owner;
		}

		public AltSerializer Owner
		{
			get
			{
				return this._owner;
			}
			set
			{
				this._owner = value;
			}
		}

		public Type ObjectType
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}

		public bool ImplementsIList
		{
			get
			{
				return this._implementsIList;
			}
			set
			{
				this._implementsIList = value;
			}
		}

		public bool ImplementsIDictionary
		{
			get
			{
				return this._implementsIDictionary;
			}
			set
			{
				this._implementsIDictionary = value;
			}
		}

		public bool IsGenericList
		{
			get
			{
				return this._isGenericList;
			}
			set
			{
				this._isGenericList = value;
			}
		}

		public bool IsISerializable
		{
			get
			{
				return this._isISerializable;
			}
			set
			{
				this._isISerializable = value;
			}
		}

		public bool IsIAltSerializable
		{
			get
			{
				return this._useInterface;
			}
			set
			{
				this._useInterface = value;
			}
		}

		public ReflectedMemberInfo[] Fields
		{
			get
			{
				return this._fields;
			}
			set
			{
				this._fields = value;
			}
		}

		public ReflectedMemberInfo[] Properties
		{
			get
			{
				return this._properties;
			}
			set
			{
				this._properties = value;
			}
		}

		public ReflectedMemberInfo[] Values
		{
			get
			{
				if (this.Owner.SerializeProperties)
				{
					return this.Properties;
				}
				return this.Fields;
			}
		}

		public DynamicSerializer DynamicSerializer
		{
			get
			{
				return this._dynamicSerializer;
			}
			set
			{
				this._dynamicSerializer = value;
			}
		}

		public Type GenericTypeDefinition
		{
			get
			{
				return this._genericTypeDefinition;
			}
			set
			{
				this._genericTypeDefinition = value;
			}
		}

		public Type[] GenericParameters
		{
			get
			{
				return this._genericParameters;
			}
			set
			{
				this._genericParameters = value;
			}
		}

		public MethodInfo DeserializeMethod
		{
			get
			{
				return this._deserializeMethod;
			}
			set
			{
				this._deserializeMethod = value;
			}
		}

		public MethodInfo SerializeMethod
		{
			get
			{
				return this._serializeMethod;
			}
			set
			{
				this._serializeMethod = value;
			}
		}

		public object Extra
		{
			get
			{
				return this._extra;
			}
			set
			{
				this._extra = value;
			}
		}

		public FieldInfo SizeField
		{
			get
			{
				return this._size;
			}
			set
			{
				this._size = value;
			}
		}

		public ReflectedMemberInfo FindMemberInfoByName(string propertyName)
		{
			for (int i = 0; i < this.Values.Length; i++)
			{
				if (this.Values[i].Name == propertyName)
				{
					return this.Values[i];
				}
			}
			return null;
		}

		private AltSerializer _owner;

		private Type _type;

		private bool _implementsIList;

		private bool _implementsIDictionary;

		private bool _isGenericList;

		private bool _isISerializable;

		private bool _useInterface;

		private ReflectedMemberInfo[] _fields;

		private ReflectedMemberInfo[] _properties;

		private DynamicSerializer _dynamicSerializer;

		private Type _genericTypeDefinition;

		private Type[] _genericParameters;

		private MethodInfo _deserializeMethod;

		private MethodInfo _serializeMethod;

		private object _extra;

		private FieldInfo _size;
	}
}
