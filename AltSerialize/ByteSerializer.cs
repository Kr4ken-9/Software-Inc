﻿using System;
using System.IO;
using System.Text;

namespace AltSerialize
{
	public class ByteSerializer : IDisposable
	{
		public ByteSerializer()
		{
			this._AltSerializer = new AltSerializer();
			this._MemStream = new MemoryStream();
		}

		public SerializeFlags DefaultSerializeFlags
		{
			get
			{
				return this._defaultSerializeFlags;
			}
			set
			{
				this._defaultSerializeFlags = value;
			}
		}

		public Encoding DefaultEncoding
		{
			get
			{
				return this._AltSerializer.Encoding;
			}
			set
			{
				this._AltSerializer.Encoding = value;
			}
		}

		private void InitSerializer(SerializeFlags flags)
		{
			this._AltSerializer.Stream = this._MemStream;
			this._AltSerializer.SerializePropertyNames = ((flags & SerializeFlags.SerializePropertyNames) != SerializeFlags.None);
			this._AltSerializer.CacheEnabled = ((flags & SerializeFlags.SerializationCache) != SerializeFlags.None);
			this._AltSerializer.SerializeProperties = ((flags & SerializeFlags.SerializeProperties) != SerializeFlags.None);
		}

		public byte[] Serialize(object anObject)
		{
			object altSerializer = this._AltSerializer;
			byte[] result;
			lock (altSerializer)
			{
				this.InitSerializer(this.DefaultSerializeFlags);
				this._AltSerializer.Reset();
				this._AltSerializer.Serialize(anObject);
				result = this._MemStream.ToArray();
				this._MemStream.Position = 0L;
				this._MemStream.SetLength(0L);
				this._AltSerializer.Reset();
			}
			return result;
		}

		public byte[] Serialize(object anObject, Type objectType)
		{
			object altSerializer = this._AltSerializer;
			byte[] array;
			lock (altSerializer)
			{
				this.InitSerializer(this.DefaultSerializeFlags);
				this._AltSerializer.Reset();
				this._AltSerializer.Serialize(anObject, objectType);
				array = new byte[this._MemStream.Position];
				this._MemStream.Position = 0L;
				this._MemStream.Read(array, 0, array.Length);
				this._MemStream.SetLength(0L);
				this._AltSerializer.Reset();
			}
			return array;
		}

		public object Deserialize(byte[] bytes, Type objectType)
		{
			object altSerializer = this._AltSerializer;
			object result;
			lock (altSerializer)
			{
				this.InitSerializer(this.DefaultSerializeFlags);
				MemoryStream memoryStream = new MemoryStream(bytes);
				this._AltSerializer.Reset();
				this._AltSerializer.Stream = memoryStream;
				result = this._AltSerializer.Deserialize(objectType);
				this._AltSerializer.Stream = this._MemStream;
				memoryStream.Dispose();
			}
			return result;
		}

		public object Deserialize(byte[] bytes, Type objectType, SerializeFlags flags)
		{
			object altSerializer = this._AltSerializer;
			object result;
			lock (altSerializer)
			{
				this.InitSerializer(flags);
				MemoryStream memoryStream = new MemoryStream(bytes);
				this._AltSerializer.Reset();
				this._AltSerializer.Stream = memoryStream;
				result = this._AltSerializer.Deserialize(objectType);
				this._AltSerializer.Stream = this._MemStream;
				memoryStream.Dispose();
			}
			return result;
		}

		public object Deserialize(byte[] bytes)
		{
			object altSerializer = this._AltSerializer;
			object result;
			lock (altSerializer)
			{
				this.InitSerializer(this.DefaultSerializeFlags);
				MemoryStream memoryStream = new MemoryStream(bytes);
				this._AltSerializer.Reset();
				this._AltSerializer.Stream = memoryStream;
				result = this._AltSerializer.Deserialize();
				this._AltSerializer.Stream = this._MemStream;
				memoryStream.Dispose();
			}
			return result;
		}

		public void CacheObject(object cachedObject)
		{
			this._AltSerializer.CacheObject(cachedObject);
		}

		public void Dispose()
		{
			this.Dispose(true);
		}

		protected virtual void Dispose(bool disposeAll)
		{
			if (this._MemStream != null)
			{
				this._MemStream.Dispose();
			}
			if (this._AltSerializer != null)
			{
				this._AltSerializer.Dispose();
			}
			GC.SuppressFinalize(this);
		}

		private AltSerializer _AltSerializer;

		private MemoryStream _MemStream;

		private SerializeFlags _defaultSerializeFlags = SerializeFlags.All;
	}
}
