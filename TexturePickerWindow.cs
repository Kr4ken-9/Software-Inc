﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TexturePickerWindow : MonoBehaviour
{
	public void Show(List<Room> rooms)
	{
		this.TempUndo.Clear();
		this.Undo = new UndoObject.UndoAction(rooms, true);
		this.OutDoors = !rooms.Any((Room x) => !x.Outdoors);
		rooms.RemoveAll((Room x) => x.Outdoors ^ this.OutDoors);
		foreach (KeyValuePair<Toggle, string> keyValuePair in this.Choices)
		{
			UnityEngine.Object.Destroy(keyValuePair.Key.gameObject);
		}
		this.Chosen[0] = rooms[0].OutsideMat;
		this.Chosen[1] = rooms[0].InsideMat;
		this.Chosen[2] = rooms[0].FloorMat;
		this.Chosen[3] = rooms[0].FenceStyle;
		for (int i = 0; i < 4; i++)
		{
			this.TabToggles[i].gameObject.SetActive((this.OutDoors && i > 1) || (!this.OutDoors && i < 3));
			this.TabToggles[i].isOn = ((!this.OutDoors) ? (i == 0) : (i == 2));
		}
		this.Rooms = rooms;
		this._colors[0] = this.Rooms[0].OutsideColor;
		this._colors[1] = this.Rooms[0].InsideColor;
		this._colors[2] = this.Rooms[0].FloorColor;
		this._colors[3] = this.Rooms[0].OutsideColor;
		this.InitButtons((!this.OutDoors) ? 0 : 2);
		this.DefaultTex = new string[4, this.Rooms.Count];
		for (int j = 0; j < 4; j++)
		{
			for (int k = 0; k < this.Rooms.Count; k++)
			{
				switch (j)
				{
				case 0:
					this.DefaultTex[j, k] = this.Rooms[k].OutsideMat;
					break;
				case 1:
					this.DefaultTex[j, k] = this.Rooms[k].InsideMat;
					break;
				case 2:
					this.DefaultTex[j, k] = this.Rooms[k].FloorMat;
					break;
				case 3:
					this.DefaultTex[j, k] = this.Rooms[k].FenceStyle;
					break;
				}
			}
		}
		this.Window.OnClose = delegate
		{
			UndoObject undoObject = new UndoObject(this.TempUndo.ToArray());
			undoObject.Execute();
			this.TempUndo.Clear();
			for (int l = 0; l < this.Rooms.Count; l++)
			{
				this.Rooms[l].OutsideMat = this.DefaultTex[0, l];
				this.Rooms[l].InsideMat = this.DefaultTex[1, l];
				this.Rooms[l].FloorMat = this.DefaultTex[2, l];
				this.Rooms[l].SetFenceStyle(this.DefaultTex[3, l], null);
			}
		};
		this.Window.Show();
	}

	public void InitButtons(int choice)
	{
		foreach (KeyValuePair<Toggle, string> keyValuePair in this.Choices)
		{
			UnityEngine.Object.Destroy(keyValuePair.Key.gameObject);
		}
		this.Choices.Clear();
		if (choice < 3)
		{
			List<RoomMaterialController.WallMaterial> list = (from x in RoomMaterialController.Instance.AllMaterials.Values
			where x.Category.Equals(TexturePickerWindow.Tabs[choice]) && (this.OutDoors || (!x.Name.Equals("None") && !x.Name.Equals("CannotRent")))
			select x).ToList<RoomMaterialController.WallMaterial>();
			foreach (RoomMaterialController.WallMaterial wallMaterial in list)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.TogglePrefab);
				RawImage componentInChildren = gameObject.GetComponentInChildren<RawImage>();
				Material material = new Material(componentInChildren.material);
				material.SetColor("_Color", (!wallMaterial.Name.Equals("None")) ? this._colors[choice] : TimeOfDay.Instance.GetGroundColor());
				material.SetTexture("_MainTex", (!(wallMaterial.Base != null)) ? RoomMaterialController.Instance.DefaultBase : wallMaterial.Base);
				material.SetTexture("_BumpTex", (!(wallMaterial.Bump != null)) ? RoomMaterialController.Instance.DefaultBump : wallMaterial.Bump);
				material.SetTexture("_ExtraTex", (!(wallMaterial.Extra != null)) ? RoomMaterialController.Instance.DefaultExtra : wallMaterial.Extra);
				componentInChildren.material = material;
				gameObject.GetComponentsInChildren<Image>(true).First((Image x) => x.name.Equals("SteamIcon")).gameObject.SetActive(wallMaterial.FromSteam);
				gameObject.transform.SetParent(this.Panel.transform, false);
				Toggle component = gameObject.GetComponent<Toggle>();
				component.group = this.Panel;
				string mName = wallMaterial.Name;
				component.isOn = (mName == this.Chosen[choice]);
				component.onValueChanged.AddListener(delegate(bool x)
				{
					if (choice == 0)
					{
						this.Rooms.ForEach(delegate(Room z)
						{
							z.OutsideMat = mName;
						});
					}
					else if (choice == 1)
					{
						this.Rooms.ForEach(delegate(Room z)
						{
							z.InsideMat = mName;
						});
					}
					else
					{
						this.Rooms.ForEach(delegate(Room z)
						{
							z.FloorMat = mName;
						});
					}
					this.Chosen[choice] = mName;
				});
				this.Choices.Add(component, mName);
			}
		}
		else
		{
			foreach (ObjectDatabase.FenceStyle fenceStyle in ObjectDatabase.Instance.FenceStyles)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.TogglePrefab);
				RawImage componentInChildren2 = gameObject2.GetComponentInChildren<RawImage>();
				Material material2 = new Material(componentInChildren2.material);
				material2.SetColor("_Color", this._colors[choice]);
				material2.SetTexture("_MainTex", fenceStyle.Thumbnail);
				material2.SetTexture("_BumpTex", fenceStyle.ThumbnailBump);
				material2.SetTexture("_ExtraTex", this.DefaultFenceExtra);
				componentInChildren2.material = material2;
				gameObject2.transform.SetParent(this.Panel.transform, false);
				Toggle component2 = gameObject2.GetComponent<Toggle>();
				component2.group = this.Panel;
				string fName = fenceStyle.Name;
				component2.isOn = (fName == this.Chosen[choice]);
				component2.onValueChanged.AddListener(delegate(bool x)
				{
					UndoObject undoObject = new UndoObject(this.TempUndo.ToArray());
					undoObject.Execute();
					this.TempUndo.Clear();
					this.Rooms.ForEach(delegate(Room z)
					{
						z.SetFenceStyle(fName, this.TempUndo);
					});
					this.Chosen[choice] = fName;
				});
				this.Choices.Add(component2, fName);
			}
		}
	}

	public void Apply()
	{
		this.Window.OnClose = null;
		if (this.OutDoors)
		{
			UndoObject undoObject = new UndoObject(this.TempUndo.ToArray());
			undoObject.Execute();
			this.TempUndo.Clear();
			this.Rooms.ForEach(delegate(Room x)
			{
				x.FloorMat = this.Chosen[2];
				x.SetFenceStyle(this.Chosen[3], this.TempUndo);
			});
			GameSettings.Instance.AddUndo(this.TempUndo.ToArray());
		}
		else
		{
			this.Rooms.ForEach(delegate(Room x)
			{
				x.OutsideMat = this.Chosen[0];
				x.InsideMat = this.Chosen[1];
				x.FloorMat = this.Chosen[2];
			});
		}
		GameSettings.Instance.AddUndo(new UndoObject.UndoAction[]
		{
			this.Undo
		});
		this.Window.Close();
	}

	public static string[] Tabs = new string[]
	{
		"Exterior",
		"Interior",
		"Floor"
	};

	public GUIWindow Window;

	public ToggleGroup Panel;

	public GameObject TogglePrefab;

	public Texture DefaultFenceExtra;

	public Toggle[] TabToggles;

	public Material None;

	public Dictionary<Toggle, string> Choices = new Dictionary<Toggle, string>();

	private List<Room> Rooms = new List<Room>();

	private string[,] DefaultTex;

	private string[] Chosen = new string[]
	{
		string.Empty,
		string.Empty,
		string.Empty,
		string.Empty
	};

	[NonSerialized]
	private Color[] _colors = new Color[4];

	public bool OutDoors;

	private UndoObject.UndoAction Undo;

	private List<UndoObject.UndoAction> TempUndo = new List<UndoObject.UndoAction>();
}
