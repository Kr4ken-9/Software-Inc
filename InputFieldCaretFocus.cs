﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldCaretFocus : MonoBehaviour
{
	private void Start()
	{
		this.Input.onValueChanged.AddListener(delegate(string s)
		{
			this.DuplicateText.text = s;
		});
	}

	private void Update()
	{
		if (this.Input.caretPosition != this.LastPos)
		{
			if (this.SetCaret())
			{
				float height = this.Input.GetComponent<RectTransform>().rect.height;
				float num = Mathf.Floor(height / this.LineHeight);
				float num2 = Mathf.Ceil((this.Text.offsetMax.y + 2f) / this.LineHeight);
				int num3 = InputFieldCaretFocus.CountNewlines(this.Input.caretPosition, this.Input.text);
				if ((float)num3 <= num2)
				{
					float num4 = (float)num3 * this.LineHeight;
					this.Text.offsetMax = new Vector2(this.Text.offsetMax.x, num4 - 2f);
					this._caret.offsetMax = new Vector2(this._caret.offsetMax.x, num4 - 2f);
					int caretPosition = this.Input.caretPosition;
					this.Input.caretPosition = this.Input.text.Length;
					this.Input.caretPosition = caretPosition;
				}
				else if ((float)num3 >= num2 + num)
				{
					float num5 = ((float)num3 - num + 1f) * this.LineHeight;
					this.Text.offsetMax = new Vector2(this.Text.offsetMax.x, num5 - 2f);
					this._caret.offsetMax = new Vector2(this._caret.offsetMax.x, num5 - 2f);
				}
			}
			this.LastPos = this.Input.caretPosition;
		}
	}

	private bool SetCaret()
	{
		if (this._caret == null)
		{
			this._caret = this.Input.transform.GetChild(0).GetComponent<RectTransform>();
			this._caret.pivot = this.Text.pivot;
			return this._caret != null;
		}
		return true;
	}

	private static int CountNewlines(int p, string s)
	{
		int num = 0;
		for (int i = 0; i < Mathf.Min(s.Length, p); i++)
		{
			if (s[i] == '\n')
			{
				num++;
			}
		}
		return num;
	}

	public InputField Input;

	public RectTransform Text;

	private RectTransform _caret;

	public float LineHeight = 24f;

	private int LastPos;

	public Text DuplicateText;
}
