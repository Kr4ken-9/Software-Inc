﻿using System;
using UnityEngine;

public class MainMenuTree : MonoBehaviour
{
	private void Start()
	{
		MainMenuTree.TargetCol1 = Color.white;
		MainMenuTree.TargetCol2 = Color.white;
	}

	private void Update()
	{
		base.GetComponent<Renderer>().material.SetColor("_Color1", Color.Lerp(base.GetComponent<Renderer>().material.GetColor("_Color1"), MainMenuTree.TargetCol1, 4f * Time.deltaTime));
		base.GetComponent<Renderer>().material.SetColor("_Color2", Color.Lerp(base.GetComponent<Renderer>().material.GetColor("_Color2"), MainMenuTree.TargetCol2, 4f * Time.deltaTime));
	}

	public static Color TargetCol1;

	public static Color TargetCol2;
}
