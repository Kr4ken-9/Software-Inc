﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CopyOrderWindow : MonoBehaviour
{
	public void Show(SoftwareProduct product)
	{
		this.Product = product;
		VarValueSheet infoSheet = this.InfoSheet;
		string[] var = new string[]
		{
			"Consumer reach".Loc(),
			"Physical market share".Loc(),
			"Physical copies sold".Loc(),
			"In stock".Loc()
		};
		string[] array = new string[4];
		array[0] = product.Type.GetReach(product._category, product.Features, product.OSs).ToString("N0");
		array[1] = (MarketSimulation.GetPhysicalVsDigital(SDateTime.Now()) * 100f).ToString("N2") + "%";
		array[2] = product.GetUnitSales(false).SumSafe((int x) => x).ToString("N0");
		array[3] = product.PhysicalCopies.ToString("N0");
		infoSheet.SetData(var, array);
		this.Window.NonLocTitle = string.Format("CopiesOf".Loc(), product.Name);
		this.AmountField.text = "0";
		this.Window.Show();
		TutorialSystem.Instance.StartTutorial("Distribution", false);
	}

	public void AmountChange()
	{
		try
		{
			uint num = Convert.ToUInt32(this.AmountField.text.Replace(",", string.Empty));
			this.PriceLabel.text = (num * MarketSimulation.PhysicalCopyPrice).Currency(true);
		}
		catch (Exception)
		{
		}
	}

	public void OKClick()
	{
		try
		{
			uint num = Convert.ToUInt32(this.AmountField.text.Replace(",", string.Empty));
			float num2 = num * MarketSimulation.PhysicalCopyPrice;
			if (GameSettings.Instance.MyCompany.CanMakeTransaction(-num2))
			{
				GameSettings.Instance.MyCompany.MakeTransaction(-num2, Company.TransactionCategory.Distribution, "Copy order");
				this.Product.PhysicalCopies += num;
				this.Product.Loss += num2;
				this.Window.Close();
			}
			else
			{
				WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), false, DialogWindow.DialogType.Error);
			}
		}
		catch (Exception)
		{
			WindowManager.Instance.ShowMessageBox("InvalidAmount".Loc(), false, DialogWindow.DialogType.Error);
		}
	}

	public GUIWindow Window;

	public VarValueSheet InfoSheet;

	public Text PriceLabel;

	public InputField AmountField;

	[NonSerialized]
	public SoftwareProduct Product;
}
