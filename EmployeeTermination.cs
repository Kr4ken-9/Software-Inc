﻿using System;
using UnityEngine;

[Serializable]
public class EmployeeTermination
{
	public EmployeeTermination()
	{
	}

	public EmployeeTermination(Actor actor, EmployeeTermination.TerminationType type, float payout)
	{
		this.Name = actor.employee.ExtraName;
		this.Team = actor.Team;
		this.Role = actor.employee.CurrentRoleBit;
		this.YearsHired = Mathf.Round(Utilities.GetMonths(actor.employee.Hired, SDateTime.Now()) / 12f * 4f) / 4f;
		this.Payout = payout;
		this.Termination = type;
		this.Date = SDateTime.Now();
	}

	public string Name;

	public string Team;

	public Employee.RoleBit Role;

	public float YearsHired;

	public float Payout;

	public EmployeeTermination.TerminationType Termination;

	public SDateTime Date;

	public enum TerminationType
	{
		Dead,
		Hospitalized,
		Retired
	}
}
