﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ServerWindow : MonoBehaviour
{
	public void SetName()
	{
		if (this.ServerList.Selected.Count > 0 && this.InputName.text.Length > 0 && !GameSettings.Instance.GetServerNames().Contains(this.InputName.text))
		{
			GameSettings.Instance.ChangeServerName(this.ServerList.GetSelected<Server>()[0].ServerName, this.InputName.text);
			this.DoUpdateServerList();
		}
	}

	public void Unsupported()
	{
		this.ItemList.Items.Clear();
		this.ItemList.Items.AddRange(GameSettings.Instance.UnsupportedServerItems.Cast<object>());
	}

	public void CancelSelected()
	{
		foreach (IServerItem item in this.ItemList.GetSelected<IServerItem>())
		{
			GameSettings.Instance.RegisterWithServer(null, item, false);
			this.ItemList.Items.Remove(item);
			this.ItemList.Selected.Clear();
			if (this.ItemList.Items.Count > 0)
			{
				this.ItemList.Selected.Add(0);
				this.ItemList.SaveSelected();
			}
		}
	}

	public void DelegateSelected()
	{
		if (this.ItemList.Selected.Count == 0)
		{
			return;
		}
		string[] servers = GameSettings.Instance.GetServerNames();
		SelectorController.Instance.selectWindow.Show("Server", servers, delegate(int i)
		{
			if (i == -1)
			{
				this.CancelSelected();
				return;
			}
			string text = servers[i];
			if (!string.IsNullOrEmpty(text))
			{
				bool flag = false;
				Server server = GameSettings.Instance.GetServer(text);
				foreach (IServerItem item in this.ItemList.GetSelected<IServerItem>())
				{
					if (server.items.Contains(item))
					{
						flag = true;
					}
					else
					{
						GameSettings.Instance.RegisterWithServer(text, item, false);
						this.ItemList.Items.Remove(item);
						this.ItemList.Selected.Clear();
						if (this.ItemList.Items.Count > 0)
						{
							this.ItemList.Selected.Add(0);
							this.ItemList.SaveSelected();
						}
					}
				}
				if (flag)
				{
					WindowManager.Instance.ShowMessageBox("ServerDelegateError".Loc(), true, DialogWindow.DialogType.Error);
				}
			}
		}, true, true, true, false, null);
	}

	private void Start()
	{
		this.ServerList.OnSelectChange = delegate(bool d)
		{
			if (this.ServerList.Selected.Count > 0)
			{
				Server server = this.ServerList.GetSelected<Server>()[0];
				this.ItemList.Items.Clear();
				this.ItemList.Items.AddRange(server.Items.Cast<object>());
				this.InputName.text = server.ServerName;
			}
			else
			{
				this.InputName.text = string.Empty;
			}
		};
		this.DoUpdateServerList();
		this.UpdateServerWarning();
	}

	public void UpdateServerList()
	{
		this.ShouldUpdateServerList = true;
	}

	public void DoUpdateServerList()
	{
		this.ShouldUpdateServerList = false;
		this.ServerList.Items.Clear();
		this.ServerList.Items.AddRange(GameSettings.Instance.GetAllServersQuick().Values.Cast<object>().ToList<object>());
		this.ServerList.Selected.Clear();
		this.UpdateItems();
	}

	public void UpdateServerWarning()
	{
		this.UnsupportButton.color = ((GameSettings.Instance.UnsupportedServerItems.Count <= 0) ? Color.white : Color.red);
		this.UnsupportText.text = string.Concat(new object[]
		{
			"UnsupportedProc".Loc(),
			" (",
			GameSettings.Instance.UnsupportedServerItems.Count,
			")"
		});
		if (HUD.Instance != null)
		{
			HUD.Instance.UpdateServerWarning();
		}
	}

	public void UpdateItems()
	{
		if (this.ServerList.Selected.Count > 0)
		{
			this.ItemList.Items.Clear();
			this.ItemList.Items.AddRange(this.ServerList.GetSelected<IServerHost>()[0].Items.Cast<object>());
		}
		else
		{
			this.ItemList.Selected.Clear();
			this.ItemList.Items.Clear();
		}
	}

	public GUIWindow Window;

	public GUIListView ServerList;

	public GUIListView ItemList;

	public Image UnsupportButton;

	public Text UnsupportText;

	public InputField InputName;

	[NonSerialized]
	public bool ShouldUpdateServerList;
}
