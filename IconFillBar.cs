﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class IconFillBar : MaskableGraphic, IPointerDownHandler, IEventSystemHandler
{
	public override Texture mainTexture
	{
		get
		{
			return this.IconTexture;
		}
	}

	public float this[int i]
	{
		get
		{
			return this.Values[i];
		}
		set
		{
			this.Values[i] = value;
			this.SetVerticesDirty();
		}
	}

	protected override void OnPopulateMesh(VertexHelper vh)
	{
		vh.Clear();
		int num = Mathf.Min(this.Values.Count, this.Colors.Count);
		float a = 0f;
		Vector2 a2 = new Vector2(-base.rectTransform.pivot.x * base.rectTransform.rect.width, (1f - base.rectTransform.pivot.y) * base.rectTransform.rect.height);
		if (this.Highlight)
		{
			Rect rect = new Rect(a2.x, a2.y - base.rectTransform.rect.height, base.rectTransform.rect.width, base.rectTransform.rect.height);
			Color c = new Color(1f, 1f, 1f, 0.8f);
			vh.AddUIVertexQuad(new UIVertex[]
			{
				new UIVertex
				{
					color = c,
					position = new Vector3(rect.xMin, rect.yMax),
					uv0 = this.HightlightUV
				},
				new UIVertex
				{
					color = c,
					position = new Vector3(rect.xMax, rect.yMax),
					uv0 = this.HightlightUV
				},
				new UIVertex
				{
					color = c,
					position = new Vector3(rect.xMax, rect.yMin),
					uv0 = this.HightlightUV
				},
				new UIVertex
				{
					color = c,
					position = new Vector3(rect.xMin, rect.yMin),
					uv0 = this.HightlightUV
				}
			});
		}
		a2 += this.Offset;
		if (this._cutOff.Count < num + 1)
		{
			for (int i = this._cutOff.Count; i < num + 1; i++)
			{
				this._cutOff.Add(0f);
			}
		}
		for (int j = num - 1; j > -1; j--)
		{
			float num2 = Mathf.Max(a, this.Values[j]);
			this._cutOff[j] = num2;
			a = num2;
		}
		for (int k = 0; k < num; k++)
		{
			float num3 = this._cutOff[k + 1];
			float num4 = this._cutOff[k];
			if (!Mathf.Approximately(num3, num4))
			{
				int num5 = Mathf.CeilToInt(num3);
				int num6 = Mathf.FloorToInt(num3);
				int num7 = Mathf.FloorToInt(num4);
				float num8 = num3 - (float)num6;
				float num9 = num4 - (float)num7;
				if (num6 == num7)
				{
					if (this.Fade && k == num - 1)
					{
						this.DrawIcon(a2.x + (float)num6 * (this.IconSize.x + this.IconSpacing), a2.y, num8, 1f, this.Colors[k] * new Color(1f, 1f, 1f, num9), vh);
					}
					else
					{
						this.DrawIcon(a2.x + (float)num6 * (this.IconSize.x + this.IconSpacing), a2.y, num8, num9, this.Colors[k], vh);
					}
				}
				else
				{
					if (!Mathf.Approximately(num8, 0f))
					{
						this.DrawIcon(a2.x + (float)num6 * (this.IconSize.x + this.IconSpacing), a2.y, num8, 1f, this.Colors[k], vh);
					}
					for (int l = num5; l < num7; l++)
					{
						this.DrawIcon(a2.x + (float)l * (this.IconSize.x + this.IconSpacing), a2.y, 0f, 1f, this.Colors[k], vh);
					}
					if (!Mathf.Approximately(num9, 0f))
					{
						if (this.Fade && k == num - 1)
						{
							this.DrawIcon(a2.x + (float)num7 * (this.IconSize.x + this.IconSpacing), a2.y, 0f, 1f, this.Colors[k] * new Color(1f, 1f, 1f, num9), vh);
						}
						else
						{
							this.DrawIcon(a2.x + (float)num7 * (this.IconSize.x + this.IconSpacing), a2.y, 0f, num9, this.Colors[k], vh);
						}
					}
				}
			}
		}
	}

	private void DrawIcon(float x, float y, float startP, float endP, Color col, VertexHelper vh)
	{
		Color c = col * this.color;
		vh.AddUIVertexQuad(new UIVertex[]
		{
			new UIVertex
			{
				position = new Vector3(x + startP * this.IconSize.x, y, 0f),
				uv0 = new Vector2(startP, 1f),
				color = c
			},
			new UIVertex
			{
				position = new Vector3(x + endP * this.IconSize.x, y, 0f),
				uv0 = new Vector2(endP, 1f),
				color = c
			},
			new UIVertex
			{
				position = new Vector3(x + endP * this.IconSize.x, y - this.IconSize.y, 0f),
				uv0 = new Vector2(endP, 0f),
				color = c
			},
			new UIVertex
			{
				position = new Vector3(x + startP * this.IconSize.x, y - this.IconSize.y, 0f),
				uv0 = new Vector2(startP, 0f),
				color = c
			}
		});
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		this.OnClick.Invoke();
	}

	public Texture IconTexture;

	public Vector2 IconSize = new Vector2(16f, 16f);

	public Vector2 Offset;

	public float IconSpacing = 4f;

	public bool Fade;

	public List<float> Values = new List<float>
	{
		6f,
		3f
	};

	public List<Color> Colors = new List<Color>
	{
		Color.gray,
		Color.white
	};

	[NonSerialized]
	private List<float> _cutOff = new List<float>();

	public bool Highlight;

	public Vector2 HightlightUV;

	public UnityEvent OnClick;
}
