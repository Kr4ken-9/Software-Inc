﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ITAI : AI<Actor>
{
	public override void InitTree()
	{
		string name = "Spawn";
		if (ITAI.<>f__mg$cache0 == null)
		{
			ITAI.<>f__mg$cache0 = new Func<Actor, int>(ITAI.Spawn);
		}
		BehaviorNode<Actor> behaviorNode = new BehaviorNode<Actor>(name, ITAI.<>f__mg$cache0, true, -1);
		string name2 = "FindRepair";
		if (ITAI.<>f__mg$cache1 == null)
		{
			ITAI.<>f__mg$cache1 = new Func<Actor, int>(ITAI.FindRepair);
		}
		BehaviorNode<Actor> behaviorNode2 = new BehaviorNode<Actor>(name2, ITAI.<>f__mg$cache1, false, -1);
		string name3 = "Loiter";
		if (ITAI.<>f__mg$cache2 == null)
		{
			ITAI.<>f__mg$cache2 = new Func<Actor, int>(ITAI.Loiter);
		}
		BehaviorNode<Actor> behaviorNode3 = new BehaviorNode<Actor>(name3, ITAI.<>f__mg$cache2, true, 1);
		string name4 = "GoHome";
		if (ITAI.<>f__mg$cache3 == null)
		{
			ITAI.<>f__mg$cache3 = new Func<Actor, int>(Actor.GoHomeState);
		}
		BehaviorNode<Actor> behaviorNode4 = new BehaviorNode<Actor>(name4, ITAI.<>f__mg$cache3, true, -1);
		string name5 = "Despawn";
		if (ITAI.<>f__mg$cache4 == null)
		{
			ITAI.<>f__mg$cache4 = new Func<Actor, int>(ITAI.Despawn);
		}
		BehaviorNode<Actor> behaviorNode5 = new BehaviorNode<Actor>(name5, ITAI.<>f__mg$cache4, true, -1);
		string name6 = "GoToRepair";
		if (ITAI.<>f__mg$cache5 == null)
		{
			ITAI.<>f__mg$cache5 = new Func<Actor, int>(ITAI.GoToRepair);
		}
		BehaviorNode<Actor> behaviorNode6 = new BehaviorNode<Actor>(name6, ITAI.<>f__mg$cache5, true, -1);
		string name7 = "Repair";
		if (ITAI.<>f__mg$cache6 == null)
		{
			ITAI.<>f__mg$cache6 = new Func<Actor, int>(ITAI.Repair);
		}
		BehaviorNode<Actor> behaviorNode7 = new BehaviorNode<Actor>(name7, ITAI.<>f__mg$cache6, true, -1);
		string name8 = "IsOff";
		if (ITAI.<>f__mg$cache7 == null)
		{
			ITAI.<>f__mg$cache7 = new Func<Actor, int>(ITAI.IsOff);
		}
		BehaviorNode<Actor> behaviorNode8 = new BehaviorNode<Actor>(name8, ITAI.<>f__mg$cache7, false, -1);
		string name9 = "GoHomeBusStop";
		if (ITAI.<>f__mg$cache8 == null)
		{
			ITAI.<>f__mg$cache8 = new Func<Actor, int>(Actor.GoHomeBusStop);
		}
		BehaviorNode<Actor> behaviorNode9 = new BehaviorNode<Actor>(name9, ITAI.<>f__mg$cache8, true, -1);
		string name10 = "ShouldUseBus";
		if (ITAI.<>f__mg$cache9 == null)
		{
			ITAI.<>f__mg$cache9 = new Func<Actor, int>(Actor.ShouldUseBus);
		}
		BehaviorNode<Actor> behaviorNode10 = new BehaviorNode<Actor>(name10, ITAI.<>f__mg$cache9, true, -1);
		this.BehaviorNodes.Add("Spawn", behaviorNode);
		this.BehaviorNodes.Add("FindRepair", behaviorNode2);
		this.BehaviorNodes.Add("Loiter", behaviorNode3);
		this.BehaviorNodes.Add("GoHome", behaviorNode4);
		this.BehaviorNodes.Add("Despawn", behaviorNode5);
		this.BehaviorNodes.Add("GoToRepair", behaviorNode6);
		this.BehaviorNodes.Add("Repair", behaviorNode7);
		this.BehaviorNodes.Add("GoHomeBusStop", behaviorNode9);
		this.BehaviorNodes.Add("ShouldUseBus", behaviorNode10);
		behaviorNode.Success = behaviorNode2;
		behaviorNode8.Success = behaviorNode10;
		behaviorNode8.Failure = behaviorNode2;
		behaviorNode2.Success = behaviorNode6;
		behaviorNode2.Failure = behaviorNode3;
		behaviorNode6.Success = behaviorNode7;
		behaviorNode6.Failure = behaviorNode3;
		behaviorNode7.Success = behaviorNode8;
		behaviorNode4.Success = behaviorNode5;
		behaviorNode4.Failure = behaviorNode9;
		behaviorNode5.Success = AI<Actor>.DummyNode;
		behaviorNode3.Success = behaviorNode8;
		behaviorNode10.Success = behaviorNode9;
		behaviorNode10.Failure = behaviorNode4;
		behaviorNode9.Success = behaviorNode5;
		behaviorNode9.Failure = behaviorNode3;
	}

	private static int Spawn(Actor self)
	{
		if (self.CurrentPath == null)
		{
			return 2;
		}
		return (!self.WalkPath()) ? 1 : 2;
	}

	private static int FindRepair(Actor self)
	{
		if (self.LastCheckWait > 0f)
		{
			return 0;
		}
		if (self.employee.Fired)
		{
			self.CurrentPath = null;
			return 0;
		}
		IEnumerable<Room> enumerable;
		if (self.HasAssignedRooms)
		{
			enumerable = self.GetAssignedRooms();
		}
		else if (self.currentRoom == GameSettings.Instance.sRoomManager.Outside)
		{
			enumerable = from x in GameSettings.Instance.sRoomManager.Rooms
			orderby (from y in x.GetFurnitures()
			where y.ITFix
			select y.HasUpg ? y.upg.Quality : 1f).MinOrDefault(1f)
			select x;
		}
		else
		{
			enumerable = from x in GameSettings.Instance.sRoomManager.GetConnectedRooms(self.currentRoom)
			select x.Key;
		}
		IEnumerable<Room> enumerable2 = enumerable;
		bool flag = true;
		List<Furniture> list = new List<Furniture>();
		foreach (Room room in enumerable2)
		{
			if (!room.NavmeshRebuildStarted)
			{
				List<Furniture> furnitures = room.GetFurnitures();
				for (int i = 0; i < furnitures.Count; i++)
				{
					Furniture furniture = furnitures[i];
					if (furniture != null && furniture.HasUpg && furniture.ITFix && furniture.upg.Quality < 0.8f && GameSettings.Instance.MyCompany.CanMakeTransaction((1f - furniture.upg.Quality) * -500f) && furniture.TestAvailable(false, "Repair"))
					{
						list.Add(furniture);
					}
				}
				if (list.Count > 0)
				{
					foreach (Furniture furniture2 in from x in list
					orderby x.PathFailCount, x.upg.Quality
					select x)
					{
						flag = false;
						InteractionPoint interactionPoint = furniture2.GetInteractionPoint(self, "Repair");
						if (interactionPoint != null)
						{
							InteractionPoint interactionPoint2 = furniture2.GetInteractionPoint("Use", true);
							if (interactionPoint2 != null && interactionPoint2.UsedBy != null)
							{
								if ((!self.OnCall && furniture2.upg.Quality > 0.15f) || (self.OnCall && furniture2.upg.Quality > 0.5f))
								{
									continue;
								}
								Actor usedBy = interactionPoint2.UsedBy;
								usedBy.UsingPoint = null;
								usedBy.AIScript.currentNode = usedBy.AIScript.BehaviorNodes["Loiter"];
							}
							if (self.PathToFurniture(interactionPoint, true))
							{
								self.FreeLoiterTable();
								GameSettings.Instance.MyCompany.MakeTransaction((1f - furniture2.upg.Quality) * -500f, Company.TransactionCategory.Repairs, "IT");
								return 2;
							}
							HUD.Instance.AddPopupMessage("FurnitureBlockedStaff".Loc(), "Exclamation", PopupManager.PopUpAction.GotoFurn, furniture2.DID, PopupManager.NotificationSound.Issue, 1f, PopupManager.PopupIDs.BlockedOff, 1);
						}
					}
				}
				list.Clear();
			}
		}
		if (self.OnCall && flag)
		{
			self.GoHomeNow = true;
		}
		self.LastCheckWait = 30f;
		return 0;
	}

	private static int IsOff(Actor self)
	{
		if (self.GoHomeNow)
		{
			self.FreeLoiterTable();
			if (self.UsingPoint != null)
			{
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
			return 2;
		}
		return 0;
	}

	private static int Loiter(Actor self)
	{
		if (self.GoHomeNow)
		{
			return 2;
		}
		return self.HandleLoiter(false);
	}

	private static int Despawn(Actor self)
	{
		GameSettings.Instance.StaffSalaryDue += self.GetRealSalary() / (float)GameSettings.DaysPerMonth;
		if (self.OnCall)
		{
			UnityEngine.Object.Destroy(self.gameObject);
		}
		else
		{
			SDateTime sdateTime = SDateTime.Now();
			GameSettings.Instance.sActorManager.AddToAwaiting(self, new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), self.StaffOn - 1, sdateTime.Day + ((self.StaffOn <= TimeOfDay.Instance.Hour) ? 1 : 0), sdateTime.Month, sdateTime.Year), false, true);
			self.OnDespawn();
		}
		return 2;
	}

	private static int GoToRepair(Actor self)
	{
		foreach (InteractionPoint interactionPoint in self.InQueue.Values)
		{
			if (interactionPoint != null)
			{
				interactionPoint.RemoveFromQueue(self);
			}
		}
		self.InQueue.Clear();
		if (self.UsingPoint != null)
		{
			bool flag = self.WalkPath();
			if (flag)
			{
				self.TurnToFurniture();
			}
			return (!flag) ? 1 : 2;
		}
		self.anim.SetInteger("AnimControl", 0);
		self.CurrentPath = null;
		return 0;
	}

	private static int Repair(Actor self)
	{
		if (self.UsingPoint == null)
		{
			return 2;
		}
		self.anim.SetEnum("AnimControl", self.UsingPoint.Animation);
		self.anim.SetInteger("SubAnim", self.UsingPoint.subAnimation);
		bool flag = self.UsingPoint.Parent.upg.RepairMe();
		if (flag)
		{
			self.UsingPoint = null;
			return 2;
		}
		return 1;
	}

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache5;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache6;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache7;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache8;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache9;
}
