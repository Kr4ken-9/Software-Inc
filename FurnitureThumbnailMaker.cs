﻿using System;
using System.IO;
using System.Linq;
using DevConsole;
using UnityEngine;

public class FurnitureThumbnailMaker : MonoBehaviour
{
	private void Awake()
	{
		FurnitureThumbnailMaker.Instance = this;
		this.ThumbnailCam.targetTexture = this.Target;
	}

	private void OnDestroy()
	{
		FurnitureThumbnailMaker.Instance = null;
	}

	public Vector3[] GetMinMax(GameObject obj)
	{
		Vector3 vector = Vector3.one * 500f;
		Vector3 vector2 = Vector3.one * -500f;
		MeshFilter[] componentsInChildren = obj.GetComponentsInChildren<MeshFilter>(true);
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			MeshFilter meshFilter = componentsInChildren[i];
			Renderer component = meshFilter.GetComponent<Renderer>();
			if (!(component == null) && component.enabled)
			{
				MeshFilter localItem = meshFilter;
				foreach (Vector3 v in from x in meshFilter.mesh.vertices
				select localItem.transform.localToWorldMatrix.MultiplyPoint(x))
				{
					vector = Utilities.MinVector(vector, v);
					vector2 = Utilities.MaxVector(vector2, v);
				}
			}
		}
		return new Vector3[]
		{
			vector,
			vector2
		};
	}

	private void Preview(GameObject activeObject)
	{
		Vector3[] minMax = this.GetMinMax(activeObject);
		activeObject.transform.localPosition = -((minMax[0] + minMax[1]) * 0.5f);
		this.ThumbnailCam.orthographicSize = (minMax[0] - minMax[1]).magnitude / 2f;
		this.ThumbnailCam.Render();
	}

	public void TakePicture(string furnitureName)
	{
		GameObject furniture = ObjectDatabase.Instance.GetFurniture(furnitureName);
		if (furniture != null)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(furniture);
			gameObject.transform.SetParent(base.transform);
			gameObject.transform.rotation = Quaternion.identity;
			gameObject.name = furniture.name;
			gameObject.GetComponent<Furniture>().isTemporary = true;
			gameObject.GetComponent<Furniture>().ForceTemporary();
			gameObject.SetActive(true);
			foreach (Transform transform in gameObject.GetComponentsInChildren<Transform>())
			{
				transform.gameObject.layer = 9;
			}
			string path = "Furniture/" + furniture.name.Replace(" ", string.Empty) + "Thumb.png";
			this.light.enabled = true;
			this.Preview(gameObject);
			RenderTexture renderTexture = new RenderTexture(this.Target.width / 2, this.Target.height / 2, 16, RenderTextureFormat.ARGB32);
			RenderTexture active = RenderTexture.active;
			RenderTexture.active = renderTexture;
			this.BlitMat.SetFloat("_inputSize", 256f);
			Graphics.Blit(this.Target, renderTexture, this.BlitMat);
			Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
			texture2D.ReadPixels(new Rect(0f, 0f, (float)renderTexture.width, (float)renderTexture.height), 0, 0);
			texture2D.Apply();
			RenderTexture.active = active;
			UnityEngine.Object.Destroy(renderTexture);
			File.WriteAllBytes(path, texture2D.EncodeToPNG());
			UnityEngine.Object.Destroy(gameObject);
			UnityEngine.Object.Destroy(texture2D);
			this.light.enabled = false;
			DevConsole.Console.Log("Thumbnail saved as " + Path.GetFullPath(path));
		}
		else
		{
			DevConsole.Console.Log("Furniture not found");
		}
	}

	public static FurnitureThumbnailMaker Instance;

	public Camera ThumbnailCam;

	public RenderTexture Target;

	public Material BlitMat;

	public Light light;
}
