﻿using System;
using UnityEngine;

public class CensorCamera : MonoBehaviour
{
	public static void Refresh()
	{
		if (CensorCamera.Instance != null)
		{
			if (Options.Frosted)
			{
				CensorCamera.Instance.Mat2.SetFloat("_AlphaFactor", 1f);
			}
			else
			{
				CensorCamera.Instance.Mat2.SetFloat("_AlphaFactor", 0f);
			}
		}
	}

	private void Awake()
	{
		float num = (float)Screen.width / (float)Screen.height;
		this.tex = new RenderTexture(Mathf.RoundToInt(64f * num), 64, 24, RenderTextureFormat.ARGB32);
		this.tex.autoGenerateMips = false;
		this.tex.filterMode = FilterMode.Point;
		this.tex.wrapMode = TextureWrapMode.Clamp;
		this.Mat.mainTexture = this.tex;
		this.tex3 = new RenderTexture(Mathf.RoundToInt(256f * num), 256, 24, RenderTextureFormat.ARGB32);
		this.tex3.autoGenerateMips = false;
		this.tex3.filterMode = FilterMode.Bilinear;
		this.tex3.wrapMode = TextureWrapMode.Clamp;
		CensorCamera.Instance = this;
		CensorCamera.Refresh();
	}

	private void OnDestroy()
	{
		CensorCamera.Instance = null;
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Graphics.Blit(source, this.tex);
		if (Options.Frosted)
		{
			this.BlurMaterial.SetVector("_Parameter", new Vector4(this.BlurSize, -this.BlurSize, 0f, 0f));
			Graphics.Blit(source, this.tex2, this.BlurMaterial, 0);
			Graphics.Blit(this.tex2, this.tex3, this.BlurMaterial, 1);
			Graphics.Blit(this.tex3, this.tex2, this.BlurMaterial, 2);
		}
		Graphics.Blit(source, destination);
	}

	private RenderTexture tex;

	private RenderTexture tex3;

	public RenderTexture tex2;

	public Texture2D White;

	public Material Mat;

	public Material Mat2;

	public Material BlurMaterial;

	public float BlurSize;

	public static CensorCamera Instance;
}
