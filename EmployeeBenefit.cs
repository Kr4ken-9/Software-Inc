﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class EmployeeBenefit
{
	public EmployeeBenefit(string name, float min, float max, float increment, float defaultVal, Func<float, string> valToText, string units, float weight, float baseLine = -100f, Action<Actor, float, float> onChange = null)
	{
		this.Name = name;
		this.Min = min;
		this.Max = max;
		this.Increment = increment;
		this.Default = defaultVal;
		this.Weight = weight;
		this.ValueToText = valToText;
		this.OnChange = onChange;
		this.Units = units;
		this.Baseline = Mathf.Max(this.Min, baseLine);
	}

	public EmployeeBenefit(string name, float min, float max, float increment, float defaultVal, Func<float, string> valToText, string units, Func<Employee, float> weighting, float baseLine = -100f, Action<Actor, float, float> onChange = null)
	{
		this.Name = name;
		this.Min = min;
		this.Max = max;
		this.Increment = increment;
		this.Default = defaultVal;
		this._weighting = weighting;
		this.ValueToText = valToText;
		this.OnChange = onChange;
		this.Units = units;
		this.Baseline = Mathf.Max(this.Min, baseLine);
	}

	public float GetScore(float value)
	{
		if (value < this.Baseline)
		{
			return -(this.Baseline - value) / (this.Baseline - this.Min);
		}
		return (value - this.Min) / (this.Max - this.Min);
	}

	public float GetWeight(Employee emp)
	{
		return (this._weighting == null) ? this.Weight : this._weighting(emp);
	}

	public string AddPost(string input)
	{
		if (this.Units == null)
		{
			return input;
		}
		return input + "/" + this.Units.Loc().ToLower();
	}

	private static void CompanyCarChange(Actor emp, float before, float after)
	{
		GameSettings.Instance.MyCompany.MakeTransaction(EmployeeBenefit.CompanyCarPrice[(int)before] * 0.5f, Company.TransactionCategory.Benefits, "Company car");
		GameSettings.Instance.MyCompany.MakeTransaction(-EmployeeBenefit.CompanyCarPrice[(int)after], Company.TransactionCategory.Benefits, "Company car");
	}

	private static void ChristmasBonusChange(Actor emp, float before, float after)
	{
		emp.ChristmasBonus = Mathf.Max(emp.ChristmasBonus, after);
	}

	public static float GetBenefitValue(Dictionary<string, float> benefits, string benefit)
	{
		if (benefits == null)
		{
			return EmployeeBenefit.Benefits[benefit].Default;
		}
		return benefits.GetOrDefault(benefit, (benefits != GameSettings.Instance.CompanyBenefits) ? EmployeeBenefit.GetBenefitValue(GameSettings.Instance.CompanyBenefits, benefit) : EmployeeBenefit.Benefits[benefit].Default);
	}

	public static Dictionary<string, float> GetDefaultBenefits()
	{
		return EmployeeBenefit.Benefits.ToDictionary((KeyValuePair<string, EmployeeBenefit> x) => x.Key, (KeyValuePair<string, EmployeeBenefit> x) => x.Value.Default);
	}

	private static float AgeWeight(Employee emp, float bestAge)
	{
		float age = emp.Age;
		float num = Mathf.Min(age, (float)Employee.Youngest);
		float num2 = Mathf.Max(age, (float)Employee.RetirementAge);
		float x = Mathf.Sqrt(1f - Mathf.Abs(bestAge - age) / (num2 - num));
		return x.MapRange(0f, 1f, 0.25f, 1f, false);
	}

	static EmployeeBenefit()
	{
		// Note: this type is marked as 'beforefieldinit'.
		Dictionary<string, EmployeeBenefit> dictionary = new Dictionary<string, EmployeeBenefit>();
		dictionary.Add("Pension", new EmployeeBenefit("Pension", 0f, 200f, 10f, 0f, (float x) => x.Currency(true), "Month", (Employee x) => EmployeeBenefit.AgeWeight(x, 20f) * 5f, -100f, null));
		dictionary.Add("Life insurance", new EmployeeBenefit("Life insurance", 0f, 200000f, 1000f, 0f, (float x) => x.Currency(true), "Death", (Employee x) => EmployeeBenefit.AgeWeight(x, 65f) * 2f, -100f, null));
		dictionary.Add("Health insurance", new EmployeeBenefit("Health insurance", 0f, 100000f, 1000f, 0f, (float x) => x.Currency(true), "Hospitalization", (Employee x) => EmployeeBenefit.AgeWeight(x, 42f), -100f, null));
		dictionary.Add("Minimum raise", new EmployeeBenefit("Minimum raise", 0f, 500f, 50f, 0f, (float x) => x.Currency(true), "Year", (Employee x) => Employee.AverageWage / x.Salary * 8f, -100f, null));
		Dictionary<string, EmployeeBenefit> dictionary2 = dictionary;
		string key = "Severance pay";
		string name = "Severance pay";
		float min = 0f;
		float max = 2f;
		float increment = 0.25f;
		float defaultVal = 0f;
		if (EmployeeBenefit.<>f__mg$cache0 == null)
		{
			EmployeeBenefit.<>f__mg$cache0 = new Func<float, string>(Utilities.ToPercent);
		}
		dictionary2.Add(key, new EmployeeBenefit(name, min, max, increment, defaultVal, EmployeeBenefit.<>f__mg$cache0, "Termination", 3f, -100f, null));
		dictionary.Add("Vacation months", new EmployeeBenefit("Vacation months", 0f, 3f, 1f, 1f, (float x) => x.ToString("N0"), null, 16f, 1f, null));
		Dictionary<string, EmployeeBenefit> dictionary3 = dictionary;
		string key2 = "Paid vacation";
		string name2 = "Paid vacation";
		float min2 = 0f;
		float max2 = 1f;
		float increment2 = 0.25f;
		float defaultVal2 = 1f;
		if (EmployeeBenefit.<>f__mg$cache1 == null)
		{
			EmployeeBenefit.<>f__mg$cache1 = new Func<float, string>(Utilities.ToPercent);
		}
		dictionary3.Add(key2, new EmployeeBenefit(name2, min2, max2, increment2, defaultVal2, EmployeeBenefit.<>f__mg$cache1, null, (Employee x) => x.GetBenefitValue("Vacation months") * 4f, 0.5f, null));
		dictionary.Add("Free food", new EmployeeBenefit("Free food", 0f, 1f, 1f, 1f, (float x) => (x <= 0.5f) ? "No".Loc() : "Yes".Loc(), null, 4f, -100f, null));
		Dictionary<string, EmployeeBenefit> dictionary4 = dictionary;
		string key3 = "Christmas bonus";
		string name3 = "Christmas bonus";
		float min3 = 0f;
		float max3 = 1f;
		float increment3 = 0.25f;
		float defaultVal3 = 0f;
		if (EmployeeBenefit.<>f__mg$cache2 == null)
		{
			EmployeeBenefit.<>f__mg$cache2 = new Func<float, string>(Utilities.ToPercent);
		}
		Func<float, string> valToText = EmployeeBenefit.<>f__mg$cache2;
		string units = "Employee";
		float weight = 8f;
		float baseLine = 0f;
		if (EmployeeBenefit.<>f__mg$cache3 == null)
		{
			EmployeeBenefit.<>f__mg$cache3 = new Action<Actor, float, float>(EmployeeBenefit.ChristmasBonusChange);
		}
		dictionary4.Add(key3, new EmployeeBenefit(name3, min3, max3, increment3, defaultVal3, valToText, units, weight, baseLine, EmployeeBenefit.<>f__mg$cache3));
		Dictionary<string, EmployeeBenefit> dictionary5 = dictionary;
		string key4 = "Company car";
		string name4 = "Company car";
		float min4 = 0f;
		float max4 = 2f;
		float increment4 = 1f;
		float defaultVal4 = 0f;
		Func<float, string> valToText2 = (float x) => EmployeeBenefit.CompanyCarPrice[(int)x].Currency(true);
		string units2 = "Employee";
		float weight2 = 16f;
		float baseLine2 = 0f;
		if (EmployeeBenefit.<>f__mg$cache4 == null)
		{
			EmployeeBenefit.<>f__mg$cache4 = new Action<Actor, float, float>(EmployeeBenefit.CompanyCarChange);
		}
		dictionary5.Add(key4, new EmployeeBenefit(name4, min4, max4, increment4, defaultVal4, valToText2, units2, weight2, baseLine2, EmployeeBenefit.<>f__mg$cache4));
		EmployeeBenefit.Benefits = dictionary;
	}

	public string Name;

	public string Units;

	public float Min;

	public float Max;

	public float Increment;

	public float Default;

	public float Weight;

	public float Baseline;

	private Func<Employee, float> _weighting;

	public Func<float, string> ValueToText;

	public Action<Actor, float, float> OnChange;

	public static float[] CompanyCarPrice = new float[]
	{
		0f,
		4000f,
		25000f
	};

	public static float MaxBenefits = 75f;

	public static Dictionary<string, EmployeeBenefit> Benefits;

	[CompilerGenerated]
	private static Func<float, string> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<float, string> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<float, string> <>f__mg$cache2;

	[CompilerGenerated]
	private static Action<Actor, float, float> <>f__mg$cache3;

	[CompilerGenerated]
	private static Action<Actor, float, float> <>f__mg$cache4;
}
