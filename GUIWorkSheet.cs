﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GUIWorkSheet : MonoBehaviour
{
	public Text this[int i, int j]
	{
		get
		{
			return this.CellText[i, j];
		}
	}

	private void Start()
	{
		this.rect = base.GetComponent<RectTransform>();
		this.layout = base.GetComponent<GridLayoutGroup>();
		if (!this.ManualInit)
		{
			this.Init();
		}
	}

	public void Initialize()
	{
		if (this.ManualInit)
		{
			this.Init();
		}
	}

	public void MarkRow(int r, Color color)
	{
		for (int i = 0; i < this.Width; i++)
		{
			this.CellImage[i, r].color = color;
			this.CellText[i, r].fontSize = 16;
		}
	}

	public void IndentCell(int x, int y, float amount)
	{
		RectTransform component = this.CellText[x, y].GetComponent<RectTransform>();
		component.anchoredPosition = new Vector2(amount, component.anchoredPosition.y);
	}

	private void Init()
	{
		this.CellText = new Text[this.Width, this.Height];
		this.CellImage = new Image[this.Width, this.Height];
		for (int i = 0; i < this.Height; i++)
		{
			for (int j = 0; j < this.Width; j++)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.CellPrefab);
				Text componentInChildren = gameObject.GetComponentInChildren<Text>();
				Image component = gameObject.GetComponent<Image>();
				if (i % 2 == 1)
				{
					component.color = new Color(1f, 1f, 1f, 0.5f);
				}
				componentInChildren.text = string.Empty;
				gameObject.transform.SetParent(base.transform, false);
				this.CellText[j, i] = componentInChildren;
				this.CellImage[j, i] = component;
			}
		}
	}

	private void Update()
	{
		this.layout.cellSize = new Vector2(this.rect.rect.width / (float)this.Width, this.rect.rect.height / (float)this.Height);
	}

	public int Width;

	public int Height;

	public GameObject CellPrefab;

	public Text[,] CellText;

	public Image[,] CellImage;

	public bool ManualInit;

	private GridLayoutGroup layout;

	private RectTransform rect;
}
