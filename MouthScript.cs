﻿using System;
using UnityEngine;

public class MouthScript : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
		if (this.MainMenu)
		{
			this.Satisfaction = Mathf.Max(this.Satisfaction - Time.deltaTime / 120f, 0f);
		}
		float num = this.Satisfaction * 2f - 1f;
		this.SetJointY(this.Joints[0], num * this.range);
		this.SetJointY(this.Joints[1], num * this.range / 2f);
		this.SetJointY(this.Joints[this.Joints.Length - 2], num * this.range / 2f);
		this.SetJointY(this.Joints[this.Joints.Length - 1], num * this.range);
		if (this.talk)
		{
			float num2 = this.TalkTime;
			if (num2 > 1f)
			{
				num2 = 2f - num2;
			}
			this.Joints[2].localScale = new Vector3(1f, 1f + num2, 1f);
		}
		else
		{
			this.Joints[2].localScale = new Vector3(1f, 1f, 1f);
		}
		this.TalkTime = (this.TalkTime + Time.deltaTime * 6f) % 2f;
	}

	public void SetJointY(Transform joint, float y)
	{
		joint.localPosition = new Vector3(joint.localPosition.x, y, joint.localPosition.z);
	}

	public float Satisfaction;

	public float range = 0.3f;

	public bool talk;

	public bool MainMenu;

	private float TalkTime;

	public Transform[] Joints;
}
