﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DevConsole;
using Steamworks;
using UnityEngine;

public static class Localization
{
	static Localization()
	{
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		string localizationFolder = Localization.LocalizationFolder;
		if (!Directory.Exists(localizationFolder))
		{
			Directory.CreateDirectory(localizationFolder);
		}
		Localization.CopyDefault();
		Localization.LoadLanguages();
		string language = Options.Language;
		Localization.CurrentTranslation = language;
		Localization._originalTranslation = language;
		if (Localization.CurrentTranslation == null)
		{
			Localization.CopyDefault();
			Localization.LoadLanguages();
			Localization.CurrentTranslation = "English";
		}
		Debug.Log("Localization load time: " + (Time.realtimeSinceStartup - realtimeSinceStartup));
		if (LoadDebugger.Instance != null)
		{
			LoadDebugger.AddInfo("Finished loading languages");
		}
	}

	public static string CurrentTranslation
	{
		get
		{
			return Localization._currentTranslation;
		}
		set
		{
			if (Localization.Translations.ContainsKey(value))
			{
				Localization._currentTranslation = value;
			}
			if (Localization._currentTranslation == null && Localization.Translations.Count > 0)
			{
				Localization._currentTranslation = Localization.Translations.Keys.First<string>();
			}
		}
	}

	public static string LocalizationFolder
	{
		get
		{
			return Path.Combine(Utilities.GetRoot(), "Localization");
		}
	}

	public static void CopyDefault()
	{
		string localizationFolder = Localization.LocalizationFolder;
		string[] source = new string[]
		{
			"English"
		};
		foreach (string text in from x in source
		orderby x
		select x)
		{
			string text2 = Path.Combine(localizationFolder, text);
			if (!Directory.Exists(text2))
			{
				Directory.CreateDirectory(text2);
			}
			TextAsset[] array = Resources.LoadAll<TextAsset>("Localization/" + text);
			foreach (TextAsset textAsset in array)
			{
				try
				{
					File.WriteAllText(Path.Combine(text2, textAsset.name + ".xml"), textAsset.text);
				}
				catch (Exception)
				{
				}
			}
		}
	}

	public static void LoadLanguages()
	{
		string[] directories = Directory.GetDirectories(Localization.LocalizationFolder);
		foreach (string text in directories)
		{
			try
			{
				string fileName = Path.GetFileName(text);
				Localization.Translations[fileName] = new Localization.Translation(text, fileName);
			}
			catch (Exception ex)
			{
				LoadDebugger.AddError("Failed loading language: " + Path.GetFileName(text));
				GameData.FailedModList.Add(Path.GetFileName(text));
				Debug.LogError("Localization failed loading language: " + Path.GetFileName(text) + ". With error:\n" + ex.ToString());
			}
		}
	}

	public static IWorkshopItem LoadSteamLanguage(string path, string name)
	{
		try
		{
			string fileName = Path.GetFileName(path);
			Localization.Translation translation = new Localization.Translation(path, fileName);
			Localization.Translations[fileName] = translation;
			if (fileName.Equals(Localization._originalTranslation))
			{
				Options.Language = fileName;
				if (LanguageCombo.Instance != null)
				{
					LanguageCombo.Instance.RefreshMainMenu();
				}
			}
			return translation;
		}
		catch (Exception ex)
		{
			string text = "Error loading localization " + name + ":\n" + ex.ToString();
			Debug.Log(text);
			DevConsole.Console.LogError(text);
		}
		return null;
	}

	public static string[] GetFurniture(string name, string desc)
	{
		if (Localization.CurrentTranslation == null)
		{
			return new string[]
			{
				name,
				desc
			};
		}
		string[] result;
		if (Localization.Translations[Localization.CurrentTranslation].Furniture.TryGetValue(name, out result))
		{
			return result;
		}
		return new string[]
		{
			name,
			desc
		};
	}

	public static string GetTutorial(string id)
	{
		if (Localization.CurrentTranslation == null || Localization.Translations[Localization.CurrentTranslation].TutorialSystem == null)
		{
			string result;
			if (Localization.Translations["English"].TutorialSystem.TryGetValue(id, out result))
			{
				return result;
			}
			return "[NoLoc]" + id;
		}
		else
		{
			string result;
			if (Localization.Translations[Localization.CurrentTranslation].TutorialSystem.TryGetValue(id, out result))
			{
				return result;
			}
			if (Localization.Translations["English"].TutorialSystem.TryGetValue(id, out result))
			{
				return result;
			}
			return "[NoLoc]" + id;
		}
	}

	public static string[] GetSoftware(SoftwareType sw)
	{
		if (Localization.CurrentTranslation == null)
		{
			return new string[]
			{
				sw.Name,
				sw.Description
			};
		}
		string[] result;
		if (Localization.Translations[Localization.CurrentTranslation].Software.TryGetValue(sw.Name, out result))
		{
			return result;
		}
		return new string[]
		{
			sw.Name,
			sw.Description
		};
	}

	public static string[] GetSoftware(string sw, string desc)
	{
		if (Localization.CurrentTranslation == null)
		{
			return new string[]
			{
				sw,
				desc
			};
		}
		string[] result;
		if (Localization.Translations[Localization.CurrentTranslation].Software.TryGetValue(sw, out result))
		{
			return result;
		}
		return new string[]
		{
			sw,
			desc
		};
	}

	public static string LocSWC(this string cat, string sw)
	{
		return Localization.GetSoftwareCat(sw, cat, null)[0];
	}

	public static string[] GetSoftwareCat(string sw, string cat, string description)
	{
		if (Localization.CurrentTranslation == null)
		{
			return new string[]
			{
				cat,
				description
			};
		}
		if ("Default".Equals(cat))
		{
			return Localization.Translations[Localization.CurrentTranslation].SWCategoryDefault;
		}
		Dictionary<string, string[]> dictionary;
		string[] result;
		if (Localization.Translations[Localization.CurrentTranslation].SoftwareCat.TryGetValue(sw, out dictionary) && dictionary.TryGetValue(cat, out result))
		{
			return result;
		}
		return new string[]
		{
			cat,
			description
		};
	}

	public static string SWFeat(this string feat, string swType)
	{
		return Localization.GetFeature(swType, feat)[0];
	}

	public static string[] GetFeature(SoftwareType sw, string feature)
	{
		if (Localization.CurrentTranslation == null)
		{
			Feature feature2 = sw.Features[feature];
			return new string[]
			{
				feature2.Name,
				feature2.Description
			};
		}
		string[] result;
		if (Localization.Translations[Localization.CurrentTranslation].Feature.TryGetValue(new KeyValuePair<string, string>(sw.Name, feature), out result))
		{
			return result;
		}
		if (Localization.Translations[Localization.CurrentTranslation].BaseFeatures.TryGetValue(feature, out result))
		{
			return result;
		}
		Feature feature3 = sw.Features[feature];
		return new string[]
		{
			feature3.Name,
			feature3.Description
		};
	}

	public static string[] GetFeature(string sw, string feature)
	{
		if (Localization.CurrentTranslation == null)
		{
			Feature feature2 = GameSettings.Instance.SoftwareTypes[sw].Features[feature];
			return new string[]
			{
				feature2.Name,
				feature2.Description
			};
		}
		string[] result;
		if (Localization.Translations[Localization.CurrentTranslation].Feature.TryGetValue(new KeyValuePair<string, string>(sw, feature), out result))
		{
			return result;
		}
		if (Localization.Translations[Localization.CurrentTranslation].BaseFeatures.TryGetValue(feature, out result))
		{
			return result;
		}
		Feature feature3 = GameSettings.Instance.SoftwareTypes[sw].Features[feature];
		return new string[]
		{
			feature3.Name,
			feature3.Description
		};
	}

	public static string LocSW(this string input)
	{
		if (string.IsNullOrEmpty(input))
		{
			return input;
		}
		if (Localization.CurrentTranslation == null)
		{
			return input;
		}
		string[] array;
		if (Localization.Translations[Localization.CurrentTranslation].Software.TryGetValue(input, out array))
		{
			return array[0];
		}
		return input;
	}

	public static string Loc(this string input, params object[] args)
	{
		return string.Format(input.Loc(), args);
	}

	public static string Loc(this string input)
	{
		if (string.IsNullOrEmpty(input))
		{
			return input;
		}
		if (Localization.CurrentTranslation == null)
		{
			return "[Localization not loaded]";
		}
		string result;
		if (Localization.Translations[Localization.CurrentTranslation].UI.TryGetValue(input, out result))
		{
			return result;
		}
		Localization.Translation translation;
		if (Localization.Translations.TryGetValue("English", out translation) && translation.UI.TryGetValue(input, out result))
		{
			return result;
		}
		return "[NoLoc]" + input;
	}

	public static string LocTry(this string input)
	{
		if (string.IsNullOrEmpty(input))
		{
			return input;
		}
		if (Localization.CurrentTranslation == null)
		{
			return input;
		}
		string result;
		if (Localization.Translations[Localization.CurrentTranslation].UI.TryGetValue(input, out result))
		{
			return result;
		}
		return input;
	}

	public static string LocDef(this string input, string defaultLoc)
	{
		if (string.IsNullOrEmpty(input))
		{
			return defaultLoc;
		}
		if (Localization.CurrentTranslation == null)
		{
			return defaultLoc;
		}
		string result;
		if (Localization.Translations[Localization.CurrentTranslation].UI.TryGetValue(input, out result))
		{
			return result;
		}
		Localization.Translation translation;
		if (Localization.Translations.TryGetValue("English", out translation) && translation.UI.TryGetValue(input, out result))
		{
			return result;
		}
		return defaultLoc;
	}

	public static string LocPlural(this string input, int number)
	{
		if (number == 1)
		{
			return string.Format(input.Loc(), number);
		}
		return string.Format((input + "Plural").Loc(), number);
	}

	public static Dictionary<string, Localization.Translation> Translations = new Dictionary<string, Localization.Translation>();

	private static string _currentTranslation = null;

	private static string _originalTranslation = null;

	public class Translation : IWorkshopItem
	{
		public Translation(string folder, string locName)
		{
			string[] array = new string[2];
			array[0] = "Default";
			this.SWCategoryDefault = array;
			this._setTitle = true;
			base..ctor();
			this.Name = locName;
			this.RealName = locName;
			this.Root = folder;
			string text = Path.Combine(folder, "Furniture.xml");
			string text2 = Path.Combine(folder, "UI.xml");
			string text3 = Path.Combine(folder, "Software.xml");
			if (!File.Exists(text) || !File.Exists(text2) || !File.Exists(text3))
			{
				throw new Exception("Missing files in translation");
			}
			this.UI = new Dictionary<string, string>();
			XMLParser.XMLNode xmlnode = XMLParser.ParseXML(Utilities.ReadOnlyReadAllText(text2));
			string text4 = Path.Combine(folder, "Articles.xml");
			if (File.Exists(text4))
			{
				XMLParser.XMLNode xmlnode2 = XMLParser.ParseXML(Utilities.ReadOnlyReadAllText(text4));
				xmlnode.Children.AddRange(xmlnode2.Children);
			}
			foreach (XMLParser.XMLNode node in xmlnode.Children)
			{
				this.AddNodes(node, this.UI);
			}
			this.Furniture = new Dictionary<string, string[]>();
			XMLParser.XMLNode xmlnode3 = XMLParser.ParseXML(Utilities.ReadOnlyReadAllText(text));
			foreach (XMLParser.XMLNode xmlnode4 in xmlnode3.Children)
			{
				string attribute = xmlnode4.GetAttribute("Name");
				this.Furniture[attribute] = new string[]
				{
					xmlnode4.GetNode("Name", true).Value,
					xmlnode4.GetNode("Description", true).Value
				};
			}
			this.Software = new Dictionary<string, string[]>();
			this.SoftwareCat = new Dictionary<string, Dictionary<string, string[]>>();
			this.BaseFeatures = new Dictionary<string, string[]>();
			this.Feature = new Dictionary<KeyValuePair<string, string>, string[]>();
			XMLParser.XMLNode xmlnode5 = XMLParser.ParseXML(Utilities.ReadOnlyReadAllText(text3));
			foreach (XMLParser.XMLNode xmlnode6 in xmlnode5.Children)
			{
				if (xmlnode6.Name.Equals("Feature"))
				{
					string attribute2 = xmlnode6.GetAttribute("Name");
					XMLParser.XMLNode node2 = xmlnode6.GetNode("Description", false);
					this.BaseFeatures[attribute2] = new string[]
					{
						xmlnode6.GetNode("Name", true).Value,
						(node2 != null) ? node2.Value : null
					};
				}
				else if (xmlnode6.Name.Equals("CategoryDefault"))
				{
					string[] array2 = new string[2];
					array2[0] = xmlnode6.Value;
					this.SWCategoryDefault = array2;
				}
				else
				{
					string attribute3 = xmlnode6.GetAttribute("Name");
					this.Software[attribute3] = new string[]
					{
						xmlnode6.GetNode("Name", true).Value,
						xmlnode6.GetNode("Description", true).Value
					};
					foreach (XMLParser.XMLNode xmlnode7 in xmlnode6.GetNode("Features", true).Children)
					{
						string attribute4 = xmlnode7.GetAttribute("Name");
						XMLParser.XMLNode node3 = xmlnode7.GetNode("Description", false);
						this.Feature[new KeyValuePair<string, string>(attribute3, attribute4)] = new string[]
						{
							xmlnode7.GetNode("Name", true).Value,
							(node3 != null) ? node3.Value : null
						};
					}
					Dictionary<string, string[]> dictionary = new Dictionary<string, string[]>();
					this.SoftwareCat[attribute3] = dictionary;
					XMLParser.XMLNode node4 = xmlnode6.GetNode("Categories", false);
					if (node4 != null)
					{
						foreach (XMLParser.XMLNode xmlnode8 in node4.Children)
						{
							string attribute5 = xmlnode8.GetAttribute("Name");
							XMLParser.XMLNode node5 = xmlnode8.GetNode("Description", false);
							string text5 = (node5 == null) ? null : node5.Value;
							string value = xmlnode8.GetNode("Name", true).Value;
							dictionary[attribute5] = new string[]
							{
								value,
								text5
							};
						}
					}
				}
			}
			string text6 = Path.Combine(folder, "Tutorial.xml");
			if (File.Exists(text6))
			{
				this.TutorialSystem = new Dictionary<string, string>();
				XMLParser.XMLNode xmlnode9 = XMLParser.ParseXML(Utilities.ReadOnlyReadAllText(text6));
				foreach (XMLParser.XMLNode xmlnode10 in xmlnode9.Children)
				{
					this.TutorialSystem[xmlnode10.GetAttribute("id")] = xmlnode10.Value;
				}
			}
		}

		public string ItemTitle
		{
			get
			{
				return this.Name;
			}
			set
			{
				this.Name = value;
			}
		}

		public PublishedFileId_t? SteamID { get; set; }

		public bool SetTitle
		{
			get
			{
				return this._setTitle;
			}
			set
			{
				this._setTitle = value;
			}
		}

		public bool CanUpload
		{
			get
			{
				return !this.RealName.Equals("English") && this._canUpload;
			}
			set
			{
				this._canUpload = value;
			}
		}

		private void AddNodes(XMLParser.XMLNode node, Dictionary<string, string> tree)
		{
			if (node.Children != null && node.Children.Count > 0)
			{
				for (int i = 0; i < node.Children.Count; i++)
				{
					this.AddNodes(node.Children[i], tree);
				}
			}
			else
			{
				string attribute = node.GetAttribute("Name");
				tree[attribute] = node.Value;
			}
		}

		public string GetWorkshopType()
		{
			return "Localization";
		}

		public string FolderPath()
		{
			return Path.GetFullPath(this.Root);
		}

		public string[] GetValidExts()
		{
			return new string[]
			{
				"xml",
				"txt",
				"png"
			};
		}

		public string[] ExtraTags()
		{
			return new string[0];
		}

		public string GetThumbnail()
		{
			string text = Path.Combine(this.FolderPath(), "Thumbnail.png");
			return (!File.Exists(text)) ? null : text;
		}

		public override string ToString()
		{
			return this.Name;
		}

		public string RealName;

		public string Name;

		public string Root;

		private bool _canUpload = true;

		public string[] SWCategoryDefault;

		public Dictionary<string, Dictionary<string, string[]>> SoftwareCat;

		public Dictionary<string, string[]> Software;

		public Dictionary<KeyValuePair<string, string>, string[]> Feature;

		public Dictionary<string, string[]> Furniture;

		public Dictionary<string, string> UI;

		public Dictionary<string, string[]> BaseFeatures;

		public Dictionary<string, string> TutorialSystem;

		public string[] FemaleNames;

		public string[] MaleNames;

		public string[] LastNames;

		private bool _setTitle;
	}
}
