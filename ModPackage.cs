﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Steamworks;

[Serializable]
public class ModPackage : IWorkshopItem
{
	private ModPackage(string root, string name, Dictionary<string, SoftwareType> s, Dictionary<string, SoftwareTypeOverride> so, Dictionary<string, CompanyType> c, Dictionary<string, RandomNameGenerator> n, Dictionary<string, EventCompany> ec, Dictionary<string, EventCompany[]> e, PersonalityGraph p, Feature[] bf, bool bfo, string[] dct)
	{
		this.Root = root;
		this.Name = name;
		this.SoftwareTypes = s;
		this.SoftwareTypeOverrides = so;
		this.CompanyTypes = c;
		this.NameGenerators = n;
		this.EventCompanies = ec;
		this.Events = e;
		this.Personalities = p;
		this.BaseFeatures = bf;
		this.BaseFeatureOverride = bfo;
		this.DeleteCompanyTypes = dct;
	}

	public bool Enabled
	{
		get
		{
			return this._enabled;
		}
		set
		{
			this._enabled = value;
		}
	}

	public PublishedFileId_t? SteamID { get; set; }

	public bool SetTitle
	{
		get
		{
			return this._setTitle;
		}
		set
		{
			this._setTitle = value;
		}
	}

	public bool CanUpload
	{
		get
		{
			return this._canUpload;
		}
		set
		{
			this._canUpload = value;
		}
	}

	public string ItemTitle
	{
		get
		{
			return this.Name;
		}
		set
		{
			this.Name = value;
		}
	}

	public static ModPackage Load(string root)
	{
		string fileName = Path.GetFileName(root);
		Dictionary<string, SoftwareType> dictionary = new Dictionary<string, SoftwareType>();
		Dictionary<string, SoftwareTypeOverride> dictionary2 = new Dictionary<string, SoftwareTypeOverride>();
		Dictionary<string, CompanyType> dictionary3 = new Dictionary<string, CompanyType>();
		Dictionary<string, RandomNameGenerator> dictionary4 = new Dictionary<string, RandomNameGenerator>();
		Dictionary<string, EventCompany> dictionary5 = new Dictionary<string, EventCompany>();
		Dictionary<string, EventCompany[]> dictionary6 = new Dictionary<string, EventCompany[]>();
		bool bfo = false;
		PersonalityGraph p = null;
		if (Directory.Exists(root + "/NameGenerators"))
		{
			string[] files = Directory.GetFiles(root + "/NameGenerators", "*.txt");
			foreach (string path in files)
			{
				try
				{
					dictionary4.Add(Path.GetFileNameWithoutExtension(path), RandomNameGenerator.Load(File.ReadAllLines(path)));
				}
				catch (Exception ex)
				{
					throw new Exception("Error loading name generator: " + Path.GetFileName(path) + " with error:\n" + ex.Message);
				}
			}
		}
		Feature[] bf = new Feature[0];
		if (Directory.Exists(root + "/SoftwareTypes"))
		{
			if (File.Exists(root + "/SoftwareTypes/base.xml"))
			{
				try
				{
					XMLParser.XMLNode xmlnode = XMLParser.ParseXML(Utilities.ReadAllText(root + "/SoftwareTypes/base.xml"));
					bfo = xmlnode.Attributes.ContainsKey("Override");
					bf = xmlnode.GetNodes("Feature", false).SelectInPlace((XMLParser.XMLNode x) => new Feature(x, null, true));
				}
				catch (Exception ex2)
				{
					throw new Exception("Error loading Base feature xml file with error:\n" + ex2.Message);
				}
			}
			string[] files2 = Directory.GetFiles(root + "/SoftwareTypes", "*.xml");
			foreach (string text in from x in files2
			where !Path.GetFileNameWithoutExtension(x).ToLower().Equals("base")
			select x)
			{
				try
				{
					XMLParser.XMLNode xmlnode2 = XMLParser.ParseXML(Utilities.ReadAllText(text));
					if (xmlnode2.Attributes.ContainsKey("Override"))
					{
						SoftwareTypeOverride softwareTypeOverride = new SoftwareTypeOverride(xmlnode2);
						dictionary2[softwareTypeOverride.Name] = softwareTypeOverride;
					}
					else
					{
						SoftwareType softwareType = new SoftwareType(xmlnode2, true);
						dictionary[softwareType.Name] = softwareType;
					}
				}
				catch (Exception ex3)
				{
					throw new Exception("Error loading software type: " + Path.GetFileName(text) + " with error:\n" + ex3.Message);
				}
			}
		}
		string[] dct = new string[0];
		if (Directory.Exists(root + "/CompanyTypes"))
		{
			string[] files3 = Directory.GetFiles(root + "/CompanyTypes", "*.xml");
			foreach (string text2 in files3)
			{
				try
				{
					if (Path.GetFileName(text2).ToLower().Equals("delete.xml"))
					{
						XMLParser.XMLNode xmlnode3 = XMLParser.ParseXML(Utilities.ReadAllText(text2));
						if (xmlnode3.Children.Count > 0)
						{
							dct = (from x in xmlnode3.Children
							select x.Value).ToArray<string>();
						}
					}
					else
					{
						CompanyType companyType = new CompanyType(XMLParser.ParseXML(Utilities.ReadAllText(text2)));
						dictionary3[companyType.Name] = companyType;
					}
				}
				catch (Exception ex4)
				{
					throw new Exception("Error loading company type: " + Path.GetFileName(text2) + " with error:\n" + ex4.Message);
				}
			}
		}
		if (Directory.Exists(root + "/Companies"))
		{
			string[] files4 = Directory.GetFiles(root + "/Companies", "*.xml");
			foreach (string text3 in files4)
			{
				try
				{
					EventCompany eventCompany = EventCompany.Load(XMLParser.ParseXML(Utilities.ReadAllText(text3)));
					dictionary5[eventCompany.Name] = eventCompany;
				}
				catch (Exception ex5)
				{
					throw new Exception("Error loading event company: " + Path.GetFileName(text3) + " with error:\n" + ex5.Message);
				}
			}
		}
		if (Directory.Exists(root + "/Events"))
		{
			string[] files5 = Directory.GetFiles(root + "/Events", "*.xml");
			foreach (string text4 in files5)
			{
				try
				{
					KeyValuePair<string, EventCompany[]> keyValuePair = GameData.LoadEvents(XMLParser.ParseXML(Utilities.ReadAllText(text4)), dictionary5);
					dictionary6[keyValuePair.Key] = keyValuePair.Value;
				}
				catch (Exception ex6)
				{
					throw new Exception("Error loading event: " + Path.GetFileName(text4) + " with error:\n" + ex6.Message);
				}
			}
		}
		if (File.Exists(root + "/Personalities.xml"))
		{
			try
			{
				p = new PersonalityGraph(XMLParser.ParseXML(Utilities.ReadAllText(root + "/Personalities.xml")));
			}
			catch (Exception ex7)
			{
				throw new Exception("Error loading personalities with error:\n" + ex7.Message);
			}
		}
		ModPackage modPackage = new ModPackage(root, fileName, dictionary, dictionary2, dictionary3, dictionary4, dictionary5, dictionary6, p, bf, bfo, dct);
		SoftwareType[] types = new SoftwareType[0];
		CompanyType[] cTypes = new CompanyType[0];
		try
		{
			types = GameData.AllSoftwareTypes(new ModPackage[]
			{
				modPackage
			}).ToArray<SoftwareType>();
			cTypes = GameData.AllCompanyTypes(new ModPackage[]
			{
				modPackage
			}).ToArray<CompanyType>();
		}
		catch (Exception ex8)
		{
			throw new Exception("Error applying mod with error:\n" + ex8.Message);
		}
		string text5 = GameData.CheckForErrors(types);
		if (text5 != null)
		{
			throw new Exception("Error applying mod with error:\n" + text5);
		}
		text5 = ModPackage.OtherErrors(types, cTypes, GameData.GetRandomNameGenerators(dictionary4));
		if (text5 != null)
		{
			throw new Exception("Error applying mod with error:\n" + text5);
		}
		return modPackage;
	}

	public static string OtherErrors(SoftwareType[] types, CompanyType[] cTypes, Dictionary<string, RandomNameGenerator> generators)
	{
		foreach (SoftwareType softwareType in types)
		{
			foreach (SoftwareCategory softwareCategory in softwareType.Categories.Values)
			{
				if (!generators.ContainsKey(softwareCategory.GetNameGenName()))
				{
					return string.Concat(new string[]
					{
						"Name generator ",
						softwareCategory.GetNameGenName(),
						" for category ",
						softwareCategory.Name,
						" in software ",
						softwareType.Name,
						" does not exist"
					});
				}
			}
		}
		foreach (CompanyType companyType in cTypes)
		{
			List<string> list = (from x in companyType.Types.Keys
			select x.Key).ToList<string>();
			if (list.Count != list.Distinct<string>().Count<string>())
			{
				return "Company type " + companyType.Name + " has same software type defined twice";
			}
			using (Dictionary<KeyValuePair<string, string>, float>.Enumerator enumerator2 = companyType.Types.GetEnumerator())
			{
				while (enumerator2.MoveNext())
				{
					KeyValuePair<KeyValuePair<string, string>, float> item = enumerator2.Current;
					SoftwareType softwareType2 = types.FirstOrDefault((SoftwareType x) => x.Name.Equals(item.Key.Key));
					if (softwareType2 == null)
					{
						return string.Concat(new object[]
						{
							"Software ",
							item.Key,
							" in company type ",
							companyType.Name,
							" does not exist"
						});
					}
					if (item.Key.Value != null && !softwareType2.Categories.ContainsKey(item.Key.Value))
					{
						return string.Concat(new string[]
						{
							"Category ",
							item.Key.Value,
							" for software ",
							item.Key.Key,
							" in company type ",
							companyType.Name,
							" does not exist"
						});
					}
				}
			}
			if (companyType.Force != null)
			{
				string[] sp = companyType.Force.Split(new char[]
				{
					','
				});
				SoftwareType softwareType3 = types.FirstOrDefault((SoftwareType x) => x.Name.Equals(sp[0]));
				if (softwareType3 == null)
				{
					return string.Concat(new string[]
					{
						"Forced release software ",
						sp[0],
						" in company type ",
						companyType.Name,
						" does not exist"
					});
				}
				if (!softwareType3.Categories.ContainsKey(sp[1]))
				{
					return string.Concat(new string[]
					{
						"Forced software release category ",
						sp[1],
						" for software ",
						sp[0],
						" in company type ",
						companyType.Name,
						" does not exist"
					});
				}
			}
			if (companyType.NameGen != null && !generators.ContainsKey(companyType.NameGen))
			{
				return string.Concat(new string[]
				{
					"Name generator ",
					companyType.NameGen,
					" for company type ",
					companyType.Name,
					" does not exist"
				});
			}
		}
		return null;
	}

	public override string ToString()
	{
		return this.Name;
	}

	public string GetWorkshopType()
	{
		return "Mod";
	}

	public string FolderPath()
	{
		return Path.GetFullPath(this.Root);
	}

	public string[] GetValidExts()
	{
		return new string[]
		{
			"txt",
			"xml",
			"png"
		};
	}

	public string[] ExtraTags()
	{
		List<string> list = new List<string>();
		if ((this.SoftwareTypes != null && this.SoftwareTypes.Count > 0) || (this.SoftwareTypeOverrides != null && this.SoftwareTypeOverrides.Count > 0))
		{
			list.Add("Software types");
		}
		if (this.Personalities != null && this.Personalities.PersonalityTraits.Count > 0)
		{
			list.Add("Personalities");
		}
		if (this.CompanyTypes != null && this.CompanyTypes.Count > 0)
		{
			list.Add("AI companies");
		}
		return list.ToArray();
	}

	public string GetThumbnail()
	{
		string text = Path.Combine(this.FolderPath(), "Thumbnail.png");
		return (!File.Exists(text)) ? null : text;
	}

	public string Name;

	public readonly string Root;

	public readonly Dictionary<string, SoftwareType> SoftwareTypes;

	public readonly Dictionary<string, SoftwareTypeOverride> SoftwareTypeOverrides;

	public readonly Dictionary<string, CompanyType> CompanyTypes;

	public readonly Dictionary<string, RandomNameGenerator> NameGenerators;

	public readonly Dictionary<string, EventCompany> EventCompanies;

	public readonly Dictionary<string, EventCompany[]> Events;

	public readonly string[] DeleteCompanyTypes;

	public readonly PersonalityGraph Personalities;

	public readonly Feature[] BaseFeatures;

	public readonly bool BaseFeatureOverride;

	private bool _enabled;

	private bool _canUpload = true;

	private bool _setTitle = true;
}
