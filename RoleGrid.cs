﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RoleGrid : MaskableGraphic, IPointerClickHandler, IEventSystemHandler
{
	public void Show(IEnumerable<Actor> actors)
	{
		this.Actors = (from x in actors
		orderby (x.GetTeam() != null) ? x.GetTeam().Name : string.Empty, x.employee.FullName
		select x).ToList<Actor>();
		if (this.Actors.Count > 0)
		{
			this.Window.Show();
			this.UpdateActorLabel();
			this.selfRect.sizeDelta = new Vector2(this.selfRect.sizeDelta.x, (float)Mathf.Max(4, this.Actors.Count) * this.LineHeight);
			this.ContentPanel.sizeDelta = new Vector2(this.ContentPanel.sizeDelta.x, (float)Mathf.Max(4, this.Actors.Count + 1) * this.LineHeight);
			this.SetVerticesDirty();
			LayoutRebuilder.ForceRebuildLayoutImmediate(this.RowLabels.rectTransform.parent.GetComponent<RectTransform>());
			for (int i = 0; i < this.RoleToggles.Length; i++)
			{
				this.UpdateRoleToggle(i);
			}
		}
	}

	private void UpdateActorLabel()
	{
		this.RowLabels.text = string.Join("\n", this.Actors.Select(delegate(Actor x)
		{
			if (x.employee.CurrentRoleBit == Employee.RoleBit.None)
			{
				return "<color=#ff0000ff>" + x.employee.FullName + "</color>";
			}
			return x.employee.FullName;
		}).ToArray<string>());
	}

	public void OnToggle(int role)
	{
		if (!this._isUpdatingRoleToggle)
		{
			foreach (Actor actor in this.Actors)
			{
				if (actor != null)
				{
					actor.ChangeRole((Employee.EmployeeRole)role, this.RoleToggles[role].isOn);
				}
			}
			this.SetVerticesDirty();
			this.UpdateActorLabel();
		}
	}

	public void UpdateRoleToggle(int r)
	{
		this._isUpdatingRoleToggle = true;
		this.RoleToggles[r].isOn = (from x in this.Actors
		where x != null
		select x).All((Actor x) => x.employee.IsRoleIndex(r));
		this._isUpdatingRoleToggle = false;
	}

	protected override void OnPopulateMesh(VertexHelper vh)
	{
		vh.Clear();
		bool isOn = this.RelativeToggle.isOn;
		for (int i = 0; i < this.Actors.Count; i++)
		{
			Actor actor = this.Actors[i];
			if (actor == null)
			{
				this.Actors.RemoveAt(i);
				i--;
				this.UpdateActorLabel();
			}
			float num = 0.01f;
			if (isOn)
			{
				for (int j = 0; j < 5; j++)
				{
					num = Mathf.Max(num, actor.employee.GetSkillI(j));
				}
			}
			for (int k = 0; k < 5; k++)
			{
				bool flag = actor.employee.IsRoleIndex(k);
				float num2 = this.ColumnWidth * (float)k;
				float num3 = this.LineHeight * (float)i;
				Color color = (!flag) ? this.CheckBackColor : this.BackColor;
				this.DrawRect(num2, num3, this.ColumnWidth - this.Padding / 2f, this.LineHeight, color, color, vh);
				float num4 = actor.employee.GetSkillI(k);
				if (isOn)
				{
					num4 /= num;
				}
				Color color2 = this.SkillGrad.Evaluate(num4);
				this.DrawRect(num2 + this.Padding / 2f, num3 + this.Padding / 2f, Mathf.Round((this.ColumnWidth - this.CheckSpace - this.Padding) * num4), this.LineHeight - this.Padding, this.SkillGrad.Evaluate(0f), color2, vh);
				this.DrawRect(num2 + this.ColumnWidth - this.CheckSpace, num3 + this.Padding / 2f, this.CheckSpace - this.Padding, this.LineHeight - this.Padding, color2, color2, vh);
				if (flag)
				{
					this.DrawRect(num2 + this.ColumnWidth - this.CheckSpace / 2f - this.CheckSize / 2f - this.Padding / 2f, num3 + this.LineHeight / 2f - this.CheckSize / 2f, this.CheckSize, this.CheckSize, this.CheckColor, this.CheckColor, vh);
				}
			}
		}
	}

	public void DrawRect(float x, float y, float w, float h, Color col, Color col2, VertexHelper vh)
	{
		vh.AddUIVertexQuad(new UIVertex[]
		{
			new UIVertex
			{
				color = col,
				position = new Vector3(x, -y, 0f)
			},
			new UIVertex
			{
				color = col2,
				position = new Vector3(x + w, -y, 0f)
			},
			new UIVertex
			{
				color = col2,
				position = new Vector3(x + w, -y - h, 0f)
			},
			new UIVertex
			{
				color = col,
				position = new Vector3(x, -y - h, 0f)
			}
		});
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		Vector2 zero = Vector2.zero;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(this.selfRect, eventData.position, null, out zero);
		int num = Mathf.FloorToInt(-zero.y / this.LineHeight);
		if (num >= 0 && num < this.Actors.Count)
		{
			int num2 = Mathf.FloorToInt(zero.x / this.ColumnWidth);
			if (num2 >= 0 && num2 < 5)
			{
				Employee.EmployeeRole role = (Employee.EmployeeRole)num2;
				this.Actors[num].ChangeRole(role, !this.Actors[num].employee.IsRole(role));
				this.SetVerticesDirty();
				this.UpdateActorLabel();
				this.UpdateRoleToggle(num2);
			}
		}
	}

	public void PointerClickName(BaseEventData bEventData)
	{
		PointerEventData pointerEventData = (PointerEventData)bEventData;
		Vector2 zero = Vector2.zero;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(this.selfRect, pointerEventData.position, null, out zero);
		int num = Mathf.FloorToInt(-zero.y / this.LineHeight);
		if (num >= 0 && num < this.Actors.Count)
		{
			Actor actor = this.Actors[num];
			Employee.RoleBit currentRoleBit = actor.employee.CurrentRoleBit;
			Employee.RoleBit roleBit = currentRoleBit & Employee.RoleBit.Lead;
			if ((currentRoleBit & Employee.RoleBit.AnyRole) == Employee.RoleBit.AnyRole)
			{
				actor.ChangeRole(roleBit);
			}
			else
			{
				actor.ChangeRole(roleBit | Employee.RoleBit.AnyRole);
			}
			this.SetVerticesDirty();
			this.UpdateActorLabel();
			for (int i = 0; i < this.RoleToggles.Length; i++)
			{
				this.UpdateRoleToggle(i);
			}
		}
	}

	public float LineHeight;

	public float ColumnWidth;

	public Toggle[] RoleToggles;

	public Gradient SkillGrad;

	public Toggle RelativeToggle;

	public Color BackColor;

	public Color CheckColor;

	public Color CheckBackColor;

	public List<Actor> Actors = new List<Actor>();

	public Text RowLabels;

	public GUIWindow Window;

	public RectTransform ContentPanel;

	public RectTransform selfRect;

	public float Padding = 2f;

	public float CheckSize = 8f;

	public float CheckSpace = 24f;

	private bool _isUpdatingRoleToggle;
}
