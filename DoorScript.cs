﻿using System;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
	public int Floor
	{
		get
		{
			return Mathf.FloorToInt((base.transform.position.y + 1f) / 2f);
		}
	}

	private void Start()
	{
		this.OriginalRot = base.transform.localRotation;
	}

	private void Update()
	{
		if (this.doorAudio != null && this.MayPlaySound())
		{
			Room cameraRoom = GameSettings.Instance.sRoomManager.CameraRoom;
			this.doorAudio.outputAudioMixerGroup = (((this.Owner.OnlyInterior || !(cameraRoom == null)) && !object.ReferenceEquals(cameraRoom, this.Owner.ParentRooms[0]) && !object.ReferenceEquals(cameraRoom, this.Owner.ParentRooms[1])) ? AudioManager.InGameHighPass : AudioManager.InGameNormal);
		}
		if (this.Close || this.Open || this.Wait)
		{
			this.timer = Mathf.Min(1f, this.timer + Time.deltaTime * GameSettings.GameSpeed * ((!this.Wait) ? this.Speed : (1f / this.waitTime)));
		}
		if (this.Close)
		{
			if (this.Scale)
			{
				base.transform.localScale = new Vector3(1f, 1f, this.timer);
			}
			else
			{
				base.transform.localRotation = Quaternion.Euler(0f, Mathf.Lerp((float)((!this.Front) ? -90 : 90), 0f, this.timer), 0f) * this.OriginalRot;
			}
		}
		if (this.Open)
		{
			if (this.Scale)
			{
				base.transform.localScale = new Vector3(1f, 1f, 1f - this.timer);
			}
			else
			{
				base.transform.localRotation = Quaternion.Euler(0f, Mathf.Lerp(0f, (float)((!this.Front) ? -90 : 90), this.timer), 0f) * this.OriginalRot;
			}
		}
		if (Mathf.Approximately(this.timer, 1f))
		{
			if (this.Wait)
			{
				if (this.DoorCloseSoundOnClose && this.CloseSound != null && this.doorAudio != null && this.MayPlaySound())
				{
					this.doorAudio.PlayOneShot(this.CloseSound);
				}
				this.Close = true;
				this.Wait = false;
			}
			else if (this.Open)
			{
				this.Wait = true;
				this.Open = false;
			}
			else if (this.Close)
			{
				if (!this.DoorCloseSoundOnClose && this.CloseSound != null && this.doorAudio != null && this.MayPlaySound())
				{
					this.doorAudio.PlayOneShot(this.CloseSound);
				}
				this.Close = false;
			}
			this.timer = 0f;
		}
	}

	public bool MayPlaySound()
	{
		return this.Floor == GameSettings.Instance.ActiveFloor && (CameraScript.Instance.Listener.transform.position - base.transform.position).sqrMagnitude < this.doorAudio.maxDistance * this.doorAudio.maxDistance;
	}

	public void DoorCollision(bool front)
	{
		if (this.Close)
		{
			this.timer = 1f - this.timer;
			this.Open = true;
			this.Close = false;
		}
		else if (this.Wait)
		{
			this.timer = 0f;
		}
		else if (!this.Open)
		{
			if (this.OpenSound != null && this.doorAudio != null && this.MayPlaySound())
			{
				this.doorAudio.PlayOneShot(this.OpenSound);
			}
			this.Open = true;
			this.Front = (this.Reverse ^ front);
		}
	}

	public float Speed = 2f;

	public float waitTime = 1f;

	public AudioClip OpenSound;

	public AudioClip CloseSound;

	private float timer;

	private bool Open;

	private bool Close;

	private bool Wait;

	private bool Front;

	public bool Scale;

	public bool Reverse;

	public bool DoorCloseSoundOnClose;

	public AudioSource doorAudio;

	public Quaternion OriginalRot;

	public RoomSegment Owner;
}
