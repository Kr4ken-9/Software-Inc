﻿using System;
using System.Linq;
using UnityEngine;

[Serializable]
public class PrintDeal : Deal
{
	public PrintDeal(SimulatedCompany company, SimulatedCompany.ProductPrototype product, float offer, uint goal) : base(company, false)
	{
		this.PID = product.ForceID();
		this.PerCopy = offer;
		this.Goal = goal;
		this.EndDate = product.ReleaseDate;
	}

	public PrintDeal()
	{
	}

	public override string ReputationCategory()
	{
		return null;
	}

	public override void Accept(Company company)
	{
		PrintJob printJob = new PrintJob(this.PID, this.ID);
		GameSettings.Instance.PrintOrders[this.PID] = printJob;
		HUD.Instance.distributionWindow.Show(printJob);
		this.StartDate = SDateTime.Now();
		base.Accept(company);
	}

	public override float Worth()
	{
		return this.PerCopy;
	}

	public override float Payout()
	{
		return 0f;
	}

	public override float ReputationEffect(bool ending)
	{
		return 0f;
	}

	public override string Description()
	{
		SimulatedCompany simulatedCompany = base.Client as SimulatedCompany;
		if (simulatedCompany == null)
		{
			return string.Empty;
		}
		SimulatedCompany.ProductPrototype productPrototype = simulatedCompany.Releases.FirstOrDefault((SimulatedCompany.ProductPrototype x) => x.SoftwareID == this.PID);
		return (productPrototype != null) ? "PrintDealTitle".Loc(new object[]
		{
			productPrototype.Name
		}) : string.Empty;
	}

	public override string Title()
	{
		return "Printing";
	}

	public override void HandleUpdate()
	{
	}

	public override bool StillValid(bool active)
	{
		return base.StillValid(active) && (active || Utilities.GetMonths(SDateTime.Now(), this.EndDate) > 1f);
	}

	public void FinalizeDeal()
	{
		if (this.CurrentAmount < this.Goal)
		{
			float num = Mathf.Max(0f, Utilities.GetMonths(SDateTime.Now(), this.EndDate));
			float num2 = Mathf.Max(0f, Utilities.GetMonths(this.StartDate, this.EndDate));
			float num3 = this.Goal / num2;
			string bill = this.Title();
			uint num4 = this.Goal - this.CurrentAmount;
			float num5 = num4 - num3 * num;
			if (num5 > 0f)
			{
				float num6 = num5 * MarketSimulation.PhysicalCopyPrice;
				base.Company.MakeTransaction(-num6, Company.TransactionCategory.Deals, bill);
				if (num5 > 1000f)
				{
					base.Company.ChangeBusinessRep(-0.25f, "Printing", 1f);
				}
				Company client = base.Client;
				if (client != null)
				{
					client.MakeTransaction(num6, Company.TransactionCategory.Deals, bill);
				}
				HUD.Instance.AddPopupMessage(string.Format("PrintDealFail2".Loc(), new object[]
				{
					num4.ToString("N0"),
					(client != null) ? client.Name : "N/A",
					num6.Currency(true),
					Mathf.Floor(num)
				}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Issue, 2f, PopupManager.PopupIDs.None, 1);
			}
		}
		else
		{
			base.Company.ChangeBusinessRep(this.Goal / 1E+08f, "Printing", 1f);
		}
		HUD.Instance.dealWindow.CancelDeal(this, false);
	}

	public override bool Cancel()
	{
		if (!base.Cancel())
		{
			return false;
		}
		GameSettings.Instance.PrintOrders.Remove(this.PID);
		return true;
	}

	public override string[] GetDetailedDescriptionVars()
	{
		return new string[]
		{
			"Amount".Loc(),
			"Per copy".Loc(),
			"Expires".Loc()
		};
	}

	public override string[] GetDetailedDescriptionValues()
	{
		return new string[]
		{
			this.Goal.ToString("N0"),
			this.PerCopy.Currency(true),
			this.EndDate.ToCompactString()
		};
	}

	public float PerCopy;

	public uint PID;

	public uint Goal;

	public uint CurrentAmount;

	public SDateTime EndDate;

	public SDateTime StartDate;
}
