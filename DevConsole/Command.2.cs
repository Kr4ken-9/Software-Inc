﻿using System;

namespace DevConsole
{
	public class Command<T0> : CommandBase
	{
		public Command(string name, Command<T0>.ConsoleMethod method) : base(name, method)
		{
		}

		public Command(string name, Command<T0>.ConsoleMethod method, string helpText) : base(name, method, helpText)
		{
		}

		public Command(string name, Command<T0>.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(name, method, helpMethod)
		{
		}

		public Command(Command<T0>.ConsoleMethod method) : base(method)
		{
		}

		public Command(Command<T0>.ConsoleMethod method, string helpText) : base(method, helpText)
		{
		}

		public Command(Command<T0>.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(method, helpMethod)
		{
		}

		protected override object[] ParseArguments(string args)
		{
			return new object[]
			{
				base.GetValueType<T0>(args)
			};
		}

		protected override string ArgumentList()
		{
			return typeof(T0).Name;
		}

		public delegate void ConsoleMethod(T0 arg0);
	}
}
