﻿using System;
using UnityEngine;

namespace DevConsole
{
	public class Vector2Command : CommandBase
	{
		public Vector2Command(string name, Vector2Command.ConsoleMethod method) : base(name, method)
		{
		}

		public Vector2Command(string name, Vector2Command.ConsoleMethod method, string helpText) : base(name, method, helpText)
		{
		}

		public Vector2Command(string name, Vector2Command.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(name, method, helpMethod)
		{
		}

		public Vector2Command(Vector2Command.ConsoleMethod method) : base(method)
		{
		}

		public Vector2Command(Vector2Command.ConsoleMethod method, string helpText) : base(method, helpText)
		{
		}

		public Vector2Command(Vector2Command.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(method, helpMethod)
		{
		}

		protected override object[] ParseArguments(string message)
		{
			object[] result;
			try
			{
				string[] array = message.Split(new char[]
				{
					' '
				});
				Vector2 vector = new Vector2(float.Parse(array[0]), float.Parse(array[1]));
				result = new object[]
				{
					vector
				};
			}
			catch
			{
				throw new ArgumentException("The entered value is not a valid");
			}
			return result;
		}

		protected override string ArgumentList()
		{
			return "2D vector";
		}

		public delegate void ConsoleMethod(Vector2 vector);
	}
}
