﻿using System;
using UnityEngine;

namespace DevConsole
{
	public class Vector3Command : CommandBase
	{
		public Vector3Command(string name, Vector3Command.ConsoleMethod method) : base(name, method)
		{
		}

		public Vector3Command(string name, Vector3Command.ConsoleMethod method, string helpText) : base(name, method, helpText)
		{
		}

		public Vector3Command(string name, Vector3Command.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(name, method, helpMethod)
		{
		}

		public Vector3Command(Vector3Command.ConsoleMethod method) : base(method)
		{
		}

		public Vector3Command(Vector3Command.ConsoleMethod method, string helpText) : base(method, helpText)
		{
		}

		public Vector3Command(Vector3Command.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(method, helpMethod)
		{
		}

		protected override object[] ParseArguments(string message)
		{
			object[] result;
			try
			{
				string[] array = message.Split(new char[]
				{
					' '
				});
				Vector3 vector = new Vector3(float.Parse(array[0]), float.Parse(array[1]), float.Parse(array[2]));
				result = new object[]
				{
					vector
				};
			}
			catch
			{
				throw new ArgumentException("The entered value is not a valid");
			}
			return result;
		}

		protected override string ArgumentList()
		{
			return "3D vector";
		}

		public delegate void ConsoleMethod(Vector3 vector);
	}
}
