﻿using System;

namespace DevConsole
{
	public class Command : CommandBase
	{
		public Command(string name, Command.ConsoleMethod method) : base(name, method)
		{
		}

		public Command(string name, Command.ConsoleMethod method, string helpText) : base(name, method, helpText)
		{
		}

		public Command(string name, Command.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(name, method, helpMethod)
		{
		}

		public Command(Command.ConsoleMethod method) : base(method)
		{
		}

		public Command(Command.ConsoleMethod method, string helpText) : base(method, helpText)
		{
		}

		public Command(Command.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(method, helpMethod)
		{
		}

		protected override object[] ParseArguments(string args)
		{
			return new object[0];
		}

		protected override string ArgumentList()
		{
			return "Nothing";
		}

		public delegate void ConsoleMethod();
	}
}
