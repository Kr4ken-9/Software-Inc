﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using StatementParser;
using UnityEngine;

namespace DevConsole
{
	[Serializable]
	public class Console : MonoBehaviour, ISerializationCallbackReceiver
	{
		public static bool verbose
		{
			get
			{
				return Console.Singleton.extraSettings.logVerbose;
			}
			set
			{
				Console.Singleton.extraSettings.logVerbose = value;
			}
		}

		public static bool isOpen
		{
			get
			{
				return Console.Singleton != null && !Console.Singleton.closed;
			}
		}

		private void Awake()
		{
			if (base.enabled)
			{
				if (Console.Singleton != null)
				{
					UnityEngine.Object.Destroy(this);
					Example component = base.GetComponent<Example>();
					if (component != null)
					{
						component.enabled = false;
					}
					return;
				}
				Console.Singleton = this;
				if (this.dontDestroyOnLoad)
				{
					UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
				}
				if (this.extraSettings.showDebugLog)
				{
					Application.logMessageReceived += this.LogCallback;
				}
			}
		}

		private void OnEnable()
		{
			if (this.consoleCommands == null)
			{
				this.consoleCommands = new List<CommandBase>();
			}
			if (this.extraSettings.defaultCommands)
			{
				CommandBase[] array = new CommandBase[9];
				array[0] = new Command("CLEAR", new Command.ConsoleMethod(this.Clear), "Clears the console");
				array[1] = new Command<bool>("SHOW_ON_ERROR", delegate(bool x)
				{
					Options.ConsoleOnError = x;
				}, "Toggles whether to show the console when an error occurs");
				array[2] = new Command<bool>("SHOW_TIMESTAMP", new Command<bool>.ConsoleMethod(this.ShowTimeStamp), "Establishes whether or not to show the time stamp for each command");
				array[3] = new Command("HELP", new Command.ConsoleMethod(this.HelpCommand), "Shows a list of all Commands available");
				array[4] = new Command<int>("SET_FONT_SIZE", new Command<int>.ConsoleMethod(this.SetFontSize), "Sets the font size of the Console");
				array[5] = new Command<bool>("SHOW_VERBOSE", new Command<bool>.ConsoleMethod(this.ShowVerbose), "Sets true or false the property Verbose");
				array[6] = new Command<bool>("showDebugLog", new Command<bool>.ConsoleMethod(this.ShowLog), "Establishes whether or not to show Unity Debug Log");
				int num = 7;
				string name = "exit";
				if (Console.<>f__mg$cache0 == null)
				{
					Console.<>f__mg$cache0 = new Command.ConsoleMethod(Application.Quit);
				}
				array[num] = new Command(name, Console.<>f__mg$cache0, "Exits the game");
				int num2 = 8;
				string name2 = "quit";
				if (Console.<>f__mg$cache1 == null)
				{
					Console.<>f__mg$cache1 = new Command.ConsoleMethod(Application.Quit);
				}
				array[num2] = new Command(name2, Console.<>f__mg$cache1, "Exits the game");
				Console.AddCommands(array);
			}
		}

		public void OnBeforeSerialize()
		{
			this.serializedConsoleText = this.consoleText.ToString();
		}

		public void OnAfterDeserialize()
		{
			this.consoleText.Append(this.serializedConsoleText);
		}

		private void OnGUI()
		{
			GUISkin skin = GUI.skin;
			if (this.extraSettings.skin != null)
			{
				GUI.skin = this.extraSettings.skin;
			}
			Event current = Event.current;
			GUI.skin.textArea.richText = true;
			if (this.extraSettings.font != null)
			{
				GUI.skin.font = this.extraSettings.font;
			}
			GUIStyle textArea = GUI.skin.textArea;
			int num = this.fontSize;
			GUI.skin.textField.fontSize = num;
			textArea.fontSize = num;
			if (current.type == EventType.KeyDown && ((!this.closed && !InputController.IsBound(InputController.Keys.NewConsole)) || InputController.EventKey(InputController.Keys.NewConsole, current, true)))
			{
				GUIUtility.keyboardControl = 0;
				InputController.InputEnabled = !this.closed;
				base.StartCoroutine(this.FadeInOut(this.closed));
			}
			bool flag = this.currentConsoleHeight != this.maxConsoleHeight && this.currentConsoleHeight != 0f;
			float lineHeight = GUI.skin.textArea.lineHeight;
			float num2 = lineHeight * (float)this.numLines;
			float height = (num2 <= this.currentConsoleHeight) ? this.currentConsoleHeight : num2;
			if (!this.closed)
			{
				for (int i = 0; i < this.buffer.Count; i++)
				{
					this.BasePrintOnGUI(this.buffer[i].Key, this.buffer[i].Value);
				}
				this.buffer.Clear();
				if (!flag)
				{
					GUI.FocusControl("TextField");
				}
				if (current.type == EventType.KeyDown)
				{
					if (!string.IsNullOrEmpty(this.inputText))
					{
						KeyCode keyCode = current.keyCode;
						if (keyCode != KeyCode.Tab)
						{
							if (keyCode != KeyCode.Return)
							{
								if (keyCode != KeyCode.Escape && keyCode != KeyCode.Space)
								{
									if (keyCode != KeyCode.Period)
									{
										if (keyCode == KeyCode.F1)
										{
											this.showHelp = true;
										}
									}
									else if (this.inputText.ToUpper().StartsWith("EXECUTE "))
									{
										this.showHelp = true;
									}
								}
								else
								{
									this.showHelp = false;
									this.candidates.Clear();
								}
							}
							else
							{
								this.PrintInput(this.inputText);
							}
						}
						else if (this.candidates.Count != 0)
						{
							if (this.inputText.ToUpper().StartsWith("EXECUTE "))
							{
								int cursorPos = this.GetCursorPos();
								int[] array = Console.DotSearchBack(this.inputText, Mathf.Clamp(cursorPos, 0, this.inputText.Length - 1));
								string text = this.candidates[this.selectedCandidate];
								this.inputText = this.inputText.Remove(array[1] + 1, cursorPos - array[1] - 1).Insert(array[1] + 1, text);
								this.candidates.Clear();
								this.SetCursorPos(this.inputText, array[1] + text.Length + 1);
							}
							else
							{
								this.inputText = this.candidates[this.selectedCandidate];
								this.showHelp = false;
								this.candidates.Clear();
								this.SetCursorPos(this.inputText, this.inputText.Length);
							}
						}
					}
					KeyCode keyCode2 = current.keyCode;
					if (keyCode2 != KeyCode.UpArrow)
					{
						if (keyCode2 == KeyCode.DownArrow)
						{
							int cursorPos2 = this.GetCursorPos();
							if ((this.inHistory || this.inputText == string.Empty) && this.history.Count != 0)
							{
								this.selectedHistory = Mathf.Clamp(this.selectedHistory - ((!this.inHistory) ? 0 : 1), 0, this.history.Count - 1);
								this.inputText = this.history[this.selectedHistory];
								this.showHelp = false;
								this.inHistory = true;
								this.lastText = this.inputText;
								this.SetCursorPos(this.inputText, this.inputText.Length);
							}
							else if (this.inputText != string.Empty && !this.inHistory)
							{
								this.selectedCandidate = Mathf.Clamp(++this.selectedCandidate, 0, this.candidates.Count - 1);
								if ((float)this.selectedCandidate * lineHeight > this.helpWindowScroll.y + lineHeight * (float)(this.numHelpCommandsToShow - 2) || (float)this.selectedCandidate * lineHeight < this.helpWindowScroll.y)
								{
									this.helpWindowScroll = new Vector2(0f, (float)this.selectedCandidate * lineHeight - (float)(this.numHelpCommandsToShow - 2) * lineHeight);
								}
								this.SetCursorPos(this.inputText, cursorPos2);
							}
						}
					}
					else
					{
						int cursorPos3 = this.GetCursorPos();
						if ((this.inHistory || this.inputText == string.Empty) && this.history.Count != 0)
						{
							this.selectedHistory = Mathf.Clamp(this.selectedHistory + ((!this.inHistory) ? 0 : 1), 0, this.history.Count - 1);
							this.inputText = this.history[this.selectedHistory];
							this.showHelp = false;
							this.inHistory = true;
							this.lastText = this.inputText;
							this.SetCursorPos(this.inputText, this.inputText.Length);
						}
						else if (this.inputText != string.Empty && !this.inHistory)
						{
							this.selectedCandidate = Mathf.Clamp(--this.selectedCandidate, 0, this.candidates.Count - 1);
							if ((float)this.selectedCandidate * lineHeight <= this.helpWindowScroll.y || (float)this.selectedCandidate * lineHeight > this.helpWindowScroll.y + lineHeight * (float)(this.numHelpCommandsToShow - 1))
							{
								this.helpWindowScroll = new Vector2(0f, (float)this.selectedCandidate * lineHeight - 1f * lineHeight);
							}
							this.SetCursorPos(this.inputText, cursorPos3);
						}
					}
				}
				if (this.lastText != this.inputText)
				{
					this.inHistory = false;
					this.lastText = string.Empty;
				}
				GUI.Box(new Rect(0f, 0f, (float)Screen.width, this.currentConsoleHeight), new GUIContent());
				GUI.SetNextControlName("TextField");
				GUI.enabled = !this.opening;
				this.inputText = GUI.TextField(new Rect(0f, this.currentConsoleHeight, (float)Screen.width, 25f), this.inputText, 1024);
				GUI.enabled = true;
				GUI.skin.textArea.normal.background = null;
				GUI.skin.textArea.hover.background = null;
				this.consoleScroll = GUI.BeginScrollView(new Rect(0f, 0f, (float)Screen.width, this.currentConsoleHeight), this.consoleScroll, new Rect(0f, 0f, (float)(Screen.width - 20), height));
				GUI.TextArea(new Rect(0f, -5f + this.currentConsoleHeight - ((this.numLines != 0) ? num2 : lineHeight) + (((float)this.numLines < this.numLinesThreshold - 1f) ? 0f : (lineHeight * ((float)this.numLines - this.numLinesThreshold))), (float)Screen.width, 7f + ((this.numLines != 0) ? num2 : lineHeight)), this.consoleText.ToString());
				GUI.EndScrollView();
				if (this.inputText == string.Empty)
				{
					this.showHelp = true;
				}
			}
			if (this.showHelp && this.helpEnabled && this.inputText.Trim() != string.Empty)
			{
				this.ShowHelp();
				if (this.candidates.Count != 0)
				{
					string text2 = string.Empty;
					foreach (string text3 in this.candidates)
					{
						text2 = text2 + ((!(this.candidates[this.selectedCandidate] == text3)) ? text3 : ("<color=yellow>" + text3 + "</color>")) + '\n';
					}
					GUI.skin.textArea.normal.background = GUI.skin.textField.normal.background;
					GUI.skin.textArea.hover.background = GUI.skin.textField.hover.background;
					if (this.candidates.Count > this.numHelpCommandsToShow)
					{
						this.helpWindowScroll = GUI.BeginScrollView(new Rect(0f, this.currentConsoleHeight - (float)this.numHelpCommandsToShow * lineHeight - 7f, this.helpWindowWidth, 5f + lineHeight * (float)this.numHelpCommandsToShow), this.helpWindowScroll, new Rect(0f, 0f, this.helpWindowWidth - 20f, 7f + (float)this.candidates.Count * lineHeight));
						GUI.TextArea(new Rect(0f, 0f, this.helpWindowWidth, 7f + (float)this.candidates.Count * lineHeight), text2);
						GUI.EndScrollView();
					}
					else
					{
						GUI.TextArea(new Rect(0f, this.currentConsoleHeight - 7f - ((this.candidates.Count <= this.numHelpCommandsToShow) ? (lineHeight * (float)this.candidates.Count) : ((float)this.numHelpCommandsToShow * lineHeight)), this.helpWindowWidth, ((this.candidates.Count <= this.numHelpCommandsToShow) ? (lineHeight * (float)this.candidates.Count) : ((float)this.numHelpCommandsToShow * lineHeight)) + 7f), text2);
					}
				}
			}
			GUI.skin = skin;
		}

		public static void Open()
		{
			if (Console.Singleton == null || Console.isOpen)
			{
				return;
			}
			InputController.InputEnabled = false;
			GUIUtility.keyboardControl = 0;
			Console.Singleton.StartCoroutine(Console.Singleton.FadeInOut(true));
		}

		public static void Close()
		{
			if (Console.Singleton == null || !Console.isOpen)
			{
				return;
			}
			GUIUtility.keyboardControl = 0;
			InputController.InputEnabled = true;
			Console.Singleton.StartCoroutine(Console.Singleton.FadeInOut(false));
		}

		private IEnumerator FadeInOut(bool open)
		{
			if (this.opening)
			{
				yield break;
			}
			this.opening = true;
			this.maxConsoleHeight = (float)(Screen.height / 3);
			this.numLinesThreshold = this.maxConsoleHeight / this.extraSettings.skin.textArea.lineHeight;
			this.closed = false;
			float duration = this.extraSettings.curve[this.extraSettings.curve.length - 1].time;
			float time = 0f;
			do
			{
				this.currentConsoleHeight = this.maxConsoleHeight * this.extraSettings.curve.Evaluate((!open) ? (duration - time) : time);
				yield return null;
				time = Mathf.Clamp(time + Time.deltaTime, 0f, duration);
			}
			while (time < duration && !Mathf.Approximately(time, duration));
			this.currentConsoleHeight = ((!open) ? 0f : (this.maxConsoleHeight * this.extraSettings.curve.Evaluate(time)));
			this.closed = !open;
			if (this.closed)
			{
				this.inputText = string.Empty;
			}
			this.opening = false;
			yield break;
		}

		private void ShowHelp()
		{
			string text = string.Empty;
			if (this.candidates.Count != 0 && this.selectedCandidate >= 0 && this.candidates.Count > this.selectedCandidate)
			{
				text = this.candidates[this.selectedCandidate];
			}
			this.candidates.Clear();
			if (this.inputText.ToUpper().StartsWith("EXECUTE "))
			{
				string text2 = this.inputText.Substring(8);
				int num = this.GetCursorPos() - 9;
				int[] array = Console.DotSearchBack(text2, Mathf.Clamp(num, 0, text2.Length - 1));
				if (array[1] > -1)
				{
					try
					{
						LineParse.TreeNode treeNode = LineParse.Parse(text2.Substring(array[0], array[1] - array[0]));
						string text3 = text2.Substring(array[1] + 1, num - array[1]);
						Type type = LineParse.GetType(treeNode, null, new Example.ParserWorld());
						if (type != null)
						{
							string rr = text3.ToUpper();
							HashSet<string> hashSet = (from x in type.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public)
							where !x.IsSpecialName
							select x.Name).ToHashSet<string>();
							hashSet.AddRange(from x in type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public)
							where !x.IsSpecialName
							select x.Name);
							hashSet.AddRange(from x in type.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public)
							where !x.IsSpecialName
							select x.Name + "(");
							this.candidates.AddRange(from x in hashSet
							where x.ToUpper().StartsWith(rr)
							orderby x
							select x);
							if (this.candidates.Count == 1 && this.candidates[0] == text3)
							{
								this.candidates.Clear();
							}
						}
					}
					catch (Exception ex)
					{
					}
				}
			}
			else
			{
				for (int i = 0; i < this.consoleCommands.Count; i++)
				{
					if (this.consoleCommands[i].name.ToUpper().StartsWith(this.inputText.ToUpper()))
					{
						this.candidates.Add(this.consoleCommands[i].name);
					}
				}
			}
			if (text == string.Empty)
			{
				this.selectedCandidate = 0;
				return;
			}
			for (int j = 0; j < this.candidates.Count; j++)
			{
				if (this.candidates[j] == text)
				{
					this.selectedCandidate = j;
					return;
				}
			}
			this.selectedCandidate = 0;
		}

		private void SetCursorPos(string text, int pos)
		{
			TextEditor textEditor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
			textEditor.text = text;
			textEditor.cursorIndex = pos;
			textEditor.selectIndex = pos;
			GUIUtility.ExitGUI();
		}

		private int GetCursorPos()
		{
			TextEditor textEditor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
			return textEditor.cursorIndex;
		}

		public static string ColorToHex(Color color)
		{
			string text = "0123456789ABCDEF";
			int num = (int)(color.r * 255f);
			int num2 = (int)(color.g * 255f);
			int num3 = (int)(color.b * 255f);
			return string.Concat(new string[]
			{
				text[(int)Mathf.Floor((float)(num / 16))].ToString(),
				text[(int)Mathf.Round((float)(num % 16))].ToString(),
				text[(int)Mathf.Floor((float)(num2 / 16))].ToString(),
				text[(int)Mathf.Round((float)(num2 % 16))].ToString(),
				text[(int)Mathf.Floor((float)(num3 / 16))].ToString(),
				text[(int)Mathf.Round((float)(num3 % 16))].ToString()
			});
		}

		public static int[] DotSearchBack(string input, int pos)
		{
			int[] array = new int[]
			{
				pos,
				(input[pos] != '.') ? -1 : pos
			};
			int num = 0;
			while (array[0] > 0)
			{
				array[0]--;
				char c = input[array[0]];
				if (num > 0)
				{
					if (c == '(' || c == '[')
					{
						num--;
					}
				}
				else if (c == ')' || c == ']')
				{
					num++;
				}
				else if (c == '.' && array[1] == -1)
				{
					array[1] = array[0];
				}
				else if (!char.IsDigit(c) && !char.IsLetter(c) && c != '.' && c != '$')
				{
					array[0]++;
					break;
				}
			}
			return array;
		}

		public static void Log(string text)
		{
			if (Console.Singleton != null)
			{
				Console.Singleton.BasePrint(text);
			}
		}

		public static void Log(object obj)
		{
			Console.Log(obj.ToString());
		}

		public static void LogInfo(string text)
		{
			if (Console.Singleton != null)
			{
				Console.Singleton.BasePrint(text, Color.cyan);
			}
		}

		public static void LogInfo(object obj)
		{
			Console.LogInfo(obj.ToString());
		}

		public static void LogWarning(string text)
		{
			if (Console.Singleton != null)
			{
				Console.Singleton.BasePrint(text, Color.yellow);
			}
		}

		public static void LogWarning(object obj)
		{
			Console.LogWarning(obj.ToString());
		}

		public static void LogError(string text)
		{
			if (Console.Singleton != null)
			{
				Console.Singleton.BasePrint(text, Color.red);
				if (Options.ConsoleOnError && !Console.isOpen)
				{
					Console.Open();
				}
			}
		}

		public static void LogError(object obj)
		{
			Console.LogError(obj.ToString());
		}

		public static void Log(string text, string color)
		{
			if (Console.Singleton != null)
			{
				Console.Singleton.BasePrint(text, color);
			}
		}

		public static void Log(object obj, string color)
		{
			Console.Log(obj.ToString(), color);
		}

		public static void Log(string text, Color color)
		{
			if (Console.Singleton != null)
			{
				Console.Singleton.BasePrint(text, color);
			}
		}

		public static void Log(object obj, Color color)
		{
			Console.Log(obj.ToString(), color);
		}

		private void BasePrint(string text)
		{
			this.BasePrint(text, Console.ColorToHex(Color.white));
		}

		private void BasePrint(string text, Color color)
		{
			this.BasePrint(text, Console.ColorToHex(color));
		}

		private void BasePrint(string text, string color)
		{
			this.buffer.Add(new KeyValuePair<string, string>(text, color));
		}

		private void BasePrintOnGUI(string text, string color)
		{
			text = "> " + text;
			int num = 1;
			string value = (!this.showTimeStamp) ? string.Empty : ("[" + DateTime.Now.ToShortTimeString() + "]  ");
			StringBuilder stringBuilder = new StringBuilder(value);
			for (int i = 0; i < text.Length; i++)
			{
				if (text[i] == '\n')
				{
					num++;
					stringBuilder = new StringBuilder(value);
				}
				else
				{
					stringBuilder.Append(text[i]);
				}
				if (GUI.skin.textArea.CalcSize(new GUIContent(stringBuilder.ToString())).x > (float)(Screen.width - 20))
				{
					text = text.Insert(i, "\n");
					i--;
				}
			}
			text += '\n';
			this.numLines += num;
			if ((float)this.numLines >= this.numLinesThreshold - 1f)
			{
				this.consoleScroll = new Vector2(0f, this.consoleScroll.y + 2.14748365E+09f);
			}
			this.AddText(text, color);
			if (this.consoleText.Length >= 16000)
			{
				this.Clear();
				this.AddText("Buffer cleared automatically\n", Console.ColorToHex(Color.yellow));
			}
			else if (this.consoleText.Length >= 15000)
			{
				this.AddText("Buffer size too large. You should clear the console\n", Console.ColorToHex(Color.red));
			}
			else if (this.consoleText.Length >= 14000)
			{
				this.AddText("Buffer size too large. You should clear the console\n", Console.ColorToHex(Color.yellow));
			}
		}

		private void AddText(string text, string color)
		{
			this.consoleText.Append(string.Format("{0}<color=#{1}>{2}</color>", (!this.showTimeStamp) ? string.Empty : ("[" + DateTime.Now.ToShortTimeString() + "]  "), color, text));
		}

		private void PrintInput(string input)
		{
			this.inputText = string.Empty;
			if ((this.history.Count == 0 || this.history[0] != input) && input.Trim() != string.Empty)
			{
				this.history.Insert(0, input);
			}
			this.selectedHistory = 0;
			this.BasePrint(input);
			this.ExecuteCommandInternal(input);
		}

		private void LogCallback(string log, string stackTrace, LogType type)
		{
			if (type != LogType.Error && type != LogType.Exception)
			{
				if (type != LogType.Assert)
				{
					return;
				}
			}
			try
			{
				this.BasePrint(log, Color.red);
				this.BasePrint(stackTrace, Color.red);
				if (Options.ConsoleOnError && !Console.isOpen)
				{
					Console.Open();
				}
			}
			catch (Exception ex)
			{
				Console.Log(ex.ToString(), Color.cyan);
			}
		}

		public static void ExecuteCommand(string command)
		{
			Console.Singleton.ExecuteCommandInternal(command);
		}

		public static void ExecuteCommand(string command, string args)
		{
			Console.Singleton.ExecuteCommandInternal(command + " " + args);
		}

		private void ExecuteCommandInternal(string command)
		{
			for (int i = 0; i < this.consoleCommands.Count; i++)
			{
				if (command.ToUpper().StartsWith(this.consoleCommands[i].name.ToUpper()))
				{
					if (command.ToUpper() == this.consoleCommands[i].name.ToUpper() + "?")
					{
						this.consoleCommands[i].ShowHelp();
					}
					else
					{
						this.consoleCommands[i].Execute(command.Substring(this.consoleCommands[i].name.Length + ((!command.Contains(" ")) ? 0 : 1)));
					}
				}
			}
		}

		public static void AddCommands(params CommandBase[] cs)
		{
			foreach (CommandBase c in cs)
			{
				Console.AddCommand(c);
			}
		}

		public static void AddCommand(CommandBase c)
		{
			if (!Console.CommandExists(c.name) && Console.Singleton != null)
			{
				Console.Singleton.consoleCommands.Add(c);
			}
		}

		private static bool CommandExists(string commandName)
		{
			foreach (CommandBase commandBase in Console.Singleton.consoleCommands)
			{
				if (commandBase.name.ToUpper() == commandName.ToUpper())
				{
					Console.LogError("The command " + commandName + " already exists");
					return true;
				}
			}
			return false;
		}

		public static void RemoveCommand(string commandName)
		{
			foreach (CommandBase commandBase in Console.Singleton.consoleCommands)
			{
				if (commandBase.name == commandName)
				{
					Console.Singleton.consoleCommands.Remove(commandBase);
					Console.Log("Command " + commandName + " removed successfully", Color.green);
					return;
				}
			}
			Console.LogWarning("The command " + commandName + " could not be found");
		}

		private void HelpCommand()
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this.consoleCommands.Count; i++)
			{
				stringBuilder.Append(this.consoleCommands[i].name + ((this.consoleCommands[i].helpText != null) ? (": " + this.consoleCommands[i].helpText) : string.Empty));
				stringBuilder.Append('\n');
			}
			Console.LogInfo("\n" + stringBuilder.ToString());
		}

		private void Clear()
		{
			Console.Singleton.consoleText = new StringBuilder();
			Console.Singleton.numLines = 0;
		}

		private void ShowVerbose(bool show)
		{
			Console.verbose = show;
			Console.Log("Changed Succesful", Color.green);
		}

		private void ShowLog(bool value)
		{
			if (value)
			{
				Application.logMessageReceived += this.LogCallback;
			}
			else
			{
				Application.logMessageReceived -= this.LogCallback;
			}
			Console.Log("Change successful", Color.green);
		}

		private void ShowTimeStamp(bool value)
		{
			this.showTimeStamp = value;
			Console.Log("Change successful", Color.green);
		}

		private void SetFontSize(int size)
		{
			this.fontSize = size;
			Console.Log("Change successful", Color.green);
		}

		public static string[] SplitString(string input)
		{
			int num = input.CountLetter('"');
			if (num % 2 == 1)
			{
				throw new ArgumentException("Uneven amount of quotations");
			}
			if (num == 0)
			{
				return input.Split(new char[]
				{
					' '
				});
			}
			List<string> list = new List<string>();
			string[] array = input.Split(new char[]
			{
				'"'
			}, StringSplitOptions.None);
			for (int i = 0; i < array.Length; i++)
			{
				string text = array[i].Trim();
				if (i % 2 == 1)
				{
					list.Add(text);
				}
				else if (!string.IsNullOrEmpty(text))
				{
					list.AddRange(text.Split(new char[]
					{
						' '
					}));
				}
			}
			return list.ToArray();
		}

		[SerializeField]
		private bool dontDestroyOnLoad;

		[Range(8f, 20f)]
		public int fontSize;

		private const int TEXT_AREA_OFFSET = 7;

		private bool helpEnabled = true;

		private int numHelpCommandsToShow = 5;

		private float helpWindowWidth = 256f;

		private const int WARNING_THRESHOLD = 14000;

		private const int DANGER_THRESHOLD = 15000;

		private const int AUTOCLEAR_THRESHOLD = 16000;

		private List<CommandBase> consoleCommands;

		private List<string> candidates = new List<string>();

		private int selectedCandidate;

		private List<string> history = new List<string>();

		private int selectedHistory;

		private List<KeyValuePair<string, string>> buffer = new List<KeyValuePair<string, string>>();

		public static Console Singleton;

		private bool opening;

		private bool closed = true;

		private bool showHelp = true;

		private bool inHistory;

		private bool showTimeStamp;

		private float numLinesThreshold;

		private float maxConsoleHeight;

		private float currentConsoleHeight;

		private Vector2 consoleScroll = Vector2.zero;

		private Vector2 helpWindowScroll = Vector2.zero;

		[HideInInspector]
		[SerializeField]
		private string serializedConsoleText = string.Empty;

		private StringBuilder consoleText = new StringBuilder();

		private string inputText = string.Empty;

		private string lastText = string.Empty;

		private int numLines;

		[SerializeField]
		private Settings extraSettings;

		[CompilerGenerated]
		private static Command.ConsoleMethod <>f__mg$cache0;

		[CompilerGenerated]
		private static Command.ConsoleMethod <>f__mg$cache1;
	}
}
