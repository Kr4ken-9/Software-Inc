﻿using System;

namespace DevConsole
{
	[Serializable]
	public abstract class CommandBase
	{
		public CommandBase(string name, Delegate method)
		{
			this.name = name;
			this.method = method;
		}

		public CommandBase(string name, Delegate method, string helpText) : this(name, method)
		{
			this.helpText = helpText;
		}

		public CommandBase(string name, Delegate method, CommandBase.HelpMethod helpMethod) : this(name, method)
		{
			this.helpMethod = helpMethod;
		}

		public CommandBase(Delegate method) : this(method.Method.DeclaringType.Name + "." + method.Method.Name, method)
		{
		}

		public CommandBase(Delegate method, string helpText) : this(method.Method.DeclaringType.Name + "." + method.Method.Name, method, helpText)
		{
		}

		public CommandBase(Delegate method, CommandBase.HelpMethod helpMethod) : this(method.Method.DeclaringType.Name + "." + method.Method.Name, method, helpMethod)
		{
		}

		public string name { get; private set; }

		public string helpText { get; private set; }

		public void Execute(string args)
		{
			try
			{
				this.method.Method.Invoke(this.method.Target, this.ParseArguments(args));
			}
			catch (Exception innerException)
			{
				if (innerException is ArgumentException)
				{
					Console.LogError("Expected: " + this.ArgumentList());
				}
				else
				{
					while (innerException.InnerException != null)
					{
						innerException = innerException.InnerException;
					}
					Console.LogError(innerException.Message + ((!Console.verbose) ? string.Empty : ("\n" + innerException.StackTrace)));
				}
			}
		}

		protected abstract object[] ParseArguments(string args);

		protected abstract string ArgumentList();

		public virtual void ShowHelp()
		{
			if (this.helpMethod != null)
			{
				this.helpMethod();
			}
			else
			{
				Console.LogInfo("Command Info: " + ((this.helpText != null) ? this.helpText : "There's no help for this command"));
			}
		}

		protected T GetValueType<T>(string arg)
		{
			Type typeFromHandle = typeof(T);
			T result;
			try
			{
				T t;
				if (typeof(bool) == typeFromHandle)
				{
					bool flag;
					if (!this.StringToBool(arg, out flag))
					{
						throw new ArgumentException("The entered value is not a valid");
					}
					t = (T)((object)flag);
				}
				else
				{
					t = (T)((object)Convert.ChangeType(arg, typeFromHandle));
				}
				result = t;
			}
			catch
			{
				throw new ArgumentException("The entered value is not a valid");
			}
			return result;
		}

		protected bool StringToBool(string value, out bool result)
		{
			bool flag = result = false;
			int num = 0;
			if (bool.TryParse(value, out flag))
			{
				result = flag;
			}
			else if (int.TryParse(value, out num))
			{
				if (num != 1 && num != 0)
				{
					return false;
				}
				result = (num == 1);
			}
			else
			{
				string text = value.ToLower().Trim();
				if (text.Equals("yes") || text.Equals("y"))
				{
					result = true;
				}
				else
				{
					if (!text.Equals("no") && !text.Equals("n"))
					{
						return false;
					}
					result = false;
				}
			}
			return true;
		}

		private CommandBase.HelpMethod helpMethod;

		private Delegate method;

		public delegate void HelpMethod();
	}
}
