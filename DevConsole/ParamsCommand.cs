﻿using System;

namespace DevConsole
{
	public class ParamsCommand<T0> : CommandBase
	{
		public ParamsCommand(string name, ParamsCommand<T0>.ConsoleMethod method) : base(name, method)
		{
		}

		public ParamsCommand(string name, ParamsCommand<T0>.ConsoleMethod method, string helpText) : base(name, method, helpText)
		{
		}

		public ParamsCommand(string name, ParamsCommand<T0>.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(name, method, helpMethod)
		{
		}

		public ParamsCommand(ParamsCommand<T0>.ConsoleMethod method) : base(method)
		{
		}

		public ParamsCommand(ParamsCommand<T0>.ConsoleMethod method, string helpText) : base(method, helpText)
		{
		}

		public ParamsCommand(ParamsCommand<T0>.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(method, helpMethod)
		{
		}

		protected override object[] ParseArguments(string message)
		{
			string[] array = Console.SplitString(message);
			T0[] array2 = new T0[array.Length];
			for (int i = 0; i < array2.Length; i++)
			{
				array2[i] = base.GetValueType<T0>(array[i]);
			}
			return new object[]
			{
				array2
			};
		}

		protected override string ArgumentList()
		{
			return typeof(T0).Name + " array";
		}

		public delegate void ConsoleMethod(params T0[] arg0);
	}
}
