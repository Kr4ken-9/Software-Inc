﻿using System;

namespace DevConsole
{
	public class Command<T0, T1> : CommandBase
	{
		public Command(string name, Command<T0, T1>.ConsoleMethod method) : base(name, method)
		{
		}

		public Command(string name, Command<T0, T1>.ConsoleMethod method, string helpText) : base(name, method, helpText)
		{
		}

		public Command(string name, Command<T0, T1>.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(name, method, helpMethod)
		{
		}

		public Command(Command<T0, T1>.ConsoleMethod method) : base(method)
		{
		}

		public Command(Command<T0, T1>.ConsoleMethod method, string helpText) : base(method, helpText)
		{
		}

		public Command(Command<T0, T1>.ConsoleMethod method, CommandBase.HelpMethod helpMethod) : base(method, helpMethod)
		{
		}

		protected override object[] ParseArguments(string message)
		{
			string[] array = Console.SplitString(message);
			if (array.Length < 2)
			{
				throw new ArgumentException("Not enough arguments");
			}
			return new object[]
			{
				base.GetValueType<T0>(array[0]),
				base.GetValueType<T1>(array[1])
			};
		}

		protected override string ArgumentList()
		{
			return typeof(T0).Name + ", " + typeof(T1).Name;
		}

		public delegate void ConsoleMethod(T0 arg0, T1 arg1);
	}
}
