﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using ClipperLib;
using UnityEngine;

public class RoomManager
{
	public bool TeamText
	{
		get
		{
			return this._teamText;
		}
		set
		{
			this._teamText = value;
			this.Rooms.ForEach(delegate(Room x)
			{
				x.TeamText.gameObject.SetActive(value);
				x.RoleText.gameObject.SetActive(value);
			});
		}
	}

	public List<KeyValuePair<Room, int>> GetConnectedRooms(Room r)
	{
		if (r != null)
		{
			object bfslock = this.BFSLock;
			lock (bfslock)
			{
				List<KeyValuePair<Room, int>> result;
				if (this.ConnectedRooms.TryGetValue(r, out result))
				{
					return result;
				}
			}
		}
		return this.EmptyConnection;
	}

	public void UpdateFurnitureCombines()
	{
		foreach (int num in this.DirtyCombineFloors)
		{
			FloorFurnitureMesh floorFurnitureMesh;
			if (!this.CombinedFurnitureMeshes.TryGetValue(num, out floorFurnitureMesh))
			{
				floorFurnitureMesh = new FloorFurnitureMesh(num);
				this.CombinedFurnitureMeshes[num] = floorFurnitureMesh;
			}
			floorFurnitureMesh.Refresh(this.Rooms);
		}
		this.DirtyCombineFloors.Clear();
	}

	public void UpdateSupport(int floor)
	{
		Dictionary<Room, Vector2[]> dictionary = new Dictionary<Room, Vector2[]>();
		foreach (Room room in from x in this.Rooms
		where x.Floor == floor && !x.Outdoors
		select x)
		{
			List<List<IntPoint>> list = Clipper.SimplifyPolygon(room.GetExpanded(-4.01f).Select((Vector2 x) => new IntPoint((double)(x.x * 100f), (double)(x.y * 100f))).ToList<IntPoint>(), 2);
			if (list.Count > 0)
			{
				dictionary[room] = list[0].Select((IntPoint x) => new Vector2((float)x.X / 100f, (float)x.Y / 100f)).ToArray<Vector2>();
			}
		}
		this.RoomSupport[floor] = dictionary;
	}

	public void UpdateSupport(Room r)
	{
		if (r.Floor > -1)
		{
			Dictionary<Room, Vector2[]> dictionary;
			if (!this.RoomSupport.TryGetValue(r.Floor, out dictionary))
			{
				dictionary = (this.RoomSupport[r.Floor] = new Dictionary<Room, Vector2[]>());
			}
			List<List<IntPoint>> list = Clipper.SimplifyPolygon((from x in r.GetExpanded(-4.01f)
			select new IntPoint((double)(x.x * 100f), (double)(x.y * 100f))).ToList<IntPoint>(), 2);
			if (list.Count > 0)
			{
				dictionary[r] = (from x in list[0]
				select new Vector2((float)x.X / 100f, (float)x.Y / 100f)).ToArray<Vector2>();
			}
		}
	}

	public void ChangeFloor()
	{
		for (int i = 0; i < this.Rooms.Count; i++)
		{
			this.Rooms[i].UpdateVisibility();
			this.Rooms[i].UpdateAllMaterials();
		}
		foreach (KeyValuePair<int, FloorFurnitureMesh> keyValuePair in this.CombinedFurnitureMeshes)
		{
			keyValuePair.Value.UpdateVisbility();
		}
		RoadManager.Instance.UpdateRoadVisibility();
		RoomMaterialController.UpdateCutoffMat();
		GameSettings.Instance.UpdateGridVisibility();
	}

	public void UpdateStates()
	{
		for (int i = 0; i < RoomManager.MaxRoomsPerUpdate; i++)
		{
			this.UpdateRooms[i] = null;
		}
		for (int j = 0; j < this.Rooms.Count; j++)
		{
			if (this.Rooms[j].AccRefreshTime > 0f)
			{
				for (int k = 0; k < RoomManager.MaxRoomsPerUpdate; k++)
				{
					if (this.UpdateRooms[k] == null || this.Rooms[j].AccRefreshTime > this.UpdateRooms[k].AccRefreshTime)
					{
						this.UpdateRooms[k] = this.Rooms[j];
						break;
					}
				}
			}
			this.Rooms[j].AccRefreshTime += Time.deltaTime;
		}
		for (int l = 0; l < RoomManager.MaxRoomsPerUpdate; l++)
		{
			if (this.UpdateRooms[l] != null)
			{
				this.UpdateRooms[l].UpdateFrameState();
				this.UpdateRooms[l].AccRefreshTime = 0f;
			}
		}
	}

	public IEnumerable<WallEdge> GetEdgesOnFloor(int floor)
	{
		foreach (WallEdge x in this.AllSegments)
		{
			if (x.Floor == floor)
			{
				yield return x;
			}
		}
		yield break;
	}

	public Room RoomUnderMouse()
	{
		Ray ray = CameraScript.Instance.mainCam.ScreenPointToRay(Input.mousePosition);
		float distance = 0f;
		Plane plane = new Plane(Vector3.up, new Vector3(0f, (float)(GameSettings.Instance.ActiveFloor * 2), 0f));
		if (plane.Raycast(ray, out distance))
		{
			Vector3 point = ray.GetPoint(distance);
			Vector2 point2 = new Vector2(point.x, point.z);
			foreach (Room room in from x in this.Rooms
			where x.Floor == GameSettings.Instance.ActiveFloor
			select x)
			{
				if (room.RoomBounds.Contains(point2))
				{
					return room;
				}
			}
		}
		return null;
	}

	public RoomSegment[] GetAllSegments()
	{
		return this.RoomSegments.ToArray<RoomSegment>();
	}

	public void ClearReservations(Actor actor)
	{
		foreach (Furniture furniture in actor.ReservedFurniture.ToList<Furniture>())
		{
			if (furniture.Reserved == actor)
			{
				furniture.Reserved = null;
			}
		}
	}

	public void RecalculateNearRooms()
	{
		if (!this.DisableMeshRebuild && !this.BFSStarted)
		{
			if (this.Outside.NavmeshRebuildStarted)
			{
				return;
			}
			for (int i = 0; i < this.Rooms.Count; i++)
			{
				if (this.Rooms[i].NavmeshRebuildStarted)
				{
					return;
				}
			}
			this.RoomNearnessDirty = false;
			GameSettings.Instance.sRoomManager.TemperatureControlDirty = true;
			this.BFSStarted = true;
			Thread thread = new Thread(delegate
			{
				try
				{
					this.BFS();
					object bfslock = this.BFSLock;
					lock (bfslock)
					{
						foreach (KeyValuePair<Room, List<KeyValuePair<Room, int>>> keyValuePair in this.ConnectedRooms)
						{
							if (!(keyValuePair.Key == this.Outside))
							{
								keyValuePair.Key.Accessible = false;
								for (int j = 0; j < keyValuePair.Value.Count; j++)
								{
									if (keyValuePair.Value[j].Key == this.Outside)
									{
										keyValuePair.Key.Accessible = true;
										break;
									}
								}
							}
						}
					}
				}
				catch (Exception ex)
				{
					ErrorLogging.AddException(ex);
					this.BFSStarted = false;
					throw;
				}
				this.BFSStarted = false;
			});
			thread.Start();
		}
	}

	private void BFS()
	{
		RoomManager.<BFS>c__AnonStorey3 <BFS>c__AnonStorey = new RoomManager.<BFS>c__AnonStorey3();
		<BFS>c__AnonStorey.$this = this;
		<BFS>c__AnonStorey.rooms = this.Rooms.ToList<Room>();
		<BFS>c__AnonStorey.rooms.Add(this.Outside);
		RoomManager.<BFS>c__AnonStorey3 <BFS>c__AnonStorey2 = <BFS>c__AnonStorey;
		IEnumerable<Room> rooms = <BFS>c__AnonStorey.rooms;
		Func<Room, Room> keySelector = (Room x) => x;
		if (RoomManager.<>f__mg$cache0 == null)
		{
			RoomManager.<>f__mg$cache0 = new Func<Room, HashSet<Room>>(RoomManager.GetAllConnectedRooms);
		}
		<BFS>c__AnonStorey2.conn = rooms.ToDictionary(keySelector, RoomManager.<>f__mg$cache0);
		List<Thread> list = new List<Thread>();
		object bfslock = this.BFSLock;
		lock (bfslock)
		{
			this.ConnectedRooms.Clear();
		}
		for (int i = 0; i < <BFS>c__AnonStorey.rooms.Count; i++)
		{
			Room r = <BFS>c__AnonStorey.rooms[i];
			Thread thread = new Thread(delegate
			{
				try
				{
					Dictionary<Room, RoomManager.BFSNode<Room>> dictionary = <BFS>c__AnonStorey.rooms.ToDictionary((Room x) => x, (Room x) => new RoomManager.BFSNode<Room>(x));
					Queue<RoomManager.BFSNode<Room>> queue = new Queue<RoomManager.BFSNode<Room>>();
					RoomManager.BFSNode<Room> bfsnode = dictionary[r];
					queue.Clear();
					bfsnode.Distance = 0;
					queue.Enqueue(bfsnode);
					while (queue.Count > 0)
					{
						RoomManager.BFSNode<Room> bfsnode2 = queue.Dequeue();
						HashSet<Room> hashSet;
						if (<BFS>c__AnonStorey.conn.TryGetValue(bfsnode2.Value, out hashSet))
						{
							foreach (Room key in hashSet)
							{
								RoomManager.BFSNode<Room> bfsnode3;
								if (dictionary.TryGetValue(key, out bfsnode3) && bfsnode3.Distance == 2147483647)
								{
									bfsnode3.Distance = bfsnode2.Distance + 1;
									bfsnode3.Parent = bfsnode2;
									queue.Enqueue(bfsnode3);
								}
							}
						}
					}
					object bfslock2 = <BFS>c__AnonStorey.$this.BFSLock;
					lock (bfslock2)
					{
						<BFS>c__AnonStorey.$this.ConnectedRooms[r] = (from x in dictionary.Values
						where x.Distance != int.MaxValue
						orderby x.Distance
						select new KeyValuePair<Room, int>(x.Value, x.Distance)).ToList<KeyValuePair<Room, int>>();
					}
				}
				catch (Exception ex)
				{
					List<KeyValuePair<Room, int>> list2 = new List<KeyValuePair<Room, int>>();
					list2.Add(new KeyValuePair<Room, int>(r, 0));
					object bfslock3 = <BFS>c__AnonStorey.$this.BFSLock;
					lock (bfslock3)
					{
						<BFS>c__AnonStorey.$this.ConnectedRooms[r] = list2;
					}
					ErrorLogging.AddException(ex);
					throw;
				}
			});
			thread.Start();
			list.Add(thread);
		}
		list.JoinThreads();
	}

	private float GetDistance(Dictionary<KeyValuePair<Room, Room>, float> dist, KeyValuePair<Room, Room> key)
	{
		float result = 0f;
		if (dist.TryGetValue(key, out result))
		{
			return result;
		}
		return float.PositiveInfinity;
	}

	private static HashSet<Room> GetAllConnectedRooms(Room room)
	{
		HashSet<Room> hashSet = new HashSet<Room>();
		object pathNodes = room.PathNodes;
		lock (pathNodes)
		{
			for (int i = 0; i < room.PathNodes.Count; i++)
			{
				SHashSet<PathNode<Vector3>> connections = room.PathNodes[i].GetConnections();
				foreach (PathNode<Vector3> pathNode in connections)
				{
					SHashSet<PathNode<Vector3>> connections2 = pathNode.GetConnections();
					hashSet.AddRange((from x in connections2
					select x.Tag).OfType<Room>());
					hashSet.AddRange((from x in (from x in connections2
					select x.Tag).OfType<Furniture>()
					where x.Type.Equals("Elevator")
					select x).SelectMany((Furniture x) => (from y in x.pathNode.GetConnections()
					select y.Tag).OfType<Room>()));
				}
			}
		}
		hashSet.Remove(room);
		return hashSet;
	}

	public Room GetRoomFromPoint(Vector3I p)
	{
		return this.GetRoomFromPoint(p.y, new Vector2((float)p.x, (float)p.z), false);
	}

	public bool ValidRoomPos(Rect r, int floor)
	{
		foreach (Room room in from x in this.Rooms
		where x.Floor == floor
		select x)
		{
			int num = 0;
			Rect roomBounds = room.RoomBounds;
			if (roomBounds.xMin == r.xMin)
			{
				num |= 1;
			}
			if (roomBounds.xMax == r.xMax)
			{
				num |= 2;
			}
			if (roomBounds.yMin == r.yMin)
			{
				num |= 4;
			}
			if (roomBounds.yMax == r.yMax)
			{
				num |= 8;
			}
			if (num == 15)
			{
				return false;
			}
			if (num == 14)
			{
				if (r.xMin - roomBounds.xMin <= 1f && roomBounds.xMin <= r.xMin)
				{
					return false;
				}
			}
			else if (num == 13)
			{
				if (roomBounds.xMax - r.xMax <= 1f && roomBounds.xMax >= r.xMax)
				{
					return false;
				}
			}
			else if (num == 11)
			{
				if (r.yMin - roomBounds.yMin <= 1f && roomBounds.yMin <= r.yMin)
				{
					return false;
				}
			}
			else if (num == 7)
			{
				if (roomBounds.yMax - r.yMax <= 1f && roomBounds.yMax >= r.yMax)
				{
					return false;
				}
			}
			else if (room.RoomBounds.Overlaps(r.Expand(2f, 2f)))
			{
				return false;
			}
		}
		return true;
	}

	public static Rect[] SplitRect(Rect oldRect, Rect newRect)
	{
		int num = 0;
		if (oldRect.xMin == newRect.xMin)
		{
			num |= 1;
		}
		if (oldRect.xMax == newRect.xMax)
		{
			num |= 2;
		}
		if (oldRect.yMin == newRect.yMin)
		{
			num |= 4;
		}
		if (oldRect.yMax == newRect.yMax)
		{
			num |= 8;
		}
		if (num == 14)
		{
			return new Rect[]
			{
				new Rect(oldRect.x, oldRect.y, oldRect.width - newRect.width - 1f, oldRect.height),
				new Rect(newRect.x, oldRect.y, newRect.width, oldRect.height)
			};
		}
		if (num == 13)
		{
			return new Rect[]
			{
				new Rect(oldRect.x, oldRect.y, newRect.width, oldRect.height),
				new Rect(newRect.xMax + 1f, oldRect.y, oldRect.width - newRect.width - 1f, oldRect.height)
			};
		}
		if (num == 11)
		{
			return new Rect[]
			{
				new Rect(oldRect.x, oldRect.y, oldRect.width, oldRect.height - newRect.height - 1f),
				new Rect(oldRect.x, newRect.y, oldRect.width, newRect.height)
			};
		}
		if (num == 7)
		{
			return new Rect[]
			{
				new Rect(oldRect.x, oldRect.y, oldRect.width, newRect.height),
				new Rect(oldRect.x, newRect.yMax + 1f, oldRect.width, oldRect.height - newRect.height - 1f)
			};
		}
		return null;
	}

	public Dictionary<Room, RoomManager.RoomBuildState> CheckRoomBounds(Rect r, int floor)
	{
		IEnumerable<Room> enumerable = from x in this.Rooms
		where x.Floor == floor
		select x;
		Dictionary<Room, RoomManager.RoomBuildState> dictionary = new Dictionary<Room, RoomManager.RoomBuildState>();
		foreach (Room room in enumerable)
		{
			int num = 0;
			Rect roomBounds = room.RoomBounds;
			if (roomBounds.xMin == r.xMin)
			{
				num |= 1;
			}
			if (roomBounds.xMax == r.xMax)
			{
				num |= 2;
			}
			if (roomBounds.yMin == r.yMin)
			{
				num |= 4;
			}
			if (roomBounds.yMax == r.yMax)
			{
				num |= 8;
			}
			if (num == 15)
			{
				dictionary.Add(room, RoomManager.RoomBuildState.Identical);
				break;
			}
			if (num == 14)
			{
				dictionary.Add(room, (roomBounds.xMin >= r.xMin) ? RoomManager.RoomBuildState.Expand : RoomManager.RoomBuildState.Split);
			}
			else if (num == 13)
			{
				dictionary.Add(room, (roomBounds.xMax <= r.xMax) ? RoomManager.RoomBuildState.Expand : RoomManager.RoomBuildState.Split);
			}
			else if (num == 11)
			{
				dictionary.Add(room, (roomBounds.yMin >= r.yMin) ? RoomManager.RoomBuildState.Expand : RoomManager.RoomBuildState.Split);
			}
			else if (num == 7)
			{
				dictionary.Add(room, (roomBounds.yMax <= r.yMax) ? RoomManager.RoomBuildState.Expand : RoomManager.RoomBuildState.Split);
			}
			else if (room.RoomBounds.Overlaps(r.Expand(2f, 2f)))
			{
				dictionary.Add(room, RoomManager.RoomBuildState.Invalid);
				break;
			}
		}
		return dictionary;
	}

	public Vector2 GetRoomChange(Rect r, int floor)
	{
		IEnumerable<Room> enumerable = from x in this.Rooms
		where x.Floor == floor
		select x;
		int num = 0;
		bool flag = false;
		Rect roomBounds = new Rect(0f, 0f, 0f, 0f);
		foreach (Room room in enumerable)
		{
			int num2 = 0;
			Rect roomBounds2 = room.RoomBounds;
			if (roomBounds2.xMin == r.xMin)
			{
				num2 |= 1;
			}
			if (roomBounds2.xMax == r.xMax)
			{
				num2 |= 2;
			}
			if (roomBounds2.yMin == r.yMin)
			{
				num2 |= 4;
			}
			if (roomBounds2.yMax == r.yMax)
			{
				num2 |= 8;
			}
			if (num2 == 15)
			{
				return r.size;
			}
			if (num2 == 14)
			{
				roomBounds = room.RoomBounds;
				flag = (roomBounds2.xMin < r.xMin);
				num++;
			}
			else if (num2 == 13)
			{
				roomBounds = room.RoomBounds;
				flag = (roomBounds2.xMax > r.xMax);
				num++;
			}
			else if (num2 == 11)
			{
				roomBounds = room.RoomBounds;
				flag = (roomBounds2.yMin < r.yMin);
				num++;
			}
			else if (num2 == 7)
			{
				roomBounds = room.RoomBounds;
				flag = (roomBounds2.yMax > r.yMax);
				num++;
			}
			else if (room.RoomBounds.Overlaps(r.Expand(2f, 2f)))
			{
				return r.size;
			}
		}
		if (num == 2)
		{
			return (r.width != roomBounds.width) ? new Vector2(1f, r.height) : new Vector2(r.width, 1f);
		}
		if (num != 1)
		{
			return r.size;
		}
		if (flag)
		{
			return (r.width != roomBounds.width) ? new Vector2(1f, r.height) : new Vector2(r.width, 1f);
		}
		return (r.width != roomBounds.width) ? new Vector2(r.width - roomBounds.width, r.height) : new Vector2(r.width, r.height - roomBounds.height);
	}

	public Room GetRoomFromPoint(int floor, Vector2 point, bool add = true)
	{
		for (int i = 0; i < this.Rooms.Count; i++)
		{
			if (this.Rooms[i].Floor == floor && this.Rooms[i].IsInside(point))
			{
				return this.Rooms[i];
			}
		}
		if (floor == 0)
		{
			return this.Outside;
		}
		return null;
	}

	public Room GetRoomFromPoint(Vector3 p)
	{
		return this.GetRoomFromPoint(Mathf.FloorToInt((p.y + 1f) / 2f), new Vector2(p.x, p.z), true);
	}

	public float Distance(Vector3 v1, Vector3 v2)
	{
		return (v1 - v2).sqrMagnitude;
	}

	public List<Room> GetRooms()
	{
		return this.Rooms;
	}

	public void RemoveRoom(Room room)
	{
		if (room != null)
		{
			this.Rooms.RemoveAll((Room x) => x == room);
		}
	}

	public void AddRoom(Room room)
	{
		if (room == null)
		{
			return;
		}
		if (!this.Rooms.Contains(room))
		{
			this.Rooms.Add(room);
			if (room.Floor == 0)
			{
				GameSettings.Instance.sRoomManager.Outside.DirtyNavMesh = true;
			}
		}
	}

	public bool FilterFunction(Team team, Employee.RoleBit role, object node)
	{
		Room room = node as Room;
		return room == null || room.AllowedInRoom(team, role);
	}

	private bool IsElevator(object s)
	{
		return s is Furniture && (s as Furniture).Type.Equals("Elevator");
	}

	public void FixPath(List<PathNode<Vector3>> path)
	{
		Dictionary<object, int> dictionary = new Dictionary<object, int>();
		for (int i = 0; i < path.Count; i++)
		{
			if (path[i].Tag2 != null)
			{
				int num = -1;
				if (dictionary.TryGetValue(path[i].Tag2, out num))
				{
					for (int j = i; j > num; j--)
					{
						dictionary.Remove(path[j].Tag2);
						path.RemoveAt(j);
					}
					i = num;
				}
				else
				{
					dictionary[path[i].Tag2] = i;
				}
			}
		}
	}

	public List<Vector3> FindPath(Vector3 startV, Vector3 endV, Team team, Employee.RoleBit role, bool anyRoom)
	{
		startV = new Vector3(startV.x, (float)Mathf.RoundToInt(startV.y), startV.z);
		endV = new Vector3(endV.x, (float)Mathf.RoundToInt(endV.y), endV.z);
		Room roomFromPoint = this.GetRoomFromPoint(startV);
		Room roomFromPoint2 = this.GetRoomFromPoint(endV);
		if (roomFromPoint == null || roomFromPoint2 == null)
		{
			return null;
		}
		if (roomFromPoint2.GetNodeAt(endV.FlattenVector3()) == null)
		{
			return null;
		}
		Func<object, bool> func;
		if (anyRoom)
		{
			func = ((object x) => true);
		}
		else
		{
			func = ((object x) => this.FilterFunction(team, role, x));
		}
		Func<object, bool> filter = func;
		PathNode<Vector3> availableNode = roomFromPoint.GetAvailableNode(startV);
		PathNode<Vector3> availableNode2 = roomFromPoint2.GetAvailableNode(endV);
		availableNode.Point = startV;
		availableNode2.Point = endV;
		List<PathNode<Vector3>> list = NodePathFinding<Vector3>.FindPath(availableNode, availableNode2, (Vector3 x, Vector3 y) => (x - y).magnitude, (Vector3 x, Vector3 y) => (x - y).magnitude, filter, (availableNode.Point + availableNode2.Point) * 0.5f);
		if (list == null)
		{
			return null;
		}
		this.FixPath(list);
		List<Vector3> list2 = new List<Vector3>();
		list2.Add(startV);
		IRoomConnector a = null;
		for (int i = 0; i < list.Count - 2; i++)
		{
			object tag = list[i].Tag;
			object tag2 = list[i + 1].Tag;
			Room room = tag as Room;
			if (room != null && tag2 is IRoomConnector)
			{
				IRoomConnector roomConnector = tag2 as IRoomConnector;
				Vector3 offsetPos = roomConnector.GetOffsetPos(room, false);
				Vector3 start = list2.Last<Vector3>();
				if (!room.FindPath(start, offsetPos, ref list2, a, roomConnector))
				{
					return null;
				}
				IRoomConnector roomConnector2 = roomConnector;
				Transform[] array = roomConnector2.IntermediatePoints(room);
				if (array != null)
				{
					for (int j = 0; j < array.Length; j++)
					{
						list2.Add(array[j].position);
					}
				}
				Vector3 offsetPos2 = roomConnector2.GetOffsetPos(room, true);
				list2.Add(offsetPos2);
				a = roomConnector;
			}
			else if (this.IsElevator(tag) && tag2 is Room)
			{
				IRoomConnector roomConnector3 = tag as IRoomConnector;
				list2.Add(roomConnector3.ObjectTransform.position);
				list2.Add(roomConnector3.GetOffsetPos(tag2 as Room, false));
				a = null;
			}
		}
		if (list.Count > 1)
		{
			object tag3 = list[list.Count - 2].Tag;
			object tag4 = list[list.Count - 1].Tag;
			if (this.IsElevator(tag3) && tag4 is Room)
			{
				IRoomConnector roomConnector4 = tag3 as IRoomConnector;
				list2.Add(roomConnector4.ObjectTransform.position);
				list2.Add(roomConnector4.GetOffsetPos(tag4 as Room, false));
			}
		}
		Vector3 start2 = list2.Last<Vector3>();
		if (!roomFromPoint2.FindPath(start2, endV, ref list2, null, null))
		{
			return null;
		}
		if (list2.Count > 2)
		{
			list2.RemoveAt(0);
		}
		list2[0] = startV;
		list2[list2.Count - 1] = endV;
		for (int k = 1; k < list2.Count; k++)
		{
			if (list2[k] == list2[k - 1])
			{
				list2.RemoveAt(k);
				k--;
			}
		}
		return list2;
	}

	public bool CanDestroy(Room room)
	{
		if (room.Floor == -1)
		{
			return true;
		}
		List<Room> connected = this.GetConnected(room, false, true);
		for (int i = 0; i < connected.Count; i++)
		{
			Room room2 = connected[i];
			if (room2.Floor == room.Floor + 1)
			{
				if (!this.IsSupported(from x in room2.Edges
				select x.Pos, room2.Floor, room, true, null))
				{
					return false;
				}
			}
		}
		return true;
	}

	public List<Room> GetConnected(Room room, bool oneFloor = false, bool outDoor = true)
	{
		if (room.Outdoors && !outDoor)
		{
			return new List<Room>();
		}
		HashSet<Room> untouched = (!oneFloor) ? (from x in this.Rooms
		where !x.Outdoors || outDoor
		select x).ToHashSet<Room>() : (from x in this.Rooms
		where x.Floor == room.Floor && (!x.Outdoors || outDoor)
		select x).ToHashSet<Room>();
		untouched.Remove(room);
		this.SubGetConnected(room, untouched, oneFloor);
		return (from x in this.Rooms
		where (!x.Outdoors || outDoor) && (!oneFloor || x.Floor == room.Floor) && !untouched.Contains(x)
		select x).ToList<Room>();
	}

	private void SubGetConnected(Room room, HashSet<Room> untouched, bool oneFloor)
	{
		if (room == null)
		{
			return;
		}
		HashSet<Room> hashSet = new HashSet<Room>();
		List<WallEdge> edges = room.Edges;
		Rect rect = room.RoomBounds.Expand(1f, 1f);
		foreach (Room room2 in untouched.ToList<Room>())
		{
			if (untouched.Contains(room2))
			{
				if (!(room2 == null))
				{
					int num = Mathf.Abs(room2.Floor - room.Floor);
					if (num <= 1)
					{
						if (rect.Overlaps(room2.RoomBounds))
						{
							if (num == 0)
							{
								for (int i = 0; i < room2.Edges.Count; i++)
								{
									if (room2.Edges[i].Links.ContainsKey(room))
									{
										untouched.Remove(room2);
										hashSet.Add(room2);
										break;
									}
								}
							}
							else if (!oneFloor)
							{
								bool flag = false;
								List<WallEdge> edges2 = room2.Edges;
								for (int j = 0; j < room.Edges.Count; j++)
								{
									if (Utilities.IsInside(room.Edges[j].Pos, edges2))
									{
										untouched.Remove(room2);
										hashSet.Add(room2);
										flag = true;
										break;
									}
								}
								if (!flag)
								{
									for (int k = 0; k < room2.Edges.Count; k++)
									{
										if (Utilities.IsInside(room2.Edges[k].Pos, edges))
										{
											untouched.Remove(room2);
											hashSet.Add(room2);
											flag = true;
											break;
										}
									}
									if (!flag)
									{
										for (int l = 0; l < room2.Edges.Count; l++)
										{
											for (int m = 0; m < room.Edges.Count; m++)
											{
												int index = (l + 1) % room2.Edges.Count;
												int index2 = (m + 1) % room.Edges.Count;
												if (Utilities.LinesIntersect(room2.Edges[l].Pos, room2.Edges[index].Pos, room.Edges[m].Pos, room.Edges[index2].Pos, false, true))
												{
													untouched.Remove(room2);
													hashSet.Add(room2);
													flag = true;
													break;
												}
											}
											if (flag)
											{
												break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		foreach (Room room3 in hashSet)
		{
			this.SubGetConnected(room3, untouched, oneFloor);
		}
	}

	public bool IsSupported(Vector2 p, int floor, Room without)
	{
		if (floor < 1)
		{
			return true;
		}
		Dictionary<Room, Vector2[]> dictionary = null;
		if (this.RoomSupport.TryGetValue(floor - 1, out dictionary))
		{
			foreach (KeyValuePair<Room, Vector2[]> keyValuePair in dictionary)
			{
				if (!(keyValuePair.Key == without))
				{
					if (Utilities.IsInside(p, keyValuePair.Value))
					{
						return true;
					}
				}
			}
			return false;
		}
		return false;
	}

	public bool IsSupported(IEnumerable<Vector2> points, int floor, Room without, bool checkFloor = true, HashSet<Room> with = null)
	{
		if (checkFloor && floor < 1)
		{
			return true;
		}
		List<Vector2> list = new List<Vector2>(points);
		for (int i = 0; i < list.Count; i += 2)
		{
			list.Insert(i + 1, (list[i] + list[(i + 1) % list.Count]) * 0.5f);
		}
		Dictionary<Room, Vector2[]> source = null;
		if (this.RoomSupport.TryGetValue(floor - 1, out source))
		{
			HashSet<Room> hashSet = new HashSet<Room>();
			for (int j = 0; j < list.Count; j++)
			{
				Vector2 p = list[j];
				bool flag = false;
				bool flag2 = false;
				foreach (KeyValuePair<Room, Vector2[]> keyValuePair in from x in source
				where x.Key != null
				select x)
				{
					if (with == null || with.Contains(keyValuePair.Key))
					{
						if (Utilities.IsInside(p, keyValuePair.Value))
						{
							hashSet.Add(keyValuePair.Key);
							if (keyValuePair.Key == without)
							{
								flag2 = true;
							}
							else
							{
								flag = true;
							}
						}
					}
				}
				if (!flag && (without == null || flag2))
				{
					return false;
				}
			}
			Vector2[] rpa = list.ToArray();
			bool flag3 = false;
			foreach (Room room in hashSet)
			{
				if (room.Edges.Any((WallEdge x) => Utilities.IsInside(x.Pos, rpa)))
				{
					if (!(room == without))
					{
						return true;
					}
					flag3 = true;
				}
				for (int k = 0; k < list.Count; k++)
				{
					if (room.IsInside(list[k]))
					{
						if (!(room == without))
						{
							return true;
						}
						flag3 = true;
					}
					for (int l = 0; l < room.Edges.Count; l++)
					{
						Vector2 pos = room.Edges[l].Pos;
						Vector2 pos2 = room.Edges[(l + 1) % room.Edges.Count].Pos;
						if (Utilities.LinesIntersect(list[k], list[(k + 1) % list.Count], pos, pos2, true, false))
						{
							if (!(room == without))
							{
								return true;
							}
							flag3 = true;
						}
					}
				}
			}
			return without != null && !flag3;
		}
		return false;
	}

	public void UpdateFloorMeshes(int floor, bool navMesh = false)
	{
		foreach (Room room in from x in this.Rooms
		where x.Floor == floor
		select x)
		{
			room.UpdateRoom(navMesh, true, true, false, true, false, false);
		}
	}

	public void UpdateTemperatureControllers()
	{
		this.TemperatureControlDirty = false;
		this.Rooms.ForEach(delegate(Room x)
		{
			if (x.TempControllers.Count > 0)
			{
				x.TempControllers.Clear();
				x.MakeTemperatureDirty(true);
			}
		});
		IEnumerable<IGrouping<Room, Furniture>> source = from x in this.AllFurniture
		where x != null && x.gameObject != null && x.Parent != null && x.TemperatureController
		group x by x.Parent;
		Func<IGrouping<Room, Furniture>, Room> keySelector = (IGrouping<Room, Furniture> x) => x.Key;
		if (RoomManager.<>f__mg$cache1 == null)
		{
			RoomManager.<>f__mg$cache1 = new Func<IGrouping<Room, Furniture>, HashSet<Furniture>>(Utilities.ToHashSet<Furniture>);
		}
		Dictionary<Room, HashSet<Furniture>> dictionary = source.ToDictionary(keySelector, RoomManager.<>f__mg$cache1);
		while (dictionary.Count > 0)
		{
			KeyValuePair<Room, HashSet<Furniture>> keyValuePair = dictionary.First<KeyValuePair<Room, HashSet<Furniture>>>();
			dictionary.Remove(keyValuePair.Key);
			List<Room> connected = this.GetConnected(keyValuePair.Key, false, false);
			for (int i = 0; i < connected.Count; i++)
			{
				HashSet<Furniture> range;
				if (dictionary.TryGetValue(connected[i], out range))
				{
					keyValuePair.Value.AddRange(range);
					dictionary.Remove(connected[i]);
				}
			}
			List<Furniture> list = (from x in keyValuePair.Value
			where x.HeatCoolPotential < 0f
			select x).ToList<Furniture>();
			List<Furniture> list2 = (from x in keyValuePair.Value
			where x.HeatCoolPotential > 0f
			select x).ToList<Furniture>();
			List<Room> list3 = new List<Room>(connected);
			List<Room> list4 = new List<Room>(connected);
			for (int j = connected.Count - 1; j >= 0; j--)
			{
				bool flag = false;
				bool flag2 = false;
				for (int k = 0; k < 2; k++)
				{
					HashList<Furniture> furniture = connected[j].GetFurniture((k != 0) ? "Radiator" : "Ventilation");
					for (int l = 0; l < furniture.Count; l++)
					{
						Furniture furniture2 = furniture[l];
						if (!furniture2.TemperatureController && furniture2.HeatCoolPotential != 0f)
						{
							if (furniture2.HeatCoolPotential > 0f)
							{
								flag = true;
							}
							else
							{
								flag2 = true;
							}
							if (!furniture2.EqualizeTemperature)
							{
								flag = true;
								flag2 = true;
							}
						}
						if (flag && flag2)
						{
							break;
						}
					}
				}
				if (flag)
				{
					list3.RemoveAt(j);
				}
				if (flag2)
				{
					list4.RemoveAt(j);
				}
			}
			for (int m = 0; m < list4.Count; m++)
			{
				list4[m].MakeTemperatureDirty(true);
			}
			for (int n = 0; n < list3.Count; n++)
			{
				list3[n].MakeTemperatureDirty(true);
			}
			float num = list4.SumSafe((Room x) => x.Area);
			float num2 = list3.SumSafe((Room x) => x.Area);
			num /= (float)list.Count;
			num2 /= (float)list2.Count;
			for (int num3 = 0; num3 < list4.Count; num3++)
			{
				Room room = list4[num3];
				room.TempControllers.AddRange(list);
			}
			for (int num4 = 0; num4 < list3.Count; num4++)
			{
				Room room2 = list3[num4];
				room2.TempControllers.AddRange(list2);
			}
			foreach (Furniture furniture3 in keyValuePair.Value)
			{
				float num5 = (furniture3.HeatCoolPotential <= 0f) ? (num / furniture3.HeatCoolArea) : (num2 / furniture3.HeatCoolArea);
				num5 = Mathf.Max(1f, num5);
				if (num5 > 1f && HUD.Instance != null)
				{
					HUD.Instance.AddPopupMessage("TemperatureOveruse".Loc(), "Exclamation", PopupManager.PopUpAction.GotoFurn, furniture3.DID, PopupManager.NotificationSound.Issue, 0.25f, PopupManager.PopupIDs.TemperatureOverburdenend, 1);
				}
				furniture3.Wattage = furniture3.TempWatt * num5;
				furniture3.Water = furniture3.TempWater * num5;
				furniture3.upg.TimeToAtrophy = furniture3.TempBreak / num5;
				furniture3.TempControllerArea = ((furniture3.HeatCoolPotential <= 0f) ? num : num2);
				furniture3.RoomTemperature.Clear();
				furniture3.RoomTemperature.AddRange((furniture3.HeatCoolPotential <= 0f) ? list4 : list3);
			}
		}
	}

	private void WindowConnection(Room room, Dictionary<Room, Dictionary<Room, float>> set)
	{
		if (room.DirtyStateVariables)
		{
			room.stateRefreshNeighbours = false;
			room.UpdateRoom(false, false, false, false, true, false, false);
		}
		Dictionary<Room, float> dictionary = new Dictionary<Room, float>();
		set.Add(room, dictionary);
		room.IndirectLighting = 0f;
		foreach (RoomSegment roomSegment in from x in room.GetSegments(null)
		where x.LightAddition > 0f
		select x)
		{
			Room[] parentRooms = roomSegment.ParentRooms;
			if (parentRooms[0] != null && parentRooms[1] != null)
			{
				Room room2 = (!(parentRooms[0] == room)) ? parentRooms[0] : parentRooms[1];
				if (dictionary.ContainsKey(room2))
				{
					Dictionary<Room, float> dictionary2;
					Room key;
					(dictionary2 = dictionary)[key = room2] = dictionary2[key] + roomSegment.LightAddition;
				}
				else
				{
					dictionary[room2] = roomSegment.LightAddition;
				}
				if (!set.ContainsKey(room2))
				{
					this.WindowConnection(room2, set);
				}
			}
		}
	}

	private void Propagate(Room r, float amount, HashSet<Room> visited, Dictionary<Room, Dictionary<Room, float>> map, bool diminish)
	{
		visited.Add(r);
		amount /= Mathf.Max(1f, r.Area / 4f);
		if (!diminish)
		{
			amount = Mathf.Min(1f, amount * 4f);
		}
		r.IndirectLighting += amount;
		if (amount.Appx(0f, 0.001f))
		{
			return;
		}
		foreach (KeyValuePair<Room, float> keyValuePair in map[r])
		{
			if (!visited.Contains(keyValuePair.Key))
			{
				this.Propagate(keyValuePair.Key, amount, visited, map, !r.Outdoors);
			}
		}
	}

	public void PropagateLighting(Room room)
	{
		Dictionary<Room, Dictionary<Room, float>> dictionary = new Dictionary<Room, Dictionary<Room, float>>();
		this.WindowConnection(room, dictionary);
		foreach (KeyValuePair<Room, Dictionary<Room, float>> keyValuePair in dictionary)
		{
			if (keyValuePair.Key.WindowDarkLevel != 0f)
			{
				foreach (KeyValuePair<Room, float> keyValuePair2 in keyValuePair.Value)
				{
					HashSet<Room> hashSet = new HashSet<Room>();
					hashSet.Add(keyValuePair.Key);
					this.Propagate(keyValuePair2.Key, keyValuePair2.Value * keyValuePair.Key.WindowDarkLevel, hashSet, dictionary, !keyValuePair.Key.Outdoors);
				}
			}
		}
	}

	private static RoomManager.ProtoEdge FindNextEdge(RoomManager.ProtoEdge s2, RoomManager.ProtoEdge s3)
	{
		Vector2 pos = s2.Pos;
		Vector2 pos2 = s3.Pos;
		float num = float.MinValue;
		RoomManager.ProtoEdge result = s3;
		foreach (RoomManager.ProtoEdge protoEdge in s3.Links)
		{
			float num2 = Room.LeftVal(pos, pos2, protoEdge.Pos);
			if (num2 > num)
			{
				result = protoEdge;
				num = num2;
			}
		}
		return result;
	}

	private static List<RoomManager.ProtoEdge[]> BuildRoomGraph(BuildingPrefab b, int floor)
	{
		List<RoomManager.ProtoEdge[]> list = new List<RoomManager.ProtoEdge[]>();
		RoomManager.ProtoEdge[] edges = b.Edges.SelectInPlace((SVector3 x) => new RoomManager.ProtoEdge(x));
		for (int i = 0; i < b.Rooms.Length; i++)
		{
			BuildingPrefab.RoomObject roomObject = b.Rooms[i];
			if (roomObject.Floor == floor)
			{
				RoomManager.ProtoEdge[] array = roomObject.Edges.SelectInPlace((int x) => edges[x]);
				for (int j = 0; j < array.Length; j++)
				{
					RoomManager.ProtoEdge protoEdge = array[j];
					RoomManager.ProtoEdge item = array[(j + 1) % array.Length];
					protoEdge.Links.Add(item);
				}
				list.Add(array);
			}
		}
		return list;
	}

	public static List<List<Vector2>[]> CombineRoomEdges(BuildingPrefab b, int floor)
	{
		List<RoomManager.ProtoEdge[]> list = RoomManager.BuildRoomGraph(b, floor);
		List<List<Vector2>[]> list2 = new List<List<Vector2>[]>();
		HashSet<RoomManager.ProtoEdge> hashSet = new HashSet<RoomManager.ProtoEdge>();
		for (int i = 0; i < list.Count; i++)
		{
			RoomManager.ProtoEdge[] array = list[i];
			for (int j = 0; j < array.Length; j++)
			{
				RoomManager.ProtoEdge protoEdge = array[j];
				if (!hashSet.Contains(protoEdge))
				{
					RoomManager.ProtoEdge protoEdge2 = array[(j + 1) % array.Length];
					if (!protoEdge2.Links.Contains(protoEdge))
					{
						hashSet.Add(protoEdge);
						hashSet.Add(protoEdge2);
						List<Vector2> list3 = new List<Vector2>();
						list3.Add(protoEdge2.Pos);
						RoomManager.ProtoEdge protoEdge3 = protoEdge;
						RoomManager.ProtoEdge s = protoEdge;
						RoomManager.ProtoEdge protoEdge5;
						for (RoomManager.ProtoEdge protoEdge4 = protoEdge2; protoEdge4 != protoEdge3; protoEdge4 = protoEdge5)
						{
							protoEdge5 = RoomManager.FindNextEdge(s, protoEdge4);
							hashSet.Add(protoEdge5);
							list3.Add(protoEdge5.Pos);
							s = protoEdge4;
						}
						if (Utilities.Clockwise(list3))
						{
							list3.Reverse();
						}
						List<Vector2> list4 = new List<Vector2>(list3.Count);
						List<Vector2> list5 = new List<Vector2>(list3.Count);
						for (int k = 0; k < list3.Count; k++)
						{
							int index = (k + 1) % list3.Count;
							int index2 = (k + 2) % list3.Count;
							list4.Add(Room.GetOffset(list3[k], list3[index], list3[index2], -Room.WallOffset / 2f + 0.01f, true));
							list5.Add(Room.GetOffset(list3[k], list3[index], list3[index2], 0.01f, true));
						}
						list2.Add(new List<Vector2>[]
						{
							list5,
							list4
						});
					}
				}
			}
		}
		List<List<Vector2>[]> list6 = new List<List<Vector2>[]>();
		for (int l = 0; l < list2.Count; l++)
		{
			bool flag = true;
			for (int m = 0; m < list2[l][0].Count; m++)
			{
				for (int n = 0; n < list2.Count; n++)
				{
					if (n != l)
					{
						if (Utilities.IsInside(list2[l][0][m], list2[n][0]))
						{
							flag = false;
							break;
						}
					}
				}
				if (!flag)
				{
					break;
				}
			}
			if (flag)
			{
				list6.Add(list2[l]);
			}
		}
		return list6;
	}

	public static int MaxRoomsPerUpdate = 10;

	public Room[] UpdateRooms = new Room[RoomManager.MaxRoomsPerUpdate];

	public List<Room> Rooms = new List<Room>();

	public HashSet<WallEdge> AllSegments = new HashSet<WallEdge>();

	public HashSet<RoomSegment> RoomSegments = new HashSet<RoomSegment>();

	public HashSet<Furniture> AllFurniture = new HashSet<Furniture>();

	public Dictionary<int, FloorFurnitureMesh> CombinedFurnitureMeshes = new Dictionary<int, FloorFurnitureMesh>();

	public HashSet<int> DirtyCombineFloors = new HashSet<int>();

	private Dictionary<Room, List<KeyValuePair<Room, int>>> ConnectedRooms = new Dictionary<Room, List<KeyValuePair<Room, int>>>();

	private Dictionary<int, Dictionary<Room, Vector2[]>> RoomSupport = new Dictionary<int, Dictionary<Room, Vector2[]>>();

	public Room Outside;

	public Room CameraRoom;

	public bool DisableMeshRebuild;

	public bool RoomNearnessDirty;

	public bool TemperatureControlDirty;

	private bool _teamText;

	private List<KeyValuePair<Room, int>> EmptyConnection = new List<KeyValuePair<Room, int>>();

	private bool BFSStarted;

	private object BFSLock = new object();

	[CompilerGenerated]
	private static Func<Room, HashSet<Room>> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<IGrouping<Room, Furniture>, HashSet<Furniture>> <>f__mg$cache1;

	public enum RoomBuildState
	{
		Invalid,
		Identical,
		Expand,
		Split
	}

	private class BFSNode<T>
	{
		public BFSNode(T val)
		{
			this.Value = val;
		}

		public RoomManager.BFSNode<T> Parent;

		public T Value;

		public int Distance = int.MaxValue;
	}

	private class ProtoEdge
	{
		public ProtoEdge(Vector2 pos)
		{
			this.Pos = pos;
			this.Links = new HashSet<RoomManager.ProtoEdge>();
		}

		public Vector2 Pos;

		public HashSet<RoomManager.ProtoEdge> Links;
	}
}
