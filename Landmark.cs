﻿using System;
using UnityEngine;

public abstract class Landmark : Writeable
{
	private void Awake()
	{
		base.InitWritable();
	}

	private void OnDestroy()
	{
		if (RoadManager.Instance != null)
		{
			RoadManager.Instance.Landmarks.Remove(this);
		}
	}

	public abstract Vector2[] GetNavMesh();

	public abstract Vector2 Center();
}
