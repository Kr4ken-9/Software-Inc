﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EventElement : MonoBehaviour
{
	public string Icon
	{
		get
		{
			return this.icon;
		}
		set
		{
			this.icon = value;
			this.iconImage.sprite = IconManager.GetIcon(this.icon);
		}
	}

	private string icon;

	public Image iconImage;

	public Text Description;
}
