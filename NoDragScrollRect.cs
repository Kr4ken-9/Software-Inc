﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NoDragScrollRect : ScrollRect
{
	public override void OnBeginDrag(PointerEventData eventData)
	{
	}

	public override void OnDrag(PointerEventData eventData)
	{
	}

	public override void OnEndDrag(PointerEventData eventData)
	{
	}

	public void OnChange(Vector2 pos)
	{
		float num = Mathf.Max(0f, base.content.rect.height - base.viewRect.rect.height);
		float num2 = (1f - pos.y) * num;
		float d = num2 + base.viewRect.rect.height;
		float num3 = 0f;
		for (int i = 0; i < base.content.childCount; i++)
		{
			RectTransform component = base.content.GetChild(i).GetComponent<RectTransform>();
			if (component.gameObject.activeSelf)
			{
				GUIWorkItem component2 = component.GetComponent<GUIWorkItem>();
				float a = num3;
				num3 += ((!(component2 != null) || component2.work == null) ? component.rect.height : ((float)((!component2.work.Collapsed) ? 128 : 43))) + 4f;
				if (component2 != null)
				{
					component2.enabled = Utilities.RelaxedOverlap(a, num3, num2, d);
				}
			}
		}
	}
}
