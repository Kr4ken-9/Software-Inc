﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class SoftwareProduct : IServerItem, IStockable
{
	public SoftwareProduct()
	{
	}

	public SoftwareProduct(string name, string type, string category, Dictionary<string, uint> needs, uint[] os, float codeProgress, float artProgress, float codeQuality, float artQuality, float price, SDateTime start, SDateTime release, int bugs, bool inHouse, Company company, uint? sequelto, uint id, float loss, string[] features, string server, uint followers, SoftwareAlpha mock = null)
	{
		this.MockWork = mock;
		this.Name = name;
		this._type = type;
		this._category = category;
		this.Needs = (needs ?? new Dictionary<string, uint>());
		this.OSs = ((os != null) ? os.ToList<uint>() : null);
		if (features == null)
		{
			this.Features = (from x in this.Type.Features.Values
			where x.Forced
			select x.Name).ToArray<string>();
		}
		else
		{
			this.Features = features;
		}
		this.FeatureScore = this.Type.FeatureScore(this.Features);
		this.CodeProgress = Mathf.Clamp01(codeProgress);
		this.ArtProgress = Mathf.Clamp01(artProgress);
		this.CodeQuality = Mathf.Clamp01(codeQuality);
		this.ArtQuality = Mathf.Clamp01(artQuality);
		this._quality = Mathf.Clamp01(this.Type.FinalQualityCalc(this.CodeProgress, this.ArtProgress, this.CodeQuality, this.ArtQuality, features));
		this.DevStart = start;
		this.Release = release;
		this.Bugs = bugs;
		this.StartBugs = bugs;
		this.DevTime = this.Type.DevTime(this.Features, this._category, null);
		this.Price = price;
		this.OriginalPrice = price;
		this.LowestPrice = price;
		this.InHouse = (this.Type.InHouse && inHouse);
		float[] balance = this.Type.GetBalance(this.Features);
		float num = balance.Sum();
		this.Innovation = balance[0] / num;
		this.Stability = balance[1] / num;
		this.Usability = balance[2] / num;
		this.DevCompany = company;
		this.EnableServerHosting = (this.DevCompany == GameSettings.Instance.MyCompany);
		this.RandomFactor = Utilities.RandomGaussClamped(1f, 0.2f);
		this.sequelTo = sequelto;
		if (sequelto != null && (!this.SequelTo._type.Equals(this._type) || this.SequelTo.DevCompany == null || this.SequelTo.DevCompany != this.DevCompany))
		{
			sequelto = null;
		}
		if (sequelto != null && !this.IsMock)
		{
			this.SequelTo.sequel = new uint?(id);
			HUD.Instance.dealWindow.CancelBids(this.SequelTo);
		}
		this.SequelBonus = SoftwareProduct.CalculateSequelBonus(this.SequelTo, this.DevTime, this.Quality);
		this.Loss = loss;
		float num2 = Mathf.Max(1f, GameSettings.Instance.simulation.GetFollowerReach(this._type, this._category, features, this.OSs));
		this._awareness = Mathf.Clamp01(followers / num2) * this.SequelBonus * GameSettings.Instance.simulation.GetMaxAwareness(this);
		this.Followers = followers / 2u;
		this.ID = id;
		GameSettings.Instance.simulation.ClearStockable(this.ID);
		this.ServerReq = this.Type.GetServerReq(this.Features);
		this.Server = server;
		if (!this.IsMock)
		{
			this.RegisterServer();
		}
		this.PatentableFeatures = (from x in this.Features
		select this.Type.Features[x] into x
		where x.Research != null
		select x.Name).ToArray<string>();
		if (!this.InHouse && !this.IsMock && this.DevCompany.Player)
		{
			Newspaper.GenerateProductReview(this);
		}
		if (!this.IsMock)
		{
			this.NewFeatures = GameSettings.Instance.NewFeatures(this, true);
		}
	}

	public bool IsMock
	{
		get
		{
			return this.MockWork != null;
		}
	}

	public float Quality
	{
		get
		{
			return this._quality;
		}
	}

	public float RealQuality
	{
		get
		{
			return this._quality;
		}
	}

	public SoftwareProduct SequelTo
	{
		get
		{
			return (this.sequelTo == null) ? null : GameSettings.Instance.simulation.GetProduct(this.sequelTo.Value, true);
		}
	}

	public SoftwareProduct Sequel
	{
		get
		{
			if (this._sequelCached != null)
			{
				return this._sequelCached;
			}
			if (this.sequel != null)
			{
				SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(this.sequel.Value, true);
				this._sequelCached = product;
				return product;
			}
			return null;
		}
	}

	public bool HasSequel
	{
		get
		{
			return this.sequel != null;
		}
	}

	public uint SoftwareID
	{
		get
		{
			return this.ID;
		}
	}

	public uint PhysicalCopies { get; set; }

	public bool OpenSource
	{
		get
		{
			return this.Price == 0f;
		}
	}

	public bool Traded
	{
		get
		{
			return this.inventor != null;
		}
	}

	public string Inventor
	{
		get
		{
			return this.inventor ?? this.DevCompany.Name;
		}
	}

	public SoftwareType Type
	{
		get
		{
			return GameSettings.Instance.SoftwareTypes[this._type];
		}
	}

	public SoftwareCategory Category
	{
		get
		{
			return this.Type.Categories[this._category];
		}
	}

	public List<float> GetCashflow()
	{
		if (this._cashflow == null)
		{
			return SoftwareProduct.EmptyFlow;
		}
		return this._cashflow;
	}

	public List<int> GetUnitSales(bool online)
	{
		List<int> list = (!online) ? this._unitOfflineSales : this._unitOnlineSales;
		if (list == null)
		{
			return SoftwareProduct.EmptyUnit;
		}
		return list;
	}

	public List<int> GetRefunds()
	{
		if (this._refunds == null)
		{
			return SoftwareProduct.EmptyUnit;
		}
		return this._refunds;
	}

	public void AddToCashflow(int onlineUnits, int offlineUnits, int refunds, float amount, SDateTime now)
	{
		bool flag = true;
		bool flag2 = true;
		if (this._cashflow != null && this._cashflow[this._cashflow.Count - 1] < 0.01f)
		{
			flag = (amount >= 0.01f);
		}
		if (this._unitOfflineSales != null && this._unitOfflineSales[this._unitOfflineSales.Count - 1] < 1 && this._unitOnlineSales[this._unitOnlineSales.Count - 1] < 1)
		{
			flag2 = (onlineUnits != 0 || offlineUnits != 0 || refunds != 0);
		}
		if (!flag && !flag2)
		{
			return;
		}
		if (this._unitOfflineSales == null)
		{
			this.LastAddedToUnits = now;
			this._unitOfflineSales = new List<int>
			{
				0
			};
			this._unitOnlineSales = new List<int>
			{
				0
			};
			this._refunds = new List<int>
			{
				0
			};
		}
		if (this._cashflow == null)
		{
			this.LastAddedToCashflow = now;
			this._cashflow = new List<float>
			{
				0f
			};
		}
		if (flag && (this.LastAddedToCashflow.Month != now.Month || this.LastAddedToCashflow.Year != now.Year))
		{
			int monthsFlat = Utilities.GetMonthsFlat(this.LastAddedToCashflow, now);
			for (int i = 0; i < monthsFlat; i++)
			{
				this._cashflow.Add(0f);
			}
		}
		if (flag2 && (this.LastAddedToUnits.Month != now.Month || this.LastAddedToUnits.Year != now.Year))
		{
			int monthsFlat2 = Utilities.GetMonthsFlat(this.LastAddedToUnits, now);
			for (int j = 0; j < monthsFlat2; j++)
			{
				this._unitOfflineSales.Add(0);
				this._unitOnlineSales.Add(0);
				this._refunds.Add(0);
			}
		}
		if (flag)
		{
			List<float> cashflow;
			int index;
			(cashflow = this._cashflow)[index = this._cashflow.Count - 1] = cashflow[index] + amount;
		}
		if (flag2)
		{
			List<int> list;
			int index2;
			(list = this._unitOfflineSales)[index2 = this._unitOfflineSales.Count - 1] = list[index2] + offlineUnits;
			int index3;
			(list = this._unitOnlineSales)[index3 = this._unitOnlineSales.Count - 1] = list[index3] + onlineUnits;
			int index4;
			(list = this._refunds)[index4 = this._refunds.Count - 1] = list[index4] + refunds;
		}
		this.Sum += amount;
		this.UnitSum += (uint)(offlineUnits + onlineUnits);
		this.RefundSum += (uint)refunds;
		if (flag)
		{
			this.LastAddedToCashflow = now;
		}
		if (flag2)
		{
			this.LastAddedToUnits = now;
		}
	}

	public static float CalculateSequelBonus(SoftwareProduct sequelTo, float devTime, float quality)
	{
		float num = 1f;
		SoftwareProduct softwareProduct = sequelTo;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		int num5 = 0;
		while (softwareProduct != null && num5 < 3)
		{
			if (!softwareProduct.InHouse)
			{
				float num6 = softwareProduct.Quality;
				if (Mathf.Abs(num6 - quality) < 0.05f)
				{
					num6 = quality;
				}
				float t = Mathf.Min(1f, softwareProduct.UnitSum / 50000f);
				num6 = Mathf.Lerp(quality, num6, t);
				num3 += num6 * (float)(3 - num5);
				num4 += softwareProduct.DevTime * (float)(3 - num5);
				num2 += (float)(3 - num5);
			}
			softwareProduct = softwareProduct.SequelTo;
			num5++;
		}
		if (num2 > 0f)
		{
			float num7 = num3 / num2;
			num = Mathf.Clamp((quality / num7 - 1f) * num7 + 1f, 0f, 2f);
			float num8 = num4 / num2;
			if (num8 > devTime)
			{
				num *= devTime / num8;
			}
			if (num < 1f)
			{
				num = Mathf.Lerp(num, 1f, quality);
			}
		}
		return num;
	}

	public List<KeyValuePair<uint, Feature>> GetRoyalties()
	{
		if (this.DevCompany == null || this.DevCompany.OwnedStock == null || this.PatentableFeatures == null || this.PatentableFeatures.Length == 0)
		{
			return null;
		}
		List<KeyValuePair<uint, Feature>> list = new List<KeyValuePair<uint, Feature>>();
		SoftwareType type = this.Type;
		Company ownerCompany = this.DevCompany.OwnerCompany;
		foreach (string text in this.PatentableFeatures)
		{
			if (text != null)
			{
				Feature feature = type.Features[text];
				uint patentOwner = feature.PatentOwner;
				if (patentOwner != 0u && patentOwner != this.DevCompany.ID && (ownerCompany == null || patentOwner != ownerCompany.ID))
				{
					bool flag = true;
					for (int j = 0; j < this.DevCompany.OwnedStock.Count; j++)
					{
						if (this.DevCompany.OwnedStock[j].Target == patentOwner)
						{
							flag = false;
							break;
						}
					}
					if (flag)
					{
						list.Add(new KeyValuePair<uint, Feature>(patentOwner, feature));
					}
				}
			}
		}
		return (list.Count <= 0) ? null : list;
	}

	public void RegisterServer()
	{
		if (!this.ExternalHostingActive && this.EnableServerHosting && this.ServerReq > 0f && this.DevCompany.Player && !this.DropHosting())
		{
			GameSettings.Instance.RegisterWithServer(this.Server, this, true);
		}
	}

	public void CancelHostingServices()
	{
		if (this.ExternalHosting > 0u && HUD.Instance.dealWindow.AllDeals.ContainsKey(this.ExternalHosting))
		{
			HUD.Instance.dealWindow.CancelDeal(HUD.Instance.dealWindow.AllDeals[this.ExternalHosting], true);
		}
	}

	public void Trade(Company company)
	{
		this.CancelHostingServices();
		HUD.Instance.dealWindow.CancelBids(this);
		if (this.inventor == null)
		{
			this.inventor = this.DevCompany.Name;
		}
		GameSettings.Instance.CancelPrintOrder(this.ID, false);
		GameSettings.Instance.MyCompany.CancelAllWorkFor(this);
		this.DevCompany.Products.Remove(this);
		this.DevCompany = company;
		this.DevCompany.Products.Add(this);
		GameSettings.Instance.DeregisterServerItem(this);
		this.RegisterServer();
	}

	public float RelativeFeatureScore(MarketSimulation sim, SDateTime time)
	{
		float featureScore = sim.GetFeatureScore(this, time);
		return (featureScore != 0f) ? (this.FeatureScore / featureScore) : 1f;
	}

	public void SimulateAwareness()
	{
		this._awareness = Mathf.Max(0f, this._awareness * (1f - 0.15f / (float)GameSettings.DaysPerMonth) + this.Marketing);
		this.Marketing = 0f;
	}

	public void KillAwareness()
	{
		this._awareness = 0f;
		this.Marketing = 0f;
	}

	public float DevTimePoint()
	{
		return Mathf.Max(1f, this.DevTime / 90f).WeightOne(0.1f);
	}

	public float InnovationPoint()
	{
		return this.Innovation.WeightOne(0.2f);
	}

	public float GetPriceAdjustedQuality()
	{
		return ((this.Price >= 1f) ? (this.Quality / this.Price) : this.Quality) * this.DevTime;
	}

	private float GetPFactor(float[] factors, float def)
	{
		return (!this.DevCompany.Player) ? def : factors[GameSettings.Instance.Difficulty];
	}

	public float GetWeightedQualityAddition(SDateTime time, bool market)
	{
		if (market && time.Equals(this._cachedWeightedMarketQualityDate, true) && this._cachedWeightedMarketQuality > 0f)
		{
			return this._cachedWeightedMarketQuality;
		}
		float time2 = this.GetTime(time, 0.15f);
		float num = (!market) ? time2 : (this.RandomPoint() * ((this.NewFeatures != 0f) ? (1f + 0.1f * this.GetDevFalloff(time, this.NewFeatures, 0.15f)) : 1f) * (Mathf.Pow(this.DevCompany.GetReputation(this._type, this._category), 2f) + this.GetPFactor(SoftwareProduct.DifficultyRepFactor, 0.02f)) * Mathf.Pow(this.RelativeFeatureScore(GameSettings.Instance.simulation, time), 2f) * Mathf.Lerp(this.GetAwareness(), Mathf.Max(0f, (float)((long)this.Userbase - (long)((ulong)this.RefundSum))) * this.GetPFactor(SoftwareProduct.DifficultyUserFactor, 0.1f) / MarketSimulation.Population, 1f - time2) * time2);
		float num2 = num * this.GetPriceAdjustedQuality() * this.SequelBonus;
		if (market)
		{
			this._cachedWeightedMarketQuality = num2;
			this._cachedWeightedMarketQualityDate = time;
		}
		return num2;
	}

	public float GetQuality(float max, SDateTime time, bool market)
	{
		if (max == 0f)
		{
			return 1f;
		}
		return Mathf.Min(1f, this.GetWeightedQualityAddition(time, market) / max);
	}

	public float GetQuality(SDateTime time, bool market)
	{
		float quality = GameSettings.Instance.simulation.GetQuality(this, time, market);
		return this.GetQuality(quality, time, market);
	}

	public float GetLicenseCost()
	{
		if (this.OpenSource || this.DevCompany.Bankrupt)
		{
			return 0f;
		}
		float devTime = this.DevTime;
		return this.Category.TimeScale * this.Quality * devTime * 200f;
	}

	public float GetLicenseCost(Company target)
	{
		if (target.OwnsLicense(this))
		{
			return 0f;
		}
		return this.GetLicenseCost();
	}

	public void TransferLicense(Company target, SDateTime time)
	{
		if (!target.OwnsLicense(this))
		{
			List<KeyValuePair<uint, Feature>> royalties = this.GetRoyalties();
			float licenseCost = this.GetLicenseCost();
			float num = 0f;
			this.AddToCashflow(0, 0, 0, licenseCost, time);
			if (royalties != null)
			{
				foreach (KeyValuePair<uint, Feature> keyValuePair in royalties)
				{
					Company company = GameSettings.Instance.simulation.GetCompany(keyValuePair.Key);
					if (company != null)
					{
						float num2 = licenseCost * keyValuePair.Value.Royalty;
						if (company.Player)
						{
							keyValuePair.Value.Income += num2;
						}
						company.MakeTransaction(num2, Company.TransactionCategory.Royalties, this.Name);
						num += num2;
					}
				}
			}
			target.MakeTransaction(-licenseCost, Company.TransactionCategory.Licenses, null);
			this.DevCompany.MakeTransaction(licenseCost, Company.TransactionCategory.Licenses, null);
			this.DevCompany.MakeTransaction(-num, Company.TransactionCategory.Royalties, this.Name);
			if (this.DevCompany.Player)
			{
				this.DevCompany.LicenseIncome += licenseCost;
			}
			target.AddLicense(this);
		}
	}

	public float GetTime(SDateTime time, float dieValue = 0.15f)
	{
		float p = Mathf.Max(0f, Utilities.GetMonths(this.Release, time));
		float num = this.DevTime * Mathf.Pow(0.99f, this.DevTime);
		float f = Mathf.Pow(10f, Mathf.Log10(dieValue * (1f + this.Innovation / 2f)) / num);
		return Mathf.Pow(f, p);
	}

	public float GetDevFalloff(SDateTime time, float devTime, float dieValue = 0.15f)
	{
		float p = Mathf.Max(0f, Utilities.GetMonths(this.Release, time));
		float num = devTime * Mathf.Pow(0.99f, devTime);
		float f = Mathf.Pow(10f, Mathf.Log10(dieValue) / num);
		return Mathf.Pow(f, p);
	}

	public float RandomPoint()
	{
		return this.RandomFactor.WeightOne(this.Type.RandomFactor).WeightOne(0.25f);
	}

	public float UsabilityPoint()
	{
		return this.Usability.WeightOne(0.3f);
	}

	public void AddToMarketing(float amount)
	{
		this.Marketing += amount;
	}

	public override string ToString()
	{
		return this.Name;
	}

	public float GetLoadRequirement()
	{
		return this.ServerReq * (float)this.Userbase;
	}

	private bool DropHosting()
	{
		return Utilities.GetMonths(this.Release, SDateTime.Now()) > 6f && this.GetLoadRequirement().BandwidthFactor(SDateTime.Now()) < 1f;
	}

	public void HandleLoad(float load)
	{
		if (this.DropHosting())
		{
			if (this.ExternalHostingActive)
			{
				this.CancelHostingServices();
			}
			else
			{
				GameSettings.Instance.DeregisterServerItem(this);
			}
			return;
		}
		if (this.GetLoadRequirement() < 0.001f)
		{
			return;
		}
		if (load < 0.75f)
		{
			load /= 0.75f;
			float num = 1f - (1f - load) * 0.01f;
			GameSettings.Instance.MyCompany.AddFans(-Mathf.RoundToInt(num * (float)this.Userbase), this._type, this._category);
			this.Marketing *= num;
			HUD.Instance.AddPopupMessage("ServerItemLoadWarning".Loc(new object[]
			{
				this.Name
			}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.ServerProblems, 1);
		}
	}

	public string GetDescription()
	{
		return this.Name;
	}

	public void SerializeServer(string name)
	{
		this.Server = name;
	}

	public bool CancelOnUnload()
	{
		return false;
	}

	public float GetAwareness()
	{
		return Mathf.Clamp01(this._awareness / GameSettings.Instance.simulation.GetMaxAwareness(this));
	}

	public float GetRealAwareness()
	{
		return this._awareness;
	}

	public SoftwareProduct GetLatestSuccessor()
	{
		SoftwareProduct softwareProduct = this;
		while (softwareProduct.sequel != null)
		{
			softwareProduct = softwareProduct.Sequel;
		}
		return softwareProduct;
	}

	public string GetName()
	{
		return this.Name;
	}

	public void AddLoss(float cost)
	{
		this._activeLoss += cost;
	}

	public void TurnLoss()
	{
		this.Loss += this._activeLoss;
		this._activeLoss = 0f;
	}

	public Company GetCompany()
	{
		return this.DevCompany;
	}

	public bool OSOverlap(SoftwareProduct other)
	{
		if (this.OSs != null && this.OSs.Count > 0 && other.OSs != null && other.OSs.Count > 0)
		{
			for (int i = 0; i < this.OSs.Count; i++)
			{
				uint num = this.OSs[i];
				for (int j = 0; j < other.OSs.Count; j++)
				{
					if (num == other.OSs[j])
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public static readonly float DieValue = 0.25f;

	private readonly float _quality;

	public uint ExternalHosting;

	public bool ExternalHostingActive;

	public bool EnableServerHosting;

	public uint Followers;

	public SoftwareAlpha MockWork;

	public bool MockSucceeded;

	[NonSerialized]
	private SoftwareProduct _sequelCached;

	public readonly string Name;

	public readonly string _type;

	public readonly string _category;

	public readonly Dictionary<string, uint> Needs;

	public List<uint> OSs;

	public readonly uint ID;

	public int MissedPhysicalSales;

	private uint? sequelTo;

	private uint? sequel;

	public readonly SDateTime DevStart;

	public readonly SDateTime Release;

	public readonly float DevTime;

	public readonly string[] Features;

	public readonly float CodeQuality;

	public readonly float ArtQuality;

	public readonly float CodeProgress;

	public readonly float ArtProgress;

	public readonly float FeatureScore;

	public Company DevCompany;

	public readonly float Innovation;

	public readonly float Stability;

	public readonly float Usability;

	public readonly float RandomFactor;

	public readonly float SequelBonus;

	public bool InHouse;

	public int Bugs;

	public int StartBugs = 1;

	public float Marketing;

	private float _awareness;

	public float ServerReq;

	public float Price;

	public float OriginalPrice;

	public float LowestPrice = -1f;

	public float Retention;

	public string Server;

	private List<float> _cashflow;

	private List<int> _unitOnlineSales;

	private List<int> _unitOfflineSales;

	private List<int> _refunds;

	public List<float> Rep = new List<float>();

	private SDateTime LastAddedToCashflow;

	private SDateTime LastAddedToUnits;

	public float Sum;

	public uint UnitSum;

	public uint RefundSum;

	public int Userbase;

	public float Loss;

	private float _activeLoss;

	private string inventor;

	public float NewFeatures;

	public bool HadOSWarning;

	public string[] PatentableFeatures;

	public static List<float> EmptyFlow = new List<float>();

	public static List<int> EmptyUnit = new List<int>();

	public static float[] DifficultyRepFactor = new float[]
	{
		0.02f,
		0.005f,
		0.001f
	};

	public static float[] DifficultyUserFactor = new float[]
	{
		0.5f,
		0.1f,
		0.05f
	};

	[NonSerialized]
	private float _cachedWeightedMarketQuality = -1f;

	[NonSerialized]
	private SDateTime _cachedWeightedMarketQualityDate;
}
