﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using AltSerialize;
using UnityEngine;

public class GameReader : MonoBehaviour
{
	public static void DebugBuildLoadGame(string filename)
	{
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		GameSettings.Instance.sRoomManager.DisableMeshRebuild = true;
		TimeProbe.BeginTime("Rebuild room time:");
		using (MemoryStream memoryStream = new MemoryStream(Utilities.ReadData(filename, "Rooms")))
		{
			RoomDescriptor roomDescriptor = (RoomDescriptor)binaryFormatter.Deserialize(memoryStream);
			roomDescriptor.BuildRooms(() => UnityEngine.Object.Instantiate<GameObject>(BuildController.Instance.RoomPrefab));
		}
		TimeProbe.FinalizeTime("Rebuild room time:");
		GameSettings.Instance.sRoomManager.DisableMeshRebuild = false;
		TimeProbe.BeginTime("Room mesh rebuild time:");
		for (int i = -1; i <= GameSettings.MaxFloor; i++)
		{
			GameSettings.Instance.sRoomManager.UpdateFloorMeshes(i, false);
		}
		TimeProbe.FinalizeTime("Room mesh rebuild time:");
		GameSettings.Instance.sRoomManager.RoomNearnessDirty = true;
		HUD.Instance.companyWindow.Bind();
		HUD.Instance.TeamWindow.TeamList.Items = GameSettings.Instance.sActorManager.Teams.Values.Cast<object>().ToList<object>();
		HUD.Instance.loanWindow.UpdateLoans();
	}

	public static void LoadGame(string filename, SaveGame game, GameReader.LoadMode mode)
	{
		byte[] array = Utilities.ReadData(filename, "Data");
		TimeProbe.BeginTime("Decompress time:");
		using (MemoryStream memoryStream = new MemoryStream(array))
		{
			using (GZipStream gzipStream = new GZipStream(memoryStream, CompressionMode.Decompress, false))
			{
				using (MemoryStream memoryStream2 = new MemoryStream())
				{
					gzipStream.CopyTo(memoryStream2, 16384);
					array = memoryStream2.ToArray();
				}
			}
		}
		TimeProbe.FinalizeTime("Decompress time:");
		TimeProbe.BeginTime("Deserialization time:");
		WriteDictionary[] data = GameReader.DeserializeDictionaries(array);
		TimeProbe.FinalizeTime("Deserialization time:");
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		RoomDescriptor roomData = null;
		using (MemoryStream memoryStream3 = new MemoryStream(Utilities.ReadData(filename, "Rooms")))
		{
			roomData = (RoomDescriptor)binaryFormatter.Deserialize(memoryStream3);
		}
		GameReader.LoadGame(data, roomData, mode);
	}

	public static WriteDictionary[] DeserializeDictionaries(byte[] dat)
	{
		return (WriteDictionary[])Serializer.Deserialize(dat);
	}

	public static void LoadGame(WriteDictionary[] data, RoomDescriptor roomData, GameReader.LoadMode mode)
	{
		Writeable.DeserializedObjects.Clear();
		if (mode == GameReader.LoadMode.Full || mode == GameReader.LoadMode.Company)
		{
			GameSettings.Instance.sActorManager.Teams.Clear();
			foreach (Actor actor in UnityEngine.Object.FindObjectsOfType<Actor>())
			{
				UnityEngine.Object.Destroy(actor.gameObject);
			}
		}
		GameSettings.Instance.sRoomManager.DisableMeshRebuild = true;
		GameSettings.Instance.FurnitureErrorOccured = false;
		if (roomData != null)
		{
			TimeProbe.BeginTime("Rebuild room time:");
			roomData.BuildRooms(() => UnityEngine.Object.Instantiate<GameObject>(BuildController.Instance.RoomPrefab));
			TimeProbe.FinalizeTime("Rebuild room time:");
		}
		TimeProbe.BeginTime("WriteDictionary element load time:");
		List<Writeable> list = new List<Writeable>(data.Length);
		for (int j = 0; j < data.Length; j++)
		{
			Writeable writeable = GameReader.LoadElement(data[j]);
			if (writeable != null)
			{
				list.Add(writeable);
			}
		}
		for (int k = 0; k < list.Count; k++)
		{
			list[k].PostDeserialize();
		}
		if (roomData != null)
		{
			for (int l = 0; l < GameSettings.Instance.sRoomManager.Rooms.Count; l++)
			{
				Room room = GameSettings.Instance.sRoomManager.Rooms[l];
				room.PostDeserialize();
			}
		}
		GameSettings.Instance.MyCompany.WorkItems.OfType<AutoDevWorkItem>().ToList<AutoDevWorkItem>().ForEach(delegate(AutoDevWorkItem x)
		{
			x.Leader = (Writeable.STGetDeserializedObject(x.LeaderID) as Actor);
		});
		if (mode == GameReader.LoadMode.Company)
		{
			for (int m = 0; m < GameSettings.Instance.sActorManager.Actors.Count; m++)
			{
				Actor actor2 = GameSettings.Instance.sActorManager.Actors[m];
				if (GameSettings.Instance.sActorManager.GetArriveTime(actor2) == null)
				{
					SDateTime sdateTime = SDateTime.Now();
					Team team = actor2.GetTeam();
					SDateTime sdateTime2 = (team == null) ? sdateTime : new SDateTime(0, team.WorkStart - 1, sdateTime.Day, sdateTime.Month, sdateTime.Year);
					sdateTime2 += new SDateTime(1, 0, 0);
					GameSettings.Instance.sActorManager.AddToAwaiting(actor2, sdateTime2, false, true);
				}
			}
			HUD.Instance.companyWindow.Bind();
			HUD.Instance.TeamWindow.TeamList.Items = GameSettings.Instance.sActorManager.Teams.Values.Cast<object>().ToList<object>();
			HUD.Instance.loanWindow.UpdateLoans();
		}
		TimeProbe.FinalizeTime("WriteDictionary element load time:");
		TimeProbe.FinalizeTime("Room segment load time:");
		TimeProbe.FinalizeTime("Furniture load time:");
		TimeProbe.FinalizeTime("Road manager load time:");
		TimeProbe.FinalizeTime("Actor load time:");
		TimeProbe.FinalizeTime("GameSettings load time:");
		TimeProbe.FinalizeTime("Teams load time:");
		TimeProbe.FinalizeTime("Cars load time:");
		GameSettings.Instance.sRoomManager.DisableMeshRebuild = false;
		if (mode == GameReader.LoadMode.Full || mode == GameReader.LoadMode.Building)
		{
			TimeProbe.BeginTime("Room mesh rebuild time:");
			for (int n = -1; n <= GameSettings.MaxFloor; n++)
			{
				GameSettings.Instance.sRoomManager.UpdateFloorMeshes(n, false);
			}
			TimeProbe.FinalizeTime("Room mesh rebuild time:");
			GameSettings.Instance.sRoomManager.RoomNearnessDirty = true;
		}
	}

	private static Writeable LoadElement(WriteDictionary dictionary)
	{
		string name = dictionary.Name;
		switch (name)
		{
		case "RoomSegment":
		{
			TimeProbe.BeginTime("Room segment load time:");
			GameObject gameObject = ObjectDatabase.Instance.RoomSegments.FirstOrDefault((GameObject x) => x.name.Equals(dictionary["Type"].ToString()));
			RoomSegment roomSegment = null;
			if (gameObject != null)
			{
				roomSegment = UnityEngine.Object.Instantiate<GameObject>(gameObject).GetComponent<RoomSegment>();
				roomSegment.name = gameObject.name;
				roomSegment.DeserializeThis(dictionary);
			}
			TimeProbe.EndTime("Room segment load time:");
			return roomSegment;
		}
		case "Furniture":
		{
			TimeProbe.BeginTime("Furniture load time:");
			string text = dictionary["Type"].ToString();
			GameObject furniture = ObjectDatabase.Instance.GetFurniture(text);
			if (!(furniture != null))
			{
				Debug.Log("Missing furniture " + text);
				if (!GameSettings.Instance.FurnitureErrorOccured)
				{
					WindowManager.Instance.ShowMessageBox("FurnitureLoadError".Loc(new object[]
					{
						text
					}), true, DialogWindow.DialogType.Error);
					GameSettings.Instance.FurnitureErrorOccured = true;
				}
				TimeProbe.EndTime("Furniture load time:");
				return null;
			}
			if (GameData.LoadYear > 1900 && !GameData.EditMode && furniture.GetComponent<Furniture>().UnlockYear > GameData.LoadYear)
			{
				GameSettings.Instance.FurnitureErrorOccured = true;
				return null;
			}
			Furniture component = UnityEngine.Object.Instantiate<GameObject>(furniture).GetComponent<Furniture>();
			component.name = furniture.name;
			component.DeserializeThis(dictionary);
			if (!component.isTemporary)
			{
				component.gameObject.SetActive(true);
			}
			TimeProbe.EndTime("Furniture load time:");
			return component;
		}
		case "GameSettings":
			TimeProbe.BeginTime("GameSettings load time:");
			GameSettings.Instance.Deserialize(dictionary);
			TimeProbe.EndTime("GameSettings load time:");
			break;
		case "Actor":
		{
			TimeProbe.BeginTime("Actor load time:");
			Actor actor = GameSettings.Instance.SpawnActor(dictionary.Get<bool>("Female", true));
			actor.GetComponent<Actor>().DeserializeThis(dictionary);
			TimeProbe.EndTime("Actor load time:");
			return actor;
		}
		case "Awaiting":
		{
			Dictionary<uint, SDateTime> dictionary2 = dictionary.Get<Dictionary<uint, SDateTime>>("Awaiting", new Dictionary<uint, SDateTime>());
			foreach (KeyValuePair<uint, SDateTime> keyValuePair in dictionary2)
			{
				Actor actor2 = Writeable.STGetDeserializedObject(keyValuePair.Key) as Actor;
				if (actor2 != null)
				{
					GameSettings.Instance.sActorManager.AddToAwaiting(actor2, keyValuePair.Value, false, true);
				}
			}
			break;
		}
		case "WaitingForBus":
			GameSettings.Instance.sActorManager.ReadyForBus = (from x in dictionary.Get<List<uint>>("Actors", new List<uint>())
			select (Actor)Writeable.STGetDeserializedObject(x)).ToHashSet<Actor>();
			break;
		case "ReadyForHome":
			GameSettings.Instance.sActorManager.ReadyForHome = (from x in dictionary.Get<List<uint>>("Actors", new List<uint>())
			select (Actor)Writeable.STGetDeserializedObject(x)).ToHashSet<Actor>();
			break;
		case "Team":
		{
			TimeProbe.BeginTime("Teams load time:");
			Team team = new Team("temp");
			string key = (string)dictionary["Name"];
			GameSettings.Instance.sActorManager.Teams.Add(key, team);
			team.Deserialize(dictionary);
			TimeProbe.EndTime("Teams load time:");
			break;
		}
		case "Camera":
			UnityEngine.Object.FindObjectOfType<CameraScript>().Deserialize(dictionary);
			break;
		case "Car":
		{
			TimeProbe.BeginTime("Cars load time:");
			int idx = dictionary.Get<int>("CarIdx", 1);
			CarScript carScript = RoadManager.Instance.CreateCar(idx);
			carScript.DeserializeThis(dictionary);
			TimeProbe.EndTime("Cars load time:");
			return carScript;
		}
		case "RoadManager":
			TimeProbe.BeginTime("Road manager load time:");
			RoadManager.Instance.DeserializeThis(dictionary);
			TimeProbe.EndTime("Road manager load time:");
			break;
		}
		return null;
	}

	public static float[] GetBuildingMeta()
	{
		if (GameSettings.Instance.RentMode)
		{
			List<Room> list = (from x in GameSettings.Instance.sRoomManager.GetRooms()
			where x.PlayerOwned
			select x).ToList<Room>();
			List<Room> list2 = (from x in GameSettings.Instance.sRoomManager.GetRooms()
			where x.Rentable
			select x).ToList<Room>();
			float[] array = new float[4];
			array[1] = list.SumSafe((Room x) => x.Area);
			array[2] = list.SumSafe((Room x) => BuildController.GetRoomCost(x.Edges, x.Area, x.Outdoors, x.Floor, false, true));
			array[3] = list2.SumSafe((Room x) => x.Area);
			return array;
		}
		List<Room> rooms = GameSettings.Instance.sRoomManager.GetRooms();
		float[] array2 = new float[4];
		array2[0] = 1f;
		array2[1] = rooms.SumSafe((Room x) => x.Area);
		array2[2] = GameSettings.Instance.GetMapCost(false);
		array2[3] = GameSettings.Instance.PlayerPlots.SumSafe((PlotArea x) => x.Area);
		return array2;
	}

	public static string SaveGame(SaveGame saveGame, GameReader.LoadMode mode, string fileName = null)
	{
		if (fileName == null)
		{
			fileName = saveGame.FilePath;
		}
		if (!saveGame.BuildingOnly && File.Exists(fileName) && Options.Backup)
		{
			try
			{
				File.Copy(fileName, fileName + ".bak", true);
			}
			catch (Exception)
			{
			}
		}
		Dictionary<string, byte[]> dictionary = new Dictionary<string, byte[]>();
		ConfigFile configFile = new ConfigFile();
		configFile.Add("CompanyName", saveGame.CompanyName);
		configFile.Add("InGameTime", saveGame.InGameTime.ToInt().ToString());
		configFile.Add("RealTime", saveGame.RealTime.ToString());
		configFile.Add("Money", saveGame.Money.ToString());
		configFile.Add("Products", saveGame.Products.ToString());
		configFile.Add("Employees", saveGame.Employees.ToString());
		configFile.Add("GameVersion", Versioning.VersionString);
		configFile.Add("DaysPerMonth", GameSettings.DaysPerMonth.ToString());
		configFile.Add("BuildingOnly", (mode == GameReader.LoadMode.Building).ToString());
		dictionary.Add("Meta", Utilities.GetBytesFromString(configFile.Serialize()));
		dictionary.Add("BuildingMeta", Utilities.GetBytesFromFloats(GameReader.GetBuildingMeta()));
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		using (MemoryStream memoryStream = new MemoryStream())
		{
			MiniMapMaker.MapDescriptor mapDescriptor = MinimapThumbnailMaker.Instance.MinimapMaker.MapDescFromGame((!GameSettings.Instance.RentMode) ? GameSettings.Instance.PlotRect() : GameSettings.Instance.BuildingRect());
			saveGame.Map = mapDescriptor;
			binaryFormatter.Serialize(memoryStream, mapDescriptor);
			dictionary.Add("Map", memoryStream.ToArray());
		}
		using (MemoryStream memoryStream2 = new MemoryStream())
		{
			RoomDescriptor graph = RoomDescriptor.SaveRooms((mode != GameReader.LoadMode.Full && mode != GameReader.LoadMode.Building) ? new List<Room>() : GameSettings.Instance.sRoomManager.GetRooms(), mode);
			binaryFormatter.Serialize(memoryStream2, graph);
			dictionary.Add("Rooms", memoryStream2.ToArray());
		}
		byte[] array = GameReader.CreateDictionaryData(mode);
		float num = (float)array.Length;
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		using (MemoryStream memoryStream3 = new MemoryStream())
		{
			using (GZipStream gzipStream = new GZipStream(memoryStream3, CompressionMode.Compress, false))
			{
				gzipStream.Write(array, 0, array.Length);
			}
			array = memoryStream3.ToArray();
			dictionary.Add("Data", array);
		}
		Debug.Log("Compress time: " + (Time.realtimeSinceStartup - realtimeSinceStartup));
		Debug.Log("Compress effect: " + ((float)array.Length / num * 100f).ToString("F2") + "%");
		return Utilities.WriteMultipleFiles(fileName, dictionary);
	}

	public static byte[] CreateDictionaryData(GameReader.LoadMode mode)
	{
		List<WriteDictionary> result = new List<WriteDictionary>();
		if (mode == GameReader.LoadMode.Full || mode == GameReader.LoadMode.Building)
		{
			foreach (RoomSegment roomSegment in from x in GameSettings.Instance.sRoomManager.GetAllSegments()
			where x.GetComponent<Furniture>() == null && !x.name.Contains("Wall")
			select x)
			{
				result.Add(roomSegment.SerializeThis(mode));
			}
			foreach (Furniture furniture in from x in GameSettings.Instance.sRoomManager.AllFurniture
			where x != null
			orderby x.GetSnappingDepth()
			select x)
			{
				result.Add(furniture.SerializeThis(mode));
			}
		}
		result.Add(GameSettings.Instance.Serialize(mode));
		if (mode == GameReader.LoadMode.Company || mode == GameReader.LoadMode.Full)
		{
			GameSettings.Instance.sActorManager.Actors.ForEach(delegate(Actor x)
			{
				result.Add(x.SerializeThis(mode));
			});
			if (mode == GameReader.LoadMode.Full)
			{
				GameSettings.Instance.sActorManager.Staff.ForEach(delegate(Actor x)
				{
					result.Add(x.SerializeThis(mode));
				});
				GameSettings.Instance.sActorManager.Guests.ForEach(delegate(Actor x)
				{
					result.Add(x.SerializeThis(mode));
				});
				WriteDictionary writeDictionary = new WriteDictionary("Awaiting");
				writeDictionary["Awaiting"] = GameSettings.Instance.sActorManager.GetAwaiting().ToDictionary((Actor x) => x.DID, (Actor x) => GameSettings.Instance.sActorManager.GetArriveTime(x).Value);
				result.Add(writeDictionary);
				WriteDictionary writeDictionary2 = new WriteDictionary("WaitingForBus");
				writeDictionary2["Actors"] = (from x in GameSettings.Instance.sActorManager.ReadyForBus
				select x.DID).ToList<uint>();
				result.Add(writeDictionary2);
				WriteDictionary writeDictionary3 = new WriteDictionary("ReadyForHome");
				writeDictionary3["Actors"] = (from x in GameSettings.Instance.sActorManager.ReadyForHome
				select x.DID).ToList<uint>();
				result.Add(writeDictionary3);
			}
			else
			{
				HashSet<Actor> actorList = GameSettings.Instance.sActorManager.Actors.ToHashSet<Actor>();
				WriteDictionary writeDictionary4 = new WriteDictionary("Awaiting");
				writeDictionary4["Awaiting"] = (from x in GameSettings.Instance.sActorManager.GetAwaiting()
				where actorList.Contains(x)
				select x).ToDictionary((Actor x) => x.DID, (Actor x) => GameSettings.Instance.sActorManager.GetArriveTime(x).Value);
				result.Add(writeDictionary4);
				WriteDictionary writeDictionary5 = new WriteDictionary("WaitingForBus");
				writeDictionary5["Actors"] = (from x in GameSettings.Instance.sActorManager.ReadyForBus
				where actorList.Contains(x)
				select x.DID).ToList<uint>();
				result.Add(writeDictionary5);
			}
			result.AddRange(from x in GameSettings.Instance.sActorManager.Teams
			select x.Value.Serialize());
		}
		if (mode == GameReader.LoadMode.Full || mode == GameReader.LoadMode.Building)
		{
			result.Add(CameraScript.Instance.Serialize());
			result.Add(RoadManager.Instance.SerializeThis(mode));
		}
		if (mode == GameReader.LoadMode.Full)
		{
			result.AddRange(from x in RoadManager.Instance.Cars
			select x.SerializeThis(mode));
		}
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		byte[] result2 = Serializer.Serialize(result.ToArray());
		Debug.Log("Serialization time: " + (Time.realtimeSinceStartup - realtimeSinceStartup));
		return result2;
	}

	public enum LoadMode
	{
		Full,
		Building,
		Company
	}
}
