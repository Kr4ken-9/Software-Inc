﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TutorialMessage
{
	public TutorialMessage(XMLParser.XMLNode data)
	{
		this.Message = data.GetNode("ID", true).Value;
		this.NonLoc = data.GetNode("Message", true).Value;
		XMLParser.XMLNode[] nodes = data.GetNodes("Continue", false);
		if (nodes.Length > 0)
		{
			this.ManualContinue = false;
			this._continueNames = new string[nodes.Length];
			this._continues = new Func<bool>[nodes.Length];
			for (int i = 0; i < nodes.Length; i++)
			{
				this._continueNames[i] = nodes[i].Value;
				if (this._continueNames[i][0] == '!')
				{
					this._continueNames[i] = this._continueNames[i].Substring(1);
					Func<bool> func = TutorialSystem.ContinueChecks.GetOrDefault(this._continueNames[i], () => false);
					this._continues[i] = (() => !func());
				}
				else
				{
					this._continues[i] = TutorialSystem.ContinueChecks.GetOrDefault(this._continueNames[i], () => true);
				}
			}
		}
		else
		{
			this.ManualContinue = true;
		}
		this.Points.AddRange(from x in data.GetNode("Points", true).Children
		select new TutorialMessage.TutorialPoint(x));
	}

	public bool CanContinue()
	{
		for (int i = 0; i < this._continues.Length; i++)
		{
			if (!this._continues[i]())
			{
				return false;
			}
		}
		return true;
	}

	public List<GameObject> GetPoints()
	{
		List<GameObject> list = new List<GameObject>();
		list.AddRange(this.Points.Select(delegate(TutorialMessage.TutorialPoint point)
		{
			if (point.ThreeD)
			{
				GameObject gameObject = TutorialSystem.Instance.InsantiateArrow();
				GUIArrow component = gameObject.GetComponent<GUIArrow>();
				component.ThreeD = true;
				component.ThreeDP = point.P;
				return gameObject;
			}
			if (point.Angled)
			{
				GameObject gameObject2 = TutorialSystem.Instance.InsantiateArrow();
				GUIArrow component2 = gameObject2.GetComponent<GUIArrow>();
				component2.ScreenParent = string.IsNullOrEmpty(point.ElementAnchor);
				component2.Anchor = point.ElementAnchor;
				component2.ThreeD = false;
				component2.AnyAngle = false;
				component2.Angle = point.Angle;
				component2.Offset = point.P;
				component2.HorizontalAlign = point.HAnchor;
				component2.VerticalAlign = point.VAnchor;
				return gameObject2;
			}
			GameObject gameObject3 = TutorialSystem.Instance.InsantiateArrow();
			GUIArrow component3 = gameObject3.GetComponent<GUIArrow>();
			component3.ScreenParent = string.IsNullOrEmpty(point.ElementAnchor);
			component3.Anchor = point.ElementAnchor;
			component3.ThreeD = false;
			component3.AnyAngle = true;
			component3.Offset = point.P;
			component3.HorizontalAlign = point.HAnchor;
			component3.VerticalAlign = point.VAnchor;
			return gameObject3;
		}));
		return list;
	}

	public readonly string NonLoc;

	public readonly string Message;

	private readonly string[] _continueNames;

	private readonly Func<bool>[] _continues;

	public readonly bool ManualContinue;

	public readonly List<TutorialMessage.TutorialPoint> Points = new List<TutorialMessage.TutorialPoint>();

	public enum HorizontalAnchor
	{
		Left,
		Center,
		Right
	}

	public enum VerticalAnchor
	{
		Top,
		Middle,
		Bottom
	}

	public class TutorialPoint
	{
		public TutorialPoint(Vector3 p, bool threeD, bool angled, float angle, string elementAnchor, TutorialMessage.HorizontalAnchor hAnchor, TutorialMessage.VerticalAnchor vAnchor)
		{
			this.P = p;
			this.ThreeD = threeD;
			this.Angled = angled;
			this.Angle = angle;
			this.ElementAnchor = elementAnchor;
			this.HAnchor = hAnchor;
			this.VAnchor = vAnchor;
		}

		public TutorialPoint(XMLParser.XMLNode node)
		{
			this.P = SVector3.Deserialize(node.GetNode("Position", true).Value, false).ToVector3();
			this.ThreeD = (node.GetNode("Position", true).Value.CountLetter(',') == 2);
			XMLParser.XMLNode node2 = node.GetNode("Angle", false);
			this.Angled = (node2 != null);
			if (this.Angled)
			{
				this.Angle = (float)Convert.ToDouble(node2.Value);
			}
			XMLParser.XMLNode node3 = node.GetNode("Element", false);
			this.ElementAnchor = ((node3 != null) ? node3.Value : null);
			string text = node.TryGetAttribute("Anchor", null);
			if (text != null)
			{
				string[] array = text.Split(new char[]
				{
					','
				});
				this.VAnchor = (TutorialMessage.VerticalAnchor)Enum.Parse(typeof(TutorialMessage.VerticalAnchor), array[0], true);
				this.HAnchor = (TutorialMessage.HorizontalAnchor)Enum.Parse(typeof(TutorialMessage.HorizontalAnchor), array[1], true);
			}
			else
			{
				this.VAnchor = TutorialMessage.VerticalAnchor.Top;
				this.HAnchor = TutorialMessage.HorizontalAnchor.Left;
			}
		}

		public Vector3 P;

		public bool ThreeD;

		public bool Angled;

		public float Angle;

		public string ElementAnchor;

		public TutorialMessage.HorizontalAnchor HAnchor;

		public TutorialMessage.VerticalAnchor VAnchor;
	}
}
