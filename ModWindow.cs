﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ModWindow : MonoBehaviour
{
	private void Start()
	{
		ModWindow.Instance = this;
		this.Refresh();
	}

	private void OnDestroy()
	{
		ModWindow.Instance = null;
	}

	public void Refresh()
	{
		HashSet<IWorkshopItem> hashSet = new HashSet<IWorkshopItem>();
		hashSet.AddRange((from x in GameData.ModPackages
		where x.CanUpload
		select x).OfType<IWorkshopItem>());
		hashSet.AddRange((from x in Localization.Translations.Values
		where x.CanUpload
		select x).OfType<IWorkshopItem>());
		hashSet.AddRange((from x in GameData.Prefabs
		where x.CanUpload
		select x).OfType<IWorkshopItem>());
		hashSet.AddRange((from x in FurnitureLoader.LoadedFurniture
		where x.CanUpload
		select x).OfType<IWorkshopItem>());
		hashSet.AddRange((from x in RoomMaterialController.Instance.MaterialPacks
		where x.CanUpload
		select x).OfType<IWorkshopItem>());
		hashSet.AddRange((from x in SaveGameManager.WorkshoppableGames
		where x.CanUpload
		select x).OfType<IWorkshopItem>());
		hashSet.AddRange(from x in SteamWorkshop.WorkshopItems.Values
		where x.CanUpload
		select x);
		this.List.Items.Clear();
		this.List.Items.AddRange(hashSet.Cast<object>());
	}

	public GUIListView List;

	public static ModWindow Instance;
}
