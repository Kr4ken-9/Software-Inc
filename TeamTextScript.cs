﻿using System;
using UnityEngine;

public class TeamTextScript : MonoBehaviour
{
	private void Start()
	{
		this.rend = base.GetComponent<Renderer>();
	}

	private void OnEnable()
	{
		base.transform.rotation = Quaternion.LookRotation(new Vector3(base.transform.position.x - CameraScript.Instance.mainCam.transform.position.x, 0f, base.transform.position.z - CameraScript.Instance.mainCam.transform.position.z));
	}

	private void Update()
	{
		bool flag = this.InUse && GameSettings.Instance.ActiveFloor == this.Parent.Floor;
		if (this.rend.enabled != flag)
		{
			this.rend.enabled = flag;
		}
		if (flag)
		{
			if (CameraScript.Instance.TopDown)
			{
				base.transform.rotation = Quaternion.Euler(90f, CameraScript.Instance.transform.rotation.eulerAngles.y, 0f);
				if (this.Bottom)
				{
					base.transform.position = this.OrigPos - Quaternion.Euler(0f, CameraScript.Instance.transform.rotation.eulerAngles.y, 0f) * Vector3.forward;
				}
			}
			else
			{
				if (this.Bottom)
				{
					base.transform.position = this.OrigPos;
				}
				base.transform.rotation = Quaternion.LookRotation(new Vector3(base.transform.position.x - CameraScript.Instance.mainCam.transform.position.x, 0f, base.transform.position.z - CameraScript.Instance.mainCam.transform.position.z));
			}
		}
	}

	public Room Parent;

	public bool InUse;

	public bool Bottom;

	private Renderer rend;

	public Vector3 OrigPos;

	public TextMesh tm;
}
