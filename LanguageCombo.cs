﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LanguageCombo : MonoBehaviour
{
	private void Awake()
	{
		if (LanguageCombo.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		LanguageCombo.Instance = this;
	}

	private void OnDestroy()
	{
		if (LanguageCombo.Instance == this)
		{
			LanguageCombo.Instance = null;
		}
	}

	private void Start()
	{
		this._immediateLoc = new string[this.ImmediateEffect.Length];
		for (int i = 0; i < this.ImmediateEffect.Length; i++)
		{
			this._immediateLoc[i] = this.ImmediateEffect[i].text;
		}
		this.Refresh();
	}

	public void RefreshMainMenu()
	{
		for (int i = 0; i < this.ImmediateEffect.Length; i++)
		{
			this.ImmediateEffect[i].text = this._immediateLoc[i].LocTry();
		}
	}

	public void Refresh()
	{
		GUICombobox combo = base.GetComponent<GUICombobox>();
		combo.OnSelectedChanged.RemoveAllListeners();
		combo.UpdateContent<Localization.Translation>(from x in Localization.Translations.Values
		orderby x.Name
		select x);
		combo.SelectedItem = Localization.Translations[Localization.CurrentTranslation];
		combo.OnSelectedChanged.AddListener(delegate
		{
			Options.Language = ((Localization.Translation)combo.SelectedItem).RealName;
			this.RefreshMainMenu();
		});
		this.RefreshMainMenu();
	}

	public Text[] ImmediateEffect;

	private string[] _immediateLoc;

	public static LanguageCombo Instance;
}
