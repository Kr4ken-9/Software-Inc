﻿using System;
using UnityEngine;

[Serializable]
public class Stock
{
	public Stock()
	{
	}

	public Stock(uint owner, uint target, float price, float percent)
	{
		this.Owner = owner;
		this.Target = target;
		this.InitialPrice = price;
		this.Percentage = percent;
	}

	public Company OwnerCompany
	{
		get
		{
			return GameSettings.Instance.simulation.GetCompany(this.Owner);
		}
	}

	public Company TargetCompany
	{
		get
		{
			return GameSettings.Instance.simulation.GetCompany(this.Target);
		}
	}

	public float RealPercentage
	{
		get
		{
			return this.Percentage / this.TargetCompany.GetSumStock();
		}
	}

	public float CurrentWorth
	{
		get
		{
			return this.Percentage * this.TargetCompany.GetMoneyWithInsurance(true);
		}
	}

	public float Change
	{
		get
		{
			return this.CurrentWorth / Mathf.Max(1f, this.InitialPrice) - 1f;
		}
	}

	public uint Owner;

	public uint Target;

	public float InitialPrice;

	public float Percentage;
}
