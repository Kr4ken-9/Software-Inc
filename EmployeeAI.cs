﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class EmployeeAI : AI<Actor>
{
	public override void InitTree()
	{
		string name = "Spawn";
		if (EmployeeAI.<>f__mg$cache0 == null)
		{
			EmployeeAI.<>f__mg$cache0 = new Func<Actor, int>(EmployeeAI.Spawn);
		}
		BehaviorNode<Actor> behaviorNode = new BehaviorNode<Actor>(name, EmployeeAI.<>f__mg$cache0, true, -1);
		string name2 = "GoToDesk";
		if (EmployeeAI.<>f__mg$cache1 == null)
		{
			EmployeeAI.<>f__mg$cache1 = new Func<Actor, int>(EmployeeAI.GoToDesk);
		}
		BehaviorNode<Actor> behaviorNode2 = new BehaviorNode<Actor>(name2, EmployeeAI.<>f__mg$cache1, true, -1);
		string name3 = "IsOff";
		if (EmployeeAI.<>f__mg$cache2 == null)
		{
			EmployeeAI.<>f__mg$cache2 = new Func<Actor, int>(EmployeeAI.IsOff);
		}
		BehaviorNode<Actor> behaviorNode3 = new BehaviorNode<Actor>(name3, EmployeeAI.<>f__mg$cache2, false, -1);
		string name4 = "GoHome";
		if (EmployeeAI.<>f__mg$cache3 == null)
		{
			EmployeeAI.<>f__mg$cache3 = new Func<Actor, int>(Actor.GoHomeState);
		}
		BehaviorNode<Actor> behaviorNode4 = new BehaviorNode<Actor>(name4, EmployeeAI.<>f__mg$cache3, true, 99);
		string name5 = "Despawn";
		if (EmployeeAI.<>f__mg$cache4 == null)
		{
			EmployeeAI.<>f__mg$cache4 = new Func<Actor, int>(EmployeeAI.Despawn);
		}
		BehaviorNode<Actor> behaviorNode5 = new BehaviorNode<Actor>(name5, EmployeeAI.<>f__mg$cache4, true, 99);
		string name6 = "StandAround";
		if (EmployeeAI.<>f__mg$cache5 == null)
		{
			EmployeeAI.<>f__mg$cache5 = new Func<Actor, int>(EmployeeAI.StandAround);
		}
		BehaviorNode<Actor> value = new BehaviorNode<Actor>(name6, EmployeeAI.<>f__mg$cache5, true, -1);
		string name7 = "Work";
		if (EmployeeAI.<>f__mg$cache6 == null)
		{
			EmployeeAI.<>f__mg$cache6 = new Func<Actor, int>(EmployeeAI.Work);
		}
		BehaviorNode<Actor> behaviorNode6 = new BehaviorNode<Actor>(name7, EmployeeAI.<>f__mg$cache6, true, 5);
		string name8 = "IsHungry";
		if (EmployeeAI.<>f__mg$cache7 == null)
		{
			EmployeeAI.<>f__mg$cache7 = new Func<Actor, int>(EmployeeAI.IsHungry);
		}
		BehaviorNode<Actor> behaviorNode7 = new BehaviorNode<Actor>(name8, EmployeeAI.<>f__mg$cache7, false, -1);
		string name9 = "GoToServingTray";
		if (EmployeeAI.<>f__mg$cache8 == null)
		{
			EmployeeAI.<>f__mg$cache8 = new Func<Actor, int>(EmployeeAI.GoToServingTray);
		}
		BehaviorNode<Actor> behaviorNode8 = new BehaviorNode<Actor>(name9, EmployeeAI.<>f__mg$cache8, true, 88);
		string name10 = "TakeFoodFromTray";
		if (EmployeeAI.<>f__mg$cache9 == null)
		{
			EmployeeAI.<>f__mg$cache9 = new Func<Actor, int>(EmployeeAI.TakeFoodFromTray);
		}
		BehaviorNode<Actor> behaviorNode9 = new BehaviorNode<Actor>(name10, EmployeeAI.<>f__mg$cache9, true, 88);
		string name11 = "GoToEat";
		if (EmployeeAI.<>f__mg$cacheA == null)
		{
			EmployeeAI.<>f__mg$cacheA = new Func<Actor, int>(EmployeeAI.GoToEat);
		}
		BehaviorNode<Actor> behaviorNode10 = new BehaviorNode<Actor>(name11, EmployeeAI.<>f__mg$cacheA, true, 88);
		string name12 = "EatRealFood";
		if (EmployeeAI.<>f__mg$cacheB == null)
		{
			EmployeeAI.<>f__mg$cacheB = new Func<Actor, int>(EmployeeAI.EatRealFood);
		}
		BehaviorNode<Actor> behaviorNode11 = new BehaviorNode<Actor>(name12, EmployeeAI.<>f__mg$cacheB, true, 88);
		string name13 = "GoToFridge";
		if (EmployeeAI.<>f__mg$cacheC == null)
		{
			EmployeeAI.<>f__mg$cacheC = new Func<Actor, int>(EmployeeAI.GoToFridge);
		}
		BehaviorNode<Actor> behaviorNode12 = new BehaviorNode<Actor>(name13, EmployeeAI.<>f__mg$cacheC, true, 88);
		string name14 = "GetFood";
		if (EmployeeAI.<>f__mg$cacheD == null)
		{
			EmployeeAI.<>f__mg$cacheD = new Func<Actor, int>(EmployeeAI.GetFood);
		}
		BehaviorNode<Actor> behaviorNode13 = new BehaviorNode<Actor>(name14, EmployeeAI.<>f__mg$cacheD, true, 88);
		string name15 = "WantCoffee";
		if (EmployeeAI.<>f__mg$cacheE == null)
		{
			EmployeeAI.<>f__mg$cacheE = new Func<Actor, int>(EmployeeAI.WantCoffee);
		}
		BehaviorNode<Actor> behaviorNode14 = new BehaviorNode<Actor>(name15, EmployeeAI.<>f__mg$cacheE, false, -1);
		string name16 = "GoToCoffee";
		if (EmployeeAI.<>f__mg$cacheF == null)
		{
			EmployeeAI.<>f__mg$cacheF = new Func<Actor, int>(EmployeeAI.GoToCoffee);
		}
		BehaviorNode<Actor> behaviorNode15 = new BehaviorNode<Actor>(name16, EmployeeAI.<>f__mg$cacheF, true, -1);
		string name17 = "MakeCoffee";
		if (EmployeeAI.<>f__mg$cache10 == null)
		{
			EmployeeAI.<>f__mg$cache10 = new Func<Actor, int>(EmployeeAI.MakeCoffee);
		}
		BehaviorNode<Actor> behaviorNode16 = new BehaviorNode<Actor>(name17, EmployeeAI.<>f__mg$cache10, true, -1);
		string name18 = "HasMakeMeeting";
		if (EmployeeAI.<>f__mg$cache11 == null)
		{
			EmployeeAI.<>f__mg$cache11 = new Func<Actor, int>(EmployeeAI.HasMakeMeeting);
		}
		BehaviorNode<Actor> behaviorNode17 = new BehaviorNode<Actor>(name18, EmployeeAI.<>f__mg$cache11, false, -1);
		string name19 = "GoToMeeting";
		if (EmployeeAI.<>f__mg$cache12 == null)
		{
			EmployeeAI.<>f__mg$cache12 = new Func<Actor, int>(EmployeeAI.GoToMeeting);
		}
		BehaviorNode<Actor> behaviorNode18 = new BehaviorNode<Actor>(name19, EmployeeAI.<>f__mg$cache12, true, -1);
		string name20 = "HaveMeeting";
		if (EmployeeAI.<>f__mg$cache13 == null)
		{
			EmployeeAI.<>f__mg$cache13 = new Func<Actor, int>(EmployeeAI.HaveMeeting);
		}
		BehaviorNode<Actor> behaviorNode19 = new BehaviorNode<Actor>(name20, EmployeeAI.<>f__mg$cache13, true, 99);
		string name21 = "HasToPee";
		if (EmployeeAI.<>f__mg$cache14 == null)
		{
			EmployeeAI.<>f__mg$cache14 = new Func<Actor, int>(EmployeeAI.HasToPee);
		}
		BehaviorNode<Actor> behaviorNode20 = new BehaviorNode<Actor>(name21, EmployeeAI.<>f__mg$cache14, false, -1);
		string name22 = "GoToToilet";
		if (EmployeeAI.<>f__mg$cache15 == null)
		{
			EmployeeAI.<>f__mg$cache15 = new Func<Actor, int>(EmployeeAI.GoToToilet);
		}
		BehaviorNode<Actor> behaviorNode21 = new BehaviorNode<Actor>(name22, EmployeeAI.<>f__mg$cache15, true, 77);
		string name23 = "DoBusiness";
		if (EmployeeAI.<>f__mg$cache16 == null)
		{
			EmployeeAI.<>f__mg$cache16 = new Func<Actor, int>(EmployeeAI.DoBusiness);
		}
		BehaviorNode<Actor> behaviorNode22 = new BehaviorNode<Actor>(name23, EmployeeAI.<>f__mg$cache16, true, 77);
		string name24 = "Loiter";
		if (EmployeeAI.<>f__mg$cache17 == null)
		{
			EmployeeAI.<>f__mg$cache17 = new Func<Actor, int>(EmployeeAI.Loiter);
		}
		BehaviorNode<Actor> behaviorNode23 = new BehaviorNode<Actor>(name24, EmployeeAI.<>f__mg$cache17, true, 1);
		string name25 = "NeedsSocial";
		if (EmployeeAI.<>f__mg$cache18 == null)
		{
			EmployeeAI.<>f__mg$cache18 = new Func<Actor, int>(EmployeeAI.NeedsSocial);
		}
		BehaviorNode<Actor> behaviorNode24 = new BehaviorNode<Actor>(name25, EmployeeAI.<>f__mg$cache18, false, -1);
		string name26 = "GoToSocial";
		if (EmployeeAI.<>f__mg$cache19 == null)
		{
			EmployeeAI.<>f__mg$cache19 = new Func<Actor, int>(EmployeeAI.GoToSocial);
		}
		BehaviorNode<Actor> behaviorNode25 = new BehaviorNode<Actor>(name26, EmployeeAI.<>f__mg$cache19, true, -1);
		string name27 = "BeSocial";
		if (EmployeeAI.<>f__mg$cache1A == null)
		{
			EmployeeAI.<>f__mg$cache1A = new Func<Actor, int>(EmployeeAI.BeSocial);
		}
		BehaviorNode<Actor> behaviorNode26 = new BehaviorNode<Actor>(name27, EmployeeAI.<>f__mg$cache1A, true, -1);
		string name28 = "NeedsSocialLeader";
		if (EmployeeAI.<>f__mg$cache1B == null)
		{
			EmployeeAI.<>f__mg$cache1B = new Func<Actor, int>(EmployeeAI.NeedsSocialLeader);
		}
		BehaviorNode<Actor> behaviorNode27 = new BehaviorNode<Actor>(name28, EmployeeAI.<>f__mg$cache1B, false, -1);
		string name29 = "GoToSocialLeader";
		if (EmployeeAI.<>f__mg$cache1C == null)
		{
			EmployeeAI.<>f__mg$cache1C = new Func<Actor, int>(EmployeeAI.GoToSocialLeader);
		}
		BehaviorNode<Actor> behaviorNode28 = new BehaviorNode<Actor>(name29, EmployeeAI.<>f__mg$cache1C, true, -1);
		string name30 = "BeSocialLeader";
		if (EmployeeAI.<>f__mg$cache1D == null)
		{
			EmployeeAI.<>f__mg$cache1D = new Func<Actor, int>(EmployeeAI.BeSocialLeader);
		}
		BehaviorNode<Actor> behaviorNode29 = new BehaviorNode<Actor>(name30, EmployeeAI.<>f__mg$cache1D, true, -1);
		string name31 = "GoHomeBusStop";
		if (EmployeeAI.<>f__mg$cache1E == null)
		{
			EmployeeAI.<>f__mg$cache1E = new Func<Actor, int>(Actor.GoHomeBusStop);
		}
		BehaviorNode<Actor> behaviorNode30 = new BehaviorNode<Actor>(name31, EmployeeAI.<>f__mg$cache1E, true, 99);
		string name32 = "ShouldUseBus";
		if (EmployeeAI.<>f__mg$cache1F == null)
		{
			EmployeeAI.<>f__mg$cache1F = new Func<Actor, int>(Actor.ShouldUseBus);
		}
		BehaviorNode<Actor> behaviorNode31 = new BehaviorNode<Actor>(name32, EmployeeAI.<>f__mg$cache1F, true, 99);
		this.BehaviorNodes.Add("Spawn", behaviorNode);
		this.BehaviorNodes.Add("GoToDesk", behaviorNode2);
		this.BehaviorNodes.Add("IsOff", behaviorNode3);
		this.BehaviorNodes.Add("GoHome", behaviorNode4);
		this.BehaviorNodes.Add("Despawn", behaviorNode5);
		this.BehaviorNodes.Add("StandAround", value);
		this.BehaviorNodes.Add("Work", behaviorNode6);
		this.BehaviorNodes.Add("IsHungry", behaviorNode7);
		this.BehaviorNodes.Add("GoToServingTray", behaviorNode8);
		this.BehaviorNodes.Add("TakeFoodFromTray", behaviorNode9);
		this.BehaviorNodes.Add("GoToEat", behaviorNode10);
		this.BehaviorNodes.Add("EatRealFood", behaviorNode11);
		this.BehaviorNodes.Add("GoToFridge", behaviorNode12);
		this.BehaviorNodes.Add("GetFood", behaviorNode13);
		this.BehaviorNodes.Add("WantCoffee", behaviorNode14);
		this.BehaviorNodes.Add("GoToCoffee", behaviorNode15);
		this.BehaviorNodes.Add("MakeCoffee", behaviorNode16);
		this.BehaviorNodes.Add("HasMakeMeeting", behaviorNode17);
		this.BehaviorNodes.Add("GoToMeeting", behaviorNode18);
		this.BehaviorNodes.Add("HaveMeeting", behaviorNode19);
		this.BehaviorNodes.Add("HasToPee", behaviorNode20);
		this.BehaviorNodes.Add("GoToToilet", behaviorNode21);
		this.BehaviorNodes.Add("DoBusiness", behaviorNode22);
		this.BehaviorNodes.Add("Loiter", behaviorNode23);
		this.BehaviorNodes.Add("NeedsSocial", behaviorNode24);
		this.BehaviorNodes.Add("GoToSocial", behaviorNode25);
		this.BehaviorNodes.Add("BeSocial", behaviorNode26);
		this.BehaviorNodes.Add("NeedsSocialLeader", behaviorNode27);
		this.BehaviorNodes.Add("GoToSocialLeader", behaviorNode28);
		this.BehaviorNodes.Add("BeSocialLeader", behaviorNode29);
		this.BehaviorNodes.Add("GoHomeBusStop", behaviorNode30);
		this.BehaviorNodes.Add("ShouldUseBus", behaviorNode31);
		behaviorNode.Success = behaviorNode2;
		behaviorNode2.Success = behaviorNode6;
		behaviorNode2.Failure = behaviorNode23;
		behaviorNode6.Success = behaviorNode3;
		behaviorNode6.Failure = behaviorNode2;
		behaviorNode7.Success = behaviorNode8;
		behaviorNode7.Failure = behaviorNode20;
		behaviorNode8.Success = behaviorNode9;
		behaviorNode8.Failure = behaviorNode12;
		behaviorNode9.Success = behaviorNode10;
		behaviorNode9.Failure = behaviorNode12;
		behaviorNode10.Success = behaviorNode11;
		behaviorNode10.Failure = behaviorNode12;
		behaviorNode11.Success = behaviorNode20;
		behaviorNode12.Success = behaviorNode13;
		behaviorNode12.Failure = behaviorNode20;
		behaviorNode13.Success = behaviorNode20;
		behaviorNode20.Success = behaviorNode21;
		behaviorNode20.Failure = behaviorNode14;
		behaviorNode21.Success = behaviorNode22;
		behaviorNode21.Failure = behaviorNode14;
		behaviorNode22.Success = behaviorNode14;
		behaviorNode14.Success = behaviorNode15;
		behaviorNode14.Failure = behaviorNode24;
		behaviorNode15.Success = behaviorNode16;
		behaviorNode15.Failure = behaviorNode24;
		behaviorNode16.Success = behaviorNode24;
		behaviorNode24.Success = behaviorNode25;
		behaviorNode24.Failure = behaviorNode27;
		behaviorNode25.Success = behaviorNode26;
		behaviorNode25.Failure = behaviorNode27;
		behaviorNode26.Success = behaviorNode17;
		behaviorNode27.Success = behaviorNode28;
		behaviorNode27.Failure = behaviorNode17;
		behaviorNode28.Success = behaviorNode29;
		behaviorNode28.Failure = behaviorNode17;
		behaviorNode29.Success = behaviorNode17;
		behaviorNode17.Success = behaviorNode18;
		behaviorNode17.Failure = behaviorNode6;
		behaviorNode18.Success = behaviorNode19;
		behaviorNode18.Failure = behaviorNode3;
		behaviorNode19.Success = behaviorNode3;
		behaviorNode3.Success = behaviorNode31;
		behaviorNode3.Failure = behaviorNode7;
		behaviorNode4.Success = behaviorNode5;
		behaviorNode4.Failure = behaviorNode30;
		behaviorNode5.Success = AI<Actor>.DummyNode;
		behaviorNode23.Success = behaviorNode3;
		behaviorNode31.Success = behaviorNode30;
		behaviorNode31.Failure = behaviorNode4;
		behaviorNode30.Success = behaviorNode5;
		behaviorNode30.Failure = behaviorNode7;
	}

	private static int Spawn(Actor self)
	{
		if (self.CoursePoints > 0f)
		{
			float num = self.employee.Autodidactic.MapRange(-1f, 1f, 0.75f, 1.25f, false);
			self.employee.AddToSpecialization(self.CourseRole, self.CourseSpec, num * self.CoursePoints, 1f, true);
			self.CoursePoints = 0f;
		}
		if (self.HREd)
		{
			self.employee.HR = true;
			self.HREd = false;
			TutorialSystem.Instance.StartTutorial("HR", false);
		}
		if (self.Despawned)
		{
			self.employee.Spawn();
			self.SpecialState = Actor.HomeState.Default;
			self.Despawned = false;
		}
		if (self.CurrentPath == null)
		{
			return 2;
		}
		return (!self.WalkPath()) ? 1 : 2;
	}

	private static int GoToDesk(Actor self)
	{
		int num = self.GoToFurniture("Computer", "Use", -1, false, null, false, false, null);
		if (num != 0)
		{
			self.FreeLoiterTable();
		}
		if (num > 0)
		{
			self.MakeUnIdle();
		}
		if (num == 2)
		{
			if (self.UsingPoint == null)
			{
				return 0;
			}
			self.UsingPoint.Parent.Reserved = self;
			if (self.coffee != null)
			{
				Furniture parent = self.UsingPoint.Parent.SnappedTo.Parent;
				if (parent.Table != null)
				{
					Holdable component = self.coffee.GetComponent<Holdable>();
					parent.Table.PlaceHoldable(component, self.UsingPoint.Parent.SnappedTo);
					self.LeaveItem(component, false);
				}
			}
			self.TurnToFurniture();
		}
		return num;
	}

	private static int IsOff(Actor self)
	{
		if (self.GoHomeNow)
		{
			self.FreeLoiterTable();
			if (self.UsingPoint != null)
			{
				self.ShutdownPC();
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
			self.CurrentPath = null;
			return 2;
		}
		return 0;
	}

	private static int Despawn(Actor self)
	{
		self.MakeUnIdle();
		GameSettings.Instance.SalaryDue += self.GetRealSalary() / (float)GameSettings.DaysPerMonth;
		if (self.coffee != null)
		{
			self.LeaveItem(self.coffee.GetComponent<Holdable>(), true);
		}
		foreach (InteractionPoint interactionPoint in self.InQueue.Values)
		{
			interactionPoint.RemoveFromQueue(self);
		}
		self.InQueue.Clear();
		self.Despawned = true;
		GameSettings.Instance.sRoomManager.ClearReservations(self);
		if (self.employee.Fired)
		{
			self.enabled = false;
			self.Dismiss();
			return 2;
		}
		if (!self.TakingCourses)
		{
			SDateTime d = SDateTime.Now();
			if (self.SpawnTime <= d.Hour)
			{
				d += new SDateTime(1, 0, 0);
			}
			int max = Mathf.CeilToInt(self.employee.Diligence.MapRange(-1f, 1f, 16f, 48f, false));
			if (!self.employee.Fired && self.employee.AgeMonth >= Employee.RetirementAge * 12 - 1)
			{
				HUD.Instance.AddPopupMessage("RetireNotify".Loc(new object[]
				{
					self.employee.FullName
				}), "Exclamation", PopupManager.PopUpAction.OpenInsurance, 0u, PopupManager.NotificationSound.Issue, 2f, PopupManager.PopupIDs.None, 6);
				if (self.employee.IsRole(Employee.RoleBit.Lead) && self.Team != null && self.employee.HR)
				{
					HUD.Instance.AddPopupMessage("NoHRWarning".Loc(new object[]
					{
						self.Team
					}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.None, 6);
				}
				HUD.Instance.insuranceWindow.AddRetire();
				self.SpecialState = Actor.HomeState.Retired;
				HUD.Instance.insuranceWindow.Terminations.Items.Add(new EmployeeTermination(self, EmployeeTermination.TerminationType.Retired, 0f));
				self.Dismiss();
			}
			else if (!self.employee.Fired && !self.employee.Founder)
			{
				int num = Mathf.RoundToInt(self.GetBenefitValue("Vacation months"));
				if (num > 0 && self.AlternateVacation < SDateTime.Now() + new SDateTime(0, 0, 1, 0, 0))
				{
					d += new SDateTime(0, num, 0);
					self.employee.AddInstantMood("GoodVacation", self, (float)num / 6f);
					self.SpecialState = Actor.HomeState.Vacation;
					self.IgnoreOffSalary = true;
					if (self.Team != null)
					{
						self.ScheduleVacation(true);
					}
					if (self.Team == null || !self.GetTeam().HREnabled)
					{
						GameSettings.Instance.Vacations++;
					}
				}
				else if (UnityEngine.Random.Range(0, max) == 0)
				{
					d += new SDateTime(1, 0, 0);
					self.SpecialState = Actor.HomeState.Sick;
					TimeOfDay.Instance.AddToSick(self);
				}
				else if ((SDateTime.Now() - self.employee.Hired).Year >= 2 && UnityEngine.Random.Range(0, 10000 * GameSettings.DaysPerMonth) == 0)
				{
					self.QuitAffectTeam();
					if (UnityEngine.Random.value < self.employee.Age - (float)Employee.Youngest / (float)(Employee.RetirementAge - Employee.Youngest))
					{
						self.SpecialState = Actor.HomeState.Dead;
						HUD.Instance.AddPopupMessage("DiedNotify".Loc(new object[]
						{
							self.employee.FullName
						}), "Exclamation", PopupManager.PopUpAction.OpenInsurance, 0u, PopupManager.NotificationSound.Issue, 2f, PopupManager.PopupIDs.None, 6);
						if (self.employee.IsRole(Employee.RoleBit.Lead) && self.Team != null && self.employee.HR)
						{
							HUD.Instance.AddPopupMessage("NoHRWarning".Loc(new object[]
							{
								self.Team
							}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.None, 6);
						}
						HUD.Instance.insuranceWindow.AddRetire();
						float benefitValue = self.GetBenefitValue("Life insurance");
						GameSettings.Instance.MyCompany.MakeTransaction(-benefitValue, Company.TransactionCategory.Benefits, "Life insurance");
						HUD.Instance.insuranceWindow.Terminations.Items.Add(new EmployeeTermination(self, EmployeeTermination.TerminationType.Dead, benefitValue));
						self.Dismiss();
					}
					else
					{
						self.SpecialState = Actor.HomeState.Hospitalized;
						HUD.Instance.AddPopupMessage("HospitalizedNotify".Loc(new object[]
						{
							self.employee.FullName
						}), "Exclamation", PopupManager.PopUpAction.OpenInsurance, 0u, PopupManager.NotificationSound.Issue, 2f, PopupManager.PopupIDs.None, 6);
						if (self.employee.IsRole(Employee.RoleBit.Lead) && self.Team != null && self.employee.HR)
						{
							HUD.Instance.AddPopupMessage("NoHRWarning".Loc(new object[]
							{
								self.Team
							}), "Exclamation", PopupManager.PopUpAction.None, 0u, PopupManager.NotificationSound.Warning, 2f, PopupManager.PopupIDs.None, 6);
						}
						HUD.Instance.insuranceWindow.AddRetire();
						float benefitValue2 = self.GetBenefitValue("Health insurance");
						GameSettings.Instance.MyCompany.MakeTransaction(-benefitValue2, Company.TransactionCategory.Benefits, "Health insurance");
						HUD.Instance.insuranceWindow.Terminations.Items.Add(new EmployeeTermination(self, EmployeeTermination.TerminationType.Hospitalized, benefitValue2));
						self.Dismiss();
					}
				}
			}
			float mean = self.employee.Diligence.MapRange(-1f, 1f, 1f, 0f, false);
			GameSettings.Instance.sActorManager.AddToAwaiting(self, new SDateTime(Utilities.GaussRange(mean, 0, 60, 0.2f), self.SpawnTime, d.Day, d.Month, d.Year), false, true);
		}
		self.OnDespawn();
		return 2;
	}

	private static int StandAround(Actor self)
	{
		throw new UnityException("StandAround state, should be deprecated");
	}

	private static int Work(Actor self)
	{
		if (self.UsingPoint == null || !self.UsingPoint.Parent.Type.Equals("Computer") || self.UsingPoint.Parent.Broken() || (self.UsingPoint.Parent.OwnedBy != null && self.UsingPoint.Parent.OwnedBy != self))
		{
			return 0;
		}
		self.Noisiness = ((!self.IsWorking) ? 3f : 0.5f);
		self.ShouldWork = true;
		if (self.IsWorking)
		{
			if (!self.AudioComp.isPlaying && self.MayPlaySound())
			{
				self.AudioComp.clip = self.KeyboardSFX.GetRandom<AudioClip>();
				self.AudioComp.Play();
			}
			self.WorkBoost();
		}
		self.anim.SetInteger("AnimControl", (!self.IsWorking) ? ((!self.UsingPoint.Parent.CanLean) ? 12 : 4) : ((self.Effectiveness <= 2f) ? 3 : 13));
		self.UsingPoint.Parent.IsOn = true;
		return self.WaitForTimer((float)UnityEngine.Random.Range(1, 10));
	}

	private static int IsHungry(Actor self)
	{
		return ((double)self.employee.Hunger >= 0.35) ? 0 : 2;
	}

	private static int GoToServingTray(Actor self)
	{
		int num = self.GoToFurniture("Tray", "Use", -1, false, null, false, false, null);
		if (num == 1)
		{
			self.MakeUnIdle();
			if (self.UsingPoint.Parent.HasHoldables == 0)
			{
				self.UsingPoint = null;
				num = self.GoToFurniture("Tray", "Use", -1, false, null, false, false, null);
			}
		}
		if (num == 2)
		{
			self.TurnToFurniture();
			self.FreeLoiterTable();
		}
		return num;
	}

	private static int TakeFoodFromTray(Actor self)
	{
		self.anim.SetInteger("AnimControl", 0);
		if (self.UsingPoint == null || self.UsingPoint.Parent.HasHoldables == 0)
		{
			return 0;
		}
		InteractionPoint usingPoint = self.UsingPoint;
		TableScript tableScript = self.FindFreeTable(Room.RoomLimits.Canteen, true, true, false);
		if (tableScript != null)
		{
			Furniture freeChair = tableScript.GetFreeChair(self, true);
			if (freeChair != null)
			{
				InteractionPoint interactionPoint = freeChair.GetInteractionPoint(self, "Use");
				if (interactionPoint != null && self.PathToFurniture(interactionPoint, false))
				{
					self.Food = usingPoint.Parent.TakeHoldable();
					if (self.Food == null)
					{
						return 0;
					}
					if (self.GetBenefitValue("Free food") < 0.5f)
					{
						GameSettings.Instance.MyCompany.MakeTransaction(10f, Company.TransactionCategory.Bills, "Food");
					}
					self.ReTakeItemAnyHand(self.Food);
					tableScript.ReserveTables(true, true);
					self.LoiterTable = tableScript;
					return 2;
				}
			}
		}
		self.UsingPoint = null;
		return 0;
	}

	private static int GoToEat(Actor self)
	{
		if (self.UsingPoint == null)
		{
			self.FreeLoiterTable();
			return 0;
		}
		if (self.WalkPath())
		{
			if (self.UsingPoint.Parent.SnappedTo != null)
			{
				SnapPoint snapPoint = self.UsingPoint.Parent.SnappedTo.Links.FirstOrDefault<SnapPoint>();
				if (snapPoint != null && self.UsingPoint.Parent.SnappedTo.Parent.Table.PlaceHoldable(self.Food, snapPoint))
				{
					self.LeaveItem(self.Food, false);
				}
			}
			else if (self.UsingPoint.Parent.Table.PlaceHoldable(self.Food, self.UsingPoint.Parent.SnapPoints[Array.IndexOf<InteractionPoint>(self.UsingPoint.Parent.InteractionPoints, self.UsingPoint)]))
			{
				self.LeaveItem(self.Food, false);
			}
			self.TurnToFurniture();
			self.employee.HadProperFood = true;
			return 2;
		}
		return 1;
	}

	private static int EatRealFood(Actor self)
	{
		if (self.UsingPoint == null || self.UsingPoint.Parent == null || self.GoHomeNow)
		{
			if (self.Food != null)
			{
				self.Food.DestroyMe();
				self.Food = null;
				self.FreeLoiterTable();
			}
			return 2;
		}
		self.anim.SetInteger("AnimControl", 4);
		bool flag = false;
		IEnumerable<Furniture> enumerable = (!(self.UsingPoint.Parent.SnappedTo != null)) ? self.UsingPoint.Parent.Table.GetFreeChairs() : self.UsingPoint.Parent.SnappedTo.Parent.Table.GetFreeChairs();
		foreach (Furniture furniture in enumerable)
		{
			for (int i = 0; i < furniture.InteractionPoints.Length; i++)
			{
				InteractionPoint interactionPoint = furniture.InteractionPoints[i];
				if (interactionPoint.UsedBy != null && interactionPoint.UsedBy != self)
				{
					flag = true;
					break;
				}
			}
		}
		self.StressFactor = -4f;
		if (flag)
		{
			self.SocialFactor = -32f;
		}
		self.employee.Hunger = Mathf.Clamp01(self.employee.Hunger + Time.deltaTime * GameSettings.GameSpeed / 45f);
		int num = (self.employee.Hunger != 1f) ? 1 : 2;
		if (num == 2 && self.Food != null)
		{
			self.Food.DestroyMe();
			self.Food = null;
		}
		return num;
	}

	private static int GoToFridge(Actor self)
	{
		if (self.QueuedFor("Tray"))
		{
			return 0;
		}
		int num = self.GoToFurniture("FastFood", "Use", -1, false, null, false, false, null);
		if (num == 1)
		{
			self.MakeUnIdle();
		}
		if (num == 2)
		{
			if (self.UsingPoint == null)
			{
				return 0;
			}
			self.FreeLoiterTable();
			self.anim.SetInteger("AnimControl", 6);
			self.TurnToFurniture();
		}
		return num;
	}

	private static int WantCoffee(Actor self)
	{
		if (self.coffee != null && self.coffee.Life <= 0f)
		{
			self.LeaveItem(self.coffee.GetComponent<Holdable>(), true);
		}
		if (self.Holding[0] != null && self.Holding[1] != null)
		{
			return 0;
		}
		if (Mathf.Approximately(self.employee.CoffeeQual, 0f))
		{
			return (self.GoToFurniture("Coffee", "Use", 3, false, null, false, false, null) <= 0) ? 0 : 2;
		}
		return 0;
	}

	private static int GoToCoffee(Actor self)
	{
		int num = self.GoToFurniture("Coffee", "Use", 3, false, null, false, false, null);
		if (num > 0)
		{
			self.MakeUnIdle();
		}
		if (num != 0 && self.coffee != null)
		{
			self.ReTakeItem(self.coffee.GetComponent<Holdable>(), true);
		}
		if (num == 2)
		{
			self.FreeLoiterTable();
			self.TurnToFurniture();
			if (self.coffee == null && self.UsingPoint != null && self.UsingPoint.Parent != null)
			{
				self.UsingPoint.Parent.IsOn = true;
			}
		}
		return num;
	}

	private static int MakeCoffee(Actor self)
	{
		if (self.coffee != null)
		{
			Holdable component = self.coffee.GetComponent<Holdable>();
			self.LeaveItem(component, true);
			return 2;
		}
		if (self.UsingPoint == null)
		{
			return 2;
		}
		self.anim.SetInteger("AnimControl", 6);
		int num = self.WaitForTimer(self.UsingPoint.Parent.Wait);
		if (num == 2)
		{
			Holdable itemAnyHand = self.GetItemAnyHand("CoffeeCup");
			if (itemAnyHand != null)
			{
				if (self.GetBenefitValue("Free food") > 0.5f)
				{
					GameSettings.Instance.MyCompany.MakeTransaction(-2f, Company.TransactionCategory.Bills, "Food");
				}
				self.coffee = itemAnyHand.GetComponent<CoffeeScript>();
				float quality = self.UsingPoint.Parent.upg.Quality;
				self.employee.CoffeeQual = self.UsingPoint.Parent.Coffee * quality;
				if (quality < 0.5f)
				{
					self.employee.AddInstantMood("BadCoffee", self, (1f - quality * 2f) * (1f / self.UsingPoint.Parent.Coffee));
				}
				else
				{
					self.employee.AddInstantMood("GoodCoffee", self, (quality - 0.5f) * 2f * self.UsingPoint.Parent.Coffee);
				}
			}
			self.UsingPoint.Parent.IsOn = false;
			self.UsingPoint.UsedBy = null;
			self.UsingPoint = null;
		}
		return num;
	}

	private static int GetFood(Actor self)
	{
		int num = self.WaitForTimer(2f);
		if (num == 2)
		{
			if (self.GetBenefitValue("Free food") > 0.5f)
			{
				GameSettings.Instance.MyCompany.MakeTransaction(-5f, Company.TransactionCategory.Bills, "Food");
			}
			self.anim.SetInteger("AnimControl", 0);
			self.employee.Hunger = 1f;
			if (self.UsingPoint != null)
			{
				self.UsingPoint.UsedBy = null;
				self.UsingPoint = null;
			}
		}
		return num;
	}

	private static int HasMakeMeeting(Actor self)
	{
		if (self.Team == null)
		{
			return 0;
		}
		if (self.employee.IsRole(Employee.RoleBit.Lead))
		{
			if ((SDateTime.Now() - self.LastMeeting).ToInt() >= 1440)
			{
				TableScript tableScript = self.FindFreeTable(Room.RoomLimits.Meeting, true, false, true);
				if (tableScript != null)
				{
					Furniture freeChair = tableScript.GetFreeChair(self, false);
					if (freeChair != null)
					{
						InteractionPoint interactionPoint = freeChair.GetInteractionPoint(self, "Use");
						if (interactionPoint != null)
						{
							Team team = self.GetTeam();
							team.MeetingTable = tableScript;
							List<Actor> list = self.CallForMeeting();
							if (list.Count > 0)
							{
								self.ShutdownPC();
								if (self.PathToFurniture(interactionPoint, false))
								{
									self.UsingPoint = interactionPoint;
									self.AtFurniture = false;
									team.MeetingTable.ReserveTables(true, false);
									self.Timer = 60f;
									team.Talking = self;
									team.Meeting.AddRange(list);
									team.Meeting.Add(self);
									return 2;
								}
							}
							team.MeetingTable = null;
						}
					}
					return 0;
				}
			}
		}
		else if (self.GetTeam().Meeting.Contains(self) && self.GetTeam().MeetingTable != null)
		{
			Furniture freeChair2 = self.GetTeam().MeetingTable.GetFreeChair(self, false);
			if (freeChair2 != null)
			{
				InteractionPoint interactionPoint2 = freeChair2.GetInteractionPoint(self, "Use");
				if (interactionPoint2 != null)
				{
					self.ShutdownPC();
					bool flag = self.PathToFurniture(interactionPoint2, false);
					if (flag)
					{
						self.UsingPoint = interactionPoint2;
						self.AtFurniture = false;
					}
					return (!flag) ? 0 : 2;
				}
			}
		}
		return 0;
	}

	private static int GoToMeeting(Actor self)
	{
		int num = self.GoToFurniture("Chair", "Use", -1, false, null, false, false, null);
		if (num > 0)
		{
			self.MakeUnIdle();
		}
		if (num == 2)
		{
			self.TurnToFurniture();
		}
		return num;
	}

	private static int HaveMeeting(Actor self)
	{
		if (self.Team == null)
		{
			return 2;
		}
		self.SocialFactor = -16f * Mathf.Max(0f, self.TeamCompatibility);
		self.Noisiness = (float)((!self.IsTalking) ? 3 : 6);
		self.anim.SetInteger("AnimControl", (!(self.GetTeam().Talking == self)) ? 4 : 5);
		Team team = self.GetTeam();
		if (team.Talking == self && !self.AudioComp.isPlaying)
		{
			if (self.IsTalking)
			{
				if (team.MeetingTable != null)
				{
					team.Talking = (from x in team.Meeting
					where x.AIScript.currentNode.Name.Equals("HaveMeeting")
					select x).GetRandom<Actor>();
				}
				self.IsTalking = false;
			}
			else if (self.MayPlaySound())
			{
				self.AudioComp.clip = ((!self.Female) ? self.TalkSFX.GetRandom<AudioClip>() : self.FemaleTalkSFX.GetRandom<AudioClip>());
				self.AudioComp.Play();
				self.IsTalking = true;
			}
		}
		if (self.employee.IsRole(Employee.RoleBit.Lead))
		{
			if (team.MeetingTable == null)
			{
				team.Meeting.Clear();
				self.LastMeeting = SDateTime.Now();
				return 2;
			}
			self.Timer -= Time.deltaTime * GameSettings.GameSpeed;
			if (self.Timer <= 0f)
			{
				team.MeetingTable.ReserveTables(false, false);
				team.MeetingTable = null;
				team.Meeting.Clear();
				self.LastMeeting = SDateTime.Now();
				return 2;
			}
			self.MeetingBoost();
			return 1;
		}
		else
		{
			if (team.MeetingTable != null && (team.Leader == null || !team.Leader.isActiveAndEnabled))
			{
				team.MeetingTable.ReserveTables(false, false);
				team.MeetingTable = null;
				team.Meeting.Clear();
			}
			if (team.MeetingTable != null)
			{
				self.MeetingBoost();
				return 1;
			}
			self.LastMeeting = SDateTime.Now();
			return 2;
		}
	}

	private static int HasToPee(Actor self)
	{
		return ((double)self.employee.Bladder >= 0.2) ? 0 : 2;
	}

	private static int GoToToilet(Actor self)
	{
		int num = self.GoToFurniture("Toilet", "Use", -1, false, null, false, false, null);
		if (num == 2)
		{
			self.FreeLoiterTable();
			if (self.UsingPoint == null)
			{
				return 0;
			}
			self.anim.SetInteger("AnimControl", 12);
			self.TurnToFurniture();
			self.UsingPoint.Parent.IsOn = true;
			self.UsingPoint.Parent.InteractStart();
		}
		if (num == 1)
		{
			self.MakeUnIdle();
		}
		return num;
	}

	private static int DoBusiness(Actor self)
	{
		self.CensorCube.SetActive(true);
		int num = self.WaitForTimer((float)UnityEngine.Random.Range(2, 4));
		if (num == 2)
		{
			self.CensorCube.SetActive(false);
			self.UsingPoint.Parent.IsOn = false;
			float quality = self.UsingPoint.Parent.upg.Quality;
			self.employee.Bladder = 1f * quality;
			if (quality < 0.5f)
			{
				self.employee.AddMood("DisgustingToilet", self, Time.deltaTime, 1f - quality, true, true);
			}
			if (!self.currentRoom.IsPrivate || self.currentRoom.Occupants.Count > 1)
			{
				self.employee.AddMood("NoToiletPrivacy", self);
			}
			self.UsingPoint.Parent.InteractEnd();
			self.UsingPoint.UsedBy = null;
			self.UsingPoint = null;
		}
		return num;
	}

	private static int Loiter(Actor self)
	{
		return self.HandleLoiter(true);
	}

	private static int NeedsSocial(Actor self)
	{
		if ((self.employee.Social < 0.5f || self.employee.Stress == 0f) && (self.Holding[0] == null || "WaterCup".Equals(self.Holding[0].name)))
		{
			InteractionPoint usingPoint = self.UsingPoint;
			if (self.GoToFurniture("Watercooler", "Use", 3, false, null, false, false, null) > 0)
			{
				if (self.ShouldTrySocial())
				{
					self.LastSocial = SDateTime.Now();
					return 2;
				}
				bool flag = false;
				InteractionPoint[] interactionPoints = self.UsingPoint.Parent.InteractionPoints;
				for (int i = 0; i < interactionPoints.Length; i++)
				{
					if (interactionPoints[i].UsedBy != null && interactionPoints[i].UsedBy != self)
					{
						flag = true;
						break;
					}
				}
				if (flag)
				{
					return 2;
				}
				self.UsingPoint = usingPoint;
			}
			self.LastSocial = SDateTime.Now();
		}
		return 0;
	}

	private static int GoToSocial(Actor self)
	{
		int num = self.GoToFurniture("Watercooler", "Use", 3, false, null, false, false, null);
		if (num > 0)
		{
			self.MakeUnIdle();
		}
		if (num == 2)
		{
			self.FreeLoiterTable();
			self.anim.SetInteger("AnimControl", 14);
			self.TurnToFurniture();
		}
		return num;
	}

	private static int BeSocial(Actor self)
	{
		if (self.GoHomeNow)
		{
			if (self.Holding[0] != null)
			{
				self.LeaveItem(self.Holding[0], true);
			}
			self.IsTalking = false;
			return 2;
		}
		if (self.Holding[0] == null || self.anim.GetCurrentAnimatorStateInfo(0).IsName("Take water"))
		{
			return 1;
		}
		if (self.UsingPoint == null || (self.Team != null && self.GetTeam().MeetingTable != null && self.GetTeam().Meeting.Contains(self)))
		{
			if (self.Holding[0] != null)
			{
				self.LeaveItem(self.Holding[0], true);
			}
			self.IsTalking = false;
			return 2;
		}
		if (self.anim.GetInteger("AnimControl") == 15)
		{
			self.anim.SetInteger("AnimControl", 14);
		}
		if (self.anim.GetInteger("AnimControl") == 14 && UnityEngine.Random.value < 0.1f * Time.deltaTime * GameSettings.GameSpeed)
		{
			self.anim.SetInteger("AnimControl", 15);
		}
		bool flag = false;
		bool flag2 = self.IsTalking;
		if (self.IsTalking && !self.AudioComp.isPlaying)
		{
			self.anim.SetInteger("AnimControl", 14);
			self.IsTalking = false;
		}
		InteractionPoint[] interactionPoints = self.UsingPoint.Parent.InteractionPoints;
		for (int i = 0; i < interactionPoints.Length; i++)
		{
			if (interactionPoints[i].UsedBy != null && interactionPoints[i].UsedBy != self && interactionPoints[i].UsedBy.AIScript.currentNode.Name.Equals("BeSocial"))
			{
				flag2 |= interactionPoints[i].UsedBy.IsTalking;
				flag = true;
				break;
			}
		}
		if (flag)
		{
			if (!flag2 && self.MayPlaySound())
			{
				self.IsTalking = true;
				self.anim.SetInteger("AnimControl", 16);
				self.AudioComp.clip = ((!self.Female) ? self.TalkSFX.GetRandom<AudioClip>() : self.FemaleTalkSFX.GetRandom<AudioClip>());
				self.AudioComp.Play();
			}
			self.Noisiness = (float)((!self.IsTalking) ? 3 : 6);
			self.SocialFactor = -32f;
			if (self.employee.Social < 1f)
			{
				return 1;
			}
		}
		else if (self.IsTalking)
		{
			self.IsTalking = false;
			self.anim.SetInteger("AnimControl", 14);
			self.AudioComp.Stop();
		}
		if (self.employee.Stress < 0.2f)
		{
			return 1;
		}
		int num = self.WaitForTimer(self.UsingPoint.Parent.Wait);
		if (num == 2)
		{
			if (self.Holding[0] != null)
			{
				self.LeaveItem(self.Holding[0], true);
			}
			self.IsTalking = false;
		}
		return num;
	}

	private static int NeedsSocialLeader(Actor self)
	{
		if (self.employee.IsRole(Employee.RoleBit.Lead))
		{
			float num = self.employee.Leadership.MapRange(-1f, 1f, 0.25f, 0.5f, false);
			if (self.employee.Social < num)
			{
				return 2;
			}
			List<Actor> employeesDirect = self.GetTeam().GetEmployeesDirect();
			for (int i = 0; i < employeesDirect.Count; i++)
			{
				Actor actor = employeesDirect[i];
				if (!(actor == self) && actor.gameObject.activeSelf && !(actor.UsingPoint == null) && actor.UsingPoint.Parent.Type.Equals("Computer"))
				{
					if (actor.employee.Social < num)
					{
						return 2;
					}
				}
			}
		}
		return 0;
	}

	private static int GoToSocialLeader(Actor self)
	{
		if (!(self.UsingPoint != null) || !self.UsingPoint.Name.Equals("Social") || self.CurrentPath == null)
		{
			if (self.employee.IsRole(Employee.RoleBit.Lead))
			{
				self.Noisiness = 5f;
				Team team = self.GetTeam();
				List<Actor> employeesDirect = team.GetEmployeesDirect();
				foreach (Actor actor in from x in employeesDirect
				orderby x.employee.Social
				select x)
				{
					if (!(actor == self))
					{
						if (actor.gameObject.activeSelf && actor.UsingPoint != null && actor.UsingPoint.Parent.Type.Equals("Computer"))
						{
							InteractionPoint interactionPoint = actor.UsingPoint.Parent.GetInteractionPoint(self, "Social");
							if (interactionPoint != null && self.PathToFurniture(interactionPoint, false))
							{
								self.MakeUnIdle();
								return 1;
							}
						}
					}
				}
				return 0;
			}
			return 0;
		}
		if (self.WalkPath())
		{
			self.FreeLoiterTable();
			self.TurnToFurniture();
			return 2;
		}
		self.MakeUnIdle();
		return 1;
	}

	private static int BeSocialLeader(Actor self)
	{
		if (self.UsingPoint == null || self.GoHomeNow || !self.employee.IsRole(Employee.RoleBit.Lead))
		{
			self.UsingPoint = null;
			return 2;
		}
		Actor usedBy = self.UsingPoint.Parent.GetInteractionPoint("Use", true).UsedBy;
		if (usedBy == null)
		{
			self.UsingPoint = null;
			return 2;
		}
		self.anim.SetInteger("AnimControl", 17);
		float num = self.employee.Leadership.MapRange(-1f, 1f, 0.5f, 1f, false);
		self.SocialFactor = -32f * num;
		usedBy.SocialFactor = -32f * num;
		if (self.employee.Social < num || usedBy.employee.Social < num)
		{
			return 1;
		}
		self.UsingPoint = null;
		return 2;
	}

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache2;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache3;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache4;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache5;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache6;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache7;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache8;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache9;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheA;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheB;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheC;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheD;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheE;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cacheF;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache10;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache11;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache12;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache13;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache14;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache15;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache16;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache17;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache18;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache19;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1A;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1B;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1C;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1D;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1E;

	[CompilerGenerated]
	private static Func<Actor, int> <>f__mg$cache1F;
}
