﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GUILegend : MonoBehaviour
{
	private void Start()
	{
		this.Items.OnChange = new Action(this.UpdateItems);
	}

	public bool IsOn(int i)
	{
		return i < this.GUIItems.Count && this.GUIItems[i].Value.GetComponent<Toggle>().isOn;
	}

	private void OnToggleWrap()
	{
		if (this.OnToggle != null)
		{
			this.OnToggle();
		}
	}

	public void UpdateItems()
	{
		using (List<KeyValuePair<string, GameObject>>.Enumerator enumerator = (from x in this.GUIItems
		where !this.Items.Contains(x.Key)
		select x).ToList<KeyValuePair<string, GameObject>>().GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				KeyValuePair<string, GameObject> item = enumerator.Current;
				this.GUIItems.RemoveAll((KeyValuePair<string, GameObject> x) => x.Key.Equals(item.Key));
				UnityEngine.Object.Destroy(item.Value.gameObject);
			}
		}
		int num = this.GUIItems.Count;
		foreach (string text in from x in this.Items
		where !this.GUIItems.Any((KeyValuePair<string, GameObject> y) => y.Key.Equals(x))
		select x)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ItemPrefab);
			gameObject.GetComponentsInChildren<Image>().FirstOrDefault((Image x) => x.name.Equals("Background")).color = this.Colors[num % this.Colors.Count];
			gameObject.GetComponentInChildren<Text>().text = ((!this.Sheet) ? text : ("Sheet" + text).LocDef(text.Loc()));
			Toggle component = gameObject.GetComponent<Toggle>();
			if (!this.Toggleable)
			{
				component.isOn = false;
				component.interactable = false;
			}
			else
			{
				component.onValueChanged.AddListener(delegate(bool x)
				{
					this.OnToggleWrap();
				});
			}
			gameObject.transform.SetParent(base.transform, false);
			this.GUIItems.Add(new KeyValuePair<string, GameObject>(text, gameObject));
			num++;
		}
		if (this.OnToggle != null)
		{
			this.OnToggle();
		}
	}

	public EventList<string> Items = new EventList<string>();

	public List<Color> Colors = new List<Color>();

	public GameObject ItemPrefab;

	public bool Toggleable;

	[NonSerialized]
	public bool Sheet;

	public Action OnToggle;

	[NonSerialized]
	public List<KeyValuePair<string, GameObject>> GUIItems = new List<KeyValuePair<string, GameObject>>();
}
