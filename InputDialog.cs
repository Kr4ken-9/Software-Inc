﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InputDialog : MonoBehaviour
{
	public void Init(string prompt, string title, string defaultText, Action<string> onFinish, Action onCancel = null)
	{
		this.Window.NonLocTitle = title;
		this.Prompt.text = prompt;
		this.OnFinish = onFinish;
		this.OnCancel = onCancel;
		this.InputBox.text = defaultText;
		this.InputBox.Select();
		this.InputBox.caretPosition = 0;
		this.InputBox.MoveTextEnd(true);
		if (GameSettings.Instance != null)
		{
			GameSettings.ForcePause = true;
		}
	}

	public void CloseNow(bool ok)
	{
		if (ok)
		{
			this.OnFinish(this.InputBox.text);
		}
		else if (this.OnCancel != null)
		{
			this.OnCancel();
		}
		this.OnCancel = null;
		if (GameSettings.Instance != null)
		{
			GameSettings.ForcePause = false;
		}
		this.Window.Close();
	}

	public void OnEndEdit()
	{
		if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
		{
			InputController.InputEnabled = true;
			this.CloseNow(true);
		}
	}

	public GUIWindow Window;

	public Text Prompt;

	public InputField InputBox;

	private Action<string> OnFinish;

	private Action OnCancel;
}
