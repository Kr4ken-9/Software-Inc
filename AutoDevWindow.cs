﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AutoDevWindow : MonoBehaviour
{
	public AutoDevWorkItem Work
	{
		get
		{
			return this._work;
		}
	}

	public void Show(AutoDevWorkItem item)
	{
		if (this._work != null)
		{
			this._work.Items.OnChange = null;
		}
		this._work = item;
		this.Window.Show();
		this.PlanPanel.SetActive(true);
		this.PlanList.Items = this._work.Items.OfType<object>().ToList<object>();
		this._work.Items.OnChange = delegate
		{
			this.PlanList.Items = this._work.Items.OfType<object>().ToList<object>();
		};
		this.OKButton.SetActive(false);
		this.ClearFields();
	}

	private void Update()
	{
		if (this._work != null)
		{
			float num = this._work.MaxActions();
			if (num > 0f)
			{
				this.ActionPanel.SetActive(true);
				this.DesignAction[1] = num;
				this.DesignAction[2] = this._work.DesignActions;
				this.DesignAction[3] = Mathf.Floor(this._work.DesignActions);
				this.DevAction[1] = num;
				this.DevAction[2] = this._work.DevActions;
				this.DevAction[3] = Mathf.Floor(this._work.DevActions);
				this.MarketAction[1] = num;
				this.MarketAction[2] = this._work.MarketingActions;
				this.MarketAction[3] = Mathf.Floor(this._work.MarketingActions);
			}
			else
			{
				this.ActionPanel.SetActive(false);
			}
		}
	}

	public void Toggle()
	{
		this.Window.Toggle(false);
		this._work = null;
		this.PlanPanel.SetActive(false);
		this.OKButton.SetActive(true);
		this.ActionPanel.SetActive(false);
		this.ClearFields();
		if (this.Window.Shown)
		{
			TutorialSystem.Instance.StartTutorial("Project management", false);
		}
	}

	private void Start()
	{
		this.Window.OnClose = delegate
		{
			if (this._work != null)
			{
				this._work.Items.OnChange = null;
			}
		};
	}

	private void SetLeader(Actor actor)
	{
		if (this._work != null)
		{
			if (!this._initializing)
			{
				this._work.Leader = actor;
			}
		}
		else
		{
			this._leaderPick = actor;
		}
		this.LeaderLabel.text = ((!(actor == null)) ? actor.employee.FullName : "None".Loc());
	}

	public void TeamButtonClick(int i)
	{
		HashSet<string> team = null;
		Text label = null;
		switch (i)
		{
		case 0:
			team = ((this._work == null) ? this._designPick : this._work.DesignTeams);
			label = this.DesignLabel;
			break;
		case 1:
			team = ((this._work == null) ? this._devPick : this._work.SDevTeams);
			label = this.DevLabel;
			break;
		case 2:
			team = ((this._work == null) ? this._supportPick : this._work.SupportTeams);
			label = this.SupportLabel;
			break;
		case 3:
			team = ((this._work == null) ? this._prePick : this._work.MarketingTeams);
			label = this.PreLabel;
			break;
		case 4:
			team = ((this._work == null) ? this._postPick : this._work.PostMarketingTeams);
			label = this.PostLabel;
			break;
		case 5:
			team = ((this._work == null) ? this._dev2Pick : this._work.SecondaryDevTeams);
			label = this.Dev2Label;
			break;
		}
		if (team != null && label != null)
		{
			HUD.Instance.TeamSelectWindow.Show(false, team, delegate(string[] x)
			{
				this.SetTeam(team, label, x.ToHashSet<string>());
			}, null);
		}
	}

	public void PickLeader()
	{
		Actor[] leaders = (from x in GameSettings.Instance.sActorManager.Actors
		where x.employee.IsRole(Employee.RoleBit.Lead)
		select x).ToArray<Actor>();
		if (leaders.Length > 0)
		{
			SelectorController.Instance.selectWindow.Show("Leader", from x in leaders
			select x.employee.FullName, delegate(int x)
			{
				this.SetLeader(leaders[x]);
			}, false, true, true, false, null);
		}
		else
		{
			WindowManager.Instance.ShowMessageBox("AutoDevNoLeaders".Loc(), false, DialogWindow.DialogType.Error);
		}
	}

	public void ServerButtonClick(int i)
	{
		if (GameSettings.Instance.GetAllServersQuick().Count > 0)
		{
			string[] servs = GameSettings.Instance.GetAllServersQuick().Keys.ToArray<string>();
			if (this._work != null)
			{
				if (i != 0)
				{
					if (i == 1)
					{
						SelectorController.Instance.selectWindow.Show("Server", servs, delegate(int x)
						{
							this.SetServer(ref this._work.MainServer, this.ServerLabel, (x <= -1) ? null : servs[x]);
						}, true, true, true, false, null);
					}
				}
				else
				{
					SelectorController.Instance.selectWindow.Show("Server", servs, delegate(int x)
					{
						this.SetServer(ref this._work.SCMServer, this.SCMLabel, (x <= -1) ? null : servs[x]);
					}, true, true, true, false, null);
				}
			}
			else if (i != 0)
			{
				if (i == 1)
				{
					SelectorController.Instance.selectWindow.Show("Server", servs, delegate(int x)
					{
						this.SetServer(ref this._server, this.ServerLabel, (x <= -1) ? null : servs[x]);
					}, true, true, true, false, null);
				}
			}
			else
			{
				SelectorController.Instance.selectWindow.Show("Server", servs, delegate(int x)
				{
					this.SetServer(ref this._SCM, this.SCMLabel, (x <= -1) ? null : servs[x]);
				}, true, true, true, false, null);
			}
		}
	}

	public void ToggleSet(int i)
	{
		if (this._work != null)
		{
			switch (i)
			{
			case 0:
				this._work.Hype = this.HypeToggle.isOn;
				break;
			case 1:
				this._work.AutoProject = this.AutoToggle.isOn;
				break;
			case 2:
				this._work.OnlySequels = this.SequelToggle.isOn;
				break;
			case 3:
				this._work.PostMarketing = this.PostToggle.isOn;
				break;
			case 4:
				this._work.HandleMarketing = this.MarketToggle.isOn;
				break;
			case 5:
				this._work.UseOwnLicenses = this.LicenseToggle.isOn;
				break;
			case 6:
				this._work.SingleIP = this.IPToggle.isOn;
				break;
			case 7:
				this._work.AutoSupport = this.SupportToggle.isOn;
				break;
			}
		}
	}

	private string GetTeam(HashSet<string> values)
	{
		if (values.Count > 1)
		{
			return values.First<string>() + "+" + (values.Count - 1);
		}
		if (values.Count > 0)
		{
			return values.First<string>();
		}
		return "None".Loc();
	}

	private void SetTeam(HashSet<string> teams, Text label, HashSet<string> value)
	{
		if (this._work != null)
		{
			if (!this._initializing)
			{
				teams.Clear();
				teams.AddRange(value);
				this._work.RefreshTeams();
			}
		}
		else
		{
			teams.Clear();
			teams.AddRange(value);
		}
		label.text = this.GetTeam(value);
	}

	public void UpdateDevTime()
	{
		this.DevTimeLabel.text = Mathf.RoundToInt(this.DevTimeSlider.value * 10f) + "%";
		if (this._work != null && !this._initializing)
		{
			this._work.DevTimeMultiplier = this.DevTimeSlider.value / 10f;
		}
	}

	private void SetServer(ref string server, Text label, string value)
	{
		if (this._work != null)
		{
			if (!this._initializing)
			{
				server = value;
			}
		}
		else
		{
			server = value;
		}
		label.text = (value ?? "None".Loc());
	}

	public void NameChange()
	{
		if (this._work != null && !this._initializing)
		{
			this._work.Name = this.ProjectName.text;
		}
	}

	public void PhysicalChange(bool end)
	{
		if (this.DisablePh || this._initializing)
		{
			this.DisablePh = false;
			return;
		}
		try
		{
			bool flag = false;
			string text = this.PhysicalCopies.text;
			if (text.EndsWith("%"))
			{
				flag = true;
				text = text.Substring(0, text.Length - 1).Trim();
			}
			uint physicalCopies = Convert.ToUInt32(text.Replace(",", string.Empty));
			if (this._work != null)
			{
				this._work.PhysicalCopies = physicalCopies;
				this._work.PhysicalCopyRel = flag;
			}
			if (end)
			{
				this.DisablePh = true;
				this.PhysicalCopies.text = physicalCopies.ToString("N0") + ((!flag) ? string.Empty : "%");
				this.DisablePh = false;
			}
		}
		catch (Exception)
		{
		}
	}

	public void PrintChange(bool end)
	{
		if (this.DisablePr || this._initializing)
		{
			this.DisablePr = false;
			return;
		}
		try
		{
			bool flag = false;
			string text = this.PrintingCopies.text;
			if (text.EndsWith("%"))
			{
				flag = true;
				text = text.Substring(0, text.Length - 1).Trim();
			}
			uint printingCopies = Convert.ToUInt32(text.Replace(",", string.Empty));
			if (this._work != null)
			{
				this._work.PrintingCopies = printingCopies;
				this._work.PrintingCopyRel = flag;
			}
			if (end)
			{
				this.DisablePr = true;
				this.PrintingCopies.text = printingCopies.ToString("N0") + ((!flag) ? string.Empty : "%");
				this.DisablePr = false;
			}
		}
		catch (Exception)
		{
		}
	}

	private void ClearFields()
	{
		this._initializing = true;
		if (this._work != null)
		{
			this.ProjectName.text = this._work.Name;
			this.SetLeader(this._work.Leader);
			this.SetTeam(null, this.DesignLabel, this._work.DesignTeams);
			this.SetTeam(null, this.DevLabel, this._work.SDevTeams);
			this.SetTeam(null, this.Dev2Label, this._work.SecondaryDevTeams);
			this.SetTeam(null, this.SupportLabel, this._work.SupportTeams);
			this.SetTeam(null, this.PreLabel, this._work.MarketingTeams);
			this.SetTeam(null, this.PostLabel, this._work.PostMarketingTeams);
			this.DevTimeSlider.value = Mathf.Round(this._work.DevTimeMultiplier * 10f);
			this.HypeToggle.isOn = this._work.Hype;
			this.AutoToggle.isOn = this._work.AutoProject;
			this.SequelToggle.isOn = this._work.OnlySequels;
			this.PostToggle.isOn = this._work.PostMarketing;
			this.MarketToggle.isOn = this._work.HandleMarketing;
			this.LicenseToggle.isOn = this._work.UseOwnLicenses;
			this.IPToggle.isOn = this._work.SingleIP;
			this.SupportToggle.isOn = this._work.AutoSupport;
			this.PhysicalCopies.text = this._work.PhysicalCopies.ToString("N0") + ((!this._work.PhysicalCopyRel) ? string.Empty : "%");
			this.PrintingCopies.text = this._work.PrintingCopies.ToString("N0") + ((!this._work.PrintingCopyRel) ? string.Empty : "%");
			this.SetServer(ref this._work.MainServer, this.ServerLabel, this._work.MainServer);
			this.SetServer(ref this._work.SCMServer, this.SCMLabel, this._work.SCMServer);
		}
		else
		{
			this.ProjectName.text = "ProjectManagementDefaultName".Loc();
			this.SetLeader(null);
			HashSet<string> value = GameSettings.Instance.GetDefaultTeams("Design").ToHashSet<string>();
			this.SetTeam(this._designPick, this.DesignLabel, value);
			this.SetTeam(this._devPick, this.DevLabel, value);
			this.SetTeam(this._dev2Pick, this.Dev2Label, value);
			this.SetTeam(this._supportPick, this.SupportLabel, value);
			this.SetTeam(this._prePick, this.PreLabel, GameSettings.Instance.GetDefaultTeams("PressRelease").ToHashSet<string>());
			this.SetTeam(this._postPick, this.PostLabel, GameSettings.Instance.GetDefaultTeams("Marketing").ToHashSet<string>());
			this.DevTimeSlider.value = 10f;
			this.HypeToggle.isOn = true;
			this.AutoToggle.isOn = true;
			this.SequelToggle.isOn = true;
			this.PostToggle.isOn = true;
			this.MarketToggle.isOn = false;
			this.LicenseToggle.isOn = false;
			this.IPToggle.isOn = false;
			this.SupportToggle.isOn = true;
			this.PhysicalCopies.text = "0";
			this.PrintingCopies.text = "0";
			this.SetServer(ref this._server, this.ServerLabel, null);
			this.SetServer(ref this._SCM, this.SCMLabel, null);
		}
		this.UpdateDevTime();
		this._initializing = false;
	}

	public void OKClick()
	{
		if (this._leaderPick != null)
		{
			AutoDevWorkItem autoDevWorkItem = new AutoDevWorkItem(this.ProjectName.text, this._leaderPick);
			autoDevWorkItem.DesignTeams.AddRange(this._designPick);
			autoDevWorkItem.SDevTeams.AddRange(this._devPick);
			autoDevWorkItem.SecondaryDevTeams.AddRange(this._dev2Pick);
			autoDevWorkItem.SupportTeams.AddRange(this._supportPick);
			autoDevWorkItem.MarketingTeams.AddRange(this._prePick);
			autoDevWorkItem.PostMarketingTeams.AddRange(this._postPick);
			autoDevWorkItem.Hype = this.HypeToggle.isOn;
			autoDevWorkItem.AutoProject = this.AutoToggle.isOn;
			autoDevWorkItem.OnlySequels = this.SequelToggle.isOn;
			autoDevWorkItem.PostMarketing = this.PostToggle.isOn;
			autoDevWorkItem.HandleMarketing = this.MarketToggle.isOn;
			autoDevWorkItem.UseOwnLicenses = this.LicenseToggle.isOn;
			autoDevWorkItem.AutoSupport = this.SupportToggle.isOn;
			autoDevWorkItem.SingleIP = this.IPToggle.isOn;
			autoDevWorkItem.DevTimeMultiplier = this.DevTimeSlider.value / 10f;
			autoDevWorkItem.MainServer = this._server;
			autoDevWorkItem.SCMServer = this._SCM;
			this._work = autoDevWorkItem;
			this.PhysicalChange(false);
			this.PrintChange(false);
			GameSettings.Instance.MyCompany.WorkItems.Add(autoDevWorkItem);
			this.Window.Close();
		}
		else
		{
			WindowManager.Instance.ShowMessageBox("AutoDevMissingLeader".Loc(), false, DialogWindow.DialogType.Error);
		}
	}

	[NonSerialized]
	private Actor _leaderPick;

	[NonSerialized]
	private AutoDevWorkItem _work;

	[NonSerialized]
	private bool _initializing;

	[NonSerialized]
	private string _server;

	[NonSerialized]
	private string _SCM;

	[NonSerialized]
	private HashSet<string> _designPick = new SHashSet<string>();

	[NonSerialized]
	private HashSet<string> _devPick = new SHashSet<string>();

	[NonSerialized]
	private HashSet<string> _dev2Pick = new SHashSet<string>();

	[NonSerialized]
	private HashSet<string> _supportPick = new SHashSet<string>();

	[NonSerialized]
	private HashSet<string> _prePick = new SHashSet<string>();

	[NonSerialized]
	private HashSet<string> _postPick = new SHashSet<string>();

	public GUIWindow Window;

	public InputField ProjectName;

	public InputField PhysicalCopies;

	public InputField PrintingCopies;

	public Text LeaderLabel;

	public Text DesignLabel;

	public Text DevLabel;

	public Text Dev2Label;

	public Text SupportLabel;

	public Text PreLabel;

	public Text PostLabel;

	public Text DevTimeLabel;

	public Text SCMLabel;

	public Text ServerLabel;

	public Toggle HypeToggle;

	public Toggle AutoToggle;

	public Toggle SequelToggle;

	public Toggle PostToggle;

	public Toggle MarketToggle;

	public Toggle LicenseToggle;

	public Toggle IPToggle;

	public Toggle SupportToggle;

	public Slider DevTimeSlider;

	public GUIListView PlanList;

	public GameObject PlanPanel;

	public GameObject OKButton;

	public GameObject ActionPanel;

	public IconFillBar DesignAction;

	public IconFillBar DevAction;

	public IconFillBar MarketAction;

	private bool DisablePh;

	private bool DisablePr;
}
