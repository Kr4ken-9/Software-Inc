﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ListView<T>
{
	public ListView(Rect position, GUISkin guiskin)
	{
		this.Rect = position;
		this.GUIskin = guiskin;
		this.label = new GUIStyle(this.GUIskin.label);
	}

	public List<T> Items
	{
		get
		{
			return this._items;
		}
		set
		{
			this._items = value;
			this.sortBy = -1;
		}
	}

	public List<ListViewColumn<T>> Columns
	{
		get
		{
			return this._columns;
		}
		set
		{
			this._columns = value;
			this.sortBy = -1;
		}
	}

	public IEnumerable<T> SelectedItem
	{
		get
		{
			return from i in this.Selected
			select this.Items[i];
		}
	}

	public void Select(int i, bool clear = true)
	{
		if (clear || !this.MultiSelect)
		{
			this.Selected.Clear();
		}
		if (!this.Selected.Contains(i) && i >= 0 && i < this.Items.Count)
		{
			this.Selected.Add(i);
		}
	}

	public void ResetScroll()
	{
		this.scroll = 0f;
	}

	public void Select(T obj, bool clear = true)
	{
		int i = this.Items.IndexOf(obj);
		this.Select(i, clear);
	}

	public void Draw()
	{
		int num = Mathf.FloorToInt((this.Rect.height - 24f) / 24f);
		this.scroll = Mathf.Clamp(this.scroll, 0f, (float)Mathf.Max(0, this.Items.Count - num + 1));
		GUI.Box(new Rect(this.Rect.x, this.Rect.y + 25f, this.Rect.width - 1f, this.Rect.height - 25f), string.Empty, this.GUIskin.customStyles[2]);
		if (this.Items.Count == 0)
		{
			this.Selected.Clear();
		}
		bool useScroll = this.Rect.height - 24f < (float)(this.Items.Count * 24);
		float s = (from x in this.Columns
		where x.Visible
		select x).Sum((ListViewColumn<T> x) => x.Width);
		float[] array = (from x in this.Columns
		select x.Width * (this.Rect.width - (float)((!useScroll) ? 0 : 16)) / s).ToArray<float>();
		float num2 = 0f;
		for (int i = 0; i < this.Columns.Count; i++)
		{
			if (this.Columns[i].Visible)
			{
				if (this.Columns[i].IsAction)
				{
					GUI.Box(new Rect(this.Rect.x + num2, this.Rect.y, array[i], 24f), this.Columns[i].Name, this.GUIskin.box);
				}
				else if (GUI.Button(new Rect(this.Rect.x + num2, this.Rect.y, array[i], 24f), this.Columns[i].Name + ((this.sortBy != i) ? string.Empty : ((!this.sortAsc) ? "▼" : "▲")), this.GUIskin.button))
				{
					this.sortAsc = this.Columns[i].SortAsc;
					this.Columns[i].Sort(this.Items, this.Selected);
					this.sortBy = i;
				}
				num2 += array[i];
			}
		}
		if (this.Columns.Count == 0)
		{
			return;
		}
		for (int j = 0; j < num; j++)
		{
			int num3 = j + (int)this.scroll;
			if (num3 >= this.Items.Count)
			{
				break;
			}
			int num4 = 0;
			num2 = 0f;
			foreach (ListViewColumn<T> listViewColumn in this.Columns)
			{
				if (!listViewColumn.Visible)
				{
					num4++;
				}
				else
				{
					if (listViewColumn.IsAction)
					{
						if (GUI.Button(new Rect(this.Rect.x + num2, this.Rect.y + 24f + (float)(j * 24), array[num4], 24f), listViewColumn.ActionName, this.GUIskin.button))
						{
							listViewColumn.Action(this.Items[num3]);
						}
					}
					else
					{
						GUI.Box(new Rect(this.Rect.x + num2, this.Rect.y + 24f + (float)(j * 24), array[num4], 24f), listViewColumn.Output(this.Items[num3]), this.label);
					}
					num2 += array[num4];
					num4++;
				}
			}
			if (GUI.Button(new Rect(this.Rect.x, this.Rect.y + 24f + (float)(j * 24), this.Rect.width - (float)((!useScroll) ? 0 : 16), 24f), string.Empty, GUIStyle.none))
			{
				if (this.Selected.Contains(num3) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
				{
					this.Selected.Remove(num3);
				}
				else
				{
					this.Select(num3, !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift));
				}
				if (this.SelectChange != null)
				{
					this.SelectChange(this.SelectedItem.ToArray<T>());
				}
				SelectorController.CanClick = false;
			}
		}
		if (useScroll)
		{
			GUI.skin.verticalScrollbarThumb = this.GUIskin.verticalScrollbarThumb;
			this.scroll = Mathf.Round(GUI.VerticalScrollbar(new Rect(this.Rect.x + this.Rect.width - 16f, this.Rect.y, 16f, this.Rect.height), this.scroll, 1f, 0f, (float)(this.Items.Count - num + 1), this.GUIskin.verticalScrollbar));
			if (this.scroll < 0f)
			{
				this.scroll = 0f;
			}
		}
		foreach (int num5 in this.Selected)
		{
			if ((float)num5 >= this.scroll && (float)num5 < this.scroll + (float)num)
			{
				float num6 = (float)num5 - this.scroll;
				GUI.Box(new Rect(this.Rect.x, this.Rect.y + 24f + num6 * 24f, this.Rect.width - (float)((!useScroll) ? 0 : 16), 24f), string.Empty);
			}
		}
	}

	private List<T> _items = new List<T>();

	public List<ListViewColumn<T>> _columns = new List<ListViewColumn<T>>();

	public List<int> Selected = new List<int>();

	private float scroll;

	public Rect Rect;

	private GUISkin GUIskin;

	private GUIStyle label;

	public Action<T[]> SelectChange;

	public bool MultiSelect = true;

	private int sortBy = -1;

	private bool sortAsc = true;
}
