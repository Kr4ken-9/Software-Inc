﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SkraperGen : Landmark
{
	private void Start()
	{
		this.rend = base.GetComponent<Renderer>();
	}

	public void Init(Rect r, bool thn, bool thw, int bandH, int bandX, int bandY, bool generateBands, float height = -1f)
	{
		this.Blob = r;
		this.TopHouseWest = thw;
		this.TopHouseNorth = thn;
		float num = GameData.RNDRange(Mathf.Min(this.Blob.height, this.Blob.width), Mathf.Max(this.Blob.width, this.Blob.height));
		num = ((height != -1f) ? height : (Mathf.Ceil(num / 2f) * 2f));
		this.Height = Mathf.Min(30f, num);
		this.BandH = bandH;
		this.BandX = bandX;
		this.BandY = bandY;
		if (generateBands)
		{
			this.BandH = ((this.Height < 8f || GameData.RNDValue <= 0.25f) ? 0 : GameData.RNDRange(1, Mathf.FloorToInt(this.Height / 8f)));
			this.BandX = ((this.Blob.width < 6f || GameData.RNDValue <= 0.5f) ? 0 : GameData.RNDRange(1, Mathf.FloorToInt(this.Blob.width / 6f)));
			this.BandY = ((this.Blob.height < 6f || GameData.RNDValue <= 0.5f) ? 0 : GameData.RNDRange(1, Mathf.FloorToInt(this.Blob.height / 6f)));
		}
		HashSet<int> hashSet = new HashSet<int>();
		if (this.BandH > 0)
		{
			for (int i = 0; i < this.BandH; i++)
			{
				hashSet.Add(Mathf.FloorToInt((this.Height / 2f - 2f) / (float)(this.BandH + 1) * (float)(i + 1)));
			}
		}
		HashSet<int> hashSet2 = new HashSet<int>();
		if (this.BandX > 0)
		{
			for (int j = 0; j < this.BandX; j++)
			{
				hashSet2.Add(Mathf.FloorToInt(this.Blob.width / (float)(this.BandX + 1) * (float)(j + 1)));
			}
		}
		HashSet<int> hashSet3 = new HashSet<int>();
		if (this.BandY > 0)
		{
			for (int k = 0; k < this.BandY; k++)
			{
				hashSet3.Add(Mathf.FloorToInt(this.Blob.height / (float)(this.BandY + 1) * (float)(k + 1)));
			}
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.FloorPrefab);
		gameObject.transform.position = new Vector3(this.Blob.center.x, 0.01f, this.Blob.center.y);
		gameObject.transform.localScale = new Vector3(this.Blob.width, 1f, this.Blob.height);
		gameObject.transform.parent = base.transform;
		this.rend2 = gameObject.GetComponent<Renderer>();
		List<CombineInstance> list = new List<CombineInstance>();
		list.Add(this.MakeCombine(this.DoorPrefab, this.Blob.center.x, 1f, this.Blob.yMin, 90f, 180f, 0f, 2f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x - this.Blob.width / 4f - 0.5f, 1f, this.Blob.yMin, 90f, 180f, 0f, this.Blob.width / 2f - 1f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x + this.Blob.width / 4f + 0.5f, 1f, this.Blob.yMin, 90f, 180f, 0f, this.Blob.width / 2f - 1f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.DoorPrefab, this.Blob.center.x, 1f, this.Blob.yMax, 90f, 0f, 0f, 2f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x - this.Blob.width / 4f - 0.5f, 1f, this.Blob.yMax, 90f, 0f, 0f, this.Blob.width / 2f - 1f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x + this.Blob.width / 4f + 0.5f, 1f, this.Blob.yMax, 90f, 0f, 0f, this.Blob.width / 2f - 1f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.DoorPrefab, this.Blob.xMin, 1f, this.Blob.center.y, 90f, 270f, 0f, 2f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMin, 1f, this.Blob.center.y - this.Blob.height / 4f - 0.5f, 90f, 270f, 0f, this.Blob.height / 2f - 1f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMin, 1f, this.Blob.center.y + this.Blob.height / 4f + 0.5f, 90f, 270f, 0f, this.Blob.height / 2f - 1f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.DoorPrefab, this.Blob.xMax, 1f, this.Blob.center.y, 90f, 90f, 0f, 2f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMax, 1f, this.Blob.center.y - this.Blob.height / 4f - 0.5f, 90f, 90f, 0f, this.Blob.height / 2f - 1f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMax, 1f, this.Blob.center.y + this.Blob.height / 4f + 0.5f, 90f, 90f, 0f, this.Blob.height / 2f - 1f, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x, this.Height - 1f, this.Blob.yMin, 90f, 180f, 0f, this.Blob.width, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x, this.Height - 1f, this.Blob.yMax, 90f, 0f, 0f, this.Blob.width, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMin, this.Height - 1f, this.Blob.center.y, 90f, 270f, 0f, this.Blob.height, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMax, this.Height - 1f, this.Blob.center.y, 90f, 90f, 0f, this.Blob.height, 1f, 2f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x, this.Height - 0.5f, this.Blob.yMin + 0.5f, 90f, 0f, 0f, this.Blob.width, 1f, 1f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x, this.Height - 0.5f, this.Blob.yMax - 0.5f, 90f, 180f, 0f, this.Blob.width, 1f, 1f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMin + 0.5f, this.Height - 0.5f, this.Blob.center.y, 90f, 90f, 0f, this.Blob.height, 1f, 1f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMax - 0.5f, this.Height - 0.5f, this.Blob.center.y, 90f, 270f, 0f, this.Blob.height, 1f, 1f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x, this.Height, this.Blob.yMax - 0.25f, 0f, 0f, 0f, this.Blob.width - 1f, 1f, 0.5f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x, this.Height, this.Blob.yMin + 0.25f, 0f, 0f, 0f, this.Blob.width - 1f, 1f, 0.5f, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMax - 0.25f, this.Height, this.Blob.center.y, 0f, 0f, 0f, 0.5f, 1f, this.Blob.height, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMin + 0.25f, this.Height, this.Blob.center.y, 0f, 0f, 0f, 0.5f, 1f, this.Blob.height, false));
		list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x, this.Height - 1f, this.Blob.center.y, 0f, 0f, 0f, this.Blob.width - 1f, 1f, this.Blob.height - 1f, false));
		foreach (int num2 in hashSet)
		{
			list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x, (float)(3 + num2 * 2), this.Blob.yMin, 90f, 180f, 0f, this.Blob.width, 1f, 2f, false));
			list.Add(this.MakeCombine(this.WallPrefab, this.Blob.center.x, (float)(3 + num2 * 2), this.Blob.yMax, 90f, 0f, 0f, this.Blob.width, 1f, 2f, false));
			list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMin, (float)(3 + num2 * 2), this.Blob.center.y, 90f, 270f, 0f, this.Blob.height, 1f, 2f, false));
			list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMax, (float)(3 + num2 * 2), this.Blob.center.y, 90f, 90f, 0f, this.Blob.height, 1f, 2f, false));
		}
		foreach (int num3 in hashSet2)
		{
			list.Add(this.MakeCombine(this.WallPrefab, this.Blob.x + (float)num3 + 0.5f, this.Height / 2f, this.Blob.yMin, 90f, 180f, 0f, 1f, 1f, this.Height - 4f, false));
			list.Add(this.MakeCombine(this.WallPrefab, this.Blob.x + (float)num3 + 0.5f, this.Height / 2f, this.Blob.yMax, 90f, 0f, 0f, 1f, 1f, this.Height - 4f, false));
		}
		foreach (int num4 in hashSet3)
		{
			list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMin, this.Height / 2f, this.Blob.y + (float)num4 + 0.5f, 90f, 270f, 0f, 1f, 1f, this.Height - 4f, false));
			list.Add(this.MakeCombine(this.WallPrefab, this.Blob.xMax, this.Height / 2f, this.Blob.y + (float)num4 + 0.5f, 90f, 90f, 0f, 1f, 1f, this.Height - 4f, false));
		}
		if (r.width >= 12f && r.height >= 12f)
		{
			Vector2 vector = new Vector2(this.Blob.center.x + ((!this.TopHouseWest) ? (this.Blob.width / 4f) : (-this.Blob.width / 4f)), this.Blob.center.y + ((!this.TopHouseNorth) ? (this.Blob.height / 4f) : (-this.Blob.height / 4f)));
			float num5 = this.Blob.width / 2f - 4f;
			float num6 = this.Blob.height / 2f - 4f;
			Rect rect = new Rect(vector.x - num5 / 2f, vector.y - num6 / 2f, num5, num6);
			list.Add(this.MakeCombine(this.WallPrefab, rect.center.x, this.Height, rect.yMin, 90f, 180f, 0f, rect.width, 1f, 2f, false));
			list.Add(this.MakeCombine(this.WallPrefab, rect.center.x, this.Height, rect.yMax, 90f, 0f, 0f, rect.width, 1f, 2f, false));
			list.Add(this.MakeCombine(this.WallPrefab, rect.xMin, this.Height, rect.center.y, 90f, 270f, 0f, rect.height, 1f, 2f, false));
			list.Add(this.MakeCombine(this.WallPrefab, rect.xMax, this.Height, rect.center.y, 90f, 90f, 0f, rect.height, 1f, 2f, false));
			list.Add(this.MakeCombine(this.WallPrefab, rect.center.x, this.Height + 1f, rect.center.y, 0f, 0f, 0f, rect.width, 1f, rect.height, false));
			if (this.TopHouseNorth)
			{
				list.Add(this.MakeCombine(this.DoorPrefab, rect.center.x, this.Height, rect.yMax + 0.01f, 90f, 0f, 0f, 2f, 1f, 2f, false));
			}
			else
			{
				list.Add(this.MakeCombine(this.DoorPrefab, rect.center.x, this.Height, rect.yMin - 0.01f, 90f, 180f, 0f, 2f, 1f, 2f, false));
			}
		}
		float num7 = 0.35f;
		int num8 = 0;
		while ((float)num8 < this.Blob.width)
		{
			if (!hashSet2.Contains(num8))
			{
				int num9 = 0;
				while ((float)num9 < this.Height / 2f - 2f)
				{
					if (!hashSet.Contains(num9))
					{
						list.Add(this.MakeCombine((UnityEngine.Random.value <= num7) ? this.LightWindowPrefab : this.WindowPrefab, this.Blob.x + (float)num8 + 0.5f, (float)(num9 * 2 + 3), this.Blob.yMin, 90f, 180f, 0f, 1f, 1f, 2f, true));
						list.Add(this.MakeCombine((UnityEngine.Random.value <= num7) ? this.LightWindowPrefab : this.WindowPrefab, this.Blob.x + (float)num8 + 0.5f, (float)(num9 * 2 + 3), this.Blob.yMax, 90f, 0f, 0f, 1f, 1f, 2f, true));
					}
					num9++;
				}
			}
			num8++;
		}
		int num10 = 0;
		while ((float)num10 < this.Blob.height)
		{
			if (!hashSet3.Contains(num10))
			{
				int num11 = 0;
				while ((float)num11 < this.Height / 2f - 2f)
				{
					if (!hashSet.Contains(num11))
					{
						list.Add(this.MakeCombine((UnityEngine.Random.value <= num7) ? this.LightWindowPrefab : this.WindowPrefab, this.Blob.xMin, (float)(num11 * 2 + 3), this.Blob.y + (float)num10 + 0.5f, 90f, 270f, 0f, 1f, 1f, 2f, true));
						list.Add(this.MakeCombine((UnityEngine.Random.value <= num7) ? this.LightWindowPrefab : this.WindowPrefab, this.Blob.xMax, (float)(num11 * 2 + 3), this.Blob.y + (float)num10 + 0.5f, 90f, 90f, 0f, 1f, 1f, 2f, true));
					}
					num11++;
				}
			}
			num10++;
		}
		MeshFilter component = base.GetComponent<MeshFilter>();
		component.mesh.CombineMeshes(list.ToArray());
		this.refProbe.transform.position = new Vector3(this.Blob.center.x, this.Height / 2f + 0.5f, this.Blob.center.y);
		this.refProbe.size = new Vector3(this.Blob.width, this.Height + 1f, this.Blob.height);
	}

	private CombineInstance MakeCombine(Mesh mesh, float x, float y, float z, float rx, float ry, float rz, float sx, float sy, float sz, bool randomUV = false)
	{
		CombineInstance result = default(CombineInstance);
		result.mesh = mesh;
		if (randomUV)
		{
			result.mesh = this.CopyMesh(result.mesh);
		}
		result.transform = Matrix4x4.TRS(new Vector3(x, y, z), Quaternion.Euler(rx, ry, rz), new Vector3(sx, sy, sz));
		return result;
	}

	private Mesh CopyMesh(Mesh mesh)
	{
		Mesh mesh2 = UnityEngine.Object.Instantiate<Mesh>(mesh);
		Vector2[] array = new Vector2[mesh.vertexCount];
		Vector2 vector = new Vector2(UnityEngine.Random.value, UnityEngine.Random.value);
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = vector;
		}
		mesh2.uv2 = array;
		return mesh2;
	}

	private void Update()
	{
		bool flag = GameSettings.Instance.ActiveFloor < 0;
		if (this.Hide ^ flag)
		{
			this.Hide = flag;
			if (this.Hide)
			{
				this.rend.enabled = false;
				this.rend2.enabled = false;
				return;
			}
			this.rend.enabled = true;
			this.rend2.enabled = true;
		}
		if (this.Hide)
		{
			return;
		}
		if (!SkraperGen.NeverTransparent && (!EnvironmentEditor.Instance.gameObject.activeSelf || EnvironmentEditor.Instance.CurrentType != EnvironmentEditor.EditorType.Skyscraper) && !CameraScript.Instance.FlyMode)
		{
			Vector3 position = CameraScript.Instance.transform.position;
			Vector2 a = new Vector2(position.x, position.z);
			Vector3 position2 = CameraScript.Instance.mainCam.transform.position;
			Vector2 b = new Vector2(position2.x, position2.z);
			float num = Vector2.Dot((a - b).normalized, (a - this.Blob.center).normalized);
			bool flag2 = num > 0.5f;
			if (this.transparent ^ flag2)
			{
				this.transparent = flag2;
				this.rend.sharedMaterial = this.Materials[(!this.transparent) ? 0 : 1];
			}
		}
		else if (this.transparent)
		{
			this.transparent = false;
			this.rend.sharedMaterial = this.Materials[0];
		}
		float time = ((float)TimeOfDay.Instance.Hour + TimeOfDay.Instance.Minute / 60f) / 24f;
		float value = this.LightOnage.Evaluate(time);
		this.rend.material.SetFloat("_EmissionFactor", value);
		this.rend.material.SetFloat("_Whiteness", (!DataOverlay.HasActive) ? 0f : Mathf.Clamp01((Time.timeSinceLevelLoad - DataOverlay.Instance.ActivateTime) * 4f));
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["x"] = this.Blob.x;
		dictionary["y"] = this.Blob.y;
		dictionary["w"] = this.Blob.width;
		dictionary["h"] = this.Blob.height;
		dictionary["z"] = this.Height;
		dictionary["north"] = this.TopHouseNorth;
		dictionary["west"] = this.TopHouseWest;
		dictionary["BandH"] = this.BandH;
		dictionary["BandX"] = this.BandX;
		dictionary["BandY"] = this.BandY;
	}

	protected override object DeserializeMe(WriteDictionary d)
	{
		Rect r = new Rect(d.Get<float>("x"), d.Get<float>("y"), d.Get<float>("w"), d.Get<float>("h"));
		this.Init(r, d.Get<bool>("north"), d.Get<bool>("west"), d.Get<int>("BandH"), d.Get<int>("BandX"), d.Get<int>("BandY"), false, d.Get<float>("z"));
		return this;
	}

	public override string WriteName()
	{
		return "SkyScraper";
	}

	public override Vector2[] GetNavMesh()
	{
		return new Vector2[]
		{
			new Vector2(this.Blob.xMin, this.Blob.yMin),
			new Vector2(this.Blob.xMax, this.Blob.yMin),
			new Vector2(this.Blob.xMax, this.Blob.yMax),
			new Vector2(this.Blob.xMin, this.Blob.yMax)
		};
	}

	public override Vector2 Center()
	{
		return this.Blob.center;
	}

	public Mesh WindowPrefab;

	public Mesh LightWindowPrefab;

	public Mesh WallPrefab;

	public Mesh DoorPrefab;

	public GameObject FloorPrefab;

	public Material[] Materials;

	public ReflectionProbe refProbe;

	public Rect Blob;

	public AnimationCurve LightOnage;

	private Renderer rend;

	private Renderer rend2;

	public static bool NeverTransparent;

	private bool transparent;

	public float Height;

	private bool Hide;

	public bool TopHouseNorth;

	public bool TopHouseWest;

	public int BandH;

	public int BandX;

	public int BandY;
}
