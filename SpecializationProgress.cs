﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecializationProgress : MonoBehaviour
{
	private void Start()
	{
		if (SpecializationProgress.Instance != null)
		{
			UnityEngine.Object.Destroy(SpecializationProgress.Instance.gameObject);
		}
		SpecializationProgress.Instance = this;
		this.rect = base.GetComponent<RectTransform>();
		base.gameObject.SetActive(false);
	}

	public void Show(GUIWorkItem workItem)
	{
		this.WorkItem = workItem;
		foreach (KeyValuePair<string, GUIProgressBar[]> keyValuePair in this.Bars)
		{
			UnityEngine.Object.Destroy(keyValuePair.Value[0].transform.parent.parent.gameObject);
		}
		this.Bars.Clear();
		this.Item = null;
		this.Item2 = null;
		if (this.WorkItem.work is SoftwareWorkItem)
		{
			this.Item = (SoftwareWorkItem)workItem.work;
			for (int i = 0; i < this.Item.Specs.Length; i++)
			{
				string text = this.Item.Specs[i];
				if (!(this.Item is DesignDocument) || this.Item.SpecDevTime[i, 0] != 0f)
				{
					GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ProgressbarPrefab);
					GUIProgressBar[] componentsInChildren = gameObject.GetComponentsInChildren<GUIProgressBar>();
					gameObject.GetComponentInChildren<Text>().text = text.LocTry();
					gameObject.transform.SetParent(this.InnerPanel.transform, false);
					this.Bars[text] = componentsInChildren;
					if (this.Item is DesignDocument)
					{
						componentsInChildren[0].EndColor = (componentsInChildren[0].StartColor = (componentsInChildren[0].EndExt = new Color32(133, 162, 219, byte.MaxValue)));
						componentsInChildren[1].gameObject.SetActive(false);
					}
					else
					{
						float num = (this.Item as SoftwareAlpha).SpecCodeArt[text];
						if (num == 1f)
						{
							componentsInChildren[1].gameObject.SetActive(false);
						}
						else if (num == 0f)
						{
							componentsInChildren[0].gameObject.SetActive(false);
						}
					}
				}
			}
		}
		else if (this.WorkItem.work is ResearchWork)
		{
			this.Item2 = (ResearchWork)workItem.work;
			string text2 = (!this.Item2.ArtCat.Equals(this.Item2.CodeCat)) ? (this.Item2.CodeCat.LocTry() + "/" + this.Item2.ArtCat.LocTry()) : this.Item2.CodeCat.LocTry();
			GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.ProgressbarPrefab);
			GUIProgressBar[] componentsInChildren2 = gameObject2.GetComponentsInChildren<GUIProgressBar>();
			gameObject2.GetComponentInChildren<Text>().text = text2;
			gameObject2.transform.SetParent(this.InnerPanel.transform, false);
			componentsInChildren2[0].EndColor = (componentsInChildren2[0].StartColor = (componentsInChildren2[0].EndExt = new Color32(133, 162, 219, byte.MaxValue)));
			componentsInChildren2[1].gameObject.SetActive(false);
			this.Bars["design"] = componentsInChildren2;
			gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.ProgressbarPrefab);
			gameObject2.GetComponentInChildren<Text>().text = string.Empty;
			componentsInChildren2 = gameObject2.GetComponentsInChildren<GUIProgressBar>();
			gameObject2.transform.SetParent(this.InnerPanel.transform, false);
			componentsInChildren2[1].gameObject.SetActive(this.Item2.CodeMax > 0f);
			componentsInChildren2[1].gameObject.SetActive(this.Item2.ArtMax > 0f);
			this.Bars["artcode"] = componentsInChildren2;
		}
		base.gameObject.SetActive(true);
		this.hide = false;
		this.rect.sizeDelta = new Vector2(0f, this.rect.sizeDelta.y);
	}

	public void Hide()
	{
		this.hide = true;
	}

	private void OnDestroy()
	{
		if (SpecializationProgress.Instance == this)
		{
			SpecializationProgress.Instance = null;
		}
	}

	private void Update()
	{
		if (this.hide)
		{
			this.rect.sizeDelta = new Vector2(Mathf.Lerp(this.rect.sizeDelta.x, 0f, Time.deltaTime * 20f), this.rect.sizeDelta.y);
			if (this.rect.sizeDelta.x < 0.1f)
			{
				base.gameObject.SetActive(false);
			}
			return;
		}
		float num = Mathf.Round(Mathf.Lerp(this.rect.sizeDelta.x, 128f, Time.deltaTime * 20f));
		this.rect.sizeDelta = new Vector2((128f - num >= 4f) ? num : 128f, this.rect.sizeDelta.y);
		if (this.WorkItem == null || this.WorkItem.gameObject == null || !this.WorkItem.gameObject.activeSelf)
		{
			this.Hide();
			return;
		}
		RectTransform component = this.WorkItem.GetComponent<RectTransform>();
		this.rect.anchoredPosition = new Vector2(-this.WorkPanel.rect.width, Mathf.Clamp(component.anchoredPosition.y - 96f + this.SubPanel.anchoredPosition.y, -this.WorkPanel.rect.height - this.WorkPanel.anchoredPosition.y + 74f, -96f));
		if (this.Item != null)
		{
			int num2 = 0;
			if (this.Item is DesignDocument)
			{
				foreach (KeyValuePair<string, GUIProgressBar[]> keyValuePair in this.Bars)
				{
					float num3 = this.Item.SpecDevTime[num2, 0];
					while (num3 == 0f && num2 < this.Item.SpecDevTime.GetLength(0))
					{
						num2++;
						num3 = this.Item.SpecDevTime[num2, 0];
					}
					float num4 = this.Item.SpecQuality[num2] / num3;
					if (num4 > 1f)
					{
						num4 = 2f - num4;
					}
					keyValuePair.Value[0].Value = num4;
					num2++;
				}
			}
			else
			{
				foreach (KeyValuePair<string, GUIProgressBar[]> keyValuePair2 in this.Bars)
				{
					SoftwareAlpha softwareAlpha = this.Item as SoftwareAlpha;
					keyValuePair2.Value[0].Value = Mathf.Min(softwareAlpha.FinalSpecQuality[num2, 0], SoftwareAlpha.GetMaxCodeQuality(softwareAlpha.CodeProgress));
					keyValuePair2.Value[1].Value = softwareAlpha.FinalSpecQuality[num2, 1];
					num2++;
				}
			}
		}
		else if (this.Item2 != null)
		{
			this.Bars["design"][0].Value = this.Item2.DesignProgress / this.Item2.DesignMax;
			this.Bars["artcode"][0].Value = this.Item2.CodeProgress / this.Item2.CodeMax;
			this.Bars["artcode"][1].Value = this.Item2.ArtProgress / this.Item2.ArtMax;
		}
	}

	public GameObject InnerPanel;

	public Dictionary<string, GUIProgressBar[]> Bars = new Dictionary<string, GUIProgressBar[]>();

	public static SpecializationProgress Instance;

	public GameObject ProgressbarPrefab;

	public RectTransform WorkPanel;

	public RectTransform SubPanel;

	private GUIWorkItem WorkItem;

	[NonSerialized]
	public SoftwareWorkItem Item;

	[NonSerialized]
	public ResearchWork Item2;

	private RectTransform rect;

	private bool hide;
}
