﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ErrorOverlay : MonoBehaviour
{
	private void Awake()
	{
		if (ErrorOverlay.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		ErrorOverlay.Instance = this;
		base.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		if (ErrorOverlay.Instance == this)
		{
			ErrorOverlay.Instance = null;
		}
	}

	public void ShowError(string nonloc, bool stay = false, bool timed = false, float timer = 0f, bool loc = true)
	{
		if (!string.IsNullOrEmpty(nonloc))
		{
			base.gameObject.SetActive(true);
			this.Show = 4;
			this.Label.text = ((!loc) ? nonloc : nonloc.Loc());
			this.Stay = stay;
			this.Timed = timed;
			this.Timer = timer;
			LayoutRebuilder.ForceRebuildLayoutImmediate(this.rect);
		}
	}

	private void Update()
	{
		this.rect.anchoredPosition = new Vector2(Input.mousePosition.x, (float)(-(float)Screen.height) + Input.mousePosition.y + 64f);
		if (!this.Stay)
		{
			if (this.Timed)
			{
				this.Timer -= Time.deltaTime;
				if (this.Timer <= 0f)
				{
					this.Timed = false;
					base.gameObject.SetActive(false);
				}
			}
			else
			{
				this.Show--;
				if (this.Show < 0)
				{
					base.gameObject.SetActive(false);
				}
			}
		}
	}

	private int Show;

	private bool Stay;

	private bool Timed;

	private float Timer = -1f;

	private string error;

	public RectTransform rect;

	public Text Label;

	public static ErrorOverlay Instance;
}
