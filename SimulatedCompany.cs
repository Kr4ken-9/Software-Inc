﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class SimulatedCompany : Company
{
	public SimulatedCompany(string name, SDateTime time, CompanyType stype, Dictionary<string, string[]> categories, float avgQual) : base(name, SimulatedCompany.StartingMoney(categories, avgQual, time.Year), time, false)
	{
		this._type = stype.Name;
		this.Categories = categories.SelectMany((KeyValuePair<string, string[]> x) => from z in x.Value
		select new KeyValuePair<string, string>(x.Key, z)).ToList<KeyValuePair<string, string>>();
		this.AverageQuality = avgQual;
		this.BusinessSavy = Utilities.RandomGaussClamped(0.5f, 0.2f);
	}

	public SimulatedCompany()
	{
	}

	public CompanyType Type
	{
		get
		{
			return GameSettings.Instance.CompanyTypes[this._type];
		}
	}

	public bool Autonomous
	{
		get
		{
			return this._autonomous;
		}
		set
		{
			this._autonomous = value;
		}
	}

	public bool DoWork(float work)
	{
		if (this._focus >= work)
		{
			this._workItemFocus += work;
			this._focus -= work;
			return true;
		}
		return false;
	}

	private static int StartingMoney(Dictionary<string, string[]> category, float avgQual, int year)
	{
		float num = 0f;
		int num2 = 0;
		foreach (KeyValuePair<string, string[]> keyValuePair in category)
		{
			SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[keyValuePair.Key];
			foreach (string text in keyValuePair.Value)
			{
				if (text == null)
				{
					foreach (string cat in GameSettings.Instance.SoftwareTypes[keyValuePair.Key].Categories.Keys)
					{
						num += softwareType.MaxDevTime(cat, year);
						num2++;
					}
				}
				else
				{
					num += softwareType.MaxDevTime(text, year);
					num2++;
				}
			}
		}
		float num3 = (num2 != 0) ? (num / (float)num2) : 1f;
		return (int)Mathf.Max(10000f, Utilities.RandomGaussClamped(0.75f, 0.1f) * (num3 * 18000f));
	}

	public void SimulateDistribution(MarketSimulation sim, SDateTime time, float pvsd)
	{
		float num = base.Money * 0.2f;
		foreach (SoftwareProduct softwareProduct in from x in this.Products
		where Utilities.GetMonths(x.Release, time) <= 60f
		orderby x.Release descending
		select x)
		{
			List<int> unitSales = softwareProduct.GetUnitSales(false);
			if (unitSales.Count > 3)
			{
				int num2 = unitSales[unitSales.Count - 1];
				if (softwareProduct.MissedPhysicalSales + num2 == 0)
				{
					continue;
				}
			}
			num -= this.SimulateProductDistribution(softwareProduct, num, pvsd, (unitSales.Count <= 0) ? -1 : unitSales[unitSales.Count - 1]);
			if (num <= 0f)
			{
				break;
			}
		}
	}

	private float SimulateProductDistribution(SoftwareProduct p, float budget, float pvsd, int last)
	{
		float b;
		if (last > -1)
		{
			b = Mathf.Max(0f, (float)last * 0.9f + p.Followers * 0.25f + (float)p.MissedPhysicalSales * 1.5f - p.PhysicalCopies) * MarketSimulation.PhysicalCopyPrice;
		}
		else
		{
			b = (Mathf.Max(0f, p.Type.GetReach(p._category, p.Features, p.OSs) * pvsd * 0.01f * p.Quality + p.Followers - p.UnitSum - p.PhysicalCopies) + (float)(p.MissedPhysicalSales * 2)) * MarketSimulation.PhysicalCopyPrice;
		}
		int num = Mathf.FloorToInt(Mathf.Min(budget, b) / MarketSimulation.PhysicalCopyPrice);
		if (num > 0)
		{
			float num2 = (float)num * MarketSimulation.PhysicalCopyPrice;
			base.MakeTransaction(-num2, Company.TransactionCategory.Distribution, null);
			p.Loss += num2;
			p.PhysicalCopies += (uint)num;
			return num2;
		}
		return 0f;
	}

	public void FreeFocus(SimulatedCompany.ProductPrototype p)
	{
		this._focus += this.Type.GetEffort(p.Type, p.Category);
	}

	private void UpdateReleases(SDateTime time, List<SoftwareProduct> releases)
	{
		for (int i = 0; i < this.Releases.Count; i++)
		{
			SimulatedCompany.ProductPrototype productPrototype = this.Releases[i];
			try
			{
				int num = (time - productPrototype.ReleaseDate).ToInt();
				float productMonthlyCost = this.GetProductMonthlyCost(productPrototype);
				float num2 = 1f;
				if (productMonthlyCost > 0f)
				{
					float num3 = Mathf.Min(base.Money * 0.25f, productMonthlyCost);
					num2 = num3 / productMonthlyCost;
					base.MakeTransaction(-productMonthlyCost, Company.TransactionCategory.Salaries, null);
					productPrototype.Loss += productMonthlyCost;
				}
				productPrototype.DevTimeLeft = Mathf.Max(0f, productPrototype.DevTimeLeft - num2);
				if (!productPrototype.IsInDeal() && num >= 0)
				{
					HUD.Instance.dealWindow.CancelWorkDeal(productPrototype, this);
					SoftwareProduct softwareProduct = productPrototype.ToProduct(time);
					releases.Add(softwareProduct);
					this.Products.Add(softwareProduct);
					GameSettings.Instance.CancelPrintOrder(softwareProduct.ID, true);
					productPrototype.RemoveProject();
					i--;
				}
				else if (!productPrototype.PrintDeal && !base.IsPlayerOwned() && num >= -1440 * GameSettings.DaysPerMonth * Utilities.RandomRange(4, 12))
				{
					this.OfferPrintDeal(productPrototype, time);
				}
			}
			catch (Exception exception)
			{
				Debug.LogException(exception);
				this.Releases.Remove(productPrototype);
				i--;
			}
		}
		if (this._workItemFocus > 0f)
		{
			base.MakeTransaction(-this.WorkItemCost, Company.TransactionCategory.Salaries, null);
			this.WorkItemCost = 0f;
			this._focus += this._workItemFocus;
			this._workItemFocus = 0f;
		}
	}

	private void OfferPrintDeal(SimulatedCompany.ProductPrototype pr, SDateTime time)
	{
		pr.PrintDeal = true;
		if (pr.DevTime * Utilities.RandomGauss(1f - GameSettings.Instance.MyCompany.BusinessReputation * 0.5f, 0.2f) < GameSettings.Instance.MyCompany.BusinessReputation * 50f)
		{
			float physicalVsDigital = MarketSimulation.GetPhysicalVsDigital(time);
			SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[pr.Type];
			uint num = (uint)(softwareType.GetReach(pr.Category, pr.Features, pr.OSs) * physicalVsDigital * Utilities.RandomRange(0.1f, 0.5f) * pr.Quality);
			if (num > 10000u)
			{
				float num2 = Utilities.RandomRange(0.1f, 0.35f);
				num2 *= MarketSimulation.PhysicalCopyPrice;
				uint num3 = (uint)(base.Money * 0.2f / num2);
				if (num > num3)
				{
					num = num3;
				}
				if (num > 10000u)
				{
					num3 = (uint)(Utilities.GetMonths(time, pr.ReleaseDate) * 600000f);
					if (num > num3)
					{
						num = num3;
					}
					HUD.Instance.dealWindow.AddDeal(new PrintDeal(this, pr, num2, num));
				}
			}
		}
	}

	private void Research()
	{
		if (base.IsPlayerOwned())
		{
			return;
		}
		using (Dictionary<KeyValuePair<string, string>, float>.Enumerator enumerator = this.Type.Types.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				KeyValuePair<KeyValuePair<string, string>, float> type = enumerator.Current;
				SoftwareType softwareType;
				if (GameSettings.Instance.SoftwareTypes.TryGetValue(type.Key.Key, out softwareType))
				{
					foreach (Feature feature in (from x in softwareType.Features.Values
					where x.Research != null && !x.Researched && x.IsCompatible(type.Key.Value) && x.IsUnlocked(TimeOfDay.Instance.Year, type.Key.Value)
					select x).ToList<Feature>())
					{
						float num = Mathf.Max(0f, Utilities.GetMonths(new SDateTime(feature.Unlock.Value), SDateTime.Now()) - feature.DevTime);
						float num2 = Mathf.Clamp(0.1f * (num / (feature.DevTime * 8f)), 0f, 0.1f);
						num2 *= 0.4f + this.BusinessSavy * 0.2f;
						if (Utilities.RandomValue < num2)
						{
							feature.TransferPatent(this);
							return;
						}
					}
				}
			}
		}
	}

	private int GetUpcoming(string type, string cat)
	{
		int num = 0;
		foreach (SimulatedCompany simulatedCompany in GameSettings.Instance.simulation.Companies.Values)
		{
			for (int i = 0; i < simulatedCompany.Releases.Count; i++)
			{
				SimulatedCompany.ProductPrototype productPrototype = simulatedCompany.Releases[i];
				if (productPrototype.Type.Equals(type) && (cat == null || productPrototype.Category.Equals(cat)))
				{
					num++;
				}
			}
		}
		return num;
	}

	private KeyValuePair<SoftwareType, SoftwareCategory>? PickReleaseType(SDateTime time)
	{
		if (this._focus <= 0f)
		{
			return null;
		}
		KeyValuePair<string, string>? keyValuePair = null;
		if (this.Categories.Count == 1)
		{
			SoftwareType orNull = GameSettings.Instance.SoftwareTypes.GetOrNull(this.Categories[0].Key);
			if (orNull != null && orNull.IsUnlocked(time.Year))
			{
				keyValuePair = new KeyValuePair<string, string>?(this.Categories[0]);
			}
		}
		else
		{
			int num = 0;
			foreach (KeyValuePair<string, string> value in from x in this.Categories
			orderby Utilities.RandomValue
			select x)
			{
				SoftwareType orNull2 = GameSettings.Instance.SoftwareTypes.GetOrNull(value.Key);
				if (orNull2 != null && orNull2.IsUnlocked(time.Year))
				{
					int upcoming = this.GetUpcoming(value.Key, value.Value);
					if (keyValuePair == null || upcoming < num)
					{
						num = upcoming;
						keyValuePair = new KeyValuePair<string, string>?(value);
					}
				}
			}
		}
		if (keyValuePair != null)
		{
			SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[keyValuePair.Value.Key];
			string text = keyValuePair.Value.Value;
			SoftwareCategory softwareCategory = null;
			if (text != null)
			{
				softwareCategory = softwareType.Categories.GetOrNull(text);
				if (softwareCategory == null || !softwareCategory.IsUnlocked(time.Year))
				{
					softwareCategory = null;
				}
			}
			else if (softwareType.Categories.Count == 1)
			{
				text = softwareType.Categories.First<KeyValuePair<string, SoftwareCategory>>().Key;
				softwareCategory = softwareType.Categories.GetOrNull(text);
				if (softwareCategory == null || !softwareCategory.IsUnlocked(time.Year))
				{
					softwareCategory = null;
				}
			}
			else
			{
				int num2 = 0;
				foreach (SoftwareCategory softwareCategory2 in from x in softwareType.Categories.Values
				orderby Utilities.RandomValue
				select x)
				{
					if (softwareCategory2.IsUnlocked(time.Year))
					{
						int upcoming2 = this.GetUpcoming(softwareType.Name, softwareCategory2.Name);
						if (softwareCategory == null || upcoming2 < num2)
						{
							num2 = upcoming2;
							softwareCategory = softwareCategory2;
						}
					}
				}
			}
			if (softwareCategory != null)
			{
				return new KeyValuePair<SoftwareType, SoftwareCategory>?(new KeyValuePair<SoftwareType, SoftwareCategory>(softwareType, softwareCategory));
			}
		}
		return null;
	}

	private float UpgradeChance()
	{
		return Utilities.RandomGaussClamped(1f, (1f - this.BusinessSavy) * 0.1f);
	}

	public SoftwareProduct ReleaseNow(SoftwareType type, SoftwareCategory cat, SDateTime time)
	{
		this._firstLaunch = false;
		Dictionary<string, uint> needs = this.ChooseNeeds(type, cat.Name, time);
		if (needs == null)
		{
			return null;
		}
		uint[] os = this.ChooseOS(type, time, needs);
		string[] features = type.GenerateFeatures(this.UpgradeChance(), cat.Name, needs, os, time, false);
		string[] needs2 = type.GetNeeds(features, cat.Name);
		if (needs2.Any((string x) => !needs.ContainsKey(x)))
		{
			return null;
		}
		needs = needs2.ToDictionary((string x) => x, (string x) => needs[x]);
		float codeProgress = Utilities.RandomGaussClamped(1f, 0.1f);
		float artProgress = Utilities.RandomGaussClamped(1f, 0.1f);
		float codeQuality = Utilities.RandomGaussClamped(this.AverageQuality, 0.1f);
		float artQuality = Utilities.RandomGaussClamped(this.AverageQuality, 0.1f);
		SoftwareProduct softwareProduct = new SoftwareProduct(GameSettings.Instance.simulation.GenerateProductName(type, cat), type.Name, cat.Name, needs, os, codeProgress, artProgress, codeQuality, artQuality, Utilities.GetBasePrice(type.Name, cat.Name, type.DevTime(features, cat.Name, null), type.FinalQualityCalc(codeProgress, artProgress, codeQuality, artQuality, features)), time, time, 0, false, this, null, GameSettings.Instance.simulation.GetID(), 0f, features, null, 0u, null);
		this.Products.Add(softwareProduct);
		return softwareProduct;
	}

	public void Simulate(SDateTime time, List<SoftwareProduct> releases)
	{
		if (!base.IsSubsidiary() && this.Cashflow["Balance"].Count > 0 && base.Money < this.Cashflow["Balance"].Average() / 2f)
		{
			int freeStock = base.GetFreeStock();
			if (freeStock >= 0)
			{
				KeyValuePair<Company, float> keyValuePair = GameSettings.Instance.simulation.FindBuyer(0u, this.ID, base.CompanyStockPrice);
				if (keyValuePair.Key != null)
				{
					base.BuyStock(keyValuePair.Key, freeStock, keyValuePair.Value);
				}
			}
		}
		this.TradeStocks();
		this.PayExpenses(time);
		bool flag = false;
		if (this.ProjectQueue.Count > 0 && this._focus > 0f)
		{
			SimulatedCompany.ProductPrototype productPrototype = this.ProjectQueue[0];
			this.ProjectQueue.RemoveAt(0);
			productPrototype.StartDev(time, this.PickReleaseDate(GameSettings.Instance.SoftwareTypes[productPrototype.Type], productPrototype.Category, productPrototype.Features, time));
			this.Releases.Add(productPrototype);
			flag = true;
		}
		if (!this.Autonomous || flag)
		{
			this.Marketing(time);
			this.Research();
			this.UpdateReleases(time, releases);
			return;
		}
		KeyValuePair<SoftwareType, SoftwareCategory>? keyValuePair2 = this.PickReleaseType(time);
		if (keyValuePair2 == null)
		{
			this.Marketing(time);
			this.Research();
			this.UpdateReleases(time, releases);
			return;
		}
		Dictionary<string, uint> needs = this.ChooseNeeds(keyValuePair2.Value.Key, keyValuePair2.Value.Value.Name, time);
		if (needs == null)
		{
			this.Marketing(time);
			this.Research();
			this.UpdateReleases(time, releases);
			return;
		}
		uint[] array = this.ChooseOS(keyValuePair2.Value.Key, time, needs);
		if (keyValuePair2.Value.Key.OSSpecific && array == null)
		{
			this.Marketing(time);
			this.Research();
			this.UpdateReleases(time, releases);
			return;
		}
		if (keyValuePair2.Value.Key.ForceIssueBool(keyValuePair2.Value.Value.Name, needs, array))
		{
			this.Marketing(time);
			this.Research();
			this.UpdateReleases(time, releases);
			return;
		}
		string[] array2 = keyValuePair2.Value.Key.GenerateFeatures(this.UpgradeChance(), keyValuePair2.Value.Value.Name, needs, array, time, true);
		string[] needs2 = keyValuePair2.Value.Key.GetNeeds(array2, keyValuePair2.Value.Value.Name);
		if (needs2.Any((string x) => !needs.ContainsKey(x)))
		{
			this.Marketing(time);
			this.Research();
			this.UpdateReleases(time, releases);
			return;
		}
		needs = needs2.ToDictionary((string x) => x, (string x) => needs[x]);
		List<uint> list = needs.Values.ToList<uint>();
		if (array != null)
		{
			list.AddRange(array);
		}
		float num = 0f;
		if (!this.PayLicenses(list, time, ref num))
		{
			this.Marketing(time);
			this.Research();
			this.UpdateReleases(time, releases);
			return;
		}
		uint? sequelTo = this.MakeSequel(keyValuePair2.Value.Key.Name, keyValuePair2.Value.Value.Name);
		float codeProgress = this.PickQuality(false, true);
		float artProgress = this.PickQuality(true, true);
		float codeQuality = this.PickQuality(false, false);
		float artQuality = this.PickQuality(true, false);
		float num2 = keyValuePair2.Value.Key.DevTime(array2, keyValuePair2.Value.Value.Name, null);
		KeyValuePair<float, float> keyValuePair3 = this.Premarket(num2);
		SDateTime sdateTime = this.PickReleaseDate(keyValuePair2.Value.Key, keyValuePair2.Value.Value.Name, array2, time);
		int monthsFlat = Utilities.GetMonthsFlat(time, sdateTime);
		SimulatedCompany.ProductPrototype productPrototype2 = new SimulatedCompany.ProductPrototype((sequelTo == null) ? GameSettings.Instance.simulation.GenerateProductName(keyValuePair2.Value.Key, keyValuePair2.Value.Value) : GameSettings.Instance.simulation.GenerateProductSequalName(GameSettings.Instance.simulation.GetProduct(sequelTo.Value, false).Name), keyValuePair2.Value.Key.Name, keyValuePair2.Value.Value.Name, needs, array, codeProgress, artProgress, codeQuality, artQuality, SimulatedCompany.PickPrice(keyValuePair2.Value.Key.Name, keyValuePair2.Value.Value.Name, keyValuePair2.Value.Key.DevTime(array2, keyValuePair2.Value.Value.Name, null), keyValuePair2.Value.Key.FinalQualityCalc(codeProgress, artProgress, codeQuality, artQuality, array2), this.BusinessSavy, time), this, false, keyValuePair3.Key, sequelTo, array2, num + keyValuePair3.Value);
		productPrototype2.StartDev(time, sdateTime);
		this.Releases.Add(productPrototype2);
		if (!base.IsPlayerOwned() && num2 * Utilities.RandomGauss(1f - GameSettings.Instance.MyCompany.BusinessReputation * 0.5f, 0.2f) < GameSettings.Instance.MyCompany.BusinessReputation * 50f)
		{
			float num3 = (keyValuePair2.Value.Key.CodeArtRatio(array2) <= 0f) ? 0f : Utilities.RandomValue;
			HUD.Instance.dealWindow.AddDeal(new WorkDeal(productPrototype2, (num3 <= 0.5f) ? WorkDeal.WorkType.Development : WorkDeal.WorkType.Design, this, time, monthsFlat - 2));
		}
		this.Marketing(time);
		this.Research();
		if (this.Products.Count > 0 && Utilities.RandomRange(0, 10 * GameSettings.DaysPerMonth) == 0)
		{
			float num4 = Utilities.RandomGaussClamped(0.5f, 0.2f);
			SoftwareProduct softwareProduct = this.Products.Last<SoftwareProduct>();
			base.AddFans(-Mathf.RoundToInt(num4 * (float)MarketSimulation.MagicRepFactor), softwareProduct._type, softwareProduct._category);
		}
		this.UpdateReleases(time, releases);
	}

	public float PickQuality(bool art, bool progress)
	{
		return Utilities.RandomGaussClamped((!progress) ? this.AverageQuality : 1f, 0.1f);
	}

	private SDateTime PickReleaseDate(SoftwareType type, string category, string[] features, SDateTime time)
	{
		int num = 0;
		float artRatio = type.CodeArtRatio(features);
		float num2 = type.DevTime(features, category, null);
		bool firstLaunch = this._firstLaunch;
		if (!this._firstLaunch)
		{
			int[] optimalEmployeeCount = type.GetOptimalEmployeeCount(num2, artRatio);
			optimalEmployeeCount[0] = Utilities.RandomRange(Mathf.Max(1, optimalEmployeeCount[0] - 5), optimalEmployeeCount[0] + 5);
			optimalEmployeeCount[1] = Utilities.RandomRange(Mathf.Max(1, optimalEmployeeCount[1] - 5), optimalEmployeeCount[1] + 5);
			num = Mathf.RoundToInt(GameData.ProjectDevTime(optimalEmployeeCount[0], optimalEmployeeCount[1], num2, artRatio) * Utilities.RandomRange(0.95f, 1.05f)) * 2;
		}
		else
		{
			this._firstLaunch = false;
		}
		SDateTime sdateTime = time + new SDateTime(num, 0);
		int num3 = 0;
		if (!firstLaunch)
		{
			float num4 = Mathf.Min(12f, Utilities.RandomRange(num2 / 4f, num2 / 3f));
			float num5 = MarketSimulation.TimeFactor[sdateTime.Month];
			int num6 = Mathf.RoundToInt(-Mathf.Min(num2 / 10f, num4 / 2f));
			int num7 = Mathf.RoundToInt((num4 % 2f != 1f) ? (num4 / 2f) : (num4 / 2f + 1f));
			for (int i = num6; i < num7; i++)
			{
				int num8 = (i >= 0) ? i : (12 + i);
				float num9 = MarketSimulation.TimeFactor[(sdateTime.Month + num8) % 12];
				if (num9 > num5)
				{
					num5 = num9;
					num3 = i;
				}
			}
		}
		return time + new SDateTime(Mathf.Max(1, num + num3), 0);
	}

	public static float PickPrice(string type, string category, float devtime, float quality, float BusinessSavy, SDateTime time)
	{
		float num = Utilities.GetBasePrice(type, category, devtime, quality) * Utilities.RandomRange(0.95f, 1.05f);
		List<SoftwareProduct> list = (from x in GameSettings.Instance.simulation.GetAllProducts()
		where x._type.Equals(type) && x._category.Equals(category) && Utilities.GetMonths(x.Release, time) < 60f && x.UnitSum > 0u
		select x).ToList<SoftwareProduct>();
		if (list.Count > 0)
		{
			float? num2 = list.AverageOutlier((SoftwareProduct x) => x.Price / x.DevTime, 0.5f);
			if (num2 != null && num2.Value > 0f)
			{
				float num3 = list.Average((SoftwareProduct x) => x.RealQuality);
				if (num3 > 0f)
				{
					float num4 = 1.05f - Utilities.RandomValue * BusinessSavy * 0.1f;
					return Mathf.Lerp(num, quality / num3 * num2.Value * devtime * num4, 0.5f);
				}
			}
		}
		return num;
	}

	private uint? MakeSequel(string type, string category)
	{
		SimulatedCompany.<MakeSequel>c__AnonStorey6 <MakeSequel>c__AnonStorey = new SimulatedCompany.<MakeSequel>c__AnonStorey6();
		<MakeSequel>c__AnonStorey.type = type;
		<MakeSequel>c__AnonStorey.category = category;
		<MakeSequel>c__AnonStorey.$this = this;
		List<SoftwareProduct> list = (from x in this.Products
		where x._type.Equals(<MakeSequel>c__AnonStorey.type) && x._category.Equals(<MakeSequel>c__AnonStorey.category)
		select x).ToList<SoftwareProduct>();
		foreach (SoftwareProduct softwareProduct in list.ToList<SoftwareProduct>())
		{
			if (softwareProduct.SequelTo != null)
			{
				list.Remove(softwareProduct.SequelTo);
			}
		}
		if (list.Count <= 0 || Utilities.RandomValue >= GameSettings.Instance.SoftwareTypes[<MakeSequel>c__AnonStorey.type].Categories[<MakeSequel>c__AnonStorey.category].Iterative)
		{
			return null;
		}
		uint r = list.MaxInstance((SoftwareProduct x) => <MakeSequel>c__AnonStorey.$this.GetFranchiseWorth(x)).ID;
		if (this.Releases.Any((SimulatedCompany.ProductPrototype x) => x.SequelTo != null && x.SequelTo.Value == r))
		{
			return null;
		}
		if (this.ProjectQueue.Any((SimulatedCompany.ProductPrototype x) => x.SequelTo != null && x.SequelTo.Value == r))
		{
			return null;
		}
		return new uint?(r);
	}

	private float GetFranchiseWorth(SoftwareProduct p)
	{
		float num = p.Sum;
		for (SoftwareProduct sequelTo = p.SequelTo; sequelTo != null; sequelTo = sequelTo.SequelTo)
		{
			num += sequelTo.Sum;
		}
		return num;
	}

	private float GetProductMonthlyCost(SimulatedCompany.ProductPrototype product)
	{
		if (!product.IsInDeal())
		{
			SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[product.Type];
			float artRatio = softwareType.CodeArtRatio(product.Features);
			float num = Mathf.Sqrt(base.Money / 100000f) / 1.5f;
			return num * this.BusinessSavy.MapRange(0f, 1f, 1f, 0.75f, false) * this.AverageQuality.MapRange(0f, 1f, 0.5f, 1f, false) * (Employee.AverageWage * (float)softwareType.GetOptimalEmployeeCount(product.DevTime, artRatio).Sum()) / (float)GameSettings.DaysPerMonth;
		}
		return 0f;
	}

	private void PayExpenses(SDateTime time)
	{
		float num = 0f;
		foreach (SoftwareProduct softwareProduct in from x in this.Products
		where Utilities.GetMonths(x.Release, time) < x.DevTime
		select x)
		{
			float artRatio = softwareProduct.Type.CodeArtRatio(softwareProduct.Features);
			float num2 = (float)softwareProduct.Type.GetOptimalEmployeeCount(softwareProduct.DevTime, artRatio).Sum() * softwareProduct.GetTime(time, 0.15f);
			num += 2f * (Employee.AverageWage * num2) / (float)GameSettings.DaysPerMonth;
		}
		base.MakeTransaction(-num, Company.TransactionCategory.Salaries, null);
		float num3 = (from x in this.Products
		where !x.Traded
		select x).SumSafe((SoftwareProduct x) => Mathf.Max(0f, x.Sum)) / (15000f * (1f + this.BusinessSavy));
		num3 *= 0.1f + Utilities.RandomGaussClamped(0f, 0.05f);
		num3 = Mathf.Max(1f, num3);
		float num4 = -(this.AverageQuality + Utilities.RandomGaussClamped(0f, 0.05f)) * 400f * num3;
		num4 /= (float)GameSettings.DaysPerMonth;
		base.MakeTransaction(num4, Company.TransactionCategory.Bills, null);
	}

	private void TradeStocks()
	{
		if (base.IsSubsidiary())
		{
			return;
		}
		foreach (Stock stock in this.OwnedStock.ToList<Stock>())
		{
			if (this.Bankrupt)
			{
				break;
			}
			if (this.OwnedStock.Contains(stock))
			{
				Company targetCompany = stock.TargetCompany;
				bool bankrupt = targetCompany.Bankrupt;
				bool flag = stock.Target == this.ID;
				float change = stock.Change;
				float currentWorth = stock.CurrentWorth;
				if (!bankrupt && !flag)
				{
					if (targetCompany is SimulatedCompany && targetCompany.Stocks[0] != null && base.Money > targetCompany.Stocks[0].CurrentWorth * 2f && targetCompany.CanBuyOut(this))
					{
						targetCompany.BuyStock(this, 0, 0f);
					}
					else if (currentWorth > 0f && change > 0.25f)
					{
						KeyValuePair<Company, float> keyValuePair = GameSettings.Instance.simulation.FindBuyer(this.ID, targetCompany.ID, currentWorth);
						if (keyValuePair.Key != null && keyValuePair.Value > stock.InitialPrice)
						{
							targetCompany.TradeStock(keyValuePair.Key, stock, keyValuePair.Value);
						}
					}
				}
			}
		}
	}

	public KeyValuePair<float, float> Premarket(float devTime)
	{
		float num = 4800f * devTime * (1f - this.BusinessSavy * 0.25f);
		float num2 = base.Money * 0.1f;
		float num3 = Mathf.Min(1f, num2 / num);
		float num4 = num3 * num;
		base.MakeTransaction(-num4, Company.TransactionCategory.Marketing, null);
		return new KeyValuePair<float, float>(num3, num4);
	}

	private void Marketing(SDateTime time)
	{
		HashSet<SoftwareProduct> hashSet = new HashSet<SoftwareProduct>();
		List<object> activeDealsPerformance = HUD.Instance.dealWindow.GetActiveDealsPerformance();
		for (int i = 0; i < activeDealsPerformance.Count; i++)
		{
			object obj = activeDealsPerformance[i];
			WorkDeal workDeal = obj as WorkDeal;
			if (workDeal != null && workDeal.workType == WorkDeal.WorkType.Marketing)
			{
				MarketingPlan marketingPlan = workDeal.WorkItem as MarketingPlan;
				if (marketingPlan != null)
				{
					SoftwareProduct targetProd = marketingPlan.TargetProd;
					if (targetProd != null)
					{
						hashSet.Add(targetProd);
					}
				}
			}
		}
		IOrderedEnumerable<SoftwareProduct> orderedEnumerable = from x in this.Products
		where Utilities.GetMonths(x.Release, time) <= 12f || x.GetCashflow().GetLastOrDefault(0f) > 1000f
		orderby x.Release descending
		select x;
		foreach (SoftwareProduct softwareProduct in orderedEnumerable)
		{
			if (!hashSet.Contains(softwareProduct))
			{
				float num = MarketingPlan.PostMarketingPrice * 0.8f * (1f - this.BusinessSavy / 2f);
				float num2 = Mathf.Max(Utilities.GetMarketingEffort(softwareProduct.DevTime), GameSettings.Instance.simulation.GetMaxAwareness(softwareProduct));
				float b = Mathf.Floor(num * Mathf.Max(0f, num2 - softwareProduct.GetRealAwareness()));
				float num3 = Mathf.Min(base.Money * 0.1f, b);
				float num4 = num3 * ((!softwareProduct.OpenSource) ? 1f : 0.5f);
				if (num3 <= 0f || !base.CanMakeTransaction(-num4))
				{
					break;
				}
				base.MakeTransaction(-num4, Company.TransactionCategory.Marketing, null);
				softwareProduct.Loss += num4;
				softwareProduct.AddToMarketing(num3 / num);
			}
		}
	}

	private bool PayLicenses(List<uint> products, SDateTime time, ref float loss)
	{
		float num = 0f;
		List<SoftwareProduct> list = (from x in products
		select GameSettings.Instance.simulation.GetProduct(x, false)).ToList<SoftwareProduct>();
		foreach (float num2 in from x in list
		where !base.OwnsLicense(x)
		select x.GetLicenseCost())
		{
			num += num2;
		}
		if (base.CanMakeTransaction(-num))
		{
			foreach (SoftwareProduct softwareProduct in list)
			{
				softwareProduct.TransferLicense(this, time);
			}
			loss = num;
			return true;
		}
		return false;
	}

	public Dictionary<string, uint> ChooseNeeds(SoftwareType type, string category, SDateTime time)
	{
		Dictionary<string, uint> dictionary = new Dictionary<string, uint>();
		Dictionary<uint, SoftwareProduct>.ValueCollection allProducts = GameSettings.Instance.simulation.GetAllProducts();
		Dictionary<string, HashSet<string>> dictionary2 = new Dictionary<string, HashSet<string>>();
		foreach (Feature feature in from x in type.Features.Values
		where x.Forced && x.IsCompatible(category)
		select x)
		{
			foreach (KeyValuePair<string, string> keyValuePair in feature.Dependencies)
			{
				HashSet<string> hashSet;
				if (!dictionary2.TryGetValue(keyValuePair.Key, out hashSet))
				{
					hashSet = new HashSet<string>();
					dictionary2[keyValuePair.Key] = hashSet;
				}
				hashSet.Add(keyValuePair.Value);
			}
		}
		string[] needs = type.GetNeeds(category);
		for (int j = 0; j < needs.Length; j++)
		{
			SimulatedCompany.<ChooseNeeds>c__AnonStoreyB <ChooseNeeds>c__AnonStoreyB = new SimulatedCompany.<ChooseNeeds>c__AnonStoreyB();
			<ChooseNeeds>c__AnonStoreyB.need = needs[j];
			SoftwareProduct[] array;
			HashSet<string> feats;
			if (dictionary2.TryGetValue(<ChooseNeeds>c__AnonStoreyB.need, out feats))
			{
				array = (from x in allProducts
				where (!x.InHouse || x.DevCompany == this) && x._type.Equals(<ChooseNeeds>c__AnonStoreyB.need) && (time - x.Release).Year < 5 && feats.All((string y) => SoftwareType.DependencyMet(y, x))
				select x).ToArray<SoftwareProduct>();
			}
			else
			{
				array = (from x in allProducts
				where (!x.InHouse || x.DevCompany == this) && x._type.Equals(<ChooseNeeds>c__AnonStoreyB.need) && (time - x.Release).Year < 5
				select x).ToArray<SoftwareProduct>();
			}
			if (array.Length == 0)
			{
				if (<ChooseNeeds>c__AnonStoreyB.need.Equals(type.OSNeed))
				{
					return null;
				}
			}
			else
			{
				dictionary.Add(<ChooseNeeds>c__AnonStoreyB.need, array.GetRandom<SoftwareProduct>().ID);
			}
		}
		return dictionary;
	}

	public uint[] ChooseOS(SoftwareType type, SDateTime time, Dictionary<string, uint> needs)
	{
		if (!type.OSSpecific)
		{
			return null;
		}
		List<SoftwareProduct> source;
		if (type.OSNeed == null)
		{
			source = (from x in GameSettings.Instance.simulation.GetAllProducts()
			where x.Sequel == null && x._type.Equals("Operating System") && (type.OSLimit == null || x._category.Equals(type.OSLimit))
			select x).ToList<SoftwareProduct>();
		}
		else
		{
			source = (from x in GameSettings.Instance.simulation.GetCompatibleOS(needs[type.OSNeed], false)
			where x.Sequel == null && (type.OSLimit == null || x._category.Equals(type.OSLimit))
			select x).ToList<SoftwareProduct>();
		}
		List<SoftwareProduct> list = (from x in source
		where (time - x.Release).Year < 5
		select x).ToList<SoftwareProduct>();
		if (list.Count > 0)
		{
			source = list;
		}
		HashSet<string> hashSet = new HashSet<string>();
		SoftwareType softwareType = GameSettings.Instance.SoftwareTypes["Operating System"];
		float num = 0f;
		Dictionary<string, float> dictionary = new Dictionary<string, float>();
		foreach (KeyValuePair<string, SoftwareCategory> keyValuePair in softwareType.Categories)
		{
			if (keyValuePair.Value.IsUnlocked(time.Year))
			{
				float num2 = softwareType.MaxDevTimeVital(keyValuePair.Key, time.Year);
				num = Mathf.Max(num2, num);
				dictionary.Add(keyValuePair.Key, num2);
			}
		}
		foreach (string key in softwareType.Categories.Keys)
		{
			float num3 = 0f;
			if (dictionary.TryGetValue(key, out num3) && Utilities.RandomValue > num3 / num)
			{
				dictionary.Remove(key);
			}
		}
		int count = dictionary.Count;
		int num4 = (type.OSLimit == null) ? Utilities.RandomRange(count, count + 2) : Utilities.RandomRange(1, 2);
		List<SoftwareProduct> list2 = new List<SoftwareProduct>(num4);
		List<SoftwareProduct> list3 = new List<SoftwareProduct>(num4);
		foreach (SoftwareProduct softwareProduct in from x in source
		orderby x.FeatureScore * x.GetWeightedQualityAddition(time, false) descending
		select x)
		{
			if (dictionary.ContainsKey(softwareProduct._category))
			{
				if (list3.Count == num4)
				{
					break;
				}
				if (type.OSLimit != null)
				{
					list3.Add(softwareProduct);
				}
				else if (list3.Count >= count || !hashSet.Contains(softwareProduct._category))
				{
					list3.Add(softwareProduct);
					hashSet.Add(softwareProduct._category);
				}
				else if (list2.Count < num4)
				{
					list2.Add(softwareProduct);
				}
			}
		}
		if (list2.Count > list3.Count && list3.Count < num4)
		{
			list3 = list2;
		}
		return (from x in list3
		select x.ID).ToArray<uint>();
	}

	public List<KeyValuePair<string, string>> Categories;

	public float AverageQuality;

	public float BusinessSavy;

	public List<SimulatedCompany.ProductPrototype> Releases = new List<SimulatedCompany.ProductPrototype>();

	public List<SimulatedCompany.ProductPrototype> ProjectQueue = new List<SimulatedCompany.ProductPrototype>();

	private readonly string _type;

	private float _focus = 1f;

	private float _workItemFocus;

	public float WorkItemCost;

	private bool _firstLaunch = true;

	private bool _autonomous = true;

	[Serializable]
	public class ProductPrototype : IStockable
	{
		public ProductPrototype(string name, string type, string category, Dictionary<string, uint> needs, uint[] os, float codeProgress, float artProgress, float codeQuality, float artQuality, float price, SimulatedCompany company, bool inHouse, float reception, uint? sequelTo, string[] feats, float loss)
		{
			this.Name = name;
			this.Type = type;
			this.Category = category;
			this.SequelTo = sequelTo;
			this.Needs = needs;
			this.OSs = os;
			this.CodeProgress = codeProgress;
			this.ArtProgress = artProgress;
			this.CodeQuality = codeQuality;
			this.ArtQuality = artQuality;
			this.Price = price;
			this.InHouse = inHouse;
			this.Reception = reception;
			this.Features = feats;
			this.Loss = loss;
			this.DevCompany = company;
			SoftwareType softwareType = GameSettings.Instance.SoftwareTypes[type];
			this.DevTime = softwareType.DevTime(this.Features, this.Category, null);
			this.Quality = Mathf.Clamp01(softwareType.FinalQualityCalc(this.CodeProgress, this.ArtProgress, this.CodeQuality, this.ArtQuality, this.Features));
		}

		public ProductPrototype()
		{
		}

		public uint SoftwareID
		{
			get
			{
				uint? swid = this.SWID;
				return (swid == null) ? 0u : swid.Value;
			}
		}

		public uint PhysicalCopies { get; set; }

		public uint ForceID()
		{
			if (this.SWID == null)
			{
				this.SWID = new uint?(GameSettings.Instance.simulation.GetID());
			}
			return this.SWID.Value;
		}

		public bool IsInDeal()
		{
			return HUD.Instance != null && HUD.Instance.dealWindow.IsInDeal(this, this.DevCompany);
		}

		public void StartDev(SDateTime start, SDateTime releaseDate)
		{
			this.DevStart = start;
			this.ReleaseDate = releaseDate;
			this.DevTimeLeft = (float)Utilities.GetMonthsFlat(start, releaseDate);
			this.DevCompany._focus -= this.DevCompany.Type.GetEffort(this.Type, this.Category);
			this.DevStarted = true;
		}

		public void RemoveProject()
		{
			if (this.DevStarted)
			{
				this.DevCompany.FreeFocus(this);
				this.DevCompany.Releases.Remove(this);
			}
			else
			{
				this.DevCompany.ProjectQueue.Remove(this);
			}
		}

		public SoftwareProduct ToProduct(SDateTime time)
		{
			this.Released = true;
			int num = Mathf.FloorToInt(Utilities.RandomGaussClamped(this.DevCompany.BusinessSavy, 0.1f) * this.DevTime * 50f);
			uint followers = (uint)(GameSettings.Instance.simulation.GetFollowerReach(this.Type, this.Category, this.Features, this.OSs) * 0.1f * this.Reception);
			bool osspecific = GameSettings.Instance.SoftwareTypes[this.Type].OSSpecific;
			string[] features = GameSettings.Instance.SoftwareTypes[this.Type].FixUp(this.Features, this.Category);
			float num2 = Mathf.Max(0.01f, 1f - this.DevTimeLeft / Mathf.Max(1f, (float)Utilities.GetMonthsFlat(this.DevStart, this.ReleaseDate)));
			string name = this.Name;
			string type = this.Type;
			string category = this.Category;
			Dictionary<string, uint> needs = this.Needs;
			uint[] os = (!osspecific) ? null : this.OSs;
			float codeProgress = num2 * this.CodeProgress;
			float artProgress = num2 * this.ArtProgress;
			float codeQuality = this.CodeQuality;
			float artQuality = this.ArtQuality;
			float price = this.Price;
			SDateTime devStart = this.DevStart;
			int bugs = num;
			bool inHouse = this.InHouse;
			Company devCompany = this.DevCompany;
			uint? sequelTo = this.SequelTo;
			uint? swid = this.SWID;
			SoftwareProduct softwareProduct = new SoftwareProduct(name, type, category, needs, os, codeProgress, artProgress, codeQuality, artQuality, price, devStart, time, bugs, inHouse, devCompany, sequelTo, (swid == null) ? GameSettings.Instance.simulation.GetID() : swid.Value, this.Loss, features, null, followers, null);
			softwareProduct.PhysicalCopies += this.PhysicalCopies;
			return softwareProduct;
		}

		public string GetName()
		{
			return this.Name;
		}

		public void AddLoss(float cost)
		{
			this.Loss += cost;
		}

		public Company GetCompany()
		{
			return this.DevCompany;
		}

		public readonly string Name;

		public readonly string Type;

		public readonly string Category;

		public readonly uint? SequelTo;

		public readonly Dictionary<string, uint> Needs;

		public readonly uint[] OSs;

		public readonly string[] Features;

		public readonly float Price;

		public float CodeProgress;

		public float ArtProgress;

		public float CodeQuality;

		public float ArtQuality;

		public float Quality;

		public float Loss;

		public SDateTime DevStart;

		public readonly bool InHouse;

		public readonly float Reception;

		public readonly float DevTime;

		public float DevTimeLeft;

		public SDateTime ReleaseDate;

		public SimulatedCompany DevCompany;

		public uint? SWID;

		public bool Released;

		public bool DevStarted;

		public bool PrintDeal;
	}
}
