﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Legend
{
	public static void DrawLegend(IEnumerable<string> names, Rect rect, GUIStyle label, GUIStyle box)
	{
		int num = 0;
		int num2 = Mathf.FloorToInt(rect.height / 24f);
		int num3 = Mathf.CeilToInt((float)names.Count<string>() / (float)num2);
		float num4 = rect.width / (float)num3;
		foreach (string text in names)
		{
			float num5 = rect.x + (float)(num / num2) * num4;
			float y = rect.y + (float)(24 * (num % num2));
			GUI.color = HUD.ThemeColors[num % HUD.ThemeColors.Length];
			GUI.Box(new Rect(num5, y, 24f, 24f), string.Empty, box);
			GUI.color = Color.white;
			GUI.Label(new Rect(num5 + 24f, y, num4, 24f), text, label);
			num++;
		}
	}
}
