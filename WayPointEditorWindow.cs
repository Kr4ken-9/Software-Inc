﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WayPointEditorWindow : MonoBehaviour
{
	public void Bake()
	{
		Dictionary<WayPointEditorWindow.WayPoint, float> dictionary = new Dictionary<WayPointEditorWindow.WayPoint, float>();
		float num = 0f;
		foreach (WayPointEditorWindow.WayPoint wayPoint in this.WayPointList.Items.OfType<WayPointEditorWindow.WayPoint>())
		{
			dictionary[wayPoint] = num;
			num += wayPoint.Time;
		}
		Camera mainCam = CameraScript.Instance.mainCam;
		mainCam.transform.SetParent(null);
		ListenerScript componentInChildren = CameraScript.Instance.GetComponentInChildren<ListenerScript>(true);
		componentInChildren.transform.SetParent(mainCam.transform);
		componentInChildren.transform.localPosition = Vector3.zero;
		componentInChildren.transform.localRotation = Quaternion.identity;
		componentInChildren.enabled = false;
		Animation animation = mainCam.gameObject.AddComponent<Animation>();
		AnimationClip animationClip = new AnimationClip
		{
			legacy = true,
			wrapMode = WrapMode.Loop
		};
		AnimationCurve animationCurve = new AnimationCurve();
		Dictionary<string, Type> dictionary2 = new Dictionary<string, Type>
		{
			{
				"localPosition.x",
				typeof(Transform)
			},
			{
				"localPosition.y",
				typeof(Transform)
			},
			{
				"localPosition.z",
				typeof(Transform)
			},
			{
				"localRotation.x",
				typeof(Transform)
			},
			{
				"localRotation.y",
				typeof(Transform)
			},
			{
				"localRotation.z",
				typeof(Transform)
			},
			{
				"localRotation.w",
				typeof(Transform)
			},
			{
				"field of view",
				typeof(Camera)
			}
		};
		Dictionary<string, Func<WayPointEditorWindow.WayPoint, float>> dictionary3 = new Dictionary<string, Func<WayPointEditorWindow.WayPoint, float>>();
		dictionary3.Add("localPosition.x", (WayPointEditorWindow.WayPoint x) => x.Position.x);
		dictionary3.Add("localPosition.y", (WayPointEditorWindow.WayPoint x) => x.Position.y);
		dictionary3.Add("localPosition.z", (WayPointEditorWindow.WayPoint x) => x.Position.z);
		dictionary3.Add("localRotation.x", (WayPointEditorWindow.WayPoint x) => x.Rotation.x);
		dictionary3.Add("localRotation.y", (WayPointEditorWindow.WayPoint x) => x.Rotation.y);
		dictionary3.Add("localRotation.z", (WayPointEditorWindow.WayPoint x) => x.Rotation.z);
		dictionary3.Add("localRotation.w", (WayPointEditorWindow.WayPoint x) => x.Rotation.w);
		dictionary3.Add("field of view", (WayPointEditorWindow.WayPoint x) => x.FOV);
		Dictionary<string, Func<WayPointEditorWindow.WayPoint, float>> dictionary4 = dictionary3;
		using (Dictionary<string, Func<WayPointEditorWindow.WayPoint, float>>.Enumerator enumerator2 = dictionary4.GetEnumerator())
		{
			while (enumerator2.MoveNext())
			{
				KeyValuePair<string, Func<WayPointEditorWindow.WayPoint, float>> curve = enumerator2.Current;
				Type type = dictionary2[curve.Key];
				AnimationCurve animationCurve2 = new AnimationCurve((from x in dictionary
				select new Keyframe(x.Value, curve.Value(x.Key))).ToArray<Keyframe>());
				for (int i = 0; i < dictionary.Count; i++)
				{
					animationCurve2.SmoothTangents(i, 1f);
				}
				animationClip.SetCurve(string.Empty, type, curve.Key, animationCurve2);
			}
		}
		animation.AddClip(animationClip, "BakeAnim");
		animation.Play("BakeAnim");
	}

	public void ToggleBake()
	{
		Camera mainCam = CameraScript.Instance.mainCam;
		Animation component = mainCam.gameObject.GetComponent<Animation>();
		if (component != null)
		{
			if (component.isPlaying)
			{
				component.Stop();
			}
			else
			{
				component.Play("BakeAnim");
			}
		}
	}

	public void ClearBake()
	{
		Camera mainCam = CameraScript.Instance.mainCam;
		ListenerScript componentInChildren = mainCam.GetComponentInChildren<ListenerScript>(true);
		componentInChildren.transform.SetParent(CameraScript.Instance.transform);
		componentInChildren.enabled = true;
		UnityEngine.Object.Destroy(mainCam.gameObject.GetComponent<Animation>());
		mainCam.transform.SetParent(CameraScript.Instance.transform);
	}

	private void LateUpdate()
	{
		if (this.FreezeFrame)
		{
			if (this.CurrentWayPoint < 0 || this.CurrentWayPoint > this.WayPointList.Items.Count - 1)
			{
				this.FreezeFrame = false;
				return;
			}
			Camera mainCam = CameraScript.Instance.mainCam;
			WayPointEditorWindow.WayPoint wayPoint = (WayPointEditorWindow.WayPoint)this.WayPointList.Items[this.CurrentWayPoint];
			mainCam.transform.position = wayPoint.Position;
			mainCam.transform.rotation = wayPoint.Rotation;
			mainCam.fieldOfView = wayPoint.FOV;
		}
		else
		{
			if (this.IsPlaying)
			{
				if (this.CurrentWayPoint < 0 || this.CurrentWayPoint > this.WayPointList.Items.Count - 1)
				{
					this.IsPlaying = false;
					return;
				}
				Camera mainCam2 = CameraScript.Instance.mainCam;
				WayPointEditorWindow.WayPoint wayPoint2 = (WayPointEditorWindow.WayPoint)this.WayPointList.Items[this.CurrentWayPoint];
				if (this.CurrentWayPoint == this.WayPointList.Items.Count - 1)
				{
					mainCam2.transform.position = wayPoint2.Position;
					mainCam2.transform.rotation = wayPoint2.Rotation;
					mainCam2.fieldOfView = wayPoint2.FOV;
				}
				else
				{
					this.CurrentWayPointPos += Time.deltaTime / wayPoint2.Time;
					if (this.CurrentWayPointPos >= 1f)
					{
						this.CurrentWayPoint++;
						this.CurrentWayPointPos %= 1f;
					}
					wayPoint2 = (WayPointEditorWindow.WayPoint)this.WayPointList.Items[this.CurrentWayPoint];
					if (this.CurrentWayPoint == this.WayPointList.Items.Count - 1)
					{
						mainCam2.transform.position = wayPoint2.Position;
						mainCam2.transform.rotation = wayPoint2.Rotation;
						mainCam2.fieldOfView = wayPoint2.FOV;
					}
					else
					{
						WayPointEditorWindow.WayPoint wayPoint3 = (WayPointEditorWindow.WayPoint)this.WayPointList.Items[this.CurrentWayPoint + 1];
						mainCam2.transform.position = Vector3.Lerp(wayPoint2.Position, wayPoint3.Position, this.CurrentWayPointPos);
						mainCam2.transform.rotation = Quaternion.Lerp(wayPoint2.Rotation, wayPoint3.Rotation, this.CurrentWayPointPos);
						mainCam2.fieldOfView = Mathf.Lerp(wayPoint2.FOV, wayPoint3.FOV, this.CurrentWayPointPos);
					}
				}
			}
			if (this.IsPlaying)
			{
				if (Input.GetKeyDown(KeyCode.Backspace))
				{
					this.IsPlaying = false;
				}
			}
			else if (Input.GetKeyDown(KeyCode.Backspace))
			{
				this.Play();
			}
		}
		if (Input.GetKeyDown(KeyCode.KeypadMultiply))
		{
			int num = this.WayPointList.Selected.First<int>();
			if (this.CurrentWayPoint != num)
			{
				this.FreezeFrame = true;
				this.CurrentWayPoint = num;
			}
			else
			{
				this.FreezeFrame = !this.FreezeFrame;
			}
		}
		if (Input.GetKeyDown(KeyCode.KeypadPlus))
		{
			int num2 = this.WayPointList.Selected.First<int>();
			if (num2 < this.WayPointList.Items.Count - 1)
			{
				object item = this.WayPointList.Items[num2];
				this.WayPointList.Items.RemoveAt(num2);
				this.WayPointList.Items.Insert(num2 + 1, item);
			}
		}
		if (Input.GetKeyDown(KeyCode.KeypadPlus))
		{
			int num3 = this.WayPointList.Selected.First<int>();
			if (num3 > 0)
			{
				object item2 = this.WayPointList.Items[num3];
				this.WayPointList.Items.RemoveAt(num3);
				this.WayPointList.Items.Insert(num3 - 1, item2);
			}
		}
		if (Input.GetKeyDown(KeyCode.Return))
		{
			this.CapturePos(false);
		}
		if (Input.GetKeyDown(KeyCode.KeypadEnter))
		{
			this.CapturePos(true);
		}
	}

	public void Play()
	{
		if (this.WayPointList.Items.Count > 0)
		{
			this.CurrentWayPoint = 0;
			this.CurrentWayPointPos = 0f;
			this.IsPlaying = true;
		}
	}

	private void Awake()
	{
		WayPointEditorWindow.Instance = this;
	}

	private void OnDestroy()
	{
		WayPointEditorWindow.Instance = null;
	}

	public void CapturePos(bool replace)
	{
		Vector3 position = CameraScript.Instance.mainCam.transform.position;
		Quaternion rotation = CameraScript.Instance.mainCam.transform.rotation;
		float fieldOfView = CameraScript.Instance.mainCam.fieldOfView;
		WayPointEditorWindow.WayPoint item = new WayPointEditorWindow.WayPoint
		{
			Position = position,
			Rotation = rotation,
			FOV = fieldOfView,
			Time = 1f
		};
		if (replace)
		{
			this.WayPointList.Items.RemoveAt(this.CurrentWayPoint);
			this.WayPointList.Items.Insert(this.CurrentWayPoint, item);
		}
		else
		{
			this.WayPointList.Items.Add(item);
		}
	}

	public GUIListView WayPointList;

	[NonSerialized]
	public int CurrentWayPoint;

	[NonSerialized]
	public float CurrentWayPointPos;

	public bool IsPlaying;

	public bool FreezeFrame;

	public GUIWindow Window;

	public static WayPointEditorWindow Instance;

	public class WayPoint
	{
		public Vector3 Position;

		public Quaternion Rotation;

		public float FOV;

		public float Time;
	}
}
