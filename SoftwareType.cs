﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class SoftwareType
{
	public SoftwareType(string name, string category, string description, string osneed, string osLimit, float random, bool OS, bool oneClient, bool inHouse, Dictionary<string, Feature> feat, int? unlock, float? idealPrice, Dictionary<string, SoftwareCategory> categories, bool modded)
	{
		this.Name = name;
		this.Category = category;
		this.Description = description;
		this.OSNeed = osneed;
		this.OSLimit = osLimit;
		this.RandomFactor = random;
		this.OSSpecific = OS;
		this.OneClient = oneClient;
		this.InHouse = inHouse;
		this.Features = feat;
		this.Unlock = unlock;
		this.IdealPrice = idealPrice;
		this._featureUpgrades = null;
		this.Categories = categories.ToDictionary((KeyValuePair<string, SoftwareCategory> x) => x.Key, (KeyValuePair<string, SoftwareCategory> x) => x.Value);
		this.PickRoyalities();
		this.Modded = modded;
	}

	public SoftwareType()
	{
	}

	public SoftwareType(XMLParser.XMLNode node, bool modded)
	{
		SoftwareType $this = this;
		this.Name = node.GetNode("Name", true).Value;
		this.Category = node.GetNode("Category", true).Value;
		this.OSNeed = node.GetNodeValue("OSNeed", null);
		this.Description = node.GetNode("Description", true).Value;
		this.RandomFactor = node.GetNode("Random", true).Value.ConvertToFloat("Random");
		this.IdealPrice = node.GetNodeValueOptional<float>("IdealPrice");
		string defaultGen = null;
		XMLParser.XMLNode node2 = node.GetNode("NameGenerator", false);
		if (node2 != null)
		{
			defaultGen = node2.Value;
		}
		if (!node.Contains("Categories"))
		{
			float popularity = node.GetNode("Popularity", true).Value.ConvertToFloat("Popularity");
			float retention = node.GetNode("Retention", true).Value.ConvertToFloat("Retention");
			float iterative = node.GetNode("Iterative", true).Value.ConvertToFloat("Iterative");
			this.Categories = new Dictionary<string, SoftwareCategory>
			{
				{
					"Default",
					SoftwareCategory.GetDefault(popularity, retention, iterative, defaultGen)
				}
			};
		}
		else
		{
			this.Categories = (from x in node.GetNode("Categories", true).GetNodes("Category", true)
			select new SoftwareCategory(x, defaultGen)).ToDictionary((SoftwareCategory x) => x.Name, (SoftwareCategory x) => x);
		}
		this.OSSpecific = node.GetNode("OSSpecific", true).Value.ConvertToBool("OSSpecific");
		if (this.OSSpecific)
		{
			XMLParser.XMLNode node3 = node.GetNode("OSLimit", false);
			this.OSLimit = ((node3 != null) ? node3.Value : null);
		}
		this.OneClient = node.GetNode("OneClient", true).Value.ConvertToBool("OneClient");
		this.InHouse = node.GetNode("InHouse", true).Value.ConvertToBool("InHouse");
		this.Features = (from x in node.GetNode("Features", true).GetNodes("Feature", true)
		select new Feature(x, $this.Name, false)).ToDictionary((Feature x) => x.Name, (Feature x) => x);
		this.Unlock = null;
		XMLParser.XMLNode node4 = node.GetNode("Unlock", false);
		if (node4 != null)
		{
			this.Unlock = new int?(node4.Value.ConvertToInt("Unlock"));
		}
		if (this.Categories.Values.All((SoftwareCategory x) => x.Unlock != null))
		{
			int num = (from x in this.Categories.Values
			where x.Unlock != null
			select x).Min((SoftwareCategory x) => x.Unlock.Value);
			if (this.Unlock != null)
			{
				num = Mathf.Min(this.Unlock.Value, num);
			}
			this.Unlock = new int?(num);
		}
		this._featureUpgrades = null;
		this.PickRoyalities();
		this.Modded = modded;
	}

	public Dictionary<string, string[]> FeatureUpgrades
	{
		get
		{
			if (this._featureUpgrades == null)
			{
				this.InitUpgrades();
			}
			return this._featureUpgrades;
		}
	}

	private void InitUpgrades()
	{
		Dictionary<string, List<string>> dictionary = this.Features.ToDictionary((KeyValuePair<string, Feature> x) => x.Key, (KeyValuePair<string, Feature> x) => new List<string>());
		foreach (Feature feature in this.Features.Values)
		{
			string from = feature.From;
			dictionary[feature.Name].Add(feature.Name);
			while (from != null)
			{
				dictionary[from].Add(feature.Name);
				from = this.Features[from].From;
			}
		}
		this._featureUpgrades = dictionary.ToDictionary((KeyValuePair<string, List<string>> x) => x.Key, (KeyValuePair<string, List<string>> x) => x.Value.ToArray());
	}

	public string[] GetNeeds(string category)
	{
		HashSet<string> hashSet = new HashSet<string>();
		if (this.OSNeed != null)
		{
			hashSet.Add(this.OSNeed);
		}
		foreach (Feature feature in from x in this.Features.Values
		where x.IsCompatible(category)
		select x)
		{
			foreach (KeyValuePair<string, string> keyValuePair in feature.Dependencies)
			{
				hashSet.Add(keyValuePair.Key);
			}
		}
		hashSet.Remove(this.Name);
		hashSet.Remove("Operating System");
		return hashSet.ToArray<string>();
	}

	public string[] GetNeeds(IEnumerable<string> features, string category)
	{
		HashSet<string> hashSet = new HashSet<string>();
		if (this.OSNeed != null)
		{
			hashSet.Add(this.OSNeed);
		}
		foreach (Feature feature in from x in this.Features.Values
		where x.Forced && x.IsCompatible(category)
		select x)
		{
			foreach (KeyValuePair<string, string> keyValuePair in feature.Dependencies)
			{
				hashSet.Add(keyValuePair.Key);
			}
		}
		foreach (Feature feature2 in from x in features
		select this.Features[x])
		{
			foreach (KeyValuePair<string, string> keyValuePair2 in feature2.Dependencies)
			{
				hashSet.Add(keyValuePair2.Key);
			}
		}
		hashSet.Remove(this.Name);
		hashSet.Remove("Operating System");
		return hashSet.ToArray<string>();
	}

	private void PickRoyalities()
	{
		Dictionary<Feature, float> dictionary = (from x in this.Features.Values
		where x.Research != null
		select x).ToDictionary((Feature x) => x, (Feature x) => x.DevTime * (float)((!x.Vital) ? 1 : 2));
		if (dictionary.Count > 0)
		{
			float num = dictionary.Values.Sum();
			foreach (KeyValuePair<Feature, float> keyValuePair in dictionary)
			{
				keyValuePair.Key.Royalty = keyValuePair.Value / num * SoftwareType.RoyaltySum;
			}
		}
	}

	public float? GetIdealPrice(SoftwareCategory cat)
	{
		float? idealPrice = cat.IdealPrice;
		return (idealPrice == null) ? this.IdealPrice : idealPrice;
	}

	public SoftwareType Clone(Feature[] Base, bool modded, params SoftwareTypeOverride[] Overrides)
	{
		Dictionary<string, Feature> dictionary = new Dictionary<string, Feature>();
		bool? flag = (from x in Overrides
		where x.OneClient != null
		select x.OneClient).LastOrDefault<bool?>();
		flag = new bool?((flag == null) ? this.OneClient : flag.Value);
		float? num = (from x in Overrides
		where x.IdealPrice != null
		select x.IdealPrice).LastOrDefault<float?>();
		num = ((num == null) ? this.IdealPrice : num);
		if (Overrides.Any((SoftwareTypeOverride x) => x.Features != null))
		{
			foreach (SoftwareTypeOverride softwareTypeOverride in from x in Overrides
			where x.Features != null
			select x)
			{
				foreach (Feature feature in softwareTypeOverride.Features)
				{
					dictionary[feature.Name] = new Feature(feature);
				}
			}
		}
		else
		{
			foreach (KeyValuePair<string, Feature> keyValuePair in this.Features)
			{
				dictionary[keyValuePair.Key] = new Feature(keyValuePair.Value);
			}
		}
		if (Base != null && !flag.Value)
		{
			foreach (Feature feature2 in Base)
			{
				if (!dictionary.ContainsKey(feature2.Name))
				{
					dictionary[feature2.Name] = feature2;
				}
			}
		}
		string text = (from x in Overrides
		where x.Category != null
		select x.Category).LastOrDefault<string>();
		text = (text ?? this.Category);
		string text2 = (from x in Overrides
		where x.Description != null
		select x.Description).LastOrDefault<string>();
		text2 = (text2 ?? this.Description);
		string text3 = (from x in Overrides
		where x.OSNeed != null
		select x.OSNeed).LastOrDefault<string>();
		text3 = (text3 ?? this.OSNeed);
		float? num2 = (from x in Overrides
		where x.RandomFactor != null
		select x.RandomFactor).LastOrDefault<float?>();
		num2 = new float?((num2 == null) ? this.RandomFactor : num2.Value);
		bool? flag2 = (from x in Overrides
		where x.OSSpecific != null
		select x.OSSpecific).LastOrDefault<bool?>();
		flag2 = new bool?((flag2 == null) ? this.OSSpecific : flag2.Value);
		string oslimit = this.OSLimit;
		if (flag2.Value)
		{
			if (Overrides.Any((SoftwareTypeOverride x) => x.OSLimit != null))
			{
				oslimit = Overrides.Last((SoftwareTypeOverride x) => x.OSLimit != null).OSLimit;
			}
		}
		bool? flag3 = (from x in Overrides
		where x.InHouse != null
		select x.InHouse).LastOrDefault<bool?>();
		flag3 = new bool?((flag3 == null) ? this.InHouse : flag3.Value);
		string text4 = (from x in Overrides
		where x.NameGenerator != null
		select x.NameGenerator).LastOrDefault<string>();
		int? num3 = (from x in Overrides
		where x.Unlock != null
		select x.Unlock).LastOrDefault<int?>();
		num3 = ((num3 == null) ? this.Unlock : num3);
		Dictionary<string, SoftwareCategory> dictionary2 = this.Categories.ToDictionary((KeyValuePair<string, SoftwareCategory> x) => x.Key, (KeyValuePair<string, SoftwareCategory> x) => new SoftwareCategory(x.Value));
		string text5;
		if ((text5 = text4) == null)
		{
			text5 = (from x in dictionary2
			select x.Value.GetNameGenName()).First<string>();
		}
		string text6 = text5;
		if (Overrides.Any((SoftwareTypeOverride x) => x.Categories != null))
		{
			dictionary2.Clear();
			foreach (SoftwareTypeOverride softwareTypeOverride2 in Overrides)
			{
				if (softwareTypeOverride2.Categories != null)
				{
					foreach (SoftwareCategory softwareCategory in softwareTypeOverride2.Categories)
					{
						string text7 = softwareTypeOverride2.CategoryRngs[softwareCategory.Name];
						dictionary2[softwareCategory.Name] = new SoftwareCategory(softwareCategory, text7 ?? text6);
					}
				}
			}
		}
		return new SoftwareType(this.Name, text, text2, text3, oslimit, num2.Value, flag2.Value, flag.Value, flag3.Value, dictionary, num3, num, dictionary2, modded);
	}

	public bool IsUnlocked(int year)
	{
		return this.Unlock == null || year >= this.Unlock.Value - SDateTime.BaseYear;
	}

	public bool Equals(SoftwareType other)
	{
		return other != null && this.Name.Equals(other.Name);
	}

	public override bool Equals(object obj)
	{
		SoftwareType softwareType = obj as SoftwareType;
		return softwareType != null && this.Equals(softwareType);
	}

	public override int GetHashCode()
	{
		return base.GetHashCode();
	}

	public override string ToString()
	{
		return this.Name;
	}

	public int[] GetOptimalEmployeeCount(float devTime, float artRatio)
	{
		return new int[]
		{
			(artRatio != 0f) ? GameData.GetOptimalEmployees(Mathf.Max(1, Mathf.RoundToInt(devTime * artRatio * SoftwareType.DesignRatio))) : 0,
			GameData.GetOptimalEmployees(Mathf.Max(1, Mathf.RoundToInt(devTime * (1f - SoftwareType.DesignRatio))))
		};
	}

	public static float GetEmployeeCountEffect(int employees, float devTime, bool design)
	{
		float num = (!design) ? (1f - SoftwareType.DesignRatio) : SoftwareType.DesignRatio;
		return GameData.GetProjectEffectiveness(employees, Mathf.Max(1, Mathf.RoundToInt(devTime * num)));
	}

	public float FeatureScore(string[] features)
	{
		List<string> features2 = this.AddPreviousFeatures(features, true);
		return this.DevTime(features2, null, null);
	}

	private List<string> AddPreviousFeatures(string[] features, bool onlyVital)
	{
		object previousFeaturesCache = SoftwareType.PreviousFeaturesCache;
		List<string> result;
		lock (previousFeaturesCache)
		{
			SoftwareType.PreviousFeaturesCache.Clear();
			SoftwareType.PreviousFeaturesCache.AddRange(features);
			foreach (string text in features)
			{
				Feature feature = this.Features[text];
				for (text = feature.From; text != null; text = feature.From)
				{
					feature = this.Features[text];
					if (!onlyVital || feature.Forced || feature.Vital)
					{
						SoftwareType.PreviousFeaturesCache.Add(text);
					}
				}
			}
			result = SoftwareType.PreviousFeaturesCache.ToList<string>();
		}
		return result;
	}

	public float MaxQuality(Dictionary<string, uint> needs, IList<string> features, string category)
	{
		Dictionary<string, float> dictionary = new Dictionary<string, float>();
		float num = this.DevTime(features, null, null);
		for (int i = 0; i < features.Count; i++)
		{
			Feature feature = this.Features[features[i]];
			for (int j = 0; j < feature.Dependencies.Length; j++)
			{
				KeyValuePair<string, string> keyValuePair = feature.Dependencies[j];
				if (!dictionary.ContainsKey(keyValuePair.Key))
				{
					dictionary[keyValuePair.Key] = 0f;
				}
				Dictionary<string, float> dictionary2;
				string key;
				(dictionary2 = dictionary)[key = keyValuePair.Key] = dictionary2[key] + feature.DevTime / num;
			}
		}
		if (this.OSNeed != null)
		{
			dictionary[this.OSNeed] = 1f;
		}
		float num2 = 1f;
		float num3 = 1f;
		foreach (KeyValuePair<string, uint> keyValuePair2 in needs)
		{
			if (keyValuePair2.Value != 0u)
			{
				SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(keyValuePair2.Value, false);
				float num4 = 0f;
				if (dictionary.TryGetValue(keyValuePair2.Key, out num4))
				{
					num2 += product.RealQuality * num4;
					num3 += num4;
				}
			}
		}
		num2 /= num3;
		return num2 * 1.05f;
	}

	public float DevTime(IList<string> features, string category, SoftwareProduct sequelTo = null)
	{
		float num = (category != null) ? this.Categories[category].TimeScale : 1f;
		if (sequelTo == null)
		{
			float num2 = 0f;
			for (int i = 0; i < features.Count; i++)
			{
				num2 += this.Features[features[i]].DevTime * num;
			}
			return num2;
		}
		float num3 = 0f;
		for (int j = 0; j < features.Count; j++)
		{
			string text = features[j];
			num3 += ((!sequelTo.Features.Contains(text)) ? 1f : SoftwareType.SequelBoost) * this.Features[text].DevTime * num;
		}
		return num3;
	}

	public string[] MaxFeat(string cat, int year, bool vital)
	{
		string[] features = (from x in this.Features.Values
		where (!vital || x.Vital || x.Forced) && x.IsUnlocked(year, cat) && x.IsCompatible(cat)
		select x.Name).ToArray<string>();
		return this.FixUp(features, cat);
	}

	public float MaxDevTime(string cat, int year)
	{
		string[] features = (from x in this.Features.Values
		where x.IsUnlocked(year, cat) && x.IsCompatible(cat)
		select x.Name).ToArray<string>();
		features = this.FixUp(features, cat);
		return this.DevTime(features, cat, null);
	}

	public float MaxDevTimeVital(string cat, int year)
	{
		string[] features = (from x in this.Features.Values
		where (x.Vital || x.Forced) && x.IsUnlocked(year, cat) && x.IsCompatible(cat)
		select x.Name).ToArray<string>();
		features = this.FixUp(features, cat);
		return this.DevTime(features, cat, null);
	}

	public float CodeArtRatio(IList<string> features)
	{
		float num = 0f;
		float num2 = 0f;
		for (int i = 0; i < features.Count; i++)
		{
			Feature feature = this.Features[features[i]];
			num += feature.CodeArtRatio * feature.DevTime;
			num2 += feature.DevTime;
		}
		return (num2 != 0f) ? (num / num2) : 1f;
	}

	public float[] GetBalance(IList<string> features)
	{
		float num = this.DevTime(features, null, null);
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		for (int i = 0; i < features.Count; i++)
		{
			Feature feature = this.Features[features[i]];
			num2 += feature.Innovation * feature.DevTime;
			num3 += feature.Stability * feature.DevTime;
			num4 += feature.Usability * feature.DevTime;
		}
		return new float[]
		{
			num2 / num,
			num3 / num,
			num4 / num
		};
	}

	private string[] FeatureByDependency(SDateTime time, string swCategory)
	{
		List<Feature> list = (from x in this.Features.Values
		where x.IsUnlocked(swCategory, time.Year, GameSettings.Instance.SoftwareTypes) && x.IsCompatible(swCategory)
		orderby (!x.Vital) ? 2f : Utilities.RandomValue
		select x).ToList<Feature>();
		List<string> list2 = new List<string>();
		int num = 0;
		while (list.Count > 0 && num < list.Count)
		{
			Feature feature = list[num];
			bool flag = true;
			foreach (KeyValuePair<string, string> keyValuePair in feature.Dependencies)
			{
				if (keyValuePair.Key.Equals(this.Name) && !list2.Contains(keyValuePair.Value))
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				list.Remove(feature);
				list2.Add(feature.Name);
				num = 0;
			}
			else
			{
				num++;
			}
		}
		return list2.ToArray();
	}

	public string[] GenerateFeatures(float upgradeChance, string swCategory, Dictionary<string, uint> needs, uint[] OS, SDateTime date, bool online = true)
	{
		string[] baseFeatures = this.FixUp(new string[0], swCategory);
		List<string> list = new List<string>(baseFeatures);
		foreach (string text in from x in this.FeatureByDependency(date, swCategory)
		where !baseFeatures.Contains(x)
		select x)
		{
			if (this.Features[text].ServerRequirement <= 0f || online)
			{
				list.Add(text);
				string[] collection = this.FixUp(list, swCategory, needs, OS);
				if (Utilities.RandomValue > upgradeChance && !this.Features[text].Vital)
				{
					list.Remove(text);
					return this.FixUp(list, swCategory, needs, OS);
				}
				list.Clear();
				list.AddRange(collection);
			}
		}
		return list.ToArray();
	}

	public static bool DependencyMet(string feature, SoftwareProduct sw)
	{
		string[] array = sw.Type.FeatureUpgrades[feature];
		for (int i = 0; i < array.Length; i++)
		{
			for (int j = 0; j < sw.Features.Length; j++)
			{
				if (array[i].Equals(sw.Features[j]))
				{
					return true;
				}
			}
		}
		return false;
	}

	public static bool DependencyMet(string feature, SoftwareType type, List<string> features)
	{
		string[] array = type.FeatureUpgrades[feature];
		for (int i = 0; i < array.Length; i++)
		{
			for (int j = 0; j < features.Count; j++)
			{
				if (array[i].Equals(features[j]))
				{
					return true;
				}
			}
		}
		return false;
	}

	public string[] FixUp(IEnumerable<string> features, string swCategory, Dictionary<string, uint> needs, uint[] OS)
	{
		Dictionary<string, Feature> feat = this.Features;
		List<string> list = (from x in features
		where feat.ContainsKey(x) && feat[x].IsCompatible(swCategory)
		select x).ToList<string>();
		foreach (Feature feature in from x in this.Features.Values
		where x.Forced && x.IsCompatible(swCategory)
		select x)
		{
			if (!list.Contains(feature.Name))
			{
				list.Add(feature.Name);
			}
		}
		foreach (string text in list.ToList<string>())
		{
			KeyValuePair<string, string>[] dependencies = feat[text].Dependencies;
			bool flag = true;
			foreach (KeyValuePair<string, string> keyValuePair in dependencies)
			{
				if (keyValuePair.Key.Equals(this.Name))
				{
					if (!SoftwareType.DependencyMet(keyValuePair.Value, this, list))
					{
						flag = false;
						break;
					}
				}
				else if (keyValuePair.Key.Equals("Operating System"))
				{
					if (OS == null || OS.Length <= 0)
					{
						flag = false;
						break;
					}
					foreach (SoftwareProduct sw in from x in OS
					select GameSettings.Instance.simulation.GetProduct(x, true))
					{
						if (!SoftwareType.DependencyMet(keyValuePair.Value, sw))
						{
							flag = false;
							break;
						}
					}
				}
				else
				{
					if (needs == null || !needs.ContainsKey(keyValuePair.Key))
					{
						flag = false;
						break;
					}
					uint num = needs[keyValuePair.Key];
					if (num == 0u || !SoftwareType.DependencyMet(keyValuePair.Value, GameSettings.Instance.simulation.GetProduct(num, false)))
					{
						flag = false;
						break;
					}
				}
			}
			if (!flag)
			{
				list.Remove(text);
			}
		}
		foreach (Feature feature2 in from x in this.Features.Values
		where x.From != null
		select x)
		{
			if (list.Contains(feature2.Name))
			{
				for (string from = feature2.From; from != null; from = feat[from].From)
				{
					list.Remove(from);
				}
			}
		}
		return list.ToArray();
	}

	public string[] FixUp(IEnumerable<string> features, string swCategory)
	{
		Dictionary<string, Feature> feat = this.Features;
		List<string> list = (from x in features
		where feat.ContainsKey(x) && feat[x].IsCompatible(swCategory)
		select x).ToList<string>();
		foreach (Feature feature in from x in this.Features.Values
		where x.Forced && x.IsCompatible(swCategory)
		select x)
		{
			if (!list.Contains(feature.Name))
			{
				list.Add(feature.Name);
			}
		}
		foreach (Feature feature2 in from x in this.Features.Values
		where x.From != null
		select x)
		{
			if (list.Contains(feature2.Name))
			{
				for (string from = feature2.From; from != null; from = feat[from].From)
				{
					list.Remove(from);
				}
			}
		}
		return list.ToArray();
	}

	public float GetServerReq(IEnumerable<string> feat)
	{
		return (from x in feat
		select this.Features[x].ServerRequirement).Sum();
	}

	public static string GetQualityLabel(float quality)
	{
		return ("QualityAmount" + Mathf.FloorToInt(SoftwareType.QualityLevel.Evaluate(quality))).Loc();
	}

	public static string GetPerceptionLabel(float perception)
	{
		return ("PerceptionAmount" + (Mathf.Min(SoftwareType.PerceptionLabelCount - 1, Mathf.FloorToInt(perception * (float)(SoftwareType.PerceptionLabelCount - 1))) + 1)).Loc();
	}

	public static string GetAwarenessLabel(float popularity)
	{
		return (popularity >= 0.0001f) ? ("MarketingAmount" + (Mathf.Clamp(Mathf.FloorToInt(Mathf.Sqrt(popularity) * (float)(SoftwareType.AwarenessLabelCount - 1)), 0, SoftwareType.AwarenessLabelCount - 2) + 2)).Loc() : "MarketingAmount1".Loc();
	}

	public Feature[] FeaturesFromSpecialization(string spec, bool code)
	{
		return (from x in this.Features.Values
		where spec.Equals(x.GetCategory(code))
		select x).ToArray<Feature>();
	}

	public Dictionary<string, float> GetSpecializationMonths(IEnumerable<string> features, string category, uint[] OSs, SoftwareProduct sequelTo)
	{
		Dictionary<string, float> dictionary = new Dictionary<string, float>();
		SoftwareCategory orNull = this.Categories.GetOrNull(category);
		float num = (orNull == null) ? 1f : orNull.TimeScale;
		foreach (string text in features)
		{
			float num2 = (sequelTo == null || !sequelTo.Features.Contains(text)) ? 1f : SoftwareType.SequelBoost;
			Feature feature = this.Features[text];
			if (feature.OneCategory())
			{
				string category2 = feature.GetCategory(true);
				dictionary.AddUp(category2, num2 * feature.DevTime * num);
			}
			else
			{
				if (feature.CodeArtRatio > 0f)
				{
					string category3 = feature.GetCategory(true);
					dictionary.AddUp(category3, num2 * feature.DevTime * num * feature.CodeArtRatio);
				}
				if (feature.CodeArtRatio < 1f)
				{
					string category4 = feature.GetCategory(false);
					dictionary.AddUp(category4, num2 * feature.DevTime * num * (1f - feature.CodeArtRatio));
				}
			}
		}
		return dictionary;
	}

	public Dictionary<string, float[]> GetSpecializationMonthsCodeArt(IEnumerable<string> features, string category, uint[] OSs, SoftwareProduct sequelTo)
	{
		Dictionary<string, float[]> dictionary = new Dictionary<string, float[]>();
		float timeScale = this.Categories[category].TimeScale;
		foreach (string text in features)
		{
			float num = (sequelTo == null || !sequelTo.Features.Contains(text)) ? 1f : SoftwareType.SequelBoost;
			Feature feature = this.Features[text];
			float num2 = num * feature.DevTime * timeScale;
			if (feature.OneCategory())
			{
				string category2 = feature.GetCategory(true);
				float[] array;
				if (dictionary.TryGetValue(category2, out array))
				{
					array[0] += num2 * feature.CodeArtRatio;
					array[1] += num2 * (1f - feature.CodeArtRatio);
				}
				else
				{
					dictionary[category2] = new float[]
					{
						num2 * feature.CodeArtRatio,
						num2 * (1f - feature.CodeArtRatio)
					};
				}
			}
			else
			{
				if (feature.CodeArtRatio > 0f)
				{
					string category3 = feature.GetCategory(true);
					float[] array2;
					if (dictionary.TryGetValue(category3, out array2))
					{
						array2[0] += num2 * feature.CodeArtRatio;
					}
					else
					{
						Dictionary<string, float[]> dictionary2 = dictionary;
						string key = category3;
						float[] array3 = new float[2];
						array3[0] = num2 * feature.CodeArtRatio;
						dictionary2[key] = array3;
					}
				}
				if (feature.CodeArtRatio < 1f)
				{
					string category4 = feature.GetCategory(false);
					float[] array4;
					if (dictionary.TryGetValue(category4, out array4))
					{
						array4[1] += num2 * feature.CodeArtRatio;
					}
					else
					{
						dictionary[category4] = new float[]
						{
							0f,
							num2 * (1f - feature.CodeArtRatio)
						};
					}
				}
			}
		}
		return dictionary;
	}

	public bool ForceIssueBool(string cat, Dictionary<string, SoftwareProduct> needs, List<SoftwareProduct> OSs)
	{
		foreach (Feature feature in from x in this.Features.Values
		where x.Forced && x.IsCompatible(cat)
		select x)
		{
			foreach (KeyValuePair<string, string> keyValuePair in feature.Dependencies)
			{
				if (!keyValuePair.Key.Equals(this.Name))
				{
					if (keyValuePair.Key.Equals("Operating System"))
					{
						if (OSs == null)
						{
							return true;
						}
						for (int j = 0; j < OSs.Count; j++)
						{
							if (!SoftwareType.DependencyMet(keyValuePair.Value, OSs[j]))
							{
								return true;
							}
						}
					}
					else
					{
						SoftwareProduct sw;
						if (!needs.TryGetValue(keyValuePair.Key, out sw))
						{
							return true;
						}
						if (!SoftwareType.DependencyMet(keyValuePair.Value, sw))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public bool ForceIssueBool(string cat, Dictionary<string, uint> needs, uint[] OSs)
	{
		foreach (Feature feature in from x in this.Features.Values
		where x.Forced && x.IsCompatible(cat)
		select x)
		{
			foreach (KeyValuePair<string, string> keyValuePair in feature.Dependencies)
			{
				if (!keyValuePair.Key.Equals(this.Name))
				{
					if (keyValuePair.Key.Equals("Operating System"))
					{
						if (OSs == null)
						{
							return true;
						}
						for (int j = 0; j < OSs.Length; j++)
						{
							if (!SoftwareType.DependencyMet(keyValuePair.Value, GameSettings.Instance.simulation.GetProduct(OSs[j], true)))
							{
								return true;
							}
						}
					}
					else
					{
						uint id;
						if (!needs.TryGetValue(keyValuePair.Key, out id))
						{
							return true;
						}
						if (!SoftwareType.DependencyMet(keyValuePair.Value, GameSettings.Instance.simulation.GetProduct(id, false)))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public string[] ForceIssue(string cat, Dictionary<string, SoftwareProduct> needs, SoftwareProduct[] OSs)
	{
		string[] result = null;
		foreach (Feature feature in from x in this.Features.Values
		where x.Forced && x.IsCompatible(cat)
		select x)
		{
			foreach (KeyValuePair<string, string> keyValuePair in feature.Dependencies)
			{
				if (!keyValuePair.Key.Equals(this.Name))
				{
					if (keyValuePair.Key.Equals("Operating System"))
					{
						if (OSs == null)
						{
							result = new string[]
							{
								keyValuePair.Key,
								keyValuePair.Value
							};
						}
						for (int j = 0; j < OSs.Length; j++)
						{
							if (!SoftwareType.DependencyMet(keyValuePair.Value, OSs[j]))
							{
								result = new string[]
								{
									keyValuePair.Key,
									keyValuePair.Value
								};
							}
						}
					}
					else
					{
						SoftwareProduct sw;
						if (!needs.TryGetValue(keyValuePair.Key, out sw))
						{
							return new string[]
							{
								keyValuePair.Key,
								keyValuePair.Value
							};
						}
						if (!SoftwareType.DependencyMet(keyValuePair.Value, sw))
						{
							return new string[]
							{
								keyValuePair.Key,
								keyValuePair.Value
							};
						}
					}
				}
			}
		}
		return result;
	}

	public uint GetReach(string category, string[] features, IList<uint> OSs)
	{
		return this.GetReach(this.Categories[category], features, OSs);
	}

	public uint GetReach(SoftwareCategory catt, string[] features, IList<uint> OSs)
	{
		uint num = (uint)Math.Round((double)(catt.Popularity * MarketSimulation.Population));
		if (this.OSSpecific && OSs != null && OSs.Count > 0)
		{
			num = Math.Min(num, GameSettings.Instance.simulation.GetOSCoverage(OSs));
		}
		return num;
	}

	public float FinalQualityCalc(float codeProgress, float artProgress, float codeQuality, float artQuality, IList<string> features)
	{
		return SoftwareAlpha.FinalQualityCalc(codeProgress, artProgress, codeQuality, artQuality, this.CodeArtRatio(features));
	}

	public void CalculateLOCArt(string[] features, out int loc, out float art)
	{
		float num = 0f;
		float num2 = 0f;
		foreach (Feature feature in from x in features
		select this.Features[x])
		{
			float num3 = feature.DevTime * feature.CodeArtRatio;
			num2 += feature.DevTime - num3;
			for (int i = 0; i < feature.Dependencies.Length; i++)
			{
				num3 /= 2f;
			}
			num += num3;
		}
		loc = ((num != 0f) ? (Mathf.RoundToInt(6453f * Mathf.Pow(num, 1.9f)) + Utilities.RandomRange(-100, 100)) : 0);
		loc = Mathf.Max(0, loc);
		art = ((num2 != 0f) ? (Utilities.RandomRange(1.44f, 3.5f) * Mathf.Pow(1f + Mathf.Sqrt(num2) / 15f, (float)Mathf.Max(0, SDateTime.Now().RealYear - 1980))) : 0f);
	}

	public static float SequelBoost = 0.9f;

	public static float RoyaltySum = 0.2f;

	private static int QualityLabelCount = 6;

	private static int AwarenessLabelCount = 5;

	private static int PerceptionLabelCount = 5;

	public readonly string Name;

	public readonly string Category;

	public readonly string Description;

	public readonly string OSNeed;

	public readonly string OSLimit;

	public readonly float RandomFactor;

	public readonly bool OSSpecific;

	public readonly bool OneClient;

	public readonly bool InHouse;

	public readonly Dictionary<string, Feature> Features;

	public readonly Dictionary<string, SoftwareCategory> Categories;

	public readonly int? Unlock;

	public readonly float? IdealPrice;

	public readonly bool Modded;

	[NonSerialized]
	private Dictionary<string, string[]> _featureUpgrades;

	public static float DesignRatio = 0.333333343f;

	private static readonly HashSet<string> PreviousFeaturesCache = new HashSet<string>();

	public static FloatInterpolator QualityLevel = new FloatInterpolator(1f, true, new float[]
	{
		1f,
		1f,
		2f,
		2f,
		2f,
		3f,
		3f,
		3f,
		3f,
		4f,
		4f,
		4f,
		4f,
		4f,
		4f,
		5f,
		5f,
		5f,
		5f,
		6f,
		6f
	});
}
