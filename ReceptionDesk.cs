﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ReceptionDesk : MonoBehaviour
{
	public bool Active
	{
		get
		{
			return base.GetComponent<Furniture>().GetInteractionPoint("Use", true).UsedBy != null;
		}
	}

	[NonSerialized]
	public uint[] QueueSave;

	[NonSerialized]
	public List<Actor> Queue = new List<Actor>();
}
