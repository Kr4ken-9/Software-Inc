﻿using System;
using Steamworks;

public interface IWorkshopItem
{
	string ItemTitle { get; set; }

	PublishedFileId_t? SteamID { get; set; }

	bool SetTitle { get; set; }

	bool CanUpload { get; set; }

	string GetWorkshopType();

	string FolderPath();

	string[] GetValidExts();

	string[] ExtraTags();

	string GetThumbnail();
}
