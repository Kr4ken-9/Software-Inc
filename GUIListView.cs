﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUIListView : MonoBehaviour
{
	public GUIListView()
	{
		Dictionary<string, GUIListView.ColumnDef> dictionary = new Dictionary<string, GUIListView.ColumnDef>();
		dictionary.Add("GenericName", new GUIListView.ColumnDefinition<object>("Name", (object x) => x.ToString(), false, null, false));
		dictionary.Add("EmployeeName", new GUIListView.ColumnDefinition<Actor>("Name", (Actor x) => x.employee.FullName, false, new float?(145f), false));
		dictionary.Add("EmployeeRole", new GUIListView.ColumnDefinition<Actor>("Role", (Actor x) => x.employee.CurrentRoleBit, (Actor x) => Employee.RoleBitOrder(x.employee.CurrentRoleBit), true, new float?(120f), new GUIListView.FilterType?(GUIListView.FilterType.Bitmask), (Actor x) => x.employee.CurrentRoleBit, new GUIColumn.ColumnType?(GUIColumn.ColumnType.Role), null));
		dictionary.Add("EmployeeState", new GUIListView.ColumnDefinition<Actor>("Status", (Actor x) => x.CurrentState(false), true, null, true));
		dictionary.Add("EmployeeTeam", new GUIListView.ColumnDefinition<Actor>("Team", (Actor x) => x.Team ?? "None".Loc(), true, null, true));
		dictionary.Add("EmployeeSalary", new GUIListView.ColumnDefinition<Actor>("Salary", (Actor x) => x.GetRealSalary(), true, null, true));
		dictionary.Add("EmployeeWorth", new GUIListView.ColumnDefinition<Actor>("Request", (Actor x) => (WageWindow.GetMinSalary(x, x.employee.Worth(-2, true), true) - x.GetRealSalary()).CurrencyDiff(true), (Actor x) => WageWindow.GetMinSalary(x, x.employee.Worth(-2, true), true) - x.GetRealSalary(), false, null, new GUIListView.FilterType?(GUIListView.FilterType.Number), (Actor x) => (x.employee.Worth(-2, true) - x.GetRealSalary()).CurrencyMul(), null, null));
		dictionary.Add("EmployeeVacation", new GUIListView.ColumnDefinition<Actor>("Vacation", (Actor x) => (!x.employee.Founder && x.GetBenefitValue("Vacation months") != 0f) ? x.AlternateVacation.ToCompactString() : "None".Loc(), (Actor z) => (float)z.AlternateVacation.ToInt(), true, null, new GUIListView.FilterType?(GUIListView.FilterType.Name), null, null, null));
		dictionary.Add("EmployeeEffectiveness", new GUIListView.ColumnDefinition<Actor>("Effectiveness", (Actor x) => x.Effectiveness, true, null, false));
		dictionary.Add("EmployeeCompatibility", new GUIListView.ColumnDefinition<Actor>("Team Compatibility", (Actor x) => (x.TeamCompatibility != -1f) ? ((x.TeamCompatibility * 100f).ToString("F") + "%") : "N/A", (Actor z) => z.TeamCompatibility, true, new float?(145f), new GUIListView.FilterType?(GUIListView.FilterType.Number), (Actor x) => x.TeamCompatibility * 100f, null, null));
		dictionary.Add("EmployeeSatisfaction", new GUIListView.ColumnDefinition<Actor>("Satisfaction", (Actor z) => z.employee.JobSatisfaction, true, null, false));
		dictionary.Add("EmployeeAptitude", new GUIListView.ColumnDefinition<Actor>("Aptitude", (Actor z) => z.employee.Autodidactic, true, new float?(85f), false));
		dictionary.Add("EmployeeLeadership", new GUIListView.ColumnDefinition<Actor>("Leadership", (Actor z) => z.employee.Leadership, true, new float?(90f), false));
		dictionary.Add("EmployeeDiligence", new GUIListView.ColumnDefinition<Actor>("Diligence", (Actor z) => z.employee.Diligence, true, new float?(80f), false));
		dictionary.Add("EmployeeAge", new GUIListView.ColumnDefinition<Actor>("Age", (Actor x) => Mathf.FloorToInt(x.employee.Age).ToString(), (Actor z) => (float)z.employee.AgeMonth, true, new float?(65f), new GUIListView.FilterType?(GUIListView.FilterType.Number), (Actor x) => x.employee.Age, null, null));
		dictionary.Add("EmployeeYears", new GUIListView.ColumnDefinition<Actor>("Years", (Actor x) => (TimeOfDay.Instance.Year - x.employee.Hired.Year).ToString(), (Actor z) => (float)z.employee.Hired.ToInt(), false, null, new GUIListView.FilterType?(GUIListView.FilterType.Number), (Actor x) => Utilities.GetMonths(x.employee.Hired, SDateTime.Now()) / 12f, null, null));
		dictionary.Add("EmployeeSkillLead", new GUIListView.ColumnDefinition<Actor>("Lead skill", (Actor x) => x.employee.GetSkill(Employee.EmployeeRole.Lead), true, new float?(75f), false));
		dictionary.Add("EmployeeSkillCode", new GUIListView.ColumnDefinition<Actor>("Code skill", (Actor x) => x.employee.GetSkill(Employee.EmployeeRole.Programmer), true, new float?(80f), false));
		dictionary.Add("EmployeeSkillDesign", new GUIListView.ColumnDefinition<Actor>("Design skill", (Actor x) => x.employee.GetSkill(Employee.EmployeeRole.Designer), true, new float?(90f), false));
		dictionary.Add("EmployeeSkillArt", new GUIListView.ColumnDefinition<Actor>("Art skill", (Actor x) => x.employee.GetSkill(Employee.EmployeeRole.Artist), true, new float?(75f), false));
		dictionary.Add("EmployeeSkillMarketing", new GUIListView.ColumnDefinition<Actor>("Marketing skill", (Actor x) => x.employee.GetSkill(Employee.EmployeeRole.Marketer), true, new float?(115f), false));
		dictionary.Add("EmployeeDetails", new GUIListView.ColumnDefinition<Actor>("Details", delegate(Actor x)
		{
			HUD.Instance.DetailWindow.Show(x, true, false);
		}, null));
		dictionary.Add("ProductName", new GUIListView.ColumnDefinition<SoftwareProduct>("Name", (SoftwareProduct x) => x.Name, false, new float?(220f), false));
		dictionary.Add("ProductType", new GUIListView.ColumnDefinition<SoftwareProduct>("Category", (SoftwareProduct x) => x._category.LocSWC(x._type), (SoftwareProduct z) => z._category, false, new float?(80f), new GUIListView.FilterType?(GUIListView.FilterType.Name), null, null, null));
		dictionary.Add("ProductNeedType", new GUIListView.ColumnDefinition<SoftwareProduct>("Type", (SoftwareProduct x) => x._type.LocSW(), (SoftwareProduct z) => z._type, false, new float?(140f), new GUIListView.FilterType?(GUIListView.FilterType.Name), null, null, null));
		dictionary.Add("ProductCompany", new GUIListView.ColumnDefinition<SoftwareProduct>("Company", (SoftwareProduct x) => x.DevCompany.Name, false, new float?(200f), true));
		dictionary.Add("ProductInventor", new GUIListView.ColumnDefinition<SoftwareProduct>("Inventor", (SoftwareProduct z) => !z.Traded, false, null));
		dictionary.Add("ProductRelease", new GUIListView.ColumnDefinition<SoftwareProduct>("Release", (SoftwareProduct x) => x.Release, false, new float?(125f)));
		dictionary.Add("ProductQuality", new GUIListView.ColumnDefinition<SoftwareProduct>("Quality", (SoftwareProduct x) => SoftwareType.GetQualityLabel(x.RealQuality), (SoftwareProduct z) => z.RealQuality, false, new float?(80f), new GUIListView.FilterType?(GUIListView.FilterType.Name), null, null, null));
		dictionary.Add("ProductAwareness", new GUIListView.ColumnDefinition<SoftwareProduct>("Marketing", (SoftwareProduct x) => SoftwareType.GetAwarenessLabel(x.GetAwareness()), (SoftwareProduct z) => z.GetAwareness(), true, null, new GUIListView.FilterType?(GUIListView.FilterType.Name), null, null, null));
		dictionary.Add("ProductCost", new GUIListView.ColumnDefinition<SoftwareProduct>("License", (SoftwareProduct z) => (!GameSettings.Instance.MyCompany.OwnsLicense(z)) ? z.GetLicenseCost() : 0f, false, null, true));
		dictionary.Add("ProductIncome", new GUIListView.ColumnDefinition<SoftwareProduct>("Profit", (SoftwareProduct z) => z.Sum - z.Loss, true, null, true));
		dictionary.Add("ProductSource", new GUIListView.ColumnDefinition<SoftwareProduct>("Open source", (SoftwareProduct z) => z.OpenSource, false, null));
		dictionary.Add("ProductLoss", new GUIListView.ColumnDefinition<SoftwareProduct>("Loss", (SoftwareProduct z) => z.Loss, true, null, true));
		dictionary.Add("ProductUserBase", new GUIListView.ColumnDefinition<SoftwareProduct>("Active users", (SoftwareProduct z) => z.Userbase, true, new float?(115f)));
		dictionary.Add("ProductPrice", new GUIListView.ColumnDefinition<SoftwareProduct>("Retail price", (SoftwareProduct z) => z.Price, false, null, true));
		dictionary.Add("ProductUnitSales", new GUIListView.ColumnDefinition<SoftwareProduct>("Units sold", (SoftwareProduct z) => z.UnitSum, true, new float?(115f)));
		dictionary.Add("ProductCopies", new GUIListView.ColumnDefinition<SoftwareProduct>("In stock", (SoftwareProduct z) => z.PhysicalCopies, true, null));
		dictionary.Add("ProductStorage", new GUIListView.ColumnDefinition<SoftwareProduct>("In storage", (SoftwareProduct z) => GameSettings.Instance.GetPrintsInStorage(z.ID, false), true, null));
		dictionary.Add("ProductLastMonth", new GUIListView.ColumnDefinition<SoftwareProduct>("Past month", (SoftwareProduct z) => z.GetCashflow().LastOrDefault<float>(), true, null, true));
		dictionary.Add("ProductRefunds", new GUIListView.ColumnDefinition<SoftwareProduct>("Refunds", (SoftwareProduct z) => z.RefundSum, true, null));
		dictionary.Add("ProductHasSequel", new GUIListView.ColumnDefinition<SoftwareProduct>("Has sequel", (SoftwareProduct z) => z.HasSequel, true, null));
		dictionary.Add("ProductDetail", new GUIListView.ColumnDefinition<SoftwareProduct>("Details", delegate(SoftwareProduct x)
		{
			HUD.Instance.GetProductWindow(null).ShowProductDetails(x);
		}, null));
		dictionary.Add("CompanyName", new GUIListView.ColumnDefinition<Company>("Name", (Company x) => x.Name, false, new float?(160f), false));
		dictionary.Add("CompanyWorth", new GUIListView.ColumnDefinition<Company>("Worth", (Company x) => x.GetMoneyWithInsurance(true), true, new float?(125f), true));
		dictionary.Add("CompanyBank", new GUIListView.ColumnDefinition<Company>("Bank", (Company x) => x.Money, true, new float?(125f), true));
		dictionary.Add("CompanyReputation", new GUIListView.ColumnDefinition<Company>("Reputation", (Company x) => x.BusinessReputation, true, null, false));
		dictionary.Add("CompanyFans", new GUIListView.ColumnDefinition<Company>("Fans", (Company x) => x.Fans, true, null));
		dictionary.Add("CompanyBankrupt", new GUIListView.ColumnDefinition<Company>("Bankrupt", (Company x) => x.Bankrupt, true, new float?(80f)));
		dictionary.Add("CompanyFounded", new GUIListView.ColumnDefinition<Company>("Founded", (Company x) => x.Founded, false, new float?(125f)));
		dictionary.Add("CompanyProducts", new GUIListView.ColumnDefinition<Company>("Products", (Company x) => x.Products.Count, true, new float?(90f)));
		dictionary.Add("CompanyPatents", new GUIListView.ColumnDefinition<Company>("Patents", (Company x) => x.Patents.Count, true, null));
		dictionary.Add("CompanyDistribution", new GUIListView.ColumnDefinition<Company>("Distribution", (Company x) => x.GetDistributionDealString(), delegate(Company x)
		{
			float? distributionDeal = x.DistributionDeal;
			return (distributionDeal == null) ? 0f : distributionDeal.Value;
		}, true, null, new GUIListView.FilterType?(GUIListView.FilterType.Number), delegate(Company x)
		{
			float? distributionDeal = x.DistributionDeal;
			return (distributionDeal == null) ? 0f : distributionDeal.Value;
		}, null, null));
		dictionary.Add("CompanySubsidiary", new GUIListView.ColumnDefinition<Company>("Subsidiary", (Company x) => x.IsPlayerOwned(), true, null));
		dictionary.Add("CompanyChart", new GUIListView.ColumnDefinition<Company>("Chart", delegate(Company x)
		{
			HUD.Instance.companyChart.Show(x);
		}, new float?(60f)));
		dictionary.Add("CompanyStock", new GUIListView.ColumnDefinition<Company>("Details", delegate(Company x)
		{
			if (!x.Bankrupt)
			{
				HUD.Instance.companyWindow.ShowCompanyDetails(x);
			}
		}, new float?(60f)));
		dictionary.Add("CompanyProductList", new GUIListView.ColumnDefinition<Company>("Products", delegate(Company y)
		{
			ProductWindow productWindow = HUD.Instance.GetProductWindow("AllRelease");
			productWindow.Show(true, "CompanyReleases".Loc(new object[]
			{
				y.Name
			}), false);
			productWindow.SetFilters(false, true, true);
			productWindow.SetCompany(y.Name);
		}, new float?(80f)));
		dictionary.Add("StockOwner", new GUIListView.ColumnDefinition<Stock>("Owner", (Stock x) => x.OwnerCompany.Name, false, new float?(130f), true));
		dictionary.Add("StockCompany", new GUIListView.ColumnDefinition<Stock>("Company", (Stock x) => x.TargetCompany.Name, false, new float?(130f), true));
		dictionary.Add("StockShare", new GUIListView.ColumnDefinition<Stock>("Share", (Stock x) => x.RealPercentage, false, new float?(70f), false));
		dictionary.Add("StockChange", new GUIListView.ColumnDefinition<Stock>("Change", (Stock x) => x.Change, true, new float?(70f), false));
		dictionary.Add("StockWorth", new GUIListView.ColumnDefinition<Stock>("Worth", (Stock x) => x.CurrentWorth, true, new float?(80f), true));
		Dictionary<string, GUIListView.ColumnDef> dictionary2 = dictionary;
		string key = "StockSell";
		string header = "Sell";
		if (GUIListView.<>f__mg$cache0 == null)
		{
			GUIListView.<>f__mg$cache0 = new Action<Stock>(GUIListView.SellStock);
		}
		dictionary2.Add(key, new GUIListView.ColumnDefinition<Stock>(header, GUIListView.<>f__mg$cache0, new float?(60f)));
		dictionary.Add("StockDetail", new GUIListView.ColumnDefinition<Stock>("Details", delegate(Stock x)
		{
			Company targetCompany = x.TargetCompany;
			if (!targetCompany.Bankrupt)
			{
				HUD.Instance.companyWindow.ShowCompanyDetails(targetCompany);
			}
		}, new float?(60f)));
		dictionary.Add("TeamName", new GUIListView.ColumnDefinition<Team>("Name", (Team x) => x.Name, false, null, false));
		dictionary.Add("TeamCount", new GUIListView.ColumnDefinition<Team>("Count", (Team x) => x.Count, true, new float?(60f)));
		dictionary.Add("TeamCompatibility", new GUIListView.ColumnDefinition<Team>("Compatibility", (Team x) => x.Compatibility, true, new float?(60f), false));
		dictionary.Add("TeamWork", new GUIListView.ColumnDefinition<Team>("Work", (Team x) => x.WorkItems.Count, true, new float?(60f)));
		dictionary.Add("TeamStart", new GUIListView.ColumnDefinition<Team>("Arrival", (Team x) => Utilities.HourString(x.WorkStart), (Team z) => (float)z.WorkStart, true, new float?(60f), null, null, null, null));
		dictionary.Add("TeamEnd", new GUIListView.ColumnDefinition<Team>("Departure", (Team x) => Utilities.HourString(x.WorkEnd), (Team z) => (float)z.WorkEnd, true, new float?(85f), null, null, null, null));
		Dictionary<string, GUIListView.ColumnDef> dictionary3 = dictionary;
		string key2 = "TeamVacation";
		string header2 = "Vacation";
		if (GUIListView.<>f__mg$cache1 == null)
		{
			GUIListView.<>f__mg$cache1 = new Func<Team, object>(GUIListView.GetVacationMonths);
		}
		dictionary3.Add(key2, new GUIListView.ColumnDefinition<Team>(header2, GUIListView.<>f__mg$cache1, (Team z) => (float)z.VacationMonth, true, null, new GUIListView.FilterType?(GUIListView.FilterType.Name), null, null, null));
		dictionary.Add("TeamVacationSpread", new GUIListView.ColumnDefinition<Team>("Vacation range", (Team x) => "MonthPostfix".LocPlural(x.VacationSpread + 1), (Team z) => (float)z.VacationSpread, true, new float?(120f), new GUIListView.FilterType?(GUIListView.FilterType.Number), (Team x) => (float)x.VacationSpread, null, null));
		dictionary.Add("TeamHR", new GUIListView.ColumnDefinition<Team>("HR", (Team x) => x.HREnabled, true, new float?(50f)));
		dictionary.Add("TeamCrunch", new GUIListView.ColumnDefinition<Team>("Crunch", (Team x) => x.CrunchMode, true, new float?(70f)));
		dictionary.Add("TeamStartMinus", new GUIListView.ColumnDefinition<Team>("-", delegate(Team x)
		{
			x.WorkStart = x.WorkStart.AddHour(-1);
			if (x.WorkStart == x.WorkEnd)
			{
				x.WorkStart = x.WorkStart.AddHour(-1);
			}
		}, new float?(25f)));
		dictionary.Add("TeamStartPlus", new GUIListView.ColumnDefinition<Team>("+", delegate(Team x)
		{
			x.WorkStart = x.WorkStart.AddHour(1);
			if (x.WorkStart == x.WorkEnd)
			{
				x.WorkStart = x.WorkStart.AddHour(1);
			}
		}, new float?(25f)));
		dictionary.Add("TeamEndMinus", new GUIListView.ColumnDefinition<Team>("-", delegate(Team x)
		{
			x.WorkEnd = x.WorkEnd.AddHour(-1);
			if (x.WorkStart == x.WorkEnd)
			{
				x.WorkEnd = x.WorkEnd.AddHour(-1);
			}
		}, new float?(25f)));
		dictionary.Add("TeamEndPlus", new GUIListView.ColumnDefinition<Team>("+", delegate(Team x)
		{
			x.WorkEnd = x.WorkEnd.AddHour(1);
			if (x.WorkStart == x.WorkEnd)
			{
				x.WorkEnd = x.WorkEnd.AddHour(1);
			}
		}, new float?(25f)));
		dictionary.Add("TeamVacationMinus", new GUIListView.ColumnDefinition<Team>("-", delegate(Team x)
		{
			x.VacationMonth = ((x.VacationMonth != 0) ? (x.VacationMonth - 1) : 11);
			x.RescheduleVacations();
		}, new float?(25f)));
		dictionary.Add("TeamVacationPlus", new GUIListView.ColumnDefinition<Team>("+", delegate(Team x)
		{
			x.VacationMonth = (x.VacationMonth + 1) % 12;
			x.RescheduleVacations();
		}, new float?(25f)));
		dictionary.Add("TeamVacationSpreadMinus", new GUIListView.ColumnDefinition<Team>("-", delegate(Team x)
		{
			x.VacationSpread = ((x.VacationSpread != 0) ? (x.VacationSpread - 1) : 11);
			x.RescheduleVacations();
		}, new float?(25f)));
		dictionary.Add("TeamVacationSpreadPlus", new GUIListView.ColumnDefinition<Team>("+", delegate(Team x)
		{
			x.VacationSpread = (x.VacationSpread + 1) % 12;
			x.RescheduleVacations();
		}, new float?(25f)));
		dictionary.Add("StaffType", new GUIListView.ColumnDefinition<Actor>("Type", (Actor x) => x.AItype.ToString().Loc(), false, null, true));
		dictionary.Add("StaffStart", new GUIListView.ColumnDefinition<Actor>("Arrival", (Actor x) => Utilities.HourString(x.StaffOn), (Actor x) => (float)x.StaffOn, true, new float?(70f), new GUIListView.FilterType?(GUIListView.FilterType.Number), (Actor x) => (float)x.StaffOn, null, null));
		dictionary.Add("StaffEnd", new GUIListView.ColumnDefinition<Actor>("Departure", (Actor x) => Utilities.HourString(x.StaffOff), (Actor x) => (float)x.StaffOff, true, new float?(80f), new GUIListView.FilterType?(GUIListView.FilterType.Number), (Actor x) => (float)x.StaffOff, null, null));
		dictionary.Add("StaffWorkPlus", new GUIListView.ColumnDefinition<Actor>("+", delegate(Actor x)
		{
			x.StaffOn = (x.StaffOn + 1) % 24;
			x.StaffOff = (x.StaffOn + 4) % 24;
			if (!x.isActiveAndEnabled)
			{
				SDateTime sdateTime = SDateTime.Now();
				SDateTime time = new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), x.StaffOn - 1, sdateTime.Day + 1, sdateTime.Month, sdateTime.Year);
				GameSettings.Instance.sActorManager.AddToAwaiting(x, time, true, true);
			}
		}, new float?(25f)));
		Dictionary<string, GUIListView.ColumnDef> dictionary4 = dictionary;
		string key3 = "StaffWorkMinus";
		string header3 = "-";
		if (GUIListView.<>f__mg$cache2 == null)
		{
			GUIListView.<>f__mg$cache2 = new Action<Actor>(GUIListView.StaffWorkMinus);
		}
		dictionary4.Add(key3, new GUIListView.ColumnDefinition<Actor>(header3, GUIListView.<>f__mg$cache2, new float?(25f)));
		dictionary.Add("StaffDismiss", new GUIListView.ColumnDefinition<Actor>("Dismiss", delegate(Actor x)
		{
			x.Fire();
		}, new float?(70f)));
		dictionary.Add("ContractCompany", new GUIListView.ColumnDefinition<ContractWork>("Company", (ContractWork x) => x.Company, false, null, false));
		dictionary.Add("ContractMonths", new GUIListView.ColumnDefinition<ContractWork>("Months", (ContractWork x) => x.Months, false, new float?(70f)));
		dictionary.Add("ContractIncome", new GUIListView.ColumnDefinition<ContractWork>("Income", (ContractWork x) => (float)(x.Done + x.Initial), false, new float?(95f), true));
		dictionary.Add("ContractType", new GUIListView.ColumnDefinition<ContractWork>("Type", (ContractWork x) => x.SoftwareType.LocSW(), false, new float?(188f), true));
		dictionary.Add("LoanTotal", new GUIListView.ColumnDefinition<KeyValuePair<int, float>>("Total", (KeyValuePair<int, float> x) => (float)x.Key * x.Value, true, new float?(90f), true));
		dictionary.Add("LoanMonthly", new GUIListView.ColumnDefinition<KeyValuePair<int, float>>("Monthly", (KeyValuePair<int, float> x) => x.Value, false, new float?(85f), true));
		dictionary.Add("LoanMonths", new GUIListView.ColumnDefinition<KeyValuePair<int, float>>("Months", (KeyValuePair<int, float> x) => x.Key, true, new float?(70f)));
		dictionary.Add("LoanPayout", new GUIListView.ColumnDefinition<KeyValuePair<int, float>>("Payout", delegate(KeyValuePair<int, float> loanItem)
		{
			float num = (float)loanItem.Key * loanItem.Value;
			if (GameSettings.Instance.MyCompany.CanMakeTransaction(-num))
			{
				GameSettings.Instance.MyCompany.MakeTransaction(-num, Company.TransactionCategory.Loan, null);
				GameSettings.Instance.Loans.Remove(loanItem);
				HUD.Instance.loanWindow.UpdateLoans();
			}
		}, new float?(65f)));
		dictionary.Add("HireName", new GUIListView.ColumnDefinition<Employee>("Name", (Employee x) => x.Name, false, new float?(125f), false));
		dictionary.Add("HireAge", new GUIListView.ColumnDefinition<Employee>("Age", (Employee x) => Mathf.FloorToInt(x.Age).ToString(), (Employee x) => x.Age, false, new float?(56f), new GUIListView.FilterType?(GUIListView.FilterType.Number), (Employee x) => x.Age, null, null));
		dictionary.Add("HireScore", new GUIListView.ColumnDefinition<Employee>("Skill", (Employee x) => (float)x.GetHireSkill((Employee.EmployeeRole)HUD.Instance.hireWindow.RoleCombo.Selected).Quantize(4) + 1f, (Employee x) => x.GetSkill((Employee.EmployeeRole)HUD.Instance.hireWindow.RoleCombo.Selected), false, new float?(78f), null, null, new GUIColumn.ColumnType?(GUIColumn.ColumnType.Stars), null));
		dictionary.Add("HireSalary", new GUIListView.ColumnDefinition<Employee>("Salary", (Employee x) => x.Salary, false, new float?(72f), true));
		dictionary.Add("HireCompatibility", new GUIListView.ColumnDefinition<Employee>("Compatibility", (Employee x) => (HUD.Instance.hireWindow.Compatibility.Count <= 0) ? "TeamCompat0".Loc() : Team.GetCompatDesc(HUD.Instance.hireWindow.Compatibility[x]), (Employee z) => (HUD.Instance.hireWindow.Compatibility.Count <= 0) ? 0f : HUD.Instance.hireWindow.Compatibility[z], true, null, new GUIListView.FilterType?(GUIListView.FilterType.Name), null, null, null));
		dictionary.Add("HireHire", new GUIListView.ColumnDefinition<Employee>("Hire", delegate(Employee x)
		{
			HUD.Instance.hireWindow.HireEmployee(x, true);
		}, new float?(60f)));
		dictionary.Add("SaveGameName", new GUIListView.ColumnDefinition<SaveGame>("Name", (SaveGame x) => x.Name, false, new float?(100f), false));
		dictionary.Add("SaveGameCompany", new GUIListView.ColumnDefinition<SaveGame>("Company", (SaveGame x) => x.CompanyName, false, new float?(150f), true));
		dictionary.Add("SaveGameGameDate", new GUIListView.ColumnDefinition<SaveGame>("Game date", (SaveGame x) => x.InGameTime, false, new float?(120f)));
		dictionary.Add("SaveGameDate", new GUIListView.ColumnDefinition<SaveGame>("Date", (SaveGame x) => x.RealTime.ToString("yyyy MMM dd HH:mm"), (SaveGame x) => x.RealTime.ToString("yyyyMMMddHHmm"), false, new float?(125f), null, null, null, null));
		dictionary.Add("SaveGameMoney", new GUIListView.ColumnDefinition<SaveGame>("Money", (SaveGame x) => x.Money, false, new float?(110f), true));
		dictionary.Add("SaveGameProducts", new GUIListView.ColumnDefinition<SaveGame>("Products", (SaveGame x) => x.Products, false, new float?(75f)));
		dictionary.Add("SaveGameEmployees", new GUIListView.ColumnDefinition<SaveGame>("Employees", (SaveGame x) => x.Employees, false, new float?(85f)));
		dictionary.Add("SaveGameSize", new GUIListView.ColumnDefinition<SaveGame>("Size", (SaveGame x) => x.FileSize.ByteSize(false), (SaveGame x) => x.FileSize, false, new float?(64f), new GUIListView.FilterType?(GUIListView.FilterType.Number), (SaveGame x) => x.FileSize, null, null));
		Dictionary<string, GUIListView.ColumnDef> dictionary5 = dictionary;
		string key4 = "SaveGameDelete";
		string header4 = "Delete";
		if (GUIListView.<>f__mg$cache3 == null)
		{
			GUIListView.<>f__mg$cache3 = new Action<SaveGame>(GUIListView.DeleteSaveFile);
		}
		dictionary5.Add(key4, new GUIListView.ColumnDefinition<SaveGame>(header4, GUIListView.<>f__mg$cache3, null));
		dictionary.Add("ModName", new GUIListView.ColumnDefinition<ModPackage>("Name", (ModPackage x) => x.Name + ((x.SteamID == null) ? string.Empty : " (Steam)"), (ModPackage x) => x.Name, false, new float?(105f), new GUIListView.FilterType?(GUIListView.FilterType.Name), (ModPackage x) => x.Name, null, null));
		dictionary.Add("ModActive", new GUIListView.ColumnDefinition<ModPackage>("Enabled", (ModPackage x) => x.Enabled, true, new float?(70f)));
		Dictionary<string, GUIListView.ColumnDef> dictionary6 = dictionary;
		string key5 = "ModAction";
		string header5 = "Toggle";
		if (GUIListView.<>f__mg$cache4 == null)
		{
			GUIListView.<>f__mg$cache4 = new Action<ModPackage>(GUIListView.ToggleMod);
		}
		dictionary6.Add(key5, new GUIListView.ColumnDefinition<ModPackage>(header5, GUIListView.<>f__mg$cache4, new float?(65f)));
		dictionary.Add("SrvName", new GUIListView.ColumnDefinition<IServerHost>("Name", (IServerHost x) => x.ServerName, true, new float?(125f), false));
		dictionary.Add("SrvPower", new GUIListView.ColumnDefinition<IServerHost>("Bandwidth", (IServerHost x) => x.PowerSum.Bandwidth(), (IServerHost x) => x.PowerSum, true, new float?(95f), null, null, null, null));
		dictionary.Add("SrvServers", new GUIListView.ColumnDefinition<IServerHost>("Count", (IServerHost x) => x.Count, true, new float?(60f)));
		dictionary.Add("SrvLoad", new GUIListView.ColumnDefinition<IServerHost>("ServerLoad", (IServerHost x) => 1f - x.Available, true, new float?(70f), false));
		dictionary.Add("SrvItems", new GUIListView.ColumnDefinition<IServerHost>("Processes", (IServerHost x) => x.Items.Count, true, new float?(85f)));
		dictionary.Add("SrvFallback", new GUIListView.ColumnDefinition<IServerHost>("Fallback", (IServerHost x) => (x.Fallback != null) ? x.Fallback.ServerName : "None".Loc(), true, null, true));
		Dictionary<string, GUIListView.ColumnDef> dictionary7 = dictionary;
		string key6 = "SrvSelectFallback";
		string header6 = "Select fallback";
		if (GUIListView.<>f__mg$cache5 == null)
		{
			GUIListView.<>f__mg$cache5 = new Action<IServerHost>(GUIListView.SelectServerFallback);
		}
		dictionary7.Add(key6, new GUIListView.ColumnDefinition<IServerHost>(header6, GUIListView.<>f__mg$cache5, null));
		dictionary.Add("SrvItemName", new GUIListView.ColumnDefinition<IServerItem>("Name", (IServerItem x) => x.GetDescription(), false, new float?(235f), false));
		dictionary.Add("SrvItemPower", new GUIListView.ColumnDefinition<IServerItem>("Bandwidth", (IServerItem x) => x.GetLoadRequirement().BandwidthFactor(SDateTime.Now()).Bandwidth(), (IServerItem x) => x.GetLoadRequirement(), true, new float?(135f), null, null, null, null));
		dictionary.Add("ContractResultCompany", new GUIListView.ColumnDefinition<ContractResult>("Company", (ContractResult x) => x.Contract.Company, false, new float?(116f), false));
		dictionary.Add("ContractResultIncome", new GUIListView.ColumnDefinition<ContractResult>("NetProfit", (ContractResult x) => x.FinalResult, false, null, true));
		dictionary.Add("ContractResultStatus", new GUIListView.ColumnDefinition<ContractResult>("Status", (ContractResult x) => x.Status.ToString().Loc(), (ContractResult x) => (float)x.Status, false, null, new GUIListView.FilterType?(GUIListView.FilterType.Name), null, null, null));
		dictionary.Add("ContractResultDate", new GUIListView.ColumnDefinition<ContractResult>("Date", (ContractResult x) => x.Date, false, null));
		dictionary.Add("ContractResultType", new GUIListView.ColumnDefinition<ContractResult>("Type", (ContractResult x) => x.Contract.SoftwareType.LocSW(), false, new float?(145f), true));
		dictionary.Add("DealDesc", new GUIListView.ColumnDefinition<Deal>("Description", (Deal x) => x.Description(), false, null, false));
		dictionary.Add("DealClient", new GUIListView.ColumnDefinition<Deal>("Company", (Deal x) => x.CompanyName, false, null, true));
		dictionary.Add("DealWorth", new GUIListView.ColumnDefinition<Deal>("Offer", (Deal x) => x.Worth(), false, null, true));
		dictionary.Add("DealCancel", new GUIListView.ColumnDefinition<Deal>("Cancel", delegate(Deal x)
		{
			HUD.Instance.dealWindow.CancelDeal(x, true);
		}, null));
		dictionary.Add("ProducProtoName", new GUIListView.ColumnDefinition<SimulatedCompany.ProductPrototype>("Name", (SimulatedCompany.ProductPrototype x) => x.Name, false, new float?(175f), false));
		dictionary.Add("ProducProtoCat", new GUIListView.ColumnDefinition<SimulatedCompany.ProductPrototype>("Category", (SimulatedCompany.ProductPrototype x) => x.Category.LocSWC(x.Type), false, null, true));
		dictionary.Add("ProducProtoType", new GUIListView.ColumnDefinition<SimulatedCompany.ProductPrototype>("Type", (SimulatedCompany.ProductPrototype x) => x.Type.LocSW(), false, new float?(135f), true));
		dictionary.Add("ProducProtoCompany", new GUIListView.ColumnDefinition<SimulatedCompany.ProductPrototype>("Company", (SimulatedCompany.ProductPrototype x) => x.DevCompany.Name, false, new float?(175f), true));
		dictionary.Add("ProducProtoRelease", new GUIListView.ColumnDefinition<SimulatedCompany.ProductPrototype>("Release", (SimulatedCompany.ProductPrototype x) => x.ReleaseDate, false, new float?(125f)));
		dictionary.Add("ProducProtoCancel", new GUIListView.ColumnDefinition<SimulatedCompany.ProductPrototype>("Cancel", delegate(SimulatedCompany.ProductPrototype x)
		{
			x.RemoveProject();
		}, null));
		dictionary.Add("WorkItemType", new GUIListView.ColumnDefinition<WorkItem>("Type", (WorkItem x) => x.GetWorkTypeName().Loc(), false, null, true));
		dictionary.Add("WorkItemProject", new GUIListView.ColumnDefinition<WorkItem>("Project", (WorkItem x) => x.Name, false, null, false));
		dictionary.Add("WorkItemTeam", new GUIListView.ColumnDefinition<WorkItem>("Team", (WorkItem x) => x.GetTeam(), false, null, true));
		dictionary.Add("PatentName", new GUIListView.ColumnDefinition<Feature>("Name", (Feature x) => x.Name.SWFeat(x.Software), false, new float?(135f), false));
		dictionary.Add("PatentSoftware", new GUIListView.ColumnDefinition<Feature>("Software", (Feature x) => x.Software.LocSW(), false, null, true));
		dictionary.Add("PatentResearched", new GUIListView.ColumnDefinition<Feature>("Researched", (Feature x) => x.Researched, true, new float?(95f)));
		dictionary.Add("PatentOwner", new GUIListView.ColumnDefinition<Feature>("Owner", (Feature x) => (x.PatentOwner != 0u) ? GameSettings.Instance.simulation.GetCompany(x.PatentOwner).Name : "None".Loc(), true, new float?(110f), true));
		dictionary.Add("PatentUnlock", new GUIListView.ColumnDefinition<Feature>("Unlocks", (Feature x) => x.Unlock.Value.ToString(), false, new float?(70f), true));
		dictionary.Add("PatentRoyalty", new GUIListView.ColumnDefinition<Feature>("Royalty", (Feature x) => x.Royalty, false, new float?(70f), false));
		dictionary.Add("PatentIncome", new GUIListView.ColumnDefinition<Feature>("Income", (Feature x) => x.Income, true, null, true));
		dictionary.Add("WorkshopName", new GUIListView.ColumnDefinition<IWorkshopItem>("Name", (IWorkshopItem x) => x.ItemTitle, false, null, false));
		dictionary.Add("WorkshopType", new GUIListView.ColumnDefinition<IWorkshopItem>("Type", (IWorkshopItem x) => x.GetWorkshopType(), false, null, true));
		dictionary.Add("WorkshopStatus", new GUIListView.ColumnDefinition<IWorkshopItem>("Uploaded", (IWorkshopItem x) => x.SteamID != null, true, null));
		Dictionary<string, GUIListView.ColumnDef> dictionary8 = dictionary;
		string key7 = "WorkshopUpload";
		string header7 = "Upload";
		if (GUIListView.<>f__mg$cache6 == null)
		{
			GUIListView.<>f__mg$cache6 = new Action<IWorkshopItem>(GUIListView.UploadWorkshopItem);
		}
		dictionary8.Add(key7, new GUIListView.ColumnDefinition<IWorkshopItem>(header7, GUIListView.<>f__mg$cache6, null));
		Dictionary<string, GUIListView.ColumnDef> dictionary9 = dictionary;
		string key8 = "BlueprintDelete";
		string header8 = "Delete";
		if (GUIListView.<>f__mg$cache7 == null)
		{
			GUIListView.<>f__mg$cache7 = new Action<BuildingPrefab>(GUIListView.DeleteBlueprint);
		}
		dictionary9.Add(key8, new GUIListView.ColumnDefinition<BuildingPrefab>(header8, GUIListView.<>f__mg$cache7, null));
		dictionary.Add("WaypointTime", new GUIListView.ColumnDefinition<WayPointEditorWindow.WayPoint>("Duration", (WayPointEditorWindow.WayPoint x) => x.Time, true, null, true));
		dictionary.Add("WaypointSetTime", new GUIListView.ColumnDefinition<WayPointEditorWindow.WayPoint>("Set duration", delegate(WayPointEditorWindow.WayPoint wp)
		{
			WindowManager.SpawnInputDialog("Time:", "Set time", wp.Time.ToString(), delegate(string z)
			{
				wp.Time = (float)Convert.ToDouble(z);
			}, null);
		}, null));
		dictionary.Add("WaypointDelete", new GUIListView.ColumnDefinition<WayPointEditorWindow.WayPoint>("Delete", delegate(WayPointEditorWindow.WayPoint x)
		{
			WayPointEditorWindow.Instance.WayPointList.Items.Remove(x);
		}, null));
		Dictionary<string, GUIListView.ColumnDef> dictionary10 = dictionary;
		string key9 = "WaypointFreeze";
		string header9 = "Freeze";
		if (GUIListView.<>f__mg$cache8 == null)
		{
			GUIListView.<>f__mg$cache8 = new Action<WayPointEditorWindow.WayPoint>(GUIListView.FreezeWaypoint);
		}
		dictionary10.Add(key9, new GUIListView.ColumnDefinition<WayPointEditorWindow.WayPoint>(header9, GUIListView.<>f__mg$cache8, null));
		dictionary.Add("AutoDevName", new GUIListView.ColumnDefinition<AutoDevWorkItem.AutoDevItem>("Name", (AutoDevWorkItem.AutoDevItem x) => x.Name, false, null, false));
		dictionary.Add("AutoDevStatus", new GUIListView.ColumnDefinition<AutoDevWorkItem.AutoDevItem>("Queued", (AutoDevWorkItem.AutoDevItem x) => x.Queued, true, null));
		dictionary.Add("AutoDevPhase", new GUIListView.ColumnDefinition<AutoDevWorkItem.AutoDevItem>("Phase", (AutoDevWorkItem.AutoDevItem x) => x.Phase().Loc(), true, null, true));
		dictionary.Add("AutoDevRelease", new GUIListView.ColumnDefinition<AutoDevWorkItem.AutoDevItem>("Release", (AutoDevWorkItem.AutoDevItem x) => x.ReleaseDateText, (AutoDevWorkItem.AutoDevItem x) => (float)x.ReleaseDateInt, true, null, null, null, null, null));
		dictionary.Add("AutoDevFollowers", new GUIListView.ColumnDefinition<AutoDevWorkItem.AutoDevItem>("Followers", (AutoDevWorkItem.AutoDevItem x) => (int)x.SWWorkItem.Followers, true, null));
		dictionary.Add("AutoDevInfo", new GUIListView.ColumnDefinition<AutoDevWorkItem.AutoDevItem>("Info", delegate(AutoDevWorkItem.AutoDevItem x)
		{
			GUIWorkItem.SpawnDevInfoWindow(x.SWWorkItem, x.Design);
		}, null));
		dictionary.Add("AutoDevTakeover", new GUIListView.ColumnDefinition<AutoDevWorkItem.AutoDevItem>("Takeover", delegate(AutoDevWorkItem.AutoDevItem x)
		{
			x.Release();
		}, null));
		dictionary.Add("AutoDevCancel", new GUIListView.ColumnDefinition<AutoDevWorkItem.AutoDevItem>("Cancel", delegate(AutoDevWorkItem.AutoDevItem x)
		{
			x.Cancel();
		}, null));
		dictionary.Add("TerminationName", new GUIListView.ColumnDefinition<EmployeeTermination>("Name", (EmployeeTermination x) => x.Name, false, null, false));
		dictionary.Add("TerminationRole", new GUIListView.ColumnDefinition<EmployeeTermination>("Role", (EmployeeTermination x) => x.Role, (EmployeeTermination x) => Employee.RoleBitOrder(x.Role), true, new float?(120f), new GUIListView.FilterType?(GUIListView.FilterType.Bitmask), (EmployeeTermination x) => x.Role, new GUIColumn.ColumnType?(GUIColumn.ColumnType.Role), null));
		dictionary.Add("TerminationTeam", new GUIListView.ColumnDefinition<EmployeeTermination>("Team", (EmployeeTermination x) => x.Team, false, null, true));
		dictionary.Add("TerminationYears", new GUIListView.ColumnDefinition<EmployeeTermination>("Years", (EmployeeTermination x) => (int)x.YearsHired, false, null));
		dictionary.Add("TerminationType", new GUIListView.ColumnDefinition<EmployeeTermination>("Reason", (EmployeeTermination x) => x.Termination.ToString().Loc(), false, null, true));
		dictionary.Add("TerminationDate", new GUIListView.ColumnDefinition<EmployeeTermination>("Date", (EmployeeTermination x) => x.Date, false, null));
		dictionary.Add("TerminationPayout", new GUIListView.ColumnDefinition<EmployeeTermination>("Payout", (EmployeeTermination x) => x.Payout, false, null, true));
		dictionary.Add("PrintProductName", new GUIListView.ColumnDefinition<PrintJob>("Product", (PrintJob x) => x.StockableName, false, null, false));
		dictionary.Add("PrintCompanyName", new GUIListView.ColumnDefinition<PrintJob>("Company", (PrintJob x) => x.CompanyName, false, null, true));
		dictionary.Add("PrintInStock", new GUIListView.ColumnDefinition<PrintJob>("In stock", (PrintJob x) => x.PhysicalCopies, true, null));
		dictionary.Add("PrintInStorage", new GUIListView.ColumnDefinition<PrintJob>("In storage", (PrintJob x) => GameSettings.Instance.GetPrintsInStorage(x.ID, false), true, null));
		dictionary.Add("PrintPerMonth", new GUIListView.ColumnDefinition<PrintJob>("Per month", (PrintJob x) => x.PrintPerMonthLabel(), (PrintJob x) => (float)x.PrintPerMonth(), true, null, new GUIListView.FilterType?(GUIListView.FilterType.Number), (PrintJob x) => (float)x.PrintPerMonth(), null, null));
		dictionary.Add("PrintGoal", new GUIListView.ColumnDefinition<PrintJob>("Goal", (PrintJob x) => x.GetGoal(), (PrintJob x) => x.GetGoalSort(), true, null, new GUIListView.FilterType?(GUIListView.FilterType.Number), (PrintJob x) => x.GetGoalSort(), null, null));
		dictionary.Add("PrintDeadline", new GUIListView.ColumnDefinition<PrintJob>("Deadline", (PrintJob x) => x.GetDeadline(), (PrintJob x) => (float)x.GetDeadlineOrder(), true, null, null, null, null, null));
		dictionary.Add("PrintPriority", new GUIListView.ColumnDefinition<PrintJob>("Priority", (PrintJob x) => x.Priority, (PrintJob x) => x.Priority, false, null, new GUIListView.FilterType?(GUIListView.FilterType.Number), (PrintJob x) => x.Priority * 100f, new GUIColumn.ColumnType?(GUIColumn.ColumnType.Slider), delegate(PrintJob x, object y)
		{
			x.Priority = (float)y;
		}));
		dictionary.Add("RoomGroupName", new GUIListView.ColumnDefinition<RoomGroup>("Name", (RoomGroup x) => x.Name, false, new float?(124f), false));
		dictionary.Add("RoomGroupCount", new GUIListView.ColumnDefinition<RoomGroup>("Count", (RoomGroup x) => x.Count, true, new float?(72f)));
		dictionary.Add("RoomGroupIndoorStyle", new GUIListView.ColumnDefinition<RoomGroup>("Indoor style", delegate(RoomGroup x)
		{
			GUIListView.RoomGroupStyling(x, false);
		}, new float?(88f)));
		dictionary.Add("RoomGroupOutdoorStyle", new GUIListView.ColumnDefinition<RoomGroup>("Outdoor style", delegate(RoomGroup x)
		{
			GUIListView.RoomGroupStyling(x, true);
		}, new float?(97f)));
		dictionary.Add("RoomGroupSelect", new GUIListView.ColumnDefinition<RoomGroup>("Select rooms", delegate(RoomGroup x)
		{
			GUIListView.SelectRoomGroup(x);
		}, new float?(93f)));
		dictionary.Add("RoomGroupRemove", new GUIListView.ColumnDefinition<RoomGroup>("Remove", delegate(RoomGroup x)
		{
			GameSettings.Instance.RemoveRoomGroup(x.Name);
		}, new float?(66f)));
		this.ColumnDefinitions = dictionary;
		this.ActualItems = new List<object>();
		this.Selected = new EventList<int>();
		this.GUIColumns = new List<GUIColumn>();
		base..ctor();
	}

	private static void SelectRoomGroup(RoomGroup group)
	{
		SelectorController instance = SelectorController.Instance;
		instance.Selected.ForEach(delegate(global::Selectable x)
		{
			x.Highlight(false, false);
		});
		instance.Selected.Clear();
		instance.Selected.AddRange(group.GetRooms().OfType<global::Selectable>());
		instance.DoPostSelectChecks();
	}

	private static string GetVacationMonths(Team t)
	{
		if (t.VacationSpread == 11)
		{
			return "All year".Loc();
		}
		if (t.VacationSpread == 0)
		{
			return SDateTime.Months[t.VacationMonth].Loc();
		}
		return (SDateTime.Months[t.VacationMonth] + "Abbr").Loc() + " - " + (SDateTime.Months[(t.VacationMonth + t.VacationSpread) % 12] + "Abbr").Loc();
	}

	private static void RoomGroupStyling(RoomGroup group, bool outdoor)
	{
		List<RoomStyle> styles = (from x in GameSettings.Instance.RoomStyles
		where x.OutdoorStyle == outdoor
		select x).ToList<RoomStyle>();
		SelectorController.Instance.selectWindow.Show("Room style", from x in styles
		select x.StyleName, delegate(int x)
		{
			RoomStyle roomStyle = (x >= 0) ? styles[x] : null;
			if (outdoor)
			{
				group.Outdoor = roomStyle;
			}
			else
			{
				group.Indoor = roomStyle;
			}
			if (roomStyle != null)
			{
				List<Room> rooms = group.GetRooms();
				foreach (Room room in rooms)
				{
					roomStyle.Apply(room, null);
				}
			}
		}, true, true, true, false, null);
	}

	private static void DeleteSaveFile(SaveGame file)
	{
	}

	private static void ToggleMod(ModPackage x)
	{
		x.Enabled = !x.Enabled;
		if (x.Enabled)
		{
			ModPackage[] mods = (from z in GameData.ModPackages
			where z.Enabled
			select z).ToArray<ModPackage>();
			Dictionary<string, RandomNameGenerator> generators = GameData.MergeGenerators(GameData.AllNameGenerators(mods));
			SoftwareType[] types = GameData.AllSoftwareTypes(mods).ToArray<SoftwareType>();
			string text = GameData.CheckForErrors(types) ?? ModPackage.OtherErrors(types, GameData.AllCompanyTypes(mods).ToArray<CompanyType>(), generators);
			if (text != null)
			{
				x.Enabled = false;
				WindowManager.SpawnDialog("ModEnableError".Loc(), true, DialogWindow.DialogType.Error);
			}
		}
		ActorCustomization.Instance.UpdatePersonalities();
		ActorCustomization.Instance.UpdateSpec();
	}

	private static void StaffWorkMinus(Actor x)
	{
		x.StaffOn--;
		if (x.StaffOn == -1)
		{
			x.StaffOn = 23;
		}
		x.StaffOff = (x.StaffOn + 4) % 24;
		if (!x.isActiveAndEnabled)
		{
			SDateTime sdateTime = SDateTime.Now();
			SDateTime time = new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), x.StaffOn - 1, sdateTime.Day + 1, sdateTime.Month, sdateTime.Year);
			GameSettings.Instance.sActorManager.AddToAwaiting(x, time, true, true);
		}
	}

	private static void SelectServerFallback(IServerHost x)
	{
		List<IServerHost> servers = GameSettings.Instance.GetAllServers().ToList<IServerHost>();
		servers.Remove(x);
		if (servers.Count > 0)
		{
			SelectorController.Instance.selectWindow.Show("Select fallback", from z in servers
			select z.ServerName, delegate(int i)
			{
				x.Fallback = ((i != -1) ? servers[i] : null);
			}, true, true, true, false, null);
		}
	}

	private static void UploadWorkshopItem(IWorkshopItem item)
	{
		if (item.CanUpload)
		{
			string text = SteamWorkshop.CheckValid(item.GetValidExts(), item.FolderPath());
			if (text == null)
			{
				SteamWorkshop.Instance.UploadMod(item);
			}
			else
			{
				WindowManager.SpawnDialog(text, true, DialogWindow.DialogType.Error);
			}
		}
		else
		{
			WindowManager.SpawnDialog("SteamUploadDenied".Loc(), true, DialogWindow.DialogType.Error);
		}
	}

	private static void DeleteBlueprint(BuildingPrefab item)
	{
		if (item.SteamID == null)
		{
			WindowManager.Instance.ShowMessageBox("BlueprintDeleteConfirmation".Loc(new object[]
			{
				item.ItemTitle
			}), false, DialogWindow.DialogType.Warning, delegate
			{
				try
				{
					Directory.Delete(item.FolderPath(), true);
					GameData.Prefabs.Remove(item);
					if (HUD.Instance != null)
					{
						HUD.Instance.blueprint.Refresh();
					}
				}
				catch (Exception)
				{
				}
			}, null, null);
		}
		else
		{
			WindowManager.SpawnDialog("BlueprintSteamDelete".Loc(), true, DialogWindow.DialogType.Error);
		}
	}

	private static void FreezeWaypoint(WayPointEditorWindow.WayPoint x)
	{
		int num = WayPointEditorWindow.Instance.WayPointList.Items.IndexOf(x);
		if (WayPointEditorWindow.Instance.CurrentWayPoint != num)
		{
			WayPointEditorWindow.Instance.FreezeFrame = true;
			WayPointEditorWindow.Instance.CurrentWayPoint = num;
		}
		else
		{
			WayPointEditorWindow.Instance.FreezeFrame = !WayPointEditorWindow.Instance.FreezeFrame;
		}
	}

	private static void SellStock(Stock stock)
	{
		if (Array.IndexOf<Stock>(stock.TargetCompany.Stocks, stock) > 0)
		{
			KeyValuePair<Company, float> buyer = GameSettings.Instance.simulation.FindBuyer(0u, stock.TargetCompany.ID, stock.CurrentWorth);
			if (buyer.Key != null)
			{
				if (buyer.Value < stock.CurrentWorth)
				{
					WindowManager.Instance.ShowMessageBox("StockLowBid".Loc(new object[]
					{
						buyer.Key.Name,
						buyer.Value.Currency(true),
						stock.InitialPrice.Currency(true),
						stock.CurrentWorth.Currency(true)
					}), true, DialogWindow.DialogType.Question, delegate
					{
						stock.TargetCompany.TradeStock(buyer.Key, stock, buyer.Value);
					}, null, null);
				}
				else
				{
					stock.TargetCompany.TradeStock(buyer.Key, stock, buyer.Value);
				}
			}
			else
			{
				WindowManager.Instance.ShowMessageBox("NoStockInterest".Loc(), false, DialogWindow.DialogType.Information);
			}
		}
	}

	public EventList<object> Items
	{
		get
		{
			return this._items;
		}
		set
		{
			this.SaveSelected();
			this._items = value;
			this._items.OnChange = delegate
			{
				this.UpdateActiveList(true);
			};
			this._items.PreChange = delegate
			{
				if (this.SelectedBeforeChange == null)
				{
					this.SelectedBeforeChange = this.GetSelected<object>();
				}
			};
			Action onChange = this.Selected.OnChange;
			this.Selected.OnChange = null;
			this.Selected.Clear();
			this.Selected.OnChange = onChange;
			this.dirty = true;
			this.UpdateActiveList(true);
		}
	}

	public RectTransform rectTransform
	{
		get
		{
			return base.GetComponent<RectTransform>();
		}
	}

	public GUIColumn this[string key]
	{
		get
		{
			this.Initialize();
			return this.GUIColumns.FirstOrDefault((GUIColumn x) => x.name.Equals(key));
		}
		set
		{
		}
	}

	public void UpdateActiveList(bool forceRefresh = true)
	{
		bool flag = forceRefresh;
		if (forceRefresh)
		{
			this.SaveSelected();
			this.ActualItems.Clear();
		}
		bool flag2 = false;
		for (int i = 0; i < this.GUIColumns.Count; i++)
		{
			GUIColumn guicolumn = this.GUIColumns[i];
			if (guicolumn.FilterActive && guicolumn.Filter != GUIListView.FilterType.None)
			{
				flag2 = true;
				break;
			}
		}
		if (flag2)
		{
			if (!forceRefresh)
			{
				this.SaveSelected();
				this.ActualItems.Clear();
				flag = true;
			}
			for (int j = 0; j < this.Items.Count; j++)
			{
				bool flag3 = true;
				object item = this.Items[j];
				for (int k = 0; k < this.GUIColumns.Count; k++)
				{
					GUIColumn column = this.GUIColumns[k];
					if (!this.FilterItem(item, column))
					{
						flag3 = false;
						break;
					}
				}
				if (flag3)
				{
					this.ActualItems.Add(item);
				}
			}
		}
		else if (forceRefresh)
		{
			this.ActualItems.AddRange(this.Items);
		}
		if (flag)
		{
			this.dirty = true;
		}
		this.NextRefresh = Time.realtimeSinceStartup + UnityEngine.Random.Range(5f, 10f);
	}

	public bool FilterItem(object item, GUIColumn column)
	{
		if (!column.FilterActive || column.Filter == GUIListView.FilterType.None)
		{
			return true;
		}
		object obj = (column.GetFilterValue != null) ? column.GetFilterValue(item) : ((column.Filter != GUIListView.FilterType.Name) ? item : column.Label(item));
		switch (column.Filter)
		{
		case GUIListView.FilterType.Number:
		{
			float num = (float)obj;
			return num >= column.FilterNumber[0] && num <= column.FilterNumber[1] + 0.01f;
		}
		case GUIListView.FilterType.Name:
			return column.FilterName.Contains((string)obj);
		case GUIListView.FilterType.Date:
		{
			int num2 = ((SDateTime)obj).ToInt();
			return (float)num2 >= column.FilterNumber[0] && (float)num2 <= column.FilterNumber[1];
		}
		case GUIListView.FilterType.Bool:
			return (bool)obj == column.FilterBool;
		case GUIListView.FilterType.Bitmask:
		{
			int num3 = (int)obj;
			return (num3 & column.FilterMask) > 0;
		}
		default:
			return true;
		}
	}

	public int Scroll
	{
		get
		{
			return Mathf.FloorToInt(this.scrollbar.value * (float)Mathf.Max(0, this.ActualItems.Count - Mathf.FloorToInt(this.ContentPanel.GetComponent<RectTransform>().rect.height / 24f) + 1));
		}
	}

	public void KeepWithin()
	{
	}

	private void Awake()
	{
		if (this._items == null)
		{
			this.Items = new EventList<object>();
		}
	}

	public void Initialize()
	{
		if (!this.Initialized)
		{
			this.Initialized = true;
			foreach (string column in this.Columns)
			{
				this.AddColumn(column);
			}
			this.UpdateElements();
			this.lastSize = this.rectTransform.rect;
			this.Selected.OnChange = delegate
			{
				if (this.OnSelectChange != null)
				{
					this.OnSelectChange(this.LastSelectDirect);
					this.LastSelectDirect = false;
				}
			};
		}
	}

	private void Start()
	{
		this.Initialize();
	}

	private void AddColumn(string column)
	{
		GUIListView.ColumnDef columnDef = this.ColumnDefinitions[column];
		GUIListView.FilterType filterType;
		if (!WindowManager.Instance.MainScene)
		{
			filterType = GUIListView.FilterType.None;
		}
		else
		{
			GUIListView.FilterType? filterType2 = columnDef.FilterType;
			filterType = ((filterType2 == null) ? GUIListView.FilterType.None : filterType2.Value);
		}
		GUIListView.FilterType filterType3 = filterType;
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>((filterType3 == GUIListView.FilterType.None) ? this.ColumnPrefab : this.FilterColumnPrefab);
		gameObject.transform.SetParent(this.ContentPanel.transform, false);
		GUIColumn component = gameObject.GetComponent<GUIColumn>();
		component.Filter = filterType3;
		component.SetVariable = columnDef.SetValue;
		component.GetFilterValue = columnDef.FilterConversion;
		component.HeaderValue = columnDef.Header;
		if (columnDef.Action != null)
		{
			component.Action = columnDef.Action;
			component.Type = GUIColumn.ColumnType.Action;
			component.Header.text = "Action".Loc();
		}
		else
		{
			component.Label = columnDef.Label;
			component.Comparison = columnDef.Comparison;
			GUIColumn guicolumn = component;
			GUIColumn.ColumnType? typeOverride = columnDef.TypeOverride;
			guicolumn.Type = ((typeOverride == null) ? GUIColumn.ColumnType.Label : typeOverride.Value);
			component.Header.text = ((!this.IgnoreTranslation) ? component.HeaderValue.Loc() : component.HeaderValue.LocDef(component.HeaderValue));
			component.ContinuallyUpdate = columnDef.Volatile;
		}
		component.Parent = this;
		float preferredWidth = 0f;
		if (Options.GetColumnWidth(column, out preferredWidth))
		{
			component.layoutElement.preferredWidth = preferredWidth;
		}
		else if (columnDef.Width != null)
		{
			component.layoutElement.preferredWidth = columnDef.Width.Value + (float)((filterType3 == GUIListView.FilterType.None) ? 0 : 8);
		}
		component.name = column;
		this.GUIColumns.Add(component);
	}

	public void AddColumn(string label, Func<object, object> content, Comparison<object> comparison, bool isVolatile, float defaultWidth = 128f)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ColumnPrefab);
		gameObject.transform.SetParent(this.ContentPanel.transform, false);
		GUIColumn component = gameObject.GetComponent<GUIColumn>();
		component.HeaderValue = label;
		component.Label = content;
		component.Comparison = comparison;
		component.Type = GUIColumn.ColumnType.Label;
		component.Header.text = component.HeaderValue.Loc();
		component.ContinuallyUpdate = isVolatile;
		component.Parent = this;
		component.layoutElement.preferredWidth = defaultWidth;
		component.name = label;
		this.GUIColumns.Add(component);
	}

	public void AddFilterColumn(string label, Func<object, object> content, Comparison<object> comparison, bool isVolatile, GUIListView.FilterType filterType, Func<object, object> filterConversion, float defaultWidth = 128f)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ColumnPrefab);
		gameObject.transform.SetParent(this.ContentPanel.transform, false);
		GUIColumn component = gameObject.GetComponent<GUIColumn>();
		component.HeaderValue = label;
		component.Label = content;
		component.Comparison = comparison;
		component.Type = GUIColumn.ColumnType.Label;
		component.Header.text = component.HeaderValue.Loc();
		component.ContinuallyUpdate = isVolatile;
		component.Filter = filterType;
		component.GetFilterValue = filterConversion;
		component.Parent = this;
		component.layoutElement.preferredWidth = defaultWidth;
		component.name = label;
		this.GUIColumns.Add(component);
	}

	public void AddActionColumn(string label, Action<object> action, bool isVolatile, float defaultWidth = 128f)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ColumnPrefab);
		gameObject.transform.SetParent(this.ContentPanel.transform, false);
		GUIColumn component = gameObject.GetComponent<GUIColumn>();
		component.HeaderValue = label;
		component.Action = action;
		component.Type = GUIColumn.ColumnType.Action;
		component.Header.text = "Action".Loc();
		component.Parent = this;
		component.layoutElement.preferredWidth = defaultWidth;
		component.name = label;
		this.GUIColumns.Add(component);
	}

	public void ResetHeaders()
	{
		this.GUIColumns.ForEach(delegate(GUIColumn x)
		{
			if (x.Type != GUIColumn.ColumnType.Action)
			{
				x.Header.text = x.HeaderValue.Loc();
			}
		});
	}

	public void UpdateElements()
	{
		if (this.LastSort != null)
		{
			this.LastSort.Sort(!this.LastSort.SortAsc, false);
		}
		foreach (GUIColumn guicolumn in from x in this.GUIColumns
		where x != null && x.gameObject.activeInHierarchy
		select x)
		{
			guicolumn.UpdateElements();
		}
		this.UpdateSelected();
	}

	public void UpdateSelected()
	{
		foreach (GUIColumn guicolumn in from x in this.GUIColumns
		where x != null
		select x)
		{
			guicolumn.UpdateSelected();
		}
	}

	public void UpdateInUI()
	{
		foreach (GUIColumn guicolumn in from x in this.GUIColumns
		where x != null && x.gameObject.activeInHierarchy
		select x)
		{
			guicolumn.UpdateElements();
			guicolumn.UpdateSelected();
		}
	}

	public void SaveSelected()
	{
		if (this.SelectedBeforeChange == null)
		{
			this.SelectedBeforeChange = this.GetSelected<object>();
		}
		else if (!this.dirty)
		{
			this.SelectedBeforeChange = this.SelectedBeforeChange.Concat(this.GetSelected<object>().AsEnumerable<object>()).Distinct<object>().ToArray<object>();
		}
	}

	private int GetIndexOf(object obj)
	{
		for (int i = 0; i < this.ActualItems.Count; i++)
		{
			if (this.ActualItems[i] == obj)
			{
				return i;
			}
		}
		return -1;
	}

	private void Update()
	{
		this.scrollbar.size = Mathf.Min(1f, this.ContentPanel.GetComponent<RectTransform>().rect.height / ((float)(this.ActualItems.Count + 1) * 24f));
		this.scrollbar.numberOfSteps = this.ActualItems.Count + 2 - Mathf.FloorToInt(this.ContentPanel.GetComponent<RectTransform>().rect.height / 24f);
		if (this.lastSize != this.rectTransform.rect)
		{
			this.UpdateElements();
		}
		else if (this.dirty)
		{
			Action onChange = this.Selected.OnChange;
			int count = this.Selected.Count;
			this.Selected.OnChange = null;
			if (this.SelectedBeforeChange != null)
			{
				this.Selected.Clear();
				this.Selected.AddRange((from x in this.SelectedBeforeChange
				select this.GetIndexOf(x) into x
				where x > -1
				select x).Distinct<int>());
				this.SelectedBeforeChange = null;
			}
			foreach (int num in this.Selected.ToList<int>())
			{
				if (num >= this.ActualItems.Count)
				{
					this.Selected.Remove(num);
				}
			}
			this.Selected.OnChange = onChange;
			if (onChange != null && count != this.Selected.Count)
			{
				onChange();
			}
			this.UpdateElements();
			this.dirty = false;
		}
		else if (Time.realtimeSinceStartup >= this.NextRefresh)
		{
			this.UpdateActiveList(false);
		}
		else if (this.ActualItems.Count > 0)
		{
			this._updateIDX = (this._updateIDX + 1) % this.ActualItems.Count;
			this.UpdateAtI(this._updateIDX);
		}
		this.lastSize = this.rectTransform.rect;
	}

	private void UpdateAtI(int idx)
	{
		bool flag = false;
		idx %= this.ActualItems.Count;
		object obj = this.ActualItems[idx];
		bool flag2 = false;
		for (int i = 0; i < this.GUIColumns.Count; i++)
		{
			GUIColumn guicolumn = this.GUIColumns[i];
			if (guicolumn.FilterActive && guicolumn.Filter != GUIListView.FilterType.None)
			{
				flag2 = true;
				break;
			}
		}
		bool flag3 = true;
		if (flag2)
		{
			for (int j = 0; j < this.GUIColumns.Count; j++)
			{
				GUIColumn column = this.GUIColumns[j];
				if (!this.FilterItem(obj, column))
				{
					flag3 = false;
					break;
				}
			}
			if (!flag3)
			{
				this.Selected.Remove(idx);
				this.ActualItems.Remove(obj);
				flag = true;
			}
		}
		if (flag3 && this.LastSort != null && idx > 0)
		{
			object obj2 = this.ActualItems[idx - 1];
			int num = this.LastSort.Comparison(obj, obj2);
			if ((this.LastSort.SortAsc && num > 0) || (!this.LastSort.SortAsc && num < 0))
			{
				this.ActualItems[idx] = obj2;
				this.ActualItems[idx - 1] = obj;
				flag = true;
				bool flag4 = this.Selected.Contains(idx);
				bool flag5 = this.Selected.Contains(idx - 1);
				if (flag4 ^ flag5)
				{
					if (flag4)
					{
						this.Selected.Remove(idx);
						this.Selected.Add(idx - 1);
					}
					else
					{
						this.Selected.Remove(idx - 1);
						this.Selected.Add(idx);
					}
				}
			}
		}
		if (flag)
		{
			this.UpdateInUI();
		}
	}

	public void OnScroll(BaseEventData data)
	{
		int scroll = this.Scroll;
		PointerEventData pointerEventData = (PointerEventData)data;
		this.scrollbar.value -= pointerEventData.scrollDelta.y / (float)(this.ActualItems.Count + 1 - Mathf.FloorToInt(this.ContentPanel.GetComponent<RectTransform>().rect.height / 24f));
		if (this.Scroll != scroll)
		{
			this.UpdateElements();
		}
	}

	public void KeepIdxInView(int idx)
	{
		int num = this.ActualItems.Count + 1 - Mathf.FloorToInt(this.ContentPanel.GetComponent<RectTransform>().rect.height / 24f);
		float num2 = Mathf.Clamp01((float)idx / (float)num);
		if (this.scrollbar.value > num2)
		{
			this.scrollbar.value = num2;
			return;
		}
		num2 = (float)idx / (float)num;
		float num3 = Mathf.Floor(this.ContentPanel.GetComponent<RectTransform>().rect.height / 24f - 2f) / (float)num;
		if (this.scrollbar.value + num3 < num2)
		{
			this.scrollbar.value = num2 - num3;
		}
	}

	public void ClearSelected()
	{
		this.Selected.Clear();
		this.SelectedBeforeChange = this.GetSelected<object>();
		this.UpdateSelected();
	}

	public void Select(int i)
	{
		if (this.MultiSelect && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
		{
			if (this.Selected.Contains(i))
			{
				this.Selected.Remove(i);
			}
			else
			{
				this.Selected.Add(i);
			}
		}
		else if (this.MultiSelect && this.Selected.Count > 0 && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
		{
			int num = this.Selected[0];
			int item = num;
			if (i > num)
			{
				int num2 = i;
				i = num;
				num = num2;
			}
			Action onChange = this.Selected.OnChange;
			this.Selected.OnChange = null;
			this.Selected.Clear();
			for (int j = 0; j < this.ActualItems.Count; j++)
			{
				if (j >= i && j <= num)
				{
					this.Selected.Add(j);
				}
			}
			this.Selected.Remove(item);
			this.Selected.Insert(0, item);
			if (onChange != null)
			{
				onChange();
			}
			this.Selected.OnChange = onChange;
		}
		else
		{
			bool lastSelectDirect = this.LastSelectDirect;
			this.LastSelectDirect = false;
			this.Selected.Clear();
			this.LastSelectDirect = lastSelectDirect;
			this.Selected.Add(i);
		}
		this.SelectedBeforeChange = this.GetSelected<object>();
		this.UpdateSelected();
	}

	public void Select(object obj)
	{
		int num = this.ActualItems.IndexOf(obj);
		if (num > -1)
		{
			this.Select(num);
		}
	}

	public T[] GetSelected<T>()
	{
		return (from x in this.Selected
		where x < this.ActualItems.Count
		select this.ActualItems[x]).Cast<T>().ToArray<T>();
	}

	internal void ResetScroll()
	{
		this.scrollbar.value = 0f;
		this.UpdateElements();
	}

	public void ExportCSV()
	{
	}

	public Dictionary<string, GUIListView.ColumnDef> ColumnDefinitions;

	[NonSerialized]
	private EventList<object> _items;

	[NonSerialized]
	public List<object> ActualItems;

	public readonly EventList<int> Selected;

	public bool MultiSelect;

	public Rect lastSize;

	public GameObject ColumnPrefab;

	public GameObject FilterColumnPrefab;

	public GameObject ContentPanel;

	public string[] Columns;

	[NonSerialized]
	public List<GUIColumn> GUIColumns;

	public GUIColumn LastSort;

	public Scrollbar scrollbar;

	private bool dirty;

	public bool LastSelectDirect;

	public Action<bool> OnSelectChange;

	public bool IgnoreTranslation;

	public float NextRefresh;

	[NonSerialized]
	private object[] SelectedBeforeChange;

	[NonSerialized]
	private bool Initialized;

	private int _updateIDX;

	[CompilerGenerated]
	private static Action<Stock> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<Team, object> <>f__mg$cache1;

	[CompilerGenerated]
	private static Action<Actor> <>f__mg$cache2;

	[CompilerGenerated]
	private static Action<SaveGame> <>f__mg$cache3;

	[CompilerGenerated]
	private static Action<ModPackage> <>f__mg$cache4;

	[CompilerGenerated]
	private static Action<IServerHost> <>f__mg$cache5;

	[CompilerGenerated]
	private static Action<IWorkshopItem> <>f__mg$cache6;

	[CompilerGenerated]
	private static Action<BuildingPrefab> <>f__mg$cache7;

	[CompilerGenerated]
	private static Action<WayPointEditorWindow.WayPoint> <>f__mg$cache8;

	public class ColumnDef
	{
		public GUIColumn.ColumnType? TypeOverride;

		public GUIListView.FilterType? FilterType;

		public string Header;

		public Func<object, object> Label;

		public Comparison<object> Comparison;

		public Action<object> Action;

		public Action<object, object> SetValue;

		public Func<object, object> FilterConversion;

		public float? Width;

		public bool Volatile;
	}

	public class ColumnDefinition<T> : GUIListView.ColumnDef
	{
		public ColumnDefinition(string header, Func<T, object> label, Func<T, string> comparison, bool vola, float? width = null, GUIListView.FilterType? filterType = null, Func<T, object> filter = null, GUIColumn.ColumnType? typeOverride = null, Action<T, object> setValue = null)
		{
			this.Header = header;
			this.Label = ((object x) => label((T)((object)x)));
			this.Comparison = ((object x, object y) => Utilities.CompareString<T>(comparison, (T)((object)x), (T)((object)y)));
			this.Volatile = vola;
			this.Width = width;
			this.FilterType = filterType;
			if (filter != null)
			{
				this.FilterConversion = ((object x) => filter((T)((object)x)));
			}
			this.TypeOverride = typeOverride;
			if (setValue != null)
			{
				this.SetValue = delegate(object x, object y)
				{
					setValue((T)((object)x), y);
				};
			}
		}

		public ColumnDefinition(string header, Func<T, float> label, bool vola, float? width = null, bool currency = true)
		{
			this.Header = header;
			if (currency)
			{
				this.Label = ((object x) => label((T)((object)x)).Currency(true));
				this.Comparison = ((object x, object y) => Utilities.CompareNumber<T>(label, (T)((object)x), (T)((object)y)));
				this.FilterConversion = ((object x) => label((T)((object)x)).CurrencyMul());
			}
			else
			{
				this.Label = ((object x) => (label((T)((object)x)) * 100f).ToString("F") + "%");
				this.Comparison = ((object x, object y) => Utilities.CompareNumber<T>(label, (T)((object)x), (T)((object)y)));
				this.FilterConversion = ((object x) => label((T)((object)x)) * 100f);
			}
			this.Volatile = vola;
			this.Width = width;
			this.FilterType = new GUIListView.FilterType?(GUIListView.FilterType.Number);
		}

		public ColumnDefinition(string header, Func<T, int> label, bool vola, float? width = null)
		{
			this.Header = header;
			this.Label = ((object x) => label((T)((object)x)).ToString("N0"));
			this.Comparison = ((object x, object y) => Utilities.CompareNumber<T>((T z) => (float)label(z), (T)((object)x), (T)((object)y)));
			this.Volatile = vola;
			this.Width = width;
			this.FilterType = new GUIListView.FilterType?(GUIListView.FilterType.Number);
			this.FilterConversion = ((object x) => (float)label((T)((object)x)));
		}

		public ColumnDefinition(string header, Func<T, uint> label, bool vola, float? width = null)
		{
			this.Header = header;
			this.Label = ((object x) => label((T)((object)x)).ToString("N0"));
			this.Comparison = ((object x, object y) => Utilities.CompareNumber<T>((T z) => label(z), (T)((object)x), (T)((object)y)));
			this.Volatile = vola;
			this.Width = width;
			this.FilterType = new GUIListView.FilterType?(GUIListView.FilterType.Number);
			this.FilterConversion = ((object x) => label((T)((object)x)));
		}

		public ColumnDefinition(string header, Func<T, SDateTime> label, bool vola, float? width = null)
		{
			this.Header = header;
			this.Label = ((object x) => label((T)((object)x)).ToCompactString());
			this.Comparison = ((object x, object y) => Utilities.CompareNumber<T>((T z) => (float)label(z).ToInt(), (T)((object)x), (T)((object)y)));
			this.Volatile = vola;
			this.Width = width;
			this.FilterType = new GUIListView.FilterType?(GUIListView.FilterType.Date);
			this.FilterConversion = ((object x) => label((T)((object)x)));
		}

		public ColumnDefinition(string header, Func<T, bool> label, bool vola, float? width = null)
		{
			this.Header = header;
			this.Label = ((object x) => label((T)((object)x)).YesNo());
			this.Comparison = ((object x, object y) => Utilities.CompareNumber<T>((T z) => (float)((!label(z)) ? 0 : 1), (T)((object)x), (T)((object)y)));
			this.Volatile = vola;
			this.Width = width;
			this.FilterType = new GUIListView.FilterType?(GUIListView.FilterType.Bool);
			this.FilterConversion = ((object x) => label((T)((object)x)));
		}

		public ColumnDefinition(string header, Func<T, string> label, bool vola, float? width = null, bool filter = true)
		{
			this.Header = header;
			this.Label = ((object x) => label((T)((object)x)));
			this.Comparison = ((object x, object y) => Utilities.CompareString<T>((T z) => label(z), (T)((object)x), (T)((object)y)));
			this.Volatile = vola;
			this.Width = width;
			if (filter)
			{
				this.FilterType = new GUIListView.FilterType?(GUIListView.FilterType.Name);
			}
		}

		public ColumnDefinition(string header, Func<T, object> label, Func<T, float> comparison, bool vola, float? width = null, GUIListView.FilterType? filterType = null, Func<T, object> filter = null, GUIColumn.ColumnType? typeOverride = null, Action<T, object> setValue = null)
		{
			this.Header = header;
			this.Label = ((object x) => label((T)((object)x)));
			this.Comparison = ((object x, object y) => Utilities.CompareNumber<T>(comparison, (T)((object)x), (T)((object)y)));
			this.Volatile = vola;
			this.Width = width;
			this.FilterType = filterType;
			if (filter != null)
			{
				this.FilterConversion = ((object x) => filter((T)((object)x)));
			}
			this.TypeOverride = typeOverride;
			if (setValue != null)
			{
				this.SetValue = delegate(object x, object y)
				{
					setValue((T)((object)x), y);
				};
			}
		}

		public ColumnDefinition(string header, Action<T> action, float? width = null)
		{
			this.Header = header;
			this.Action = delegate(object x)
			{
				action((T)((object)x));
			};
			this.Width = width;
			this.Volatile = false;
		}
	}

	public enum FilterType
	{
		None,
		Number,
		Name,
		Date,
		Bool,
		Bitmask
	}
}
