﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WageWindow : MonoBehaviour
{
	public Actor[] employees
	{
		get
		{
			return this.List.GetSelected<Actor>();
		}
	}

	public float CurrentWage
	{
		get
		{
			return this.GetWageChange(this.employees[0], this.Wage.value);
		}
	}

	private float GetWageChange(Actor act, float percent)
	{
		float num = act.employee.Worth(-2, true);
		return WageWindow.GetMinSalary(act, (percent <= 0.5f) ? percent.MapRange(0f, 0.5f, num * WageWindow.MinChange, num, false) : percent.MapRange(0.5f, 1f, num, num * WageWindow.MaxChange, false), this.Forced);
	}

	public float GetWageDiff(Actor[] emps, float percent)
	{
		float num = 0f;
		foreach (Actor act in emps)
		{
			num += this.GetWageChange(act, percent);
		}
		return num;
	}

	public float GetMinimum(Actor[] emps)
	{
		float num = 0f;
		for (int i = 0; i < emps.Length; i++)
		{
			num += emps[i].GetBenefitValue("Minimum raise");
		}
		return num;
	}

	public void UpdateSalary()
	{
		if (!this._canUpdateSalary)
		{
			return;
		}
		this._canUpdateSalary = false;
		Actor[] employees = this.employees;
		if (employees.Length == 1)
		{
			this.SalaryLabel.text = this.CurrentWage.Currency(true);
			if (this.Wage.value < 0.5f)
			{
				this.SalaryLabel.color = Color.Lerp(this.Red, this.Black, this.Wage.value / 0.5f);
			}
			else if (this.Wage.value > 0.5f)
			{
				this.SalaryLabel.color = Color.Lerp(this.Black, this.Green, (this.Wage.value - 0.5f) / 0.5f);
			}
			else
			{
				this.SalaryLabel.color = this.Black;
			}
		}
		else if (employees.Length > 1)
		{
			for (int i = 0; i < employees.Length; i++)
			{
				employees[i].NegotiateSalary = false;
			}
			this.SalaryLabel.text = Mathf.Lerp(50f, 150f, this.Wage.value).ToString("N2") + "% (" + this.GetWageDiff(employees, this.Wage.value).Currency(true) + ")";
			if (this.Wage.value < 0.5f)
			{
				this.SalaryLabel.color = Color.Lerp(this.Red, this.Black, this.Wage.value / 0.5f);
			}
			else if (this.Wage.value > 0.5f)
			{
				this.SalaryLabel.color = Color.Lerp(this.Black, this.Green, (this.Wage.value - 0.5f) / 0.5f);
			}
			else
			{
				this.SalaryLabel.color = this.Black;
			}
		}
		float minimum = this.GetMinimum(employees);
		if (minimum > 0f)
		{
			if (this.Forced)
			{
				Text salaryLabel = this.SalaryLabel;
				string text = salaryLabel.text;
				salaryLabel.text = string.Concat(new string[]
				{
					text,
					" (",
					"Minimum raise".Loc(),
					": ",
					minimum.Currency(true),
					")"
				});
			}
			else
			{
				Text salaryLabel2 = this.SalaryLabel;
				salaryLabel2.text = salaryLabel2.text + " (" + "Minimum raise".Loc() + ")";
			}
		}
		this._canUpdateSalary = true;
	}

	public void AcceptSalary()
	{
		Actor[] e = this.employees;
		if (e.Length == 1)
		{
			Actor actor = this.employees[0];
			actor.NegotiateSalary = false;
			actor.employee.ChangeSalary(this.CurrentWage, actor.employee.Worth(-2, true), actor, true);
			this.List.Selected.Clear();
			this.List.Items.Remove(actor);
			if (this.List.Items.Count > 0)
			{
				this.List.Select(0);
			}
			else
			{
				this.Window.Close();
			}
		}
		else if (e.Length > 1)
		{
			for (int i = 0; i < e.Length; i++)
			{
				Actor actor2 = e[i];
				actor2.NegotiateSalary = false;
				actor2.employee.ChangeSalary(this.GetWageChange(actor2, this.Wage.value), actor2.employee.Worth(-2, true), actor2, true);
			}
			this.List.Items.RemoveAll((object x) => e.Contains(x));
			this.List.Selected.Clear();
			if (this.List.Items.Count > 0)
			{
				this.List.Select(0);
			}
			else
			{
				this.Window.Close();
			}
		}
	}

	public void Show(bool forced)
	{
		this.Forced = forced;
		this.Window.Show();
		if (this.Forced)
		{
			GameSettings.ForcePause = true;
			GameSettings.FreezeGame = true;
		}
		this.List.Initialize();
		this.List.Selected.Clear();
		this.List.Select(0);
	}

	private void Start()
	{
		this.List.OnSelectChange = delegate(bool d)
		{
			Actor[] employees = this.employees;
			if (employees.Length == 1)
			{
				employees[0].Snapshot();
				this.Portrait.gameObject.SetActive(true);
				this.WagePanel.offsetMin = new Vector2(142.5f, this.WagePanel.offsetMin.y);
			}
			else if (employees.Length > 1)
			{
				this.Portrait.gameObject.SetActive(false);
				this.WagePanel.offsetMin = new Vector2(2f, this.WagePanel.offsetMin.y);
			}
			this.Wage.value = 0.5f;
			this.UpdateSalary();
		};
		this.Window.OnClose = delegate
		{
			this.List.Items.Clear();
			if (this.Forced)
			{
				GameSettings.ForcePause = false;
			}
		};
	}

	public static float GetMinSalary(Actor act, float wanted, bool forced)
	{
		float benefitValue = act.GetBenefitValue("Minimum raise");
		if (benefitValue > 0f)
		{
			return (!forced) ? Mathf.Max(act.GetRealSalary(), wanted) : Mathf.Max(act.GetRealSalary() + benefitValue, wanted);
		}
		return wanted;
	}

	public void Close()
	{
		if (this.Forced && this.List.Items.Count > 0)
		{
			float[] source = (from x in this.List.Items.OfType<Actor>()
			select WageWindow.GetMinSalary(x, x.employee.Worth(-2, true), this.Forced) - x.employee.Salary).ToArray<float>();
			if (source.Sum().CurrencyMul() > 0.999f)
			{
				this.Window.gameObject.SetActive(false);
				DialogWindow dialog = WindowManager.SpawnDialog();
				dialog.Show("AcceptAllWagesWarning".Loc(new object[]
				{
					source.Average().Currency(true),
					source.Sum().Currency(true)
				}), false, DialogWindow.DialogType.Warning, new KeyValuePair<string, Action>[]
				{
					new KeyValuePair<string, Action>("Yes", delegate
					{
						foreach (Actor actor2 in this.List.Items.OfType<Actor>())
						{
							float minSalary2 = WageWindow.GetMinSalary(actor2, actor2.employee.Worth(-2, true), this.Forced);
							actor2.employee.ChangeSalary(minSalary2, minSalary2, actor2, true);
							actor2.NegotiateSalary = false;
						}
						this.Window.gameObject.SetActive(true);
						this.Window.Close();
						dialog.Window.Close();
					}),
					new KeyValuePair<string, Action>("No", delegate
					{
						this.Window.gameObject.SetActive(true);
						dialog.Window.Close();
					})
				});
			}
			else
			{
				foreach (Actor actor in this.List.Items.OfType<Actor>())
				{
					float minSalary = WageWindow.GetMinSalary(actor, actor.employee.Worth(-2, true), this.Forced);
					actor.employee.ChangeSalary(minSalary, minSalary, actor, true);
					actor.NegotiateSalary = false;
				}
				this.Window.Close();
			}
		}
		else
		{
			this.Window.Close();
		}
	}

	private void Update()
	{
		this.Panel.SetActive(this.List.Selected.Count > 0);
		this.CloseText.text = ((!this.Forced || this.List.Items.Count <= 0) ? "Close".Loc() : "AcceptAllWages".Loc());
	}

	public void SelectAll()
	{
		this.List.ClearSelected();
		int[] array = new int[this.List.Items.Count];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = i;
		}
		this.List.LastSelectDirect = true;
		this.List.Selected.AddRange(array);
		this.List.UpdateSelected();
	}

	public static float MinChange = 0.5f;

	public static float MaxChange = 1.5f;

	public GUIWindow Window;

	public GUIListView List;

	public RawImage Portrait;

	public Button CloseButton;

	public Text SalaryLabel;

	public Text CloseText;

	public GameObject Panel;

	public Slider Wage;

	public RectTransform WagePanel;

	public bool Forced = true;

	public Color Red;

	public Color Black;

	public Color Green;

	private bool _canUpdateSalary = true;
}
