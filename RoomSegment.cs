﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

public class RoomSegment : WallSnap, IRoomConnector
{
	public void FixDynamicWidth(float mult)
	{
		this.LightAddition *= mult;
		this.MiniWidth = mult;
		Matrix4x4 matrix4x = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(mult + 0.25f * (mult - 1f), 1f, 1f));
		foreach (GameObject gameObject in this.ScalableObjects)
		{
			gameObject.transform.localScale = matrix4x.MultiplyPoint(gameObject.transform.localScale);
		}
		matrix4x = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(mult, 1f, 1f));
		foreach (GameObject gameObject2 in this.ScalableObjectsEdgeToEdge)
		{
			gameObject2.transform.localScale = matrix4x.MultiplyPoint(gameObject2.transform.localScale);
		}
		foreach (GameObject gameObject3 in this.MovableObjects)
		{
			if (gameObject3.transform.localPosition.x < 0f)
			{
				float num = 0.5f + gameObject3.transform.localPosition.x;
				gameObject3.transform.localPosition = new Vector3(-mult / 2f + num, gameObject3.transform.localPosition.y, gameObject3.transform.localPosition.z);
			}
			else
			{
				float num2 = 0.5f - gameObject3.transform.localPosition.x;
				gameObject3.transform.localPosition = new Vector3(mult / 2f - num2, gameObject3.transform.localPosition.y, gameObject3.transform.localPosition.z);
			}
		}
		BoxCollider component = base.GetComponent<BoxCollider>();
		component.size = new Vector3(component.size.x * mult, component.size.y, component.size.z);
		this.WallWidth = mult;
	}

	public void UpdateParents()
	{
		List<WallEdge> es = this.WallPosition.Keys.ToList<WallEdge>();
		if (es.Count < 2)
		{
			this.ParentRooms[0] = null;
			this.ParentRooms[1] = null;
		}
		else
		{
			this.ParentRooms[0] = (from x in es[0].Links
			where x.Value == es[1]
			select x.Key).FirstOrDefault<Room>();
			this.ParentRooms[1] = (from x in es[1].Links
			where x.Value == es[0]
			select x.Key).FirstOrDefault<Room>();
			if (this.ParentRooms[0] != null)
			{
				this.ParentRooms[0].RefreshNoise();
			}
			else if (this.ParentRooms[1] != null)
			{
				this.ParentRooms[1].RefreshNoise();
			}
		}
	}

	public bool IsConnecter
	{
		get
		{
			return this.IsConnector;
		}
		set
		{
		}
	}

	public PathNode<Vector3> pathNode
	{
		get
		{
			return this._pathNode;
		}
		set
		{
			this._pathNode = value;
		}
	}

	public override string[] GetActions()
	{
		return RoomSegment.Actions;
	}

	public override string[] GetExtendedInfo()
	{
		return new string[]
		{
			Localization.GetFurniture(base.name, string.Empty)[0]
		};
	}

	public override string[] GetExtendedIconInfo()
	{
		return new string[]
		{
			"Door"
		};
	}

	public override string GetInfo()
	{
		return string.Empty;
	}

	private void Start()
	{
		base.name = base.name.Replace("(Clone)", string.Empty).Trim();
		if (!this.IsTemporary)
		{
			base.InitWritable();
			base.transform.position = new Vector3(base.transform.position.x, (float)(this.Floor * 2), base.transform.position.z);
			this.Children = base.GetComponentsInChildren<Renderer>().ToArray<Renderer>();
			this.InitPathNode();
			GameSettings.Instance.sRoomManager.RoomSegments.Add(this);
		}
		else
		{
			foreach (MeshFilter meshFilter in this.GetAllWallMeshes())
			{
				meshFilter.gameObject.AddComponent<MeshRenderer>().sharedMaterial = MaterialBank.Instance.BaseMat;
			}
		}
	}

	private void InitPathNode()
	{
		if (this.IsConnector && this.pathNode == null)
		{
			this.pathNode = new PathNode<Vector3>(new Vector3(base.transform.position.x, (float)(this.Floor * 2) + 0.5f, base.transform.position.z), this);
		}
	}

	private void FixedUpdate()
	{
		if (!this.IsTemporary)
		{
			bool flag = Utilities.InBasement(GameSettings.Instance.ActiveFloor) == Utilities.InBasement(this.Floor);
			bool flag2 = (!this.CheckWallDown() && this.Floor == GameSettings.Instance.ActiveFloor) || (flag && this.Floor > GameSettings.Instance.ActiveFloor);
			bool flag3 = flag && (this.IsAgainstExterior || this.Floor == GameSettings.Instance.ActiveFloor || (!Options.OpaqueGlass && this.Floor < GameSettings.Instance.ActiveFloor));
			if (this.IsShadowsOnly != flag2 || this.IsRendered != flag3)
			{
				this.IsRendered = flag3;
				this.IsShadowsOnly = flag2;
				for (int i = 0; i < this.Children.Length; i++)
				{
					Renderer renderer = this.Children[i];
					renderer.enabled = flag3;
					if (flag3)
					{
						renderer.shadowCastingMode = ((!flag2) ? ShadowCastingMode.On : ShadowCastingMode.ShadowsOnly);
					}
				}
			}
			if (Options.OpaqueGlass && this.HasGlass && this.IsRendered && !this.IsShadowsOnly)
			{
				bool flag4 = this.Floor == GameSettings.Instance.ActiveFloor;
				if (flag4 != this.IsTransparent)
				{
					this.IsTransparent = flag4;
					for (int j = 0; j < this.GlassRend.Length; j++)
					{
						this.GlassRend[j].sharedMaterial = ((!this.IsTransparent) ? this.OpaqueGlassMat : this.TransGlassMat);
					}
				}
			}
			else if (!this.IsTransparent)
			{
				this.IsTransparent = true;
				for (int k = 0; k < this.GlassRend.Length; k++)
				{
					this.GlassRend[k].sharedMaterial = this.TransGlassMat;
				}
			}
		}
	}

	private bool CheckWallDown()
	{
		if (GameSettings.WallsDown == GameSettings.WallState.LowNoSeg)
		{
			return false;
		}
		if (GameSettings.WallsDown == GameSettings.WallState.Low)
		{
			return !this.HideWithWalls;
		}
		if (GameSettings.WallsDown != GameSettings.WallState.Back)
		{
			return true;
		}
		if (!this.HideWithWalls)
		{
			return true;
		}
		Vector2 pos = this.FirstEdge.Pos;
		WallEdge secondEdge = base.GetSecondEdge();
		Vector2 pos2 = secondEdge.Pos;
		if (secondEdge.Links.ContainsValue(this.FirstEdge))
		{
			return true;
		}
		Quaternion a = Quaternion.LookRotation((pos - pos2).ToVector3(0f)) * Quaternion.Euler(0f, 90f, 0f);
		float f = Quaternion.Angle(a, CameraScript.Instance.mainCam.transform.rotation);
		return Mathf.Abs(f) > 90f;
	}

	public override bool ValidSnap(bool clone, HashSet<Room> destroy = null, bool keep = false)
	{
		WallEdge wallEdge = this.WallPosition.Keys.FirstOrDefault((WallEdge x) => x != this.FirstEdge);
		if (wallEdge == null)
		{
			return false;
		}
		Room room = this.FirstEdge.GetRoom(wallEdge);
		Room room2 = wallEdge.GetRoom(this.FirstEdge);
		bool flag = room != null && (destroy == null || !(keep ^ destroy.Contains(room)));
		bool flag2 = room2 != null && (destroy == null || !(keep ^ destroy.Contains(room2)));
		int num = 0;
		if (flag)
		{
			num += ((!room.Outdoors) ? -1 : 1);
		}
		if (flag2)
		{
			num += ((!room2.Outdoors) ? -1 : 1);
		}
		if (!clone && num > 0 == this.InsideSegment)
		{
			return false;
		}
		if (!flag && !flag2)
		{
			return false;
		}
		flag = (flag && !room.Outdoors);
		flag2 = (flag2 && !room2.Outdoors);
		return clone || !this.OnlyInterior || (flag && flag2);
	}

	public override bool EdgeChanged(WallEdge[] previous, bool clone)
	{
		if (this.FirstEdge != null)
		{
			this.Floor = this.FirstEdge.Floor;
		}
		this.InitPathNode();
		this.UpdateParents();
		if (!this.ValidSnap(clone, null, false))
		{
			base.DestroyMe();
			return false;
		}
		if (this.IsConnecter)
		{
			foreach (PathNode<Vector3> target in this.pathNode.GetConnections().ToList<PathNode<Vector3>>())
			{
				this.pathNode.RemoveConnection(target);
			}
			this.pathNode.GetConnections().Clear();
		}
		List<Room> list = new List<Room>();
		if (previous.Length >= 2)
		{
			Room room = previous[0].GetRoom(previous[1]);
			Room room2 = previous[1].GetRoom(previous[0]);
			if (room != null)
			{
				room.UpdateIsPrivate();
				list.Add(room);
			}
			if (room2 != null)
			{
				room2.UpdateIsPrivate();
				list.Add(room2);
			}
		}
		foreach (Room room3 in list)
		{
			if (this.IsConnector)
			{
				room3.DirtyPathNodes = true;
			}
			room3.RecalculateStateVariables(this.LightAddition > 0f);
		}
		if (list.Count == 1 && this.IsConnector && this.Floor == 0)
		{
			GameSettings.Instance.sRoomManager.Outside.DirtyPathNodes = true;
		}
		for (int i = 0; i < this.ParentRooms.Length; i++)
		{
			Room room4 = this.ParentRooms[i];
			if (room4 != null)
			{
				if (this.IsConnector)
				{
					room4.DirtyPathNodes = true;
				}
				room4.RecalculateStateVariables(this.LightAddition > 0f);
				room4.UpdateIsPrivate();
			}
		}
		if (this.IsConnector && this.Floor == 0)
		{
			GameSettings.Instance.sRoomManager.Outside.DirtyPathNodes = true;
		}
		this.IsAgainstExterior = base.GetAgainstExterior();
		return true;
	}

	public void UpdateBlocked()
	{
		HUD.Instance.BlockedDoorways.Remove(this);
		for (int i = 0; i < this.ParentRooms.Length; i++)
		{
			Room room = this.ParentRooms[i];
			if (room != null && room.GetNodeAt(this.GetOffsetPos(room, false).FlattenVector3()) == null)
			{
				HUD.Instance.BlockedDoorways.Add(this);
				break;
			}
		}
	}

	public Transform[] IntermediatePoints(Room from)
	{
		return null;
	}

	public MeshFilter[] GetWallMeshes(bool inside, WallEdge edge)
	{
		if (edge == this.FirstEdge)
		{
			return (from x in base.GetComponentsInChildren<MeshFilter>(true)
			where (inside && x.tag.Equals("InsideWall")) || (!inside && x.tag.Equals("OutsideWall"))
			select x).ToArray<MeshFilter>();
		}
		return (from x in base.GetComponentsInChildren<MeshFilter>(true)
		where (!inside && x.tag.Equals("InsideWall")) || (inside && x.tag.Equals("OutsideWall"))
		select x).ToArray<MeshFilter>();
	}

	public MeshFilter[] GetAllWallMeshes()
	{
		return (from x in base.GetComponentsInChildren<MeshFilter>(true)
		where x.tag.Equals("InsideWall") || x.tag.Equals("OutsideWall")
		select x).ToArray<MeshFilter>();
	}

	public void OnDestroy()
	{
		Room[] parentRooms = this.ParentRooms;
		base.ClearConnections();
		if (GameSettings.IsQuitting)
		{
			return;
		}
		if (GameSettings.Instance == null)
		{
			return;
		}
		if (HUD.Instance != null)
		{
			HUD.Instance.BlockedDoorways.Remove(this);
		}
		if (SelectorController.Instance != null && SelectorController.Instance.Selected.Contains(this))
		{
			SelectorController.Instance.ToggleRightClickMenu(false);
			SelectorController.Instance.Selected.Remove(this);
		}
		GameSettings.Instance.sRoomManager.RoomSegments.Remove(this);
		foreach (Room room in parentRooms)
		{
			if (room == null)
			{
				if (this.IsConnector && this.Floor == 0)
				{
					GameSettings.Instance.sRoomManager.Outside.DirtyPathNodes = true;
				}
			}
			else
			{
				if (this.LightAddition > 0f)
				{
					room.UpdateFurnitureWallNearness();
				}
				if (this.IsConnector)
				{
					room.DirtyPathNodes = true;
				}
				room.DirtyOuterMesh = true;
				room.DirtyInnerMesh = true;
				room.RecalculateStateVariables(this.LightAddition > 0f);
				room.RefreshNoise();
				room.UpdateIsPrivate();
			}
		}
	}

	public Vector3 GetOffsetPos(Room room, bool inverse = false)
	{
		if (this == null || base.transform == null || room == null)
		{
			return Vector3.zero;
		}
		WallEdge first = this.FirstEdge;
		if (first == null || this.WallPosition == null)
		{
			Vector3 vector = base.transform.forward.normalized * Room.WallOffset;
			return base.transform.position + ((!inverse) ? (-vector) : vector);
		}
		WallEdge wallEdge = this.WallPosition.Keys.FirstOrDefault((WallEdge x) => x != first);
		if (wallEdge == null)
		{
			Vector3 vector2 = base.transform.forward.normalized * Room.WallOffset;
			return base.transform.position + ((!inverse) ? (-vector2) : vector2);
		}
		WallEdge wallEdge2;
		if (room == GameSettings.Instance.sRoomManager.Outside)
		{
			if (first.Links.ContainsValue(wallEdge))
			{
				WallEdge first2 = wallEdge;
				wallEdge = first;
				first = first2;
			}
		}
		else if (!first.Links.TryGetValue(room, out wallEdge2) || wallEdge2 != wallEdge)
		{
			wallEdge2 = wallEdge;
			wallEdge = first;
			first = wallEdge2;
		}
		Vector2 vector3 = wallEdge.Pos - first.Pos;
		Vector3 vector4 = new Vector3(vector3.y, 0f, -vector3.x);
		Vector3 vector5 = vector4.normalized * Room.WallOffset;
		return base.transform.position + ((!inverse) ? (-vector5) : vector5);
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		if (this.DynamicWidth)
		{
			float mult = dictionary.Get<float>("DynamicWallWidth", this.WallWidth);
			this.FixDynamicWidth(mult);
		}
		base.DeserializeSnap(dictionary);
		if (this.FirstEdge != null)
		{
			this.Floor = this.FirstEdge.Floor;
		}
		this.UpdateParents();
		return this;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["Type"] = base.name;
		dictionary["DynamicWallWidth"] = this.WallWidth;
		base.SerializeSnap(dictionary);
	}

	public override string WriteName()
	{
		return "RoomSegment";
	}

	public void OnDrawGizmos()
	{
		if (this.IsConnector && this.pathNode != null)
		{
			foreach (PathNode<Vector3> pathNode in this.pathNode.GetConnections())
			{
				Gizmos.color = Color.red;
				Gizmos.DrawLine(this.pathNode.Point, pathNode.Point);
			}
			Gizmos.color = Color.yellow;
			Room room = (!(GameSettings.Instance != null)) ? null : GameSettings.Instance.sRoomManager.Outside;
			Vector3 offsetPos = this.GetOffsetPos(this.ParentRooms[0] ?? room, false);
			Gizmos.DrawSphere(offsetPos + Vector3.up * 0.5f, 0.1f);
			Gizmos.color = Color.cyan;
			offsetPos = this.GetOffsetPos(this.ParentRooms[1] ?? room, false);
			Gizmos.DrawSphere(offsetPos + Vector3.up * 0.5f, 0.1f);
		}
	}

	public Transform ObjectTransform
	{
		get
		{
			return base.transform;
		}
	}

	public bool IsNull
	{
		get
		{
			return this == null || base.gameObject == null;
		}
	}

	public override int GetFloor()
	{
		return this.Floor;
	}

	public override bool IsSelectableInView()
	{
		return Utilities.InBasement(GameSettings.Instance.ActiveFloor) == Utilities.InBasement(this.Floor) && ((this.Floor == GameSettings.Instance.ActiveFloor && this.CheckWallDown()) || (this.Floor < GameSettings.Instance.ActiveFloor && this.IsAgainstExterior));
	}

	public override bool IsSelectionRestricted()
	{
		return !GameSettings.Instance.EditMode && GameSettings.Instance.RentMode;
	}

	public bool MiniMap;

	public float MiniYOffset = 1f;

	public float MiniHeight = 2f;

	public float MiniWidth = 1f;

	public float Height1;

	public float Height2 = 2f;

	public Color MiniColor = Color.white;

	public Sprite Thumbnail;

	public bool HideWithWalls;

	public bool IsTemporary;

	public bool OnlyInterior;

	public bool IsPrivate;

	public bool HasGlass;

	public bool IsTransparent = true;

	public Renderer[] GlassRend;

	public Material TransGlassMat;

	public Material OpaqueGlassMat;

	public float NoiseFactor = 1f;

	public float Cost = 5f;

	public float LightAddition;

	public string Type;

	public string Desc;

	private Renderer[] Children;

	public int Floor;

	public bool DynamicWidth;

	public float MaxDynamicWidth = -1f;

	public bool InsideSegment = true;

	public List<GameObject> ScalableObjects;

	public List<GameObject> ScalableObjectsEdgeToEdge;

	public List<GameObject> MovableObjects;

	public bool Directional;

	private bool IsRendered = true;

	private bool IsShadowsOnly;

	public bool IsAgainstExterior = true;

	[NonSerialized]
	public Room[] ParentRooms = new Room[2];

	public bool IsConnector;

	private PathNode<Vector3> _pathNode;

	private static string[] Actions = new string[]
	{
		"Dismantle",
		"SelectWall"
	};
}
