﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUIArrow : MonoBehaviour
{
	private void Start()
	{
		this.MyRect = base.GetComponent<RectTransform>();
		this.MyRect.SetParent(WindowManager.Instance.Canvas.transform, false);
	}

	public static Rect RectTransformToScreenSpace(RectTransform transform)
	{
		Vector2 vector = Vector2.Scale(transform.rect.size, transform.lossyScale);
		Vector2 vector2 = new Vector2(-vector.x * transform.pivot.x, -vector.y * (1f - transform.pivot.y));
		return new Rect(transform.position.x / Options.UISize + vector2.x, ((float)Screen.height - transform.position.y) / Options.UISize + vector2.y, vector.x / Options.UISize, vector.y / Options.UISize);
	}

	private static RectTransform FindMask(RectTransform start)
	{
		Transform parent = start.parent;
		while (parent != null)
		{
			if (parent.GetComponent<RectMask2D>() != null)
			{
				return parent.GetComponent<RectTransform>();
			}
			parent = parent.parent;
		}
		return null;
	}

	public bool ClampToBounds(ref Vector2 p, Rect bounds)
	{
		bool result = false;
		float num = p.x;
		float num2 = -p.y;
		if (num < bounds.xMin)
		{
			num = bounds.xMin;
			result = true;
		}
		else if (num > bounds.xMax)
		{
			num = bounds.xMax;
			result = true;
		}
		if (num2 < bounds.yMin)
		{
			num2 = bounds.yMin;
			result = true;
		}
		else if (num2 > bounds.yMax)
		{
			num2 = bounds.yMax;
			result = true;
		}
		p = new Vector2(num, num2);
		return result;
	}

	private void Update()
	{
		if (!this.ThreeD && !this.ScreenParent && this.ParentR == null)
		{
			RectTransform rectTransform = WindowManager.FindElementPath(this.Anchor, null);
			if (!(rectTransform != null))
			{
				this.Img.enabled = false;
				return;
			}
			this.ParentR = rectTransform;
			this.BoundRect = GUIArrow.FindMask(rectTransform);
		}
		this.Img.enabled = true;
		if (!this.ThreeD)
		{
			Rect rect = new Rect(0f, 0f, (float)Screen.width / Options.UISize, (float)Screen.height / Options.UISize);
			Rect bounds = rect;
			if (!this.ScreenParent)
			{
				if (!this.ParentR.gameObject.activeInHierarchy)
				{
					this.Img.enabled = false;
					return;
				}
				this.Img.enabled = true;
				Vector3[] fourCornersArray = new Vector3[4];
				this.ParentR.GetWorldCorners(fourCornersArray);
				rect = GUIArrow.RectTransformToScreenSpace(this.ParentR);
			}
			float y = (this.VerticalAlign != TutorialMessage.VerticalAnchor.Top) ? ((this.VerticalAlign != TutorialMessage.VerticalAnchor.Bottom) ? (rect.height / 2f) : rect.height) : 0f;
			float x = (this.HorizontalAlign != TutorialMessage.HorizontalAnchor.Right) ? ((this.HorizontalAlign != TutorialMessage.HorizontalAnchor.Left) ? (rect.width / 2f) : rect.width) : 0f;
			Vector2 anchoredPosition = rect.position + this.Offset + new Vector2(x, y);
			float z = (!this.AnyAngle) ? (-this.Angle) : (-Quaternion.LookRotation(new Vector3(anchoredPosition.x - (float)(Screen.width / 2), 0f, (float)(Screen.height / 2) - anchoredPosition.y)).eulerAngles.y);
			if (this.BoundRect != null)
			{
				bounds = GUIArrow.RectTransformToScreenSpace(this.BoundRect);
			}
			bounds = Rect.MinMaxRect(bounds.xMin, -bounds.yMax, bounds.xMax, -bounds.yMin);
			Vector2 point = new Vector2(anchoredPosition.x, -anchoredPosition.y);
			bool flag = this.ClampToBounds(ref anchoredPosition, bounds);
			bool flag2 = false;
			if (this.ParentR != null && !flag)
			{
				Vector2 vector = new Vector2(Mathf.Clamp(anchoredPosition.x, rect.xMin + 1f, rect.xMax - 1f), Mathf.Clamp(-anchoredPosition.y, rect.yMin + 1f, rect.yMax - 1f));
				PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
				pointerEventData.pointerId = -1;
				pointerEventData.position = new Vector2(vector.x, (float)Screen.height - vector.y);
				List<RaycastResult> list = new List<RaycastResult>();
				EventSystem.current.RaycastAll(pointerEventData, list);
				foreach (RaycastResult raycastResult in list)
				{
					RectTransform component = raycastResult.gameObject.GetComponent<RectTransform>();
					if (component == this.ParentR || component.parent.GetComponent<RectTransform>() == this.ParentR)
					{
						break;
					}
					if (raycastResult.gameObject.GetComponent<GUIWindow>() != null)
					{
						Rect rect2 = GUIArrow.RectTransformToScreenSpace(component);
						float num = (rect2.xMin <= 64f) ? float.MaxValue : Mathf.Abs(anchoredPosition.x - rect2.xMin);
						float num2 = (rect2.xMax >= (float)(Screen.width - 64)) ? float.MaxValue : Mathf.Abs(anchoredPosition.x - rect2.xMax);
						float num3 = (rect2.yMin <= 64f) ? float.MaxValue : Mathf.Abs(-anchoredPosition.y - rect2.yMin);
						float num4 = (rect2.yMax >= (float)(Screen.height - 64)) ? float.MaxValue : Mathf.Abs(-anchoredPosition.y - rect2.yMax);
						if (num < num2 && num < num3 && num < num4)
						{
							anchoredPosition = new Vector2(rect2.xMin, anchoredPosition.y);
							this.MyRect.rotation = Quaternion.Euler(0f, 0f, 270f);
						}
						else if (num2 < num && num2 < num3 && num2 < num4)
						{
							anchoredPosition = new Vector2(rect2.xMax, anchoredPosition.y);
							this.MyRect.rotation = Quaternion.Euler(0f, 0f, 90f);
						}
						else if (num3 < num && num3 < num2 && num3 < num4)
						{
							anchoredPosition = new Vector2(anchoredPosition.x, -rect2.yMin);
							this.MyRect.rotation = Quaternion.Euler(0f, 0f, 180f);
						}
						else
						{
							anchoredPosition = new Vector2(anchoredPosition.x, -rect2.yMax);
							this.MyRect.rotation = Quaternion.Euler(0f, 0f, 0f);
						}
						flag2 = true;
						break;
					}
				}
			}
			this.MyRect.anchoredPosition = anchoredPosition;
			if (flag2)
			{
				this.Img.texture = this.BehindArrow;
			}
			else if (flag)
			{
				this.Img.texture = this.ClampedArrow;
			}
			else
			{
				this.Img.texture = this.NormalArrow;
			}
			if (!flag2)
			{
				if (bounds.Contains(point))
				{
					this.MyRect.rotation = Quaternion.Euler(0f, 0f, z);
				}
				else
				{
					this.MyRect.rotation = Quaternion.Euler(0f, 0f, -Quaternion.LookRotation(new Vector3(point.x - anchoredPosition.x, 0f, point.y - anchoredPosition.y)).eulerAngles.y);
				}
			}
		}
		else
		{
			Vector3 vector2 = Camera.main.WorldToScreenPoint(this.ThreeDP);
			this.MyRect.anchoredPosition = new Vector2(vector2.x / Options.UISize, ((float)(-(float)Screen.height) + vector2.y) / Options.UISize);
			this.MyRect.rotation = Quaternion.Euler(0f, 0f, 180f);
		}
		if (this.rising)
		{
			this.offset += Time.deltaTime * 16f;
			if (this.offset >= 16f)
			{
				this.rising = false;
			}
		}
		else
		{
			this.offset -= Time.deltaTime * 32f;
			if (this.offset <= 0f)
			{
				this.rising = true;
			}
		}
		Vector3 vector3 = this.MyRect.rotation * Vector3.right * this.offset;
		this.MyRect.anchoredPosition = this.MyRect.anchoredPosition + new Vector2(vector3.y, -vector3.x);
		this.MyRect.anchoredPosition = new Vector2(Mathf.Round(this.MyRect.anchoredPosition.x), Mathf.Round(this.MyRect.anchoredPosition.y));
		if (!this.RingAdded)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.RingPrefab);
			gameObject.transform.SetParent(base.transform, false);
			RingScript component2 = gameObject.GetComponent<RingScript>();
			component2.rect.anchoredPosition = new Vector2(32f, 0f);
			component2.size = 256;
			this.RingAdded = true;
			float pan = (this.MyRect.anchoredPosition.x / (float)Screen.width - 0.5f) * 2f;
			UISoundFX.PlaySFX("TutorialBlep", -1f, pan);
		}
	}

	public GameObject RingPrefab;

	public RectTransform ParentR;

	public RectTransform MyRect;

	public RectTransform BoundRect;

	public Vector2 Offset;

	public TutorialMessage.HorizontalAnchor HorizontalAlign;

	public TutorialMessage.VerticalAnchor VerticalAlign;

	public bool AnyAngle;

	public bool ThreeD;

	public bool ScreenParent;

	public Vector3 ThreeDP;

	public float Angle;

	public RawImage Img;

	public Texture2D NormalArrow;

	public Texture2D BehindArrow;

	public Texture2D ClampedArrow;

	public string Anchor;

	private bool RingAdded;

	private float offset;

	private bool rising = true;
}
