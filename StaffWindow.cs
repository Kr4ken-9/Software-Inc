﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class StaffWindow : MonoBehaviour
{
	private void Start()
	{
		this.StaffList.OnSelectChange = delegate(bool direct)
		{
			if (direct)
			{
				Actor[] selected = this.StaffList.GetSelected<Actor>();
				SelectorController instance = SelectorController.Instance;
				instance.Highligt(false);
				instance.Selected.Clear();
				foreach (Actor item in selected)
				{
					if (!instance.Selected.Contains(item))
					{
						instance.Selected.Add(item);
					}
				}
				instance.DoPostSelectChecks();
				HintController.Instance.Show(HintController.Hints.HintStaffAssign);
				if (selected.Length > 0)
				{
					this.ArrivalText.text = Utilities.HourToTime(selected[0].StaffOn, SDateTime.AMPM);
				}
			}
		};
	}

	private int GetArrivalTime()
	{
		return this.ArrivalText.text.TimeToHour(8);
	}

	public void ApplyArrival()
	{
		Actor[] selected = this.StaffList.GetSelected<Actor>();
		if (selected.Length > 0 && this.ArrivalText.text.Trim().Length > 0)
		{
			int arrivalTime = this.GetArrivalTime();
			foreach (Actor actor in selected)
			{
				actor.StaffOn = arrivalTime;
				actor.StaffOff = (actor.StaffOn + 4) % 24;
				if (!actor.isActiveAndEnabled)
				{
					SDateTime sdateTime = SDateTime.Now();
					SDateTime time = new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), actor.StaffOn - 1, sdateTime.Day + 1, sdateTime.Month, sdateTime.Year);
					GameSettings.Instance.sActorManager.AddToAwaiting(actor, time, true, true);
				}
			}
		}
	}

	private Employee GenerateStaffEmployee(float salary)
	{
		return new Employee(SDateTime.Now(), Employee.EmployeeRole.Artist, UnityEngine.Random.value > 0.5f, Employee.WageBracket.Medium, GameSettings.Instance.Personalities, false, null, null, null, 1f, 0.1f)
		{
			Salary = salary
		};
	}

	public void Show()
	{
		if (this.Window.Shown)
		{
			this.Window.Close();
			return;
		}
		this.Window.Show();
		this.CJan.ToolTipValue = 100.CurrencyInt(true) + "PerHour".Loc();
		this.HJan.ToolTipValue = 1500.CurrencyInt(true) + "PerMonth".Loc();
		this.CCle.ToolTipValue = 50.CurrencyInt(true) + "PerHour".Loc();
		this.HCle.ToolTipValue = 1000.CurrencyInt(true) + "PerMonth".Loc();
		this.HIT.ToolTipValue = 2500.CurrencyInt(true) + "PerMonth".Loc();
		this.HRec.ToolTipValue = 2500.CurrencyInt(true) + "PerMonth".Loc();
		this.HCo.ToolTipValue = 2000.CurrencyInt(true) + "PerMonth".Loc();
		this.HCor.ToolTipValue = 2000.CurrencyInt(true) + "PerMonth".Loc();
		this.CCor.ToolTipValue = "CourierPayment".Loc(new object[]
		{
			100.CurrencyInt(true),
			CourierAI.BoxPrice.Currency(true)
		});
		this.CIT.ToolTipValue = "ITPayment2".Loc(new object[]
		{
			100.CurrencyInt(true),
			500.CurrencyInt(true)
		});
		TutorialSystem.Instance.StartTutorial("Staff", false);
	}

	public void EmployActor(int type)
	{
		Actor actor = null;
		switch (type)
		{
		case 0:
			actor = GameSettings.Instance.SpawnActor(UnityEngine.Random.value > 0.5f);
			actor.AItype = AI<Actor>.AIType.Janitor;
			actor.employee = this.GenerateStaffEmployee(1500f);
			break;
		case 1:
			actor = GameSettings.Instance.SpawnActor(UnityEngine.Random.value > 0.5f);
			actor.AItype = AI<Actor>.AIType.Cleaning;
			actor.employee = this.GenerateStaffEmployee(1000f);
			break;
		case 2:
			actor = GameSettings.Instance.SpawnActor(UnityEngine.Random.value > 0.5f);
			actor.AItype = AI<Actor>.AIType.IT;
			actor.employee = this.GenerateStaffEmployee(2500f);
			break;
		case 3:
			actor = GameSettings.Instance.SpawnActor(UnityEngine.Random.value > 0.5f);
			actor.AItype = AI<Actor>.AIType.Receptionist;
			actor.employee = this.GenerateStaffEmployee(2500f);
			break;
		case 4:
			actor = GameSettings.Instance.SpawnActor(UnityEngine.Random.value > 0.5f);
			actor.AItype = AI<Actor>.AIType.Cook;
			actor.employee = this.GenerateStaffEmployee(2000f);
			break;
		case 5:
			actor = GameSettings.Instance.SpawnActor(UnityEngine.Random.value > 0.5f);
			actor.AItype = AI<Actor>.AIType.Courier;
			actor.employee = this.GenerateStaffEmployee(2000f);
			break;
		}
		if (actor != null)
		{
			actor.StaffOn = this.GetArrivalTime();
			actor.StaffOff = (actor.StaffOn + 4) % 24;
			SDateTime sdateTime = SDateTime.Now();
			GameSettings.Instance.sActorManager.AddToAwaiting(actor, new SDateTime(Utilities.GaussRange(0.5f, 0, 40, 0.2f), actor.StaffOn - 1, sdateTime.Day + ((actor.StaffOn <= TimeOfDay.Instance.Hour) ? 1 : 0), sdateTime.Month, sdateTime.Year), false, true);
		}
	}

	private void SpawnOnCallActor(AI<Actor>.AIType ai, float upfront, float hourly)
	{
		if (GameSettings.Instance.MyCompany.CanMakeTransaction(-upfront))
		{
			UISoundFX.PlaySFX("Kaching", -1f, 0f);
			GameSettings.Instance.MyCompany.MakeTransaction(-upfront, Company.TransactionCategory.Staff, "Upfront");
			Actor actor = GameSettings.Instance.SpawnActor(UnityEngine.Random.value > 0.5f);
			actor.AItype = ai;
			actor.WaitSpawn = true;
			GameSettings.Instance.sActorManager.AddToAwaiting(actor, SDateTime.Now(), true, true);
			actor.StaffOn = TimeOfDay.Instance.Hour;
			actor.employee = this.GenerateStaffEmployee(hourly);
			actor.OnCall = true;
		}
		else
		{
			WindowManager.Instance.ShowMessageBox("CannotAfford".Loc(), false, DialogWindow.DialogType.Error);
		}
	}

	public void CallActor(int type)
	{
		switch (type)
		{
		case 0:
			this.SpawnOnCallActor(AI<Actor>.AIType.Janitor, 100f, 100f);
			break;
		case 1:
			this.SpawnOnCallActor(AI<Actor>.AIType.Cleaning, 50f, 50f);
			break;
		case 2:
			this.SpawnOnCallActor(AI<Actor>.AIType.IT, 100f, 0f);
			break;
		case 3:
			this.SpawnOnCallActor(AI<Actor>.AIType.Courier, 100f, 0f);
			break;
		}
	}

	public GUIWindow Window;

	public GUIListView StaffList;

	public GUIButton CJan;

	public GUIButton HJan;

	public GUIButton CCle;

	public GUIButton HCle;

	public GUIButton HIT;

	public GUIButton CIT;

	public GUIButton HRec;

	public GUIButton HCo;

	public GUIButton HCor;

	public GUIButton CCor;

	public InputField ArrivalText;
}
