﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WallEdge
{
	public WallEdge(Vector2 p, int floor)
	{
		this.Pos = p;
		this.Floor = floor;
	}

	public WallEdge[] GetSplitEdges
	{
		get
		{
			return new WallEdge[]
			{
				this.TempCutOff,
				this.otherCutoff
			};
		}
	}

	public Room[] GetSplitRooms
	{
		get
		{
			return new Room[]
			{
				this.CutOffLink,
				this.otherLink
			};
		}
	}

	public bool IsSplitter
	{
		get
		{
			return this.TempCutOff != null;
		}
	}

	public void AddSegment(WallEdge edge, WallSnap segment)
	{
		if (!this.Children.ContainsKey(edge))
		{
			this.Children.Add(edge, new HashSet<WallSnap>());
		}
		this.Children[edge].Add(segment);
		Room room = (from x in this.Links
		where x.Value == edge
		select x.Key).FirstOrDefault<Room>();
		if (room != null)
		{
			room.UpdateFurnitureWallNearness();
		}
	}

	public int FenceCount(WallEdge other, bool oneWay = false)
	{
		int num = 0;
		Room room = (from x in this.Links
		where x.Value == other
		select x.Key).FirstOrDefault<Room>();
		if (room != null)
		{
			num += ((!room.Outdoors) ? -1 : 1);
		}
		if (oneWay)
		{
			return num;
		}
		room = (from x in other.Links
		where x.Value == this
		select x.Key).FirstOrDefault<Room>();
		if (room != null)
		{
			num += ((!room.Outdoors) ? -1 : 1);
		}
		return num;
	}

	public bool IsFence(WallEdge other)
	{
		return this.FenceCount(other, false) > 0;
	}

	public bool IsAgainstOutdoors(WallEdge other)
	{
		foreach (KeyValuePair<Room, WallEdge> keyValuePair in this.Links)
		{
			if (keyValuePair.Value == other && keyValuePair.Key.Outdoors)
			{
				return true;
			}
		}
		foreach (KeyValuePair<Room, WallEdge> keyValuePair2 in other.Links)
		{
			if (keyValuePair2.Value == this && keyValuePair2.Key.Outdoors)
			{
				return true;
			}
		}
		return false;
	}

	public void RemoveSegment(WallEdge edge, WallSnap segment)
	{
		if (this.Children.ContainsKey(edge))
		{
			this.Children[edge].Remove(segment);
		}
	}

	public bool HasWindows(WallEdge edge)
	{
		HashSet<WallSnap> hashSet = null;
		if (this.Children.TryGetValue(edge, out hashSet))
		{
			foreach (WallSnap wallSnap in hashSet)
			{
				RoomSegment component = wallSnap.GetComponent<RoomSegment>();
				if (component != null && component.LightAddition > 0f)
				{
					return true;
				}
			}
			return false;
		}
		return false;
	}

	public Room GetRoom(WallEdge other)
	{
		foreach (KeyValuePair<Room, WallEdge> keyValuePair in this.Links)
		{
			if (keyValuePair.Value == other)
			{
				return keyValuePair.Key;
			}
		}
		return null;
	}

	public float GetHeight(WallEdge other)
	{
		int num = this.FenceCount(other, false);
		if (num <= 0)
		{
			return 2f;
		}
		Room room = this.GetRoom(other);
		if (room != null && room.Outdoors)
		{
			return room.FenceHeight;
		}
		room = other.GetRoom(this);
		if (room != null && room.Outdoors)
		{
			return room.FenceHeight;
		}
		return 0f;
	}

	public Mesh[] GetAllMeshes(WallEdge other, Vector2 from, float last, bool inside, int floor, Vector2 uv2)
	{
		HashSet<WallSnap> source = null;
		if (this.Children.TryGetValue(other, out source))
		{
			List<Mesh> list = new List<Mesh>();
			foreach (RoomSegment roomSegment in source.OfType<RoomSegment>())
			{
				list.AddRange(from x in roomSegment.GetWallMeshes(inside, this)
				select this.FixMeshUV(x.GetComponent<MeshFilter>().mesh, x.transform.localToWorldMatrix, @from, last, floor, uv2));
			}
			return list.ToArray();
		}
		return new Mesh[0];
	}

	private Mesh FixMeshUV(Mesh inputMesh, Matrix4x4 transform, Vector2 a, float last, int floor, Vector2 uv2)
	{
		Mesh mesh = new Mesh();
		transform *= Matrix4x4.TRS(new Vector3(0f, (float)(-(float)floor * 2), 0f), Quaternion.identity, Vector3.one);
		Vector3[] array = inputMesh.vertices.SelectInPlace((Vector3 x) => transform.MultiplyPoint(x));
		mesh.vertices = array;
		mesh.triangles = inputMesh.triangles;
		mesh.normals = inputMesh.normals.SelectInPlace((Vector3 x) => transform.MultiplyVector(x).normalized);
		mesh.uv2 = Utilities.RepeatValue<Vector2>(uv2, array.Length);
		mesh.tangents = inputMesh.tangents.SelectInPlace((Vector4 x) => transform.MultiplyVector(x.FlattenVector4()).normalized.ToVector4(1f));
		List<Vector2> list = new List<Vector2>();
		foreach (Vector3 vector in array)
		{
			Vector2 p = new Vector2(vector.x, vector.z);
			float num = a.Dist(p) / 2f;
			float y = vector.y / 2f;
			list.Add(new Vector2(last + num, y));
		}
		mesh.SetUVs(0, list);
		return mesh;
	}

	public float? FirstValidFrom(float pos, WallEdge other)
	{
		HashSet<WallSnap> source;
		if (this.Children.TryGetValue(other, out source))
		{
			List<float> list = (from x in source
			select x.WallPosition[this] - x.WallWidth / 2f into x
			where x >= pos
			orderby x
			select x).ToList<float>();
			return (list.Count <= 0) ? null : new float?(list[0]);
		}
		return null;
	}

	public bool ValidSegment(ref Vector2 pos, float width, WallEdge other, bool oneSide, bool keepInWall, bool onFence, float height1 = 0f, float height2 = 2f, bool fenceWallExclusive = false, float offx = 0f, float offy = 0f, bool clone = false, WallSnap ignore = null)
	{
		if (!clone)
		{
			bool flag = this.IsFence(other);
			if (fenceWallExclusive && (flag ^ onFence))
			{
				return false;
			}
			if (flag && !onFence)
			{
				return false;
			}
			if (flag && !fenceWallExclusive && this.GetHeight(other) < height2)
			{
				return false;
			}
		}
		HashSet<WallSnap> hashSet = null;
		if (this.Children.TryGetValue(other, out hashSet))
		{
			float num = this.Pos.Dist(other.Pos);
			float num2 = this.Pos.Dist(new Vector2(pos.x + offx, pos.y + offy));
			float num3 = this.Pos.Dist(pos);
			float num4 = num3 - width / 2f;
			float num5 = num3 + width / 2f;
			foreach (WallSnap wallSnap in hashSet)
			{
				if (!(wallSnap == ignore))
				{
					if (wallSnap is Furniture)
					{
						Furniture furniture = (Furniture)wallSnap;
						if (furniture.IgnoreBoundary)
						{
							continue;
						}
						if (oneSide && furniture.FirstEdge != this)
						{
							continue;
						}
						if (!Utilities.Overlap(height1, height2, furniture.Height1, furniture.Height2))
						{
							continue;
						}
					}
					if (wallSnap is RoomSegment)
					{
						RoomSegment roomSegment = (RoomSegment)wallSnap;
						if (!Utilities.Overlap(height1, height2, roomSegment.Height1, roomSegment.Height2))
						{
							continue;
						}
					}
					float num6 = wallSnap.WallPosition[this];
					float num7 = num6 + wallSnap.WallWidth / 2f;
					num6 -= wallSnap.WallWidth / 2f;
					if (Utilities.RelaxedOverlap(num4, num5, num6, num7))
					{
						if ((num6 + num7) / 2f > num2)
						{
							pos = this.Pos + (other.Pos - this.Pos).normalized * (num6 - width / 2f);
							num4 = num6 - width;
							num5 = num6;
						}
						else
						{
							pos = this.Pos + (other.Pos - this.Pos).normalized * (num7 + width / 2f);
							num4 = num7;
							num5 = num7 + width;
						}
						if (keepInWall)
						{
							if (num4 < Room.WallOffset / 2f || num5 > num - Room.WallOffset / 2f)
							{
								return false;
							}
						}
						else if (num4 < -0.0001f || num5 > num)
						{
							return false;
						}
						num3 = this.Pos.Dist(pos);
						break;
					}
				}
			}
			foreach (WallSnap wallSnap2 in hashSet)
			{
				if (!(wallSnap2 == ignore))
				{
					if (wallSnap2 is Furniture)
					{
						Furniture furniture2 = (Furniture)wallSnap2;
						if (furniture2.IgnoreBoundary)
						{
							continue;
						}
						if (oneSide && furniture2.FirstEdge != this)
						{
							continue;
						}
						if (!Utilities.Overlap(height1, height2, furniture2.Height1, furniture2.Height2))
						{
							continue;
						}
					}
					if (wallSnap2 is RoomSegment)
					{
						RoomSegment roomSegment2 = (RoomSegment)wallSnap2;
						if (!Utilities.Overlap(height1, height2, roomSegment2.Height1, roomSegment2.Height2))
						{
							continue;
						}
					}
					float num8 = wallSnap2.WallPosition[this];
					float d = num8 + wallSnap2.WallWidth / 2f;
					num8 -= wallSnap2.WallWidth / 2f;
					if (Utilities.RelaxedOverlap(num4, num5, num8, d))
					{
						return false;
					}
				}
			}
			return true;
		}
		return true;
	}

	public Vector2[] GetSplit(WallEdge other)
	{
		HashSet<WallSnap> source = null;
		if (this.Children.TryGetValue(other, out source))
		{
			List<float> list = new List<float>();
			float num = this.Pos.Dist(other.Pos);
			foreach (RoomSegment roomSegment in from x in source.OfType<RoomSegment>()
			orderby x.WallPosition[this]
			select x)
			{
				float num2 = roomSegment.WallPosition[this];
				float num3 = num2 + roomSegment.WallWidth / 2f;
				num2 -= roomSegment.WallWidth / 2f;
				float b = num2 / num;
				if (list.Any<float>() && Mathf.Approximately(list.Last<float>(), b))
				{
					list.RemoveAt(list.Count - 1);
				}
				else
				{
					list.Add(num2 / num);
				}
				list.Add(num3 / num);
			}
			Vector2 diff = other.Pos - this.Pos;
			return (from x in list
			select this.Pos + diff * x).ToArray<Vector2>();
		}
		return null;
	}

	public void SetSplit(WallEdge split, Room link)
	{
		this.TempCutOff = split;
		this.CutOffLink = link;
		this.otherCutoff = this.TempCutOff.Links[this.CutOffLink];
		this.otherLink = this.otherCutoff.Links.FirstOrDefault((KeyValuePair<Room, WallEdge> x) => x.Value == this.TempCutOff).Key;
	}

	public void ResetSplit()
	{
		this.TempCutOff = null;
		this.CutOffLink = null;
		this.otherCutoff = null;
		this.otherLink = null;
	}

	public WallEdge FindConnectionIn(Room r)
	{
		for (int i = 0; i < r.Edges.Count; i++)
		{
			WallEdge wallEdge;
			if (r.Edges[i].Links.TryGetValue(r, out wallEdge) && wallEdge == this)
			{
				return r.Edges[i];
			}
		}
		return null;
	}

	public IEnumerable<WallEdge> FindAllConnectionIn()
	{
		return from x in this.Links.Keys
		select this.FindConnectionIn(x);
	}

	public void SplitSegment(List<UndoObject.UndoAction> undos)
	{
		if (this.TempCutOff != null)
		{
			HashSet<WallSnap> orDefault = this.TempCutOff.Children.GetOrDefault(this.otherCutoff, new HashSet<WallSnap>());
			Dictionary<WallSnap, UndoObject.UndoAction> dictionary = orDefault.ToDictionary((WallSnap x) => x, (WallSnap x) => (undos != null) ? new UndoObject.UndoAction(x, false) : null);
			this.TempCutOff.Links[this.CutOffLink] = this;
			this.Links[this.CutOffLink] = this.otherCutoff;
			if (this.otherLink != null)
			{
				this.otherCutoff.Links[this.otherLink] = this;
				this.Links[this.otherLink] = this.TempCutOff;
				int num = this.otherLink.Edges.IndexOf(this.otherCutoff);
				this.otherLink.Edges.Insert(num + 1, this);
			}
			int num2 = this.CutOffLink.Edges.IndexOf(this.TempCutOff);
			this.CutOffLink.Edges.Insert(num2 + 1, this);
			foreach (KeyValuePair<WallSnap, UndoObject.UndoAction> keyValuePair in dictionary)
			{
				if (!keyValuePair.Key.BeenDestroyed)
				{
					WallSnap key = keyValuePair.Key;
					WallEdge firstEdge = key.FirstEdge;
					WallEdge wallEdge = (firstEdge != this.TempCutOff) ? this.TempCutOff : this.otherCutoff;
					float num3 = firstEdge.Pos.Dist(this.Pos);
					float num4 = key.WallPosition[firstEdge];
					float num5 = num4 - key.WallWidth / 2f;
					float num6 = num4 + key.WallWidth / 2f;
					if (key is RoomSegment)
					{
						if ((num5 < num3 && num6 > num3) || (num5 > num3 && num6 < num3))
						{
							this.CutOffLink.DirtyInnerMesh = true;
							if (this.otherLink != null)
							{
								this.otherLink.DirtyInnerMesh = true;
							}
							if (((RoomSegment)key).IsConnector)
							{
								this.CutOffLink.DirtyPathNodes = true;
								if (this.otherLink != null)
								{
									this.otherLink.DirtyPathNodes = true;
								}
							}
							if (keyValuePair.Value != null)
							{
								undos.Add(keyValuePair.Value);
							}
							key.DestroyMe();
							continue;
						}
					}
					else if (Utilities.Overlap(num5, num6, num3 - Room.WallOffset / 2f, num3 + Room.WallOffset / 2f))
					{
						if (keyValuePair.Value != null)
						{
							undos.Add(keyValuePair.Value);
						}
						key.DestroyMe();
						continue;
					}
					if (num4 < num3)
					{
						key.Init(firstEdge, this, num4 / num3, false);
					}
					else
					{
						key.Init(this, wallEdge, (num4 - num3) / this.Pos.Dist(wallEdge.Pos), false);
					}
				}
			}
			this.TempCutOff = null;
			this.CutOffLink = null;
		}
	}

	public bool CouldSplit(Room r)
	{
		return this.CutOffLink == r || this.otherLink == r || this.Links.ContainsKey(r);
	}

	public bool UpAgainst(WallEdge s)
	{
		return this.Links.ContainsValue(s) || (this.TempCutOff != null && (this.TempCutOff == s || this.otherCutoff == s || (this.TempCutOff == s.TempCutOff && this.otherCutoff == s.otherCutoff)));
	}

	public bool CanIntersect(WallEdge a, WallEdge b)
	{
		return (a == this.TempCutOff || b == this.TempCutOff) && (a == this.otherCutoff || b == this.otherCutoff);
	}

	public override string ToString()
	{
		return this.Pos.ToString();
	}

	public Vector2 Pos;

	public int Floor;

	public Dictionary<Room, WallEdge> Links = new Dictionary<Room, WallEdge>();

	private WallEdge TempCutOff;

	private WallEdge otherCutoff;

	private Room CutOffLink;

	private Room otherLink;

	[NonSerialized]
	public Dictionary<WallEdge, HashSet<WallSnap>> Children = new Dictionary<WallEdge, HashSet<WallSnap>>();
}
