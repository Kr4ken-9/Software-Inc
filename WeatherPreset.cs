﻿using System;
using UnityEngine;

public class WeatherPreset : ScriptableObject
{
	public void ExportTemps()
	{
	}

	public void CalculateRange()
	{
	}

	public Gradient SummerGradient;

	public Gradient WinterGradient;

	public Gradient SkyGradient;

	public Gradient AmbientGradient;

	public Gradient TreeGradient;

	public Gradient TreeGradient2;

	[ContextMenuItem("Export temperature sheet", "ExportTemps")]
	public AnimationCurve TemperatureCurve;

	[ContextMenuItem("Export temperature sheet", "ExportTemps")]
	public AnimationCurve TempMin;

	[ContextMenuItem("Export temperature sheet", "ExportTemps")]
	public AnimationCurve TempRange;

	public Color GroundColor;

	public string[] Critters;

	public int[] CritterCount;

	public float BackgroundNoise;

	[ContextMenuItem("Calculate temperature range", "CalculateRange")]
	public float MinimumTemperature;

	[ContextMenuItem("Calculate temperature range", "CalculateRange")]
	public float MaximumTemperature;

	public StaticTree[] Trees;
}
