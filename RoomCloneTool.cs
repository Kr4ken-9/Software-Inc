﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomCloneTool : MonoBehaviour
{
	private void Start()
	{
		if (RoomCloneTool.Instance != null)
		{
			UnityEngine.Object.Destroy(RoomCloneTool.Instance.gameObject);
		}
		RoomCloneTool.Instance = this;
		this.rend = base.GetComponentInChildren<Renderer>();
		base.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		if (RoomCloneTool.Instance == this)
		{
			RoomCloneTool.Instance = null;
		}
	}

	private Mesh GenerateFullMesh()
	{
		int num = 0;
		for (int i = 0; i < this.VisualBounds.Length; i++)
		{
			num += this.VisualBounds[i].Length;
		}
		CombineInstance[] array = new CombineInstance[num];
		int num2 = 0;
		for (int j = 0; j < this.VisualBounds.Length; j++)
		{
			for (int k = 0; k < this.VisualBounds[j].Length; k++)
			{
				Mesh mesh = this.GenerateSubMesh(this.VisualBounds[j][k], j);
				array[num2] = default(CombineInstance);
				array[num2].mesh = mesh;
				array[num2].transform = Matrix4x4.identity;
				num2++;
			}
		}
		Mesh mesh2 = new Mesh();
		mesh2.CombineMeshes(array);
		return mesh2;
	}

	private Mesh GenerateSubMesh(Vector2[] test, int floor)
	{
		Mesh mesh = new Mesh();
		List<Vector3> list = (from x in test
		select new Vector3(x.x - this.Center.x, (float)(floor * 2), x.y - this.Center.y)).ToList<Vector3>();
		list.AddRange((from x in list
		select x + Vector3.up * 2.01f).ToList<Vector3>());
		int[] source = new Triangulator(test).Triangulate();
		List<int> list2 = (from x in source
		select x + test.Length).ToList<int>();
		for (int i = 0; i < test.Length - 1; i++)
		{
			list2.Add(i);
			list2.Add(test.Length + i);
			list2.Add(test.Length + i + 1);
			list2.Add(test.Length + i + 1);
			list2.Add(i + 1);
			list2.Add(i);
		}
		list2.Add(test.Length - 1);
		list2.Add(test.Length * 2 - 1);
		list2.Add(test.Length);
		list2.Add(test.Length);
		list2.Add(0);
		list2.Add(test.Length - 1);
		mesh.vertices = list.ToArray();
		mesh.normals = (from x in list
		select Vector3.up).ToArray<Vector3>();
		mesh.uv = (from x in list
		select Vector2.zero).ToArray<Vector2>();
		mesh.triangles = list2.ToArray();
		return mesh;
	}

	public void Show(Room[] rooms)
	{
		if (!BuildingPrefab.ValidCheck(rooms))
		{
			WindowManager.Instance.ShowMessageBox("UnsupportedStructure".Loc(), false, DialogWindow.DialogType.Error);
			return;
		}
		BuildingPrefab rooms2 = BuildingPrefab.SaveRooms(rooms, false, true, false, true);
		this.Show(rooms2);
	}

	private void CalculateBounds()
	{
		int num = this.Prefab.Rooms.Min((BuildingPrefab.RoomObject x) => x.Floor);
		int num2 = this.Prefab.Rooms.Max((BuildingPrefab.RoomObject x) => x.Floor);
		this.Bounds = new Vector2[num2 - num + 1][][];
		this.VisualBounds = new Vector2[num2 - num + 1][][];
		for (int i = num; i <= num2; i++)
		{
			List<List<Vector2>[]> list = RoomManager.CombineRoomEdges(this.Prefab, i);
			this.Bounds[i - num] = new Vector2[list.Count][];
			this.VisualBounds[i - num] = new Vector2[list.Count][];
			for (int j = 0; j < list.Count; j++)
			{
				this.Bounds[i - num][j] = list[j][0].ToArray();
				this.VisualBounds[i - num][j] = list[j][1].ToArray();
			}
		}
	}

	public void Show(BuildingPrefab rooms)
	{
		base.transform.rotation = Quaternion.identity;
		base.transform.localScale = Vector3.one;
		this.MirrorX = false;
		this.MirrorY = false;
		BuildController.Instance.ClearBuild(true, false, false, false);
		this.Prefab = rooms;
		List<Vector2> list = Utilities.ComputeConvexHull((from x in this.Prefab.Edges
		select x.ToVector2()).ToList<Vector2>());
		if (Utilities.Clockwise(list))
		{
			list.Reverse();
		}
		this.CalculateBounds();
		this.Height = this.Prefab.Rooms.Max((BuildingPrefab.RoomObject x) => x.Floor) - this.Prefab.Rooms.Min((BuildingPrefab.RoomObject x) => x.Floor);
		float num = 0f;
		float num2 = 0f;
		int num3 = 0;
		for (int i = 0; i < this.Bounds[0].Length; i++)
		{
			for (int j = 0; j < this.Bounds[0][i].Length; j++)
			{
				num += this.Bounds[0][i][j].x;
				num2 += this.Bounds[0][i][j].y;
				num3++;
			}
		}
		this.Center = new Vector3(num / (float)num3, num2 / (float)num3);
		this.Center = BuildController.Instance.CorrectMousePos(this.Center, false);
		base.GetComponent<MeshFilter>().mesh = this.MapMaker.CreateBuildingMesh(this.MapMaker.MapDescFromRooms(this.Prefab), this.Center);
		base.gameObject.SetActive(true);
		this.PreCost = 0f;
		foreach (BuildingPrefab.RoomObject roomObject in this.Prefab.Rooms)
		{
			foreach (BuildingPrefab.FurnitureObject furnitureObject in roomObject.Furniture)
			{
				Furniture furnitureComponent = ObjectDatabase.Instance.GetFurnitureComponent(furnitureObject.Name);
				if (furnitureComponent != null)
				{
					this.PreCost += furnitureComponent.Cost;
				}
			}
			foreach (BuildingPrefab.SegmentObject segmentObject in roomObject.Segments)
			{
				RoomSegment segmentComponent = ObjectDatabase.Instance.GetSegmentComponent(segmentObject.Name);
				if (segmentComponent != null)
				{
					this.PreCost += segmentComponent.Cost * (segmentObject.Width / segmentComponent.WallWidth);
				}
			}
		}
		this.ArrowX.transform.localPosition = new Vector3(0f, (float)((this.Height + 1) * 2) + 0.1f, 0.7f);
		this.ArrowY.transform.localPosition = new Vector3(0.7f, (float)((this.Height + 1) * 2) + 0.1f, 0f);
	}

	private Vector2 CorrectPos(Vector2 p)
	{
		Vector2 vector = p - this.Center;
		Vector3 vector2 = base.transform.rotation * new Vector3((!this.MirrorX) ? vector.x : (-vector.x), 0f, (!this.MirrorY) ? vector.y : (-vector.y));
		return new Vector2(vector2.x + base.transform.position.x, vector2.z + base.transform.position.z);
	}

	private bool Valid(int floor, int bound)
	{
		List<Vector2> list = (from x in this.Bounds[floor][bound]
		select this.CorrectPos(x)).ToList<Vector2>();
		if (GameSettings.Instance.ActiveFloor + this.Height > GameSettings.MaxFloor)
		{
			return false;
		}
		for (int i = 0; i < list.Count; i += 2)
		{
			list.Insert(i + 1, (list[i] + list[(i + 1) % list.Count]) * 0.5f);
		}
		if (!GameSettings.Instance.PlayerOwnedArea(list))
		{
			return false;
		}
		int num = GameSettings.Instance.ActiveFloor + floor;
		if (num >= 0 && num <= 1)
		{
			for (int j = 0; j < list.Count; j++)
			{
				Vector2 p = list[j];
				Vector2 p2 = list[(j + 1) % list.Count];
				if (BuildController.IsOnRoad(p, p2))
				{
					return false;
				}
			}
		}
		List<Room> rooms = GameSettings.Instance.sRoomManager.GetRooms();
		bool[] array = new bool[list.Count];
		for (int k = 0; k < rooms.Count; k++)
		{
			Room room = rooms[k];
			if (room.Floor == num)
			{
				for (int l = 0; l < array.Length; l++)
				{
					array[l] = false;
				}
				for (int m = 0; m < room.Edges.Count; m++)
				{
					for (int n = 0; n < array.Length; n++)
					{
						if (!array[n])
						{
							array[n] = (room.Edges[m].Pos == list[n]);
						}
					}
				}
				for (int num2 = 0; num2 < room.Edges.Count; num2++)
				{
					for (int num3 = 0; num3 < array.Length; num3++)
					{
						if (!array[num3])
						{
							Vector2? vector = Utilities.ProjectToLine(list[num3], room.Edges[num2].Pos, room.Edges[num2].Links[room].Pos);
							if (vector != null && (vector.Value - list[num3]).sqrMagnitude < 0.0001f)
							{
								array[num3] = true;
							}
						}
					}
				}
				bool flag = true;
				for (int num4 = 0; num4 < array.Length; num4++)
				{
					if (!array[num4])
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					return false;
				}
				for (int num5 = 0; num5 < list.Count; num5++)
				{
					if (room.IsInside(list[num5], 0.01f))
					{
						return false;
					}
				}
			}
		}
		HashSet<WallEdge> hashSet = new HashSet<WallEdge>();
		for (int num6 = 0; num6 < rooms.Count; num6++)
		{
			Room room2 = rooms[num6];
			if (room2.Floor == num)
			{
				for (int num7 = 0; num7 < room2.Edges.Count; num7++)
				{
					WallEdge wallEdge = room2.Edges[num7];
					if (!hashSet.Contains(wallEdge))
					{
						hashSet.Add(wallEdge);
						foreach (WallEdge wallEdge2 in wallEdge.Links.Values)
						{
							for (int num8 = 0; num8 < list.Count; num8++)
							{
								Vector2 p3 = list[num8];
								Vector2 p4 = list[(num8 + 1) % list.Count];
								if (Utilities.LinesIntersect(p3, p4, wallEdge.Pos, wallEdge2.Pos, true, false))
								{
									return false;
								}
							}
						}
					}
				}
			}
		}
		return floor != 0 || GameSettings.Instance.sRoomManager.IsSupported(list, GameSettings.Instance.ActiveFloor, null, true, null);
	}

	private void OnDisable()
	{
		if (CostDisplay.Instance != null)
		{
			CostDisplay.Instance.Hide();
		}
	}

	private void Update()
	{
		SelectorController.CanClick = false;
		if (GameSettings.FreezeGame)
		{
			CostDisplay.Instance.Hide();
			return;
		}
		if (Input.GetMouseButtonUp(1))
		{
			base.gameObject.SetActive(false);
			return;
		}
		ErrorOverlay.Instance.ShowError(string.Format("RotateRoomHint".Loc(), InputController.GetKeyBindString(InputController.Keys.FurnitureAntiClock, false), InputController.GetKeyBindString(InputController.Keys.FurnitureClock, false)), false, false, 0f, false);
		if (InputController.GetKeyUp(InputController.Keys.FurnitureClock, false))
		{
			base.transform.rotation *= Quaternion.Euler(0f, BuildController.Instance.FurnitureAngle, 0f);
			UISoundFX.PlaySFX("FurnRotate", -1f, 0f);
		}
		if (InputController.GetKeyUp(InputController.Keys.FurnitureAntiClock, false))
		{
			base.transform.rotation *= Quaternion.Euler(0f, -BuildController.Instance.FurnitureAngle, 0f);
			UISoundFX.PlaySFX("FurnRotate", -1f, 0f);
		}
		if (InputController.GetKeyUp(InputController.Keys.MirrorRoomHor, false))
		{
			this.MirrorX = !this.MirrorX;
			base.transform.localScale = new Vector3((float)((!this.MirrorX) ? 1 : -1), 1f, (float)((!this.MirrorY) ? 1 : -1));
			UISoundFX.PlaySFX("FurnRotate", -1f, 0f);
		}
		if (InputController.GetKeyUp(InputController.Keys.MirrorRoomVert, false))
		{
			this.MirrorY = !this.MirrorY;
			base.transform.localScale = new Vector3((float)((!this.MirrorX) ? 1 : -1), 1f, (float)((!this.MirrorY) ? 1 : -1));
			UISoundFX.PlaySFX("FurnRotate", -1f, 0f);
		}
		float num = this.PreCost;
		int num2 = this.Prefab.Rooms.Min((BuildingPrefab.RoomObject x) => x.Floor);
		for (int i = 0; i < this.Prefab.Rooms.Length; i++)
		{
			BuildingPrefab.RoomObject roomObject = this.Prefab.Rooms[i];
			int floor = roomObject.Floor - num2 + GameSettings.Instance.ActiveFloor;
			num += BuildController.GetRoomCost((from x in roomObject.Edges
			select this.Prefab.Edges[x].ToVector2()).ToList<Vector2>(), roomObject.Area, roomObject.Outdoor, floor, false, false);
		}
		Vector2 vector = BuildController.Instance.GetMousePos(new Plane(Vector3.up, Vector3.up * (float)GameSettings.Instance.ActiveFloor * 2f));
		vector = BuildController.Instance.CorrectMousePos(vector, false);
		if (vector != this.LastPos)
		{
			UISoundFX.PlaySFX("Tick", -1f, 0f);
		}
		this.LastPos = vector;
		base.transform.position = new Vector3(vector.x, (float)(GameSettings.Instance.ActiveFloor * 2), vector.y);
		CostDisplay.Instance.Show(num, new Vector3(vector.x, (float)(GameSettings.Instance.ActiveFloor * 2 + 1), vector.y));
		bool flag = GameSettings.Instance.MyCompany.CanMakeTransaction(-num);
		if (flag)
		{
			for (int j = 0; j < this.Bounds.Length; j++)
			{
				for (int k = 0; k < this.Bounds[j].Length; k++)
				{
					flag = this.Valid(j, k);
					if (!flag)
					{
						break;
					}
				}
				if (!flag)
				{
					break;
				}
			}
		}
		this.rend.sharedMaterial = ((!flag) ? this.InvalidMat : this.ValidMat);
		if (Input.GetMouseButtonUp(0))
		{
			if (flag)
			{
				CostDisplay.Instance.FloatAway();
				UISoundFX.PlaySFX("PlaceRoom", -1f, 0f);
				UISoundFX.PlaySFX("Kaching", -1f, 0f);
				this.BuildPrefab(this.Prefab, -num2 + GameSettings.Instance.ActiveFloor, true, true, true, null, true);
				if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
				{
					base.gameObject.SetActive(false);
				}
			}
			else
			{
				UISoundFX.PlaySFX("BuildError", -1f, 0f);
			}
		}
	}

	public void BuildPrefab(BuildingPrefab prefab, int floorOffset, bool cost, bool correctPos, bool undo, uint[] ids = null, bool supportCheck = true)
	{
		float num = 0f;
		List<KeyValuePair<Room, BuildingPrefab.RoomObject>> list = new List<KeyValuePair<Room, BuildingPrefab.RoomObject>>();
		List<BuildingPrefab.RoomObject> list2 = (from x in prefab.Rooms
		orderby x.Floor
		select x).ToList<BuildingPrefab.RoomObject>();
		int i = 0;
		while (i < list2.Count)
		{
			BuildingPrefab.RoomObject roomObject = list2[i];
			int num2 = roomObject.Floor + floorOffset;
			List<WallEdge> list3 = new List<WallEdge>();
			for (int j = 0; j < roomObject.Edges.Length; j++)
			{
				if (j == roomObject.Edges.Length - 1 && roomObject.Edges[0] == roomObject.Edges[j])
				{
					break;
				}
				Vector2 vector = (!correctPos) ? prefab.Edges[roomObject.Edges[j]].ToVector2() : this.CorrectPos(prefab.Edges[roomObject.Edges[j]]);
				WallEdge wallEdge = null;
				foreach (WallEdge wallEdge2 in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(num2))
				{
					if ((wallEdge2.Pos - vector).magnitude < BuildController.Instance.SnapDistance)
					{
						wallEdge = wallEdge2;
						break;
					}
				}
				if (wallEdge == null)
				{
					foreach (WallEdge wallEdge3 in GameSettings.Instance.sRoomManager.GetEdgesOnFloor(num2))
					{
						foreach (KeyValuePair<Room, WallEdge> keyValuePair in wallEdge3.Links)
						{
							Vector2? vector2 = Utilities.ProjectToLine(vector, wallEdge3.Pos, keyValuePair.Value.Pos);
							if (vector2 != null && (vector2.Value - vector).magnitude < BuildController.Instance.SnapDistance)
							{
								wallEdge = new WallEdge(vector2.Value, num2);
								wallEdge.SetSplit(wallEdge3, keyValuePair.Key);
								break;
							}
						}
					}
				}
				if (wallEdge == null)
				{
					wallEdge = new WallEdge(vector, num2);
				}
				list3.Add(wallEdge);
			}
			if (!supportCheck)
			{
				goto IL_2CB;
			}
			if (GameSettings.Instance.sRoomManager.IsSupported(from x in list3
			select x.Pos, num2, null, true, null))
			{
				goto IL_2CB;
			}
			list2.RemoveAt(i);
			i--;
			IL_7C7:
			i++;
			continue;
			IL_2CB:
			BuildController.Instance.CurrentSegments = list3;
			BuildController.Instance.FinalizeCuts(false, num2, null);
			BuildController.Instance.CurrentSegments = null;
			GameSettings.Instance.sRoomManager.AllSegments.AddRange(list3);
			Room room = BuildController.Instance.MakeRoom(list3, num2, null, true, false, false, roomObject.Outdoor);
			room.Rentable = roomObject.Rentable;
			if (ids != null)
			{
				room.DID = ids[i];
				Writeable.DeserializedObjects[room.DID] = room;
			}
			list.Add(new KeyValuePair<Room, BuildingPrefab.RoomObject>(room, roomObject));
			GameSettings.Instance.sRoomManager.AddRoom(room);
			GameSettings.Instance.sRoomManager.UpdateSupport(room);
			if (cost)
			{
				float roomCost = BuildController.GetRoomCost((from x in room.Edges
				select x.Pos).ToList<Vector2>(), room.Area, room.Outdoors, room.Floor, false, false);
				num += roomCost;
				GameSettings.Instance.MyCompany.MakeTransaction(-roomCost, Company.TransactionCategory.Construction, "Room");
			}
			if (roomObject.RoomData != null)
			{
				room.DeserializeThis(roomObject.RoomData);
				RoomGroup roomGroup = GameSettings.Instance.GetRoomGroup(room.RoomGroup);
				if (roomGroup != null)
				{
					roomGroup.AddRoom(room);
				}
			}
			else
			{
				room.SetFenceStyle(roomObject.Materials[3], null);
				room.FloorMat = roomObject.Materials[2];
				room.FloorColor = roomObject.Colors[2];
				room.InsideMat = roomObject.Materials[0];
				room.InsideColor = roomObject.Colors[0];
				room.OutsideMat = roomObject.Materials[1];
				room.OutsideColor = roomObject.Colors[1];
			}
			for (int k = 0; k < roomObject.Segments.Length; k++)
			{
				BuildingPrefab.SegmentObject segmentObject = roomObject.Segments[k];
				RoomSegment segmentComponent = ObjectDatabase.Instance.GetSegmentComponent(segmentObject.Name);
				if (segmentComponent != null)
				{
					Vector2 vector3 = (!correctPos) ? new Vector2(segmentObject.Position.x, segmentObject.Position.z) : this.CorrectPos(new Vector2(segmentObject.Position.x, segmentObject.Position.z));
					for (int l = 0; l < list3.Count; l++)
					{
						WallEdge wallEdge4 = list3[l];
						WallEdge wallEdge5 = list3[(l + 1) % list3.Count];
						Vector2? vector4 = Utilities.ProjectToLine(vector3, wallEdge4.Pos, wallEdge5.Pos);
						Vector2 lhs = (vector4 == null) ? Vector2.zero : vector4.Value;
						if (vector4 != null && (vector3 - vector4.Value).magnitude < 0.1f && wallEdge4.ValidSegment(ref lhs, segmentObject.Width, wallEdge5, false, false, !segmentComponent.InsideSegment, segmentComponent.Height1, segmentComponent.Height2, true, 0f, 0f, true, null) && lhs == vector4.Value)
						{
							if (segmentObject.Reversed)
							{
								WallEdge wallEdge6 = wallEdge4;
								wallEdge4 = wallEdge5;
								wallEdge5 = wallEdge6;
							}
							float num3 = segmentObject.Width / 2f;
							float num4 = vector4.Value.Dist(wallEdge4.Pos);
							float num5 = vector4.Value.Dist(wallEdge5.Pos);
							if ((num4.Appx(num3, 0.0001f) || num4 >= num3) && (num5.Appx(num3, 0.0001f) || num5 >= num3))
							{
								GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(segmentComponent.gameObject);
								RoomSegment component = gameObject.GetComponent<RoomSegment>();
								if (segmentComponent.DynamicWidth)
								{
									component.FixDynamicWidth(segmentObject.Width);
								}
								component.Floor = num2;
								component.transform.position = new Vector3(component.transform.position.x, (float)(num2 * 2), component.transform.position.z);
								component.Init(wallEdge4, wallEdge5, (wallEdge4.Pos - vector4.Value).magnitude / (wallEdge4.Pos - wallEdge5.Pos).magnitude, true);
								if (cost)
								{
									float num6 = segmentComponent.Cost / segmentComponent.WallWidth * segmentObject.Width;
									num += num6;
									GameSettings.Instance.MyCompany.MakeTransaction(-num6, Company.TransactionCategory.Construction, "Segment");
								}
								break;
							}
						}
					}
				}
			}
			goto IL_7C7;
		}
		for (int m = 0; m < list.Count; m++)
		{
			BuildingPrefab.RoomObject value = list[m].Value;
			Room key = list[m].Key;
			int floor = key.Floor;
			List<WallEdge> edges = key.Edges;
			Dictionary<uint, Furniture> dictionary = new Dictionary<uint, Furniture>();
			BuildingPrefab.FurnitureObject[] furniture = value.Furniture;
			for (int n = 0; n < furniture.Length; n++)
			{
				RoomCloneTool.<BuildPrefab>c__AnonStorey1 <BuildPrefab>c__AnonStorey = new RoomCloneTool.<BuildPrefab>c__AnonStorey1();
				<BuildPrefab>c__AnonStorey.furniture = furniture[n];
				Furniture furnitureComponent = ObjectDatabase.Instance.GetFurnitureComponent(<BuildPrefab>c__AnonStorey.furniture.Name);
				if (!(furnitureComponent == null) && (Cheats.UnlockFurn || GameSettings.Instance.EditMode || furnitureComponent.UnlockYear <= SDateTime.Now().RealYear))
				{
					if (floor >= 0 || furnitureComponent.BasementValid)
					{
						Vector2 vector5 = (!correctPos) ? new Vector2(<BuildPrefab>c__AnonStorey.furniture.Position.x, <BuildPrefab>c__AnonStorey.furniture.Position.z) : this.CorrectPos(new Vector2(<BuildPrefab>c__AnonStorey.furniture.Position.x, <BuildPrefab>c__AnonStorey.furniture.Position.z));
						Vector3 position = new Vector3(vector5.x, (float)(floor * 2), vector5.y);
						SnapPoint snapPoint = null;
						if (furnitureComponent.IsSnapping)
						{
							if (!dictionary.ContainsKey(<BuildPrefab>c__AnonStorey.furniture.Parent))
							{
								goto IL_D13;
							}
							snapPoint = dictionary[<BuildPrefab>c__AnonStorey.furniture.Parent].SnapPoints.FirstOrDefault((SnapPoint x) => x.Id == <BuildPrefab>c__AnonStorey.furniture.SnapID);
							if (snapPoint == null)
							{
								goto IL_D13;
							}
							position = snapPoint.transform.position;
						}
						WallEdge wallEdge7 = null;
						WallEdge edge = null;
						float wallPos = 0f;
						if (furnitureComponent.WallFurn)
						{
							for (int num7 = 0; num7 < edges.Count; num7++)
							{
								WallEdge wallEdge8 = edges[num7];
								WallEdge wallEdge9 = edges[(num7 + 1) % edges.Count];
								Vector2? vector6 = Utilities.ProjectToLine(vector5, wallEdge8.Pos, wallEdge9.Pos);
								Vector2 vector7 = (vector6 == null) ? Vector2.zero : vector6.Value;
								if (vector6 != null && (vector5 - vector6.Value).magnitude < 0.1f && wallEdge8.ValidSegment(ref vector7, furnitureComponent.WallWidth, wallEdge9, true, true, furnitureComponent.ValidOnFence, furnitureComponent.Height1, furnitureComponent.Height2, false, 0f, 0f, false, null))
								{
									wallEdge7 = wallEdge8;
									edge = wallEdge9;
									wallPos = (wallEdge8.Pos - vector6.Value).magnitude / (wallEdge8.Pos - wallEdge9.Pos).magnitude;
									break;
								}
							}
							if (wallEdge7 == null)
							{
								goto IL_D13;
							}
						}
						Quaternion quaternion = Quaternion.identity;
						if (furnitureComponent.IsSnapping)
						{
							quaternion = Quaternion.Euler(0f, <BuildPrefab>c__AnonStorey.furniture.RotationOffset, 0f) * snapPoint.transform.rotation;
						}
						else
						{
							Vector3 forward = <BuildPrefab>c__AnonStorey.furniture.Rotation.ToQuaternion() * Vector3.forward;
							forward = base.transform.rotation * new Vector3((!this.MirrorX) ? forward.x : (-forward.x), 0f, (!this.MirrorY) ? forward.z : (-forward.z));
							quaternion = Quaternion.LookRotation(forward);
						}
						Matrix4x4 matrix = Matrix4x4.TRS(position, quaternion, Vector3.one);
						Vector2[] array = (furnitureComponent.BuildBoundary != null) ? furnitureComponent.BuildBoundary.SelectInPlace((Vector2 x) => matrix.MultiplyPoint(x.ToVector3(0f)).FlattenVector3()) : new Vector2[0];
						Vector2[] ps = array;
						float height = furnitureComponent.Height1;
						float height2 = furnitureComponent.Height2;
						Room r = key;
						bool wallFurn = furnitureComponent.WallFurn;
						Furniture[] ignore;
						if (furnitureComponent.IsSnapping)
						{
							(ignore = new Furniture[1])[0] = snapPoint.Parent;
						}
						else
						{
							ignore = new Furniture[0];
						}
						if (FurnitureBuilder.IsValid(ps, height, height2, r, wallFurn, ignore) && (!furnitureComponent.TwoFloors || FurnitureBuilder.IsValid(array, furnitureComponent.Height1, furnitureComponent.Height2, FurnitureBuilder.GetBestRoom(floor + 1, position.FlattenVector3(), furnitureComponent, matrix), furnitureComponent.WallFurn, new Furniture[0])))
						{
							num += furnitureComponent.Cost;
							Furniture furniture2 = FurnitureBuilder.MakeFurn(position, quaternion, key, wallEdge7, edge, wallPos, snapPoint, furnitureComponent.gameObject, <BuildPrefab>c__AnonStorey.furniture.RotationOffset, true, (!cost) ? 0f : furnitureComponent.Cost);
							FurnitureBuilder.CopyStyle(<BuildPrefab>c__AnonStorey.furniture, furniture2);
							dictionary[<BuildPrefab>c__AnonStorey.furniture.ID] = furniture2;
						}
					}
				}
				IL_D13:;
			}
		}
		list.ForEach(delegate(KeyValuePair<Room, BuildingPrefab.RoomObject> x)
		{
			x.Key.OptimizeSegments();
			x.Key.RefreshNoise();
		});
		Dictionary<uint, List<Room>> dictionary2 = (from x in list
		group x by x.Value.RoomGroupID).ToDictionary((IGrouping<uint, KeyValuePair<Room, BuildingPrefab.RoomObject>> x) => x.Key, (IGrouping<uint, KeyValuePair<Room, BuildingPrefab.RoomObject>> x) => (from y in x
		select y.Key).ToList<Room>());
		foreach (List<Room> list4 in from x in dictionary2.Values
		where x.Count > 1
		select x)
		{
			Room room2 = list4[0];
			for (int num8 = 1; num8 < list4.Count; num8++)
			{
				list4[num8].ParentRoom = room2;
				room2.ChildrenRooms.Add(list4[num8]);
			}
		}
		if (undo)
		{
			GameSettings instance = GameSettings.Instance;
			UndoObject.UndoAction[] array2 = new UndoObject.UndoAction[1];
			array2[0] = new UndoObject.UndoAction((from x in list
			select x.Key).ToArray<Room>(), num);
			instance.AddUndo(array2);
		}
	}

	public static RoomCloneTool Instance;

	public Material ValidMat;

	public Material InvalidMat;

	[NonSerialized]
	public BuildingPrefab Prefab;

	private Renderer rend;

	private float PreCost;

	private Vector2[][][] Bounds;

	private Vector2[][][] VisualBounds;

	private Vector2 Center;

	private int Height;

	public GameObject ArrowX;

	public GameObject ArrowY;

	public bool MirrorX;

	public bool MirrorY;

	public MiniMapMaker MapMaker;

	private Vector2 LastPos;
}
