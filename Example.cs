﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using DevConsole;
using StatementParser;
using UnityEngine;
using UnityEngine.EventSystems;

public class Example : MonoBehaviour
{
	private void Start()
	{
		string name = "TAKE_ALL_LAND";
		if (Example.<>f__mg$cache0 == null)
		{
			Example.<>f__mg$cache0 = new Command.ConsoleMethod(Example.TakeAllLand);
		}
		global::DevConsole.Console.AddCommand(new Command(name, Example.<>f__mg$cache0, "Claims the entire plot"));
		string name2 = "WRITE_ERROR";
		if (Example.<>f__mg$cache1 == null)
		{
			Example.<>f__mg$cache1 = new Command.ConsoleMethod(Example.WriteErrors);
		}
		global::DevConsole.Console.AddCommand(new Command(name2, Example.<>f__mg$cache1, "Show all errors that has occured during this save"));
		string name3 = "TOGGLE_LIGHTS";
		if (Example.<>f__mg$cache2 == null)
		{
			Example.<>f__mg$cache2 = new Command<bool>.ConsoleMethod(Example.ToggleLights);
		}
		global::DevConsole.Console.AddCommand(new Command<bool>(name3, Example.<>f__mg$cache2, "Toggles whether lamps should always be on"));
		string name4 = "UNLOCK_FURNITURE";
		if (Example.<>f__mg$cache3 == null)
		{
			Example.<>f__mg$cache3 = new Command.ConsoleMethod(Example.UnlockFurniture);
		}
		global::DevConsole.Console.AddCommand(new Command(name4, Example.<>f__mg$cache3, "Unlocks all furniture"));
		string name5 = "SPAWN_GUEST";
		if (Example.<>f__mg$cache4 == null)
		{
			Example.<>f__mg$cache4 = new Command.ConsoleMethod(Example.SpawnGuest);
		}
		global::DevConsole.Console.AddCommand(new Command(name5, Example.<>f__mg$cache4, "Spawns a guest, that appears in the game for a receptionist"));
		string name6 = "SPAWN_EMPLOYEES";
		if (Example.<>f__mg$cache5 == null)
		{
			Example.<>f__mg$cache5 = new Command<int, string>.ConsoleMethod(Example.HireEmployees);
		}
		global::DevConsole.Console.AddCommand(new Command<int, string>(name6, Example.<>f__mg$cache5, "Spawns the specified employees in the specified role"));
		string name7 = "TOGGLE_SKYSCRAPERS";
		if (Example.<>f__mg$cache6 == null)
		{
			Example.<>f__mg$cache6 = new Command<bool>.ConsoleMethod(Example.ToggleScraperTransparency);
		}
		global::DevConsole.Console.AddCommand(new Command<bool>(name7, Example.<>f__mg$cache6, "Toggle whether skyscrapers should become see-through"));
		string name8 = "MAX_EMPLOYEE_STATS";
		if (Example.<>f__mg$cache7 == null)
		{
			Example.<>f__mg$cache7 = new Command.ConsoleMethod(Example.MaxEmployee);
		}
		global::DevConsole.Console.AddCommand(new Command(name8, Example.<>f__mg$cache7, "Maxes the stats of all selected employees"));
		string name9 = "SET_EMPLOYEE_STATS";
		if (Example.<>f__mg$cache8 == null)
		{
			Example.<>f__mg$cache8 = new Command<float>.ConsoleMethod(Example.SetEmployeeStats);
		}
		global::DevConsole.Console.AddCommand(new Command<float>(name9, Example.<>f__mg$cache8, "Sets the selected employees stats to the specified value between 0-1"));
		string name10 = "SET_EMPLOYEE_STAT";
		if (Example.<>f__mg$cache9 == null)
		{
			Example.<>f__mg$cache9 = new Command<string, float>.ConsoleMethod(Example.SetEmployeeStat);
		}
		global::DevConsole.Console.AddCommand(new Command<string, float>(name10, Example.<>f__mg$cache9, "Sets a specific stat of the selected employees to the specified value between 0-1"));
		string name11 = "RESET_MOOD";
		if (Example.<>f__mg$cacheA == null)
		{
			Example.<>f__mg$cacheA = new Command.ConsoleMethod(Example.ResetMoods);
		}
		global::DevConsole.Console.AddCommand(new Command(name11, Example.<>f__mg$cacheA, "Reset thoughts and mood affectors of selected employees"));
		string name12 = "SPAWN_CAR";
		if (Example.<>f__mg$cacheB == null)
		{
			Example.<>f__mg$cacheB = new Command<int>.ConsoleMethod(Example.SpawnDebugCar);
		}
		global::DevConsole.Console.AddCommand(new Command<int>(name12, Example.<>f__mg$cacheB, "Spawns the specified amount of cars, which park somewhere for a couple of minutes and drives off"));
		string name13 = "ADD_MONEY";
		if (Example.<>f__mg$cacheC == null)
		{
			Example.<>f__mg$cacheC = new Command<float>.ConsoleMethod(Example.AddMoney);
		}
		global::DevConsole.Console.AddCommand(new Command<float>(name13, Example.<>f__mg$cacheC, "Adds the specified amount of money"));
		string name14 = "ADD_FANS";
		if (Example.<>f__mg$cacheD == null)
		{
			Example.<>f__mg$cacheD = new Command<string, string, int>.ConsoleMethod(Example.AddReputation);
		}
		global::DevConsole.Console.AddCommand(new Command<string, string, int>(name14, Example.<>f__mg$cacheD, "Adds the specified amount of fans to the specified software category"));
		string name15 = "SET_BUSINESS_REP";
		if (Example.<>f__mg$cacheE == null)
		{
			Example.<>f__mg$cacheE = new Command<float>.ConsoleMethod(Example.SetBusinessRep);
		}
		global::DevConsole.Console.AddCommand(new Command<float>(name15, Example.<>f__mg$cacheE, "Sets business reputation to the specified level in percent"));
		string name16 = "CLEAR_DIRT";
		if (Example.<>f__mg$cacheF == null)
		{
			Example.<>f__mg$cacheF = new Command.ConsoleMethod(Example.ClearDirt);
		}
		global::DevConsole.Console.AddCommand(new Command(name16, Example.<>f__mg$cacheF, "Clears dirt in all rooms"));
		string name17 = "SKIP_DAYS";
		if (Example.<>f__mg$cache10 == null)
		{
			Example.<>f__mg$cache10 = new Command<int>.ConsoleMethod(Example.SkipDay);
		}
		global::DevConsole.Console.AddCommand(new Command<int>(name17, Example.<>f__mg$cache10, "Skips the specified amount of days. This might freeze the game for a while"));
		string name18 = "SKIP_HOURS";
		if (Example.<>f__mg$cache11 == null)
		{
			Example.<>f__mg$cache11 = new Command<int>.ConsoleMethod(Example.SkipHour);
		}
		global::DevConsole.Console.AddCommand(new Command<int>(name18, Example.<>f__mg$cache11, "Skips the specified amount of hours"));
		string name19 = "SKIP_TO";
		if (Example.<>f__mg$cache12 == null)
		{
			Example.<>f__mg$cache12 = new Command<int>.ConsoleMethod(Example.SkipTo);
		}
		global::DevConsole.Console.AddCommand(new Command<int>(name19, Example.<>f__mg$cache12, "Skips to the specified hour between 0 and 23"));
		global::DevConsole.Console.AddCommand(new Command("INSTA_DEVELOP_DESIGN", new Command.ConsoleMethod(this.InstaRelease), "Instantly releases software in design document window if modded"));
		string name20 = "AGE_EMPLOYEES";
		if (Example.<>f__mg$cache13 == null)
		{
			Example.<>f__mg$cache13 = new Command<int>.ConsoleMethod(Example.AgeEmployees);
		}
		global::DevConsole.Console.AddCommand(new Command<int>(name20, Example.<>f__mg$cache13, "Ages all selected employees the specified amount of months"));
		string name21 = "RELOAD_FURNITURE";
		if (Example.<>f__mg$cache14 == null)
		{
			Example.<>f__mg$cache14 = new Command.ConsoleMethod(FurnitureLoader.ReLoadFurniture);
		}
		global::DevConsole.Console.AddCommand(new Command(name21, Example.<>f__mg$cache14, "Reloads all modded furniture with immediate effect"));
		global::DevConsole.Console.AddCommand(new Command<bool>("FURNITURE_DEBUG", delegate(bool x)
		{
			ActiveFurnDebug.Activate(x);
		}, "Toggle whether to show the build(red->cyan) and nav(green->pink) boundary, interaction points(blue) and snap points(yellow) of the selected furniture"));
		string name22 = "FURNITURE_THUMBNAIL";
		if (Example.<>f__mg$cache15 == null)
		{
			Example.<>f__mg$cache15 = new Command<string>.ConsoleMethod(Example.FurnitureThumbnail);
		}
		global::DevConsole.Console.AddCommand(new Command<string>(name22, Example.<>f__mg$cache15, "Generates a thumbnail for the specified furniture. Only works in the main menu"));
		string name23 = "EXPORT_FURNITURE_BOUNDS";
		if (Example.<>f__mg$cache16 == null)
		{
			Example.<>f__mg$cache16 = new Command<string>.ConsoleMethod(Example.FurnitureBounds);
		}
		global::DevConsole.Console.AddCommand(new Command<string>(name23, Example.<>f__mg$cache16, "Exports boundary information to the XML of the specified furniture."));
		string name24 = "FLYCAM_DRAW_DISTANCE";
		if (Example.<>f__mg$cache17 == null)
		{
			Example.<>f__mg$cache17 = new Command<float>.ConsoleMethod(Example.FlycamDistance);
		}
		global::DevConsole.Console.AddCommand(new Command<float>(name24, Example.<>f__mg$cache17, "The draw distance multiplier to use in first person mode. Between 1 and 10."));
		string name25 = "ACTIVATE_WAYPOINT_EDITOR";
		if (Example.<>f__mg$cache18 == null)
		{
			Example.<>f__mg$cache18 = new Command.ConsoleMethod(Example.WayPoints);
		}
		global::DevConsole.Console.AddCommand(new Command(name25, Example.<>f__mg$cache18, "Activates a waypoint animation system for first person mode. Press Backspace to add a waypoint and Enter to play."));
		string name26 = "TOGGLE_CEILING";
		if (Example.<>f__mg$cache19 == null)
		{
			Example.<>f__mg$cache19 = new Command<bool>.ConsoleMethod(Example.ToggleCeiling);
		}
		global::DevConsole.Console.AddCommand(new Command<bool>(name26, Example.<>f__mg$cache19, "Toggles the generation of ceiling meshes for first person mode. Might need a reload and has a performance impact."));
		string name27 = "RELOAD_MOD";
		if (Example.<>f__mg$cache1A == null)
		{
			Example.<>f__mg$cache1A = new Command<string>.ConsoleMethod(Example.ReloadMod);
		}
		global::DevConsole.Console.AddCommand(new Command<string>(name27, Example.<>f__mg$cache1A, "Reloads the specified mod, but not for the currently running game."));
		string name28 = "UI_UNDER_MOUSE";
		if (Example.<>f__mg$cache1B == null)
		{
			Example.<>f__mg$cache1B = new Command.ConsoleMethod(Example.UIUnderMouse);
		}
		global::DevConsole.Console.AddCommand(new Command(name28, Example.<>f__mg$cache1B, "Lists all UI elements currently under the mouse cursor"));
		string name29 = "UI_COMPONENTS";
		if (Example.<>f__mg$cache1C == null)
		{
			Example.<>f__mg$cache1C = new Command<string>.ConsoleMethod(Example.UIComponents);
		}
		global::DevConsole.Console.AddCommand(new Command<string>(name29, Example.<>f__mg$cache1C, "Get a list of all components attached to a UI element"));
		global::DevConsole.Console.AddCommand(new Command("RELOAD_TUTORIALS", new Command.ConsoleMethod(this.ReloadTutorials), "Reloads all tutorials"));
		global::DevConsole.Console.AddCommand(new Command("RELOAD_LOCALIZATION", delegate
		{
			Localization.LoadLanguages();
		}, "Reloads all localizations, but doesn't update UI"));
		string name30 = "STORE_CAM_STATE";
		if (Example.<>f__mg$cache1D == null)
		{
			Example.<>f__mg$cache1D = new Command.ConsoleMethod(Example.StoreCameraState);
		}
		global::DevConsole.Console.AddCommand(new Command(name30, Example.<>f__mg$cache1D, "Saves current camera position"));
		string name31 = "RESTORE_CAM_STATE";
		if (Example.<>f__mg$cache1E == null)
		{
			Example.<>f__mg$cache1E = new Command.ConsoleMethod(Example.RestoreCameraState);
		}
		global::DevConsole.Console.AddCommand(new Command(name31, Example.<>f__mg$cache1E, "Loads last saved camera position"));
	}

	private void ParseCommand(string command)
	{
		LineParse.TreeNode treeNode = LineParse.Parse(command);
		object obj = LineParse.Execute(treeNode, new Example.ParserWorld());
		global::DevConsole.Console.Log((obj != null) ? obj.ToString() : "Nothing returned");
	}

	private void ParseCommandOut(string command)
	{
		LineParse.TreeNode treeNode = LineParse.Parse(command);
		global::DevConsole.Console.Log(LineParse.WriteTree(treeNode));
	}

	private void InstaRelease()
	{
		if (HUD.Instance != null && HUD.Instance.docWindow.Window.Shown)
		{
			HUD.Instance.docWindow.DevelopClick(true);
		}
	}

	public static void HireEmployees(int n, string role)
	{
		Employee.EmployeeRole employeeRole = (Employee.EmployeeRole)Enum.Parse(typeof(Employee.EmployeeRole), role, true);
		float num = Employee.AverageWage * Employee.RoleSalary[(int)employeeRole];
		foreach (Employee employee in HUD.Instance.hireWindow.GenerateEmployees(n, Employee.WageBracket.High, employeeRole, 10))
		{
			Actor actor = GameSettings.Instance.SpawnActor(employee.Female);
			actor.employee = employee;
		}
	}

	private void ReloadTutorials()
	{
		TutorialSystem.Instance.LoadTutorials();
		if (TutorialSystem.Instance.CurrentTutorial != null)
		{
			TutorialSystem.Instance.CurrentTutorial = TutorialSystem.Tutorials[TutorialSystem.Instance.CurrentTutorialName];
			TutorialSystem.Instance.CurrentMessage--;
			TutorialSystem.Instance.AdvanceTutorial();
		}
	}

	public static void UIUnderMouse()
	{
		List<string> list = new List<string>();
		HashSet<Transform> hashSet = new HashSet<Transform>();
		if (EventSystem.current != null)
		{
			PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
			pointerEventData.pointerId = -1;
			pointerEventData.position = Input.mousePosition;
			List<RaycastResult> list2 = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointerEventData, list2);
			foreach (RaycastResult raycastResult in list2)
			{
				Transform transform = raycastResult.gameObject.transform;
				if (!hashSet.Contains(transform))
				{
					string pathToObject = WindowManager.GetPathToObject(transform, hashSet);
					if (pathToObject != null)
					{
						list.Add(pathToObject);
					}
				}
			}
		}
		if (list.Count == 0)
		{
			global::DevConsole.Console.Log("No objects found");
		}
		else
		{
			list.Insert(0, "Found " + list.Count + " objects:");
			global::DevConsole.Console.Log(string.Join("\n", list.ToArray()));
		}
	}

	public static void UIComponents(string arg)
	{
		RectTransform rectTransform = WindowManager.FindElementPath(arg, null);
		if (rectTransform != null)
		{
			Component[] components = rectTransform.gameObject.GetComponents<Component>();
			List<string> list = new List<string>
			{
				"Found components:"
			};
			list.AddRange(from x in components
			select x.GetType().Name);
			global::DevConsole.Console.Log(string.Join("\n", list.ToArray()));
		}
		else
		{
			global::DevConsole.Console.Log("Element does not exist");
		}
	}

	public static void ReloadMod(string args)
	{
		ModPackage modPackage = GameData.ModPackages.FirstOrDefault((ModPackage x) => x.Name.Equals(args));
		if (modPackage == null)
		{
			global::DevConsole.Console.LogError("Could not find mod");
			return;
		}
		ModPackage item;
		try
		{
			item = ModPackage.Load(modPackage.Root);
		}
		catch (Exception ex)
		{
			global::DevConsole.Console.LogError("Failed loading mod with error:\n" + ex.Message);
			return;
		}
		GameData.ModPackages.Remove(modPackage);
		GameData.ModPackages.Add(item);
		global::DevConsole.Console.Log("Mod reloaded, start a new game to use");
	}

	public static void ToggleCeiling(bool val)
	{
		Cheats.CeilingMeshes = val;
	}

	public static void StoreCameraState()
	{
		Example.StCamFloor = GameSettings.Instance.ActiveFloor;
		Example.StCamPos = CameraScript.Instance.transform.position.FlattenVector3();
		Example.StCamDir = CameraScript.Instance.transform.rotation;
		Example.StCamZoom = CameraScript.Instance.mainCam.transform.localPosition.z;
	}

	public static void RestoreCameraState()
	{
		GameSettings.Instance.ActiveFloor = Example.StCamFloor;
		Furniture.UpdateEdgeDetection();
		GameSettings.Instance.sRoomManager.ChangeFloor();
		CameraScript.GotoPos(Example.StCamPos);
		CameraScript.Instance.transform.rotation = Example.StCamDir;
		CameraScript.Instance.mainCam.transform.localPosition = Vector3.forward * Example.StCamZoom;
	}

	public static void FlycamDistance(float args)
	{
		CameraScript.FlyCamDistance = Mathf.Clamp(args, 1f, 10f);
	}

	public static void WayPoints()
	{
		if (WayPointEditorWindow.Instance != null)
		{
			WayPointEditorWindow.Instance.enabled = true;
			WayPointEditorWindow.Instance.Window.Show();
		}
	}

	public static void FurnitureThumbnail(string args)
	{
		if (FurnitureThumbnailMaker.Instance != null)
		{
			FurnitureThumbnailMaker.Instance.TakePicture(args);
		}
		else
		{
			global::DevConsole.Console.Log("Please execute this command in the main menu");
		}
	}

	public static void FurnitureBounds(string args)
	{
		FurnitureLoader.BakeBounds(ObjectDatabase.Instance.GetFurnitureComponent(args));
	}

	public static void AgeEmployees(int args)
	{
		if (SelectorController.Instance != null)
		{
			SelectorController.Instance.Selected.OfType<Actor>().ToList<Actor>().ForEach(delegate(Actor x)
			{
				x.employee.AgeMonth += args;
				x.UpdateAgeLook();
			});
		}
	}

	public static void SkipDay(int args)
	{
		if (GameSettings.Instance != null && !GameSettings.ForcePause)
		{
			int hour = TimeOfDay.Instance.Hour;
			bool flag = false;
			for (int i = 0; i < args; i++)
			{
				do
				{
					flag |= TimeOfDay.Instance.AddHour(!flag);
				}
				while (!GameSettings.ForcePause && TimeOfDay.Instance.Hour != hour && !flag);
				if (GameSettings.ForcePause || flag)
				{
					break;
				}
			}
		}
	}

	public static void SkipHour(int args)
	{
		if (GameSettings.Instance != null && !GameSettings.ForcePause)
		{
			bool flag = false;
			for (int i = 0; i < args; i++)
			{
				flag |= TimeOfDay.Instance.AddHour(!flag);
				if (GameSettings.ForcePause || flag)
				{
					break;
				}
			}
		}
	}

	public static void SkipTo(int args)
	{
		args = Mathf.Clamp(args, 0, 23);
		if (GameSettings.Instance != null && !GameSettings.ForcePause)
		{
			bool flag = false;
			do
			{
				flag |= TimeOfDay.Instance.AddHour(!flag);
			}
			while (!GameSettings.ForcePause && TimeOfDay.Instance.Hour != args && !flag);
		}
	}

	public static void ClearDirt()
	{
		if (GameSettings.Instance != null)
		{
			GameSettings.Instance.sRoomManager.Rooms.ForEach(delegate(Room room)
			{
				room.ClearDirt();
			});
		}
	}

	public static void AddMoney(float args)
	{
		if (GameSettings.Instance != null)
		{
			GameSettings.Instance.MyCompany.MakeTransaction(args, Company.TransactionCategory.Sales, null);
		}
	}

	public static void SetBusinessRep(float pct)
	{
		if (GameSettings.Instance != null)
		{
			float num = pct - GameSettings.Instance.MyCompany.BusinessReputation;
			GameSettings.Instance.MyCompany.ChangeBusinessRep(num * (float)Company.BusinessRepStars, "Cheat", (float)Company.BusinessRepStars);
		}
	}

	public static void AddReputation(string soft, string cat, int fans)
	{
		if (GameSettings.Instance != null)
		{
			GameSettings.Instance.MyCompany.AddFans(fans, soft, cat);
		}
	}

	public static void SpawnDebugCar(int num)
	{
		if (GameSettings.Instance != null)
		{
			GameSettings.Instance.StartCoroutine(Example.SpawnCar(num));
		}
	}

	public static IEnumerator SpawnCar(int num)
	{
		for (int i = 0; i < num; i++)
		{
			bool rich = UnityEngine.Random.value > 0.9f;
			CarScript car = RoadManager.Instance.CreateCar((!rich) ? 1 : 2);
			car.GetComponent<NormalCar>().Debug = true;
			car.Init();
			yield return new WaitForSeconds(0.25f);
		}
		yield break;
	}

	public static void ResetMoods()
	{
		if (SelectorController.Instance != null)
		{
			SelectorController.Instance.Selected.OfType<Actor>().ToList<Actor>().ForEach(delegate(Actor x)
			{
				x.employee.Thoughts.Clear();
				x.employee.JobSatisfaction = 1f;
			});
		}
	}

	public static void ReloadMoods()
	{
		GameData.LoadMoodEffects();
		if (GameSettings.Instance != null)
		{
			for (int i = 0; i < GameSettings.Instance.sActorManager.Actors.Count; i++)
			{
				Actor actor = GameSettings.Instance.sActorManager.Actors[i];
				for (int j = 0; j < actor.employee.Thoughts.List.Count; j++)
				{
					actor.employee.Thoughts.List[j].ResetMood();
				}
			}
		}
	}

	public static void MaxEmployee()
	{
		if (SelectorController.Instance != null)
		{
			SelectorController.Instance.Selected.OfType<Actor>().ToList<Actor>().ForEach(delegate(Actor x)
			{
				for (int i = 0; i < 5; i++)
				{
					x.employee.ChangeSkill((Employee.EmployeeRole)i, 1f, false);
					foreach (string specialization in GameSettings.Instance.Specializations)
					{
						x.employee.AddToSpecialization(Employee.EmployeeRole.Designer, specialization, 1f, 1f, true);
						x.employee.AddToSpecialization(Employee.EmployeeRole.Artist, specialization, 1f, 1f, true);
						x.employee.AddToSpecialization(Employee.EmployeeRole.Programmer, specialization, 1f, 1f, true);
					}
				}
			});
		}
	}

	public static void SetEmployeeStats(float stat)
	{
		if (SelectorController.Instance != null)
		{
			SelectorController.Instance.Selected.OfType<Actor>().ToList<Actor>().ForEach(delegate(Actor x)
			{
				for (int i = 0; i < 5; i++)
				{
					x.employee.ChangeSkillDirect((Employee.EmployeeRole)i, stat);
					foreach (string spec in GameSettings.Instance.Specializations)
					{
						x.employee.ChangeSpecializationDirect(Employee.EmployeeRole.Designer, spec, stat);
						x.employee.ChangeSpecializationDirect(Employee.EmployeeRole.Artist, spec, stat);
						x.employee.ChangeSpecializationDirect(Employee.EmployeeRole.Programmer, spec, stat);
					}
				}
			});
		}
	}

	public static void SetEmployeeStat(string name, float stat)
	{
		if (SelectorController.Instance != null)
		{
			SelectorController.Instance.Selected.OfType<Actor>().ToList<Actor>().ForEach(delegate(Actor x)
			{
				for (int i = 0; i < 5; i++)
				{
					if (name.Equals("Base"))
					{
						x.employee.ChangeSkillDirect((Employee.EmployeeRole)i, stat);
					}
					else
					{
						x.employee.ChangeSpecializationDirect(Employee.EmployeeRole.Designer, name, stat);
						x.employee.ChangeSpecializationDirect(Employee.EmployeeRole.Artist, name, stat);
						x.employee.ChangeSpecializationDirect(Employee.EmployeeRole.Programmer, name, stat);
					}
				}
			});
		}
	}

	public static void RndEmployeeStats()
	{
		if (SelectorController.Instance != null)
		{
			SelectorController.Instance.Selected.OfType<Actor>().ToList<Actor>().ForEach(delegate(Actor x)
			{
				for (int i = 0; i < 5; i++)
				{
					x.employee.ChangeSkillDirect((Employee.EmployeeRole)i, UnityEngine.Random.value);
				}
			});
		}
	}

	public static void ToggleScraperTransparency(bool val)
	{
		SkraperGen.NeverTransparent = val;
	}

	public static void SpawnGuest()
	{
		if (GameSettings.Instance != null)
		{
			Actor actor = GameSettings.Instance.SpawnActor(UnityEngine.Random.value > 0.5f);
			actor.employee = new Employee(SDateTime.Now(), Employee.EmployeeRole.Lead, UnityEngine.Random.value > 0.5f, Employee.WageBracket.High, GameSettings.Instance.Personalities, false, null, null, null, 1f, 0.1f);
			actor.employee.Salary = 20000f;
			actor.AItype = AI<Actor>.AIType.Guest;
			GameSettings.Instance.sActorManager.AddToAwaiting(actor, SDateTime.Now(), true, true);
		}
	}

	public static void ToggleLights(bool val)
	{
		Cheats.ForceLights = val;
	}

	public static void UnlockFurniture()
	{
		Cheats.UnlockFurn = !Cheats.UnlockFurn;
		if (HUD.Instance != null)
		{
			HUD.Instance.UpdateFurnitureButtons();
		}
	}

	public static void TakeAllLand()
	{
		if (GameSettings.Instance != null)
		{
			int count = GameSettings.Instance.Plots.Count;
			for (int i = 0; i < count; i++)
			{
				GameSettings.Instance.BuyPlot(GameSettings.Instance.Plots[0]);
			}
		}
	}

	public static void WriteErrors()
	{
		if (GameSettings.Instance != null)
		{
			if (GameSettings.Instance.Errors.Count == 0)
			{
				global::DevConsole.Console.Log("No errors present");
			}
			else
			{
				foreach (string text in GameSettings.Instance.Errors)
				{
					global::DevConsole.Console.Log(text);
				}
			}
		}
	}

	public DevConsole.Console DevConsole;

	public static bool EnableNavMesh;

	public static bool NavMeshPortal;

	public static bool UseNavWeight;

	public static bool EnableDirtTree;

	public static bool EnableNearness;

	public static bool CachedPaths;

	public static Vector2 StCamPos;

	public static Quaternion StCamDir;

	public static int StCamFloor;

	public static float StCamZoom;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache0;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache1;

	[CompilerGenerated]
	private static Command<bool>.ConsoleMethod <>f__mg$cache2;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache3;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache4;

	[CompilerGenerated]
	private static Command<int, string>.ConsoleMethod <>f__mg$cache5;

	[CompilerGenerated]
	private static Command<bool>.ConsoleMethod <>f__mg$cache6;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache7;

	[CompilerGenerated]
	private static Command<float>.ConsoleMethod <>f__mg$cache8;

	[CompilerGenerated]
	private static Command<string, float>.ConsoleMethod <>f__mg$cache9;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cacheA;

	[CompilerGenerated]
	private static Command<int>.ConsoleMethod <>f__mg$cacheB;

	[CompilerGenerated]
	private static Command<float>.ConsoleMethod <>f__mg$cacheC;

	[CompilerGenerated]
	private static Command<string, string, int>.ConsoleMethod <>f__mg$cacheD;

	[CompilerGenerated]
	private static Command<float>.ConsoleMethod <>f__mg$cacheE;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cacheF;

	[CompilerGenerated]
	private static Command<int>.ConsoleMethod <>f__mg$cache10;

	[CompilerGenerated]
	private static Command<int>.ConsoleMethod <>f__mg$cache11;

	[CompilerGenerated]
	private static Command<int>.ConsoleMethod <>f__mg$cache12;

	[CompilerGenerated]
	private static Command<int>.ConsoleMethod <>f__mg$cache13;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache14;

	[CompilerGenerated]
	private static Command<string>.ConsoleMethod <>f__mg$cache15;

	[CompilerGenerated]
	private static Command<string>.ConsoleMethod <>f__mg$cache16;

	[CompilerGenerated]
	private static Command<float>.ConsoleMethod <>f__mg$cache17;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache18;

	[CompilerGenerated]
	private static Command<bool>.ConsoleMethod <>f__mg$cache19;

	[CompilerGenerated]
	private static Command<string>.ConsoleMethod <>f__mg$cache1A;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache1B;

	[CompilerGenerated]
	private static Command<string>.ConsoleMethod <>f__mg$cache1C;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache1D;

	[CompilerGenerated]
	private static Command.ConsoleMethod <>f__mg$cache1E;

	public class ParserWorld
	{
		public GameSettings GameSettings
		{
			get
			{
				return GameSettings.Instance;
			}
		}

		public SelectorController SelectorController
		{
			get
			{
				return SelectorController.Instance;
			}
		}

		public HUD HUD
		{
			get
			{
				return HUD.Instance;
			}
		}

		public WindowManager WindowManager
		{
			get
			{
				return WindowManager.Instance;
			}
		}
	}
}
