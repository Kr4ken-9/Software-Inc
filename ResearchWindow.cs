﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ResearchWindow : MonoBehaviour
{
	private void Start()
	{
		foreach (string localItem2 in GameSettings.Instance.SoftwareTypes.Keys)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.SWButtonPrefab);
			Button component = gameObject.GetComponent<Button>();
			string localItem = localItem2;
			component.onClick.AddListener(delegate
			{
				this.Software = localItem;
				this.UpdateLists();
			});
			Text componentInChildren = gameObject.GetComponentInChildren<Text>();
			componentInChildren.text = localItem.LocSW();
			gameObject.transform.SetParent(this.SWPanel.transform, false);
			this.SWButtons[localItem] = gameObject;
		}
		this.UpdateLists();
	}

	public void ResearchClick()
	{
		if (this.Software != null && this.featureList.Selected.Count > 0)
		{
			Feature f = this.featureList.GetSelected<Feature>().First<Feature>();
			if (f.Research != null && !f.Researched && f.IsUnlocked(TimeOfDay.Instance.Year, null) && !GameSettings.Instance.IsResearching(this.Software, f.Name))
			{
				HUD.Instance.TeamSelectWindow.Show(false, GameSettings.Instance.GetDefaultTeams("Research"), delegate(string[] x)
				{
					ResearchWork researchWork = new ResearchWork(f.Name, this.Software);
					foreach (Team team in from tt in x
					select GameSettings.Instance.sActorManager.Teams[tt])
					{
						researchWork.AddDevTeam(team, false);
					}
					GameSettings.Instance.MyCompany.WorkItems.Add(researchWork);
					this.UpdateLists();
				}, "Research");
			}
			else if (f.Researched)
			{
				WindowManager.Instance.ShowMessageBox("ResearchOwnedError".Loc(), false, DialogWindow.DialogType.Error);
			}
		}
	}

	public void UpdateLists()
	{
		bool flag = false;
		IEnumerable<Feature> enumerable = null;
		foreach (KeyValuePair<string, GameObject> keyValuePair in this.SWButtons)
		{
			string sk = keyValuePair.Key;
			List<Feature> list = (from x in GameSettings.Instance.SoftwareTypes[keyValuePair.Key].Features.Values
			where x.Research != null && x.IsUnlocked(TimeOfDay.Instance.Year, null) && !GameSettings.Instance.IsResearching(sk, x.Name)
			select x).ToList<Feature>();
			bool flag2 = list.Count > 0;
			if (keyValuePair.Key.Equals(this.Software))
			{
				if (!flag2)
				{
					this.Software = null;
				}
				else
				{
					enumerable = list;
				}
			}
			flag = (flag || flag2);
			keyValuePair.Value.SetActive(flag2);
		}
		this.Counter.SetNumber(GameSettings.Instance.SoftwareTypes.Values.SumSafe((SoftwareType x) => x.Features.Values.Count((Feature y) => y.Research != null && !y.Researched && y.IsUnlocked(TimeOfDay.Instance.Year, null) && !GameSettings.Instance.IsResearching(x.Name, y.Name))));
		this.NoResearch.SetActive(!flag);
		this.featureList.Items.Clear();
		if (enumerable != null)
		{
			this.featureList.Items.AddRange(enumerable.Cast<object>());
		}
	}

	public void ToggleShow()
	{
		this.Window.Toggle(false);
		if (this.Window.Shown)
		{
			TutorialSystem.Instance.StartTutorial("Research", false);
		}
		this.Software = null;
		this.UpdateLists();
	}

	public GUIWindow Window;

	public GameObject SWButtonPrefab;

	public GameObject SWPanel;

	public GameObject NoResearch;

	public GUIListView featureList;

	public ButtonCounter Counter;

	[NonSerialized]
	private Dictionary<string, GameObject> SWButtons = new Dictionary<string, GameObject>();

	[NonSerialized]
	private string Software;
}
