﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TeamSelectWindow : MonoBehaviour
{
	public void Show(bool singleTeam, string selected, Action<string[]> accept, string saveCat = null)
	{
		this._singleTeam = singleTeam;
		this.AllNonePanel.SetActive(!singleTeam);
		this._saveCat = saveCat;
		this.Init((!string.IsNullOrEmpty(selected)) ? new HashSet<string>
		{
			selected
		} : null, null, false);
		this.OnAccept = accept;
		this.Window.Show();
	}

	public void Show(bool singleTeam, HashSet<string> selected, Action<string[]> accept, string saveCat = null)
	{
		this._singleTeam = singleTeam;
		this.AllNonePanel.SetActive(!singleTeam);
		this._saveCat = saveCat;
		this.Init(selected, null, false);
		this.OnAccept = accept;
		this.Window.Show();
	}

	public void Show(HashSet<string> selected, SimulatedCompany selectedCompany, Action<string[], SimulatedCompany> accept, string saveCat = null)
	{
		this._singleTeam = false;
		this.AllNonePanel.SetActive(true);
		this._saveCat = saveCat;
		this.Init(selected, selectedCompany, true);
		this.OnAccept = null;
		this.OnAccept2 = accept;
		this.Window.Show();
	}

	private void Init(HashSet<string> selected, SimulatedCompany selectedCompany, bool subsidiaries)
	{
		foreach (TeamToggle teamToggle in this.TeamToggles.ToArray())
		{
			UnityEngine.Object.Destroy(teamToggle.gameObject);
		}
		this.TeamToggles.Clear();
		if (subsidiaries)
		{
			foreach (KeyValuePair<uint, SimulatedCompany> keyValuePair in GameSettings.Instance.simulation.Companies)
			{
				SimulatedCompany value = keyValuePair.Value;
				if (value.IsPlayerOwned())
				{
					this.CreateToggle(value, selectedCompany == value);
				}
			}
		}
		foreach (string text in GameSettings.Instance.sActorManager.Teams.Keys)
		{
			this.CreateToggle(text, selected != null && selected.Contains(text));
		}
		this.OrderTeams();
	}

	private void OrderTeams()
	{
		int num = 0;
		foreach (TeamToggle teamToggle in from x in this.TeamToggles
		orderby (x.Company != null) ? 0 : 1, (x.Company != null) ? x.Company.Name : x.Team
		select x)
		{
			teamToggle.transform.SetSiblingIndex(num);
			num++;
		}
	}

	private void CreateToggle(SimulatedCompany company, bool selected)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.TogglePrefab);
		TeamToggle tt = gameObject.GetComponent<TeamToggle>();
		tt.Company = company;
		if (selected)
		{
			tt.MainToggle.isOn = true;
		}
		tt.MainToggle.onValueChanged.AddListener(delegate(bool x)
		{
			if (x)
			{
				foreach (TeamToggle teamToggle in this.TeamToggles)
				{
					if (teamToggle != tt)
					{
						teamToggle.MainToggle.isOn = false;
					}
				}
			}
		});
		tt.transform.SetParent(this.ContentPanel.transform, false);
		this.TeamToggles.Add(tt);
	}

	private void CreateToggle(string team, bool selected)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.TogglePrefab);
		TeamToggle component = gameObject.GetComponent<TeamToggle>();
		component.Team = team;
		if (this._singleTeam)
		{
			component.MainToggle.group = this.TGroup;
		}
		else
		{
			component.MainToggle.onValueChanged.AddListener(delegate(bool x)
			{
				if (x)
				{
					foreach (TeamToggle teamToggle in this.TeamToggles)
					{
						if (teamToggle.Company != null)
						{
							teamToggle.MainToggle.isOn = false;
						}
					}
				}
			});
		}
		if (selected)
		{
			component.MainToggle.isOn = true;
		}
		component.transform.SetParent(this.ContentPanel.transform, false);
		this.TeamToggles.Add(component);
	}

	private void Update()
	{
		if (WindowManager.Instance.IsActiveWindow(this.Window) && (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)))
		{
			this.Accept();
		}
	}

	public void SelectAllNone(bool all)
	{
		foreach (TeamToggle teamToggle in this.TeamToggles)
		{
			teamToggle.MainToggle.isOn = (all && teamToggle.Company == null);
		}
	}

	public void Accept()
	{
		TeamToggle teamToggle = this.TeamToggles.FirstOrDefault((TeamToggle x) => x.MainToggle.isOn && x.Company != null);
		string[] array;
		if (teamToggle == null)
		{
			array = (from x in this.TeamToggles
			where x.MainToggle.isOn
			select x.Team).ToArray<string>();
		}
		else
		{
			array = null;
		}
		string[] array2 = array;
		if (this.OnAccept != null)
		{
			this.OnAccept(array2);
		}
		else if (this.OnAccept2 != null)
		{
			this.OnAccept2(array2, (!(teamToggle != null)) ? null : teamToggle.Company);
		}
		if (this._saveCat != null && array2 != null)
		{
			GameSettings.Instance.TeamDefaults[this._saveCat] = array2.ToHashSet<string>();
		}
		this.Window.Close();
	}

	public void NewTeam()
	{
		WindowManager.SpawnInputDialog("Team name".Loc(), "New team".Loc(), string.Empty, delegate(string s)
		{
			if (HUD.Instance.TeamWindow.CreateTeam(s))
			{
				this.CreateToggle(s, true);
				this.OrderTeams();
			}
		}, null);
	}

	public GameObject TogglePrefab;

	public ToggleGroup TGroup;

	public GUIWindow Window;

	public GameObject ContentPanel;

	public GameObject AllNonePanel;

	[NonSerialized]
	private List<TeamToggle> TeamToggles = new List<TeamToggle>();

	[NonSerialized]
	private Action<string[]> OnAccept;

	[NonSerialized]
	private Action<string[], SimulatedCompany> OnAccept2;

	[NonSerialized]
	private string _saveCat;

	private bool _singleTeam;
}
