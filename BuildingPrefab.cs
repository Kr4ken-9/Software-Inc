﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Steamworks;
using UnityEngine;

public class BuildingPrefab : IWorkshopItem
{
	public BuildingPrefab(string name, BuildingPrefab.RoomObject[] rooms, SVector3[] edges)
	{
		this.ItemTitle = name;
		this.Rooms = rooms;
		this.Edges = edges;
	}

	public XMLParser.XMLNode ToXmlNode()
	{
		string text = "Blueprint";
		XMLParser.XMLNode[] array = new XMLParser.XMLNode[2];
		array[0] = new XMLParser.XMLNode("Rooms", this.Rooms.SelectInPlace((BuildingPrefab.RoomObject x) => x.ToXmlNode()));
		array[1] = new XMLParser.XMLNode("Edges", this.Edges.SelectInPlace((SVector3 x) => new XMLParser.XMLNode("Edge", x.Serialize(), null)));
		return new XMLParser.XMLNode(text, array);
	}

	public static BuildingPrefab FromXMLNode(XMLParser.XMLNode node)
	{
		SVector3[] edges = node.GetNode("Edges", true).GetNodes("Edge", true).SelectInPlace((XMLParser.XMLNode x) => SVector3.Deserialize(x.Value, false));
		BuildingPrefab.RoomObject[] rooms = node.GetNode("Rooms", true).GetNodes("Room", true).SelectInPlace((XMLParser.XMLNode x, int i) => BuildingPrefab.RoomObject.FromXMLNode(x, (uint)i));
		return new BuildingPrefab(null, rooms, edges);
	}

	public static bool ValidCheck(Room[] rooms)
	{
		int min = rooms.Min((Room x) => x.Floor);
		foreach (Room room in from x in rooms
		where x.Floor > min
		select x)
		{
			if (!GameSettings.Instance.sRoomManager.IsSupported(room.Edges.Select((WallEdge x) => x.Pos), room.Floor, null, false, rooms.ToHashSet<Room>()))
			{
				return false;
			}
		}
		return true;
	}

	public static BuildingPrefab SaveRooms(Room[] rooms, bool onlyRooms = false, bool removeInvalidSegments = false, bool withData = false, bool rentInfo = false)
	{
		Dictionary<WallEdge, int> edgeNum = new Dictionary<WallEdge, int>();
		List<WallEdge> list = new List<WallEdge>();
		int num = 0;
		foreach (WallEdge wallEdge in rooms.SelectMany((Room x) => x.Edges).Distinct<WallEdge>())
		{
			edgeNum[wallEdge] = num;
			list.Add(wallEdge);
			num++;
		}
		HashSet<Room> rs = rooms.ToHashSet<Room>();
		BuildingPrefab.RoomObject[] rooms2 = rooms.SelectInPlace(delegate(Room x)
		{
			string[] materials = new string[]
			{
				x.InsideMat,
				x.OutsideMat,
				x.FloorMat,
				x.FenceStyle
			};
			SVector3[] colors = new SVector3[]
			{
				x.InsideColor,
				x.OutsideColor,
				x.FloorColor
			};
			bool outdoors = x.Outdoors;
			int floor = x.Floor;
			int[] edges = (from z in x.Edges
			select edgeNum[z]).ToArray<int>();
			BuildingPrefab.FurnitureObject[] furniture;
			if (onlyRooms)
			{
				furniture = new BuildingPrefab.FurnitureObject[0];
			}
			else
			{
				IEnumerable<Furniture> source = from z in x.GetFurnitures()
				where z != null && z.Parent == x
				orderby z.GetSnappingDepth()
				select z;
				if (BuildingPrefab.<>f__mg$cache0 == null)
				{
					BuildingPrefab.<>f__mg$cache0 = new Func<Furniture, BuildingPrefab.FurnitureObject>(BuildingPrefab.SaveFurniture);
				}
				furniture = source.Select(BuildingPrefab.<>f__mg$cache0).ToArray<BuildingPrefab.FurnitureObject>();
			}
			BuildingPrefab.SegmentObject[] segments;
			if (onlyRooms)
			{
				segments = new BuildingPrefab.SegmentObject[0];
			}
			else
			{
				IEnumerable<RoomSegment> source2 = x.GetSegments(null).Where(delegate(RoomSegment z)
				{
					Room primaryRoom = z.GetPrimaryRoom();
					return (!removeInvalidSegments || z.ValidSnap(false, rs, true)) && (x == primaryRoom || !rooms.Contains(primaryRoom));
				});
				if (BuildingPrefab.<>f__mg$cache1 == null)
				{
					BuildingPrefab.<>f__mg$cache1 = new Func<RoomSegment, BuildingPrefab.SegmentObject>(BuildingPrefab.SaveSegment);
				}
				segments = source2.Select(BuildingPrefab.<>f__mg$cache1).ToArray<BuildingPrefab.SegmentObject>();
			}
			return new BuildingPrefab.RoomObject(materials, colors, outdoors, floor, edges, furniture, segments, x.Area, (!withData) ? null : x.SerializeThis(GameReader.LoadMode.Full), (!rentInfo) ? x.DID : (x.ParentRoom ?? x).DID, !rentInfo || x.Rentable);
		});
		return new BuildingPrefab(null, rooms2, list.SelectInPlace((WallEdge x) => x.Pos));
	}

	public static BuildingPrefab.SegmentObject SaveSegment(RoomSegment segment)
	{
		return new BuildingPrefab.SegmentObject(segment.name, segment.transform.position, segment.WallWidth, segment.Directional && segment.ParentRooms[0] == null);
	}

	public static BuildingPrefab.FurnitureObject SaveFurniture(Furniture furn)
	{
		return new BuildingPrefab.FurnitureObject(furn.name, furn.DID, furn.OriginalPosition, furn.transform.rotation, furn.RotationOffset, (!furn.IsSnapping) ? 0u : furn.SnappedTo.Parent.DID, (!furn.IsSnapping) ? 0 : furn.SnappedTo.Id, new SVector3[]
		{
			furn.ColorPrimary,
			furn.ColorSecondary,
			furn.ColorTertiary
		});
	}

	public string ItemTitle { get; set; }

	public PublishedFileId_t? SteamID { get; set; }

	public bool SetTitle
	{
		get
		{
			return this._setTitle;
		}
		set
		{
			this._setTitle = value;
		}
	}

	public bool CanUpload
	{
		get
		{
			return this._canUpload;
		}
		set
		{
			this._canUpload = value;
		}
	}

	public string GetWorkshopType()
	{
		return "Blueprint";
	}

	public string FolderPath()
	{
		return Path.GetFullPath(this.Root);
	}

	public string[] GetValidExts()
	{
		return new string[]
		{
			"xml",
			"png",
			"txt"
		};
	}

	public string[] ExtraTags()
	{
		return new string[0];
	}

	public string GetThumbnail()
	{
		return Path.Combine(this.FolderPath(), "Thumbnail.png");
	}

	public override string ToString()
	{
		return this.ItemTitle;
	}

	public BuildingPrefab.RoomObject[] Rooms;

	public SVector3[] Edges;

	public string Root;

	private bool _canUpload = true;

	private bool _setTitle = true;

	[CompilerGenerated]
	private static Func<Furniture, BuildingPrefab.FurnitureObject> <>f__mg$cache0;

	[CompilerGenerated]
	private static Func<RoomSegment, BuildingPrefab.SegmentObject> <>f__mg$cache1;

	public class SegmentObject
	{
		public SegmentObject(string name, SVector3 position, float width, bool reversed)
		{
			this.Name = name;
			this.Position = position;
			this.Width = width;
			this.Reversed = reversed;
		}

		public XMLParser.XMLNode ToXmlNode()
		{
			XMLParser.XMLNode xmlnode = new XMLParser.XMLNode("Segment", new XMLParser.XMLNode[]
			{
				new XMLParser.XMLNode("Name", this.Name, null),
				new XMLParser.XMLNode("Position", this.Position.Serialize(), null),
				new XMLParser.XMLNode("Width", this.Width.ToString(), null)
			});
			if (this.Reversed)
			{
				xmlnode.Children.Add(new XMLParser.XMLNode("Reversed", this.Reversed.ToString(), null));
			}
			return xmlnode;
		}

		public static BuildingPrefab.SegmentObject FromXMLNode(XMLParser.XMLNode node)
		{
			string nodeValue = node.GetNodeValue("Name", null);
			SVector3 position = SVector3.Deserialize(node.GetNodeValue("Position", null), false);
			float width = (float)Convert.ToDouble(node.GetNodeValue("Width", null));
			bool reversed = Convert.ToBoolean(node.GetNodeValue("Reversed", false.ToString()));
			return new BuildingPrefab.SegmentObject(nodeValue, position, width, reversed);
		}

		public string Name;

		public SVector3 Position;

		public float Width;

		public bool Reversed;
	}

	public class FurnitureObject
	{
		public FurnitureObject(string name, uint id, SVector3 position, SVector3 rotation, float rotationOffset, uint parent, int snapId, SVector3[] colors)
		{
			this.Name = name;
			this.ID = id;
			this.Position = position;
			this.Rotation = rotation;
			this.RotationOffset = rotationOffset;
			this.Parent = parent;
			this.SnapID = snapId;
			this.Colors = colors;
		}

		public XMLParser.XMLNode ToXmlNode()
		{
			return new XMLParser.XMLNode("Furniture", new XMLParser.XMLNode[]
			{
				new XMLParser.XMLNode("Name", this.Name, null),
				new XMLParser.XMLNode("ID", this.ID.ToString(), null),
				new XMLParser.XMLNode("Position", this.Position.Serialize(), null),
				new XMLParser.XMLNode("Rotation", this.Rotation.Serialize(), null),
				new XMLParser.XMLNode("RotationOffset", this.RotationOffset.ToString(), null),
				new XMLParser.XMLNode("Parent", this.Parent.ToString(), null),
				new XMLParser.XMLNode("SnapID", this.SnapID.ToString(), null),
				new XMLParser.XMLNode("Color1", this.Colors[0].Serialize(), null),
				new XMLParser.XMLNode("Color2", this.Colors[1].Serialize(), null),
				new XMLParser.XMLNode("Color3", this.Colors[2].Serialize(), null)
			});
		}

		public static BuildingPrefab.FurnitureObject FromXMLNode(XMLParser.XMLNode node)
		{
			string nodeValue = node.GetNodeValue("Name", null);
			uint id = Convert.ToUInt32(node.GetNodeValue("ID", null));
			SVector3 position = SVector3.Deserialize(node.GetNodeValue("Position", null), false);
			SVector3 rotation = SVector3.Deserialize(node.GetNodeValue("Rotation", null), false);
			float rotationOffset = (float)Convert.ToDouble(node.GetNodeValue("RotationOffset", null));
			uint parent = Convert.ToUInt32(node.GetNodeValue("Parent", null));
			int snapId = Convert.ToInt32(node.GetNodeValue("SnapID", null));
			SVector3 svector = SVector3.Deserialize(node.GetNodeValue("Color1", null), false);
			SVector3 svector2 = SVector3.Deserialize(node.GetNodeValue("Color2", null), false);
			SVector3 svector3 = SVector3.Deserialize(node.GetNodeValue("Color3", null), false);
			return new BuildingPrefab.FurnitureObject(nodeValue, id, position, rotation, rotationOffset, parent, snapId, new SVector3[]
			{
				svector,
				svector2,
				svector3
			});
		}

		public string Name;

		public uint ID;

		public SVector3 Position;

		public SVector3 Rotation;

		public float RotationOffset;

		public uint Parent;

		public int SnapID;

		public SVector3[] Colors;
	}

	public class RoomObject
	{
		public RoomObject(string[] materials, SVector3[] colors, bool outdoor, int floor, int[] edges, BuildingPrefab.FurnitureObject[] furniture, BuildingPrefab.SegmentObject[] segments, float area, WriteDictionary roomData = null, uint groupID = 0u, bool rentable = true)
		{
			this.Materials = materials;
			this.Colors = colors;
			this.Outdoor = outdoor;
			this.Floor = floor;
			this.Edges = edges;
			this.Furniture = furniture;
			this.Segments = segments;
			this.Area = area;
			this.RoomData = roomData;
			this.RoomGroupID = groupID;
			this.Rentable = rentable;
		}

		public XMLParser.XMLNode ToXmlNode()
		{
			string text = "Room";
			XMLParser.XMLNode[] array = new XMLParser.XMLNode[13];
			array[0] = new XMLParser.XMLNode("Outdoor", this.Outdoor.ToString(), null);
			array[1] = new XMLParser.XMLNode("Floor", this.Floor.ToString(), null);
			array[2] = new XMLParser.XMLNode("Edges", this.Edges.SelectInPlace((int x) => new XMLParser.XMLNode("Edge", x.ToString(), null)));
			array[3] = new XMLParser.XMLNode("Color1", this.Colors[0].Serialize(), null);
			array[4] = new XMLParser.XMLNode("Color2", this.Colors[1].Serialize(), null);
			array[5] = new XMLParser.XMLNode("Color3", this.Colors[2].Serialize(), null);
			array[6] = new XMLParser.XMLNode("Material1", this.Materials[0], null);
			array[7] = new XMLParser.XMLNode("Material2", this.Materials[1], null);
			array[8] = new XMLParser.XMLNode("Material3", this.Materials[2], null);
			array[9] = new XMLParser.XMLNode("Material4", this.Materials[3], null);
			array[10] = new XMLParser.XMLNode("Furniture", this.Furniture.SelectInPlace((BuildingPrefab.FurnitureObject x) => x.ToXmlNode()));
			array[11] = new XMLParser.XMLNode("Segments", this.Segments.SelectInPlace((BuildingPrefab.SegmentObject x) => x.ToXmlNode()));
			array[12] = new XMLParser.XMLNode("Area", this.Area.ToString(), null);
			return new XMLParser.XMLNode(text, array);
		}

		public static BuildingPrefab.RoomObject FromXMLNode(XMLParser.XMLNode node, uint id)
		{
			bool outdoor = Convert.ToBoolean(node.GetNodeValue("Outdoor", null));
			int floor = Convert.ToInt32(node.GetNodeValue("Floor", null));
			int[] edges = (from x in node.GetNode("Edges", true).GetNodes("Edge", true)
			select Convert.ToInt32(x.Value)).ToArray<int>();
			SVector3 svector = SVector3.Deserialize(node.GetNodeValue("Color1", null), false);
			SVector3 svector2 = SVector3.Deserialize(node.GetNodeValue("Color2", null), false);
			SVector3 svector3 = SVector3.Deserialize(node.GetNodeValue("Color3", null), false);
			string nodeValue = node.GetNodeValue("Material1", null);
			string nodeValue2 = node.GetNodeValue("Material2", null);
			string nodeValue3 = node.GetNodeValue("Material3", null);
			string nodeValue4 = node.GetNodeValue("Material4", null);
			IList<XMLParser.XMLNode> nodes = node.GetNode("Furniture", true).GetNodes("Furniture", false);
			if (BuildingPrefab.RoomObject.<>f__mg$cache0 == null)
			{
				BuildingPrefab.RoomObject.<>f__mg$cache0 = new Func<XMLParser.XMLNode, BuildingPrefab.FurnitureObject>(BuildingPrefab.FurnitureObject.FromXMLNode);
			}
			BuildingPrefab.FurnitureObject[] furniture = nodes.SelectInPlace(BuildingPrefab.RoomObject.<>f__mg$cache0);
			IList<XMLParser.XMLNode> nodes2 = node.GetNode("Segments", true).GetNodes("Segment", false);
			if (BuildingPrefab.RoomObject.<>f__mg$cache1 == null)
			{
				BuildingPrefab.RoomObject.<>f__mg$cache1 = new Func<XMLParser.XMLNode, BuildingPrefab.SegmentObject>(BuildingPrefab.SegmentObject.FromXMLNode);
			}
			BuildingPrefab.SegmentObject[] segments = nodes2.SelectInPlace(BuildingPrefab.RoomObject.<>f__mg$cache1);
			float area = (float)Convert.ToDouble(node.GetNodeValue("Area", null));
			return new BuildingPrefab.RoomObject(new string[]
			{
				nodeValue,
				nodeValue2,
				nodeValue3,
				nodeValue4
			}, new SVector3[]
			{
				svector,
				svector2,
				svector3
			}, outdoor, floor, edges, furniture, segments, area, null, id, true);
		}

		public string[] Materials;

		public SVector3[] Colors;

		public bool Outdoor;

		public int Floor;

		public int[] Edges;

		public BuildingPrefab.FurnitureObject[] Furniture;

		public BuildingPrefab.SegmentObject[] Segments;

		public float Area;

		public WriteDictionary RoomData;

		public uint RoomGroupID;

		public bool Rentable;

		[CompilerGenerated]
		private static Func<XMLParser.XMLNode, BuildingPrefab.FurnitureObject> <>f__mg$cache0;

		[CompilerGenerated]
		private static Func<XMLParser.XMLNode, BuildingPrefab.SegmentObject> <>f__mg$cache1;
	}
}
