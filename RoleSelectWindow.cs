﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RoleSelectWindow : MonoBehaviour
{
	public void Show(IEnumerable<Actor> actors)
	{
		this._actors = actors.ToList<Actor>();
		Toggle toggle = this.RoleToggles[0];
		bool isOn;
		if ((from x in this._actors
		select x.Team).Distinct<string>().Count<string>() == this._actors.Count)
		{
			isOn = this._actors.All((Actor x) => x.employee.IsRole(Employee.RoleBit.Lead));
		}
		else
		{
			isOn = false;
		}
		toggle.isOn = isOn;
		for (int i = 1; i < this.RoleToggles.Length; i++)
		{
			int i1 = i;
			this.RoleToggles[i].isOn = this._actors.Any((Actor x) => x.employee.IsRoleIndex(i1));
		}
		this.UpdateAnyRole();
		this.Window.Show();
	}

	public void SetRole(int i)
	{
		foreach (Actor actor in this._actors)
		{
			if (actor != null)
			{
				actor.ChangeRole((Employee.RoleBit)Employee.RoleToBit[i]);
			}
		}
		this.Window.Close();
	}

	public void UpdateAnyRole()
	{
		if (!this._isChangingAnyRole)
		{
			this._isChangingAnyRole = true;
			this.AnyRole.isOn = this.RoleToggles.Skip(1).All((Toggle x) => x.isOn);
			this._isChangingAnyRole = false;
		}
	}

	public void AutoRole()
	{
		foreach (Actor actor in this._actors)
		{
			if (actor != null)
			{
				Employee.RoleBit roleBit = actor.employee.BestRoles();
				if (actor.employee.IsRole(Employee.RoleBit.Lead))
				{
					roleBit |= Employee.RoleBit.Lead;
				}
				actor.ChangeRole(roleBit);
			}
		}
		this.Window.Close();
	}

	public void AnyRoleChanged()
	{
		if (!this._isChangingAnyRole)
		{
			this._isChangingAnyRole = true;
			for (int i = 1; i < this.RoleToggles.Length; i++)
			{
				this.RoleToggles[i].isOn = this.AnyRole.isOn;
			}
			this._isChangingAnyRole = false;
		}
	}

	public void Apply()
	{
		int num = 0;
		for (int i = this.RoleToggles.Length - 1; i >= 0; i--)
		{
			num <<= 1;
			if (this.RoleToggles[i].isOn)
			{
				num |= 1;
			}
		}
		Employee.RoleBit roles = (Employee.RoleBit)num;
		foreach (Actor actor in this._actors)
		{
			if (actor != null)
			{
				actor.ChangeRole(roles);
			}
		}
		this.Window.Close();
	}

	public Toggle[] RoleToggles;

	public Toggle AnyRole;

	public GUIWindow Window;

	private List<Actor> _actors;

	private bool _isChangingAnyRole;
}
