﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UMLMiniGame : Graphic, IPointerUpHandler, IPointerDownHandler, IEventSystemHandler
{
	protected override void OnPopulateMesh(VertexHelper h)
	{
		h.Clear();
		foreach (KeyValuePair<UMLObject, UMLObject> keyValuePair in this.Connections)
		{
			UMLMiniGame.DrawArrow(keyValuePair.Key.Rect.anchoredPosition, this.BoundToBox(keyValuePair.Key.Rect.anchoredPosition, new Rect(keyValuePair.Value.Rect.anchoredPosition - keyValuePair.Value.Rect.sizeDelta / 2f, keyValuePair.Value.Rect.sizeDelta)), h);
		}
		if (this.DraggingFrom != null)
		{
			Vector2 zero = Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(base.rectTransform, Input.mousePosition, null, out zero);
			UMLMiniGame.DrawArrow(this.DraggingFrom.Rect.anchoredPosition, zero, h);
		}
	}

	public Vector2 BoundToBox(Vector2 p, Rect r)
	{
		float d = Mathf.Sqrt(r.width * r.width + r.height * r.height);
		Vector2 vector = (p - r.center).normalized * d + r.center;
		return new Vector2(Mathf.Clamp(vector.x, r.xMin, r.xMax), Mathf.Clamp(vector.y, r.yMin, r.yMax));
	}

	public bool MakeConnection(UMLObject obj1, UMLObject obj2)
	{
		if (obj1 == obj2)
		{
			return false;
		}
		if (this.Connections.Contains(new KeyValuePair<UMLObject, UMLObject>(obj1, obj2)) || this.Connections.Contains(new KeyValuePair<UMLObject, UMLObject>(obj2, obj1)))
		{
			return false;
		}
		if (obj1.Num > 0 && obj2.Num > 0)
		{
			obj1.Num--;
			obj1.Label.text = obj1.Num.ToString();
			obj2.Num--;
			obj2.Label.text = obj2.Num.ToString();
			this.Connections.Add(new KeyValuePair<UMLObject, UMLObject>(obj1, obj2));
			if (obj1.Num == 0 && obj2.Num == 0)
			{
				HashSet<UMLObject> hashSet = this.CheckDeath(obj1, new HashSet<UMLObject>());
				if (hashSet != null)
				{
					int num = 0;
					foreach (UMLObject umlobject in hashSet)
					{
						num += umlobject.OriginalNum;
						UMLObject local = umlobject;
						this.Connections.RemoveAll((KeyValuePair<UMLObject, UMLObject> x) => x.Key == local || x.Value == local);
						this.UMLs.Remove(umlobject);
						umlobject.DestroyMe();
					}
				}
			}
		}
		return true;
	}

	public IEnumerable<UMLObject> GetAllConnectionsFor(UMLObject uml)
	{
		foreach (KeyValuePair<UMLObject, UMLObject> item in this.Connections)
		{
			if (item.Key == uml)
			{
				yield return item.Value;
			}
			if (item.Value == uml)
			{
				yield return item.Key;
			}
		}
		yield break;
	}

	public HashSet<UMLObject> CheckDeath(UMLObject init, HashSet<UMLObject> result)
	{
		if (init.Num > 0)
		{
			return null;
		}
		result.Add(init);
		foreach (UMLObject umlobject in this.GetAllConnectionsFor(init))
		{
			if (!result.Contains(umlobject) && this.CheckDeath(umlobject, result) == null)
			{
				return null;
			}
		}
		return result;
	}

	public static void DrawArrow(Vector2 p1, Vector2 p2, VertexHelper h)
	{
		UMLMiniGame.DrawLine(p1, p2, 2f, Color.black, h);
		float rot = Mathf.Atan2(p2.y - p1.y, p2.x - p1.x) * 57.29578f - 90f;
		int currentVertCount = h.currentVertCount;
		h.AddVert(UMLMiniGame.TransformVec(new Vector2(0f, 0f), rot, p2), Color.black, Vector2.zero);
		h.AddVert(UMLMiniGame.TransformVec(new Vector2(15f, -30f), rot, p2), Color.black, Vector2.zero);
		h.AddVert(UMLMiniGame.TransformVec(new Vector2(-15f, -30f), rot, p2), Color.black, Vector2.zero);
		h.AddTriangle(currentVertCount, currentVertCount + 1, currentVertCount + 2);
		currentVertCount = h.currentVertCount;
		h.AddVert(UMLMiniGame.TransformVec(new Vector2(0f, -6f), rot, p2), Color.white, Vector2.zero);
		h.AddVert(UMLMiniGame.TransformVec(new Vector2(11f, -28f), rot, p2), Color.white, Vector2.zero);
		h.AddVert(UMLMiniGame.TransformVec(new Vector2(-11f, -28f), rot, p2), Color.white, Vector2.zero);
		h.AddTriangle(currentVertCount, currentVertCount + 1, currentVertCount + 2);
	}

	public static void DrawArrow(Vector2 p1, Vector2 p2, VertexHelper h, Color col, float headSize)
	{
		UMLMiniGame.DrawLine(p1, p2, 2f, col, h);
		float rot = Mathf.Atan2(p2.y - p1.y, p2.x - p1.x) * 57.29578f - 90f;
		int currentVertCount = h.currentVertCount;
		h.AddVert(UMLMiniGame.TransformVec(new Vector2(0f, 0f), rot, p2), col, Vector2.zero);
		h.AddVert(UMLMiniGame.TransformVec(new Vector2(headSize / 2f, -headSize), rot, p2), col, Vector2.zero);
		h.AddVert(UMLMiniGame.TransformVec(new Vector2(-headSize / 2f, -headSize), rot, p2), col, Vector2.zero);
		h.AddTriangle(currentVertCount, currentVertCount + 1, currentVertCount + 2);
	}

	public static Vector2 TransformVec(Vector2 v, float rot, Vector2 g)
	{
		Vector3 vector = Quaternion.Euler(0f, 0f, rot) * v;
		return new Vector2(vector.x + g.x, vector.y + g.y);
	}

	public static void DrawLine(Vector2 p1, Vector2 p2, float width, Color color, VertexHelper h)
	{
		Vector2 normalized = new Vector2(p1.y - p2.y, p2.x - p1.x);
		normalized = normalized.normalized;
		h.AddUIVertexQuad(new UIVertex[]
		{
			UMLMiniGame.MakeVert(new Vector2(p1.x + normalized.x * width / 2f, p1.y + normalized.y * width / 2f), color),
			UMLMiniGame.MakeVert(new Vector2(p2.x + normalized.x * width / 2f, p2.y + normalized.y * width / 2f), color),
			UMLMiniGame.MakeVert(new Vector2(p2.x - normalized.x * width / 2f, p2.y - normalized.y * width / 2f), color),
			UMLMiniGame.MakeVert(new Vector2(p1.x - normalized.x * width / 2f, p1.y - normalized.y * width / 2f), color)
		});
	}

	public static UIVertex MakeVert(Vector2 p, Color color)
	{
		UIVertex simpleVert = UIVertex.simpleVert;
		simpleVert.position = p;
		simpleVert.color = color;
		return simpleVert;
	}

	public void Show(DesignDocument work)
	{
		this.Work = work;
		WindowManager.HasModal = true;
		InputController.InputEnabled = false;
		this.PauseGame();
		this.MainCanvas.SetActive(true);
		this.InfoText.text = "Lives left".Loc() + ": " + this.Work.MiniGameLives;
	}

	public void Hide()
	{
		WindowManager.HasModal = (WindowManager.ModalWindows.Count > 0);
		InputController.InputEnabled = true;
		this.ClearBoard();
		GameSettings.GameSpeed = 0f;
		this.MainCanvas.SetActive(false);
	}

	public void PlayGame()
	{
		HUD.Instance.GameSpeed = 2;
		this.PauseButton.SetActive(false);
	}

	public void PauseGame()
	{
		this.DraggingFrom = null;
		HUD.Instance.GameSpeed = 0;
		this.PauseButton.SetActive(true);
	}

	public void ClearBoard()
	{
		for (int i = 0; i < this.UMLs.Count; i++)
		{
			UnityEngine.Object.Destroy(this.UMLs[i].gameObject);
		}
		this.UMLs.Clear();
		this.Connections.Clear();
	}

	private void Update()
	{
		if (!Application.isPlaying)
		{
			return;
		}
		if (Input.GetKeyUp(KeyCode.Escape) || GameSettings.ForcePause || this.Work == null || this.Work.GetProgress() >= 0.5f || this.Work.MiniGameLives <= 0)
		{
			this.Hide();
			return;
		}
		float progress = this.Work.GetProgress();
		this.WorkProgress.Value = 1f - Mathf.Abs(progress * 2f - 1f);
		if (InputController.GetKeyDown(InputController.Keys.Pause, true))
		{
			this.PauseGame();
			this.SetVerticesDirty();
		}
		if (GameSettings.GameSpeed == 0f && (InputController.GetKeyDown(InputController.Keys.Speed1, true) || InputController.GetKeyDown(InputController.Keys.Speed2, true) || InputController.GetKeyDown(InputController.Keys.Speed3, true)))
		{
			this.PlayGame();
			this.SetVerticesDirty();
		}
		if (GameSettings.GameSpeed == 0f)
		{
			return;
		}
		this.InfoText.text = "Lives left".Loc() + ": " + this.Work.MiniGameLives;
		Vector2 vector = new Vector2(base.rectTransform.rect.width / 2f, -base.rectTransform.rect.height / 2f);
		for (int i = 0; i < this.UMLs.Count; i++)
		{
			UMLObject umlobject = this.UMLs[i];
			Vector2 normalized = (vector - umlobject.Rect.anchoredPosition).normalized;
			umlobject.Rect.anchoredPosition += normalized * Time.deltaTime * (this.Speed - this.NegativeSpeed);
			if ((umlobject.Rect.anchoredPosition - vector).magnitude < 16f)
			{
				foreach (KeyValuePair<UMLObject, UMLObject> item in this.Connections.ToList<KeyValuePair<UMLObject, UMLObject>>())
				{
					if (item.Key == umlobject)
					{
						item.Value.Num++;
						item.Value.Label.text = item.Value.Num.ToString();
						this.Connections.Remove(item);
					}
					if (item.Value == umlobject)
					{
						item.Key.Num++;
						item.Key.Label.text = item.Key.Num.ToString();
						this.Connections.Remove(item);
					}
				}
				this.UMLs.RemoveAt(i);
				umlobject.DestroyMe();
				this.Work.MiniGameLives--;
				this.ClearBoard();
				this.DraggingFrom = null;
				break;
			}
		}
		this.nextSpawn -= Time.deltaTime * (1f + this.Work.GetWorkScore());
		if (this.NegativeSpeed > 0f)
		{
			this.NegativeSpeed = Mathf.Max(0f, this.NegativeSpeed - Time.deltaTime * this.Speed);
		}
		if (this.nextSpawn <= 0f)
		{
			this.nextSpawn = this.SpawnTime;
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.UMLPrefab);
			gameObject.transform.SetParent(base.transform);
			bool flag = this.side < 2;
			bool flag2 = this.side % 2 == 0;
			gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2((!flag) ? ((!flag2) ? base.rectTransform.rect.width : 0f) : UnityEngine.Random.Range(0f, base.rectTransform.rect.width), (!flag) ? UnityEngine.Random.Range(0f, -base.rectTransform.rect.height) : ((!flag2) ? (-base.rectTransform.rect.height) : 0f));
			UMLObject component = gameObject.GetComponent<UMLObject>();
			this.UMLs.Add(component);
			this.KeepFromCollision(component);
			this.side = (this.side + 1) % 4;
		}
		this.SetVerticesDirty();
	}

	public void KeepFromCollision(UMLObject obj)
	{
		Rect rect = obj.GetRect();
		foreach (UMLObject umlobject in this.UMLs)
		{
			Rect rect2 = umlobject.GetRect();
			if (rect2.Overlaps(rect))
			{
				Vector2 b = (rect.center - rect2.center).normalized * (rect.Diameter() / 2f + rect2.Diameter() / 2f);
				obj.Rect.anchoredPosition += b;
				rect = obj.GetRect();
			}
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (GameSettings.GameSpeed == 0f)
		{
			return;
		}
		if (this.DraggingFrom != null)
		{
			for (int i = 0; i < this.UMLs.Count; i++)
			{
				if (RectTransformUtility.RectangleContainsScreenPoint(this.UMLs[i].Rect, eventData.position) && this.MakeConnection(this.DraggingFrom, this.UMLs[i]))
				{
					this.DraggingFrom = null;
					return;
				}
			}
		}
		this.DraggingFrom = null;
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (GameSettings.GameSpeed == 0f)
		{
			return;
		}
		for (int i = 0; i < this.UMLs.Count; i++)
		{
			if (this.UMLs[i].Num > 0 && RectTransformUtility.RectangleContainsScreenPoint(this.UMLs[i].Rect, eventData.position))
			{
				this.DraggingFrom = this.UMLs[i];
				return;
			}
		}
		this.DraggingFrom = null;
	}

	public GameObject UMLPrefab;

	[NonSerialized]
	public List<UMLObject> UMLs = new List<UMLObject>();

	public HashSet<KeyValuePair<UMLObject, UMLObject>> Connections = new HashSet<KeyValuePair<UMLObject, UMLObject>>();

	public float SpawnTime;

	public float Speed;

	[NonSerialized]
	public float NegativeSpeed;

	private float nextSpawn;

	[NonSerialized]
	public UMLObject DraggingFrom;

	[NonSerialized]
	public int side;

	public GameObject MainCanvas;

	[NonSerialized]
	public DesignDocument Work;

	public GUIProgressBar WorkProgress;

	public Text InfoText;

	public GameObject PauseButton;
}
