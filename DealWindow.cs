﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DealWindow : MonoBehaviour
{
	public void Toggle()
	{
		this.SelectedTeams.Clear();
		Deal[] selected = this.NewDealList.GetSelected<Deal>();
		if (selected.Length == 1 && selected[0] is WorkDeal)
		{
			this.SelectedTeams.Clear();
			this.SelectedTeams.AddRange(GameSettings.Instance.GetDefaultTeams("Deals" + selected[0].Title()));
		}
		else
		{
			this.SelectedTeams.Clear();
			this.SelectedTeams.AddRange(GameSettings.Instance.GetDefaultTeams("Deals"));
		}
		this.UpdateTeamText();
		this.TeamText.text = Utilities.GetTeamDescription(this.SelectedTeams);
		this.newDealsCount = 0;
		this.UpdateDealIcon();
		this.Window.Toggle(false);
	}

	public void SelectTeams()
	{
		HUD.Instance.TeamSelectWindow.Show(false, this.SelectedTeams, delegate(string[] t)
		{
			this.SelectedTeams.Clear();
			this.SelectedTeams.AddRange(t);
			this.UpdateTeamText();
			Deal[] selected = this.NewDealList.GetSelected<Deal>();
			if (selected.Length == 1 && selected[0] is WorkDeal)
			{
				GameSettings.Instance.TeamDefaults["Deals" + selected[0].Title()] = t.ToHashSet<string>();
			}
		}, null);
	}

	private void UpdateTeamText()
	{
		this.TeamText.text = Utilities.GetTeamDescription(this.SelectedTeams);
	}

	public void Serialize(WriteDictionary dict)
	{
		dict["AllDeals"] = this.AllDeals;
		dict["NewDeals"] = (from x in this.NewDeals
		select x.ID).ToArray<uint>();
		dict["ActiveDeals"] = (from x in this.ActiveDeals.Items
		select (x as Deal).ID).ToArray<uint>();
		dict["Blacklist"] = this.BlackList.ToArray<string>();
	}

	public bool IsInDeal(SimulatedCompany.ProductPrototype proto, Company company)
	{
		return this.NewDeals.OfType<WorkDeal>().Any((WorkDeal x) => x.Incoming && x.Client != null && x.Client == company && proto == x.Prototype) || this.ActiveDeals.Items.OfType<WorkDeal>().Any((WorkDeal x) => x.Incoming && x.Client != null && x.Client == company && proto == x.Prototype);
	}

	public void CancelDueWork()
	{
		foreach (Deal deal in this.NewDeals.ToList<Deal>())
		{
			if (!deal.StillValid(false))
			{
				this.CancelDeal(deal, true);
			}
		}
		foreach (Deal deal2 in this.ActiveDeals.Items.OfType<Deal>().ToList<Deal>())
		{
			if (!deal2.StillValid(true))
			{
				this.CancelDeal(deal2, true);
			}
		}
	}

	private void UpdateDealIcon()
	{
		this.DealCounter.SetNumber(this.newDealsCount);
	}

	public List<Deal> GetActiveDeals()
	{
		return this.ActiveDeals.Items.OfType<Deal>().ToList<Deal>();
	}

	public List<object> GetActiveDealsPerformance()
	{
		return this.ActiveDeals.Items;
	}

	public SDateTime? FindReceptionTime()
	{
		Actor random = (from x in GameSettings.Instance.sActorManager.Staff
		where x.AItype == AI<Actor>.AIType.Receptionist
		select x).GetRandom<Actor>();
		if (random != null)
		{
			int num = random.StaffOn + UnityEngine.Random.Range(0, 3);
			num %= 24;
			SDateTime sdateTime = new SDateTime(UnityEngine.Random.Range(0, 60), num, TimeOfDay.Instance.Day, TimeOfDay.Instance.Month, TimeOfDay.Instance.Year);
			if (sdateTime < SDateTime.Now())
			{
				sdateTime += new SDateTime(1, 0, 0);
			}
			return new SDateTime?(sdateTime);
		}
		return null;
	}

	public void InsertDeal(Deal deal)
	{
		if (deal.StillValid(false))
		{
			this.AllDeals[deal.ID] = deal;
			if (deal is ServerDeal)
			{
				(deal as ServerDeal).Product.ExternalHosting = deal.ID;
			}
			this.NewDeals.Add(deal);
			if (!this.Window.Shown && !this.BlackList.Contains(deal.Title()))
			{
				this.newDealsCount++;
			}
			this.UpdateDealIcon();
		}
	}

	public void AddDeal(Deal deal)
	{
		SDateTime? sdateTime = this.FindReceptionTime();
		if (sdateTime != null)
		{
			Actor actor = GameSettings.Instance.SpawnActor(UnityEngine.Random.value > 0.5f);
			actor.employee = new Employee(SDateTime.Now(), Employee.EmployeeRole.Lead, UnityEngine.Random.value > 0.5f, Employee.WageBracket.High, GameSettings.Instance.Personalities, false, null, null, null, 1f, 0.1f);
			actor.employee.Salary = 20000f;
			actor.AItype = AI<Actor>.AIType.Guest;
			actor.deal = deal;
			GameSettings.Instance.sActorManager.AddToAwaiting(actor, sdateTime.Value, true, true);
		}
	}

	public void GenerateBid()
	{
		if (GameSettings.Instance.MyCompany.Products.Count > 0 && GameSettings.Instance.simulation.Companies.Count > 0)
		{
			SDateTime t = SDateTime.Now();
			SimulatedCompany simulatedCompany = (from x in GameSettings.Instance.simulation.Companies.Values
			where !x.IsPlayerOwned()
			select x).MaxInstance((SimulatedCompany x) => x.Money * Utilities.RandomRange(0.8f, 1f));
			if (simulatedCompany != null)
			{
				KeyValuePair<SoftwareProduct, float> keyValuePair = (from x in GameSettings.Instance.MyCompany.Products
				select new KeyValuePair<SoftwareProduct, float>(x, x.GetQuality(t, false))).MaxInstance((KeyValuePair<SoftwareProduct, float> x) => x.Value);
				if (keyValuePair.Value > 0.25f)
				{
					IPDeal ipdeal = this.BidExists(simulatedCompany, keyValuePair.Key);
					if (ipdeal != null)
					{
						this.CancelDeal(ipdeal, true);
					}
					IPDeal ipdeal2 = new IPDeal(keyValuePair.Key, simulatedCompany, t);
					if (ipdeal2.Worth() < simulatedCompany.Money * 0.25f && UnityEngine.Random.value < GameSettings.Instance.MyCompany.BusinessReputation)
					{
						this.AddDeal(ipdeal2);
					}
				}
			}
		}
	}

	private IPDeal BidExists(Company company, SoftwareProduct product)
	{
		return this.NewDeals.OfType<IPDeal>().FirstOrDefault((IPDeal x) => x.Request && x.Bidder == company && x._products.Contains(product.ID));
	}

	public void CancelBids(SoftwareProduct product)
	{
		foreach (IPDeal ipdeal in this.NewDeals.OfType<IPDeal>().ToList<IPDeal>())
		{
			if (ipdeal._products.Contains(product.ID))
			{
				this.NewDeals.Remove(ipdeal);
			}
		}
	}

	public void CancelWorkDeal(SimulatedCompany.ProductPrototype proto, Company company)
	{
		foreach (WorkDeal workDeal in (from x in this.NewDeals.OfType<WorkDeal>()
		where x.Client == company
		select x).ToList<WorkDeal>())
		{
			if (workDeal.Prototype == proto)
			{
				this.NewDeals.Remove(workDeal);
			}
		}
	}

	public void CancelCompanyDeals(Company company)
	{
		foreach (Deal deal in this.NewDeals.ToList<Deal>())
		{
			if (deal is IPDeal && deal.Request && deal.Bidder == company)
			{
				this.NewDeals.Remove(deal);
			}
			else if (deal is WorkDeal && deal.Client == company)
			{
				this.NewDeals.Remove(deal);
			}
		}
		foreach (Deal deal2 in this.ActiveDeals.Items.OfType<Deal>().ToList<Deal>())
		{
			if (deal2 is WorkDeal && deal2.Client == company)
			{
				this.CancelDeal(deal2, false);
			}
		}
	}

	public void CancelProductDeals(SoftwareProduct p)
	{
		foreach (WorkDeal workDeal in this.NewDeals.OfType<WorkDeal>().ToList<WorkDeal>())
		{
			if (workDeal.Product == p)
			{
				this.NewDeals.Remove(workDeal);
			}
		}
		foreach (WorkDeal workDeal2 in this.ActiveDeals.Items.OfType<WorkDeal>().ToList<WorkDeal>())
		{
			if (workDeal2.Product == p)
			{
				this.CancelDeal(workDeal2, false);
			}
		}
	}

	public void CancelDeal(Deal deal, bool repercussion = true)
	{
		if (deal == null)
		{
			return;
		}
		if (deal.Active && repercussion)
		{
			float num = deal.Payout();
			if (num > 0f && deal.Company != null && deal.Client != null)
			{
				string bill = deal.Title();
				deal.Company.MakeTransaction(num, Company.TransactionCategory.Deals, bill);
				deal.Client.MakeTransaction(-num, Company.TransactionCategory.Deals, bill);
			}
			if (deal.Company != null)
			{
				deal.Company.ChangeBusinessRep(deal.ReputationEffect(true), deal.ReputationCategory(), 1f);
			}
		}
		deal.Cancel();
		this.NewDeals.Remove(deal);
		this.ActiveDeals.Items.Remove(deal);
	}

	public void AcceptDeal()
	{
		Deal[] selected = this.NewDealList.GetSelected<Deal>();
		if (selected.Length > 0)
		{
			bool flag = false;
			List<Team> list = this.SelectedTeams.SelectNotNull((string x) => GameSettings.Instance.sActorManager.Teams.GetOrNull(x)).ToList<Team>();
			foreach (Deal deal in selected)
			{
				if (!deal.StillValid(false))
				{
					this.CancelDeal(deal, false);
					return;
				}
				deal.Accept(GameSettings.Instance.MyCompany);
				if (deal is ServerDeal && this.Combo.SelectedItem != null)
				{
					GameSettings.Instance.RegisterWithServer(this.Combo.SelectedItemString, deal as ServerDeal, true);
				}
				WorkDeal workDeal = deal as WorkDeal;
				if (workDeal != null)
				{
					flag = true;
					for (int j = 0; j < list.Count; j++)
					{
						workDeal.WorkItem.AddDevTeam(list[j], false);
					}
				}
				this.NewDeals.Remove(deal);
				if (!deal.CancelOnAccept())
				{
					this.ActiveDeals.Items.Add(deal);
				}
			}
			if (flag)
			{
				GameSettings.Instance.TeamDefaults["Deals"] = (from x in list
				select x.Name).ToHashSet<string>();
			}
		}
	}

	public void RejectDeal()
	{
		Deal[] sel = this.NewDealList.GetSelected<Deal>();
		if (sel.Length > 0)
		{
			WindowManager.Instance.ShowMessageBox("RejectConfirmation".Loc(), true, DialogWindow.DialogType.Question, delegate
			{
				Deal[] sel;
				foreach (Deal deal in sel)
				{
					this.CancelDeal(deal, false);
				}
			}, "Reject deal", null);
		}
	}

	private void UpdateNewDeals()
	{
		this.NewDealList.Items = (from x in this.NewDeals
		where !this.BlackList.Contains(x.Title())
		select x).Cast<object>().ToList<object>();
		this.UpdateDesc();
	}

	private void InitNewDeals()
	{
		this.NewDeals.OnChange = delegate
		{
			this.UpdateNewDeals();
		};
	}

	private void Awake()
	{
		this.InitNewDeals();
	}

	private void UpdateDesc()
	{
		Deal[] selected = this.NewDealList.GetSelected<Deal>();
		if (selected.Length == 1)
		{
			this.ActionPanel.SetActive(true);
			this.AcceptButton.SetActive(true);
			this.DetailPanel.SetActive(true);
			Deal deal = selected[0];
			this.Combo.gameObject.SetActive(true);
			this.ComboDesc.gameObject.SetActive(true);
			if (deal is ServerDeal)
			{
				this.ComboDesc.text = "Server".Loc() + ":";
				this.Combo.gameObject.SetActive(true);
				this.TeamButton.SetActive(false);
				this.Combo.UpdateContent<string>(GameSettings.Instance.GetServerNames());
			}
			else if (deal is WorkDeal)
			{
				this.ComboDesc.text = "Team".Loc() + ":";
				this.Combo.gameObject.SetActive(false);
				this.TeamButton.SetActive(true);
				this.SelectedTeams.Clear();
				this.SelectedTeams.AddRange(GameSettings.Instance.GetDefaultTeams("Deals" + deal.Title()));
				this.UpdateTeamText();
			}
			else
			{
				this.Combo.gameObject.SetActive(false);
				this.TeamButton.SetActive(false);
				this.ComboDesc.gameObject.SetActive(false);
			}
			this.DescriptionSheet.SetData(deal.GetDetailedDescriptionVars(), deal.GetDetailedDescriptionValues());
		}
		else
		{
			this.ActionPanel.SetActive(selected.Length > 0);
			this.DetailPanel.SetActive(false);
			this.DescriptionSheet.SetData(new string[0], new string[0]);
		}
	}

	private void Start()
	{
		this.DetailPanel.SetActive(false);
		this.ActionPanel.SetActive(false);
		this.NewDealList.OnSelectChange = delegate(bool d)
		{
			this.UpdateDesc();
		};
		this.ActiveDeals.OnSelectChange = delegate(bool d)
		{
			Deal[] selected = this.ActiveDeals.GetSelected<Deal>();
			if (selected.Length == 1)
			{
				this.AcceptedDescriptionSheet.SetData(selected[0].GetDetailedDescriptionVars(), selected[0].GetDetailedDescriptionValues());
			}
			else
			{
				this.AcceptedDescriptionSheet.SetData(new string[0], new string[0]);
			}
		};
	}

	public void Deserialize(WriteDictionary dict)
	{
		this.InitNewDeals();
		this.AllDeals = dict.Get<Dictionary<uint, Deal>>("AllDeals", new Dictionary<uint, Deal>());
		this.BlackList.AddRange(dict.Get<string[]>("Blacklist", new string[0]));
		this.NewDeals.AddRange(from x in (uint[])dict["NewDeals"]
		select this.AllDeals[x]);
		this.ActiveDeals.Items.AddRange(from x in (uint[])dict["ActiveDeals"]
		select this.AllDeals[x]);
	}

	public void ChangeFilter()
	{
		string[] filters = new string[]
		{
			"Design",
			"Development",
			"Support",
			"Marketing",
			"Hosting",
			"Printing",
			"IntellectualPropertyAbbr"
		};
		SelectorController.Instance.selectWindow.ShowMulti("Filter", filters, (from x in filters
		select !this.BlackList.Contains(x)).ToArray<bool>(), delegate(int[] i)
		{
			this.BlackList.Clear();
			this.BlackList.AddRange(from x in i
			select filters[x]);
			this.UpdateNewDeals();
		}, true, true, true);
	}

	[NonSerialized]
	public HashSet<string> BlackList = new SHashSet<string>();

	public GUIWindow Window;

	[NonSerialized]
	public Dictionary<uint, Deal> AllDeals = new Dictionary<uint, Deal>();

	private EventList<Deal> NewDeals = new EventList<Deal>();

	[SerializeField]
	private GUIListView NewDealList;

	[SerializeField]
	private GUIListView ActiveDeals;

	public Text ComboDesc;

	public VarValueSheet DescriptionSheet;

	public VarValueSheet AcceptedDescriptionSheet;

	public GUICombobox Combo;

	public GameObject TeamButton;

	public GameObject AcceptButton;

	[NonSerialized]
	private HashSet<string> SelectedTeams = new HashSet<string>();

	public GameObject DetailPanel;

	public GameObject ActionPanel;

	public ButtonCounter DealCounter;

	public Text TeamText;

	private int newDealsCount;
}
