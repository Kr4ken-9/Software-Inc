﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AltSerialize;

[Serializable]
public class EventList<T> : IList<T>, IAltSerializable, IEnumerable, ICollection<T>, IEnumerable<T>
{
	public EventList()
	{
	}

	public EventList(List<T> list)
	{
		this._underlying = list;
	}

	public int Count
	{
		get
		{
			return this._underlying.Count;
		}
	}

	public T this[int i]
	{
		get
		{
			return this._underlying[i];
		}
		set
		{
			this._underlying[i] = value;
		}
	}

	public void Add(T item)
	{
		if (this.PreChange != null)
		{
			this.PreChange();
		}
		this._underlying.Add(item);
		if (this.OnChange != null)
		{
			this.OnChange();
		}
	}

	public void AddRange(IEnumerable<T> range)
	{
		if (this.PreChange != null)
		{
			this.PreChange();
		}
		this._underlying.AddRange(range);
		if (this.OnChange != null && range.Any<T>())
		{
			this.OnChange();
		}
	}

	public void Update<Y>(EventList<Y> list)
	{
		if (this.PreChange != null)
		{
			this.PreChange();
		}
		foreach (Y y in list.ToList<Y>())
		{
			T item = (T)((object)y);
			if (!this.Contains(item))
			{
				list.Remove(y);
			}
		}
		foreach (T t in this)
		{
			Y item2 = (Y)((object)t);
			if (!list.Contains(item2))
			{
				list.Add(item2);
			}
		}
		list.OnChange();
	}

	public void SyncContent<T1>(IList<T1> list)
	{
		bool flag = false;
		for (int i = 0; i < this._underlying.Count; i++)
		{
			T t = this._underlying[i];
			bool flag2 = false;
			for (int j = 0; j < list.Count; j++)
			{
				T1 t2 = list[j];
				if (t.Equals(t2))
				{
					flag2 = true;
					break;
				}
			}
			if (!flag2)
			{
				if (!flag && this.PreChange != null)
				{
					flag = true;
					this.PreChange();
				}
				this._underlying.RemoveAt(i);
				i--;
			}
		}
		for (int k = 0; k < list.Count; k++)
		{
			T1 t3 = list[k];
			bool flag3 = false;
			for (int l = 0; l < this._underlying.Count; l++)
			{
				T t4 = this._underlying[l];
				if (t4.Equals(t3))
				{
					flag3 = true;
					break;
				}
			}
			if (!flag3)
			{
				if (!flag && this.PreChange != null)
				{
					flag = true;
					this.PreChange();
				}
				this._underlying.Add((T)((object)t3));
				k--;
			}
		}
		if (flag && this.OnChange != null)
		{
			this.OnChange();
		}
	}

	public void Remove(T item)
	{
		if (this.PreChange != null)
		{
			this.PreChange();
		}
		this._underlying.Remove(item);
		if (this.OnChange != null)
		{
			this.OnChange();
		}
	}

	public int IndexOf(T item)
	{
		return this._underlying.IndexOf(item);
	}

	public void Sort(Comparison<T> comp)
	{
		this._underlying.Sort(comp);
	}

	public void Reverse()
	{
		this._underlying.Reverse();
	}

	public static implicit operator EventList<T>(List<T> list)
	{
		return new EventList<T>(list);
	}

	public static implicit operator List<T>(EventList<T> list)
	{
		return new List<T>(list._underlying);
	}

	public void ForEach(Action<T> a)
	{
		this._underlying.ForEach(a);
	}

	public T FirstOrDefault(Func<T, bool> a)
	{
		return this._underlying.FirstOrDefault(a);
	}

	public void Insert(int index, T item)
	{
		if (this.PreChange != null)
		{
			this.PreChange();
		}
		this._underlying.Insert(index, item);
		if (this.OnChange != null)
		{
			this.OnChange();
		}
	}

	public void RemoveAt(int index)
	{
		if (this.PreChange != null)
		{
			this.PreChange();
		}
		this._underlying.RemoveAt(index);
		if (this.OnChange != null)
		{
			this.OnChange();
		}
	}

	public IEnumerator<T> GetEnumerator()
	{
		foreach (T item in this._underlying)
		{
			yield return item;
		}
		yield break;
	}

	public void Clear()
	{
		if (this.PreChange != null)
		{
			this.PreChange();
		}
		this._underlying.Clear();
		if (this.OnChange != null)
		{
			this.OnChange();
		}
	}

	public bool Contains(T item)
	{
		return this._underlying.Contains(item);
	}

	public void CopyTo(T[] array, int arrayIndex)
	{
		this._underlying.CopyTo(array, arrayIndex);
	}

	public bool IsReadOnly
	{
		get
		{
			return false;
		}
	}

	bool ICollection<T>.Remove(T item)
	{
		if (this.PreChange != null)
		{
			this.PreChange();
		}
		bool result = this._underlying.Remove(item);
		if (this.OnChange != null)
		{
			this.OnChange();
		}
		return result;
	}

	IEnumerator<T> IEnumerable<T>.GetEnumerator()
	{
		foreach (T item in this._underlying)
		{
			yield return item;
		}
		yield break;
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return this._underlying.GetEnumerator();
	}

	public void RemoveAll(Func<T, bool> predicate)
	{
		bool flag = false;
		for (int i = 0; i < this._underlying.Count; i++)
		{
			if (predicate(this._underlying[i]))
			{
				if (!flag)
				{
					if (this.PreChange != null)
					{
						this.PreChange();
					}
					flag = true;
				}
				this.RemoveAt(i);
				i--;
			}
		}
		if (flag && this.OnChange != null)
		{
			this.OnChange();
		}
	}

	public void Serialize(AltSerializer serializer)
	{
		serializer.Serialize(this._underlying);
	}

	public void Deserialize(AltSerializer deserializer)
	{
		this._underlying = (List<T>)deserializer.Deserialize();
	}

	[NonSerialized]
	public Action OnChange;

	[NonSerialized]
	public Action PreChange;

	private List<T> _underlying = new List<T>();
}
