﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class TutorialSystem : MonoBehaviour
{
	private void OnDestroy()
	{
		if (TutorialSystem.Instance == this)
		{
			TutorialSystem.Instance = null;
		}
	}

	private void Start()
	{
		this.Window.Close();
		if (TutorialSystem.Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		TutorialSystem.Instance = this;
		if (TutorialSystem.Tutorials == null)
		{
			this.LoadTutorials();
		}
	}

	private void CheckBacklog()
	{
		if (this.CurrentTutorial == null && this.TutorialBacklog.Count > 0)
		{
			string name = this.TutorialBacklog[0];
			this.TutorialBacklog.RemoveAt(0);
			this.StartTutorial(name, false);
		}
	}

	public static void LoadTutorialFiles(Dictionary<string, TutorialMessage[]> tuts)
	{
		foreach (KeyValuePair<string, string> keyValuePair in GameData.LoadAllTextAssetsWithNameNoSplit("Tutorials"))
		{
			XMLParser.XMLNode xmlnode = XMLParser.ParseXML(keyValuePair.Value);
			string text = xmlnode.TryGetAttribute("Continuation", null);
			if (text != null)
			{
				TutorialSystem.Continuation[keyValuePair.Key] = text;
			}
			tuts.Add(keyValuePair.Key, xmlnode.GetNodes("Message", true).SelectInPlace((XMLParser.XMLNode x) => new TutorialMessage(x)));
		}
	}

	public void LoadTutorials()
	{
		TutorialSystem.Tutorials = new Dictionary<string, TutorialMessage[]>();
		TutorialSystem.LoadTutorialFiles(TutorialSystem.Tutorials);
	}

	private void ShowTut(string name)
	{
		this.Window.rectTransform.anchoredPosition = new Vector2((float)Screen.width / 2f - this.Window.rectTransform.rect.width / 2f, (float)(-(float)Screen.height) / 2f + this.Window.rectTransform.rect.height / 2f);
		this.Window.NonLocTitle = "TutorialPost".Loc(new object[]
		{
			name.Loc()
		});
		this.Window.Show();
		this.UpdateContent(true);
		this.CurrentTutorialName = name;
	}

	public void StartTutorial(string name, bool force = false)
	{
		if (this.CurrentTutorialName != name && (force || (Options.Tutorial && (GameSettings.Instance == null || !GameSettings.Instance.DisabledTutorials.Contains(name)))))
		{
			if (HUD.Instance == null)
			{
				this.CurrentMessage = 0;
				this.CurrentTutorial = null;
				if (TutorialSystem.Tutorials.TryGetValue(name, out this.CurrentTutorial))
				{
					this.ShowTut(name);
				}
			}
			else if (force || (this.CurrentAskDialog == null && this.CurrentTutorial != null && this.CurrentMessage == this.CurrentTutorial.Length - 1 && name.Equals(TutorialSystem.Continuation.GetOrNull(this.CurrentTutorialName))))
			{
				if (this.CurrentTutorial != null && this.CurrentMessage == this.CurrentTutorial.Length - 1)
				{
					GameSettings.Instance.DisabledTutorials.Add(this.CurrentTutorialName);
				}
				this.CurrentMessage = 0;
				this.CurrentTutorial = null;
				if (TutorialSystem.Tutorials.TryGetValue(name, out this.CurrentTutorial))
				{
					this.ShowTut(name);
					GameSettings.GameSpeed = 0f;
				}
			}
			else if (this.CurrentTutorial == null || this.CurrentMessage == this.CurrentTutorial.Length - 1)
			{
				if (this.CurrentAskDialog != null)
				{
					GameSettings.ForcePause = false;
					if (!this.TutorialBacklog.Contains(this.LastAsk))
					{
						this.TutorialBacklog.Insert(0, this.LastAsk);
					}
					UnityEngine.Object.Destroy(this.CurrentAskDialog);
				}
				bool show = this.Window.Shown;
				this.Window.Close();
				this.LastAsk = name;
				this.CurrentAskDialog = WindowManager.Instance.ShowMessageBox("TutorialPrompt".Loc(new object[]
				{
					name.Loc()
				}), true, DialogWindow.DialogType.Question, new KeyValuePair<string, Action>[]
				{
					new KeyValuePair<string, Action>("Yes", delegate
					{
						this.UpdateContent(false);
						this.CurrentMessage = 0;
						this.CurrentTutorial = null;
						if (TutorialSystem.Tutorials.TryGetValue(name, out this.CurrentTutorial))
						{
							this.ShowTut(name);
							GameSettings.GameSpeed = 0f;
						}
					}),
					new KeyValuePair<string, Action>("Never", delegate
					{
						GameSettings.Instance.DisabledTutorials.Add(name);
						this.CurrentAskDialog = null;
						this.CheckBacklog();
						if (show)
						{
							this.Window.Show();
						}
					}),
					new KeyValuePair<string, Action>("Disable all", delegate
					{
						if (show)
						{
							this.Window.Show();
						}
						Options.Tutorial = false;
					}),
					new KeyValuePair<string, Action>("Not now", delegate
					{
						this.CurrentAskDialog = null;
						this.CheckBacklog();
						if (show)
						{
							this.Window.Show();
						}
					})
				}).gameObject;
			}
		}
	}

	public void AdvanceTutorial()
	{
		this.CurrentMessage++;
		this.UpdateContent(true);
	}

	public void EndTutorial()
	{
		if (this.EndText.text == "Cancel".Loc())
		{
			this.Window.Close();
			DialogWindow d = WindowManager.SpawnDialog();
			d.Show("CancelTutorial".Loc(), false, DialogWindow.DialogType.Question, new KeyValuePair<string, Action>[]
			{
				new KeyValuePair<string, Action>("Yes", delegate
				{
					this.CurrentMessage = this.CurrentTutorial.Length;
					this.UpdateContent(true);
					d.Window.Close();
				}),
				new KeyValuePair<string, Action>("No", delegate
				{
					d.Window.Close();
					this.Window.Show();
				})
			});
		}
		else
		{
			this.CurrentMessage = this.CurrentTutorial.Length;
			this.UpdateContent(true);
		}
	}

	public void UpdateContent(bool checkBacklog = true)
	{
		if (this.CurrentTutorial == null || this.CurrentMessage >= this.CurrentTutorial.Length)
		{
			this.PointsOfInterest.ForEach(delegate(GameObject x)
			{
				UnityEngine.Object.Destroy(x.gameObject);
			});
			this.PointsOfInterest.Clear();
			if (this.CurrentTutorial != null && GameSettings.Instance != null)
			{
				GameSettings.Instance.DisabledTutorials.Add(this.CurrentTutorialName);
			}
			this.CurrentTutorialName = null;
			this.CurrentTutorial = null;
			this.CurrentMessage = 0;
			this.Window.Close();
			if (checkBacklog)
			{
				this.CheckBacklog();
			}
			return;
		}
		this.AddPointsOfInterest();
		TutorialMessage tutorialMessage = this.CurrentTutorial[this.CurrentMessage];
		string input = Localization.GetTutorial(tutorialMessage.Message);
		Regex regex = new Regex("\\{([^\\}|:]+)(:([^\\}]+))?\\}");
		input = regex.Replace(input, delegate(Match x)
		{
			Func<string, string> orNull = TutorialSystem.RegReplacers.GetOrNull(x.Groups[1].Value);
			return (orNull != null) ? orNull(x.Groups[3].Value) : x.Groups[1].Value;
		});
		this.text.text = input;
		this.scrollBar.value = 0f;
		this.contentPanel.anchoredPosition = new Vector2(this.contentPanel.anchoredPosition.x, 0f);
		this.ContinueButton.SetActive(tutorialMessage.ManualContinue && this.CurrentMessage < this.CurrentTutorial.Length - 1);
		this.ContinueText.text = ((!tutorialMessage.ManualContinue) ? "Skip" : "Continue").Loc();
		this.EndText.text = ((this.CurrentMessage != this.CurrentTutorial.Length - 1) ? "Cancel" : "End").Loc();
		this.AddRings();
	}

	public void AddPointsOfInterest()
	{
		this.PointsOfInterest.ForEach(delegate(GameObject x)
		{
			UnityEngine.Object.Destroy(x.gameObject);
		});
		this.PointsOfInterest.Clear();
		if (this.CurrentMessage >= this.CurrentTutorial.Length)
		{
			return;
		}
		this.PointsOfInterest = this.CurrentTutorial[this.CurrentMessage].GetPoints();
	}

	public GameObject InsantiateArrow()
	{
		return UnityEngine.Object.Instantiate<GameObject>(this.ArrowPrefab);
	}

	private void Update()
	{
		if (this.CurrentTutorial != null && this.CurrentMessage < this.CurrentTutorial.Length)
		{
			TutorialMessage tutorialMessage = this.CurrentTutorial[this.CurrentMessage];
			if (!tutorialMessage.ManualContinue && tutorialMessage.CanContinue())
			{
				this.AdvanceTutorial();
			}
		}
	}

	private void AddRings()
	{
		if (this.CurrentMessage >= this.CurrentTutorial.Length)
		{
			return;
		}
		this.AddRing(this.Window.rectTransform.anchoredPosition + new Vector2(this.Window.rectTransform.sizeDelta.x / 2f, -this.Window.rectTransform.sizeDelta.y / 2f), 512);
	}

	public void AddRing(Vector2 p, int size = 256)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.RingPrefab);
		gameObject.transform.SetParent(WindowManager.Instance.MainPanel.transform, false);
		RingScript component = gameObject.GetComponent<RingScript>();
		component.rect.anchoredPosition = p;
		component.size = size;
	}

	static TutorialSystem()
	{
		// Note: this type is marked as 'beforefieldinit'.
		Dictionary<string, Func<bool>> dictionary = new Dictionary<string, Func<bool>>();
		dictionary.Add("Build mode", () => HUD.Instance.BuildMode);
		dictionary.Add("AudioOverlay", () => AudioVisualizer.Instance != null && AudioVisualizer.Instance.isActiveAndEnabled);
		dictionary.Add("Company window", () => HUD.Instance.companyWindow.Window.Shown);
		Dictionary<string, Func<bool>> dictionary2 = dictionary;
		string key = "Company detail window";
		if (TutorialSystem.<>f__mg$cache0 == null)
		{
			TutorialSystem.<>f__mg$cache0 = new Func<bool>(WindowManager.AnyWindowsOfType<CompanyDetailWindow>);
		}
		dictionary2.Add(key, TutorialSystem.<>f__mg$cache0);
		dictionary.Add("Employee window", () => HUD.Instance.employeeWindow.Window.Shown);
		dictionary.Add("Contract window", () => HUD.Instance.contractWindow.Window.Shown);
		dictionary.Add("Product window", () => HUD.Instance.GetProductWindow("YourRelease").Window.Shown);
		dictionary.Add("Educate window", () => HUD.Instance.educationWindow.Window.Shown);
		dictionary.Add("Review window", () => HUD.Instance.reviewWindow.Window.Shown);
		dictionary.Add("Benefit window", () => HUD.Instance.benefitWindow.Window.Shown && HUD.Instance.benefitWindow.IsCompany);
		dictionary.Add("Individual benefit window", () => HUD.Instance.benefitWindow.Window.Shown && !HUD.Instance.benefitWindow.IsCompany);
		dictionary.Add("Copy order window", () => HUD.Instance.copyOrderWindow.Window.Shown);
		dictionary.Add("Server window", () => HUD.Instance.serverWindow.Window.Shown);
		dictionary.Add("Build environment", () => HUD.Instance.LastType == BuildDescriptor.BuildType.Environment);
		dictionary.Add("Research window", () => HUD.Instance.researchWindow.Window.Shown);
		dictionary.Add("Team window", () => HUD.Instance.TeamWindow.Window.Shown);
		dictionary.Add("Automation window", () => HUD.Instance.TeamWindow.autoWindow.Window.Shown);
		dictionary.Add("Project management window", () => HUD.Instance.AutoDevWindow.Window.Shown);
		dictionary.Add("Design document window", () => HUD.Instance.docWindow.Window.Shown);
		dictionary.Add("Distribution window", () => HUD.Instance.distributionWindow.Window.Shown);
		dictionary.Add("Market window", () => HUD.Instance.marketingWindow.Window.Shown);
		dictionary.Add("Owns stocks", () => GameSettings.Instance.MyCompany.OwnedStock.Count > 1);
		dictionary.Add("Building room", () => BuildController.Instance.CurrentTempWall != null || BuildController.Instance.RectPoints != null);
		dictionary.Add("Has room", () => GameSettings.Instance.sRoomManager.Rooms.Count > 0);
		dictionary.Add("Has segments", delegate
		{
			bool result;
			if (GameSettings.Instance.sRoomManager.GetAllSegments().Any((RoomSegment x) => x.Type.Contains("Door")))
			{
				result = GameSettings.Instance.sRoomManager.GetAllSegments().Any((RoomSegment x) => x.Type.Contains("Window"));
			}
			else
			{
				result = false;
			}
			return result;
		});
		dictionary.Add("Has table", () => GameSettings.Instance.sRoomManager.Rooms.Any((Room x) => x.IsPlayerControlled() && x.GetFurniture("Table").Count > 0));
		dictionary.Add("Has computer", () => GameSettings.Instance.sRoomManager.Rooms.Any((Room x) => x.IsPlayerControlled() && x.GetFurniture("Computer").Count > 0));
		dictionary.Add("Has chair", () => GameSettings.Instance.sRoomManager.Rooms.Any((Room x) => x.IsPlayerControlled() && x.GetFurniture("Chair").Count > 0));
		dictionary.Add("Room with team", () => GameSettings.Instance.sRoomManager.Rooms.Any((Room x) => x.Teams.Count > 0));
		dictionary.Add("Employee with team", () => GameSettings.Instance.sActorManager.Actors.Any((Actor x) => x.Team != null));
		dictionary.Add("Work with team", () => (from x in GameSettings.Instance.MyCompany.WorkItems
		where x is DesignDocument
		select x).Any((WorkItem x) => x.DevTeams.Count > 0));
		dictionary.Add("Employee details", () => HUD.Instance.DetailWindow.Window.Shown);
		dictionary.Add("Select stuff", () => SelectorController.Instance.Selected.Count > 1);
		dictionary.Add("Employee select", delegate
		{
			bool result;
			if (SelectorController.Instance.Selected.Count > 0)
			{
				result = SelectorController.Instance.Selected.All((global::Selectable x) => x is Actor);
			}
			else
			{
				result = false;
			}
			return result;
		});
		dictionary.Add("Has contract design", () => GameSettings.Instance.MyCompany.WorkItems.OfType<DesignDocument>().Any((DesignDocument x) => x.contract != null));
		dictionary.Add("Has contract alpha", () => GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareAlpha>().Any((SoftwareAlpha x) => x.contract != null));
		dictionary.Add("No contract alpha", () => GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareAlpha>().None((SoftwareAlpha x) => x.contract != null));
		dictionary.Add("Contract selected", () => HUD.Instance.contractWindow.Contracts.Selected.Count > 0);
		dictionary.Add("Has review", () => GameSettings.Instance.MyCompany.WorkItems.OfType<ReviewWork>().Any<ReviewWork>());
		dictionary.Add("Has autodev", () => GameSettings.Instance.MyCompany.WorkItems.OfType<AutoDevWorkItem>().Any<AutoDevWorkItem>());
		dictionary.Add("Has autodev project", () => GameSettings.Instance.MyCompany.WorkItems.OfType<AutoDevWorkItem>().Any((AutoDevWorkItem x) => x.Items.Count > 0));
		dictionary.Add("Has delay", () => GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareAlpha>().Any((SoftwareAlpha x) => x.InDelay));
		dictionary.Add("Has beta", () => GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareAlpha>().Any((SoftwareAlpha x) => x.InBeta));
		dictionary.Add("Role change", () => HUD.Instance.roleSelect.Window.Shown);
		dictionary.Add("Hire window open", () => HUD.Instance.hireWindow.Window.Shown);
		dictionary.Add("Hire selected", () => HUD.Instance.hireWindow.EmployeeList.Selected.Count > 0);
		dictionary.Add("Has marketing plan", () => GameSettings.Instance.MyCompany.WorkItems.Any((WorkItem x) => x is MarketingPlan));
		dictionary.Add("Staff open", () => HUD.Instance.staffWindow.Window.Shown);
		dictionary.Add("Hire look open", () => HUD.Instance.hireWindow.LookWindow.Shown);
		dictionary.Add("Insurance open", () => HUD.Instance.insuranceWindow.Window.Shown);
		dictionary.Add("Color window", () => ColorWindow.Open);
		dictionary.Add("Wire mode", () => GameSettings.Instance.WireMode);
		dictionary.Add("Save window", () => SaveGameManager.Instance.SaveGameWindow.Shown);
		dictionary.Add("HR window", () => HUD.Instance.TeamWindow.autoWindow.Window.Shown);
		dictionary.Add("Has release date", () => GameSettings.Instance.MyCompany.WorkItems.OfType<DesignDocument>().Any((DesignDocument x) => x.ReleaseDate != null));
		dictionary.Add("Has press release", () => GameSettings.Instance.MyCompany.WorkItems.OfType<MarketingPlan>().Any((MarketingPlan x) => x.Type == MarketingPlan.TaskType.PressRelease));
		dictionary.Add("Has press build", () => GameSettings.Instance.PressBuildQueue.Count > 0);
		dictionary.Add("Team select window", () => HUD.Instance.TeamSelectWindow.Window.Shown);
		dictionary.Add("Rent overlay", () => "Rent".Equals(DataOverlay.Instance.ActiveOverlayName));
		dictionary.Add("Has subsidiary window", () => WindowManager.FindWindowType<CompanyDetailWindow>().Any((CompanyDetailWindow x) => x.company != null && x.company.IsPlayerOwned()));
		dictionary.Add("Has subsidiary button", () => HUD.Instance.docWindow.SubsidiaryCombo.gameObject.activeSelf);
		dictionary.Add("Leasable room selected", () => SelectorController.Instance.Selected.OfType<Room>().Any((Room x) => x.Rentable && !x.PlayerOwned));
		TutorialSystem.ContinueChecks = dictionary;
		TutorialSystem.Tutorials = null;
		TutorialSystem.Continuation = new Dictionary<string, string>();
	}

	public static Dictionary<string, Func<string, string>> RegReplacers = new Dictionary<string, Func<string, string>>
	{
		{
			"StartingFunds",
			(string x) => ActorCustomization.GetDefaultStartMoney().CurrencyInt(true)
		},
		{
			"StartingPlot",
			(string x) => PlotArea.StartPlotPrice.Currency(true)
		},
		{
			"SavingsInterest",
			(string x) => (Mathf.Pow(1f + InsuranceAccount.MonthlyInterest, 12f) - 1f).ToPercent()
		},
		{
			"KeyBind",
			(string x) => InputController.GetFullKeyBindString(InputController.GetKeyNum(x), false)
		}
	};

	public static Dictionary<string, Func<bool>> ContinueChecks;

	public static Dictionary<string, TutorialMessage[]> Tutorials;

	public static Dictionary<string, string> Continuation;

	[NonSerialized]
	public TutorialMessage[] CurrentTutorial;

	public string CurrentTutorialName;

	public int CurrentMessage;

	public static TutorialSystem Instance;

	public GUIWindow Window;

	public GameObject ContinueButton;

	public GameObject EndButton;

	public Text text;

	public Text ContinueText;

	public Text EndText;

	public LayoutElement textLayout;

	public Scrollbar scrollBar;

	public Texture2D ArrowTex;

	public GameObject RingPrefab;

	public GameObject ArrowPrefab;

	public RectTransform contentPanel;

	[NonSerialized]
	public List<string> TutorialBacklog = new List<string>();

	public GameObject CurrentAskDialog;

	public string LastAsk;

	private List<GameObject> PointsOfInterest = new List<GameObject>();

	[CompilerGenerated]
	private static Func<bool> <>f__mg$cache0;

	[Serializable]
	public struct TutorialRectObject
	{
		public TutorialRectObject(string name, RectTransform rect)
		{
			this.Name = name;
			this.Rect = rect;
		}

		public override string ToString()
		{
			return this.Name;
		}

		public string Name;

		public RectTransform Rect;
	}
}
