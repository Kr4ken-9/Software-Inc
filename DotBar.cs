﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DotBar : MaskableGraphic
{
	public override Texture mainTexture
	{
		get
		{
			return (!(this.DotImage == null)) ? this.DotImage : Graphic.s_WhiteTexture;
		}
	}

	public float Value
	{
		get
		{
			return this._value;
		}
		set
		{
			this._value = value;
			this.UpdateMe();
		}
	}

	protected override void OnPopulateMesh(VertexHelper vh)
	{
		vh.Clear();
		Vector2 p = Vector2.zero - base.rectTransform.pivot;
		Vector2 p2 = Vector2.one - base.rectTransform.pivot;
		p = new Vector2(p.x * base.rectTransform.rect.width, p.y * base.rectTransform.rect.height);
		p2 = new Vector2(p2.x * base.rectTransform.rect.width, p2.y * base.rectTransform.rect.height);
		ColorBarButton.DrawRect(p, p2, this.color, vh);
		float num = (p2.x - p.x - (float)(this.padding.right + this.padding.left) - this.InnerPadding * ((float)this.Count - 1f)) / (float)this.Count;
		float num2 = p.x + (float)this.padding.left;
		float height = p2.y - p.y - (float)(this.padding.bottom + this.padding.top);
		int num3 = Mathf.RoundToInt(this.Value * (float)(this.Count - 1));
		for (int i = 0; i < this.Count; i++)
		{
			ColorBarButton.DrawRectUV(new Rect(num2, p.y + (float)this.padding.top, num, height), (i != num3) ? this.Normal : this.Highlight, vh);
			num2 += num + this.InnerPadding;
		}
	}

	private void UpdateMe()
	{
		this.SetVerticesDirty();
	}

	public RectOffset padding;

	public Texture DotImage;

	public int Count;

	public float InnerPadding;

	public Color Highlight;

	public Color Normal;

	private float _value;
}
