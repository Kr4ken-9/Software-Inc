﻿using System;
using System.Collections.Generic;
using System.Linq;

public class SoftwareTypeOverride
{
	public SoftwareTypeOverride(XMLParser.XMLNode node)
	{
		string nodeValue = node.GetNodeValue("Delete", null);
		this.Delete = (nodeValue != null && nodeValue.ConvertToBool("Delete"));
		this.Name = node.GetNode("Name", true).Value;
		this.Category = node.GetNodeValue("Category", null);
		this.OSNeed = node.GetNodeValue("OSNeed", null);
		this.Description = node.GetNodeValue("Description", null);
		this.RandomFactor = node.GetNodeValueOptional<float>("Random");
		this.Popularity = node.GetNodeValueOptional<float>("Popularity");
		this.OSSpecific = node.GetNodeValueOptional<bool>("OSSpecific");
		if (this.OSSpecific != null && this.OSSpecific.Value)
		{
			this.OSLimit = node.GetNodeValue("OSLimit", null);
		}
		this.OneClient = node.GetNodeValueOptional<bool>("OneClient");
		this.InHouse = node.GetNodeValueOptional<bool>("InHouse");
		this.IdealPrice = node.GetNodeValueOptional<float>("IdealPrice");
		XMLParser.XMLNode node2 = node.GetNode("Categories", false);
		if (node2 != null)
		{
			this.Categories = (from x in node2.GetNodes("Category", true)
			select new SoftwareCategory(x, null)).ToArray<SoftwareCategory>();
			if (this.Categories.Length == 0)
			{
				this.Categories = null;
			}
			else
			{
				this.CategoryRngs = node.GetNode("Categories", true).GetNodes("Category", true).ToDictionary((XMLParser.XMLNode x) => x.GetAttribute("Name"), (XMLParser.XMLNode x) => x.GetNodeValue("NameGenerator", null));
			}
		}
		else
		{
			this.Categories = null;
		}
		this.NameGenerator = node.GetNodeValue("NameGenerator", null);
		XMLParser.XMLNode node3 = node.GetNode("Features", false);
		if (node3 != null)
		{
			this.Features = (from x in node.GetNode("Features", true).GetNodes("Feature", true)
			select new Feature(x, this.Name, false)).ToArray<Feature>();
			if (this.Features.Length == 0)
			{
				this.Features = null;
			}
		}
		else
		{
			this.Features = null;
		}
		this.Unlock = node.GetNodeValueOptional<int>("Unlock");
	}

	public readonly string Name;

	public readonly string Category;

	public readonly string Description;

	public readonly string NameGenerator;

	public readonly string OSNeed;

	public readonly string OSLimit;

	public readonly float? RandomFactor;

	public readonly float? Popularity;

	public readonly bool? OSSpecific;

	public readonly bool? OneClient;

	public readonly bool? InHouse;

	public readonly Feature[] Features;

	public readonly SoftwareCategory[] Categories;

	public readonly Dictionary<string, string> CategoryRngs;

	public readonly int? Unlock;

	public readonly float? IdealPrice;

	public readonly bool Delete;
}
