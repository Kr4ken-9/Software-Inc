﻿using System;

public interface IStockable
{
	uint SoftwareID { get; }

	uint PhysicalCopies { get; set; }

	string GetName();

	void AddLoss(float cost);

	Company GetCompany();
}
