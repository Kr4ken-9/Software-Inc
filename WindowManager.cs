﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WindowManager : MonoBehaviour
{
	private void OnApplicationQuit()
	{
		GameSettings.IsQuitting = true;
	}

	private void Awake()
	{
		if (WindowManager.Instance != null)
		{
			UnityEngine.Object.Destroy(WindowManager.Instance.gameObject);
		}
		if (this.Canvas != null)
		{
			this.Canvas.GetComponent<CanvasScaler>().scaleFactor = Options.UISize;
		}
		WindowManager.Instance = this;
	}

	private void Start()
	{
		if (!AudioManager.VolumeLoaded)
		{
			foreach (KeyValuePair<string, float> keyValuePair in AudioManager.InitVolumes)
			{
				AudioManager.SetVolume(keyValuePair.Key, keyValuePair.Value);
			}
			AudioManager.VolumeLoaded = true;
		}
	}

	public static string GetPathToObject(Transform sel, HashSet<Transform> visited = null)
	{
		if (WindowManager.Instance == null)
		{
			return null;
		}
		if (sel.parent == null)
		{
			return null;
		}
		string text = WindowManager.GetNameWithIndex(sel);
		if (visited != null)
		{
			visited.Add(sel);
		}
		sel = sel.parent;
		while (sel.gameObject != WindowManager.Instance.Canvas)
		{
			if (visited != null)
			{
				visited.Add(sel);
			}
			text = WindowManager.GetNameWithIndex(sel) + "/" + text;
			sel = sel.parent;
			if (sel == null)
			{
				return null;
			}
		}
		return text;
	}

	private static string GetNameWithIndex(Transform t)
	{
		string text = t.name;
		Transform parent = t.parent;
		int num = 0;
		if (parent != null)
		{
			for (int i = 0; i < parent.childCount; i++)
			{
				Transform child = parent.GetChild(i);
				if (child == t)
				{
					break;
				}
				if (child.name.Equals(t.name))
				{
					num++;
				}
			}
		}
		if (num > 0)
		{
			string text2 = text;
			text = string.Concat(new object[]
			{
				text2,
				"[",
				num,
				"]"
			});
		}
		return text;
	}

	public static RectTransform FindElementPath(string path, Transform startFrom = null)
	{
		if (WindowManager.Instance == null)
		{
			return null;
		}
		Transform transform = startFrom ?? WindowManager.Instance.Canvas.transform;
		bool flag = false;
		if (path.StartsWith("/"))
		{
			path = path.Substring(1);
			flag = true;
		}
		string[] array = path.Split(new char[]
		{
			'/'
		});
		int i = 0;
		if (flag && array.Length > 0)
		{
			transform = WindowManager.FindElement(array[0], SceneManager.GetActiveScene().GetChildren());
			i = 1;
		}
		while (i < array.Length)
		{
			string name = array[i];
			transform = WindowManager.FindElement(name, transform.GetChildren());
			if (transform == null)
			{
				return null;
			}
			i++;
		}
		return transform.GetComponent<RectTransform>();
	}

	private static Transform FindElement(string name, IEnumerable<Transform> t)
	{
		int num = 0;
		int num2 = name.IndexOf("[");
		if (num2 > 0)
		{
			int num3 = name.IndexOf("]");
			if (num3 > num2)
			{
				try
				{
					num = Convert.ToInt32(name.Substring(num2 + 1, num3 - num2 - 1));
					name = name.Substring(0, num2);
				}
				catch (Exception)
				{
				}
			}
		}
		foreach (Transform transform in t)
		{
			if (transform.name.Equals(name))
			{
				if (num <= 0)
				{
					return transform;
				}
				num--;
			}
		}
		return null;
	}

	public static void AddElementToWindow(GameObject element, GUIWindow window, Rect position, Rect anchors)
	{
		WindowManager.AddElementToElement(element, window.MainPanel, position, anchors);
	}

	public static void AddElementToElement(GameObject element, GameObject parent, Rect position, Rect anchors)
	{
		element.transform.SetParent(parent.transform, false);
		RectTransform component = element.GetComponent<RectTransform>();
		component.anchorMin = new Vector2(anchors.xMin, 1f - anchors.yMax);
		component.anchorMax = new Vector2(anchors.xMax, 1f - anchors.yMin);
		component.offsetMin = new Vector2(position.xMin, -position.yMax);
		component.offsetMax = new Vector2(position.xMax, -position.yMin);
	}

	public bool IsActiveWindow(GUIWindow window)
	{
		return window == this.GetFrontMostWindow();
	}

	public GUIWindow GetFrontMostWindow()
	{
		if (WindowManager.HasModal && WindowManager.ModalWindows.Count > 0)
		{
			return WindowManager.ModalWindows[WindowManager.ModalWindows.Count - 1];
		}
		if (WindowManager.ActiveWindows.Count > 0)
		{
			return WindowManager.ActiveWindows[WindowManager.ActiveWindows.Count - 1];
		}
		return null;
	}

	public static GUIWindow SpawnWindow()
	{
		if (WindowManager.Instance == null || WindowManager.Instance.WindowPrefab == null)
		{
			return null;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.WindowPrefab);
		gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		return gameObject.GetComponent<GUIWindow>();
	}

	public static Text SpawnLabel()
	{
		if (WindowManager.Instance == null || WindowManager.Instance.LabelPrefab == null)
		{
			return null;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.LabelPrefab);
		return gameObject.GetComponent<Text>();
	}

	public static GUIListView SpawnList()
	{
		if (WindowManager.Instance == null || WindowManager.Instance.GUIListPrefab == null)
		{
			return null;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.GUIListPrefab);
		return gameObject.GetComponent<GUIListView>();
	}

	public static InputField SpawnInputbox()
	{
		if (WindowManager.Instance == null || WindowManager.Instance.InputPrefab == null)
		{
			return null;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.InputPrefab);
		return gameObject.GetComponent<InputField>();
	}

	public static RectTransform SpawnPanel()
	{
		if (WindowManager.Instance == null || WindowManager.Instance.PanelPrefab == null)
		{
			return null;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.PanelPrefab);
		return gameObject.GetComponent<RectTransform>();
	}

	public static Button SpawnButton()
	{
		if (WindowManager.Instance == null || WindowManager.Instance.ButtonPrefab == null)
		{
			return null;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.ButtonPrefab);
		return gameObject.GetComponent<Button>();
	}

	public static Toggle SpawnCheckbox()
	{
		if (WindowManager.Instance == null || WindowManager.Instance.CheckboxPrefab == null)
		{
			return null;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.CheckboxPrefab);
		return gameObject.GetComponent<Toggle>();
	}

	public static GUIProgressBar SpawnProgressbar()
	{
		if (WindowManager.Instance == null || WindowManager.Instance.ProgressbarPrefab == null)
		{
			return null;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.ProgressbarPrefab);
		return gameObject.GetComponent<GUIProgressBar>();
	}

	private void OnDestroy()
	{
		if (WindowManager.Instance == this)
		{
			WindowManager.Instance = null;
		}
	}

	public static DialogWindow SpawnDialog()
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.DialogPrefab);
		gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		return gameObject.GetComponent<DialogWindow>();
	}

	public static void SpawnDialog(string msg, bool modal, DialogWindow.DialogType type)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.DialogPrefab);
		gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		DialogWindow diag = gameObject.GetComponent<DialogWindow>();
		diag.Show(msg, !modal, type, new KeyValuePair<string, Action>[]
		{
			new KeyValuePair<string, Action>("OK", delegate
			{
				diag.Window.Close();
			})
		});
	}

	public static void SpawnInputDialog(string prompt, string title, string defaultText, Action<string> onFinish, Action onCancel = null)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.InputDialogPrefab);
		gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2((float)(Screen.width / 2 - 219), (float)(-(float)Screen.height / 2 + 73));
		gameObject.GetComponent<InputDialog>().Init(prompt, title, defaultText, onFinish, onCancel);
	}

	public static void SpawnTableInfoWindow(string title, string header, string[] var, string[] value, string ID = null)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.TableInfoPrefab);
		gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		gameObject.GetComponent<TableInfoWindow>().Init(title, header, var, value, ID);
		if (ID != null)
		{
			GUIWindow guiwindow;
			if (WindowManager.Instance.InfoTableOpen.TryGetValue(ID, out guiwindow))
			{
				guiwindow.Close();
			}
			WindowManager.Instance.InfoTableOpen[ID] = gameObject.GetComponent<GUIWindow>();
		}
	}

	public static void DeregisterTableInfoWindow(string ID)
	{
		WindowManager.Instance.InfoTableOpen.Remove(ID);
	}

	public static List<T> FindWindowType<T>()
	{
		List<T> list = WindowManager.ActiveWindows.SelectNotNull((GUIWindow x) => x.GetComponent<T>()).ToList<T>();
		list.AddRange(WindowManager.ModalWindows.SelectNotNull((GUIWindow x) => x.GetComponent<T>()));
		return list;
	}

	public static IEnumerable<T> FindWindowTypeEnum<T>()
	{
		for (int i = 0; i < WindowManager.ActiveWindows.Count; i++)
		{
			T window = WindowManager.ActiveWindows[i].GetComponent<T>();
			if (window != null)
			{
				yield return window;
			}
		}
		for (int j = 0; j < WindowManager.ModalWindows.Count; j++)
		{
			T window2 = WindowManager.ModalWindows[j].GetComponent<T>();
			if (window2 != null)
			{
				yield return window2;
			}
		}
		yield break;
	}

	public static bool AnyWindowsOfType<T>()
	{
		for (int i = 0; i < WindowManager.ActiveWindows.Count; i++)
		{
			T component = WindowManager.ActiveWindows[i].GetComponent<T>();
			if (component != null)
			{
				return true;
			}
		}
		for (int j = 0; j < WindowManager.ModalWindows.Count; j++)
		{
			T component2 = WindowManager.ModalWindows[j].GetComponent<T>();
			if (component2 != null)
			{
				return true;
			}
		}
		return false;
	}

	public static ColorWindow SpawnColorDialog(Action<Color> action, Color startColor, Color[] defaults = null)
	{
		return WindowManager.SpawnColorDialog(new string[]
		{
			string.Empty
		}, new Action<Color>[]
		{
			action
		}, new Color[]
		{
			startColor
		}, defaults, null);
	}

	public static ColorWindow SpawnColorDialog(string[] tabs, IList<Action<Color>> actions, Color[] startColors, Color[] defaults = null, Action extraClose = null)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(WindowManager.Instance.ColorDialogPrefab);
		gameObject.transform.SetParent(WindowManager.Instance.Canvas.transform, false);
		RectTransform component = gameObject.GetComponent<RectTransform>();
		component.anchoredPosition = new Vector2((float)(Screen.width / 2) - component.rect.width / 2f, (float)(-(float)Screen.height / 2) + component.rect.height / 2f);
		ColorWindow component2 = gameObject.GetComponent<ColorWindow>();
		component2.Init(tabs, actions, startColors, defaults, extraClose);
		return component2;
	}

	private void UpdateCorner(Vector2 size)
	{
		float num = (float)Screen.width / Options.UISize - size.x;
		float num2 = (float)Screen.height / Options.UISize - size.y;
		this.Corner = new Vector2((num > 0f) ? ((this.Corner.x + 64f) % num) : 0f, (num2 > 0f) ? ((this.Corner.y + 64f) % num2) : 0f);
	}

	public static void RegisterWindow(GUIWindow window)
	{
		bool flag = false;
		if (!window.IsCollapsed && !string.IsNullOrEmpty(window.WindowSizeID))
		{
			SVector3 windowSize = Options.GetWindowSize(window.WindowSizeID);
			if (windowSize != null && windowSize.z > 0f && windowSize.w > 0f)
			{
				if (window.SavePosition)
				{
					window.rectTransform.anchoredPosition = new Vector2(windowSize.x, windowSize.y);
					flag = true;
				}
				if (window.CanSize)
				{
					window.rectTransform.sizeDelta = new Vector2(Mathf.Max(window.rectTransform.sizeDelta.x, windowSize.z), Mathf.Max(window.rectTransform.sizeDelta.y, windowSize.w));
				}
			}
			else if (windowSize == null && !window.HasBeenShown && window.CanSize)
			{
				float num = Mathf.Min((float)Screen.width / 1024f, (float)Screen.height / 768f) / Options.UISize;
				if (num > 1f)
				{
					window.rectTransform.sizeDelta = new Vector2(Mathf.Max(window.MinSize.x, window.rectTransform.sizeDelta.x * num), Mathf.Max(window.MinSize.y, window.rectTransform.sizeDelta.y * num));
				}
			}
		}
		if (!flag && !window.HasBeenShown)
		{
			WindowManager.Instance.UpdateCorner(window.rectTransform.sizeDelta);
			window.rectTransform.anchoredPosition = new Vector2(WindowManager.Instance.Corner.x, -WindowManager.Instance.Corner.y);
		}
		window.rectTransform.sizeDelta = new Vector2(Mathf.Clamp(window.rectTransform.sizeDelta.x, window.MinSize.x, (float)Screen.width / Options.UISize), Mathf.Clamp(window.rectTransform.sizeDelta.y, window.MinSize.y, (float)Screen.height / Options.UISize));
		window.rectTransform.anchoredPosition = new Vector2(Mathf.Clamp(window.rectTransform.anchoredPosition.x, -window.rectTransform.sizeDelta.x + 64f, (float)Screen.width / Options.UISize - 64f), Mathf.Clamp(window.rectTransform.anchoredPosition.y, (float)(-(float)Screen.height) / Options.UISize + window.rectTransform.sizeDelta.y, 0f));
		if (window.Modal)
		{
			if (WindowManager.ActiveWindows.Contains(window))
			{
				WindowManager.ActiveWindows.Remove(window);
			}
			if (!WindowManager.ModalWindows.Contains(window))
			{
				WindowManager.ModalWindows.Add(window);
				WindowManager.HasModal = true;
			}
		}
		else
		{
			if (WindowManager.ModalWindows.Contains(window))
			{
				WindowManager.ModalWindows.Remove(window);
			}
			if (!WindowManager.ActiveWindows.Contains(window))
			{
				WindowManager.AddWindow(window);
			}
		}
		WindowManager.Focus(window);
		if (WindowManager.HasModal)
		{
			GUIWindow guiwindow = WindowManager.ModalWindows[WindowManager.ModalWindows.Count - 1];
			WindowManager.Focus(guiwindow);
			WindowManager.ActivateBlockPanel(true, !guiwindow.HideBlockPanel);
		}
	}

	public static void Focus(GUIWindow window)
	{
		int num = WindowManager.ActiveWindows.IndexOf(window);
		if (num > -1)
		{
			if (num < WindowManager.ActiveWindows.Count - 1)
			{
				WindowManager.ActiveWindows.RemoveAt(num);
				WindowManager.AddWindow(window);
			}
		}
		else
		{
			num = WindowManager.ModalWindows.IndexOf(window);
			if (num > -1 && num < WindowManager.ModalWindows.Count - 1)
			{
				WindowManager.ModalWindows.RemoveAt(num);
				WindowManager.ModalWindows.Add(window);
			}
		}
		WindowManager.ReorderWindows();
	}

	private static void AddWindow(GUIWindow window)
	{
		if (window.AlwaysOnTop)
		{
			WindowManager.ActiveWindows.Add(window);
		}
		else if (WindowManager.ActiveWindows.Any((GUIWindow x) => x.AlwaysOnTop))
		{
			int i;
			for (i = 0; i < WindowManager.ActiveWindows.Count; i++)
			{
				if (WindowManager.ActiveWindows[i].AlwaysOnTop)
				{
					break;
				}
			}
			WindowManager.ActiveWindows.Insert(i, window);
		}
		else
		{
			WindowManager.ActiveWindows.Add(window);
		}
	}

	private static void ReorderWindows()
	{
		int num = WindowManager.Instance.StartIndex;
		WindowManager.ActiveWindows.RemoveAll((GUIWindow x) => x == null || x.gameObject == null);
		WindowManager.ModalWindows.RemoveAll((GUIWindow x) => x == null || x.gameObject == null);
		foreach (GUIWindow guiwindow in from x in WindowManager.ActiveWindows
		where !x.AlwaysOnTop
		select x)
		{
			guiwindow.transform.SetSiblingIndex(num);
			num++;
		}
		if (WindowManager.Instance.BlockPanel != null)
		{
			WindowManager.Instance.BlockPanel.transform.SetSiblingIndex(num);
			num++;
		}
		foreach (GUIWindow guiwindow2 in WindowManager.ModalWindows)
		{
			guiwindow2.transform.SetSiblingIndex(num);
			num++;
		}
		foreach (GUIWindow guiwindow3 in from x in WindowManager.ActiveWindows
		where x.AlwaysOnTop
		select x)
		{
			guiwindow3.transform.SetSiblingIndex(num);
			num++;
		}
	}

	private static void ActivateMainPanel(bool activate)
	{
		if (WindowManager.Instance != null && WindowManager.Instance.MainPanel != null)
		{
			WindowManager.Instance.MainPanel.SetActive(activate);
		}
	}

	private static void ActivateBlockPanel(bool activate, bool visible)
	{
		if (WindowManager.Instance == null || WindowManager.Instance.BlockPanel == null || WindowManager.Instance.BlockPanel.gameObject == null)
		{
			return;
		}
		if (WindowManager.Instance.BlockPanel == null)
		{
			WindowManager.ActivateMainPanel(!activate);
			return;
		}
		WindowManager.Instance.BlockPanel.GetComponent<Image>().color = new Color32(0, 0, 0, (!visible) ? 1 : 91);
		if (!GameSettings.IsQuitting && !ErrorLogging.SceneChanging)
		{
			WindowManager.Instance.BlockPanel.gameObject.SetActive(activate);
		}
	}

	public static void DisableAll(bool mainPanel, GUIWindow Exempt = null)
	{
		bool flag = Exempt != null && Exempt.gameObject.activeSelf;
		WindowManager.ActiveWindows.ForEach(delegate(GUIWindow x)
		{
			x.gameObject.SetActive(false);
		});
		WindowManager.ModalWindows.ForEach(delegate(GUIWindow x)
		{
			x.gameObject.SetActive(false);
		});
		if (mainPanel)
		{
			WindowManager.ActivateMainPanel(false);
		}
		if (flag)
		{
			Exempt.gameObject.SetActive(true);
		}
	}

	public static void EnableAll()
	{
		if (WindowManager.ModalWindows.Count > 0)
		{
			WindowManager.ModalWindows[WindowManager.ModalWindows.Count - 1].gameObject.SetActive(true);
		}
		else
		{
			WindowManager.ActiveWindows.ForEach(delegate(GUIWindow x)
			{
				x.gameObject.SetActive(true);
			});
			WindowManager.ActivateMainPanel(true);
		}
	}

	public static void DeregisterWindow(GUIWindow window)
	{
		if (window.Modal)
		{
			WindowManager.ModalWindows.Remove(window);
			if (WindowManager.ModalWindows.Count > 0)
			{
				WindowManager.ActivateBlockPanel(true, !WindowManager.ModalWindows[WindowManager.ModalWindows.Count - 1].HideBlockPanel);
			}
			else
			{
				WindowManager.HasModal = false;
				WindowManager.ActivateBlockPanel(false, true);
			}
		}
		else
		{
			WindowManager.ActiveWindows.Remove(window);
		}
	}

	public DialogWindow ShowMessageBox(string msg, bool pausing, DialogWindow.DialogType type)
	{
		if (pausing && GameSettings.Instance != null)
		{
			GameSettings.ForcePause = true;
			GameSettings.FreezeGame = true;
		}
		DialogWindow dialog = WindowManager.SpawnDialog();
		dialog.Show(msg, false, type, new KeyValuePair<string, Action>[]
		{
			new KeyValuePair<string, Action>("OK", delegate
			{
				if (pausing)
				{
					GameSettings.ForcePause = false;
				}
				dialog.Window.Close();
			})
		});
		return dialog;
	}

	public DialogWindow ShowMessageBox(string msg, bool pausing, DialogWindow.DialogType type, Action yesAction, string questionID = null, Action extraNoAction = null)
	{
		if (questionID != null && Options.IgnoreQuestions.Contains(questionID))
		{
			yesAction();
			return null;
		}
		if (pausing && GameSettings.Instance != null)
		{
			GameSettings.ForcePause = true;
			GameSettings.FreezeGame = true;
		}
		DialogWindow dialog = WindowManager.SpawnDialog();
		if (questionID == null)
		{
			dialog.Show(msg, false, type, new KeyValuePair<string, Action>[]
			{
				new KeyValuePair<string, Action>("Yes", delegate
				{
					if (pausing)
					{
						GameSettings.ForcePause = false;
					}
					yesAction();
					dialog.Window.Close();
				}),
				new KeyValuePair<string, Action>("No", delegate
				{
					if (pausing)
					{
						GameSettings.ForcePause = false;
					}
					if (extraNoAction != null)
					{
						extraNoAction();
					}
					dialog.Window.Close();
				})
			});
		}
		else
		{
			dialog.Show(msg, false, type, new KeyValuePair<string, Action>[]
			{
				new KeyValuePair<string, Action>("Yes", delegate
				{
					if (pausing)
					{
						GameSettings.ForcePause = false;
					}
					yesAction();
					dialog.Window.Close();
				}),
				new KeyValuePair<string, Action>("Don't ask again", delegate
				{
					if (pausing)
					{
						GameSettings.ForcePause = false;
					}
					yesAction();
					Options.IgnoreQuestions.Add(questionID);
					Options.SaveToFile();
					dialog.Window.Close();
				}),
				new KeyValuePair<string, Action>("No", delegate
				{
					if (pausing)
					{
						GameSettings.ForcePause = false;
					}
					if (extraNoAction != null)
					{
						extraNoAction();
					}
					dialog.Window.Close();
				})
			});
		}
		return dialog;
	}

	public DialogWindow ShowMessageBox(string msg, bool pausing, DialogWindow.DialogType type, params KeyValuePair<string, Action>[] actions)
	{
		if (pausing && GameSettings.Instance != null)
		{
			GameSettings.ForcePause = true;
			GameSettings.FreezeGame = true;
		}
		DialogWindow dialog = WindowManager.SpawnDialog();
		KeyValuePair<string, Action>[] buttons = actions.SelectInPlace((KeyValuePair<string, Action> x) => new KeyValuePair<string, Action>(x.Key, delegate
		{
			if (pausing)
			{
				GameSettings.ForcePause = false;
			}
			x.Value();
			dialog.Window.Close();
		}));
		dialog.Show(msg, false, type, buttons);
		return dialog;
	}

	public static List<GUIWindow> ActiveWindows = new List<GUIWindow>();

	public static List<GUIWindow> ModalWindows = new List<GUIWindow>();

	public GameObject DialogPrefab;

	public GameObject ColorDialogPrefab;

	public GameObject InputDialogPrefab;

	public GameObject TableInfoPrefab;

	public GameObject Canvas;

	public GameObject MainPanel;

	public GameObject BlockPanel;

	public GameObject WindowPrefab;

	public GameObject LabelPrefab;

	public GameObject PanelPrefab;

	public GameObject ButtonPrefab;

	public GameObject InputPrefab;

	public GameObject CheckboxPrefab;

	public GameObject ScrollbarPrefab;

	public GameObject ProgressbarPrefab;

	public GameObject GUIListPrefab;

	public static bool HasModal = false;

	public static WindowManager Instance;

	public int StartIndex = 1;

	public bool MainScene;

	private Vector2 Corner = new Vector2(64f, 64f);

	[NonSerialized]
	private Dictionary<string, GUIWindow> InfoTableOpen = new Dictionary<string, GUIWindow>();
}
