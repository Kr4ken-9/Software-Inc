﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FloorFurnitureMesh
{
	public FloorFurnitureMesh(int floor)
	{
		this.Floor = floor;
		this.go = new GameObject("CombineFloor " + floor);
	}

	public void Refresh(List<Room> rooms)
	{
		List<List<CombineInstance>> list = new List<List<CombineInstance>>();
		List<List<CombineInstance>> list2 = new List<List<CombineInstance>>();
		List<List<CombineInstance>> list3 = new List<List<CombineInstance>>();
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		for (int i = 0; i < rooms.Count; i++)
		{
			if (rooms[i].Floor == this.Floor)
			{
				List<Furniture> furnitures = rooms[i].GetFurnitures();
				for (int j = 0; j < furnitures.Count; j++)
				{
					Furniture furniture = furnitures[j];
					if (furniture.UseStandardMat && !furniture.DisableCombine)
					{
						KeyValuePair<CombineInstance[], int> keyValuePair = furniture.FixCombine();
						if (furniture.WallFurn)
						{
							if (list3.Count == 0 || num3 + keyValuePair.Value > 50000)
							{
								list3.Add(new List<CombineInstance>());
								num3 = 0;
							}
							list3[list3.Count - 1].AddRange(keyValuePair.Key);
							num3 += keyValuePair.Value;
						}
						else if (furniture.Height1 > 1.6f)
						{
							if (list2.Count == 0 || num2 + keyValuePair.Value > 50000)
							{
								list2.Add(new List<CombineInstance>());
								num2 = 0;
							}
							list2[list2.Count - 1].AddRange(keyValuePair.Key);
							num2 += keyValuePair.Value;
						}
						else
						{
							if (list.Count == 0 || num + keyValuePair.Value > 50000)
							{
								list.Add(new List<CombineInstance>());
								num = 0;
							}
							list[list.Count - 1].AddRange(keyValuePair.Key);
							num += keyValuePair.Value;
						}
					}
				}
			}
		}
		this.UpdateCombineList(this._normalFurn, list, "NormalFurniture");
		this.UpdateCombineList(this._ceilFurn, list2, "CeilingFurniture");
		this.UpdateCombineList(this._wallFurn, list3, "WallFurniture");
		this.UpdateVisbility();
	}

	public Thread RunThread(List<Furniture> furns, int min, int max)
	{
		Thread thread = new Thread(delegate
		{
			int num = 0;
			for (int i = min; i < max; i++)
			{
				num += furns[i].Colorable.Count;
			}
			CombineInstance[] array = new CombineInstance[num];
			int num2 = 0;
			for (int j = min; j < max; j++)
			{
				CombineInstance[] key = furns[j].FixCombine().Key;
				for (int k = 0; k < key.Length; k++)
				{
					array[num2] = key[k];
					num2++;
				}
			}
		});
		thread.Start();
		return thread;
	}

	public void UpdateVisbility()
	{
		bool flag = CameraScript.Instance.FlyMode || this.Floor <= GameSettings.Instance.ActiveFloor;
		if (this._ceilFurn.Count > 0)
		{
			for (int i = 0; i < this._ceilFurn.Count; i++)
			{
				this._ceilFurn[i].enabled = (!GameSettings.Instance.HideCeilingFurniture && flag);
			}
		}
		if (this._normalFurn.Count > 0)
		{
			for (int j = 0; j < this._normalFurn.Count; j++)
			{
				this._normalFurn[j].enabled = flag;
			}
		}
		if (this._wallFurn.Count > 0)
		{
			for (int k = 0; k < this._wallFurn.Count; k++)
			{
				this._wallFurn[k].enabled = (GameSettings.WallsDown == GameSettings.WallState.High && flag);
			}
		}
	}

	private void UpdateCombineList(List<MeshRenderer> cache, List<List<CombineInstance>> result, string name)
	{
		for (int i = 0; i < result.Count; i++)
		{
			if (i >= cache.Count)
			{
				GameObject gameObject = new GameObject();
				gameObject.name = string.Concat(new object[]
				{
					name,
					"(",
					i + 1,
					")"
				});
				MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
				MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
				meshFilter.mesh.Clear();
				meshRenderer.sharedMaterial = ObjectDatabase.Instance.CombineFurnitureMaterial;
				gameObject.transform.SetParent(this.go.transform);
				cache.Add(meshRenderer);
			}
			MeshFilter component = cache[i].GetComponent<MeshFilter>();
			component.sharedMesh.Clear();
			component.sharedMesh.CombineMeshes(result[i].ToArray());
		}
		for (int j = cache.Count - 1; j >= result.Count; j--)
		{
			UnityEngine.Object.Destroy(cache[j].GetComponent<MeshFilter>().sharedMesh);
			UnityEngine.Object.Destroy(cache[j].gameObject);
			cache.RemoveAt(j);
		}
	}

	private readonly List<MeshRenderer> _normalFurn = new List<MeshRenderer>();

	private readonly List<MeshRenderer> _ceilFurn = new List<MeshRenderer>();

	private readonly List<MeshRenderer> _wallFurn = new List<MeshRenderer>();

	public int Floor;

	private GameObject go;
}
