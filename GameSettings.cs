﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
	public RoomGroup GetRoomGroup(string name)
	{
		return (name != null) ? this.RoomGroups.GetOrNull(name) : null;
	}

	public IEnumerable<string> GetRoomGroups()
	{
		return this.RoomGroups.Keys;
	}

	public IEnumerable<RoomGroup> GetUnderlyingRoomGroups()
	{
		return this.RoomGroups.Values;
	}

	public RoomGroup AddRoomGroup(string name)
	{
		RoomGroup roomGroup = new RoomGroup(name);
		this.RoomGroups[name] = roomGroup;
		HUD.Instance.roomGroupWindow.UpdateList();
		return roomGroup;
	}

	public void RemoveRoomFromGroups(Room room)
	{
		foreach (RoomGroup roomGroup in this.RoomGroups.Values)
		{
			roomGroup.RemoveRoom(room);
		}
	}

	public void RemoveRoomGroup(string name)
	{
		RoomGroup roomGroup = this.GetRoomGroup(name);
		if (roomGroup != null)
		{
			List<Room> rooms = roomGroup.GetRooms();
			for (int i = 0; i < rooms.Count; i++)
			{
				Room room = rooms[i];
				room.RoomGroup = null;
			}
			foreach (Actor actor in GameSettings.Instance.sActorManager.Staff)
			{
				actor.AssignedRoomGroups.Remove(name);
			}
			this.RoomGroups.Remove(name);
			HUD.Instance.roomGroupWindow.UpdateList();
		}
	}

	private void OnApplicationQuit()
	{
		GameSettings.IsQuitting = true;
	}

	public Server GetServer(string name)
	{
		return this.MainServers.GetOrNull(name) as Server;
	}

	public void FlipBills()
	{
		Dictionary<Company.TransactionCategory, Dictionary<string, float>> billsNext = this.BillsNext;
		this.BillsNext = this.BillsCurrent;
		this.BillsNext.Clear();
		this.BillsCurrent = billsNext;
	}

	public int UndoCount
	{
		get
		{
			return this.UndoList.Count;
		}
	}

	public void AddUndo(params UndoObject.UndoAction[] undos)
	{
		this.UndoButton.SetActive(HUD.Instance.BuildMode);
		this.UndoList.Add(new UndoObject(undos));
		if (this.UndoList.Count > GameSettings.MaxUndo)
		{
			this.UndoList.RemoveAt(0);
		}
		this.UpdateUndoTip();
	}

	public void ResetUndo()
	{
		this.UndoList.Clear();
		this.UndoButton.SetActive(false);
	}

	private void UpdateUndoTip()
	{
		if (this.UndoList.Count > 0)
		{
			this.UndoTip.TooltipDescription = this.UndoList[this.UndoList.Count - 1].Description;
			this.UndoTip.UpdateTip();
		}
	}

	public void Undo()
	{
		if (this.UndoList.Count > 0)
		{
			UndoObject undoObject = this.UndoList[this.UndoList.Count - 1];
			this.UndoList.RemoveAt(this.UndoList.Count - 1);
			this.UpdateUndoTip();
			if (this.UndoAutosaveWait <= 0f)
			{
				SaveGameManager.Instance.AutoSave();
				this.UndoAutosaveWait = 20f;
			}
			BuildController.Instance.ClearBuild(false, false, false, false);
			undoObject.Execute();
			if (this.UndoList.Count == 0)
			{
				this.UndoButton.SetActive(false);
			}
		}
	}

	public void CancelPrintOrder(uint ID, bool repercussion)
	{
		PrintJob orNull = this.PrintOrders.GetOrNull(ID);
		if (orNull != null)
		{
			if (orNull.DealID != null)
			{
				Deal deal = null;
				if (HUD.Instance.dealWindow.AllDeals.TryGetValue(orNull.DealID.Value, out deal) && deal.Active)
				{
					if (repercussion)
					{
						PrintDeal printDeal = deal as PrintDeal;
						if (printDeal != null)
						{
							printDeal.FinalizeDeal();
						}
						else
						{
							HUD.Instance.dealWindow.CancelDeal(deal, false);
						}
					}
					else
					{
						HUD.Instance.dealWindow.CancelDeal(deal, false);
					}
				}
			}
			this.PrintOrders.Remove(ID);
		}
	}

	public void SetPrintsToStorage(uint id, uint amount, bool add)
	{
		uint num = this._printsInStorage.GetOrDefault(id, 0u);
		if (add)
		{
			num += amount;
			this._printsInStorage[id] = num;
		}
		else if (amount >= num)
		{
			this._printsInStorage.Remove(id);
		}
		else
		{
			this._printsInStorage[id] = num - amount;
		}
	}

	public static void UnloadNow()
	{
		GameSettings.IsQuitting = true;
		if (GameSettings.Instance != null && GameSettings.Instance._simThread != null && GameSettings.Instance._simThread.IsAlive)
		{
			GameSettings.Instance._simThread.Abort();
			float realtimeSinceStartup = Time.realtimeSinceStartup;
			while (GameSettings.Instance._simThread.IsAlive && Time.realtimeSinceStartup - realtimeSinceStartup < 5f)
			{
			}
			Debug.Log(string.Concat(new object[]
			{
				"Quitting while simulating: Waited ",
				Time.realtimeSinceStartup - realtimeSinceStartup,
				" seconds - Thread state ",
				GameSettings.Instance._simThread.ThreadState.ToString()
			}));
		}
	}

	public uint GetPrintsInStorage(uint id, bool withStock = false)
	{
		uint num = this._printsInStorage.GetOrDefault(id, 0u);
		if (withStock)
		{
			IStockable stockable = ProductPrintOrder.StockableFromID(id);
			if (stockable != null)
			{
				num += stockable.PhysicalCopies;
			}
		}
		return num;
	}

	public void RegisterActor(Actor act, bool register)
	{
		if (register)
		{
			if (act.AItype == AI<Actor>.AIType.Employee)
			{
				this.WorkUpdateHandler.RegisterObject(act);
			}
			this.ActorUpdateHandler.RegisterObject(act);
		}
		else
		{
			this.WorkUpdateHandler.UnregisterObject(act);
			this.ActorUpdateHandler.UnregisterObject(act);
		}
	}

	public void RemoveTeamAssociation(Team team)
	{
		foreach (Room room in this.sRoomManager.Rooms)
		{
			room.Teams.Remove(team);
		}
		foreach (AutoDevWorkItem autoDevWorkItem in this.MyCompany.WorkItems.OfType<AutoDevWorkItem>())
		{
			autoDevWorkItem.DesignTeams.Remove(team.Name);
			autoDevWorkItem.SDevTeams.Remove(team.Name);
			autoDevWorkItem.SecondaryDevTeams.Remove(team.Name);
			autoDevWorkItem.SupportTeams.Remove(team.Name);
			autoDevWorkItem.MarketingTeams.Remove(team.Name);
			autoDevWorkItem.PostMarketingTeams.Remove(team.Name);
			autoDevWorkItem.RefreshTeams();
		}
		foreach (KeyValuePair<string, HashSet<string>> keyValuePair in this.TeamDefaults.ToList<KeyValuePair<string, HashSet<string>>>())
		{
			if (keyValuePair.Value.Contains(team.Name))
			{
				this.TeamDefaults[keyValuePair.Key] = (from x in keyValuePair.Value
				where !x.Equals(team.Name)
				select x).ToHashSet<string>();
			}
		}
	}

	public void SwitchTeamAssociation(Team team, Team newTeam)
	{
		foreach (Room room in this.sRoomManager.Rooms)
		{
			if (room.Teams.Contains(team))
			{
				room.Teams.Remove(team);
				room.AddTeam(newTeam);
			}
		}
		foreach (AutoDevWorkItem autoDevWorkItem in this.MyCompany.WorkItems.OfType<AutoDevWorkItem>())
		{
			autoDevWorkItem.DesignTeams.Swap(team.Name, newTeam.Name);
			autoDevWorkItem.SDevTeams.Swap(team.Name, newTeam.Name);
			autoDevWorkItem.SecondaryDevTeams.Swap(team.Name, newTeam.Name);
			autoDevWorkItem.SupportTeams.Swap(team.Name, newTeam.Name);
			autoDevWorkItem.MarketingTeams.Swap(team.Name, newTeam.Name);
			autoDevWorkItem.PostMarketingTeams.Swap(team.Name, newTeam.Name);
			autoDevWorkItem.RefreshTeams();
		}
		foreach (KeyValuePair<string, HashSet<string>> keyValuePair in this.TeamDefaults.ToList<KeyValuePair<string, HashSet<string>>>())
		{
			if (keyValuePair.Value.Contains(team.Name))
			{
				this.TeamDefaults[keyValuePair.Key] = (from x in keyValuePair.Value
				select (!x.Equals(team.Name)) ? x : newTeam.Name).ToHashSet<string>();
			}
		}
	}

	public void AddServer(Server server)
	{
		this.RemoveServer(server);
		if (this.MainServers.ContainsKey(server.ServerName))
		{
			server.ServerName = this.GenerateServerName();
			return;
		}
		this.MainServers[server.ServerName] = server;
		if (HUD.Instance != null && HUD.Instance.serverWindow != null)
		{
			HUD.Instance.serverWindow.UpdateServerList();
		}
	}

	public string GenerateServerName()
	{
		string[] serverNames = GameSettings.Instance.GetServerNames();
		string text = "Server 0";
		int num = serverNames.Length;
		while (serverNames.Contains(text))
		{
			text = "Server " + num;
			num++;
		}
		return text;
	}

	public void ChangeServerName(string oldName, string newName)
	{
		Server server = this.GetServer(oldName);
		if (server != null)
		{
			List<IServerItem> list = server.Items.ToList<IServerItem>();
			server.ServerName = newName;
			list.ForEach(delegate(IServerItem x)
			{
				this.RegisterWithServer(server.ServerName, x, true);
			});
			if (HUD.Instance != null && HUD.Instance.serverWindow != null)
			{
				HUD.Instance.serverWindow.UpdateServerList();
			}
		}
	}

	public void RemoveServer(Server server)
	{
		foreach (KeyValuePair<string, IServerHost> keyValuePair in this.MainServers.ToList<KeyValuePair<string, IServerHost>>())
		{
			if (keyValuePair.Value.Equals(server))
			{
				this.MainServers.Remove(keyValuePair.Key);
			}
			else if (keyValuePair.Value.Fallback != null && keyValuePair.Value.Fallback.Equals(server))
			{
				keyValuePair.Value.Fallback = null;
			}
		}
		if (HUD.Instance != null && HUD.Instance.serverWindow != null)
		{
			HUD.Instance.serverWindow.UpdateServerList();
		}
	}

	public string[] GetServerNames()
	{
		return this.MainServers.Keys.ToArray<string>();
	}

	public IServerHost[] GetAllServers()
	{
		return this.MainServers.Values.ToArray<IServerHost>();
	}

	public Dictionary<string, IServerHost> GetAllServersQuick()
	{
		return this.MainServers;
	}

	public Server[] GetPhysicalServers()
	{
		return this.MainServers.Values.OfType<Server>().ToArray<Server>();
	}

	public void ValidateServer(Server server)
	{
		if (this.MainServers.ContainsValue(server))
		{
			string key = this.MainServers.First((KeyValuePair<string, IServerHost> x) => x.Value == server).Key;
			if (!key.Equals(server.ServerName))
			{
				this.ChangeServerName(key, server.ServerName);
			}
		}
	}

	public void DeregisterServerItem(IServerItem item)
	{
		if (this.UnsupportedServerItems.Contains(item))
		{
			this.UnsupportedServerItems.Remove(item);
			if (HUD.Instance != null)
			{
				HUD.Instance.serverWindow.UpdateServerWarning();
			}
		}
		else
		{
			foreach (IServerHost serverHost in this.MainServers.Values)
			{
				if (serverHost.Items.Contains(item))
				{
					serverHost.Items.Remove(item);
					break;
				}
			}
		}
		if (HUD.Instance != null && HUD.Instance.serverWindow != null)
		{
			HUD.Instance.serverWindow.UpdateServerList();
		}
		item.SerializeServer(null);
	}

	public void RegisterWithServer(string name, IServerItem item, bool updateGUI = true)
	{
		if (this.UnsupportedServerItems.Contains(item))
		{
			this.UnsupportedServerItems.Remove(item);
			if (HUD.Instance != null)
			{
				HUD.Instance.serverWindow.UpdateServerWarning();
			}
		}
		else
		{
			foreach (IServerHost serverHost in from x in this.MainServers.Values
			where x != null
			select x)
			{
				if (serverHost.Items.Contains(item))
				{
					if (serverHost.ServerName.Equals(name))
					{
						item.SerializeServer(name);
						return;
					}
					serverHost.Items.Remove(item);
					break;
				}
			}
		}
		if (name == null || !this.MainServers.ContainsKey(name))
		{
			if (!item.CancelOnUnload())
			{
				this.UnsupportedServerItems.Add(item);
				if (HUD.Instance != null)
				{
					HUD.Instance.serverWindow.UpdateServerWarning();
				}
			}
			item.SerializeServer(null);
		}
		else
		{
			this.MainServers[name].Items.Add(item);
			item.SerializeServer(name);
		}
		if (updateGUI && HUD.Instance != null && HUD.Instance.serverWindow != null)
		{
			HUD.Instance.serverWindow.UpdateItems();
		}
	}

	public void ChangeServerRep(string serverName, Server server)
	{
		this.MainServers[serverName] = server;
		if (HUD.Instance != null && HUD.Instance.serverWindow != null)
		{
			HUD.Instance.serverWindow.UpdateServerList();
		}
	}

	public static float GameSpeed
	{
		get
		{
			return GameSettings._gameSpeed;
		}
		set
		{
			if (GameSettings.GameSpeed == value)
			{
				return;
			}
			if (!GameSettings.ForcePause)
			{
				GameSettings._gameSpeed = value;
			}
			GameSettings.lastGameSpeed = value;
			int gameSpeed = HUD.Instance.GameSpeed;
			HUD.Instance.UpdateBorderOverlay();
			if (gameSpeed > -1 && HUD.Instance != null)
			{
				for (int i = 0; i < 4; i++)
				{
					HUD.Instance.SpeedToggles[i].isOn = (gameSpeed == i);
				}
			}
		}
	}

	public static void ResetForcePause()
	{
		GameSettings._forcePause = 0;
	}

	public static bool ForcePause
	{
		get
		{
			return GameSettings._forcePause > 0;
		}
		set
		{
			bool forcePause = GameSettings.ForcePause;
			GameSettings._forcePause = Mathf.Max(0, GameSettings._forcePause + ((!value) ? -1 : 1));
			if (!value)
			{
				GameSettings.FreezeGame = false;
			}
			if (forcePause == GameSettings.ForcePause)
			{
				return;
			}
			if (GameSettings.ForcePause)
			{
				GameSettings.Instance.wasSkipping = (TimeOfDay.Instance.IsSkipping || GameSettings.Instance.wasSkipping);
			}
			else if (GameSettings.Instance != null && GameSettings.Instance.wasSkipping)
			{
				GameSettings.Instance.wasSkipping = false;
				TimeOfDay.Instance.SkipTime();
			}
			if (GameSettings.ForcePause)
			{
				GameSettings.lastGameSpeed = GameSettings._gameSpeed;
				if (GameSettings._gameSpeed > 0f)
				{
					UISoundFX.PlaySFX("Pause", -1f, 0f);
				}
				GameSettings._gameSpeed = 0f;
			}
			else
			{
				if (GameSettings._gameSpeed != GameSettings.lastGameSpeed && GameSettings.lastGameSpeed > 0f)
				{
					UISoundFX.PlaySFX("NormalSpeed", -1f, 0f);
				}
				GameSettings._gameSpeed = GameSettings.lastGameSpeed;
			}
			if (HUD.Instance != null)
			{
				HUD.Instance.disableSpeedPanel = true;
				for (int i = 0; i < 4; i++)
				{
					HUD.Instance.SpeedToggles[i].isOn = (HUD.Instance.GameSpeed == i);
				}
				HUD.Instance.disableSpeedPanel = false;
				HUD.Instance.UpdateBorderOverlay();
			}
		}
	}

	private void OnDestroy()
	{
		if (this.Founder != null)
		{
			UnityEngine.Object.Destroy(this.Founder.gameObject);
		}
		if (GameSettings.Instance == this)
		{
			GameSettings.Instance = null;
		}
	}

	public int GetWindowID()
	{
		this.WindowIDCounter++;
		return this.WindowIDCounter;
	}

	public uint GetWorkItemID()
	{
		this.WorkItemIDCounter += 1u;
		return this.WorkItemIDCounter;
	}

	public Color GetDefaultColor(string key, Color def)
	{
		return this.ColorDefaults.GetOrDefault(key, def);
	}

	public string GetDefaultStyle(string key, string def)
	{
		return this.StyleDefaults.GetOrDefault(key, def);
	}

	public string CompareObjects(object obj1, object obj2, string fieldName, string typeName)
	{
		if (obj1 == null && obj2 == null)
		{
			return null;
		}
		if (obj1 == null)
		{
			return string.Concat(new string[]
			{
				"Got different null values with ",
				obj2.ToString(),
				" for field ",
				fieldName,
				" in type ",
				typeName
			});
		}
		if (obj2 == null)
		{
			return string.Concat(new string[]
			{
				"Got different null values with ",
				obj1.ToString(),
				" for field ",
				fieldName,
				" in type ",
				typeName
			});
		}
		if (this.Cached.Contains(obj1))
		{
			return null;
		}
		this.Cached.Add(obj1);
		Type type = obj1.GetType();
		Type type2 = obj2.GetType();
		if (type != type2)
		{
			return string.Concat(new string[]
			{
				"Got 2 types ",
				type.Name,
				" ",
				type2.Name,
				" for field ",
				fieldName
			});
		}
		if (type.IsArray)
		{
			Array array = (Array)obj1;
			Array array2 = (Array)obj2;
			if (array.Rank != array2.Rank)
			{
				return "Got different ranks for array field " + fieldName + " in type " + typeName;
			}
			for (int i = 0; i < array.Rank; i++)
			{
				if (array.GetLength(i) != array2.GetLength(i))
				{
					return "Got different array lengths for field " + fieldName + " in type " + typeName;
				}
			}
			IEnumerator enumerator = array.GetEnumerator();
			IEnumerator enumerator2 = array2.GetEnumerator();
			while (enumerator.MoveNext())
			{
				enumerator2.MoveNext();
				string text = this.CompareObjects(enumerator.Current, enumerator2.Current, fieldName, typeName);
				if (text != null)
				{
					return text;
				}
			}
			return null;
		}
		else if (type.GetInterface("System.Collections.IList") != null)
		{
			IList list = (IList)obj1;
			IList list2 = (IList)obj2;
			if (list.Count != list2.Count)
			{
				return "Got different List lengths for field " + fieldName + " in type " + typeName;
			}
			for (int j = 0; j < list.Count; j++)
			{
				string text2 = this.CompareObjects(list[j], list2[j], fieldName, typeName);
				if (text2 != null)
				{
					return text2;
				}
			}
			return null;
		}
		else if (type.GetInterface("System.Collections.IDictionary") != null)
		{
			IDictionary dictionary = (IDictionary)obj1;
			IDictionary dictionary2 = (IDictionary)obj2;
			if (dictionary.Count != dictionary2.Count)
			{
				return "Got different List lengths for field " + fieldName + " in type " + typeName;
			}
			IDictionaryEnumerator enumerator3 = dictionary.GetEnumerator();
			IDictionaryEnumerator enumerator4 = dictionary2.GetEnumerator();
			while (enumerator3.MoveNext())
			{
				enumerator4.MoveNext();
				string text3 = this.CompareObjects(enumerator3.Key, enumerator4.Key, fieldName + " key", typeName);
				if (text3 != null)
				{
					return text3;
				}
				text3 = this.CompareObjects(enumerator3.Value, enumerator4.Value, fieldName + " value", typeName);
				if (text3 != null)
				{
					return text3;
				}
			}
			return null;
		}
		else if (type == typeof(string))
		{
			if (!obj1.Equals(obj2))
			{
				return string.Concat(new object[]
				{
					"Got different values ",
					obj1,
					" and ",
					obj2,
					" for field ",
					fieldName,
					" in type ",
					typeName
				});
			}
			return null;
		}
		else
		{
			if (!type.IsPrimitive)
			{
				foreach (FieldInfo fieldInfo in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
				{
					object value = fieldInfo.GetValue(obj1);
					object value2 = fieldInfo.GetValue(obj2);
					string text4 = this.CompareObjects(value, value2, fieldInfo.Name, type.Name);
					if (text4 != null)
					{
						return text4;
					}
				}
				return null;
			}
			if (!obj1.Equals(obj2))
			{
				return string.Concat(new string[]
				{
					"Got different values ",
					obj1.ToString(),
					" and ",
					obj2.ToString(),
					" for field ",
					fieldName,
					" in type ",
					typeName
				});
			}
			return null;
		}
	}

	public void InitPlots(bool color)
	{
		if (color)
		{
			this.RecolorPlots(this.PlotColors.Take(this.PlotColors.Length - 1).ToArray<Color>(), this.PlotColors[this.PlotColors.Length - 1]);
		}
		foreach (PlotArea plotArea in this.GetPlots())
		{
			GameObject gameObject = plotArea.CreateObject(this.PlotPrefab);
			gameObject.transform.SetParent(this.PlotHolder, false);
		}
	}

	public List<UndoObject.UndoAction> BuyPlot(PlotArea plot)
	{
		if (!plot.PlayerOwned)
		{
			plot.PlayerOwned = true;
			plot.AddonCost = 0f;
			this.Plots.Remove(plot);
			this.PlayerPlots.Add(plot);
			RoadManager.Instance.UpdateParkingAvailibility();
			return RoadManager.Instance.UpdateScrapers();
		}
		return null;
	}

	public List<UndoObject.UndoAction> SellPlot(PlotArea plot)
	{
		if (plot.PlayerOwned)
		{
			plot.PlayerOwned = false;
			this.PlayerPlots.Remove(plot);
			this.Plots.Add(plot);
			HashSet<Room> rooms = this.sRoomManager.Rooms.ToHashSet<Room>();
			List<Room> list = new List<Room>();
			foreach (Room room in this.sRoomManager.Rooms)
			{
				if (!this.PlayerOwnedArea((from x in room.Edges
				select x.Pos).ToList<Vector2>()))
				{
					rooms.Remove(room);
					list.Add(room);
				}
			}
			for (int i = 1; i < GameSettings.MaxFloor; i++)
			{
				int tI = i;
				foreach (Room room2 in from x in this.sRoomManager.Rooms
				where x.Floor == tI && rooms.Contains(x)
				select x)
				{
					if (!room2.IsSupported(rooms))
					{
						rooms.Remove(room2);
						list.Add(room2);
					}
				}
			}
			HashSet<Furniture> hashSet = new HashSet<Furniture>();
			HashSet<RoomSegment> segments = new HashSet<RoomSegment>();
			List<UndoObject.UndoAction> list2 = new List<UndoObject.UndoAction>();
			List<UndoObject.UndoAction> list3 = new List<UndoObject.UndoAction>();
			foreach (Room room3 in list)
			{
				list3.Add(new UndoObject.UndoAction(room3, false, 0f));
				List<RoomSegment> segments2 = room3.GetSegments(rooms);
				list2.AddRange(from z in segments2
				where !segments.Contains(z)
				select new UndoObject.UndoAction(z, false));
				segments.AddRange(segments2);
				hashSet.AddRange(room3.GetFurnitures());
				UnityEngine.Object.Destroy(room3.gameObject);
			}
			list3.Reverse();
			list3.AddRange(list2);
			list3.AddRange(from z in hashSet
			orderby z.GetSnappingDepth()
			select new UndoObject.UndoAction(z, false));
			RoadManager.Instance.UpdateParkingAvailibility();
			return list3;
		}
		return null;
	}

	public static void GlassOpaqueChange()
	{
		if (GameSettings.Instance != null)
		{
			Furniture.UpdateEdgeDetection();
			for (int i = 0; i < GameSettings.Instance.sRoomManager.Rooms.Count; i++)
			{
				Room room = GameSettings.Instance.sRoomManager.Rooms[i];
				room.UpdateSurrounded();
				room.UpdateVisibility();
			}
		}
	}

	public Rect PlotRect()
	{
		if (this.PlayerPlots.Count == 0)
		{
			return new Rect(0f, 0f, 0f, 0f);
		}
		float num = float.MaxValue;
		float num2 = float.MaxValue;
		float num3 = float.MinValue;
		float num4 = float.MinValue;
		for (int i = 0; i < this.PlayerPlots.Count; i++)
		{
			PlotArea plotArea = this.PlayerPlots[i];
			for (int j = 0; j < plotArea.Polygon.Length; j++)
			{
				Vector2 vector = plotArea.Polygon[j];
				num = Math.Min(num, vector.x);
				num2 = Math.Min(num2, vector.y);
				num3 = Math.Max(num3, vector.x);
				num4 = Math.Max(num4, vector.y);
			}
		}
		return Rect.MinMaxRect(num, num2, num3, num4);
	}

	public Rect BuildingRect()
	{
		if (this.sRoomManager.Rooms.Count == 0)
		{
			return new Rect(0f, 0f, 0f, 0f);
		}
		float num = float.MaxValue;
		float num2 = float.MaxValue;
		float num3 = float.MinValue;
		float num4 = float.MinValue;
		for (int i = 0; i < this.sRoomManager.Rooms.Count; i++)
		{
			Room room = this.sRoomManager.Rooms[i];
			for (int j = 0; j < room.Edges.Count; j++)
			{
				WallEdge wallEdge = room.Edges[j];
				num = Math.Min(num, wallEdge.Pos.x);
				num2 = Math.Min(num2, wallEdge.Pos.y);
				num3 = Math.Max(num3, wallEdge.Pos.x);
				num4 = Math.Max(num4, wallEdge.Pos.y);
			}
		}
		return Rect.MinMaxRect(num - 1f, num2 - 1f, num3 + 1f, num4 + 1f);
	}

	public bool PlayerOwnedPoint(Vector2 p)
	{
		for (int i = 0; i < this.PlayerPlots.Count; i++)
		{
			if (this.PlayerPlots[i].IsInside(p))
			{
				return true;
			}
		}
		return false;
	}

	public bool PlayerOwnedLine(Vector2 a, Vector2 b)
	{
		Vector2 a2 = b - a;
		float magnitude = a2.magnitude;
		a2 *= 1f / magnitude;
		for (float num = 0.5f; num < magnitude; num += 0.5f)
		{
			if (!this.PlayerOwnedPoint(a + a2 * num))
			{
				return false;
			}
		}
		return true;
	}

	public bool PlayerOwnedArea(IList<Vector2> area)
	{
		for (int i = 0; i < area.Count; i++)
		{
			Vector2 vector = area[i];
			if (!this.PlayerOwnedPoint(vector))
			{
				return false;
			}
			Vector2 b = area[(i + 1) % area.Count];
			if (!this.PlayerOwnedLine(vector, b))
			{
				return false;
			}
		}
		Triangulator triangulator = new Triangulator(area);
		int[] array = triangulator.Triangulate();
		Vector2[] array2 = new Vector2[3];
		for (int j = 0; j < array.Length; j += 3)
		{
			array2[0] = area[array[j]];
			array2[1] = area[array[j + 1]];
			array2[2] = area[array[j + 2]];
			Vector2 triangleCentroid = Utilities.GetTriangleCentroid(array2);
			if (!this.PlayerOwnedPoint(triangleCentroid))
			{
				return false;
			}
		}
		return true;
	}

	public IEnumerable<PlotArea> GetPlots()
	{
		for (int i = 0; i < this.Plots.Count; i++)
		{
			PlotArea plot = this.Plots[i];
			yield return plot;
		}
		for (int j = 0; j < this.PlayerPlots.Count; j++)
		{
			PlotArea plot2 = this.PlayerPlots[j];
			yield return plot2;
		}
		yield break;
	}

	private void Start()
	{
		GameSettings.DaysPerMonth = GameData.DaysPerMonth;
		this.LeaveMat = new Material(this.LeaveMat);
		this.TreeRoot = new GameObject("Trees");
		GameSettings.ForcePause = false;
		GameSettings.GameSpeed = 1f;
		GameSettings.Instance = this;
		this.SoftwareTypes = GameData.AllSoftwareTypes(null).ToDictionary((SoftwareType x) => x.Name);
		string[][] specialization = GameData.GetSpecialization(this.SoftwareTypes.Values.ToArray<SoftwareType>());
		this.CodeSpecializations = specialization[1];
		this.ArtSpecializations = specialization[2];
		this.Specializations = specialization[0];
		this.NeverUsed = this.SoftwareTypes.Values.SelectMany((SoftwareType x) => from z in x.Categories.Values
		select new KeyValuePair<SoftwareType, SoftwareCategory>(x, z)).ToDictionary((KeyValuePair<SoftwareType, SoftwareCategory> x) => new KeyValuePair<string, string>(x.Key.Name, x.Value.Name), (KeyValuePair<SoftwareType, SoftwareCategory> x) => (from z in x.Key.Features.Values
		where !z.Forced && !z.Base
		select z.Name).ToSHashSet<string>());
		this.CompanyTypes = GameData.AllCompanyTypes().ToDictionary((CompanyType x) => x.Name);
		this.RNG = GameData.MergeGenerators(GameData.AllNameGenerators(null));
		this.Personalities = GameData.AllPersonalities();
		GameData.ModPackages.ForEach(delegate(ModPackage x)
		{
			x.Enabled = false;
		});
		this.sRoomManager = new RoomManager();
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.RoomObject);
		Room component = gameObject.GetComponent<Room>();
		component.Init(null, 0, null, true, false);
		component.CanSelect = false;
		this.sRoomManager.Outside = component;
		this.sRoomManager.RoomNearnessDirty = true;
		this.EditMode = GameData.EditMode;
		if (this.EditMode && !GameData.LoadAnyOnLoad)
		{
			this.RentMode = true;
		}
		TimeOfDay.Instance.InitYear(GameData.ActiveYear);
		if (!GameData.LoadBuildingOnLoad)
		{
			this.EnvType = GameData.Environment;
			this.CliType = GameData.Climate;
			TimeOfDay.Instance.CurrentWeather = this.WeatherPresets[(int)this.CliType];
			for (int i = 0; i < TimeOfDay.Instance.CurrentWeather.Critters.Length; i++)
			{
				CritterController.Instance.PopulateCritter(TimeOfDay.Instance.CurrentWeather.Critters[i], TimeOfDay.Instance.CurrentWeather.CritterCount[i]);
			}
			TimeOfDay.Instance.UpdateExtraLayerColor();
			TimeOfDay.Instance.RunUpdate();
			UISoundFX.ChangeMusicState("Spring");
		}
		this.tTreeTrunkMat = new Material((this.CliType != GameData.ClimateType.Warm) ? this.TreeTrunkMat : this.CactusTrunkMat);
		this.sActorManager = new ActorManager();
		this.simulation = new MarketSimulation(!this.EditMode, new SDateTime(1970));
		this.MyCompany = new Company(GameData.CompanyName, GameData.StartingMoney, SDateTime.Now(), false);
		this.MyCompany.RepEffects = new Dictionary<string, Company.RepEffectItem>();
		this.MyCompany.Player = true;
		LoanWindow.TakeLoan(ActorCustomization.StartLoanMonths, GameData.LoanAmount / (int)LoanWindow.factor);
		GameData.LoanAmount = 0;
		if (!GameData.LoadCompanyOnLoad && !this.EditMode)
		{
			GameSettings.IsQuitting = false;
			this._simThread = new Thread(new ThreadStart(this.InitSimThread));
			this._simThread.Start();
			this.CompanyBenefits = EmployeeBenefit.GetDefaultBenefits();
			HUD.Instance.contractWindow.UpdateContracts(TimeOfDay.Instance.GetDate(true));
			this.Difficulty = GameData.SelectedDifficulty;
		}
		else
		{
			TimeOfDay.Instance.DisableSunUpdate = false;
		}
		this.simulation.AddCompany(this.MyCompany);
		if (GameData.founder == null)
		{
			Employee employee = new Employee(SDateTime.Now(), Employee.EmployeeRole.Programmer, UnityEngine.Random.value > 0.5f, Employee.WageBracket.High, this.Personalities, true, null, null, null, 1f, 0.1f);
			Actor actor = this.SpawnActor(employee.Female);
			actor.employee = employee;
		}
		else
		{
			GameData.founder.enabled = true;
			this.Founder = GameData.founder;
			GameData.founder = null;
		}
		if (!GameData.LoadBuildingOnLoad)
		{
			this.CachedTrees = TimeOfDay.Instance.CurrentWeather.Trees;
			this.RNDString = GameData.RandomString;
			GameData.InitRND();
			if (this.EnvType == GameData.EnvironmentType.Rural && !this.EditMode)
			{
				this.Plots = PlotArea.GenerateRandom(GameData.RuralBigPlots);
				this.InitPlots(true);
				this.Plots[0].Price = PlotArea.StartPlotPrice;
				this.BuyPlot(this.Plots[0]);
			}
			if (this.EnvType == GameData.EnvironmentType.Rural)
			{
				this.SpawnTrees();
			}
		}
		BuildController.Instance.ResetGrid();
		if (this.EditMode)
		{
			this.MyCompany.InfiniteMoney();
			HUD.Instance.BuildMode = true;
			int num = Mathf.FloorToInt(this.BusStopSign.transform.position.z);
			this.PlayerPlots.Add(new PlotArea(new PlotArea.PlotPoint[]
			{
				new PlotArea.PlotPoint(248f, 8f),
				new PlotArea.PlotPoint(248f, 248f),
				new PlotArea.PlotPoint(8f, 248f),
				new PlotArea.PlotPoint(8f, (float)(num + 3)),
				new PlotArea.PlotPoint(9f, (float)(num + 3)),
				new PlotArea.PlotPoint(9f, (float)(num - 6)),
				new PlotArea.PlotPoint(8f, (float)(num - 6)),
				new PlotArea.PlotPoint(8f, 8f)
			}));
			this.PlayerPlots[0].PlayerOwned = true;
			this.InitPlots(false);
			TimeOfDay.Instance.DisableSunUpdate = false;
		}
	}

	private void RecolorPlots(Color[] colors, Color defCol)
	{
		List<int> list = new List<int>();
		Dictionary<PlotArea, List<PlotArea>> dictionary = new Dictionary<PlotArea, List<PlotArea>>();
		List<PlotArea> list2 = this.GetPlots().ToList<PlotArea>();
		for (int i = 0; i < list2.Count; i++)
		{
			PlotArea plotArea = list2[i];
			for (int j = i + 1; j < list2.Count; j++)
			{
				PlotArea p = list2[j];
				if (plotArea.Points.Any((KeyValuePair<PlotArea.PlotPoint, PlotArea.PointLink> x) => p.Points.ContainsKey(x.Key)))
				{
					dictionary.Append(plotArea, p);
					dictionary.Append(p, plotArea);
				}
			}
		}
		Dictionary<PlotArea, int> dictionary2 = new Dictionary<PlotArea, int>();
		foreach (PlotArea plotArea2 in from x in dictionary
		orderby x.Value.Count descending
		select x.Key)
		{
			list.AddRange(Enumerable.Range(0, colors.Length));
			using (Dictionary<PlotArea, int>.Enumerator enumerator2 = dictionary2.GetEnumerator())
			{
				while (enumerator2.MoveNext())
				{
					KeyValuePair<PlotArea, int> p = enumerator2.Current;
					if (plotArea2.Points.Any((KeyValuePair<PlotArea.PlotPoint, PlotArea.PointLink> x) => p.Key.Points.ContainsKey(x.Key)))
					{
						list.Remove(p.Value);
					}
				}
			}
			if (list.Count > 0)
			{
				int random = list.GetRandom<int>();
				dictionary2[plotArea2] = random;
				plotArea2.PlotColor = colors[random];
			}
			else
			{
				plotArea2.PlotColor = defCol;
			}
			list.Clear();
		}
	}

	public void RefreshTreeTree()
	{
		this.TreeTree = new QuadTree<global::TreeInstance>(new Rect(0f, 0f, 256f, 256f), 4f, 4, 4);
		foreach (global::TreeInstance treeInstance in this.Trees)
		{
			this.TreeTree.Insert(treeInstance);
		}
	}

	public void OptimizeTrees()
	{
		this.RefreshTreeTree();
	}

	public void SimulateWork(SDateTime start, float deltaInMinutes)
	{
		for (int i = 0; i < this.ReviewJobs.Count; i++)
		{
			this.ReviewJobs[i].Tick(deltaInMinutes);
		}
		for (int j = 0; j < this.FollowerSimulation.Count; j++)
		{
			this.FollowerSimulation[j].SimulateFollowers(deltaInMinutes);
		}
		for (int k = 0; k < this.ProductPrinters.Count; k++)
		{
			this.ProductPrinters[k].Tick(deltaInMinutes);
		}
		if (this.MyCompany != null)
		{
			bool flag = deltaInMinutes > 10f;
			float num = deltaInMinutes;
			bool flag2 = !flag;
			bool flag3 = flag || (start.Hour >= 8 && start.Hour < 16);
			foreach (WorkItem workItem in this.MyCompany.WorkItems.ToList<WorkItem>())
			{
				AutoDevWorkItem autoDevWorkItem = workItem as AutoDevWorkItem;
				if (autoDevWorkItem != null)
				{
					if (!autoDevWorkItem.Hidden)
					{
						try
						{
							autoDevWorkItem.Update();
						}
						catch (Exception exception)
						{
							Debug.LogException(exception);
						}
					}
				}
				else if (flag3 && !workItem.Paused && workItem.CompanyWorker != null && workItem.HasCompanyWork())
				{
					if (!flag2)
					{
						flag2 = true;
						if (start.Hour < 8 || start.Hour >= 16)
						{
							flag3 = false;
						}
						else
						{
							num = Mathf.Min(deltaInMinutes, (float)(960 - start.Hour * 60 - start.Minute));
							flag3 = (num > 0f);
						}
					}
					if (flag3)
					{
						if (workItem.CompanyWorker.DoWork(Utilities.PerDay(workItem.StressMultiplier() / 2f, num, false)))
						{
							float num2 = workItem.CompanyWork(num);
							workItem.AddCost(num2);
							workItem.CompanyWorker.WorkItemCost += num2;
						}
						else
						{
							HUD.Instance.AddPopupMessage("SubsidiaryOverworked".Loc(new object[]
							{
								workItem.CompanyWorker.Name
							}), "Exclamation", PopupManager.PopUpAction.OpenCompanyDetails, workItem.CompanyWorker.ID, PopupManager.NotificationSound.Warning, 0.5f, PopupManager.PopupIDs.SubsidiaryOverwork, 1);
						}
					}
				}
			}
		}
	}

	private void FixedUpdate()
	{
		if (GameSettings.GameSpeed > 0f)
		{
			this.SimulateWork(SDateTime.Now(), Time.deltaTime * GameSettings.GameSpeed);
		}
	}

	public void UpdateGridVisibility()
	{
		foreach (KeyValuePair<int, MeshFilter> keyValuePair in this.FloorRentGrids)
		{
			keyValuePair.Value.gameObject.SetActive(HUD.Instance.BuildMode && keyValuePair.Key == this.ActiveFloor);
		}
	}

	private void Update()
	{
		AudioManager.MasterMixer.SetFloat("MasterPitch", (GameSettings.GameSpeed != 0f) ? Mathf.Min(3f, 0.98f + GameSettings.GameSpeed / 50f) : 1f);
		if (this.UndoAutosaveWait >= 0f)
		{
			this.UndoAutosaveWait -= Time.deltaTime;
		}
		foreach (global::TreeInstance treeInstance in this.TempTrees)
		{
			StaticTree treeMesh = treeInstance.TreeMesh;
			Graphics.DrawMesh(treeMesh.Trunk.sharedMesh, treeInstance.Transform, this.tTreeTrunkMat, 0);
			if (treeMesh.Leaves != null)
			{
				Graphics.DrawMesh(treeMesh.Leaves.sharedMesh, treeInstance.Transform, this.LeaveMat, 0);
			}
		}
		if (this.DirtyRentGrid.Count > 0)
		{
			using (HashSet<int>.Enumerator enumerator2 = this.DirtyRentGrid.GetEnumerator())
			{
				while (enumerator2.MoveNext())
				{
					int i = enumerator2.Current;
					bool flag = false;
					MeshFilter meshFilter = this.FloorRentGrids.GetOrNull(i);
					if (meshFilter == null)
					{
						flag = true;
						GameObject gameObject = new GameObject("RentGridFloor" + i);
						gameObject.transform.position = Vector3.up * ((float)i * 2f + 0.03f);
						meshFilter = gameObject.AddComponent<MeshFilter>();
						meshFilter.sharedMesh = new Mesh();
						MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
						meshRenderer.sharedMaterial = BuildController.Instance.MainGridMaterial;
						this.FloorRentGrids[i] = meshFilter;
					}
					List<Room> list = (from x in this.sRoomManager.Rooms
					where x.Floor == i && x.PlayerOwned
					select x).ToList<Room>();
					if (list.Count > 0)
					{
						List<Vector3> list2 = new List<Vector3>();
						List<int> list3 = new List<int>();
						for (int i2 = 0; i2 < list.Count; i2++)
						{
							Room room = list[i2];
							Mesh sharedMesh = room.FloorMesh.GetComponent<MeshFilter>().sharedMesh;
							int c = list2.Count;
							list2.AddRange(sharedMesh.vertices);
							list3.AddRange(from x in sharedMesh.triangles
							select x + c);
						}
						Mesh sharedMesh2 = meshFilter.sharedMesh;
						if (!flag)
						{
							sharedMesh2.triangles = new int[0];
						}
						sharedMesh2.vertices = list2.ToArray();
						sharedMesh2.normals = list2.SelectInPlace((Vector3 x) => Vector3.up);
						Mesh mesh = sharedMesh2;
						IList<Vector3> arr = list2;
						if (GameSettings.<>f__mg$cache0 == null)
						{
							GameSettings.<>f__mg$cache0 = new Func<Vector3, Vector2>(Utilities.FlattenVector3);
						}
						mesh.uv = arr.SelectInPlace(GameSettings.<>f__mg$cache0);
						sharedMesh2.triangles = list3.ToArray();
					}
					else
					{
						meshFilter.sharedMesh.Clear();
					}
				}
			}
			this.DirtyRentGrid.Clear();
			this.UpdateGridVisibility();
		}
		for (int j = 0; j < this.TreeBatches.Count; j++)
		{
			TreeBatch treeBatch = this.TreeBatches[j];
			if (!treeBatch.GenerateMesh())
			{
				this.TreeBatches.RemoveAt(j);
				UnityEngine.Object.Destroy(treeBatch.gameObject);
				j--;
			}
		}
		if (HUD.Instance.serverWindow.ShouldUpdateServerList)
		{
			HUD.Instance.serverWindow.DoUpdateServerList();
		}
		if (this.HasToFinalizeTimers)
		{
			TimeProbe.FinalizeTime("Server init time:");
			TimeProbe.FinalizeTime("Furniture init time:");
			this.HasToFinalizeTimers = false;
		}
		if (this.sRoomManager.RoomNearnessDirty)
		{
			this.sRoomManager.RecalculateNearRooms();
		}
		if (this.sRoomManager.TemperatureControlDirty)
		{
			this.sRoomManager.UpdateTemperatureControllers();
		}
		if (Room.UpdatePCNoisiness.Count > 0)
		{
			Room room2 = Room.UpdatePCNoisiness.First<Room>();
			HashList<Furniture> furniture = room2.GetFurniture("Computer");
			for (int k = 0; k < furniture.Count; k++)
			{
				Furniture furniture2 = furniture[k];
				if (furniture2 != null)
				{
					furniture2.EnvironmentNoise = Furniture.RecalculateNoise(furniture2.OriginalPosition.FlattenVector3(), false, furniture2.Parent, furniture2, null, false, false);
					furniture2.RefreshFinalNoiseValue();
				}
			}
			Room.UpdatePCNoisiness.Remove(room2);
		}
		if (Server.FixWiringNow.Count > 0)
		{
			HashSet<Server> visited = new HashSet<Server>();
			foreach (Server server in Server.FixWiringNow)
			{
				if (!(server == null) && !(server.gameObject == null))
				{
					server.Children.RemoveWhere((Server x) => x == null);
					server.FixWiringN(visited);
					foreach (Server server2 in server.Children)
					{
						if (!(server2 == null) && !(server2.gameObject == null))
						{
							server2.FixWiringN(visited);
						}
					}
				}
			}
			Server.FixWiringNow.Clear();
		}
		if (Server.CalculatePowerNow.Count > 0)
		{
			foreach (Server server3 in Server.CalculatePowerNow)
			{
				if (server3 != null && server3.gameObject != null)
				{
					server3.CalculatePowerN();
				}
			}
			Server.CalculatePowerNow.Clear();
		}
		if (CameraScript.Instance.FlyMode)
		{
			int floor = Mathf.FloorToInt(CameraScript.Instance.mainCam.transform.position.y / 2f);
			this.sRoomManager.CameraRoom = (this.sRoomManager.GetRoomFromPoint(floor, CameraScript.Instance.mainCam.transform.position.FlattenVector3(), true) ?? this.sRoomManager.Outside);
		}
		else
		{
			Ray ray = HUD.Instance.MainCamera.ScreenPointToRay(new Vector3((float)(Screen.width / 2), (float)(Screen.height / 2), 0f));
			Plane plane = new Plane(Vector3.up, Vector3.up * (float)this.ActiveFloor * 2f);
			float distance = 0f;
			plane.Raycast(ray, out distance);
			Vector3 point = ray.GetPoint(distance);
			this.sRoomManager.CameraRoom = (this.sRoomManager.GetRoomFromPoint(this.ActiveFloor, new Vector2(point.x, point.z), true) ?? this.sRoomManager.Outside);
		}
		this.HandleSimGUI();
		this.tTreeTrunkMat.color = DataOverlay.Instance.GetColor((this.CliType != GameData.ClimateType.Warm) ? this.TreeTrunkMat.color : this.CactusTrunkMat.color);
		if (!this.presimactive && GameSettings.GameSpeed > 0f)
		{
			this.InGameMinute += GameSettings.GameSpeed * Time.deltaTime;
			if (this.InGameMinute >= 1f)
			{
				this.InGameMinute = 0f;
				this.sActorManager.DoUpdate();
			}
		}
		for (int l = 0; l < this.MergedTreeTrunks.Count; l++)
		{
			this.MergedTreeTrunks[l].SetActive(this.ActiveFloor >= 0);
		}
		for (int m = 0; m < this.MergedLeaves.Count; m++)
		{
			this.MergedLeaves[m].SetActive(this.ActiveFloor >= 0);
		}
		if (GameSettings.GameSpeed > 0f)
		{
			this.sRoomManager.UpdateStates();
		}
		this.BusStopSign.SetActive(GameSettings.Instance.ActiveFloor > -1);
		if (HUD.Instance != null)
		{
			List<object> activeDealsPerformance = HUD.Instance.dealWindow.GetActiveDealsPerformance();
			for (int n = 0; n < activeDealsPerformance.Count; n++)
			{
				Deal deal = (Deal)activeDealsPerformance[n];
				if (deal != null && deal.Active)
				{
					deal.HandleUpdate();
				}
			}
		}
	}

	public void BatchTempTrees()
	{
		this.Trees.AddRange(this.TempTrees);
		for (int i = 0; i < this.TempTrees.Count; i++)
		{
			this.AddTreeToBatch(this.TempTrees[i]);
			this.TreeTree.Insert(this.TempTrees[i]);
		}
		this.TempTrees.Clear();
	}

	public global::TreeInstance AddTree(Vector3 pos, bool temp = false)
	{
		return this.CreateTree(GameData.RNDRange(0, this.CachedTrees.Length), pos, Quaternion.Euler(0f, (float)UnityEngine.Random.Range(0, 4) * 90f, 0f), temp);
	}

	private global::TreeInstance CreateTree(int id, Vector3 position, Quaternion rotation, bool temp = false)
	{
		global::TreeInstance treeInstance = new global::TreeInstance(position, rotation, id);
		if (temp)
		{
			this.TempTrees.Add(treeInstance);
		}
		else
		{
			this.Trees.Add(treeInstance);
			this.AddTreeToBatch(treeInstance);
		}
		return treeInstance;
	}

	private void AddTreeToBatch(global::TreeInstance tree)
	{
		float num = 32f;
		TreeBatch treeBatch = null;
		Vector2 pos = tree.GetPos();
		for (int i = 0; i < this.TreeBatches.Count; i++)
		{
			TreeBatch treeBatch2 = this.TreeBatches[i];
			float num2 = treeBatch2.Center.MinDist(pos);
			if (treeBatch2.CanAdd(tree) && num2 < num)
			{
				num = num2;
				treeBatch = treeBatch2;
			}
		}
		if (treeBatch == null)
		{
			treeBatch = UnityEngine.Object.Instantiate<TreeBatch>(this.TreeBatchPrefab);
			treeBatch.transform.SetParent(this.TreeRoot.transform);
			treeBatch.LeafMat = this.LeaveMat;
			treeBatch.TrunkMat = this.tTreeTrunkMat;
			this.TreeBatches.Add(treeBatch);
		}
		treeBatch.AddTree(tree);
	}

	public void SpawnTreeAreas(List<Rect> areas)
	{
		if (this.CachedTrees.Length == 0)
		{
			Debug.LogError("No cached trees to spawn");
			return;
		}
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		foreach (Rect area in areas)
		{
			this.SpawnTreeArea(area, null);
		}
		Debug.Log("TreeGen time: " + (Time.realtimeSinceStartup - realtimeSinceStartup));
	}

	public void SpawnTreeArea(Rect area, Vector2[] polygon)
	{
		float num = (this.CliType != GameData.ClimateType.Warm) ? 4f : 10f;
		float num2 = num / 2f - 0.5f;
		area = area.Expand(-2f, -2f);
		for (float num3 = area.xMin + num2 + 0.001f; num3 < area.xMax; num3 += num)
		{
			for (float num4 = area.yMin + num2 + 0.001f; num4 < area.yMax; num4 += num)
			{
				Vector2 vector = new Vector2(num3, num4);
				Vector2 a = vector;
				Vector2 vector2 = new Vector2((float)GameData.RNDRange(-1, 1), (float)GameData.RNDRange(-1, 1));
				vector = a + vector2.normalized * num2;
				if (area.Contains(vector) && (polygon == null || Utilities.IsInside(vector, polygon)))
				{
					this.CreateTree(GameData.RNDRange(0, this.CachedTrees.Length), vector.ToVector3(0f), Quaternion.Euler(0f, (float)UnityEngine.Random.Range(0, 4) * 90f, 0f), false);
				}
			}
		}
	}

	public void SpawnTreePolygon(Vector2[] polygon)
	{
		this.SpawnTreeArea(polygon.GetBounds(), polygon);
	}

	private void SpawnTrees()
	{
		if (this.CachedTrees.Length == 0)
		{
			Debug.LogError("No cached trees to spawn");
			return;
		}
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		Rect rect = new Rect(8f, 8f, 239f, 239f);
		float num = (this.CliType != GameData.ClimateType.Warm) ? 55f : 20f;
		float max = 1f / num * 254f;
		float num2 = 3f;
		Vector2 vector = new Vector2(GameData.RNDRange(0f, 10f), GameData.RNDRange(0f, 10f));
		for (float num3 = 0f; num3 < num; num3 += 1f)
		{
			for (float num4 = 0f; num4 < num; num4 += 1f)
			{
				Vector3 vector2 = new Vector3(rect.xMin + num3 / num * rect.width, 0f, rect.yMin + num4 / num * rect.height);
				vector2 += new Vector3(GameData.RNDRange(0f, max), 0f, GameData.RNDRange(0f, max));
				float time = Mathf.PerlinNoise(vector.x + num3 / num * num2, vector.y + num4 / num * num2);
				if (GameData.RNDValue < this.TreeFalloff.Evaluate(time))
				{
					this.CreateTree(GameData.RNDRange(0, this.CachedTrees.Length), vector2, Quaternion.Euler(0f, (float)UnityEngine.Random.Range(0, 4) * 90f, 0f), false);
				}
			}
		}
		Debug.Log("TreeGen time: " + (Time.realtimeSinceStartup - realtimeSinceStartup));
	}

	public Actor SpawnActor(bool female)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ActorObj);
		gameObject.GetComponent<Actor>().Female = female;
		return gameObject.GetComponent<Actor>();
	}

	private void HandleSimGUI()
	{
		if (this.presimactive)
		{
			if (!this.PreSimLoadPanel.activeSelf)
			{
				GameSettings.ForcePause = true;
				this.PreSimLoadPanel.SetActive(true);
			}
			if (!GameSettings.FreezeGame)
			{
				GameSettings.FreezeGame = true;
			}
			this.PreSimLoadText.text = this.presimloadtxt;
			this.PreSimSWText.text = this.presimswtext;
			this.PreSimBar.Value = this.presimloadbar;
		}
		else if (this.PreSimLoadPanel.activeSelf)
		{
			GameSettings.ForcePause = false;
			this.PreSimLoadPanel.SetActive(false);
			Newspaper.StoryRollover(this.presimfinaltime, true, false);
			HUD.Instance.SpeedToggles[1].isOn = true;
			HUD.Instance.dealWindow.CancelDueWork();
			HUD.Instance.eventWindow.UpdateEvents();
			HUD.Instance.UpdateFurnitureButtons();
		}
	}

	private void InitSimThread()
	{
		SDateTime sdateTime = new SDateTime(1970);
		try
		{
			Dictionary<string, string> translate = this.SoftwareTypes.ToDictionary((KeyValuePair<string, SoftwareType> x) => x.Key, (KeyValuePair<string, SoftwareType> x) => Localization.GetSoftware(x.Value)[0]);
			this.presimactive = true;
			DateTime now = DateTime.Now;
			SDateTime time = SDateTime.Now();
			int num = TimeOfDay.Instance.Year - sdateTime.Year;
			this.simulation.InitialReleases(sdateTime);
			for (int i = 0; i <= 12 * num; i++)
			{
				if (GameSettings.IsQuitting)
				{
					return;
				}
				this.presimloadtxt = sdateTime.RealYear.ToString();
				object[] array = new object[5];
				array[0] = "CompanyLoadMsg".Loc();
				array[1] = ": ";
				array[2] = this.simulation.CompanyCount - 1;
				array[3] = "\n";
				array[4] = string.Join("\n", (from x in this.simulation.GetAllProducts()
				group x by new KeyValuePair<string, string>(translate[x._type], x._category.LocSWC(x._type)) into x
				orderby x.Key.Key
				select string.Concat(new string[]
				{
					x.Key.Key,
					" - ",
					x.Key.Value,
					": ",
					x.Count<SoftwareProduct>().ToString()
				})).ToArray<string>());
				this.presimswtext = string.Concat(array);
				this.presimloadbar = (float)i / ((float)num * 12f);
				for (int j = 0; j < GameSettings.DaysPerMonth; j++)
				{
					if (i > 1 && this.SkipSimulation)
					{
						break;
					}
					object timeLock = TimeOfDay.Instance.TimeLock;
					lock (timeLock)
					{
						TimeOfDay.Instance.UpdateTime(sdateTime);
						this.simulation.SimulateMonth(sdateTime, true);
						TimeOfDay.Instance.UpdateTime(time);
					}
					if (i == 12 * num)
					{
						break;
					}
					sdateTime += new SDateTime(1, 0, 0);
				}
				foreach (Company company in this.simulation.GetAllCompanies())
				{
					if (company != this.MyCompany)
					{
						company.PayDividends();
					}
				}
				foreach (Company company2 in this.simulation.GetAllCompanies())
				{
					if (company2 != this.MyCompany)
					{
						company2.EndDay();
					}
				}
				if (i == 12 * num - 1)
				{
					Newspaper.StoryRollover(sdateTime, true, true);
				}
			}
			sdateTime += new SDateTime(0, 10, 0, 0);
			TimeOfDay.Instance.UpdateTime(sdateTime);
			TimeOfDay.Instance.DisableSunUpdate = false;
			Debug.Log("Market presim time: " + (DateTime.Now - now).TotalSeconds);
			this.presimfinaltime = sdateTime;
			this.presimactive = false;
			foreach (Actor actor in this.sActorManager.Actors)
			{
				this.sActorManager.AddToAwaiting(actor, new SDateTime(0, 7, sdateTime.Day + 1, sdateTime.Month, sdateTime.Year), true, false);
			}
		}
		catch (ThreadAbortException)
		{
		}
		catch (Exception ex)
		{
			ErrorLogging.AddException(ex);
			this.presimfinaltime = sdateTime;
			this.presimactive = false;
		}
	}

	public float NewFeatures(SoftwareProduct product, bool news = true)
	{
		SHashSet<string> shashSet = this.NeverUsed[new KeyValuePair<string, string>(product._type, product._category)];
		if (shashSet.Count > 0)
		{
			float num = 0f;
			for (int i = 0; i < product.Features.Length; i++)
			{
				string text = product.Features[i];
				if (shashSet.Contains(text))
				{
					shashSet.Remove(text);
					num += product.Type.Features[text].DevTime;
				}
			}
			return num;
		}
		return 0f;
	}

	public void Deserialize(WriteDictionary dictionary)
	{
		GameReader.LoadMode loadMode = dictionary.Get<GameReader.LoadMode>("SaveMode", GameReader.LoadMode.Full);
		this.Errors.AddRange(dictionary.Get<SHashSet<string>>("Errors", new SHashSet<string>()));
		this.RoomStyles.AddRange(dictionary.Get<List<RoomStyle>>("RoomStyles", new List<RoomStyle>()));
		this.DefaultIndoorRoomStyle = dictionary.Get<RoomStyle>("DefaultIndoorRoomStyle", this.DefaultIndoorRoomStyle);
		this.DefaultOutdoorRoomStyle = dictionary.Get<RoomStyle>("DefaultOutdoorRoomStyle", this.DefaultOutdoorRoomStyle);
		string autos = dictionary.Get<string>("Autosave", null);
		if (autos != null)
		{
			this.AssociatedAutoSave = SaveGameManager.SaveGames.FirstOrDefault((SaveGame x) => !x.Readonly && x.FilePath.Equals(autos));
		}
		if (loadMode == GameReader.LoadMode.Building || loadMode == GameReader.LoadMode.Full)
		{
			this.RentMode = dictionary.Get<bool>("RentMode", false);
			this.Plots = dictionary.Get<List<PlotArea>>("Plots", this.Plots);
			this.PlayerPlots = dictionary.Get<List<PlotArea>>("PlayerPlots", this.PlayerPlots);
			if (loadMode == GameReader.LoadMode.Building)
			{
				this.PlayerPlots.ForEach(delegate(PlotArea x)
				{
					x.MonthsLeft = 0;
				});
			}
			this.InitPlots(false);
			this.EnvType = dictionary.Get<GameData.EnvironmentType>("EnvType", GameData.EnvironmentType.Rural);
			this.CliType = dictionary.Get<GameData.ClimateType>("CliType", GameData.ClimateType.Temperate);
			TimeOfDay.Instance.CurrentWeather = this.WeatherPresets[(int)this.CliType];
			for (int i = 0; i < TimeOfDay.Instance.CurrentWeather.Critters.Length; i++)
			{
				CritterController.Instance.PopulateCritter(TimeOfDay.Instance.CurrentWeather.Critters[i], TimeOfDay.Instance.CurrentWeather.CritterCount[i]);
			}
			this.CachedTrees = TimeOfDay.Instance.CurrentWeather.Trees;
			List<global::TreeInstance> list = dictionary.Get<List<global::TreeInstance>>("Trees2", null);
			if (list == null)
			{
				this.SpawnTrees();
			}
			else
			{
				foreach (global::TreeInstance treeInstance in list)
				{
					this.CreateTree(treeInstance.Idx, treeInstance.Position, treeInstance.Rotation, false);
				}
			}
			TimeOfDay.Instance.UpdateExtraLayerColor();
			this.RoomGroups = dictionary.Get<Dictionary<string, RoomGroup>>("RoomGroups", new Dictionary<string, RoomGroup>());
			this.MainServers = dictionary.Get<Dictionary<string, uint>>("Server", new Dictionary<string, uint>()).ToDictionary((KeyValuePair<string, uint> x) => x.Key, (KeyValuePair<string, uint> x) => (IServerHost)Writeable.STGetDeserializedObject(x.Value));
			foreach (KeyValuePair<string, IServerHost> keyValuePair in this.MainServers.ToList<KeyValuePair<string, IServerHost>>())
			{
				if (keyValuePair.Value == null)
				{
					this.MainServers.Remove(keyValuePair.Key);
				}
			}
		}
		if (loadMode == GameReader.LoadMode.Company || loadMode == GameReader.LoadMode.Full)
		{
			if (dictionary.Contains("EmployeeTerminations"))
			{
				this.SerializedEvents = (dictionary["EmployeeTerminations"] as List<EmployeeTermination>);
			}
			GameSettings.DaysPerMonth = dictionary.Get<int>("DaysPerMonth", 1);
			SDateTime sdateTime = (SDateTime)dictionary["Time"];
			TimeOfDay.Instance.Minute = (float)sdateTime.Minute;
			TimeOfDay.Instance.Hour = sdateTime.Hour;
			TimeOfDay.Instance.Day = sdateTime.Day;
			TimeOfDay.Instance.Month = sdateTime.Month;
			TimeOfDay.Instance.Year = sdateTime.Year;
			TimeOfDay.Instance.targetDate = (float)TimeOfDay.Instance.GetDate(true).ToInt();
			this.CompanyBenefits = dictionary.Get<Dictionary<string, float>>("Benefits", this.CompanyBenefits);
			this.simulation = (MarketSimulation)dictionary["Simulation"];
			this.MyCompany = (Company)dictionary["Company"];
			this.MyCompany.Products = (from x in this.MyCompany.Products
			select this.simulation.GetProduct(x.ID, false)).ToList<SoftwareProduct>();
			foreach (SimulatedCompany simulatedCompany in this.simulation.Companies.Values)
			{
				simulatedCompany.Products = (from x in simulatedCompany.Products
				select this.simulation.GetProduct(x.ID, false)).ToList<SoftwareProduct>();
			}
			HUD.Instance.contractWindow.Contracts.Items = (EventList<object>)dictionary["Contracts"];
			dictionary.Get<List<PopupManager.PopUp>>("Popups", new List<PopupManager.PopUp>()).ForEach(delegate(PopupManager.PopUp x)
			{
				HUD.Instance.popupManager.AddPopup(x);
			});
			this.DisabledTutorials = dictionary.Get<List<string>>("DisabledTutorials", new List<string>()).ToHashSet<string>();
			this.Loans = dictionary.Get<List<KeyValuePair<int, float>>>("Loans", new List<KeyValuePair<int, float>>());
			this.SoftwareTypes = dictionary.Get<Dictionary<string, SoftwareType>>("SoftwareTypes", GameData.AllSoftwareTypes(null).ToDictionary((SoftwareType x) => x.Name));
			string[][] specialization = GameData.GetSpecialization(this.SoftwareTypes.Values.ToArray<SoftwareType>());
			this.CodeSpecializations = specialization[1];
			this.ArtSpecializations = specialization[2];
			this.Specializations = specialization[0];
			this.CompanyTypes = dictionary.Get<Dictionary<string, CompanyType>>("CompanyTypes", GameData.AllCompanyTypes().ToDictionary((CompanyType x) => x.Name));
			this.RNG = dictionary.Get<Dictionary<string, RandomNameGenerator>>("RNG", GameData.MergeGenerators(GameData.AllNameGenerators(null)));
			this.RNDString = dictionary.Get<string>("RNDString", "None");
			this.Personalities = dictionary.Get<PersonalityGraph>("Personalities", GameData.AllPersonalities());
			this.WorkItemIDCounter = dictionary.Get<uint>("WorkItemID", 1u);
			this.StyleDefaults = dictionary.Get<Dictionary<string, string>>("StyleDef", new Dictionary<string, string>());
			this.ColorDefaults = dictionary.Get<Dictionary<string, SVector3>>("ColorDef", new Dictionary<string, SVector3>()).ToDictionary((KeyValuePair<string, SVector3> x) => x.Key, (KeyValuePair<string, SVector3> x) => x.Value.ToColor());
			this.ElectricityBill = dictionary.Get<float>("Electricity", 0f);
			this.Waterbill = dictionary.Get<float>("Water", 0f);
			this.Insurance = dictionary.Get<InsuranceAccount>("Insurance", new InsuranceAccount());
			this.Difficulty = dictionary.Get<int>("Difficulty", 0);
			this.MyCompany.Products.ForEach(delegate(SoftwareProduct x)
			{
				x.RegisterServer();
			});
			this.MyCompany.WorkItems.OfType<SoftwareAlpha>().ToList<SoftwareAlpha>().ForEach(delegate(SoftwareAlpha x)
			{
				x.RegisterServer();
			});
			Newspaper.Instance.Stories = dictionary.Get<Dictionary<SDateTime, List<Newspaper.Story>>>("NewspaperCurrent", new Dictionary<SDateTime, List<Newspaper.Story>>());
			Newspaper.Instance.InitializeSections();
			Newspaper.UpdateStories();
			HUD.Instance.contractWindow.ContractResults.Items.AddRange(dictionary.Get<ContractResult[]>("ContractResults", new ContractResult[0]));
			this.SalaryDue = dictionary.Get<float>("SalaryDue", 0f);
			this.StaffSalaryDue = dictionary.Get<float>("StaffSalaryDue", 0f);
			this.StockCount = dictionary.Get<int>("StockCount", this.StockCount);
			if (dictionary.Contains("NeverUsed2"))
			{
				this.NeverUsed = (Dictionary<KeyValuePair<string, string>, SHashSet<string>>)dictionary["NeverUsed2"];
			}
			else
			{
				this.NeverUsed = this.SoftwareTypes.Values.SelectMany((SoftwareType x) => from z in x.Categories.Values
				select new KeyValuePair<SoftwareType, SoftwareCategory>(x, z)).ToDictionary((KeyValuePair<SoftwareType, SoftwareCategory> x) => new KeyValuePair<string, string>(x.Key.Name, x.Value.Name), (KeyValuePair<SoftwareType, SoftwareCategory> x) => (from z in x.Key.Features.Values
				where !z.Forced && !z.Base
				select z.Name).ToSHashSet<string>());
				foreach (SoftwareProduct product in this.simulation.GetAllProducts())
				{
					this.NewFeatures(product, false);
				}
			}
			HUD.Instance.dealWindow.Deserialize(dictionary);
			HUD.Instance.eventWindow.Deserialize(dictionary);
			foreach (ServerDeal serverDeal in HUD.Instance.dealWindow.AllDeals.Values.OfType<ServerDeal>())
			{
				if (serverDeal.Incoming && serverDeal.Active)
				{
					this.RegisterWithServer(serverDeal.activeServer, serverDeal, true);
				}
			}
			this.TeamDefaults = dictionary.Get<Dictionary<string, string[]>>("TeamDefaults", new Dictionary<string, string[]>()).ToDictionary((KeyValuePair<string, string[]> x) => x.Key, (KeyValuePair<string, string[]> x) => x.Value.ToHashSet<string>());
			this.ReviewJobs = dictionary.Get<List<ReviewWork>>("ReviewJobs", new List<ReviewWork>());
			this.FollowerSimulation = dictionary.Get<List<SoftwareWorkItem>>("FollowerSimulation", new List<SoftwareWorkItem>());
			this.PressBuildQueue = dictionary.Get<List<SoftwareAlpha>>("PressBuildQueue", new List<SoftwareAlpha>()).ToHashSet<SoftwareAlpha>();
			TimeOfDay.Instance.Banktupcy = dictionary.Get<SDateTime?>("Bankruptcy", null);
			this.Distribution = dictionary.Get<PlayerDistribution>("Distribution", null);
			if (dictionary.Contains("PrintOrders2"))
			{
				this.PrintOrders = dictionary.Get<Dictionary<uint, PrintJob>>("PrintOrders2", new Dictionary<uint, PrintJob>());
			}
			else
			{
				foreach (KeyValuePair<uint, float> keyValuePair2 in dictionary.Get<Dictionary<uint, float>>("PrinterOrders", new Dictionary<uint, float>()))
				{
					this.PrintOrders[keyValuePair2.Key] = new PrintJob(keyValuePair2.Key, keyValuePair2.Value);
				}
				foreach (KeyValuePair<uint, uint> keyValuePair3 in dictionary.Get<Dictionary<uint, uint>>("PrintDeals", new Dictionary<uint, uint>()))
				{
					PrintJob printJob;
					if (this.PrintOrders.TryGetValue(keyValuePair3.Key, out printJob))
					{
						printJob.DealID = new uint?(keyValuePair3.Value);
					}
				}
			}
			HUD.Instance.distributionWindow.RefreshOrders();
			this.BillsNext = dictionary.Get<Dictionary<Company.TransactionCategory, Dictionary<string, float>>>("BillsNext2", new Dictionary<Company.TransactionCategory, Dictionary<string, float>>());
			this.BillsCurrent = dictionary.Get<Dictionary<Company.TransactionCategory, Dictionary<string, float>>>("BillsCurrent2", new Dictionary<Company.TransactionCategory, Dictionary<string, float>>());
		}
		if (loadMode == GameReader.LoadMode.Full)
		{
			this._printsInStorage = dictionary.Get<Dictionary<uint, uint>>("PrintsInStorage", new Dictionary<uint, uint>());
		}
	}

	public WriteDictionary Serialize(GameReader.LoadMode mode)
	{
		WriteDictionary writeDictionary = new WriteDictionary("GameSettings");
		writeDictionary["SaveMode"] = mode;
		writeDictionary["Errors"] = this.Errors;
		writeDictionary["Autosave"] = ((this.AssociatedAutoSave == null) ? null : this.AssociatedAutoSave.FilePath);
		writeDictionary["RoomStyles"] = this.RoomStyles;
		writeDictionary["DefaultIndoorRoomStyle"] = this.DefaultIndoorRoomStyle;
		writeDictionary["DefaultOutdoorRoomStyle"] = this.DefaultOutdoorRoomStyle;
		if (mode == GameReader.LoadMode.Full || mode == GameReader.LoadMode.Company)
		{
			writeDictionary["DaysPerMonth"] = GameSettings.DaysPerMonth;
			writeDictionary["Time"] = SDateTime.Now();
			writeDictionary["Simulation"] = this.simulation;
			writeDictionary["Company"] = this.MyCompany;
			writeDictionary["Benefits"] = this.CompanyBenefits;
			writeDictionary["Contracts"] = HUD.Instance.contractWindow.Contracts.Items;
			writeDictionary["Popups"] = (from x in HUD.Instance.popupManager.PopupButtons
			where x.popup != null
			select x.popup).ToList<PopupManager.PopUp>();
			writeDictionary["DisabledTutorials"] = this.DisabledTutorials.ToList<string>();
			writeDictionary["Loans"] = this.Loans;
			writeDictionary["SoftwareTypes"] = this.SoftwareTypes;
			writeDictionary["CompanyTypes"] = this.CompanyTypes;
			writeDictionary["RNG"] = this.RNG;
			writeDictionary["Personalities"] = this.Personalities;
			writeDictionary["RNDString"] = this.RNDString;
			writeDictionary["WorkItemID"] = this.WorkItemIDCounter;
			writeDictionary["Electricity"] = this.ElectricityBill;
			writeDictionary["Water"] = this.Waterbill;
			writeDictionary["Insurance"] = this.Insurance;
			writeDictionary["StyleDef"] = this.StyleDefaults;
			writeDictionary["ColorDef"] = this.ColorDefaults.ToDictionary((KeyValuePair<string, Color> x) => x.Key, (KeyValuePair<string, Color> x) => x.Value);
			writeDictionary["Difficulty"] = this.Difficulty;
			writeDictionary["NewspaperCurrent"] = Newspaper.Instance.Stories;
			writeDictionary["NeverUsed2"] = this.NeverUsed;
			writeDictionary["ContractResults"] = HUD.Instance.contractWindow.ContractResults.Items.Cast<ContractResult>().ToArray<ContractResult>();
			writeDictionary["SalaryDue"] = this.SalaryDue;
			writeDictionary["StaffSalaryDue"] = this.StaffSalaryDue;
			writeDictionary["StockCount"] = this.StockCount;
			writeDictionary["EmployeeTerminations"] = HUD.Instance.insuranceWindow.Terminations.Items.OfType<EmployeeTermination>().ToList<EmployeeTermination>();
			HUD.Instance.dealWindow.Serialize(writeDictionary);
			HUD.Instance.eventWindow.Serialize(writeDictionary);
			writeDictionary["TeamDefaults"] = this.TeamDefaults.ToDictionary((KeyValuePair<string, HashSet<string>> x) => x.Key, (KeyValuePair<string, HashSet<string>> x) => x.Value.ToArray<string>());
			writeDictionary["ReviewJobs"] = this.ReviewJobs;
			writeDictionary["FollowerSimulation"] = this.FollowerSimulation;
			writeDictionary["PressBuildQueue"] = this.PressBuildQueue.ToList<SoftwareAlpha>();
			writeDictionary["Bankruptcy"] = TimeOfDay.Instance.Banktupcy;
			writeDictionary["Distribution"] = this.Distribution;
			writeDictionary["PrintOrders2"] = this.PrintOrders;
			writeDictionary["BillsNext2"] = this.BillsNext;
			writeDictionary["BillsCurrent2"] = this.BillsCurrent;
		}
		if (mode == GameReader.LoadMode.Full)
		{
			writeDictionary["PrintsInStorage"] = this._printsInStorage;
		}
		if (mode == GameReader.LoadMode.Full || mode == GameReader.LoadMode.Building)
		{
			writeDictionary["RentMode"] = this.RentMode;
			writeDictionary["Plots"] = this.Plots;
			writeDictionary["PlayerPlots"] = this.PlayerPlots;
			writeDictionary["Trees2"] = this.Trees;
			writeDictionary["EnvType"] = this.EnvType;
			writeDictionary["CliType"] = this.CliType;
			writeDictionary["RoomGroups"] = this.RoomGroups;
			writeDictionary["Server"] = (from x in this.MainServers
			where x.Value is Server
			select x).ToDictionary((KeyValuePair<string, IServerHost> x) => x.Key, (KeyValuePair<string, IServerHost> x) => ((Server)x.Value).DID);
		}
		return writeDictionary;
	}

	public string[] GetUnlockedSpecializations(Employee.EmployeeRole role)
	{
		if (!this.CachedSpecs || this.LastSpec.Day != TimeOfDay.Instance.Day || this.LastSpec.Year != TimeOfDay.Instance.Year || this.LastSpec.Month != TimeOfDay.Instance.Month)
		{
			this.CachedSpecs = true;
			this.LastSpec = SDateTime.Now();
			this.CachedSpecDict[3] = (from x in this.ArtSpecializations
			where (from z in this.SoftwareTypes.Values
			where z.IsUnlocked(TimeOfDay.Instance.Year)
			select z).Any((SoftwareType y) => y.Features.Any((KeyValuePair<string, Feature> z) => x.Equals(z.Value.GetCategory(false)) && z.Value.IsUnlocked(null, TimeOfDay.Instance.Year, this.SoftwareTypes)))
			select x).ToArray<string>();
			this.CachedSpecDict[1] = (from x in this.CodeSpecializations
			where (from z in this.SoftwareTypes.Values
			where z.IsUnlocked(TimeOfDay.Instance.Year)
			select z).Any((SoftwareType y) => y.Features.Any((KeyValuePair<string, Feature> z) => x.Equals(z.Value.GetCategory(true)) && z.Value.IsUnlocked(null, TimeOfDay.Instance.Year, this.SoftwareTypes)))
			select x).ToArray<string>();
			this.CachedSpecDict[-1] = (from x in this.Specializations
			where (from z in this.SoftwareTypes.Values
			where z.IsUnlocked(TimeOfDay.Instance.Year)
			select z).Any((SoftwareType y) => y.Features.Any((KeyValuePair<string, Feature> z) => x.Equals(z.Value.GetCategory(true)) && z.Value.IsUnlocked(null, TimeOfDay.Instance.Year, this.SoftwareTypes)))
			select x).ToArray<string>();
		}
		if (role == Employee.EmployeeRole.Artist)
		{
			return this.CachedSpecDict[3];
		}
		if (role == Employee.EmployeeRole.Programmer)
		{
			return this.CachedSpecDict[1];
		}
		return this.CachedSpecDict[-1];
	}

	public float PaybackLoan()
	{
		float num = 0f;
		List<KeyValuePair<int, float>> list = new List<KeyValuePair<int, float>>();
		foreach (KeyValuePair<int, float> keyValuePair in this.Loans)
		{
			num += keyValuePair.Value;
			if (keyValuePair.Key > 1)
			{
				list.Add(new KeyValuePair<int, float>(keyValuePair.Key - 1, keyValuePair.Value));
			}
		}
		this.MyCompany.MakeTransaction(-num, Company.TransactionCategory.Loan, null);
		this.Loans.Clear();
		this.Loans.AddRange(list);
		return num;
	}

	public bool IsResearching(string sw, string f)
	{
		return this.MyCompany.WorkItems.OfType<ResearchWork>().Any((ResearchWork x) => x.Software.Equals(sw) && x.Feature.Equals(f));
	}

	public HashSet<string> GetDefaultTeams(string cat)
	{
		HashSet<string> result;
		if (this.TeamDefaults.TryGetValue(cat, out result))
		{
			return result;
		}
		string text = this.sActorManager.Teams.Keys.FirstOrDefault<string>();
		return (text != null) ? new HashSet<string>
		{
			text
		} : new HashSet<string>();
	}

	public HashSet<string> GetDefaultTeams(string cat, HashSet<string> defaultTeams)
	{
		return this.TeamDefaults.GetOrDefault(cat, defaultTeams);
	}

	public bool CanMakeSequel(SoftwareProduct p, Company c)
	{
		if (p.HasSequel || p.DevCompany != c)
		{
			return false;
		}
		if (c.Player && this.MyCompany.WorkItems.OfType<SoftwareWorkItem>().Any((SoftwareWorkItem x) => x.SequelTo != null && x.SequelTo.Value == p.ID))
		{
			return false;
		}
		SimulatedCompany simulatedCompany = c as SimulatedCompany;
		return simulatedCompany == null || (!simulatedCompany.Releases.Any((SimulatedCompany.ProductPrototype y) => y.SequelTo != null && y.SequelTo == p.ID) && !simulatedCompany.ProjectQueue.Any((SimulatedCompany.ProductPrototype y) => y.SequelTo != null && y.SequelTo == p.ID));
	}

	public float GetMapCost(bool withMortgage)
	{
		return this.sRoomManager.GetRooms().SumSafe((Room y) => BuildController.GetRoomCost(y.Edges, y.Area, y.Outdoors, y.Floor, false, false)) + this.sRoomManager.AllFurniture.SumSafe((Furniture x) => x.Cost) + this.sRoomManager.RoomSegments.SumSafe((RoomSegment x) => x.Cost) + this.PlayerPlots.SumSafe((PlotArea y) => (!withMortgage) ? y.Price : (y.Price - y.Monthly * (float)y.MonthsLeft));
	}

	public bool WireMode = true;

	public static GameSettings Instance;

	public static int MaxFloor = 10;

	public Material TreeTrunkMat;

	public Material tTreeTrunkMat;

	public Material LeaveMat;

	public Material CactusTrunkMat;

	public StaticTree[] CachedTrees = new StaticTree[0];

	public TreeBatch TreeBatchPrefab;

	public List<TreeBatch> TreeBatches = new List<TreeBatch>();

	public List<GameObject> MergedTreeTrunks = new List<GameObject>();

	public List<GameObject> MergedLeaves = new List<GameObject>();

	[NonSerialized]
	public List<global::TreeInstance> Trees = new List<global::TreeInstance>();

	[NonSerialized]
	public List<global::TreeInstance> TempTrees = new List<global::TreeInstance>();

	[NonSerialized]
	public List<RoomStyle> RoomStyles = new List<RoomStyle>();

	public RoomStyle DefaultOutdoorRoomStyle = new RoomStyle("Default", "Wood", "Plain white", "None", false, Color.white, new SVector3(0.992f, 0.788f, 0.525f, 1f), Color.white);

	public RoomStyle DefaultIndoorRoomStyle = new RoomStyle("Default", "Concrete wall", "Plain white", "White carpet", false, Color.white, Color.gray, Color.white);

	[NonSerialized]
	public QuadTree<global::TreeInstance> TreeTree;

	public RoomManager sRoomManager;

	public float ElectricityBill;

	public float Waterbill;

	public ActorManager sActorManager;

	public int ActiveFloor;

	[NonSerialized]
	public Company MyCompany;

	private int WindowIDCounter = 10;

	private uint WorkItemIDCounter = 1u;

	public bool HideCeilingFurniture;

	public bool AssignOverlay;

	[NonSerialized]
	public MarketSimulation simulation;

	public Color[] PlotColors = new Color[]
	{
		new Color32(126, 207, 112, byte.MaxValue),
		new Color32(95, 122, 155, byte.MaxValue),
		new Color32(220, 108, 130, byte.MaxValue),
		new Color32(236, 157, 112, byte.MaxValue),
		new Color32(126, 95, 160, byte.MaxValue),
		new Color32(90, 203, 207, byte.MaxValue),
		new Color32(216, 194, 89, byte.MaxValue),
		new Color32(199, 40, 40, byte.MaxValue)
	};

	public GameObject RoomObject;

	public GameObject ActorObj;

	public int ExpansionSize = 5;

	public int Difficulty = 1;

	public float ExpansionCost = 350f;

	[NonSerialized]
	public SaveGame AssociatedSave;

	[NonSerialized]
	public SaveGame AssociatedAutoSave;

	private GameObject TreeRoot;

	private static int _forcePause;

	public static bool FreezeGame;

	public static GameSettings.WallState WallsDown;

	public HashSet<string> DisabledTutorials = new HashSet<string>();

	public List<KeyValuePair<int, float>> Loans = new List<KeyValuePair<int, float>>();

	public Dictionary<string, SoftwareType> SoftwareTypes;

	public Dictionary<string, CompanyType> CompanyTypes;

	public Dictionary<string, RandomNameGenerator> RNG;

	[NonSerialized]
	public Dictionary<string, float> CompanyBenefits = new Dictionary<string, float>();

	public PersonalityGraph Personalities;

	[NonSerialized]
	public InsuranceAccount Insurance = new InsuranceAccount();

	public Dictionary<string, Color> ColorDefaults = new Dictionary<string, Color>();

	public Dictionary<string, string> StyleDefaults = new Dictionary<string, string>();

	[NonSerialized]
	public Dictionary<string, HashSet<string>> TeamDefaults = new Dictionary<string, HashSet<string>>();

	private Dictionary<string, IServerHost> MainServers = new Dictionary<string, IServerHost>();

	public List<IServerItem> UnsupportedServerItems = new List<IServerItem>();

	public Dictionary<KeyValuePair<string, string>, SHashSet<string>> NeverUsed = new Dictionary<KeyValuePair<string, string>, SHashSet<string>>();

	private Actor Founder;

	private static float _gameSpeed = 1f;

	private static float lastGameSpeed = 1f;

	public string[] Specializations;

	public string[] CodeSpecializations;

	public string[] ArtSpecializations;

	public static bool IsQuitting;

	private bool CachedSpecs;

	private SDateTime LastSpec;

	private Dictionary<int, string[]> CachedSpecDict = new Dictionary<int, string[]>();

	public static int DaysPerMonth = 1;

	public GameObject PreSimLoadPanel;

	public Text PreSimLoadText;

	public Text PreSimSWText;

	public GUIProgressBar PreSimBar;

	public int Vacations;

	public GameObject BusStopSign;

	public float SalaryDue;

	public float StaffSalaryDue;

	public string RNDString;

	public string RNDStringOverride = "Test";

	public bool RuralBigPlotOverride = true;

	public int StockCount = 5;

	public WeatherPreset[] WeatherPresets;

	public GameData.EnvironmentType EnvType;

	public GameData.ClimateType CliType;

	public bool SkipSimulation;

	public bool SkipTrees;

	public bool EditMode;

	public bool RentMode;

	public AnimationCurve TreeFalloff;

	public FrameDistributor ActorUpdateHandler;

	public FrameDistributor WorkUpdateHandler;

	public FrameDistributor FurnitureUpdateHandler;

	public FrameDistributor ComputerNoiseUpdateHandler;

	public SHashSet<string> Errors = new SHashSet<string>();

	[NonSerialized]
	public bool FurnitureErrorOccured;

	[NonSerialized]
	public List<ReviewWork> ReviewJobs = new List<ReviewWork>();

	[NonSerialized]
	public List<SoftwareWorkItem> FollowerSimulation = new List<SoftwareWorkItem>();

	[NonSerialized]
	public HashSet<SoftwareAlpha> PressBuildQueue = new HashSet<SoftwareAlpha>();

	[NonSerialized]
	public PlayerDistribution Distribution;

	[NonSerialized]
	public Dictionary<uint, PrintJob> PrintOrders = new Dictionary<uint, PrintJob>();

	[NonSerialized]
	private Dictionary<uint, uint> _printsInStorage = new Dictionary<uint, uint>();

	[NonSerialized]
	public List<ProductPrinter> ProductPrinters = new List<ProductPrinter>();

	[NonSerialized]
	public Dictionary<Company.TransactionCategory, Dictionary<string, float>> BillsCurrent = new Dictionary<Company.TransactionCategory, Dictionary<string, float>>();

	[NonSerialized]
	public Dictionary<Company.TransactionCategory, Dictionary<string, float>> BillsNext = new Dictionary<Company.TransactionCategory, Dictionary<string, float>>();

	private List<UndoObject> UndoList = new List<UndoObject>();

	public static int MaxUndo = 30;

	public GameObject UndoButton;

	public Dictionary<int, MeshFilter> FloorRentGrids = new Dictionary<int, MeshFilter>();

	public HashSet<int> DirtyRentGrid = new HashSet<int>();

	public GUIToolTipper UndoTip;

	[NonSerialized]
	private Dictionary<string, RoomGroup> RoomGroups = new Dictionary<string, RoomGroup>();

	public float UndoAutosaveWait;

	public bool wasSkipping;

	public HashSet<object> Cached = new HashSet<object>();

	public GameObject PlotPrefab;

	public Transform PlotHolder;

	[NonSerialized]
	public List<PlotArea> Plots = new List<PlotArea>();

	[NonSerialized]
	public List<PlotArea> PlayerPlots = new List<PlotArea>();

	private Thread _simThread;

	[NonSerialized]
	public bool HasToFinalizeTimers;

	public float InGameMinute;

	private bool presimactive;

	private string presimloadtxt;

	private string presimswtext;

	private float presimloadbar;

	private SDateTime presimfinaltime;

	public List<EmployeeTermination> SerializedEvents;

	[CompilerGenerated]
	private static Func<Vector3, Vector2> <>f__mg$cache0;

	public enum WallState
	{
		High,
		Back,
		Low,
		LowNoSeg
	}
}
