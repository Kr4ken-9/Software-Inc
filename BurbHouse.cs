﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BurbHouse : Landmark
{
	public void Init(Vector2[] bounds = null)
	{
		this.block = new MaterialPropertyBlock();
		this.block.SetVector("_TexOffset", new Vector4((float)Utilities.RandomRange(0, 4) / 4f, (float)Utilities.RandomRange(0, 4) / 4f, 0.25f, 0.25f));
		this.block.SetVector("_ExtraStuff", this._matVec);
		this.Rend.SetPropertyBlock(this.block);
		this.OnToggle = UnityEngine.Random.Range(0.6f, 0.9f);
		this.OffToggle = UnityEngine.Random.Range(0.1f, 0.4f);
		if (bounds != null)
		{
			for (int i = 0; i < this.TreePoints.Length; i++)
			{
				Vector3 position = this.TreePoints[i].position;
				if (Utilities.IsInside(position.FlattenVector3(), bounds))
				{
					GameSettings.Instance.AddTree(position, false);
				}
			}
		}
		this.NavMesh = this.Bounds.SelectInPlace((Vector2 x) => base.transform.localToWorldMatrix.MultiplyPoint(x.ToVector3(0f)).FlattenVector3()).ReverseArray<Vector2>();
	}

	public void Update()
	{
		float num = TimeOfDay.Instance.HouseOnage.Evaluate((float)TimeOfDay.Instance.Hour + TimeOfDay.Instance.Minute / 60f);
		if (!this.LightsOn && num > this.OnToggle)
		{
			this.LightsOn = true;
		}
		else if (this.LightsOn && num < this.OffToggle)
		{
			this.LightsOn = false;
		}
		this.Rend.enabled = (GameSettings.Instance.ActiveFloor >= 0);
		this.UpdateMatVec(false);
	}

	private void UpdateMatVec(bool force = false)
	{
		Vector4 vector = new Vector4(TimeOfDay.Instance.SnowAmount, (!this.LightsOn) ? 0f : 1f, 0f, 0f);
		if (force || !this.EqualVec(vector, this._matVec))
		{
			this._matVec = vector;
			this.block.SetVector("_ExtraStuff", this._matVec);
			this.Rend.SetPropertyBlock(this.block);
		}
	}

	private bool EqualVec(Vector4 v1, Vector4 v2)
	{
		float num = v1.x - v2.x;
		num += v1.y - v2.y;
		num += v1.z - v2.z;
		num += v1.w - v2.w;
		return num > -0.001f && num < 0.001f;
	}

	private void OnDrawGizmos()
	{
		for (int i = 0; i < this.Bounds.Length; i++)
		{
			Gizmos.color = ((i != 0) ? Color.cyan : Color.red);
			Vector3 vector = base.transform.localToWorldMatrix.MultiplyPoint(this.Bounds[i].ToVector3(0f));
			Vector3 to = base.transform.localToWorldMatrix.MultiplyPoint(this.Bounds[(i + 1) % this.Bounds.Length].ToVector3(0f));
			Gizmos.DrawSphere(vector, 0.2f);
			Gizmos.DrawLine(vector, to);
		}
		Gizmos.color = Color.white;
		for (int j = 0; j < this.TreePoints.Length; j++)
		{
			Vector3 position = this.TreePoints[j].position;
			Gizmos.DrawCube(position + Vector3.up * 1f, new Vector3(1f, 2f, 1f));
		}
	}

	public void FindTrees()
	{
		this.TreePoints = (from x in base.GetComponentsInChildren<Transform>()
		where x != base.transform && x.GetComponent<MeshRenderer>() == null
		select x).ToArray<Transform>();
	}

	public void GenerateBounds()
	{
		Mesh sharedMesh = this.Rend.GetComponent<MeshFilter>().sharedMesh;
		List<Vector2> points = (from x in sharedMesh.vertices
		select this.Rend.transform.localToWorldMatrix.MultiplyPoint(x).FlattenVector3()).ToList<Vector2>();
		this.Bounds = Utilities.ComputeConvexHull(points).ToArray();
	}

	protected override object DeserializeMe(WriteDictionary dictionary)
	{
		this.block = new MaterialPropertyBlock();
		this.block.SetVector("_TexOffset", dictionary.Get<SVector3>("TexOffset", new SVector3(0f, 0f, 0.25f, 0.25f)));
		this.OnToggle = dictionary.Get<float>("OnToggle", 0.75f);
		this.OffToggle = dictionary.Get<float>("OffToggle", 0.25f);
		base.transform.position = dictionary.Get<SVector3>("Position", new SVector3());
		base.transform.rotation = dictionary.Get<SVector3>("Rotation", new SVector3());
		this.UpdateMatVec(true);
		this.NavMesh = this.Bounds.SelectInPlace((Vector2 x) => base.transform.localToWorldMatrix.MultiplyPoint(x.ToVector3(0f)).FlattenVector3());
		return this;
	}

	protected override void SerializeMe(WriteDictionary dictionary, GameReader.LoadMode mode)
	{
		dictionary["Idx"] = this.Idx;
		dictionary["LightsOn"] = this.LightsOn;
		dictionary["OnToggle"] = this.OnToggle;
		dictionary["OffToggle"] = this.OffToggle;
		dictionary["TexOffset"] = this.block.GetVector("_TexOffset");
		dictionary["Position"] = base.transform.position;
		dictionary["Rotation"] = base.transform.rotation;
	}

	public override string WriteName()
	{
		return "BurbHouse";
	}

	public override Vector2[] GetNavMesh()
	{
		return this.NavMesh;
	}

	public override Vector2 Center()
	{
		return base.transform.position.FlattenVector3();
	}

	[ContextMenuItem("Generate", "GenerateBounds")]
	public Vector2[] Bounds;

	[ContextMenuItem("Generate", "FindTrees")]
	public Transform[] TreePoints;

	public MeshRenderer Rend;

	public bool LightsOn;

	public float OnToggle;

	public float OffToggle;

	private MaterialPropertyBlock block;

	public float LowerAreaReq;

	public float Cost;

	[NonSerialized]
	private Vector4 _matVec = Vector4.zero;

	[NonSerialized]
	public Vector2[] NavMesh;

	public int Idx;
}
