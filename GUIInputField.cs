﻿using System;
using UnityEngine;

public class GUIInputField : MonoBehaviour
{
	public void EnableInput(bool enable)
	{
		InputController.InputEnabled = enable;
	}
}
