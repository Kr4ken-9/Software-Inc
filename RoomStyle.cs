﻿using System;
using System.Collections.Generic;

[Serializable]
public class RoomStyle
{
	public RoomStyle()
	{
	}

	public RoomStyle(string name, string outsideMat, string insideMat, string floorMat, bool outdoor, SVector3 insideColor, SVector3 outsideColor, SVector3 floorColor)
	{
		this.StyleName = name;
		this.OutsideMat = outsideMat;
		this.InsideMat = insideMat;
		this.FloorMat = floorMat;
		this.InsideColor = insideColor;
		this.OutsideColor = outsideColor;
		this.FloorColor = floorColor;
		this.OutdoorStyle = outdoor;
	}

	public RoomStyle(string name, Room room)
	{
		this.StyleName = name;
		this.OutdoorStyle = room.Outdoors;
		if (this.OutdoorStyle)
		{
			this.OutsideMat = room.FenceStyle;
		}
		else
		{
			this.OutsideMat = room.OutsideMat;
			this.InsideMat = room.InsideMat;
			this.InsideColor = room.InsideColor;
		}
		this.FloorMat = room.FloorMat;
		this.OutsideColor = room.OutsideColor;
		this.FloorColor = room.FloorColor;
	}

	public void Apply(Room room, List<UndoObject.UndoAction> undos)
	{
		if (room.Outdoors == this.OutdoorStyle)
		{
			if (this.OutdoorStyle)
			{
				room.SetFenceStyle(this.OutsideMat, undos);
			}
			else
			{
				room.OutsideMat = this.OutsideMat;
				room.InsideMat = this.InsideMat;
				room.InsideColor = this.InsideColor;
			}
			room.FloorMat = this.FloorMat;
			room.OutsideColor = this.OutsideColor;
			room.FloorColor = this.FloorColor;
		}
	}

	public string StyleName;

	public string OutsideMat;

	public string InsideMat;

	public string FloorMat;

	public SVector3 InsideColor;

	public SVector3 OutsideColor;

	public SVector3 FloorColor;

	public bool OutdoorStyle;
}
