﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OverlayWarning : Graphic
{
	public override Texture mainTexture
	{
		get
		{
			return this.tex;
		}
	}

	protected override void OnPopulateMesh(VertexHelper h)
	{
		if (this.WarningCount == 0 && h.currentVertCount == 0)
		{
			return;
		}
		h.Clear();
		for (int i = 0; i < this.WarningCount; i++)
		{
			Vector2 vector = this.Warnings[i] / Options.UISize - HUD.Instance.MainContentPanel.offsetMin;
			h.AddUIVertexQuad(new UIVertex[]
			{
				new UIVertex
				{
					position = new Vector3(vector.x - 16f, vector.y - 16f),
					uv0 = new Vector2(0f, 0f),
					color = this.color
				},
				new UIVertex
				{
					position = new Vector3(vector.x + 16f, vector.y - 16f),
					uv0 = new Vector2(1f, 0f),
					color = this.color
				},
				new UIVertex
				{
					position = new Vector3(vector.x + 16f, vector.y + 16f),
					uv0 = new Vector2(1f, 1f),
					color = this.color
				},
				new UIVertex
				{
					position = new Vector3(vector.x - 16f, vector.y + 16f),
					uv0 = new Vector2(0f, 1f),
					color = this.color
				}
			});
		}
	}

	private void Update()
	{
		if (!Application.isPlaying)
		{
			return;
		}
		this.ColorT = (this.ColorT + Time.deltaTime) % 2f;
		float t = (this.ColorT >= 1f) ? (2f - this.ColorT) : this.ColorT;
		this.color = Color.Lerp(Color.white, Color.red, t);
	}

	public void BeginMessageUpdate()
	{
		this.WarningCount = 0;
		this.MouseOver = false;
	}

	public void AddMessages(Vector2 pos, Vector2 mp, string message)
	{
		if (this.WarningCount > 32 || this.Outside(pos))
		{
			return;
		}
		Vector2 vector = new Vector2(pos.x, -((float)Screen.height - pos.y));
		if (!this.MouseOver && this.Within(mp, pos))
		{
			this.ShowMessages(message, vector);
			this.MouseOver = true;
			return;
		}
		if (this.WarningCount == this.Warnings.Count)
		{
			this.Warnings.Add(Vector2.zero);
		}
		this.Warnings[this.WarningCount] = vector;
		this.WarningCount++;
	}

	public void AddMessages(Vector2 pos, Vector2 mp, List<string> messages)
	{
		if (this.WarningCount > 32 || this.Outside(pos))
		{
			return;
		}
		Vector2 vector = new Vector2(pos.x, -((float)Screen.height - pos.y));
		if (!this.MouseOver && this.Within(mp, pos))
		{
			this.ShowMessages(messages, vector);
			this.MouseOver = true;
			return;
		}
		if (this.WarningCount == this.Warnings.Count)
		{
			this.Warnings.Add(Vector2.zero);
		}
		this.Warnings[this.WarningCount] = vector;
		this.WarningCount++;
	}

	private bool Outside(Vector2 p)
	{
		return p.x < -16f || p.x > (float)(Screen.width + 16) || p.y < -16f || p.y > (float)(Screen.height + 16);
	}

	private bool Within(Vector2 p, Vector2 t)
	{
		return p.x >= t.x - 16f && p.x <= t.x + 16f && p.y >= t.y - 16f && p.y <= t.y + 16f;
	}

	private void ShowMessages(List<string> messages, Vector2 pos)
	{
		this.LabelPanel.anchoredPosition = pos / Options.UISize + Vector2.down * 16f - HUD.Instance.MainContentPanel.offsetMin;
		int i;
		for (i = 0; i < Mathf.Min(this.Pool.Count, messages.Count); i++)
		{
			this.Pool[i].text = messages[i];
			this.Pool[i].transform.parent.gameObject.SetActive(true);
		}
		if (i < messages.Count)
		{
			while (i < messages.Count)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.LabelPrefab);
				Text componentInChildren = gameObject.GetComponentInChildren<Text>();
				componentInChildren.text = messages[i];
				gameObject.transform.SetParent(this.LabelPanel, false);
				this.Pool.Add(componentInChildren);
				i++;
			}
		}
		if (i < this.Pool.Count)
		{
			while (i < this.Pool.Count)
			{
				this.Pool[i].transform.parent.gameObject.SetActive(false);
				i++;
			}
		}
	}

	private void ShowMessages(string message, Vector2 pos)
	{
		this.LabelPanel.anchoredPosition = pos + Vector2.down * 16f;
		if (this.Pool.Count == 0)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.LabelPrefab);
			gameObject.transform.SetParent(this.LabelPanel, false);
			this.Pool.Add(gameObject.GetComponentInChildren<Text>());
		}
		this.Pool[0].text = message;
		this.Pool[0].transform.parent.gameObject.SetActive(true);
		for (int i = 1; i < this.Pool.Count; i++)
		{
			this.Pool[i].transform.parent.gameObject.SetActive(false);
		}
	}

	public void EndMessageUpdate()
	{
		if (!this.MouseOver)
		{
			for (int i = 0; i < this.Pool.Count; i++)
			{
				this.Pool[i].transform.parent.gameObject.SetActive(false);
			}
		}
		this.SetVerticesDirty();
	}

	public Texture tex;

	public GameObject LabelPrefab;

	public RectTransform LabelPanel;

	public List<Text> Pool = new List<Text>();

	private List<Vector2> Warnings = new List<Vector2>();

	private int WarningCount;

	private bool MouseOver;

	private float ColorT;
}
