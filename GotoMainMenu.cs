﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GotoMainMenu : MonoBehaviour
{
	private IEnumerator Start()
	{
		try
		{
			SaveGameManager.InitializeSaves();
		}
		catch (Exception exception)
		{
			LoadDebugger.AddError("An error occured while initializing saves");
			Debug.LogException(exception);
		}
		LoadDebugger.AddInfo("Finished loading save games");
		string p = 0f.Currency(true).Loc();
		FurnitureLoader.Init();
		int frameWait = 8;
		do
		{
			frameWait--;
			yield return new WaitForEndOfFrame();
		}
		while (!SteamWorkshop.DoneLoading || frameWait >= 0);
		RoomMaterialController.Instance.Init();
		this._state = 1;
		this.rotScript.Stop();
		this.RingScript.SetActive(true);
		yield break;
	}

	private void Update()
	{
		if (this._state == 1)
		{
			if (!LoadDebugger.Instance.Errors)
			{
				SceneManager.LoadSceneAsync("MainMenu");
			}
			else
			{
				this.waitForInput = true;
				LoadDebugger.AddInfo("<size=32>Press any key to continue...</size>", "#00FF00");
			}
			this._state = 2;
		}
		if (this.waitForInput && (Input.anyKey || Input.GetMouseButton(0) || Input.GetMouseButton(1)))
		{
			SceneManager.LoadSceneAsync("MainMenu");
			this.waitForInput = false;
		}
	}

	[NonSerialized]
	private int _state;

	private bool waitForInput;

	public UIRotationScript rotScript;

	public GameObject RingScript;
}
