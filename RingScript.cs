﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class RingScript : MonoBehaviour
{
	private void Update()
	{
		if (this.rect == null)
		{
			return;
		}
		this.time += Time.deltaTime;
		this.rect.sizeDelta = new Vector2((float)this.size * this.time, (float)this.size * this.time);
		this.img.color = new Color(1f, 1f, 1f, 1f - this.time);
		if (this.time >= 1f)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public int size;

	public float time;

	public RectTransform rect;

	public RawImage img;
}
