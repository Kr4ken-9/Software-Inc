﻿using System;
using UnityEngine;

[Serializable]
public class ServerDeal : Deal, IServerItem
{
	public ServerDeal(SoftwareProduct product) : base(product.DevCompany, false)
	{
		this._product = product.ID;
		float num = Server.ISPCost / 24f / 60f;
		float num2 = num + 0.005f;
		this.PerPower = Utilities.RandomRange(num2, num2 * 2f);
	}

	public ServerDeal()
	{
	}

	public SoftwareProduct Product
	{
		get
		{
			return GameSettings.Instance.simulation.GetProduct(this._product, false);
		}
	}

	public override float Worth()
	{
		return this.PerPower * 60f;
	}

	public override bool Cancel()
	{
		if (!base.Cancel())
		{
			return false;
		}
		this.Product.ExternalHostingActive = false;
		this.Product.ExternalHosting = 0u;
		if (base.Incoming)
		{
			GameSettings.Instance.DeregisterServerItem(this);
		}
		else
		{
			this.Product.RegisterServer();
		}
		return true;
	}

	public override void Accept(Company company)
	{
		base.Accept(company);
		this.Product.ExternalHostingActive = true;
		if (base.Incoming)
		{
			GameSettings.Instance.RegisterWithServer(null, this, true);
		}
		else
		{
			GameSettings.Instance.DeregisterServerItem(this.Product);
		}
	}

	public override string Description()
	{
		return "HostingDeal".Loc(new object[]
		{
			this.Product.Name
		});
	}

	public override string Title()
	{
		return "Hosting";
	}

	public override float Payout()
	{
		float nextPayment = this.NextPayment;
		this.NextPayment = 0f;
		return nextPayment;
	}

	public float GetLoadRequirement()
	{
		return this.Product.GetLoadRequirement();
	}

	public void HandleLoad(float load)
	{
		float num = this.GetLoadRequirement().BandwidthFactor(SDateTime.Now());
		if (num > 0f)
		{
			this.NextPayment += load * this.PerPower * num * 60f / (float)GameSettings.DaysPerMonth;
		}
		this.incidents = (load < 1f);
		this.Product.HandleLoad(load);
	}

	public string GetDescription()
	{
		return this.Product.GetDescription();
	}

	public void SerializeServer(string name)
	{
		this.activeServer = name;
	}

	public override void HandleUpdate()
	{
		if (!base.Incoming)
		{
			this.NextPayment += Utilities.PerHour(this.GetLoadRequirement() * this.PerPower * 60f, true);
		}
	}

	private float MaximumUsage()
	{
		SoftwareProduct product = this.Product;
		SoftwareType type = product.Type;
		float num = type.Categories[product._category].Popularity * MarketSimulation.Population;
		if (type.OSSpecific)
		{
			num = Mathf.Min(num, GameSettings.Instance.simulation.GetOSCoverage(this.Product.OSs));
		}
		return (this.Product.ServerReq * num).BandwidthFactor(SDateTime.Now());
	}

	public override string[] GetDetailedDescriptionVars()
	{
		return new string[]
		{
			"Type".Loc(),
			"Income per Mb per hour".Loc(),
			"Current bandwidth usage".Loc(),
			"Maximum bandwidth usage".Loc()
		};
	}

	public override string[] GetDetailedDescriptionValues()
	{
		return new string[]
		{
			this.Product._type.LocSW(),
			this.Worth().Currency(true),
			this.Product.GetLoadRequirement().BandwidthFactor(SDateTime.Now()).Bandwidth(),
			this.MaximumUsage().Bandwidth()
		};
	}

	public override float ReputationEffect(bool ending)
	{
		float result = (!this.incidents) ? (this.GetLoadRequirement().BandwidthFactor(SDateTime.Now()) / 1E+07f) : 0f;
		this.incidents = false;
		return result;
	}

	public override string ReputationCategory()
	{
		return "Hosting";
	}

	public bool CancelOnUnload()
	{
		return false;
	}

	private readonly uint _product;

	public readonly float PerPower;

	private float NextPayment;

	public string activeServer;

	private bool incidents;
}
