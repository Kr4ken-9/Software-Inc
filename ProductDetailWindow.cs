﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ProductDetailWindow : MonoBehaviour
{
	private void OnDestroy()
	{
		ProductDetailWindow.OpenProductWindows.Remove(this);
	}

	public void Init(SoftwareProduct p)
	{
		ProductDetailWindow.OpenProductWindows.Add(this);
		if (ProductDetailWindow.OpenProductWindows.Count > 10)
		{
			UnityEngine.Object.Destroy(ProductDetailWindow.OpenProductWindows[0].gameObject);
		}
		this.product = p;
		this.Window.NonLocTitle = "ProductDetailTitle".Loc(new object[]
		{
			this.product.Name
		});
		this.LeftSh.SetData(new string[]
		{
			"Name".Loc(),
			"Company".Loc(),
			"Inventor".Loc(),
			"Type".Loc(),
			"Category".Loc(),
			"Price".Loc(),
			"In-house".Loc(),
			"Release".Loc()
		}, new string[]
		{
			this.product.Name,
			this.product.DevCompany.Name,
			this.product.Inventor,
			this.product._type.LocSW(),
			this.product._category.LocSWC(this.product._type),
			this.product.Price.Currency(true),
			((!this.product.InHouse) ? "No" : "Yes").Loc(),
			this.product.Release.ToCompactString()
		});
		this.RightSh.SetData(new string[]
		{
			"Active users".Loc(),
			"Quality".Loc(),
			"Marketing".Loc(),
			"License cost".Loc(),
			"In stock".Loc(),
			"Units".Loc(),
			"Refunds".Loc(),
			"Gross".Loc(),
			"Loss".Loc()
		}, new string[]
		{
			this.product.Userbase.ToString("N0"),
			SoftwareType.GetQualityLabel(this.product.Quality),
			SoftwareType.GetAwarenessLabel(this.product.GetAwareness()),
			this.product.GetLicenseCost().Currency(true),
			this.product.PhysicalCopies.ToString("N0"),
			this.product.UnitSum.ToString("N0"),
			this.product.RefundSum.ToString("N0"),
			this.product.Sum.Currency(true),
			this.product.Loss.Currency(true)
		});
		this.pieChart.Colors = HUD.ThemeColors.ToList<Color>();
		this.pieChart.Values[0] = this.product.Innovation;
		this.pieChart.Values[1] = this.product.Stability;
		this.pieChart.Values[2] = this.product.Usability;
		this.pieChart.UpdateCachedPie();
		if (this.product.OSs != null)
		{
			this.OS.AddRange(from x in this.product.OSs
			select GameSettings.Instance.simulation.GetProduct(x, false));
		}
		if (this.product.Needs != null)
		{
			this.Needs.AddRange(from x in this.product.Needs.Values
			select GameSettings.Instance.simulation.GetProduct(x, false));
		}
		for (SoftwareProduct sequelTo = this.product.SequelTo; sequelTo != null; sequelTo = sequelTo.SequelTo)
		{
			this.Franchise.Insert(0, sequelTo);
		}
		foreach (SoftwareProduct softwareProduct in GameSettings.Instance.simulation.GetAllProducts())
		{
			if (softwareProduct.OSs != null && softwareProduct.OSs.Contains(this.product.ID))
			{
				this.UsedIn.Add(softwareProduct);
			}
			else if (softwareProduct.Needs != null && softwareProduct.Needs.Values.Contains(this.product.ID))
			{
				this.UsedIn.Add(softwareProduct);
			}
		}
		this.barChart.ToolTipFunc = ((int i, float x, float y) => this.FixDate(this.product.Release, i).ToVeryCompactString() + ": " + x.Currency(true));
		this.barChart.Values[0].Clear();
		for (int j = 0; j < this.PlayerOwnerButtons.Length; j++)
		{
			this.PlayerOwnerButtons[j].SetActive(this.product.DevCompany.Player);
		}
	}

	public void ShowIncomeData()
	{
		this.legend2.gameObject.SetActive(false);
		this.legend2.OnToggle = null;
		this.barChart.Values = new List<List<float>>
		{
			new List<float>()
		};
		this.barChart.UpdateCachedBars();
		this.barChart.ToolTipFunc = ((int i, float x, float y) => this.FixDate(this.product.Release, i).ToVeryCompactString() + ": " + x.Currency(true));
		this.IncomeData = 0;
		this.UpdateMe();
	}

	public void ShowRepData()
	{
		this.legend2.gameObject.SetActive(false);
		this.legend2.OnToggle = null;
		this.barChart.Values = new List<List<float>>
		{
			new List<float>()
		};
		this.barChart.UpdateCachedBars();
		this.barChart.ToolTipFunc = ((int i, float x, float y) => this.FixDate(this.product.Release, i).ToVeryCompactString() + ": " + x.ToString("N0") + " fans");
		this.IncomeData = 1;
		this.UpdateMe();
	}

	public void ShowUnitSales()
	{
		this.legend2.Items.Clear();
		this.legend2.Items.Add("Physical units".Loc());
		this.legend2.Items.Add("Digital units".Loc());
		if (this.product.RefundSum > 0u)
		{
			this.legend2.Items.Add("Refunds".Loc());
		}
		this.legend2.UpdateItems();
		this.legend2.gameObject.SetActive(this.legend2.Items.Count > 1);
		this.barChart.Values = new List<List<float>>();
		this.legend2.OnToggle = delegate
		{
			this.barChart.Values.Clear();
			this.UpdateMe();
		};
		this.barChart.UpdateCachedBars();
		this.barChart.ToolTipFunc = ((int i, float x, float y) => string.Concat(new string[]
		{
			this.FixDate(this.product.Release, i).ToVeryCompactString(),
			": ",
			x.ToString("N0"),
			" units (",
			(x / Mathf.Max(1f, y)).ToPercent(),
			")"
		}));
		this.IncomeData = 2;
		this.UpdateMe();
	}

	private SDateTime FixDate(SDateTime d, int m)
	{
		if (GameSettings.DaysPerMonth > 1 && d.Day == GameSettings.DaysPerMonth - 1)
		{
			return d + new SDateTime(1, m, 0);
		}
		return d + new SDateTime(m, 0);
	}

	public void ToggleMarket()
	{
	}

	public void ShowCompany()
	{
		HUD.Instance.companyWindow.FocusCompany(this.product.DevCompany);
	}

	public void UpdateList(int option)
	{
		this.MainList.Items.Clear();
		this.MainList.LastSort = null;
		this.MainList["ProductNeedType"].gameObject.SetActive(option == 1);
		this.MainList["ProductName"].gameObject.SetActive(option != 4);
		this.MainList["ProductCompany"].gameObject.SetActive(option != 4);
		this.MainList["ProductDetail"].gameObject.SetActive(option != 4);
		this.MainList["GenericName"].gameObject.SetActive(option == 4);
		switch (option)
		{
		case 1:
			this.MainList.Items.AddRange(this.Needs);
			break;
		case 2:
			this.MainList.Items.AddRange(this.Franchise);
			break;
		case 3:
			this.MainList.Items.AddRange(this.UsedIn);
			break;
		case 4:
			this.MainList.Items.AddRange(this.product.Features);
			break;
		default:
			this.MainList.Items.AddRange(this.OS);
			break;
		}
	}

	private void UpdateMainRightText()
	{
		this.RightSh.UpdateValues(new string[]
		{
			this.product.Userbase.ToString("N0"),
			SoftwareType.GetQualityLabel(this.product.Quality),
			SoftwareType.GetAwarenessLabel(this.product.GetAwareness()),
			this.product.GetLicenseCost().Currency(true),
			this.product.PhysicalCopies.ToString("N0"),
			this.product.UnitSum.ToString("N0"),
			this.product.RefundSum.ToString("N0"),
			this.product.Sum.Currency(true),
			this.product.Loss.Currency(true)
		});
	}

	private void Start()
	{
		this.legend.Colors = this.pieChart.Colors;
		this.legend.Items.Add("Innovation".Loc());
		this.legend.Items.Add("Stability".Loc());
		this.legend.Items.Add("Usability".Loc());
		this.legend.UpdateItems();
		this.Window.OnSizeChanged = delegate
		{
			this.pieChart.UpdateCachedPie();
		};
		this.UpdateList(0);
		this.UpdateMe();
	}

	public void UpdateMe()
	{
		this.UpdateMainRightText();
		if (this.IncomeData == 2)
		{
			int num = 3;
			bool flag = this.legend2.IsOn(0);
			bool flag2 = this.legend2.IsOn(1);
			bool flag3 = this.legend2.IsOn(2);
			if (!flag && !flag2 && !flag3)
			{
				num = 0;
			}
			else if (!flag2 && !flag3)
			{
				num = 1;
			}
			else if (!flag3)
			{
				num = 2;
			}
			for (int i = this.barChart.Values.Count; i < num; i++)
			{
				this.barChart.Values.Add(new List<float>());
			}
		}
		else
		{
			while (this.barChart.Values.Count > 1)
			{
				this.barChart.Values.RemoveAt(1);
			}
		}
		if (this.IncomeData == 0)
		{
			this.UpdateValues(0, this.product.GetCashflow());
		}
		else if (this.IncomeData == 1)
		{
			this.UpdateValues(0, this.product.Rep);
		}
		else if (this.IncomeData == 2)
		{
			this.UpdateBar(0, this.product.GetUnitSales(false), this.legend2.IsOn(0));
			this.UpdateBar(1, this.product.GetUnitSales(true), this.legend2.IsOn(1));
			this.UpdateBar(2, this.product.GetRefunds(), this.legend2.IsOn(2));
		}
		this.barChart.UpdateCachedBars();
	}

	private void UpdateValues(int idx, List<float> values)
	{
		if (this.barChart.Values[idx].Count > 0)
		{
			this.barChart.Values[idx][this.barChart.Values[idx].Count - 1] = values[this.barChart.Values[idx].Count - 1];
		}
		if (this.barChart.Values[idx].Count != values.Count)
		{
			for (int i = this.barChart.Values[idx].Count; i < values.Count; i++)
			{
				this.barChart.Values[idx].Add(values[i]);
			}
			this.barChart.UpdateCachedBars();
		}
	}

	private void UpdateBar(int bar, List<int> data, bool getData)
	{
		if (bar < this.barChart.Values.Count)
		{
			if (this.barChart.Values[bar].Count > 0)
			{
				this.barChart.Values[bar][this.barChart.Values[bar].Count - 1] = (float)((!getData) ? 0 : data[this.barChart.Values[bar].Count - 1]);
			}
			if (this.barChart.Values[bar].Count != data.Count)
			{
				for (int i = this.barChart.Values[bar].Count; i < data.Count; i++)
				{
					this.barChart.Values[bar].Add((float)((!getData) ? 0 : data[i]));
				}
				this.barChart.UpdateCachedBars();
			}
		}
	}

	public void PortClick()
	{
		if (this.product.DevCompany.Player)
		{
			ProductWindow.StartPort(this.product);
		}
	}

	public void DistributionClick(bool print)
	{
		if (this.product.DevCompany.Player)
		{
			if (print)
			{
				if (!GameSettings.Instance.PrintOrders.ContainsKey(this.product.ID))
				{
					PrintJob printJob = new PrintJob(this.product.ID);
					GameSettings.Instance.PrintOrders[this.product.ID] = printJob;
					HUD.Instance.distributionWindow.Show(printJob);
				}
			}
			else
			{
				HUD.Instance.copyOrderWindow.Show(this.product);
			}
		}
	}

	public static List<ProductDetailWindow> OpenProductWindows = new List<ProductDetailWindow>();

	public GUIWindow Window;

	public Text ToggleMarketText;

	public VarValueSheet LeftSh;

	public VarValueSheet RightSh;

	public GameObject MarketButton;

	public GUIPieChart pieChart;

	public GUILegend legend;

	public GUILegend legend2;

	public GUIListView MainList;

	public List<object> OS = new List<object>();

	public List<object> Needs = new List<object>();

	public List<object> Franchise = new List<object>();

	public List<object> UsedIn = new List<object>();

	public GUIBarChart barChart;

	private int IncomeData;

	public GameObject[] PlayerOwnerButtons;

	[NonSerialized]
	public SoftwareProduct product;
}
