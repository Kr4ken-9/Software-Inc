﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class ReviewWork : WorkItem
{
	public ReviewWork()
	{
	}

	public ReviewWork(string name)
	{
		base.Name = name;
	}

	public ReviewWork(SoftwareAlpha alpha, string company, bool outsource) : base(alpha.SoftwareName, null, -1)
	{
		this.BuildDate = SDateTime.Now();
		this.TargetWorkID = alpha.ID;
		this.Biased.AddRange(alpha.EverWorked);
		this.ReviewCompany = company;
		this.ProjectName = alpha.SoftwareName;
		base.Name = "ReviewOf".Loc(new object[]
		{
			this.ProjectName
		});
		this.ArtResult = new Dictionary<string, float>();
		this.CodeResult = new Dictionary<string, float>();
		Dictionary<string, int> dictionary = new Dictionary<string, int>();
		for (int i = 0; i < alpha.Specs.Length; i++)
		{
			dictionary[alpha.Specs[i]] = i;
		}
		foreach (KeyValuePair<string, float> keyValuePair in alpha.SpecCodeArt)
		{
			if (keyValuePair.Value > 0f)
			{
				float num = Mathf.Min(alpha.FinalSpecQuality[dictionary[keyValuePair.Key], 0], SoftwareAlpha.GetMaxCodeQuality(alpha.CodeProgress));
				float value = (alpha.contract == null) ? SoftwareAlpha.FinalQualityCalc(alpha.CodeProgressLim, num) : (num / alpha.contract.Quality);
				this.CodeResult[keyValuePair.Key] = value;
			}
			if (keyValuePair.Value < 1f)
			{
				float num2 = alpha.FinalSpecQuality[dictionary[keyValuePair.Key], 1];
				float value2 = (alpha.contract == null) ? SoftwareAlpha.FinalQualityCalc(alpha.ArtProgressLim, num2) : (num2 / alpha.contract.Quality);
				this.ArtResult[keyValuePair.Key] = value2;
			}
		}
		if (this.ReviewCompany != null)
		{
			GameSettings.Instance.ReviewJobs.Add(this);
			int count = this.GetOptimalReviews(alpha) + UnityEngine.Random.Range(0, 10);
			if (outsource)
			{
				this.GenerateSubmitters(1f, 0.2f, 0.5f, 0.05f, count);
				this.CostPerReview = ReviewWork.StandardCost;
			}
			else
			{
				this.GenerateSubmitters(0.5f, 0.2f, 0.25f, 0.1f, count);
			}
		}
	}

	public override Color BackColor
	{
		get
		{
			return new Color(1f, 0.349f, 0.945f);
		}
	}

	public override string GetWorkTypeName()
	{
		return "Peer review";
	}

	public override float StressMultiplier()
	{
		return 0.25f;
	}

	private int GetOptimalReviews(SoftwareAlpha alpha)
	{
		return Mathf.Max(10, Mathf.CeilToInt(50f * Mathf.Clamp01(alpha.DevTime / 12f)));
	}

	private void GenerateSubmitters(float avgSkill, float devSkill, float avgBias, float devBias, int count)
	{
		for (int i = 0; i < count; i++)
		{
			ReviewWork.SubmitterData submitterData = new ReviewWork.SubmitterData(i.ToString(), null, Utilities.RandomGaussClamped(avgBias, devBias) * 2f - 1f, Utilities.RandomGaussClamped(avgSkill, devSkill), Utilities.RandomGaussClamped(avgSkill, devSkill));
			submitterData.Next = -Utilities.RandomValue;
			this.Submitters[submitterData.Name] = submitterData;
		}
	}

	public override string GetIcon()
	{
		return "Smiley";
	}

	public void Tick(float delta)
	{
		if (base.Paused)
		{
			return;
		}
		if (this.Reviews < this.Submitters.Count * (this.ArtResult.Count + this.CodeResult.Count) && (this.CostPerReview <= 0f || GameSettings.Instance.MyCompany.CanMakeTransaction(-this.CostPerReview)))
		{
			foreach (ReviewWork.SubmitterData submitterData in this.Submitters.Values)
			{
				if (submitterData.Reviews < this.ArtResult.Count + this.CodeResult.Count)
				{
					bool flag = submitterData.ArtSkill > submitterData.CodeSkill;
					int num = (!flag) ? this.CodeResult.Count : this.ArtResult.Count;
					if (submitterData.Reviews >= num)
					{
						flag = !flag;
					}
					submitterData.Next += Utilities.PerDay((!flag) ? submitterData.CodeSkill : submitterData.ArtSkill, delta, false) * 100f;
					while (submitterData.Next > 1f && submitterData.Reviews < this.ArtResult.Count + this.CodeResult.Count && (this.CostPerReview <= 0f || GameSettings.Instance.MyCompany.CanMakeTransaction(-this.CostPerReview)))
					{
						if (this.CostPerReview > 0f)
						{
							GameSettings.Instance.MyCompany.MakeTransaction(-this.CostPerReview, Company.TransactionCategory.Bills, "Reviews");
						}
						this.Reviews++;
						submitterData.Next -= 1f;
						submitterData.Reviews++;
					}
				}
			}
		}
	}

	public override WorkItem.HasWorkReturn HasWork(Actor actor)
	{
		ReviewWork.SubmitterData submitterData;
		if (!this.Submitters.TryGetValue(actor.employee.Name, out submitterData))
		{
			return WorkItem.HasWorkReturn.True;
		}
		if (submitterData.Reviews < this.ArtResult.Count + this.CodeResult.Count)
		{
			return WorkItem.HasWorkReturn.True;
		}
		return WorkItem.HasWorkReturn.Finished;
	}

	public override void DoWork(Actor act, float eff, float delta)
	{
		eff *= act.GetPCAddonBonus(Employee.EmployeeRole.Programmer);
		if (act.employee.IsRole(Employee.RoleBit.Lead))
		{
			eff *= 0.5f;
		}
		bool flag = act.employee.GetSkill(Employee.EmployeeRole.Artist) > act.employee.GetSkill(Employee.EmployeeRole.Designer);
		ReviewWork.SubmitterData submitterData;
		if (this.Submitters.TryGetValue(act.employee.Name, out submitterData))
		{
			int num = (!flag) ? this.CodeResult.Count : this.ArtResult.Count;
			if (submitterData.Reviews >= num)
			{
				flag = !flag;
			}
		}
		else
		{
			submitterData = new ReviewWork.SubmitterData(act.employee.Name, act.employee, (!this.Biased.Contains(act.DID)) ? Utilities.RandomGaussClamped(0.5f, 0.1f) : Utilities.RandomGaussClamped(1f, 0.1f), 0f, 0f);
			this.Submitters[act.employee.Name] = submitterData;
		}
		submitterData.Next += Utilities.PerDay(act.employee.GetSkill((!flag) ? Employee.EmployeeRole.Designer : Employee.EmployeeRole.Artist).MapRange(0f, 1f, 0.5f, 1f, false) * eff, delta, true) * 150f;
		while (submitterData.Next > 1f && submitterData.Reviews < this.ArtResult.Count + this.CodeResult.Count)
		{
			this.Reviews++;
			submitterData.Next -= 1f;
			submitterData.Reviews++;
		}
	}

	public override float GetWorkBoost(Employee.EmployeeRole role, float currentSkill)
	{
		return 0f;
	}

	public override Employee.EmployeeRole? GetBoostRole(Actor act)
	{
		return null;
	}

	private void AddResult(Dictionary<string, Dictionary<string, List<SmileyChart.SmileyData>>> result, string major, string minor, SmileyChart.SmileyData data)
	{
		Dictionary<string, List<SmileyChart.SmileyData>> dictionary;
		if (!result.TryGetValue(major, out dictionary))
		{
			dictionary = new Dictionary<string, List<SmileyChart.SmileyData>>();
			result[major] = dictionary;
		}
		List<SmileyChart.SmileyData> list;
		if (!dictionary.TryGetValue(minor, out list))
		{
			list = new List<SmileyChart.SmileyData>();
			dictionary[minor] = list;
		}
		list.Add(data);
	}

	public override string Category()
	{
		if (this.CostPerReview > 0f)
		{
			return "Cost".Loc() + ": " + ((float)this.Reviews * this.CostPerReview).Currency(true);
		}
		return null;
	}

	public override string CurrentStage()
	{
		return "ReviewsWritten".Loc(new object[]
		{
			this.Reviews
		});
	}

	public override float GetProgress()
	{
		return (float)this.Reviews / (float)(this.Submitters.Count * (this.ArtResult.Count + this.CodeResult.Count));
	}

	public void EndReview()
	{
		Dictionary<string, Dictionary<string, List<SmileyChart.SmileyData>>> dictionary = new Dictionary<string, Dictionary<string, List<SmileyChart.SmileyData>>>();
		float num = 0f;
		int num2 = 0;
		float num3 = 0f;
		int num4 = 0;
		foreach (KeyValuePair<string, ReviewWork.SubmitterData> keyValuePair in this.Submitters)
		{
			ReviewWork.SubmitterData sub = keyValuePair.Value;
			bool flag = Utilities.RandomValue > 0.5f;
			if (this.ReviewCompany == null)
			{
				Employee emp = sub.Emp;
				flag = (emp.GetSkill(Employee.EmployeeRole.Artist) > emp.GetSkill(Employee.EmployeeRole.Designer));
			}
			for (int i = 0; i < 2; i++)
			{
				Dictionary<string, float> source = (!flag) ? this.CodeResult : this.ArtResult;
				bool art1 = flag;
				foreach (KeyValuePair<string, float> keyValuePair2 in from x in source
				orderby (this.ReviewCompany == null) ? sub.Emp.GetSpecialization((!art1) ? Employee.EmployeeRole.Designer : Employee.EmployeeRole.Artist, x.Key, false) : Utilities.RandomValue descending
				select x)
				{
					if (sub.Reviews == 0)
					{
						break;
					}
					float num5 = (this.ReviewCompany == null) ? sub.Emp.GetSpecialization((!art1) ? Employee.EmployeeRole.Designer : Employee.EmployeeRole.Artist, keyValuePair2.Key, false) : ((!flag) ? sub.CodeSkill : sub.ArtSkill);
					float num6 = Utilities.RandomGaussClamped(Mathf.Clamp01(keyValuePair2.Value + sub.Bias / 10f), Mathf.Clamp01(0.02f + (1f - num5) * 0.13f));
					SmileyChart.SmileyData data = new SmileyChart.SmileyData((this.ReviewCompany == null) ? sub.Name : "Anonymous".Loc(), num5, num6, sub.Bias);
					num += num6;
					num2++;
					num3 += num5 * (1f - Mathf.Abs(sub.Bias));
					num4++;
					this.AddResult(dictionary, (!flag) ? "Code" : "Art", keyValuePair2.Key, data);
					sub.Reviews--;
				}
				if (sub.Reviews == 0)
				{
					break;
				}
				flag = !flag;
			}
		}
		int num7;
		if (dictionary.Count > 0)
		{
			num7 = dictionary.Sum((KeyValuePair<string, Dictionary<string, List<SmileyChart.SmileyData>>> x) => x.Value.Count);
		}
		else
		{
			num7 = 0;
		}
		int num8 = num7;
		if (num8 > 0)
		{
			float accuracy = num3 / (float)num4;
			ReviewWindow.ReviewData item = new ReviewWindow.ReviewData(dictionary, this.BuildDate, num / (float)num2, accuracy);
			SoftwareAlpha softwareAlpha = null;
			if (this.TargetWorkID > 0u)
			{
				softwareAlpha = (GameSettings.Instance.MyCompany.WorkItems.FirstOrDefault((WorkItem x) => x.ID == this.TargetWorkID) as SoftwareAlpha);
				if (softwareAlpha != null)
				{
					softwareAlpha.PastReviews.Add(item);
					float num9 = (float)this.GetOptimalReviews(softwareAlpha);
					num9 *= (float)(this.ArtResult.Count + this.CodeResult.Count);
					num9 = Mathf.Clamp01((float)this.Reviews / num9);
					softwareAlpha.AddReviewScore(accuracy, num9);
				}
			}
			HUD.Instance.reviewWindow.Show(new ReviewWindow.ReviewData(dictionary, this.BuildDate, num / (float)num2, num3 / (float)num4), softwareAlpha);
		}
		GameSettings.Instance.ReviewJobs.Remove(this);
		this.Kill(false);
	}

	public override string GetTeam()
	{
		return this.ReviewCompany ?? base.GetTeam();
	}

	public override string HightlightButton()
	{
		return (this.Reviews != this.Submitters.Count * (this.ArtResult.Count + this.CodeResult.Count)) ? null : "End";
	}

	public override int EmitType(Actor actor)
	{
		return 2;
	}

	public override void AddCost(float cost)
	{
		WorkItem workItem = GameSettings.Instance.MyCompany.WorkItems.FirstOrDefault((WorkItem x) => x.ID == this.TargetWorkID);
		if (workItem != null)
		{
			workItem.AddCost(cost);
		}
	}

	public Dictionary<string, float> ArtResult;

	public Dictionary<string, float> CodeResult;

	public Dictionary<string, ReviewWork.SubmitterData> Submitters = new Dictionary<string, ReviewWork.SubmitterData>();

	public string ReviewCompany;

	public string ProjectName;

	public int Reviews;

	public static float StandardCost = 100f;

	public float CostPerReview;

	public SDateTime BuildDate;

	public SHashSet<uint> Biased = new SHashSet<uint>();

	public uint TargetWorkID;

	[Serializable]
	public class SubmitterData
	{
		public SubmitterData()
		{
		}

		public SubmitterData(string name, Employee emp, float bias, float artSkill, float codeSkill)
		{
			this.Name = name;
			this.Emp = emp;
			this.Bias = bias;
			this.ArtSkill = artSkill;
			this.CodeSkill = codeSkill;
		}

		public string Name;

		public Employee Emp;

		public float Bias;

		public float ArtSkill;

		public float CodeSkill;

		public int Reviews;

		public float Next;
	}
}
