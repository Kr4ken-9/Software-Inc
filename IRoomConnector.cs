﻿using System;
using UnityEngine;

public interface IRoomConnector
{
	bool IsConnecter { get; set; }

	PathNode<Vector3> pathNode { get; set; }

	Vector3 GetOffsetPos(Room room, bool inverse = false);

	Transform ObjectTransform { get; }

	bool IsNull { get; }

	void UpdateBlocked();

	Transform[] IntermediatePoints(Room from);
}
