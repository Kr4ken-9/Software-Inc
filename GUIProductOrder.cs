﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GUIProductOrder : MonoBehaviour
{
	private void UpdateLimitLabel(uint? limit)
	{
		if (this.LimitLabel != null)
		{
			this.LimitLabel.text = ((limit == null) ? "Unlimited".Loc() : limit.Value.ToString("N0"));
		}
	}

	public bool Init()
	{
		SoftwareProduct product = GameSettings.Instance.simulation.GetProduct(this.OrderID, false);
		PrintJob orNull = GameSettings.Instance.PrintOrders.GetOrNull(this.OrderID);
		if (orNull == null)
		{
			return false;
		}
		this.UpdateLimitLabel(orNull.Limit);
		this.PrioritySlider.value = orNull.Priority;
		if (product != null)
		{
			this.Stockable = product;
			this.ProductNameLabel.text = product.Name;
			this.CompanyLabel.text = product.DevCompany.Name;
			return true;
		}
		SoftwareAlpha softwareAlpha = GameSettings.Instance.MyCompany.WorkItems.OfType<SoftwareAlpha>().FirstOrDefault((SoftwareAlpha x) => x.SWID != null && x.SWID.Value == this.OrderID);
		if (softwareAlpha != null)
		{
			this.Stockable = softwareAlpha;
			this.ProductNameLabel.text = softwareAlpha.SoftwareName;
			this.CompanyLabel.text = GameSettings.Instance.MyCompany.Name;
			return true;
		}
		foreach (SimulatedCompany simulatedCompany in GameSettings.Instance.simulation.Companies.Values)
		{
			SimulatedCompany.ProductPrototype productPrototype = simulatedCompany.Releases.FirstOrDefault((SimulatedCompany.ProductPrototype x) => x.SWID != null && x.SWID == this.OrderID);
			if (productPrototype != null)
			{
				this.Stockable = productPrototype;
				this.ProductNameLabel.text = productPrototype.Name;
				this.CompanyLabel.text = simulatedCompany.Name;
				Deal deal;
				if (orNull.DealID != null && HUD.Instance.dealWindow.AllDeals.TryGetValue(orNull.DealID.Value, out deal))
				{
					PrintDeal printDeal = deal as PrintDeal;
					if (printDeal != null)
					{
						this.Deal = printDeal;
						this.DeadlineLabel.text = this.Deal.EndDate.ToCompactString2();
					}
				}
				return true;
			}
		}
		return false;
	}

	public void ChangeLimit()
	{
		PrintJob order = GameSettings.Instance.PrintOrders.GetOrNull(this.OrderID);
		if (order != null)
		{
			WindowManager.SpawnInputDialog("LimitCopyPrompt".Loc(), "Maximum copies".Loc(), (order.Limit == null) ? "-1" : order.Limit.Value.ToString("N0"), delegate(string x)
			{
				if (x.Trim().Equals("-1"))
				{
					order.Limit = null;
					this.UpdateLimitLabel(null);
				}
				else
				{
					try
					{
						uint value = Convert.ToUInt32(x.Replace(",", string.Empty));
						order.Limit = new uint?(value);
						this.UpdateLimitLabel(new uint?(value));
					}
					catch (Exception)
					{
					}
				}
			}, null);
		}
	}

	public void UpdatePriority()
	{
		PrintJob orNull = GameSettings.Instance.PrintOrders.GetOrNull(this.OrderID);
		if (orNull != null)
		{
			orNull.Priority = this.PrioritySlider.value;
		}
	}

	public void UpdateContent(float speed, float MaxP)
	{
		PrintJob orNull = GameSettings.Instance.PrintOrders.GetOrNull(this.OrderID);
		if (orNull != null)
		{
			this.StockLabel.text = this.Stockable.PhysicalCopies.ToString("N0") + " + " + GameSettings.Instance.GetPrintsInStorage(this.OrderID, false).ToString("N0");
			int num = Mathf.RoundToInt(speed * orNull.Priority / MaxP);
			this.PerMonthLabel.text = num.ToString("N0");
			if (this.Deal != null)
			{
				if (this.Deal.CurrentAmount > this.Deal.Goal)
				{
					this.GoalLabel.text = "+" + (this.Deal.CurrentAmount - this.Deal.Goal).ToString("N0");
					this.PerMonthLabel.color = this.BonusColor;
				}
				else
				{
					uint num2 = this.Deal.Goal - this.Deal.CurrentAmount;
					this.GoalLabel.text = num2.ToString("N0");
					this.PerMonthLabel.color = ((Utilities.GetMonths(SDateTime.Now(), this.Deal.EndDate) * (float)num >= num2) ? this.NormalColor : this.WarningColor);
				}
			}
		}
	}

	public void Cancel()
	{
		GameSettings.Instance.CancelPrintOrder(this.OrderID, true);
		HUD.Instance.distributionWindow.RefreshOrders();
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public Text ProductNameLabel;

	public Text CompanyLabel;

	public Text StockLabel;

	public Text PerMonthLabel;

	public Text GoalLabel;

	public Text DeadlineLabel;

	public Text LimitLabel;

	public Slider PrioritySlider;

	[NonSerialized]
	public uint OrderID;

	[NonSerialized]
	public PrintDeal Deal;

	[NonSerialized]
	public IStockable Stockable;

	public Color NormalColor;

	public Color WarningColor;

	public Color BonusColor;
}
