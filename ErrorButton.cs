﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ErrorButton : MonoBehaviour
{
	public void ShowError(string errorMsg)
	{
		if (this.CanShow && !errorMsg.StartsWith("Ignore:") && !errorMsg.Contains("Point on constrained edge not supported yet"))
		{
			this.ShowNow = true;
			this.ErrorMsg = Versioning.VersionString + "\n" + errorMsg;
			this.CanShow = false;
		}
	}

	public void Click()
	{
		this.ShowNow = false;
		this.Button.sizeDelta = new Vector2(32f, 0f);
		GameSettings.ForcePause = true;
		DialogWindow dialog = WindowManager.SpawnDialog();
		dialog.Show("ExceptionNotificationMsg".Loc(), false, DialogWindow.DialogType.Information, new KeyValuePair<string, Action>[]
		{
			new KeyValuePair<string, Action>("Dismiss", delegate
			{
				GameSettings.ForcePause = false;
				dialog.Window.Close();
			}),
			new KeyValuePair<string, Action>("See error", delegate
			{
				WindowManager.Instance.ShowMessageBox(this.ErrorMsg, false, DialogWindow.DialogType.Information);
			})
		});
	}

	private void Update()
	{
		if (this.ShowNow && this.Button.sizeDelta.y < 32f)
		{
			this.Button.sizeDelta = new Vector2(this.Button.sizeDelta.x, Mathf.Lerp(this.Button.sizeDelta.y, 32f, Time.deltaTime * 10f));
		}
	}

	public RectTransform Button;

	private string ErrorMsg;

	private bool CanShow = true;

	private bool ShowNow;
}
