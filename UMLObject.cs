﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UMLObject : MonoBehaviour
{
	private void Start()
	{
		this.OriginalNum = (this.Num = Mathf.RoundToInt(1f + Utilities.RandomGaussClamped(0f, 0.3f) * 3f));
		this.Label.text = this.Num.ToString();
		this.Label2.text = GameData.GetStaticNameGenerator("UMLNames").GenerateName();
	}

	public Rect GetRect()
	{
		return new Rect(this.Rect.anchoredPosition - this.Rect.sizeDelta / 2f, this.Rect.sizeDelta);
	}

	public void DestroyMe()
	{
		this.Label.text = this.OriginalNum.ToString();
		ShortcutExtensions46.DOSizeDelta(this.Rect, this.Rect.sizeDelta * 2f, 1f, false);
		ShortcutExtensions46.DOColor(this.Label2, new Color(1f, 1f, 1f, 0f), 1f);
		ShortcutExtensions46.DOColor(this.HeaderLabel, new Color(1f, 1f, 1f, 0f), 1f);
		ShortcutExtensions46.DOColor(base.GetComponent<Image>(), new Color(1f, 1f, 1f, 0f), 1f);
		TweenSettingsExtensions.OnComplete<Sequence>(TweenSettingsExtensions.Append(TweenSettingsExtensions.Append(DOTween.Sequence(), DOTween.To(() => this.Label.fontSize, delegate(int x)
		{
			this.Label.fontSize = x;
		}, 48, 1f)), ShortcutExtensions46.DOColor(this.Label, new Color(1f, 1f, 1f, 0f), 0.5f)), delegate
		{
			UnityEngine.Object.Destroy(base.gameObject);
		});
	}

	public Text Label;

	public Text Label2;

	public Image HeaderLabel;

	public int OriginalNum;

	public int Num;

	public RectTransform Rect;
}
