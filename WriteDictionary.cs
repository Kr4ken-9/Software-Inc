﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class WriteDictionary : IEnumerable<KeyValuePair<string, object>>, IEnumerable
{
	public WriteDictionary()
	{
	}

	public WriteDictionary(string name)
	{
		this.Name = name;
	}

	public object this[string key]
	{
		get
		{
			object result = null;
			if (this.Values.TryGetValue(key, out result))
			{
				return result;
			}
			return null;
		}
		set
		{
			this.Values[key] = value;
		}
	}

	public T Get<T>(string key, T defaultValue)
	{
		object obj = this[key];
		if (obj == null)
		{
			return defaultValue;
		}
		return (!(obj is T)) ? defaultValue : ((T)((object)obj));
	}

	public T Get<T>(string key)
	{
		object obj = this[key];
		if (obj == null)
		{
			throw new Exception("Entry " + key + " was not present in WriteDictionary");
		}
		if (obj is T)
		{
			return (T)((object)obj);
		}
		throw new Exception(string.Concat(new string[]
		{
			"Entry ",
			key,
			" was not of type ",
			typeof(T).Name,
			" in WriteDictionary"
		}));
	}

	public bool Contains(string key)
	{
		return this.Values.ContainsKey(key);
	}

	public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
	{
		return this.Values.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return this.Values.GetEnumerator();
	}

	public readonly string Name;

	private Dictionary<string, object> Values = new Dictionary<string, object>();
}
