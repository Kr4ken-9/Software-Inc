﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class AI<T>
{
	public AI()
	{
		this.Initialize();
	}

	public string CurrentNodeLabel
	{
		get
		{
			return (this.currentNode != null) ? AI<T>.Translation[this.currentNode.Name] : "Idle";
		}
	}

	public void Initialize()
	{
		if (this.root != null)
		{
			this.ResetSimulation();
			return;
		}
		this.InitTree();
		this.BehaviorNodes["Dummy"] = AI<T>.DummyNode;
		this.root = this.BehaviorNodes["Spawn"];
		this.ResetSimulation();
	}

	public abstract void InitTree();

	public void RunSimulation(T self)
	{
		if (this.currentNode == null)
		{
			return;
		}
		int i = 0;
		while (i <= 25)
		{
			this.LastResult = this.currentNode.Run(self);
			if (this.LastResult == 0)
			{
				this.currentNode = this.currentNode.Failure;
			}
			if (this.LastResult == 2)
			{
				this.currentNode = this.currentNode.Success;
			}
			i++;
			if (this.currentNode == null || this.currentNode.Atomic)
			{
				return;
			}
		}
		Debug.LogError("AI went for infinite loop in state " + this.currentNode.Name);
	}

	public void ResetSimulation()
	{
		this.currentNode = this.root;
	}

	public static AI<Actor> LoadAI(AI<T>.AIType type)
	{
		switch (type)
		{
		case AI<T>.AIType.Employee:
			return new EmployeeAI();
		case AI<T>.AIType.Janitor:
			return new JanitorAI();
		case AI<T>.AIType.Cleaning:
			return new CleaningAI();
		case AI<T>.AIType.IT:
			return new ITAI();
		case AI<T>.AIType.Receptionist:
			return new ReceptionAI();
		case AI<T>.AIType.Guest:
			return new GuestAI();
		case AI<T>.AIType.Cook:
			return new CookAI();
		case AI<T>.AIType.Courier:
			return new CourierAI();
		default:
			return null;
		}
	}

	public static Dictionary<string, string> Translation = new Dictionary<string, string>
	{
		{
			"Spawn",
			"Idle"
		},
		{
			"GoToDesk",
			"Looking for computer"
		},
		{
			"IsOff",
			"Idle"
		},
		{
			"GoHome",
			"Going home"
		},
		{
			"Despawn",
			"Idle"
		},
		{
			"StandAround",
			"Idle"
		},
		{
			"Work",
			"Working"
		},
		{
			"GoToReceptionDesk",
			"Working"
		},
		{
			"WaitAtDesk",
			"Working"
		},
		{
			"IsHungry",
			"Idle"
		},
		{
			"GoToFridge",
			"Going to get food"
		},
		{
			"WantCoffee",
			"Idle"
		},
		{
			"LookForReceptionDesk",
			"Idle"
		},
		{
			"IsUp",
			"Idle"
		},
		{
			"GoToCoffee",
			"Going to get coffee"
		},
		{
			"MakeCoffee",
			"Making coffee"
		},
		{
			"GetFood",
			"Getting food"
		},
		{
			"HasMakeMeeting",
			"Idle"
		},
		{
			"GoToMeeting",
			"Going to meeting"
		},
		{
			"HaveMeeting",
			"In meeting"
		},
		{
			"HasToPee",
			"Idle"
		},
		{
			"GoToToilet",
			"Going to toilet"
		},
		{
			"DoBusiness",
			"Pooping"
		},
		{
			"Loiter",
			"Idle"
		},
		{
			"FindRepair",
			"Looking for broken stuff"
		},
		{
			"GoToRepair",
			"Going to repair something"
		},
		{
			"Repair",
			"Repairing"
		},
		{
			"FindCleanRoom",
			"Looking for dirty room"
		},
		{
			"GotoCleanSpot",
			"Going to clean room"
		},
		{
			"Clean",
			"Cleaning"
		},
		{
			"FindNewSpot",
			"Looking for dirt"
		},
		{
			"GoHomeBusStop",
			"Going home"
		},
		{
			"ShouldUseBus",
			"Going home"
		},
		{
			"NeedsSocial",
			"Idle"
		},
		{
			"GoToSocial",
			"Idle"
		},
		{
			"BeSocial",
			"Idle"
		},
		{
			"GoToSocialLeader",
			"Idle"
		},
		{
			"BeSocialLeader",
			"Idle"
		},
		{
			"NeedsSocialLeader",
			"Idle"
		},
		{
			"CanCook",
			"Idle"
		},
		{
			"FindFridge",
			"Getting food"
		},
		{
			"CookFood",
			"Idle"
		},
		{
			"FindStove",
			"Preparing food"
		},
		{
			"FoodReady",
			"Idle"
		},
		{
			"CookLoiter",
			"Idle"
		},
		{
			"FetchFood",
			"Getting food"
		},
		{
			"FindTray",
			"Serving food"
		},
		{
			"GotoTray",
			"Serving food"
		},
		{
			"GoToServingTray",
			"Getting food"
		},
		{
			"TakeFoodFromTray",
			"Getting food"
		},
		{
			"GoToEat",
			"Getting food"
		},
		{
			"EatRealFood",
			"Getting food"
		},
		{
			"HasCopies",
			"Working"
		},
		{
			"AnyCopies",
			"Working"
		},
		{
			"GotoPrinter",
			"Working"
		},
		{
			"TakePrinter",
			"Working"
		},
		{
			"GotoPallet",
			"Working"
		},
		{
			"TakePallet",
			"Working"
		},
		{
			"GotoVan",
			"Working"
		},
		{
			"DropProducts",
			"Working"
		},
		{
			"Dummy",
			"Idle"
		}
	};

	public static BehaviorNode<T> DummyNode = new BehaviorNode<T>("Dummy", (T x) => 1, true, -1);

	private BehaviorNode<T> root;

	public Dictionary<string, BehaviorNode<T>> BehaviorNodes = new Dictionary<string, BehaviorNode<T>>();

	public BehaviorNode<T> currentNode;

	public int LastResult = -1;

	public enum AIType
	{
		Employee,
		Janitor,
		Cleaning,
		IT,
		Receptionist,
		Guest,
		Cook,
		Courier
	}
}
