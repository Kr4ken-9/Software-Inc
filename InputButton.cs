﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class InputButton : MonoBehaviour
{
	private void Start()
	{
		this.UpdateText();
	}

	public void ToggleInput()
	{
		if (this.Active)
		{
			this.Active = false;
			this.UpdateText();
		}
		else
		{
			this.Active = true;
			this.label.text = "KeyBindingHint".Loc();
		}
	}

	public void UpdateText()
	{
		this.label.text = InputController.GetKeyBindString(this.Key, this.Alt);
		bool[] mods = InputController.GetMods(this.Key, this.Alt);
		for (int i = 0; i < mods.Length; i++)
		{
			this.Mods[i].gameObject.SetActive(mods[i]);
		}
	}

	private void Update()
	{
		if (this.Active)
		{
			bool flag = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
			bool flag2 = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
			bool flag3 = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
			bool flag4 = Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand);
			bool key = Input.GetKey(KeyCode.AltGr);
			this.Mods[0].gameObject.SetActive(flag);
			this.Mods[1].gameObject.SetActive(flag2);
			this.Mods[2].gameObject.SetActive(flag3);
			this.Mods[3].gameObject.SetActive(flag4);
			if (Input.GetKey(KeyCode.Escape))
			{
				if (this.Alt)
				{
					InputController.BindKey(this.Key, KeyCode.Escape, true, true, InputController.Modifiers.NONE);
					Options.SaveToFile();
				}
				foreach (InputButton inputButton in OptionsWindow.Instance.InputButtons)
				{
					inputButton.UpdateText();
				}
				this.Active = false;
			}
			else if (Input.anyKey)
			{
				KeyCode? keyPressed = this.GetKeyPressed();
				if (keyPressed != null)
				{
					InputController.BindKey(this.Key, keyPressed.Value, this.Alt, true, flag, flag2, flag3, flag4);
					foreach (InputButton inputButton2 in OptionsWindow.Instance.InputButtons)
					{
						inputButton2.UpdateText();
					}
					Options.SaveToFile();
					this.Active = false;
				}
				else if (!flag && !flag2 && !flag3 && !flag4 && !key)
				{
					WindowManager.SpawnDialog("KeyBindError".Loc(), true, DialogWindow.DialogType.Error);
					this.UpdateText();
					this.Active = false;
				}
			}
		}
	}

	private KeyCode? GetKeyPressed()
	{
		Array values = Enum.GetValues(typeof(KeyCode));
		IEnumerator enumerator = values.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				KeyCode keyCode = (KeyCode)obj;
				if (keyCode != KeyCode.LeftControl && keyCode != KeyCode.RightControl && keyCode != KeyCode.LeftShift && keyCode != KeyCode.RightShift && keyCode != KeyCode.LeftAlt && keyCode != KeyCode.RightAlt && keyCode != KeyCode.LeftCommand && keyCode != KeyCode.RightCommand && keyCode != KeyCode.AltGr && Input.GetKey(keyCode))
				{
					return new KeyCode?(keyCode);
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		return null;
	}

	public Button button;

	public RawImage[] Mods;

	public Text label;

	private bool Active;

	public bool Alt;

	public InputController.Keys Key;
}
