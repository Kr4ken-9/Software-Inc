﻿using System;
using UnityEngine;

[Serializable]
public class SRect
{
	public SRect(float X, float Y, float W, float H)
	{
		this.x = X;
		this.y = Y;
		this.width = W;
		this.height = H;
	}

	public static explicit operator SRect(Rect r)
	{
		return new SRect(r.x, r.y, r.width, r.height);
	}

	public Rect ToRect()
	{
		return new Rect(this.x, this.y, this.width, this.height);
	}

	public float x;

	public float y;

	public float width;

	public float height;
}
