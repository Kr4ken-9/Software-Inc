﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogWindow : MonoBehaviour
{
	public void Show(string msg, bool draw, DialogWindow.DialogType type, params KeyValuePair<string, Action>[] buttons)
	{
		if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter) || (this.Window.Modal && Input.GetKey(KeyCode.Escape)))
		{
			this._waitForReturn = true;
		}
		if (msg == null)
		{
			msg = string.Empty;
		}
		switch (type)
		{
		case DialogWindow.DialogType.Information:
			UISoundFX.PlaySFX("MessageNeutral", -1f, 0f);
			break;
		case DialogWindow.DialogType.Warning:
			UISoundFX.PlaySFX("MessageIssue", -1f, 0f);
			break;
		case DialogWindow.DialogType.Error:
			UISoundFX.PlaySFX("MessageWarning", -1f, 0f);
			break;
		case DialogWindow.DialogType.Question:
			UISoundFX.PlaySFX("MessageGood", -1f, 0f);
			break;
		}
		this.Icon.sprite = this.Icons[(int)type];
		Graphic iconPanel = this.IconPanel;
		Color color = this.Colors[(int)type];
		this.TopPanel.color = color;
		iconPanel.color = color;
		this.Window.Modal = !draw;
		this.Window.StartHidden = false;
		this.Window.Show();
		this.DialogText.text = msg;
		RectTransform component = this.Window.GetComponent<RectTransform>();
		float num = DialogWindow.ForceWidth;
		do
		{
			component.sizeDelta = new Vector2(num, Mathf.Min((float)Screen.height, Mathf.Ceil((float)(msg.Length * 8 + 20) / num) * 18f + 80f));
			num += 128f;
		}
		while (component.sizeDelta.y > (float)(Screen.height - 128) && num < (float)(Screen.width - 128));
		this.Window.MinSize = component.sizeDelta;
		component.anchoredPosition = new Vector2((float)Screen.width / Options.UISize / 2f - component.rect.width / 2f, (float)(-(float)Screen.height) / Options.UISize / 2f + component.rect.height / 2f);
		if (buttons == null || buttons.Length == 0)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
			Button component2 = gameObject.GetComponent<Button>();
			component2.GetComponentInChildren<Text>().text = "OK".Loc();
			this.CancelAction = (this.OKAction = delegate
			{
				this.Window.Close();
			});
			component2.onClick.AddListener(delegate
			{
				this.Window.Close();
			});
			gameObject.transform.SetParent(this.ButtonPanel.transform, false);
		}
		else
		{
			this.OKAction = buttons[0].Value;
			this.CancelAction = ((buttons.Length <= 1) ? this.OKAction : buttons[buttons.Length - 1].Value);
			for (int i = 0; i < buttons.Length; i++)
			{
				KeyValuePair<string, Action> keyValuePair = buttons[i];
				GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.ButtonPrefab);
				Button component3 = gameObject2.GetComponent<Button>();
				component3.GetComponentInChildren<Text>().text = keyValuePair.Key.LocTry();
				Action action = keyValuePair.Value;
				component3.onClick.AddListener(delegate
				{
					action();
				});
				gameObject2.transform.SetParent(this.ButtonPanel.transform, false);
			}
		}
		this.ButtonPanel.GetComponent<HorizontalLayoutGroup>().CalculateLayoutInputHorizontal();
		this.ButtonPanel.GetComponent<HorizontalLayoutGroup>().CalculateLayoutInputVertical();
		this.ButtonPanel.GetComponent<HorizontalLayoutGroup>().SetLayoutHorizontal();
		this.ButtonPanel.GetComponent<HorizontalLayoutGroup>().SetLayoutVertical();
	}

	private void Update()
	{
		if (this.OKAction != null && WindowManager.Instance.IsActiveWindow(this.Window))
		{
			if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.KeypadEnter))
			{
				if (this._waitForReturn)
				{
					this._waitForReturn = false;
					return;
				}
				this.OKAction();
			}
			if (this.Window.Modal && Input.GetKeyUp(KeyCode.Escape))
			{
				if (this._waitForReturn)
				{
					this._waitForReturn = false;
					return;
				}
				this.CancelAction();
			}
		}
	}

	public void Demo(string msg)
	{
		this.Show(msg, false, DialogWindow.DialogType.Information, null);
	}

	private bool _waitForReturn;

	public static float ForceWidth = 340f;

	public GUIWindow Window;

	public Text DialogText;

	public GameObject ButtonPanel;

	public GameObject ButtonPrefab;

	public Action OKAction;

	public Action CancelAction;

	public Sprite[] Icons;

	public Color[] Colors;

	public Image TopPanel;

	public Image IconPanel;

	public Image Icon;

	public enum DialogType
	{
		Information,
		Warning,
		Error,
		Question
	}
}
