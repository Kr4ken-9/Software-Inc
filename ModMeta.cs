﻿using System;
using UnityEngine;

public interface ModMeta
{
	string Name { get; }

	void ConstructOptionsScreen(RectTransform parent, ModBehaviour[] behaviours);
}
